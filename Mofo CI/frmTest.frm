VERSION 5.00
Begin VB.Form frmTest 
   Caption         =   "Test App"
   ClientHeight    =   3045
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   4005
   Icon            =   "frmTest.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   3045
   ScaleWidth      =   4005
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton cmdRun 
      Caption         =   "RUN"
      Height          =   735
      Left            =   960
      TabIndex        =   1
      Top             =   600
      Width           =   1935
   End
   Begin VB.CommandButton cmdStop 
      Caption         =   "STOP"
      Height          =   735
      Left            =   960
      TabIndex        =   0
      Top             =   1800
      Width           =   1935
   End
End
Attribute VB_Name = "frmTest"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private MofoCI As Contacts

Private Sub cmdRun_Click()

Dim i As Integer
Dim strAddressee As String
Dim colContacts As Collection

Set MofoCI = New Contacts
Set colContacts = MofoCI.Retrieve

    For i = 1 To colContacts.Count
        strAddressee = Trim(MofoCI.ContactInfo(i).FullName)
        If MofoCI.ContactInfo(i).JobTitle <> "" Then
            strAddressee = strAddressee & vbCr & Trim(MofoCI.ContactInfo(i).JobTitle)
        End If
            If MofoCI.ContactInfo(i).CompanyName <> "" Then
                strAddressee = strAddressee & vbCr & Trim(MofoCI.ContactInfo(i).CompanyName)
            End If
                If MofoCI.ContactInfo(i).MailingAddress <> "" Then
                    strAddressee = strAddressee & vbCr & Trim(MofoCI.ContactInfo(i).MailingAddress)
                End If
        MsgBox strAddressee
    Next i


ExitHere:
Set MofoCI = Nothing
cmdStop_Click

End Sub

Private Sub cmdStop_Click()
frmTest.Hide
Unload frmTest
Set MofoCI = Nothing

End Sub

Private Sub Form_Load()

   Me.Show
End Sub
