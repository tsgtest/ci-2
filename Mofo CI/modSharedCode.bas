Attribute VB_Name = "modSharedCode"
Option Explicit
Public gobjNameSpace As Outlook.NameSpace
Public gobjOutlook As Outlook.Application
Public gobjContactFolder As Object
Public gobjContactItems As Items
Public mstrStoreID As String
Public Const gLstEntryID = 1
Public Const gLstStoreID = 2
Private Const MAPI_User_Cancel = -2147221229
Private Const mconErrNoPermissions = -2147221233  'User does not have permission to open Contacts folder
Private Const mconErrNoMailbox = -2147467259          'User does not have mailbox set up on server yet

'===============================================================
'Public Function GetToken
'This function parses StrItem and returns the first token.  It uses a semi-colon ";"
'as the delimiter
'===============================================================

Function GetToken(StrItem)
    
    Dim DelimPos
    If Len(StrItem) = 0 Then
        GetToken = ""
    Else
        DelimPos = InStr(StrItem, ";")
        If DelimPos = 0 Then
            GetToken = StrItem
            StrItem = ""
        Else
            GetToken = Left(StrItem, DelimPos - 1)
            StrItem = Right(StrItem, Len(StrItem) - DelimPos)
        End If
    End If

End Function

'===============================================================
'Public Function GetValue
'Reads entries from the registry
'===============================================================

Public Function GetValue(ByVal strSection As String, ByVal strKeyName As String, Optional ByVal varIniLocation As Variant, Optional ByVal varMaxLength As Variant) As String
    
   Dim strValue As String
   Dim strMessage As String
   Dim lngNumCharReturned As Long
   Dim RegistryAgent As clsRegistryAgent
   Dim tTempKey            As String
   On Error GoTo ErrorHandler
   
    Set RegistryAgent = New clsRegistryAgent
    RegistryAgent.RegistryKey = "software\MoFoSetup\InsertFromOutlook"
    RegistryAgent.SubKey = strKeyName
    RegistryAgent.GetValue
    If RegistryAgent.Status <> 0 Then
        GetValue = ""
    Else
        GetValue = RegistryAgent.KeyValue
    End If
   Set RegistryAgent = Nothing
   On Error Resume Next
ExitHere:
   Exit Function
ErrorHandler:
    MsgBox "An error occured in retreiving saved information." & vbCrLf & Err.Description, vbCritical, "Insert From Outlook:GetValue"
   Resume Next
End Function

'===============================================================
'Public Function GetValue
'Writes entries to the registry
'===============================================================

Public Function SetValue(ByVal strSection As String, ByVal strKeyName As String, ByVal strValue As String, Optional varIniLocation As Variant) As Long
   
   Dim RegistryAgent As clsRegistryAgent
   Dim tTempKey            As String
   On Error GoTo ErrorHandler
   
    Set RegistryAgent = New clsRegistryAgent
    RegistryAgent.RegistryKey = "software\MoFoSetup\InsertFromOutlook"
    RegistryAgent.SubKey = strKeyName
    RegistryAgent.CreateKey
    RegistryAgent.KeyValue = strValue
    RegistryAgent.SetValue
    If RegistryAgent.Status <> 0 Then
        MsgBox "Could not save setting for " & strKeyName & vbCrLf & RegistryAgent.GetErrorText, vbExclamation, "Insert From Outlook: Setvalue"
    End If
    Set RegistryAgent = Nothing
    On Error Resume Next
ExitHere:
   Exit Function
ErrorHandler:
    MsgBox "An error occured in saving information." & vbCrLf & Err.Description, vbCritical, "Insert From Outlook:SetValue"
   Resume Next
End Function

'===============================================================
'Public Function OpenFolders
'===============================================================

Public Function funOpenFolders() As Object

Dim i As Integer
Dim intLength As Integer
Dim intSelected As Integer
Dim varFolder As Variant
Dim strTemp As String
Dim olRecipient As Outlook.Recipient

    On Error GoTo ErrorHandler
        
    Set olRecipient = gobjNameSpace.CurrentUser
    
    If frmGetContacts.lstFreqFolders.ListCount = 0 Then
        Set gobjContactFolder = gobjNameSpace.GetDefaultFolder(olFolderContacts)
        Set funOpenFolders = gobjContactFolder
        mstrStoreID = gobjContactFolder.StoreID
        frmGetContacts.lblFolderName.Caption = olRecipient.Name
    Else
          intSelected = frmGetContacts.lstFreqFolders.ListIndex
          If intSelected = -1 Then
             'No name selected as default so just open user's own Contacts folder
             Set gobjContactFolder = gobjNameSpace.GetDefaultFolder(olFolderContacts)
             Set funOpenFolders = gobjContactFolder
             mstrStoreID = gobjContactFolder.StoreID
             frmGetContacts.lblFolderName.Caption = olRecipient.Name
          Else
                strTemp = frmGetContacts.lstFreqFolders.List(intSelected)
                intLength = Len(strTemp)
                varFolder = Right(strTemp, intLength - 1)
                'Check to see if their default folder is their own
                If varFolder = olRecipient.Name Then
                    Set gobjContactFolder = gobjNameSpace.GetDefaultFolder(olFolderContacts)
                    mstrStoreID = gobjContactFolder.StoreID
                    frmGetContacts.lblFolderName.Caption = olRecipient.Name
                Else
                    Set olRecipient = gobjNameSpace.CreateRecipient(varFolder)
                    Set gobjContactFolder = gobjNameSpace.GetSharedDefaultFolder(olRecipient, olFolderContacts)
                    mstrStoreID = gobjContactFolder.StoreID
                    frmGetContacts.lblFolderName.Caption = varFolder
                End If
          End If
    End If

Set funOpenFolders = gobjContactFolder
    
Exit Function

ErrorHandler:
    MsgBox Err.Number & Err.Description, vbCritical, "GetContacts.OpenFolders", "Error in Class Module"

End Function

'===============================================================
'Public Function SetFolder
'This gets called when user reselects another folder to search
'in the lstFreqFolder listbox
'===============================================================
Public Function funSetFolder() As Object

Dim i As Integer
Dim intLength As Integer
Dim intAsterisk As Integer
Dim strFolder As String
Dim strTemp As String
Dim olRecipient As Outlook.Recipient
Dim olOtherRecipient As Outlook.Recipient
Dim strErrCannotAccessFolder

    On Error GoTo ErrorHandler
    
    Set olRecipient = gobjNameSpace.CurrentUser
    
    strTemp = frmGetContacts.lstFreqFolders.List(frmGetContacts.lstFreqFolders.ListIndex)
    intAsterisk = InStr(1, strTemp, "*")
    If intAsterisk > 0 Then
        intLength = Len(strTemp)
        strFolder = Right(strTemp, intLength - 1)
    Else
        strFolder = frmGetContacts.lstFreqFolders.List(frmGetContacts.lstFreqFolders.ListIndex)
    End If
    
    'Check to see if they've selected their own folder
    If strFolder = olRecipient.Name Then
        Set gobjContactFolder = gobjNameSpace.GetDefaultFolder(olFolderContacts)
        mstrStoreID = gobjContactFolder.StoreID
        frmGetContacts.lblFolderName.Caption = strFolder
    Else
        Set olOtherRecipient = gobjNameSpace.CreateRecipient(strFolder)
        If olOtherRecipient.Resolve = True Then
            Set gobjContactFolder = gobjNameSpace.GetSharedDefaultFolder(olOtherRecipient, olFolderContacts)
            mstrStoreID = gobjContactFolder.StoreID
            frmGetContacts.lblFolderName.Caption = strFolder
        Else
            MsgBox "Unable to open " & olOtherRecipient.Name & "'s Contact Folder", vbOKOnly, "Unable to Open Contact Folder"
        End If
    End If
    
    Set funSetFolder = gobjContactFolder

Bye:
Set olRecipient = Nothing
Set olOtherRecipient = Nothing

Exit Function

ErrorHandler:
    Select Case Err.Number
        Case mconErrNoPermissions
             MsgBox "You do not have permission to open selected Contacts Folder." & vbCr & "Ask the Folder owner to give you additional permission to the Folder."
        Case mconErrNoMailbox
            MsgBox "Unable to open the selected Contacts Folder." & vbCr & "The selected name does not use Exchange Server."
        Case Else
            MsgBox "You cannot access selected Contacts Folder.  Folder owner must give you additional permissions.", vbCritical, "Unable to Open Selected Contacts Folder"
    End Select
    'reset to default
    Set gobjContactFolder = gobjNameSpace.GetDefaultFolder(olFolderContacts)
    mstrStoreID = gobjContactFolder.StoreID
    frmGetContacts.lblFolderName.Caption = olRecipient.Name
    
    Set funSetFolder = gobjContactFolder
    
    Resume Bye
    
End Function

