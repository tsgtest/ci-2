VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Contacts"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private Declare Function GetPrivateProfileString Lib "kernel32" Alias _
    "GetPrivateProfileStringA" (ByVal lpApplicationName As String, _
    ByVal lpKeyName As Any, ByVal lpDefault As String, ByVal _
    lpReturnedString As String, ByVal nSize As Long, ByVal lpFileName _
    As String) As Long

'===============================================================
'Class_Initialize
'===============================================================

'*********************************************************************************
'*********NOT USED - RECODE FOR WIN7 WHEN NEEDED**********************************
'*********************************************************************************
Public Function GetIni(xSection As String, _
                       xKey As String, _
                       Optional xINIPath As String) As String
    Dim xValue As String
    Dim xIni As String
    Dim iNullPos As Integer
    
    If xINIPath = "" Then
        xINIPath = App.Path & "ci.ini"
    End If
    
    xIni = xINIPath
    xValue = String(255, " ") & &O0
    
'   get requsestor from ini
    GetPrivateProfileString xSection, _
                            xKey, _
                            xValue, _
                            xValue, _
                            Len(xValue), _
                            xIni
                            
    If Left(xValue, 10) = String(10, " ") Then
        GetIni = ""
        Exit Function
    ElseIf xValue <> "" Then
        iNullPos = InStr(xValue, Chr(0))
    
        If iNullPos Then
            xValue = Left(xValue, iNullPos - 1)
        End If
    End If
    GetIni = xValue
End Function

'===============================================================
'Function Retrieve
'Returns Contact collection
'===============================================================
Function Retrieve() As Collection
    Dim oForm As frmGetContacts
    Dim oContactItem As Outlook.ContactItem
    Dim oItems As Collection
    Dim i As Integer

    On Error GoTo ProcError
    
    'boot outlook session if necessary
    If gobjOutlook Is Nothing Then
        Set gobjOutlook = CreateObject("Outlook.Application")
    End If
    
    If gobjNameSpace Is Nothing Then
        Set gobjNameSpace = gobjOutlook.Session
        
'        Dim xProfile As String
'        xProfile = GetIni("Application", "MAPIProfile", App.Path & "ci.ini")
'        gobjNameSpace.Logon xProfile, , True, False
    End If
    
    'create storage for selected mofo contacts
    Set oItems = New Collection
    
    'prompt user to select contacts
    Set oForm = New frmGetContacts
    
    With frmGetContacts
        .Show vbModal
        
        'cycle through selected contacts,
        'adding to collection
        For i = 1 To .lsvInsertContacts.ListItems.Count
            Set oContactItem = ContactInfo(i)
            If Not oContactItem Is Nothing Then
                oItems.Add oContactItem
            End If
        Next i
    End With
    
    'return collection
    Set Retrieve = oItems
    Exit Function
ProcError:
    Err.Raise Err.Number, "MoFoCI.Contacts.Retrieve", Err.Description
End Function

'===============================================================
'Class_Terminate
'===============================================================

Private Sub Class_Terminate()

    Set gobjOutlook = Nothing
    Set gobjNameSpace = Nothing
    Set gobjContactFolder = Nothing
    Unload frmGetContacts

End Sub

'===============================================================
'Public sub ContactInfo
'Returns Contact item and all properties
'===============================================================
Public Property Get ContactInfo(ContactListIndex) As ContactItem
     With frmGetContacts.lsvInsertContacts.ListItems(ContactListIndex)
        Set ContactInfo = gobjNameSpace.GetItemFromID( _
            .SubItems(gLstEntryID), .SubItems(gLstStoreID))
    End With
    If Err.Number <> 0 Then
        MsgBox "Could Not Retrieve Contact Information for item #" & _
            ContactListIndex & " of the selected entries." & _
            vbCrLf & Err.Description & vbCrLf & _
            "Class: SelectedContacts, Property: Get ContactInfo", vbCritical, _
            "Get Other's Contact Info"
    End If

End Property

'===============================================================
'Public Property Count
'This will return count from ListView Box
'===============================================================
Public Property Get Count()
    Count = frmGetContacts.lsvInsertContacts.ListItems.Count
End Property

