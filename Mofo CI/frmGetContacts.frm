VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmGetContacts 
   Caption         =   "Get Other User's Contact Information"
   ClientHeight    =   7275
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   9255
   Icon            =   "frmGetContacts.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   ScaleHeight     =   363.75
   ScaleMode       =   2  'Point
   ScaleWidth      =   462.75
   StartUpPosition =   2  'CenterScreen
   Begin MSComctlLib.ImageList imlIcons 
      Left            =   5280
      Top             =   6240
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   393216
      BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
         NumListImages   =   1
         BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
            Picture         =   "frmGetContacts.frx":0442
            Key             =   ""
         EndProperty
      EndProperty
   End
   Begin VB.CommandButton cmdCancel 
      Cancel          =   -1  'True
      Caption         =   "&Cancel"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   7440
      TabIndex        =   17
      Top             =   6240
      Width           =   1215
   End
   Begin VB.CommandButton cmdOK 
      Caption         =   "&OK"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   5880
      TabIndex        =   16
      Top             =   6240
      Width           =   1215
   End
   Begin VB.Frame fraInsertContacts 
      Caption         =   "Ins&ert Contact Information For:"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   5775
      Left            =   5400
      TabIndex        =   14
      Top             =   120
      Width           =   3735
      Begin MSComctlLib.ListView lsvInsertContacts 
         Height          =   3855
         Left            =   360
         TabIndex        =   15
         Top             =   960
         Width           =   3015
         _ExtentX        =   5318
         _ExtentY        =   6800
         View            =   3
         LabelEdit       =   1
         MultiSelect     =   -1  'True
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         HideColumnHeaders=   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         NumItems        =   0
      End
      Begin VB.Label lblRemoveNames 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Double-click on name to remove it from list."
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   495
         Left            =   600
         TabIndex        =   18
         Top             =   5040
         Width           =   2655
      End
      Begin VB.Image imgInsertContacts 
         Height          =   480
         Left            =   240
         Picture         =   "frmGetContacts.frx":0894
         Top             =   360
         Width           =   480
      End
   End
   Begin VB.Frame fraSearch 
      Caption         =   "&Search:"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3855
      Left            =   120
      TabIndex        =   5
      Top             =   3360
      Width           =   5055
      Begin MSComctlLib.ListView lsvSearchResults 
         Height          =   1935
         Left            =   360
         TabIndex        =   10
         Top             =   1200
         Width           =   2895
         _ExtentX        =   5106
         _ExtentY        =   3413
         View            =   3
         Arrange         =   1
         LabelEdit       =   1
         MultiSelect     =   -1  'True
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         HideColumnHeaders=   -1  'True
         Checkboxes      =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   0
      End
      Begin VB.CommandButton cmdClear 
         Caption         =   "Clea&r"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   3480
         TabIndex        =   13
         ToolTipText     =   "Press to clear all names from list box."
         Top             =   2520
         Width           =   1335
      End
      Begin VB.CommandButton cmdInsertAll 
         Caption         =   "Insert Al&l"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   3480
         TabIndex        =   12
         ToolTipText     =   "Press to insert all names to Insert Contact Information list."
         Top             =   1920
         Width           =   1335
      End
      Begin VB.CommandButton cmdInsert 
         Caption         =   "&Insert"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   3480
         TabIndex        =   11
         ToolTipText     =   "Press to insert checked names to Insert Contact Information list."
         Top             =   1320
         Width           =   1335
      End
      Begin VB.CommandButton cmdByCategory 
         Caption         =   "By Categor&y"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   3600
         TabIndex        =   9
         ToolTipText     =   "Press to search by Category - enter complete Category name!"
         Top             =   720
         Width           =   1335
      End
      Begin VB.CommandButton cmdByCompany 
         Caption         =   "&By Company"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   2160
         TabIndex        =   8
         ToolTipText     =   "Press to search by Company name."
         Top             =   720
         Width           =   1335
      End
      Begin VB.CommandButton cmdByLastName 
         Caption         =   "By Last &Name"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   720
         TabIndex        =   7
         ToolTipText     =   "Press to search by last name."
         Top             =   720
         Width           =   1335
      End
      Begin VB.TextBox txtSearch 
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   720
         TabIndex        =   6
         ToolTipText     =   "Enter complete or partial text then press one of three  ""By"" buttons below. "
         Top             =   240
         Width           =   4215
      End
      Begin VB.Label lblRemoveInstructions 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H80000005&
         BackStyle       =   0  'Transparent
         BorderStyle     =   1  'Fixed Single
         Caption         =   "Double-click on name to remove it from list."
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   495
         Left            =   480
         TabIndex        =   19
         Top             =   3240
         Width           =   2655
      End
      Begin VB.Image imgABC 
         Height          =   480
         Left            =   120
         Picture         =   "frmGetContacts.frx":0CD6
         Top             =   240
         Width           =   480
      End
   End
   Begin VB.Frame fraFrequentFolders 
      Caption         =   "Frequent Contact &Folders:"
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   3135
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   5055
      Begin VB.CommandButton cmdSetDefault 
         Caption         =   "Set &Default"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   3480
         TabIndex        =   4
         ToolTipText     =   "Select name and then press to set as default searching folder."
         Top             =   2040
         Width           =   1215
      End
      Begin VB.CommandButton cmdRemoveFolders 
         Caption         =   ">> &Remove"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   3480
         TabIndex        =   3
         ToolTipText     =   "Select name and then press to remove from frequent folder list."
         Top             =   1560
         Width           =   1215
      End
      Begin VB.CommandButton cmdAddFolders 
         Caption         =   "<< &Add"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   3480
         TabIndex        =   2
         ToolTipText     =   "Press to add names to frequent folder list."
         Top             =   1080
         Width           =   1215
      End
      Begin VB.ListBox lstFreqFolders 
         Height          =   1425
         Left            =   120
         TabIndex        =   1
         ToolTipText     =   "Double-click to switch searching folder."
         Top             =   1080
         Width           =   3135
      End
      Begin VB.Label lblSwitchFolders 
         Alignment       =   2  'Center
         Caption         =   "Double-click on name to search that person's Contact folder."
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   495
         Left            =   240
         TabIndex        =   22
         Top             =   2520
         Width           =   2895
      End
      Begin VB.Label lblFolderName 
         BackStyle       =   0  'Transparent
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   600
         TabIndex        =   20
         Top             =   600
         Width           =   3015
      End
      Begin VB.Label lblSearching 
         Caption         =   "Current Folder Searching:"
         BeginProperty Font 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   600
         TabIndex        =   21
         Top             =   360
         Width           =   2655
      End
      Begin VB.Image imgFolders 
         Height          =   480
         Left            =   120
         Picture         =   "frmGetContacts.frx":1120
         Top             =   240
         Width           =   480
      End
   End
End
Attribute VB_Name = "frmGetContacts"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private mIniLocation As String
Private Const MAPI_User_Cancel = -2147221229

'===============================================================
'cmdAddFolders_Click
'===============================================================
Private Sub cmdAddFolders_Click()
    
    Dim objNameSpace As Outlook.NameSpace
    Dim objOutlook As Outlook.Application
    Dim oDiag As Outlook.SelectNamesDialog
    Dim bRet As Boolean

'    Dim objSession As Mapi.Session
'    Dim objNewMsg As Mapi.Message
    Dim objRecipients As Outlook.Recipients
    Dim i As Integer
    Dim intAsterisk As Integer
    Dim strTemp As String
    Dim strDefaultFolder As String
    Dim objMailItem As Outlook.MailItem
    
    On Error GoTo ErrorHandler
    
    'Set Outlook references
    Set objOutlook = CreateObject("Outlook.Application")
'    Set objNameSpace = objOutlook.GetNamespace("MAPI")

    
'    'Use Collaboration Data Object (CDO) to access Address Book
'    'First, create a MAPI session and logon
'    Set objSession = CreateObject("MAPI.Session")
'    objSession.Logon ShowDialog:=False, newsession:=False, nomail:=True'   get an outlook session object
    
'    Set objNameSpace = objOutlook.Session

    'Create a temporary message to hold the address book results.  Since we never save or send
    'the message, it never shows up in the inbox
    
    
'    Set objMailItem = objOutlook.CreateItem(olMailItem)
    
'     Set objNewMsg = objNameSpace.Inbox.Messages.Add

    'Populate recipients collection in the temporary message using names
    'in the FreqFolder listbox
'     Set objRecipients = objNewMsg.Recipients

    Set oDiag = objOutlook.Session.GetSelectNamesDialog

    With oDiag
        .ShowOnlyInitialAddressList = True
        .NumberOfRecipientSelectors = olShowTo
        .ToLabel = "Add"
        .AllowMultipleSelection = True
        bRet = .Display
        If bRet Then
            'Repopulate ListBox
            If .Recipients.Count = 0 Then
                GoTo Bye
            Else
               For i = 1 To .Recipients.Count
                   lstFreqFolders.AddItem .Recipients.Item(i).Name
               Next i
             End If
        Else
            Exit Sub
        End If
    End With

'     For i = 0 To lstFreqFolders.ListCount - 1
'        'Need to check for asterisk next to default folder name
'        strTemp = lstFreqFolders.List(i)
'        intAsterisk = InStr(1, strTemp, "*")
'        If intAsterisk > 0 Then
'            strTemp = Right(lstFreqFolders.List(i), Len(lstFreqFolders.List(i)) - 1)
'            objMailItem.Recipients.Add strTemp
'            objRecipients(i + 1).Resolve
'            strDefaultFolder = strTemp
'        Else
'            objMailItem.Recipients.Add lstFreqFolders.List(i)
'            objRecipients(i + 1).Resolve
'        End If
'     Next
     
    'Display the address book and then pass the recipient collection which
    'will also store returned names
    
'    On Error Resume Next
'
'    If Err <> 0 Then
'        GoTo ErrorHandler
'    End If
           
'     'Clear list box
'     lstFreqFolders.Clear
     
    
'Log off session
'objNameSpace.Logoff

Bye:
Set objMailItem = Nothing
Set objRecipients = Nothing
Set objNameSpace = Nothing
Set objOutlook = Nothing
    
Exit Sub

ErrorHandler:
    MsgBox "An error occurred in frmGetContacts.cmdAddFolders_Click:" & vbCr & vbCr & "(" & Err.Number & ") " & Err.Description, vbInformation, "Outlook Contacts Error"
 Resume Bye
 
End Sub

'===============================================================
'cmdByCategory_Click
'===============================================================

Private Sub cmdByCategory_Click()

    'Call subroutine to do search; pass in field name to search by
    subSearch "Categories"

End Sub

'===============================================================
'cmdCompany_Click
'===============================================================

Private Sub cmdByCompany_Click()

    'Call subroutine to do search; pass in field name to search by
    subSearch "Company"

End Sub

'===============================================================
'cmdByLastName_Click
'===============================================================
Private Sub cmdByLastName_Click()

    'Call subroutine to do search; pass in field name to search by
     subSearch "LastName"
    
End Sub

'===============================================================
'cmdCancel_Click
'===============================================================
Private Sub cmdCancel_Click()
    
    'Update folder changes
     subUpdateFolders
     frmGetContacts.Hide
     lsvInsertContacts.ListItems.Clear

End Sub

'===============================================================
'cmdClear_Click
'Remove all items in the Search Results List Box
'===============================================================
Private Sub cmdClear_Click()

If lsvSearchResults.ListItems.Count = 0 Then
    MsgBox "There are no items listed to clear.", vbOKOnly, "No Items To Clear"
    txtSearch.SetFocus
    Exit Sub
Else
    lsvSearchResults.ListItems.Clear
End If

End Sub

'===============================================================
'cmdInsert_Click
'Copy all checked items to the Insert Contact Info List Box
'===============================================================
Private Sub cmdInsert_Click()

Dim i As Integer
Dim X As Integer
Dim j As Integer
Dim boolDuplicate As Boolean
Dim SearchItem As ListItem
Dim SelectedItem As ListItem
Dim boolSelectedItems As Boolean 'True if items are selected in list
    
On Error GoTo ErrorHandler


If lsvSearchResults.ListItems.Count = 0 Then
    MsgBox "There are no items listed.  Search by Last Name, Company or Category first.", vbOKOnly, "No Items Listed"
    txtSearch.SetFocus
    Exit Sub
End If
    

For i = 1 To lsvSearchResults.ListItems.Count
    If lsvSearchResults.ListItems(i).Checked Then
    boolSelectedItems = True
    Set SearchItem = lsvSearchResults.ListItems(i)
    boolDuplicate = False
       If lsvInsertContacts.ListItems.Count > 0 Then
           For X = 1 To lsvInsertContacts.ListItems.Count
               If SearchItem.Text = lsvInsertContacts.ListItems(X).Text Then
                    MsgBox SearchItem.Text & " has already been inserted.", vbOKOnly, "Duplicate Entry"
                    boolDuplicate = True
                    Exit For
                End If
            Next X
                If Not boolDuplicate Then
                    Set SelectedItem = lsvInsertContacts.ListItems.Add
                    SelectedItem.SmallIcon = 1
                    SelectedItem.Text = SearchItem.Text
                    For j = 1 To 2 '2 subitems to add
                        SelectedItem.SubItems(j) = SearchItem.SubItems(j)
                    Next j
                End If
        Else
                    Set SelectedItem = lsvInsertContacts.ListItems.Add
                    SelectedItem.SmallIcon = 1
                    SelectedItem.Text = SearchItem.Text
                    For j = 1 To 2 '2 subitems to add
                        SelectedItem.SubItems(j) = SearchItem.SubItems(j)
                    Next j
        End If
    End If
Next i
    
If Not boolSelectedItems Then
    MsgBox "Select a name to insert and then press the Insert button.", vbOKOnly, "No Name Selected"
    GoTo Bye:
End If

'Go back and uncheck the items
For i = 1 To lsvSearchResults.ListItems.Count
    If lsvSearchResults.ListItems(i).Checked = True Then
       lsvSearchResults.ListItems(i).Checked = False
    End If
Next i

Bye:
Set SearchItem = Nothing
Set SelectedItem = Nothing
Exit Sub

ErrorHandler:
    MsgBox "An error occurred in frmGetContacts.cmdInsert_Click:" & vbCr & vbCr & "(" & Err.Number & ") " & Err.Description, vbInformation, "Outlook Contacts Error"
 Resume Bye

    
End Sub

'===============================================================
'cmdInsertAll_Click
'Copies all items to the Insert Contact Info List Box
'===============================================================

Private Sub cmdInsertAll_Click()
Dim i As Integer
Dim X As Integer
Dim j As Integer
Dim boolDuplicate As Boolean
Dim SearchItem As ListItem
Dim SelectedItem As ListItem
    
On Error GoTo ErrorHandler

If lsvSearchResults.ListItems.Count = 0 Then
    MsgBox "There are no items listed.  Search by Last Name, Company or Category first.", vbOKOnly, "No Items Listed"
    txtSearch.SetFocus
    Exit Sub
End If
    

For i = 1 To lsvSearchResults.ListItems.Count
    Set SearchItem = lsvSearchResults.ListItems(i)
    boolDuplicate = False
       If lsvInsertContacts.ListItems.Count > 0 Then
           For X = 1 To lsvInsertContacts.ListItems.Count
               If SearchItem.Text = lsvInsertContacts.ListItems(X).Text Then
                    MsgBox SearchItem.Text & " has already been inserted.", vbOKOnly, "Duplicate Entry"
                    boolDuplicate = True
                    Exit For
                End If
            Next X
                If Not boolDuplicate Then
                    Set SelectedItem = lsvInsertContacts.ListItems.Add
                    SelectedItem.SmallIcon = 1
                    SelectedItem.Text = SearchItem.Text
                    For j = 1 To 2 '2 subitems to add
                        SelectedItem.SubItems(j) = SearchItem.SubItems(j)
                    Next j
                End If
        Else
                    Set SelectedItem = lsvInsertContacts.ListItems.Add
                    SelectedItem.SmallIcon = 1
                    SelectedItem.Text = SearchItem.Text
                    For j = 1 To 2 '2 subitems to add
                        SelectedItem.SubItems(j) = SearchItem.SubItems(j)
                    Next j
        End If
Next i
    
'Go back and uncheck the items
For i = 1 To lsvSearchResults.ListItems.Count
    If lsvSearchResults.ListItems(i).Checked = True Then
       lsvSearchResults.ListItems(i).Checked = False
    End If
Next i

Bye:
Set SearchItem = Nothing
Set SelectedItem = Nothing
Exit Sub

ErrorHandler:
    MsgBox "An error occurred in frmGetContacts.cmdInsertAll_Click:" & vbCr & vbCr & "(" & Err.Number & ") " & Err.Description, vbInformation, "Outlook Contacts Error"
 Resume Bye

End Sub

'===============================================================
'cmdOK_Click
'===============================================================
Private Sub cmdOK_Click()

Dim intResponse As Integer

 If lsvInsertContacts.ListItems.Count = 0 Then
    intResponse = MsgBox("There are no Contact Names listed to insert.  Do you wish to Cancel?", vbYesNo, "No Contact Names Listed")
    If intResponse = "6" Then
        cmdCancel_Click
        Exit Sub
    Else
        txtSearch.SetFocus
    End If
 Else
    'Update folder changes
    subUpdateFolders
    frmGetContacts.Hide
 End If

End Sub

'===============================================================
'cmdRemoveFolders_Click
'===============================================================
Private Sub cmdRemoveFolders_Click()

    If lstFreqFolders.ListIndex <> -1 Then
        lstFreqFolders.RemoveItem (lstFreqFolders.ListIndex)
    End If

End Sub

'===============================================================
'cmdSetDefault_Click
'===============================================================
Private Sub cmdSetDefault_Click()
Dim intSetDefault As Integer
Dim i As Integer
Dim intAsterisk As Integer
Dim strOldDefault As String

    intSetDefault = lstFreqFolders.ListIndex

    For i = 0 To lstFreqFolders.ListCount - 1
        strOldDefault = lstFreqFolders.List(i)
        intAsterisk = InStr(1, strOldDefault, "*")
        If intAsterisk > 0 Then
            strOldDefault = Right(lstFreqFolders.List(i), Len(lstFreqFolders.List(i)) - 1)
            lstFreqFolders.List(i) = strOldDefault
            Exit For
        End If
    Next

    lstFreqFolders.List(intSetDefault) = "*" & lstFreqFolders.List(intSetDefault)
    Set gobjContactFolder = funSetFolder

    'remove default is necessary
    If UCase(Me.lblFolderName.Caption) = UCase(gobjNameSpace.CurrentUser.Name) Then
        'remove default marker
        For i = 0 To lstFreqFolders.ListCount - 1
            strOldDefault = lstFreqFolders.List(i)
            intAsterisk = InStr(1, strOldDefault, "*")
            If intAsterisk > 0 Then
                strOldDefault = Right(lstFreqFolders.List(i), Len(lstFreqFolders.List(i)) - 1)
                lstFreqFolders.List(i) = strOldDefault
                Exit For
            End If
        Next
    End If
    
End Sub

'===============================================================
'Form_Load
'===============================================================
Private Sub Form_Load()
        
Dim strCreated As String
Dim strDefaultFolder As String
Dim strFrequentFolders As String
Dim strTemp As String
Dim strNewString As String
Dim intSemiColon As Integer
Dim intLength As Integer

On Error GoTo ErrorHandler

'Set up listview boxes
lsvSearchResults.ColumnHeaders.Add 1, , , 2890
lsvSearchResults.ColumnHeaders.Add 2, , "EntryID", 0
lsvSearchResults.ColumnHeaders.Add 3, , "StoreID", 0
lsvInsertContacts.SmallIcons = frmGetContacts.imlIcons
lsvInsertContacts.ColumnHeaders.Add 1, , , 3000
lsvInsertContacts.ColumnHeaders.Add 2, , "EntryID", 0
lsvInsertContacts.ColumnHeaders.Add 3, , "StoreID", 0
 
'User's folder is used as Default Folder unless otherwise specified
strDefaultFolder = funDefaultFolder
If strDefaultFolder <> "" Then
    lblFolderName.Caption = strDefaultFolder
    lstFreqFolders.AddItem "*" & strDefaultFolder
    lstFreqFolders.ListIndex = lstFreqFolders.ListCount - 1
End If

'Load Frequent Folders names
strFrequentFolders = Trim(funFrequentFolderList)
If strFrequentFolders <> "" Then
    strTemp = Right(strFrequentFolders, 1) 'check to make sure there's no semicolon at end
    If strTemp = ";" Then
        strTemp = Left(strFrequentFolders, Len(strFrequentFolders) - 1)
        strFrequentFolders = strTemp
    End If
DoAgain:
    intSemiColon = InStr(1, strFrequentFolders, ";", vbTextCompare)
    intLength = Len(strFrequentFolders)
    If intSemiColon > 1 Then
        strTemp = Left(strFrequentFolders, intSemiColon - 1)
        strNewString = Right(strFrequentFolders, intLength - intSemiColon)
        If strTemp = strDefaultFolder Then 'make sure their name isn't added twice
            strFrequentFolders = strNewString
            GoTo DoAgain
        Else
            If strTemp <> "" Then
                lstFreqFolders.AddItem strTemp
                strFrequentFolders = strNewString
                GoTo DoAgain
            End If
        End If
    Else
        If strTemp = strDefaultFolder Then
            'do nothing
        Else
            If strTemp <> "" Then
                lstFreqFolders.AddItem strFrequentFolders
            End If
        End If
    End If
End If

'Open Contact Folders
Set gobjContactFolder = funOpenFolders

Exit Sub

ErrorHandler:
    MsgBox "An error occurred in frmGetContacts.Form_Initialize:" & vbCr & vbCr & "(" & Err.Number & ") " & Err.Description, vbInformation, "Outlook Contacts Error"
End Sub

'===============================================================
'Function SearchTo
'===============================================================
Private Function SearchTo(SearchFrom) As String
    
    'This takes the search criteria and determines what to search through.  For example,
    'to search for all names beginning with "Jor", we would actually search from "Jor" to "Jos"
    
    Dim LastLetter As String
    
    LastLetter = ChrW(AscW(Right(SearchFrom, 1)) + 1)
    SearchTo = Left(SearchFrom, Len(SearchFrom) - 1) & LastLetter

End Function

'===============================================================
'subUpdateFolders
'Updates any folder settings user may have changed, such as default Contacts
'folder or Frequent Folder List names
'===============================================================

Private Sub subUpdateFolders()

Dim i As Integer
Dim intCount As Integer
Dim strAsterisk As String
Dim strFrequentFolder As String
Dim strDefaultFolder As String
Dim strTemp As String

On Error GoTo ErrorHandler

strFrequentFolder = ""

'Update registry values with current Frequent Folder list info
intCount = lstFreqFolders.ListCount - 1
For i = 0 To intCount
    strTemp = lstFreqFolders.List(i)
    strAsterisk = Left(strTemp, 1)
    If strAsterisk = "*" Then
        strDefaultFolder = Right(strTemp, Len(strTemp) - 1)
        lstFreqFolders.RemoveItem (i)
        Exit For
    End If
Next i
    
intCount = lstFreqFolders.ListCount - 1
For i = 0 To intCount
    If strFrequentFolder = "" Then
        strFrequentFolder = lstFreqFolders.List(i)
    Else
        strFrequentFolder = strFrequentFolder & ";" & lstFreqFolders.List(i)
    End If
 Next i

'Now write values to registry
 subSetDefaultFolder (strDefaultFolder)
 subSetFrequentFolderList (strFrequentFolder)
 
Exit Sub

ErrorHandler:
       MsgBox "An error occurred in frmContacts.subUpdateFolders:" & vbCr & vbCr & "(" & Err.Number & ") " & Err.Description, vbInformation

End Sub

'===============================================================
'subSearch
'Searches Contact Folder based on field name passed in
'===============================================================

Private Sub subSearch(ByVal strFieldName As String)

Dim strSearch As String
Dim SearchItem As ContactItem
Dim strTextSearch As String
Dim itmList As ListItem
Dim objFolder As Object
Dim strFolderID As String
Dim objOutlook As Outlook.Application
Dim objNameSpace As Outlook.NameSpace

    On Error GoTo ErrorHandler

  'boot outlook session if necessary
    If objOutlook Is Nothing Then
        Set objOutlook = CreateObject("Outlook.Application")
    End If
    
    Set objFolder = objOutlook.Session.GetDefaultFolder(olFolderContacts)
    strFolderID = objFolder.EntryID
    
    DoEvents
    
    If strFieldName = "LastName" Then
        strTextSearch = "Last Name"
    ElseIf strFieldName = "Company" Then
        strTextSearch = "Company"
    Else
        strTextSearch = "Category"
    End If
    DoEvents

    If txtSearch = "" Then
       MsgBox "No " & strTextSearch & " entered.  Please enter " & strTextSearch & " and press the By " & strTextSearch & " button.", vbOKOnly, "No " & strTextSearch & " Entered"
       txtSearch.SetFocus
       Exit Sub
    Else
        strSearch = txtSearch
    End If

 Set gobjContactItems = gobjContactFolder.Items
 gobjContactItems.Sort "[FileAs]"
    DoEvents
 
If strFieldName = "Categories" Then
    Set SearchItem = gobjContactItems.Find("[Categories] = """ & strSearch & """")
Else
        If Len(strSearch) > 1 And (Right(strSearch, 1) = "z" Or Right(strSearch, 1) = "Z") Then
            strSearch = Left(strSearch, Len(strSearch) - 1)
        End If
                If Right(strSearch, 1) = "z" Or Right(strSearch, 1) = "Z" Then
                    strSearch = "([" & strFieldName & "]>=""" & strSearch & """)"
                Else
                    strSearch = "([" & strFieldName & "]>=""" & strSearch & """and [" & strFieldName & "]<""" & SearchTo(strSearch) & """)"
                End If
        Set SearchItem = gobjContactItems.Find(strSearch)
    End If
    DoEvents

  If SearchItem Is Nothing Then
        If strTextSearch = "Category" Then
            MsgBox "Search item not found.  Make sure to enter COMPLETE category name.", vbOKOnly, "Search Item Not Found"
            txtSearch.SetFocus
        Else
            MsgBox "Search item not found or Contact may be marked as Private.  Please re-enter " & strTextSearch & " to try search again.", vbOKOnly, "Search Item Not Found"
            txtSearch.SetFocus
        End If
        Exit Sub
  Else
          DoEvents


      Do Until SearchItem Is Nothing
           If SearchItem.Sensitivity = 2 Then 'contact marked as private
              If strFolderID = gobjContactFolder.EntryID Then 'only list if search folder is same as logged on user
                    Set itmList = lsvSearchResults.ListItems.Add
                    itmList.Text = SearchItem
                    itmList.SubItems(gLstEntryID) = SearchItem.EntryID
                    itmList.SubItems(gLstStoreID) = mstrStoreID
                    Set SearchItem = gobjContactItems.FindNext
                    DoEvents
              Else
                     MsgBox "Contact marked as Private and cannot be displayed.", vbOKOnly, "Search Item Cannot Be Displayed"
                     GoTo Skip
               End If
           Else
              Set itmList = lsvSearchResults.ListItems.Add
              itmList.Text = SearchItem
              itmList.SubItems(gLstEntryID) = SearchItem.EntryID
              itmList.SubItems(gLstStoreID) = mstrStoreID
Skip:
              Set SearchItem = gobjContactItems.FindNext
              DoEvents
           End If
    DoEvents
      Loop
   End If

Bye:
  Set SearchItem = Nothing
  Set objNameSpace = Nothing
  Set objOutlook = Nothing
  txtSearch.Text = ""
  
Exit Sub

ErrorHandler:
    MsgBox Err.Number & ": " & Err.Description & " in frmGetContacts_subSearch", vbOKOnly, "Template Error"
    Resume Bye

End Sub

'===============================================================
'UserForm_QueryClose
'===============================================================
Private Sub UserForm_QueryClose(Cancel As Integer, CloseMode As Integer)

    If CloseMode = 0 Then
       frmGetContacts.Hide
    End If
End Sub

'===============================================================
'funFrequentFolderList
'Returns Frequent Folder List entries from registry key value
'===============================================================

Private Function funFrequentFolderList() As String
 
    On Error GoTo ErrorHandler
    
    funFrequentFolderList = GetValue("Outlook Info", "FreqFolderList", mIniLocation, 1024)

Exit Function

ErrorHandler:
       MsgBox "An error occurred in frmGetContacts.funFrequentFolderList:" & vbCr & vbCr & "(" & Err.Number & ") " & Err.Description, vbInformation

End Function

'===============================================================
'funDefaultFolder
'Returns Default Contacts Folder from registry key value
'===============================================================

Private Function funDefaultFolder() As String
 
    On Error GoTo ErrorHandler
    
    funDefaultFolder = GetValue("Outlook Info", "DefaultFolder")

Exit Function

ErrorHandler:
       MsgBox "An error occurred in frmGetContacts.funDefaultFolder:" & vbCr & vbCr & "(" & Err.Number & ") " & Err.Description, vbInformation

End Function

'===============================================================
'subSetDefaultFolder
'Sets Default Contacts Folder registry key value
'===============================================================

Private Sub subSetDefaultFolder(strDefaultFolder)
 
    Dim Ret As Long

    On Error GoTo ErrorHandler
    
    Ret = SetValue("Outlook Info", "DefaultFolder", strDefaultFolder)

Exit Sub

ErrorHandler:
       MsgBox "An error occurred in frmGetContacts.subSetDefaultFolder:" & vbCr & vbCr & "(" & Err.Number & ") " & Err.Description, vbInformation

End Sub

'===============================================================
'subSetFrequentFolderList
'Sets Frequent Folder List entries in registry key value
'===============================================================

Public Sub subSetFrequentFolderList(strFreqFolderList)
 
    Dim Ret As Long
    
    On Error GoTo ErrorHandler
 
     Ret = SetValue("Outlook Info", "FreqFolderList", strFreqFolderList)

Exit Sub

ErrorHandler:
       MsgBox "An error occurred in frmGetContacts.funFrequentFolderList:" & vbCr & vbCr & "(" & Err.Number & ") " & Err.Description, vbInformation

End Sub

'===============================================================
'lstFreqFolders_DblClick
'Sets folder as new searching folder
'===============================================================
Private Sub lstFreqFolders_DblClick()

    Dim i As Integer

    For i = 0 To lstFreqFolders.ListCount - 1
        If lstFreqFolders.Selected(i) = True Then
            Set gobjContactFolder = funSetFolder
            Exit For
        End If
    Next i

End Sub

'===============================================================
'lsvInsertContacts_DblClick
'Removes item from list view control when users double clicks
'===============================================================

Private Sub lsvInsertContacts_DblClick()

    Dim RemoveItem As ListItem
    
    Set RemoveItem = lsvInsertContacts.SelectedItem
    
    On Error GoTo ErrorHandler
    
    If RemoveItem Is Nothing Then
        MsgBox "To remove an item, select it and then double click.", vbExclamation & vbOKOnly, "No Item Selected"
    Else
        lsvInsertContacts.ListItems.Remove (lsvInsertContacts.SelectedItem.Index)
    End If

Bye:
Set RemoveItem = Nothing
Exit Sub

ErrorHandler:
       MsgBox "An error occurred in frmGetContacts.lsvInsertContacts_DblClick:" & vbCr & vbCr & "(" & Err.Number & ") " & Err.Description, vbInformation
       Resume Bye

End Sub

'===============================================================
'lsvSearchResults_DblClick
'Removes item from list view control when users double clicks
'===============================================================

Private Sub lsvSearchResults_DblClick()

Dim RemoveItem As ListItem

Set RemoveItem = lsvSearchResults.SelectedItem

On Error GoTo ErrorHandler

If RemoveItem Is Nothing Then
    MsgBox "To remove an item, select it and then double click.", vbExclamation & vbOKOnly, "No Item Selected"
Else
    lsvSearchResults.ListItems.Remove (lsvSearchResults.SelectedItem.Index)
End If

Bye:
Set RemoveItem = Nothing
Exit Sub

ErrorHandler:
       MsgBox "An error occurred in frmGetContacts.lsvSearchResults_DblClick:" & vbCr & vbCr & "(" & Err.Number & ") " & Err.Description, vbInformation
       Resume Bye

End Sub
