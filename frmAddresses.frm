VERSION 5.00
Begin VB.Form frmAddresses 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "##"
   ClientHeight    =   3045
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6540
   ControlBox      =   0   'False
   FillColor       =   &H00C0FFFF&
   FillStyle       =   0  'Solid
   BeginProperty Font 
      Name            =   "Tahoma"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3045
   ScaleWidth      =   6540
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.ListBox lstAddressTypes 
      BackColor       =   &H00FFFFFF&
      Height          =   1815
      Left            =   135
      TabIndex        =   1
      Top             =   645
      Width           =   2040
   End
   Begin VB.CommandButton btnOK 
      Caption         =   "O&K"
      Default         =   -1  'True
      Height          =   400
      Left            =   4200
      TabIndex        =   3
      Top             =   2535
      Width           =   1100
   End
   Begin VB.CommandButton btnCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   400
      Left            =   5370
      TabIndex        =   4
      Top             =   2535
      Width           =   1100
   End
   Begin VB.CheckBox chkPromptForMissingAddresses 
      Caption         =   "&Continue to prompt for addresses when necessary"
      Height          =   390
      Left            =   165
      TabIndex        =   2
      Top             =   2535
      Value           =   1  'Checked
      Visible         =   0   'False
      Width           =   3945
   End
   Begin VB.Label lblAddresses 
      Caption         =   "The requested address for this contact does not exist.  Please select from one of the available addresses listed below."
      Height          =   405
      Left            =   135
      TabIndex        =   0
      Top             =   150
      Width           =   6150
   End
   Begin VB.Label lblAddressType 
      BackStyle       =   0  'Transparent
      Caption         =   "Address T&ype:"
      Height          =   285
      Left            =   1275
      TabIndex        =   6
      Top             =   3330
      Visible         =   0   'False
      Width           =   1125
   End
   Begin VB.Label lblDetail 
      BackStyle       =   0  'Transparent
      Height          =   1515
      Left            =   2550
      TabIndex        =   5
      Top             =   750
      Width           =   3690
   End
End
Attribute VB_Name = "frmAddresses"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private m_oAddresses As CIO.CAddresses
Private m_bCancelled As Boolean
Private m_bPromptForMissingPhones As Boolean
Private m_bPromptForMultiplePhones As Boolean
Private m_bPromptForMultiple As Boolean
 
'**********************************************************
'   Properties
'**********************************************************
Public Property Let Cancelled(bNew As Boolean)
    m_bCancelled = bNew
End Property

Public Property Get Cancelled() As Boolean
    Cancelled = m_bCancelled
End Property

Public Property Set Addresses(oNew As CIO.CAddresses)
    Set m_oAddresses = oNew
End Property

'**********************************************************
'   Methods
'**********************************************************
Public Sub SetMessage(xContactName As String, xAddrType As String)
'    Me.Caption = "Choose an Address For " & xContactName
'    If Me.PromptForMultiple Then
'        Me.lblAddresses.Caption = "&Multiple addresses of this type " & _
'               "exist for " & xSubstitute(xContactName, "&", "&&") & _
'               ".  Please select one from the following available addresses:"
'    Else
'        Me.lblAddresses.Caption = "&No address of this type " & _
'               "exists for " & xSubstitute(xContactName, "&", "&&") & _
'               ".  Please select one from the following available addresses:"
'    End If
End Sub

'**********************************************************
'   Event Procedures
'**********************************************************
Private Sub btnCancel_Click()
    Me.Cancelled = True
    Me.Hide
    DoEvents
End Sub

Private Sub btnOK_Click()
    Me.Hide
    DoEvents
End Sub

Private Sub Form_Activate()
    If Me.lstAddressTypes.ListCount > 1 Then
        Me.lstAddressTypes.SetFocus
    End If
End Sub

Private Sub lstAddressTypes_Click()
    ShowListingDetail
End Sub

Private Sub Form_Load()
'   turn off prompt for existing addresses and phones-
'   will get reset in btnOK_Click, btnCancel_Click
    Me.Cancelled = False
    
    Dim oAddress As CIO.CAddress
    
    For Each oAddress In m_oAddresses
        Me.lstAddressTypes.AddItem oAddress.Name
    Next oAddress
    
    Me.lstAddressTypes.ListIndex = -1
    
    On Error Resume Next
    Me.lstAddressTypes.ListIndex = 0
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    Me.Hide
End Sub

Private Sub lstAddressTypes_DblClick()
'do what happens when OK is clicked
    btnOK_Click
End Sub

'**********************************************************
'   Internal Functions
'**********************************************************
Private Sub ShowListingDetail()
'displays detail form with detail for
'specified listing with specified address

    Dim oAddress As CIO.CAddress

'   get address type from dlg
    On Error GoTo ProcError
    With Me.lstAddressTypes
        If .ListIndex > -1 Then
            Set oAddress = m_oAddresses.Item(.ListIndex + 1)
            Dim oContact As CIO.CContact
            'set ocontact = oaddress.
        End If
    End With

'   set detail in form
    Me.lblDetail.Caption = Me.Listing.Detail(ciDetailFormat_Address)
    DoEvents

'   enable OK if there is detail
    Me.btnOK.Enabled = (Me.lblDetail.Caption <> "")
    Exit Sub
ProcError:
    RaiseError "CIO.frmAddresses.ShowListingDetail"
    Exit Sub
End Sub

