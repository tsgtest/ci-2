VERSION 5.00
Begin VB.Form frmContacts 
   BackColor       =   &H00FFFFFF&
   BorderStyle     =   0  'None
   ClientHeight    =   4500
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   4680
   ControlBox      =   0   'False
   Icon            =   "frmContacts.frx":0000
   LinkMode        =   1  'Source
   LinkTopic       =   "Application"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   4500
   ScaleWidth      =   4680
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox txtContacts 
      BorderStyle     =   0  'None
      Height          =   4395
      Left            =   75
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   0
      Top             =   -15
      Width           =   4575
   End
End
Attribute VB_Name = "frmContacts"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private m_appCI As CI.CApplication

Sub RetrieveContacts(IncludePhone As Boolean, IncludeFax As Boolean, IncludeEMail As Boolean)
    Dim m_Contacts As mpCI.Contacts
    Dim i As Integer
    Dim xText As String
    
    If m_appCI Is Nothing Then
        Set m_appCI = New mpCI.Application
    End If
    
'   test for correct startup
    With m_appCI
        If .Initialized Then
            Set m_Contacts = .Contacts
            With .Options
                .OnEmptyAddresses = ciEmptyAddress_ReturnEmpty
                .PromptForMissingPhones = True
                .IncludeCustomFields = False
            End With
            .Contacts.Retrieve ciSelectionList_To, _
                               IncludePhone, _
                               IncludeFax, _
                               IncludeEMail
            
            OutputContacts
            Set m_Contacts = Nothing
        Else
            Set m_appCI = Nothing
        End If
    End With
End Sub

Sub RetrieveContactsOLD(IncludePhone As Boolean, IncludeFax As Boolean, IncludeEMail As Boolean)
    Dim m_Contacts As mpCI.Contacts
    Dim i As Integer
    Dim xText As String
    
    If m_appCI Is Nothing Then
        Set m_appCI = New mpCI.Application
    End If
    
'   test for correct startup
    With m_appCI
        If .Initialized Then
            Set m_Contacts = .Contacts
            With .Options
                .OnEmptyAddresses = _
                    ciEmptyAddress_PromptForExisting
                .PromptForMissingPhones = True
                .IncludeCustomFields = True
            End With
            .Contacts.Retrieve ciSelectionList_To, _
                               IncludeFax, _
                               IncludePhone, _
                               IncludeEMail
            
            OutputContacts
            Set m_Contacts = Nothing
        Else
            Set m_appCI = Nothing
        End If
    End With
End Sub

Public Sub CreateDataFile(IncludePhone As Boolean, IncludeFax As Boolean, IncludeEMail As Boolean)
    Dim m_Contacts As mpCI.Contacts
    Dim i As Integer
    Dim xText As String
    
    If m_appCI Is Nothing Then
        Set m_appCI = New mpCI.Application
    End If
    
'   test for correct startup
    With m_appCI
        If .Initialized Then
            Set m_Contacts = .Contacts
            With .Options
                .OnEmptyAddresses = _
                    ciEmptyAddress_PromptForExisting
                .PromptForMissingPhones = True
                .ReloadDlgOnRetrieve = False
                .IncludeCustomFields = True
            End With
            .Contacts.Retrieve ciSelectionList_To, _
                               IncludeFax, _
                               IncludePhone, _
                               IncludeEMail
            .Contacts.CreateDataFile App.Path & "\Contacts.dat", "|"
            OutputContacts
            Set m_Contacts = Nothing
        Else
            Set m_appCI = Nothing
        End If
    End With
End Sub

Public Sub Initialize()
'        Set m_appCI = New mpCI.Application
End Sub

Sub OutputContacts()
    Dim i As Integer
    Dim cttP As mpCI.Contact
    Dim xC As String
    For i = 1 To m_appCI.Contacts.Count
        Set cttP = m_appCI.Contacts(i)
        With cttP
            xC = xC & .AddressType & vbCrLf
            xC = xC & .ContactType & vbCrLf
            xC = xC & .DisplayName & vbCrLf
            xC = xC & .Prefix & vbCrLf
            xC = xC & .FirstName & vbCrLf
            xC = xC & .MiddleName & vbCrLf
            xC = xC & .LastName & vbCrLf
            xC = xC & .Suffix & vbCrLf
            xC = xC & .Initials & vbCrLf
            xC = xC & .Street1 & vbLf
            xC = xC & .Street2 & vbLf
            xC = xC & .Street3 & vbCrLf
            xC = xC & .City & vbCrLf
            xC = xC & .State & vbCrLf
            xC = xC & .ZipCode & vbCrLf
            xC = xC & .Country & vbCrLf
            xC = xC & .Company & vbCrLf
            xC = xC & .Department & vbCrLf
            xC = xC & .Title & vbCrLf
            xC = xC & .EMailAddress & vbCrLf
            xC = xC & .Phone & vbCrLf
            xC = xC & .PhoneExtension & vbCrLf
            xC = xC & .Fax & vbCrLf
            
            Dim j As Integer
            On Error Resume Next
            For j = 1 To .CustomFields.Count
                xC = xC & .CustomFields.Item(j).Value & vbCrLf
            Next j

'            xC = xC & .Salutation & vbCrLf
            xC = xC & "||"
        End With
    Next i
    frmContacts.txtContacts = xC
    frmApp.SetFocus
End Sub

Private Sub Form_LinkClose()
    End
End Sub

Private Sub Form_LinkExecute(CmdStr As String, Cancel As Integer)
    Cancel = 0
    Select Case CmdStr
        Case "{Load}"
            Initialize
        Case "{Retrieve}"
            RetrieveContacts False, False, False
        Case "{RetrieveWithPhone}"
            RetrieveContacts False, True, False
        Case "{RetrieveWithFax}"
            RetrieveContacts True, False, False
        Case "{RetrieveWithPhoneFax}"
            RetrieveContacts True, True, False
        Case "{ShowEXE}"
            EXEVisible True
        Case "{HideEXE}"
            EXEVisible False
        Case Else
            MsgBox CmdStr & "is not a valid command for this DDE server.", vbCritical, App.Title
            Cancel = True
    End Select
End Sub

Private Sub EXEVisible(bVisible As Boolean)
    Dim lRet As Long
    frmApp.Visible = bVisible
End Sub


Private Sub Form_Load()
    Initialize
End Sub

Public Sub Quit()
    On Error Resume Next
    Set m_appCI = Nothing
End Sub

    
Public Sub RegisterCI()
'*************************************************
'*****UNREGISTER & RE-REGISTER mpCI_ODBC.DLL******
'*****(/s is 'silent' switch, ie no dialog)*******
'*************************************************
    
'***unregister
    Shell "regsvr32 /u /s " & Chr(34) & _
          App.Path & "\mpci_ODBC.dll" & _
          Chr(34)

'***register
    Shell "regsvr32 /s " & Chr(34) & _
          App.Path & "\mpci_ODBC.dll" & _
          Chr(34)

'***********************************************
'*****UNREGISTER & RE-REGISTER mpCI.DLL*********
'***********************************************

'***unregister
    Shell "regsvr32 /u /s " & Chr(34) & _
          App.Path & "\mpci.dll" & _
          Chr(34)

'***register
    Shell "regsvr32 /s " & Chr(34) & _
          App.Path & "\mpci.dll" & _
          Chr(34)

End Sub

Public Sub RegisterMacPac()
'*************************************************
'*****UNREGISTER & RE-REGISTER MACPAC.DLL*********
'*************************************************
    
'***unregister
    Shell "regsvr32 /u /s " & Chr(34) & _
          App.Path & "\MacPac.dll" & _
          Chr(34)

'***register
    Shell "regsvr32 /s " & Chr(34) & _
          App.Path & "\MacPac.dll" & _
          Chr(34)
End Sub

