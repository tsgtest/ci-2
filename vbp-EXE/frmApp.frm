VERSION 5.00
Begin VB.MDIForm frmApp 
   Appearance      =   0  'Flat
   BackColor       =   &H8000000C&
   Caption         =   "Contact Integration"
   ClientHeight    =   4755
   ClientLeft      =   2370
   ClientTop       =   1530
   ClientWidth     =   4680
   Icon            =   "frmApp.frx":0000
   StartUpPosition =   2  'CenterScreen
   Begin VB.Menu mnuContacts 
      Caption         =   "&Contacts"
      Begin VB.Menu mnuContacts_Retrieve 
         Caption         =   "&Retrieve"
      End
      Begin VB.Menu mnuContacts_CreateDatatFile 
         Caption         =   "Create &Data File"
      End
      Begin VB.Menu mnu_Contacts_CloseCI 
         Caption         =   "&Close CI"
      End
      Begin VB.Menu mnuContacts_Sep2 
         Caption         =   "-"
      End
      Begin VB.Menu mnuContacts_Exit 
         Caption         =   "E&xit"
      End
   End
   Begin VB.Menu mnuReg 
      Caption         =   "&Register"
      Begin VB.Menu mnuReg_CI 
         Caption         =   "&CI"
      End
      Begin VB.Menu mnuReg_MacPac 
         Caption         =   "&MacPac"
      End
   End
End
Attribute VB_Name = "frmApp"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public Function SelectedContacts()
    SelectedContacts = frmContacts.txtContacts.Text
End Function

Private Sub MDIForm_Activate()
    Load frmContacts
    frmContacts.WindowState = vbMaximized
    Screen.MousePointer = vbDefault
End Sub

Private Sub MDIForm_Load()
    Dim lRet As Long
    
'   make invisible if command line param is /h
    If (Left(Command(), 2) = "/h") Then
        frmContacts.Visible = False
        Me.Visible = False
    End If
    
End Sub

Private Sub MDIForm_Resize()
    With frmContacts
        On Error Resume Next
        .txtContacts.Height = Me.ScaleHeight - 50
        .txtContacts.Width = Me.ScaleWidth - 100
    End With
End Sub

Private Sub mnu_Contacts_CloseCI_Click()
    frmContacts.Quit
End Sub

Private Sub mnuContacts_CreateDatatFile_Click()
    frmContacts.CreateDataFile True, True, True
    Me.SetFocus
End Sub

Private Sub mnuContacts_Exit_Click()
    frmContacts.Quit
    Unload Me
End Sub

Private Sub mnuContacts_Retrieve_Click()
    frmContacts.RetrieveContacts True, True, True
    Me.SetFocus
End Sub

Private Sub mnuReg_CI_Click()
    frmContacts.RegisterCI
End Sub

Private Sub mnuReg_MacPac_Click()
    frmContacts.RegisterMacPac
End Sub
