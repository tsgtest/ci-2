VERSION 5.00
Object = "{0D62353D-DBA2-11D1-B5DF-0060976089D0}#6.0#0"; "TDBL6.OCX"
Begin VB.Form frmFilter 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   " Find"
   ClientHeight    =   5676
   ClientLeft      =   48
   ClientTop       =   336
   ClientWidth     =   6768
   Icon            =   "frmFilter.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5676
   ScaleWidth      =   6768
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton btnSearch 
      Caption         =   "O&K"
      Default         =   -1  'True
      Height          =   400
      Left            =   5355
      TabIndex        =   13
      ToolTipText     =   "Execute search"
      Top             =   4680
      Width           =   1200
   End
   Begin VB.ComboBox cbxSortBy 
      Appearance      =   0  'Flat
      Height          =   315
      ItemData        =   "frmFilter.frx":000C
      Left            =   480
      List            =   "frmFilter.frx":000E
      Style           =   2  'Dropdown List
      TabIndex        =   12
      Top             =   4755
      Width           =   4000
   End
   Begin VB.CommandButton btnRemove 
      Caption         =   "Cl&ear Search"
      Height          =   400
      Left            =   5385
      TabIndex        =   14
      ToolTipText     =   "Clear search criteria"
      Top             =   1440
      Visible         =   0   'False
      Width           =   1200
   End
   Begin VB.Frame fraFilter 
      Caption         =   "Display Contacts Where:"
      Height          =   3135
      Left            =   270
      TabIndex        =   0
      Top             =   210
      Width           =   4485
      Begin VB.TextBox txtFilterField 
         Height          =   325
         Index           =   3
         Left            =   225
         TabIndex        =   8
         Top             =   2535
         Width           =   4000
      End
      Begin VB.TextBox txtFilterField 
         Height          =   325
         Index           =   2
         Left            =   225
         TabIndex        =   6
         Top             =   1875
         Width           =   4000
      End
      Begin VB.TextBox txtFilterField 
         Height          =   325
         Index           =   1
         Left            =   225
         TabIndex        =   4
         Top             =   1230
         Width           =   4000
      End
      Begin VB.TextBox txtFilterField 
         Height          =   325
         Index           =   0
         Left            =   225
         TabIndex        =   2
         Top             =   570
         Width           =   4000
      End
      Begin VB.Label lblFilterField 
         Caption         =   "##"
         Height          =   330
         Index           =   3
         Left            =   255
         TabIndex        =   7
         Top             =   2325
         Width           =   1650
      End
      Begin VB.Label lblFilterField 
         Caption         =   "##"
         Height          =   330
         Index           =   2
         Left            =   270
         TabIndex        =   5
         Top             =   1665
         Width           =   1650
      End
      Begin VB.Label lblFilterField 
         Caption         =   "##"
         Height          =   330
         Index           =   1
         Left            =   270
         TabIndex        =   3
         Top             =   1020
         Width           =   1650
      End
      Begin VB.Label lblFilterField 
         Caption         =   "##"
         Height          =   330
         Index           =   0
         Left            =   270
         TabIndex        =   1
         Top             =   360
         Width           =   1650
      End
   End
   Begin VB.CommandButton btnCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   400
      Left            =   5355
      TabIndex        =   15
      Top             =   5160
      Width           =   1200
   End
   Begin TrueDBList60.TDBCombo cmbSearchTypes 
      Height          =   360
      Left            =   495
      OleObjectBlob   =   "frmFilter.frx":0010
      TabIndex        =   10
      Top             =   4005
      Width           =   4005
   End
   Begin VB.Label lblType 
      BackStyle       =   0  'Transparent
      Caption         =   "F&ind Contacts:"
      Height          =   240
      Left            =   540
      TabIndex        =   9
      Top             =   3792
      Width           =   1728
   End
   Begin VB.Label Label2 
      Caption         =   "&Sort By:"
      Height          =   300
      Left            =   510
      TabIndex        =   11
      Top             =   4530
      Width           =   585
   End
End
Attribute VB_Name = "frmFilter"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public Enum ciMAPIFilterActions
    ciMAPIFilterAction_Cancel = 0
    ciMAPIFilterAction_Search = 1
    ciMAPIFilterAction_OutlookSearch = 2
    ciMAPIFilterAction_ClearSearch = 3
End Enum

Private m_bCancelled As Boolean
Private m_bSearchNow As Boolean
Private xMsg As String
Private m_oBackend As CIO.ICIBackend
Private m_iFilterAction As ciMAPIFilterActions
Private m_iSortCol As ciListingCols

Public Property Set Backend(oNew As CIO.ICIBackend)
    Set m_oBackend = oNew
End Property

Private Property Let Action(iNew As ciMAPIFilterActions)
    m_iFilterAction = iNew
End Property

Public Property Get Action() As ciMAPIFilterActions
    Action = m_iFilterAction
End Property

Public Property Let SortColumn(iNew As ciListingCols)
    m_iSortCol = iNew
End Property

Public Property Get SortColumn() As ciListingCols
    SortColumn = m_iSortCol
End Property

Private Sub btnCancel_Click()
    Me.Hide
    Action = ciMAPIFilterAction_Cancel
End Sub

Private Sub btnRemove_Click()
    Me.ResetFilter
    Action = ciMAPIFilterAction_Search
End Sub

Private Sub btnSearch_Click()
    Dim i As Integer
    Dim xTemp As String
    
    For i = 1 To 4
        xTemp = xTemp & Me.txtFilterField(i - 1).Text
    Next i
    
    If xTemp <> "" Then
        Action = ciMAPIFilterAction_Search
    Else
        Action = ciMAPIFilterAction_Cancel
    End If
    
    g_oIni.SetUserIni "CIOutlookOM", "SearchType", Me.cmbSearchTypes.BoundText
    On Error Resume Next
    Me.Hide
    DoEvents
End Sub

Private Sub cmbSearchTypes_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cmbSearchTypes, Reposition
    Exit Sub
ProcError:
    g_oError.ShowError
    Exit Sub
End Sub

Private Sub cmbSearchTypes_Validate(Cancel As Boolean)
    On Error GoTo ProcError
    If Me.cmbSearchTypes.Text <> Empty Then
        bValidateBoundTDBCombo Me.cmbSearchTypes
    End If
    Exit Sub
ProcError:
    g_oError.ShowError
    Exit Sub
End Sub


Private Sub Form_Activate()
    Dim i As Integer
    Dim xSearchType As String
    
    If g_oIni Is Nothing Then
        Set g_oIni = New CIni
    End If
    
'   select contents of first criterion
    With Me.txtFilterField(0)
        .SetFocus
        .SelStart = 0
        .SelLength = Len(.Text)
    End With
    
'   set sort field
    With Me.cbxSortBy
        For i = 0 To .ListCount - 1
            If .ItemData(i) = m_oBackend.Filter.SortColumn Then
                .ListIndex = i
                Exit For
            End If
        Next i
        .ListIndex = m_iSortCol
    End With
    
    xSearchType = g_oIni.GetUserIni("CIOutlookOM", "SearchType")
    If xSearchType = Empty Then
        'no default set - use first col (Display Name)
        xSearchType = ciSearchOperator_BeginsWith
    End If
        
    Me.cmbSearchTypes.BoundText = xSearchType

    
'TODO:
'   ensure correct sort field - can
'   be changed elsewhere in interface
End Sub

Private Sub Form_Load()
    Dim i As Integer
    Dim iDisplay As Integer
    Dim iNumFields As Integer
    Dim oFilter As CIO.CFilter
    Dim oArray As XArrayDB
    
    Set oArray = New XArrayDB
    With oArray
        .ReDim 0, 2, 0, 1
        .Value(0, 0) = "That begin with..."
        .Value(0, 1) = ciSearchOperator_BeginsWith
        .Value(1, 0) = "That contain..."
        .Value(1, 1) = ciSearchOperator_Contains
        .Value(2, 0) = "That have the value..."
        .Value(2, 1) = ciSearchOperator_Equals
    End With
    
    Me.cmbSearchTypes.Array = oArray
    ResizeTDBCombo Me.cmbSearchTypes, 3
    
    Action = ciMAPIFilterAction_Cancel
    
    With m_oBackend.Filter
        iNumFields = .CountFields
        
        For i = 1 To iNumFields
            With .FilterFields(i)
'               set caption for each filter field control
                Me.lblFilterField(i - 1).Caption = "&" & .Name & ":"
                'Me.txtFilterField(i - 1).Text = .Value
            End With
            
        Next i
        
'       show only those controls corresponding
'       to existing filter fields
        For i = 1 To 4
            Me.lblFilterField(i - 1).Visible = i <= iNumFields
            Me.txtFilterField(i - 1).Visible = i <= iNumFields
        Next i
        
        With Me.fraFilter
'           adjust frame for number of filter field controls
            If iNumFields < 4 Then
                .Height = .Height - ((4 - iNumFields) * (.Height / 4)) + _
                          (4 - iNumFields) * 100
            End If
        End With
    End With
    
End Sub

Public Sub ResetFilter()
    Dim i As Integer
    On Error Resume Next
    With Me.txtFilterField
        For i = .LBound To .UBound
            .Item(i).Text = ""
        Next i
    End With
End Sub

Private Sub txtFilterField_GotFocus(Index As Integer)
    With Me.txtFilterField(Index)
        .SelStart = 0
        .SelLength = Len(.Text)
    End With
End Sub
