Attribute VB_Name = "mdlGlobals"
Option Explicit

Private Declare Sub OutputDebugString Lib "kernel32" _
  Alias "OutputDebugStringA" _
  (ByVal lpOutputString As String)
  
  Declare Function GetUserName Lib "advapi32.dll" Alias "GetUserNameA" ( _
    ByVal lpBuffer As String, nSize As Long) As Long

  
Public Const MAPI_E_USER_CANCEL = &H80040113
Public Const MAPI_E_NOT_FOUND = &H8004010F

Public g_oCIFav As CIOutlookOM.CCIFavorites
Public g_oOutlook As Outlook.Application
    
Public g_iID As Integer

Public g_oError As CIO.CError
Public g_oSession As CIO.CSessionType
Public g_oGlobals As CIO.CGlobals
Public g_oIni As CIO.CIni
Public g_oReg As CIO.CRegistry
Public g_oConstants As CIO.CConstants

Public Sub DebugPrint(xOutput As String)
    Debug.Print xOutput
    OutputDebugString xOutput
End Sub

Public Function GetMaxResults() As Long
'returns the maximum number of contacts that can be retrieved
    Dim xMaxresults As String
    Dim lMaxResults As Long
    
    If g_oIni Is Nothing Then
        Set g_oIni = New CIO.CIni
    End If
    
    On Error Resume Next
    xMaxresults = g_oIni.GetIni("Backend" & g_iID, "MaxResults", g_oIni.CIIni)
    On Error GoTo ProcError
    If xMaxresults = Empty Or Not IsNumeric(xMaxresults) Then
        lMaxResults = 500
    Else
        lMaxResults = CLng(xMaxresults)
    End If
    GetMaxResults = lMaxResults
    Exit Function
ProcError:
    g_oError.RaiseError "CIOutlookOM.mdlGlobals.GetMaxResults"
    Exit Function
End Function


Public Sub ResizeTDBCombo(tdbCBX As TrueDBList60.TDBCombo, Optional iMaxRows As Integer)
'resizes the dropdown of the true db combo
'to display the specified number of rows.
    Dim oArr As XArrayDB
    Dim iRows As Integer
    
    On Error GoTo ProcError
    With tdbCBX
        Set oArr = .Array
        
        If oArr.Count(1) < iMaxRows Then
            iRows = oArr.Count(1)
        Else
            iRows = iMaxRows
        End If
            
        On Error Resume Next
        .DropdownHeight = ((iRows) * .RowHeight + 8)
        
    End With
    Exit Sub
ProcError:
    Err.Raise Err.Number, "CIOutlookOM.mdlGlobals.ResizeTDBCombo", Err.Description
    Exit Sub
End Sub

Public Function bValidateBoundTDBCombo(oTDBCombo As TDBCombo) As Boolean
'returns FALSE if invalid author has been specified, else TRUE
    With oTDBCombo
        If .BoundText = "" Then
            MsgBox "Invalid entry.  Please select an item " & _
                   "from the list.", vbExclamation, g_oSession.AppTitle
        Else
'           set text of control equal to the
'           display text of the selected row
            .Text = .Columns(.ListField)
            bValidateBoundTDBCombo = True
        End If
    End With
End Function


Public Sub CorrectTDBComboMismatch(oTDBCombo As TDBCombo, iReposition As Integer)
'resets the tdb combo value to the previous match -
'this procedure should be called in the
'TDBCombo Mismatch even procedure only
    Dim bytStart As Byte

    iReposition = False

    With oTDBCombo
'       get current selection start
        bytStart = .SelStart

'       reset the text to the current list text
        If .ListField = Empty Then
            .Text = .Columns(0)
        Else
            .Text = .Columns(.ListField)
        End If

'       return selection to original selection
        .SelStart = Max(CDbl(bytStart - 1), 0)
        .SelLength = Len(.Text)
    End With
End Sub

Function Max(i As Double, j As Double) As Double
    If i > j Then
        Max = i
    Else
        Max = j
    End If
End Function

Function Min(i As Double, j As Double) As Double
    If i > j Then
        Min = j
    Else
        Min = i
    End If
End Function

