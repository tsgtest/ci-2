VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CError"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Public Enum ciMAPIErrs
    ciMAPIErr_NoMAPISession = vbError + 512 + 1
    ciMAPIErr_InvalidMAPIMessage
    ciMAPIErr_InvalidIniSetup
End Enum
'**********************************************************
'initializer/terminator
'**********************************************************
Dim xObjectID As String
Private Sub Class_Initialize()
    Math.Randomize
    xObjectID = "CIOutlookOM.CError" & Format(Now, "hhmmss") & Math.Rnd()
    g_oGlobals.Objects.Add xObjectID, xObjectID
    DebugPrint "Added - " & xObjectID & ": " & g_oGlobals.Objects.Count
End Sub

Private Sub Class_Terminate()
    g_oGlobals.Objects.Remove xObjectID
    DebugPrint "Removed - " & xObjectID & ": " & g_oGlobals.Objects.Count
End Sub
'**********************************************************
