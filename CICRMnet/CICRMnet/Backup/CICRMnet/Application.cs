﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Collections;
using System.Net;

namespace CICRMnet
{
    using CrmSdk;

    //[ComVisible(true)]
    //[Guid("{274A2A63-084E-4d65-AE8F-5C15D2101E6E}")]
    public interface ICICRMMethods
    {
        void Connect(string xURL, string xOrganizationName, string xUserID, string xUserPassword);

        //contact interface methods
        string[,] GetContactListings(string xAttribute1, string xValue1, 
                               string xAttribute2, string xValue2,
                               string xAttribute3, string xValue3,
                               string xAttribute4, string xValue4,
                               string xSortField);
        string[,] GetContactDetails(string xContactID);
        string[,] GetContactPhoneNumbers(string xContactID, string xAddressID);
        string[,] GetContactFaxNumbers(string xContactID, string xAddressID);
        string[,] GetContactEmailAddresses(string xContactID);

        //Company/Account interface methods
        string[,] GetCompanyListings(string xAttribute1, string xValue1,
                       string xAttribute2, string xValue2,
                       string xAttribute3, string xValue3,
                       string xAttribute4, string xValue4,
                       string xSortField);
        string[,] GetCompanyDetails(string xCompanyID);
        string[,] GetCompanyPhoneNumbers(string xContactID, string xAddressID);
        string[,] GetCompanyFaxNumbers(string xContactID, string xAddressID);
        string[,] GetCompanyEmailAddresses(string xContactID);

        //interface methods shared by both Contact & Company/Account
        string[,] GetAddresses(string xEntityID);
        string[,] GetAddressDetails(string xEntityID, string xAddressID);

        
    }

    [ComVisible(true)]
    [Guid("274A2A63-084E-4d65-AE8F-5C15D2101E6E"),
    ClassInterface(ClassInterfaceType.None)]
    public class Application : ICICRMMethods
    {

        public CrmService service;

        #region *********************constructors*********************
        static Application()
        {
        }
        public Application() { }
        #endregion

        #region *********************enumerations*********************
        public enum StoreType
        {
            ContactEntity = 1,
            CompanyEntity = 2
        }
        #endregion

        #region *********************methods*********************
        public void Connect(string xURL, string xOrganizationName, 
                            string xUserName, string xUserPassword)
        {
            // Set up the CRM Service.
            CrmAuthenticationToken token = new CrmAuthenticationToken();
            token.OrganizationName = xOrganizationName;
            token.AuthenticationType = 0;

            service = new CrmService();
            service.Url = xURL;
            service.CrmAuthenticationTokenValue = token;
            service.Credentials = GetCredentials(xOrganizationName, xUserName, xUserPassword);

        }

        private ICredentials GetCredentials(string xOrgName, string xUser, string xPassword)
        {
            if (String.IsNullOrEmpty(xUser))
                return CredentialCache.DefaultCredentials;
            else if (String.IsNullOrEmpty(xOrgName))
                return new NetworkCredential(xUser, xPassword);
            else
                return new NetworkCredential(xUser, xPassword, xOrgName);
        }


        public string [,] GetContactListings(string xAttribute1, string xValue1,
                                      string xAttribute2, string xValue2,
                                      string xAttribute3, string xValue3,
                                      string xAttribute4, string xValue4, 
                                      string xSortField)
        {

            string xAccountName = "";
            string xCompanyTag = "COMPANY";

            //search for Company attribute
            if (xAttribute1.ToUpper() == xCompanyTag)
                xAccountName = xValue1;
            else if (xAttribute2.ToUpper() == xCompanyTag)
                xAccountName = xValue2;
            else if (xAttribute3.ToUpper() == xCompanyTag)
                xAccountName = xValue3;
            else if (xAttribute4.ToUpper() == xCompanyTag)
                xAccountName = xValue4;

            string[] aAccountIDs = null;

            //collect account IDs
            if (xAccountName != "")
                aAccountIDs = GetAccountIDs(xAccountName);

            // Create the ColumnSet that indicates the properties to be retrieved.
            ColumnSet cols = new ColumnSet();

            // Set the properties of the ColumnSet.
            cols.Attributes = new string[] { "contactid", "parentcustomerid", "fullname", "lastname", "firstname", "cpdc_companynameoutlook" };
            
            // Create the ConditionExpression.
            ConditionExpression condition1 = new ConditionExpression();
            ConditionExpression condition2 = new ConditionExpression();
            ConditionExpression condition3 = new ConditionExpression();
            ConditionExpression condition4 = new ConditionExpression();

            // Create Order Expression
            OrderExpression order = new OrderExpression();

            if (xAttribute1 != "")
            {
                if (xAttribute1.ToUpper() == xCompanyTag)
                {
                    // Set the condition
                    condition1.AttributeName = "parentcustomerid";
                    condition1.Operator = ConditionOperator.In;
                    condition1.Values = aAccountIDs;
                }
                else
                {
                    // Set the condition
                    condition1.AttributeName = xAttribute1;
                    condition1.Operator = ConditionOperator.Like;
                    condition1.Values = new string[] { xValue1 };
                }
            }

            if (xAttribute2 != "")
            {
                if (xAttribute2.ToUpper() == xCompanyTag)
                {
                    // Set the condition
                    condition2.AttributeName = "parentcustomerid";
                    condition2.Operator = ConditionOperator.In;
                    condition2.Values = aAccountIDs;
                }
                else
                {
                    // Set the condition
                    condition2.AttributeName = xAttribute2;
                    condition2.Operator = ConditionOperator.Like;
                    condition2.Values = new string[] { xValue2 };
                }
            }

            if (xAttribute3 != "")
            {
                if (xAttribute3.ToUpper() == xCompanyTag)
                {
                    // Set the condition
                    condition3.AttributeName = "parentcustomerid";
                    condition3.Operator = ConditionOperator.In;
                    condition3.Values = aAccountIDs;
                }
                else
                {
                    // Set the condition
                    condition3.AttributeName = xAttribute3;
                    condition3.Operator = ConditionOperator.Like;
                    condition3.Values = new string[] { xValue3 };
                }
            }

            if (xAttribute4 != "")
            {
                if (xAttribute4.ToUpper() == xCompanyTag)
                {
                    // Set the condition
                    condition4.AttributeName = "parentcustomerid";
                    condition4.Operator = ConditionOperator.In;
                    condition4.Values = aAccountIDs;
                }
                else
                {
                    // Set the condition
                    condition4.AttributeName = xAttribute4;
                    condition4.Operator = ConditionOperator.Like;
                    condition4.Values = new string[] { xValue4 };
                }
            }

            if (xSortField != "")
            {
                order.AttributeName = xSortField;
                order.OrderType = OrderType.Ascending;
            }


            ConditionExpression conditionActive = new ConditionExpression();

            // Set the condition for the retrieval of active contacts
            conditionActive.AttributeName = "statecode";
            conditionActive.Operator = ConditionOperator.Equal;
            conditionActive.Values = new string[] { AccountState.Active.ToString() };
            
            // Create the FilterExpression.
            FilterExpression filter = new FilterExpression();

            // Set the properties of the filter.
            filter.FilterOperator = LogicalOperator.And;
            if (xAttribute1 == "")
            {
                filter.Conditions = new ConditionExpression[] { conditionActive };
            }
            else if (xAttribute4 != "")
            {
                filter.Conditions = new ConditionExpression[] { condition1, condition2, condition3, condition4, conditionActive };
            }
            else if (xAttribute3 != "")
            {
                filter.Conditions = new ConditionExpression[] { condition1, condition2, condition3, conditionActive };
            }
            else if (xAttribute2 != "")
            {
                filter.Conditions = new ConditionExpression[] { condition1, condition2, conditionActive };
            }
            else
            {
                filter.Conditions = new ConditionExpression[] { condition1, conditionActive };
            }

            // Create the QueryExpression object.
            QueryExpression query = new QueryExpression();

            // Set the properties of the QueryExpression object.
            //query.EntityName = EntityName.contact.ToString();
            query.EntityName = EntityName.contact.ToString();
            query.ColumnSet = cols;
            query.Criteria = filter;

            if (xSortField != "" && xSortField.ToUpper() != "COMPANY")
            {
                query.Orders = new OrderExpression[] { order };
            }

            BusinessEntityCollection contacts = null;

            // Retrieve Contacts.
            try
            {
                contacts = service.RetrieveMultiple(query);
            }
            catch { }

            string[,] axEntities = null;
            if (contacts != null)
            {
                axEntities = new string[contacts.BusinessEntities.Count(), 5];
                int i = 0;
                //foreach ( item in collection)
                //{
                    
                //}
                foreach (contact oContact in contacts.BusinessEntities)
                {
                    axEntities[i, 0] = oContact.contactid.Value.ToString();
                    axEntities[i, 1] = oContact.fullname;
                    //if (xAccountName == "")
                    //{
                    //    axEntities[i, 2] = oContact.cpdc_companynameoutlook;
                    //}
                    if (oContact.parentcustomerid != null)
                        axEntities[i, 2] = xGetCompanyName(oContact.parentcustomerid.Value.ToString());
                    else
                        axEntities[i, 2] = "";
                    axEntities[i, 3] = oContact.lastname;
                    axEntities[i, 4] = oContact.firstname;
                    i++;
                }
            }



         return axEntities;
           
        }


        public string[,] GetCompanyListings(string xAttribute1, string xValue1,
                                              string xAttribute2, string xValue2,
                                              string xAttribute3, string xValue3,
                                              string xAttribute4, string xValue4,
                                              string xSortField)
        {

            string xAccountName = "";
            string xCompanyTag = "COMPANY";

            //search for Company attribute
            if (xAttribute1.ToUpper() == xCompanyTag)
                xAccountName = xValue1;
            else if (xAttribute2.ToUpper() == xCompanyTag)
                xAccountName = xValue2;
            else if (xAttribute3.ToUpper() == xCompanyTag)
                xAccountName = xValue3;
            else if (xAttribute4.ToUpper() == xCompanyTag)
                xAccountName = xValue4;

            //string[] aAccountIDs = null;

            ////collect account IDs
            //if (xAccountName != "")
            //    aAccountIDs = GetAccountIDs(xAccountName);

            // Create the ColumnSet that indicates the properties to be retrieved.
            ColumnSet cols = new ColumnSet();

            // Set the properties of the ColumnSet.
            cols.Attributes = new string[] { "accountid", "name" };

            // Create the ConditionExpression.
            ConditionExpression condition1 = new ConditionExpression();
            ConditionExpression condition2 = new ConditionExpression();
            ConditionExpression condition3 = new ConditionExpression();
            ConditionExpression condition4 = new ConditionExpression();

            // Create Order Expression
            OrderExpression order = new OrderExpression();

            if (xAttribute1.ToUpper() == xCompanyTag)
            {
                // Set the condition
                condition1.AttributeName = "name";
                condition1.Operator = ConditionOperator.Like;
                condition1.Values = new string[] { xValue1 };
            }

            if (xAttribute2.ToUpper() == xCompanyTag)
            {
                // Set the condition
                condition2.AttributeName = "name";
                condition2.Operator = ConditionOperator.Like;
                condition2.Values = new string[] { xValue2 };
            }

            if (xAttribute3.ToUpper() == xCompanyTag)
            {
                // Set the condition
                condition3.AttributeName = "name";
                condition3.Operator = ConditionOperator.Like;
                condition3.Values = new string[] { xValue3 };
            }

            if (xAttribute4.ToUpper() == xCompanyTag)
            {
                // Set the condition
                condition4.AttributeName = "name";
                condition4.Operator = ConditionOperator.Like;
                condition4.Values = new string[] { xValue4 };
            }


            //force listing by company (ie, account) name
            //if (xSortField != "")
            //{
                order.AttributeName = "name";
                order.OrderType = OrderType.Ascending;
            //}


            ConditionExpression conditionActive = new ConditionExpression();

            // Set the condition for the retrieval of active contacts
            conditionActive.AttributeName = "statecode";
            conditionActive.Operator = ConditionOperator.Equal;
            conditionActive.Values = new string[] { AccountState.Active.ToString() };

            // Create the FilterExpression.
            FilterExpression filter = new FilterExpression();

            // Set the properties of the filter.
            filter.FilterOperator = LogicalOperator.And;
            if (xAttribute1 == "")
            {
                filter.Conditions = new ConditionExpression[] { conditionActive };
            }
            else if (xAttribute4 != "")
            {
                filter.Conditions = new ConditionExpression[] { condition1, condition2, condition3, condition4, conditionActive };
            }
            else if (xAttribute3 != "")
            {
                filter.Conditions = new ConditionExpression[] { condition1, condition2, condition3, conditionActive };
            }
            else if (xAttribute2 != "")
            {
                filter.Conditions = new ConditionExpression[] { condition1, condition2, conditionActive };
            }
            else
            {
                filter.Conditions = new ConditionExpression[] { condition1, conditionActive };
            }

            // Create the QueryExpression object.
            QueryExpression query = new QueryExpression();

            // Set the properties of the QueryExpression object.
            //query.EntityName = EntityName.contact.ToString();
            query.EntityName = EntityName.account.ToString();
            query.ColumnSet = cols;
            query.Criteria = filter;

            //if (xSortField != "" && xSortField.ToUpper() != "COMPANY")
            //{
            query.Orders = new OrderExpression[] { order };
            //}

            BusinessEntityCollection companies = null;

            // Retrieve Contacts.
            try
            {
                companies = service.RetrieveMultiple(query);
            }
            catch { }

            string[,] axEntities = null;
            if (companies != null)
            {
                axEntities = new string[companies.BusinessEntities.Count(), 5];
                int i = 0;
                //foreach ( item in collection)
                //{

                //}
                foreach (account oCompany in companies.BusinessEntities)
                {
                    axEntities[i, 0] = oCompany.accountid.Value.ToString();
                    axEntities[i, 1] = "";
                    axEntities[i, 2] = oCompany.name;
                    axEntities[i, 3] = "";
                    axEntities[i, 4] = "";
                    i++;
                }
            }



            return axEntities;

        }

        public string [,] GetAddresses(string xEntityID)
        {

            //// Create the ColumnSet that indicates the properties to be retrieved.
            //ColumnSet cols = new ColumnSet();

            //// Set the properties of the ColumnSet.
            //cols.Attributes = new string[] { "customeraddressid", "name", "addresstypecode" };

            // Create the ConditionExpression.
            ConditionExpression condition = new ConditionExpression();

            // Set the condition for the retrieval
            condition.AttributeName = "parentid";
            condition.Operator = ConditionOperator.Like;
            condition.Values = new string[] { xEntityID };

            ConditionExpression condition2 = new ConditionExpression();

            // Set the condition for the retrieval
            condition2.AttributeName = "line1";
            condition2.Operator = ConditionOperator.NotNull;
            //condition2.Values = new string[] { AccountState.Active.ToString() };

            // Create the FilterExpression.
            FilterExpression filter = new FilterExpression();

            // Set the properties of the filter.
            filter.FilterOperator = LogicalOperator.And;

            filter.Conditions = new ConditionExpression[] { condition, condition2 };

            //Create the order expression
            //OrderExpression oe = new OrderExpression();

            //oe.AttributeName = "addressnumber";
            //oe.OrderType = OrderType.Ascending;

            OrderExpression oe2 = new OrderExpression();

            oe2.AttributeName = "name";
            oe2.OrderType = OrderType.Ascending;

            // Create the QueryExpression object.
            QueryExpression query = new QueryExpression();

            // Set the properties of the QueryExpression object.
            //query.EntityName = EntityName.contact.ToString();
            query.EntityName = EntityName.customeraddress.ToString();
            //query.ColumnSet = cols;
            query.ColumnSet = new AllColumns();
            query.Criteria = filter;
            query.Orders = new OrderExpression[] { oe2 };

            BusinessEntityCollection addresses = null;

            // Retrieve Addresses.
            try
            {
                addresses = service.RetrieveMultiple(query);
            }
            catch { };

            string[,] axAddresses = null;
            
            if (addresses != null && addresses.BusinessEntities.Count()>0)
            {
                axAddresses = new string[addresses.BusinessEntities.Count(), 2];


                //fill default address first
                foreach (customeraddress address in addresses.BusinessEntities)
                {
                    if (address.addressnumber.Value == 1)
                    {
                        axAddresses[0, 0] = address.customeraddressid.Value.ToString();
                        if (address.name != null)
                        {
                            if (address.addresstypecode == null)
                            {
                                axAddresses[0, 1] = address.name + " (Default)";
                            }
                            else
                            {
                                axAddresses[0, 1] = address.name + " (" + address.addresstypecode.name + ")";
                            }
                        }
                        else
                        {
                            axAddresses[0, 1] = "Default";
                        }
                    }
                }

                int i = 1;

                //fill non-default addresses
                foreach (customeraddress address in addresses.BusinessEntities)      
                {
                    if (address.addressnumber.Value != 1)
                    {
                        axAddresses[i, 0] = address.customeraddressid.Value.ToString();
                        if (address.name != null)
                        {
                            if (address.addresstypecode == null)
                            {
                                axAddresses[i, 1] = address.name + " (Default)";
                            }
                            else
                            {
                                axAddresses[i, 1] = address.name + " (" + address.addresstypecode.name + ")";
                            }

                        }
                        else
                        {
                            axAddresses[i, 1] = "Default";
                        }
                        i++;

                    }
                    
                }
            }
            else
            {
                axAddresses = new string[1,2];
                axAddresses[0, 0] = "0";
                axAddresses[0, 1] = "Default";
            }

            return axAddresses;
        }

        public string GetDefaultAddressID(string xEntityID)
        {
            string xID = "";

            // Create the ConditionExpression.
            ConditionExpression condition = new ConditionExpression();

            // Set the condition for the retrieval
            condition.AttributeName = "parentid";
            condition.Operator = ConditionOperator.Like;
            condition.Values = new string[] { xEntityID };

            ConditionExpression condition2 = new ConditionExpression();

            // Set the condition for the retrieval
            condition2.AttributeName = "line1";
            condition2.Operator = ConditionOperator.NotNull;

            // Create the FilterExpression.
            FilterExpression filter = new FilterExpression();

            // Set the properties of the filter.
            filter.FilterOperator = LogicalOperator.And;

            filter.Conditions = new ConditionExpression[] { condition, condition2 };

            //Create the order expression
            OrderExpression oe2 = new OrderExpression();

            oe2.AttributeName = "name";
            oe2.OrderType = OrderType.Ascending;

            // Create the QueryExpression object.
            QueryExpression query = new QueryExpression();

            // Set the properties of the QueryExpression object.
            query.EntityName = EntityName.customeraddress.ToString();
            query.ColumnSet = new AllColumns();
            query.Criteria = filter;
            query.Orders = new OrderExpression[] { oe2 };

            BusinessEntityCollection addresses = null;

            // Retrieve Addresses.
            try
            {
                addresses = service.RetrieveMultiple(query);
            }
            catch { };

            if (addresses != null && addresses.BusinessEntities.Count() > 0)
            {
                //fill default address first
                foreach (customeraddress address in addresses.BusinessEntities)
                {
                    if (address.addressnumber.Value == 1)
                    {
                        xID = address.customeraddressid.Value.ToString();
                        break;
                    }
                }
            }
            else
                xID = "0";

            return xID;
        }

        public string[,] GetContactPhoneNumbers(string xContactID, string xAddressID)
        {
            //Get Contact's numbers, then specified address's numbers if present
              
            // Create the QueryExpression object.
            QueryByAttribute query = new QueryByAttribute();

            //set fields for Contact
            query.EntityName = EntityName.contact.ToString();
            query.ColumnSet = new AllColumns();
            query.Attributes = new string[] { "contactid" };
            query.Values = new string[] { xContactID };

            BusinessEntityCollection contacts = null;

            // Retrieve contacts
            try
            {
                contacts = service.RetrieveMultiple(query);
            }
            catch { };

            string[,] axPhones = null;
            
            //Create temp ArrayList object
            ArrayList tempArray = new ArrayList();

            if (contacts != null)
            {
                foreach (contact oContact in contacts.BusinessEntities)
                {
                    if (oContact.telephone1 != null)
                    {
                        tempArray.Add(oContact.telephone1 == null ? "" : oContact.telephone1);
                        tempArray.Add("Business");
                    }
                    if (oContact.telephone2 != null)
                    {
                        tempArray.Add(oContact.telephone2 == null ? "" : oContact.telephone2);
                        tempArray.Add("Home");
                    }
                    if (oContact.telephone3 != null)
                    {
                        tempArray.Add(oContact.telephone3 == null ? "" : oContact.telephone3);
                        tempArray.Add("Telephone 3");
                    }
                    if (oContact.mobilephone != null)
                    {
                        tempArray.Add(oContact.mobilephone == null ? "" : oContact.mobilephone);
                        tempArray.Add("Mobile");
                    }
                    if (oContact.pager != null)
                    {
                        tempArray.Add(oContact.pager == null ? "" : oContact.pager);
                        tempArray.Add("Pager");
                    }
                    //i++;
                }
            }

            //set fields for Adress
            query.EntityName = EntityName.customeraddress.ToString();
            query.ColumnSet = new AllColumns();
            query.Attributes = new string[] { "customeraddressid" };
            query.Values = new string[] { xAddressID };

            BusinessEntityCollection addresses = null;

            // Retrieve addresses
            try
            {
                addresses = service.RetrieveMultiple(query);
            }
            catch { };

            if (addresses != null)
            {
                //int i = 0;
                foreach (customeraddress oAddress in addresses.BusinessEntities)
                {
                    if (oAddress.telephone1 != null)
                    {
                        tempArray.Add(oAddress.telephone1 == null ? "" : oAddress.telephone1);
                        tempArray.Add("Main (" + oAddress.name + ")");
                    }
                    if (oAddress.telephone2 != null)
                    {
                        tempArray.Add(oAddress.telephone2 == null ? "" : oAddress.telephone2);
                        tempArray.Add("Phone 2 (" + oAddress.name + ")");
                    }
                    if (oAddress.telephone3 != null)
                    {
                        tempArray.Add(oAddress.telephone3 == null ? "" : oAddress.telephone3);
                        tempArray.Add("Phone 3 (" + oAddress.name + ")");
                    }
                    //i++;
                }
            }

            axPhones = new string[tempArray.Count / 2, 2];

            int j = 1;

            for (int i = 1; i <= tempArray.Count / 2; i++)
            {
                if (i % 2 == 0)
                {
                    axPhones[i - 1, 0]=( string)tempArray[j - 1];
                    axPhones[i - 1, 1] = (string)tempArray[j];
                }
                else
                {
                    axPhones[i - 1, 0] = (string)tempArray[j - 1];
                    axPhones[i - 1, 1]= (string)tempArray[j];
                }
                j = j + 2;
            }
            
            return axPhones;
        }

        public string[,] GetCompanyPhoneNumbers(string xCompanyID, string xAddressID)
        {
            //Get Contact's numbers, then specified address's numbers if present

            // Create the QueryExpression object.
            QueryByAttribute query = new QueryByAttribute();

            //set fields for Contact
            query.EntityName = EntityName.account.ToString();
            query.ColumnSet = new AllColumns();
            query.Attributes = new string[] { "accountid" };
            query.Values = new string[] { xCompanyID };

            BusinessEntityCollection accounts = null;

            // Retrieve contacts
            try
            {
                accounts = service.RetrieveMultiple(query);
            }
            catch { };

            string[,] axPhones = null;

            //Create temp ArrayList object
            ArrayList tempArray = new ArrayList();

            if (accounts != null)
            {
                foreach (account oAccount in accounts.BusinessEntities)
                {
                    if (oAccount.telephone1 != null)
                    {
                        tempArray.Add(oAccount.telephone1 == null ? "" : oAccount.telephone1);
                        tempArray.Add("Main");
                    }
                    if (oAccount.telephone2 != null)
                    {
                        tempArray.Add(oAccount.telephone2 == null ? "" : oAccount.telephone2);
                        tempArray.Add("Telephone 2");
                    }
                    if (oAccount.telephone3 != null)
                    {
                        tempArray.Add(oAccount.telephone3 == null ? "" : oAccount.telephone3);
                        tempArray.Add("Telephone 3");
                    }
                    //if (oAccount.mobilephone != null)
                    //{
                    //    tempArray.Add(oAccount.mobilephone == null ? "" : oAccount.mobilephone);
                    //    tempArray.Add("Mobile");
                    //}
                    //if (oAccount.pager != null)
                    //{
                    //    tempArray.Add(oAccount.pager == null ? "" : oAccount.pager);
                    //    tempArray.Add("Pager");
                    //}
                    //i++;
                }
            }

            //set fields for Adress
            query.EntityName = EntityName.customeraddress.ToString();
            query.ColumnSet = new AllColumns();
            query.Attributes = new string[] { "customeraddressid" };
            query.Values = new string[] { xAddressID };

            BusinessEntityCollection addresses = null;

            // Retrieve addresses
            try
            {
                addresses = service.RetrieveMultiple(query);
            }
            catch { };

            if (addresses != null)
            {
                //int i = 0;
                foreach (customeraddress oAddress in addresses.BusinessEntities)
                {
                    if (oAddress.telephone1 != null)
                    {
                        tempArray.Add(oAddress.telephone1 == null ? "" : oAddress.telephone1);
                        tempArray.Add("Main (" + oAddress.name + ")");
                    }
                    if (oAddress.telephone2 != null)
                    {
                        tempArray.Add(oAddress.telephone2 == null ? "" : oAddress.telephone2);
                        tempArray.Add("Phone 2 (" + oAddress.name + ")");
                    }
                    if (oAddress.telephone3 != null)
                    {
                        tempArray.Add(oAddress.telephone3 == null ? "" : oAddress.telephone3);
                        tempArray.Add("Phone 3 (" + oAddress.name + ")");
                    }
                    //i++;
                }
            }

            axPhones = new string[tempArray.Count / 2, 2];

            int j = 1;

            for (int i = 1; i <= tempArray.Count / 2; i++)
            {
                if (i % 2 == 0)
                {
                    axPhones[i - 1, 0] = (string)tempArray[j - 1];
                    axPhones[i - 1, 1] = (string)tempArray[j];
                }
                else
                {
                    axPhones[i - 1, 0] = (string)tempArray[j - 1];
                    axPhones[i - 1, 1] = (string)tempArray[j];
                }
                j = j + 2;
            }

            return axPhones;
        }


        public string[,] GetContactFaxNumbers(string xContactID, string xAddressID)
        {
            //Get Contact's numbers, then specified address's numbers if present

            // Create the QueryExpression object.
            QueryByAttribute query = new QueryByAttribute();

            //set fields for Contact
            query.EntityName = EntityName.contact.ToString();
            query.ColumnSet = new AllColumns();
            query.Attributes = new string[] { "contactid" };
            query.Values = new string[] { xContactID };

            BusinessEntityCollection contacts = null;

            // Retrieve contacts
            try
            {
                contacts = service.RetrieveMultiple(query);
            }
            catch { };

            string[,] axFaxes = null;

            //Create temp ArrayList object
            ArrayList tempArray = new ArrayList();

            if (contacts != null)
            {
                foreach (contact oContact in contacts.BusinessEntities)
                {
                    if (oContact.fax != null)
                    {
                        tempArray.Add(oContact.fax);
                        tempArray.Add("Fax");
                    }
                }
            }

            //set fields for Adress
            query.EntityName = EntityName.customeraddress.ToString();
            query.ColumnSet = new AllColumns();
            query.Attributes = new string[] { "customeraddressid" };
            query.Values = new string[] { xAddressID };

            BusinessEntityCollection addresses = null;

            // Retrieve addresses
            try
            {
                addresses = service.RetrieveMultiple(query);
            }
            catch { };

            if (addresses != null)
            {
                //int i = 0;
                foreach (customeraddress oAddress in addresses.BusinessEntities)
                {
                    if (oAddress.fax != null)
                    {
                        tempArray.Add(oAddress.fax);
                        tempArray.Add("Fax (" + oAddress.name + ")");
                    }
                }
            }

            axFaxes = new string[tempArray.Count / 2, 2];

            int j = 1;

            for (int i = 1; i <= tempArray.Count / 2; i++)
            {
                if (i % 2 == 0)
                {
                    axFaxes[i - 1, 0] = (string)tempArray[j - 1];
                    axFaxes[i - 1, 1] = (string)tempArray[j];
                }
                else
                {
                    axFaxes[i - 1, 0] = (string)tempArray[j - 1];
                    axFaxes[i - 1, 1] = (string)tempArray[j];
                }
                j = j + 2;
            }

            return axFaxes;

        }

        public string[,] GetCompanyFaxNumbers(string xCompanyID, string xAddressID)
        {
            //Get Contact's numbers, then specified address's numbers if present

            // Create the QueryExpression object.
            QueryByAttribute query = new QueryByAttribute();

            //set fields for Contact
            query.EntityName = EntityName.account.ToString();
            query.ColumnSet = new AllColumns();
            query.Attributes = new string[] { "accountid" };
            query.Values = new string[] { xCompanyID };

            BusinessEntityCollection accounts = null;

            // Retrieve contacts
            try
            {
                accounts = service.RetrieveMultiple(query);
            }
            catch { };

            string[,] axFaxes = null;

            //Create temp ArrayList object
            ArrayList tempArray = new ArrayList();

            if (accounts != null)
            {
                foreach (account oAccount in accounts.BusinessEntities)
                {
                    if (oAccount.fax != null)
                    {
                        tempArray.Add(oAccount.fax);
                        tempArray.Add("Fax");
                    }
                }
            }

            //set fields for Adress
            query.EntityName = EntityName.customeraddress.ToString();
            query.ColumnSet = new AllColumns();
            query.Attributes = new string[] { "customeraddressid" };
            query.Values = new string[] { xAddressID };

            BusinessEntityCollection addresses = null;

            // Retrieve addresses
            try
            {
                addresses = service.RetrieveMultiple(query);
            }
            catch { };

            if (addresses != null)
            {
                //int i = 0;
                foreach (customeraddress oAddress in addresses.BusinessEntities)
                {
                    if (oAddress.fax != null)
                    {
                        tempArray.Add(oAddress.fax);
                        tempArray.Add("Fax (" + oAddress.name + ")");
                    }
                }
            }

            axFaxes = new string[tempArray.Count / 2, 2];

            int j = 1;

            for (int i = 1; i <= tempArray.Count / 2; i++)
            {
                if (i % 2 == 0)
                {
                    axFaxes[i - 1, 0] = (string)tempArray[j - 1];
                    axFaxes[i - 1, 1] = (string)tempArray[j];
                }
                else
                {
                    axFaxes[i - 1, 0] = (string)tempArray[j - 1];
                    axFaxes[i - 1, 1] = (string)tempArray[j];
                }
                j = j + 2;
            }

            return axFaxes;

        }

        public string[,] GetContactEmailAddresses(string xContactID)
        {
            // Create the QueryByAttribute object.
            QueryByAttribute query = new QueryByAttribute();

            // Create the ColumnSet that indicates the properties to be retrieved.
            ColumnSet cols = new ColumnSet();

            // Set the properties of the ColumnSet.
            cols.Attributes = new string[] { "emailaddress1","emailaddress2","emailaddress3" };

            // Set the properties of the QueryExpression object.
            query.ColumnSet = cols;
            query.EntityName = EntityName.contact.ToString();
            query.Attributes = new string[] { "contactid" };
            query.Values = new string[] { xContactID };

            BusinessEntityCollection contacts = null;

            // Retrieve Contacts.
            try
            {
                contacts = service.RetrieveMultiple(query);
            }
            catch { };

            string[,] axEmails = null;
            if (contacts != null)
            {
                axEmails = new string[3, 2];
                int i = 0;
                foreach (contact oContact in contacts.BusinessEntities)
                {
                    axEmails[0, 0] = oContact.emailaddress1 == null ? "" : oContact.emailaddress1;
                    axEmails[0, 1] = "Email Address 1";
                    axEmails[1, 0] = oContact.emailaddress2 == null ? "" : oContact.emailaddress2;
                    axEmails[1, 1] = "Email Address 2";
                    axEmails[2, 0] = oContact.emailaddress3 == null ? "" : oContact.emailaddress3;
                    axEmails[2, 1] = "Email Address 3";
                    i++;
                }
            }

            return axEmails;
        }

        public string[,] GetCompanyEmailAddresses(string xCompanyID)
        {
            // Create the QueryByAttribute object.
            QueryByAttribute query = new QueryByAttribute();

            // Create the ColumnSet that indicates the properties to be retrieved.
            ColumnSet cols = new ColumnSet();

            // Set the properties of the ColumnSet.
            cols.Attributes = new string[] { "emailaddress1", "emailaddress2", "emailaddress3" };

            // Set the properties of the QueryExpression object.
            query.ColumnSet = cols;
            query.EntityName = EntityName.account.ToString();
            query.Attributes = new string[] { "accountid" };
            query.Values = new string[] { xCompanyID };

            BusinessEntityCollection accounts = null;

            // Retrieve company.
            try
            {
                accounts = service.RetrieveMultiple(query);
            }
            catch { };

            string[,] axEmails = null;
            if (accounts != null)
            {
                axEmails = new string[3, 2];
                int i = 0;
                foreach (account oAccount in accounts.BusinessEntities)
                {
                    axEmails[0, 0] = oAccount.emailaddress1 == null ? "" : oAccount.emailaddress1;
                    axEmails[0, 1] = "Email Address 1";
                    axEmails[1, 0] = oAccount.emailaddress2 == null ? "" : oAccount.emailaddress2;
                    axEmails[1, 1] = "Email Address 2";
                    axEmails[2, 0] = oAccount.emailaddress3 == null ? "" : oAccount.emailaddress3;
                    axEmails[2, 1] = "Email Address 3";
                    i++;
                }
            }

            return axEmails;
        }

        public string[,] GetContactDetails(string xContactID)
        {
            // Create the ConditionExpression.
            ConditionExpression condition = new ConditionExpression();

            // Set the condition for the retrieval
            condition.AttributeName = "contactid";
            condition.Operator = ConditionOperator.Equal;
            condition.Values = new string[] { xContactID };

            // Create the FilterExpression.
            FilterExpression filter = new FilterExpression();

            // Set the properties of the filter.
            filter.FilterOperator = LogicalOperator.And;
            filter.Conditions = new ConditionExpression[] { condition };

            // Create the QueryExpression object.
            QueryExpression query = new QueryExpression();

            // Set the properties of the QueryExpression object.
            query.ColumnSet = new AllColumns();
            query.EntityName = EntityName.contact.ToString();
            query.Criteria = filter;

            BusinessEntityCollection contacts = null;

            // Retrieve Contacts.
            try
            {
                contacts = service.RetrieveMultiple(query);
            }
            catch (System.Exception oE)
            {
                MessageBox.Show(oE.Message);
            }

            string[,] axEntities = null;

            if (contacts != null)
            {
                axEntities = new string[contacts.BusinessEntities.Count(), 10];
                int i = 0;
                foreach (contact oContact in contacts.BusinessEntities)
                {
                    axEntities[i, 0] = oContact.fullname == null ? "" : oContact.fullname;
                    axEntities[i, 1] = oContact.salutation;       // Prefix;
                    axEntities[i, 2] = oContact.suffix == null ? "" : oContact.suffix;
                    axEntities[i, 3] = oContact.lastname == null ? "" : oContact.lastname;
                    axEntities[i, 4] = oContact.firstname == null ? "" : oContact.firstname;
                    axEntities[i, 5] = oContact.middlename == null ? "" : oContact.middlename;
                    axEntities[i, 6] = oContact.parentcustomerid == null ? "" : xGetCompanyName(oContact.parentcustomerid.Value.ToString());
                    axEntities[i, 7] = oContact.jobtitle == null ? "" : oContact.jobtitle;
                    axEntities[i, 8] = oContact.department == null ? "" : oContact.department;
                    axEntities[i, 9] = oContact.cpdc_defaultgreetingline == null ? "" : oContact.cpdc_defaultgreetingline;   //Salutation
                    i++;
                }
            }

            return axEntities;
        }

        public string[,] GetCompanyDetails(string xCompanyID)
        {
            // Create the ConditionExpression.
            ConditionExpression condition = new ConditionExpression();

            // Set the condition for the retrieval
            condition.AttributeName = "accountid";
            condition.Operator = ConditionOperator.Equal;
            condition.Values = new string[] { xCompanyID };

            // Create the FilterExpression.
            FilterExpression filter = new FilterExpression();

            // Set the properties of the filter.
            filter.FilterOperator = LogicalOperator.And;
            filter.Conditions = new ConditionExpression[] { condition };

            // Create the QueryExpression object.
            QueryExpression query = new QueryExpression();

            // Set the properties of the QueryExpression object.
            query.ColumnSet = new AllColumns();
            query.EntityName = EntityName.account.ToString();
            query.Criteria = filter;

            BusinessEntityCollection accounts = null;

            // Retrieve companies, ie, accounts
            try
            {
                accounts = service.RetrieveMultiple(query);
            }
            catch (System.Exception oE)
            {
                MessageBox.Show(oE.Message);
            }

            string[,] axEntities = null;

            if (accounts != null)
            {
                axEntities = new string[accounts.BusinessEntities.Count(), 10];
                int i = 0;
                foreach (account oAccount in accounts.BusinessEntities)
                {
                    axEntities[i, 0] = oAccount.name;      // oAccount.fullname == null ? "" : oAccount.fullname;
                    axEntities[i, 1] = "";  // oAccount.salutation;       // Prefix;
                    axEntities[i, 2] = "";  // oAccount.suffix == null ? "" : oAccount.suffix;
                    axEntities[i, 3] = "";  // oAccount.lastname == null ? "" : oAccount.lastname;
                    axEntities[i, 4] = "";  // oAccount.firstname == null ? "" : oAccount.firstname;
                    axEntities[i, 5] = "";  // oAccount.middlename == null ? "" : oAccount.middlename;
                    axEntities[i, 6] = oAccount.name;  // oAccount.parentcustomerid == null ? "" : xGetCompanyName(oAccount.parentcustomerid.Value.ToString());
                    axEntities[i, 7] = "";  // oAccount.jobtitle == null ? "" : oAccount.jobtitle;
                    axEntities[i, 8] = "";  // oAccount.department == null ? "" : oAccount.department;
                    axEntities[i, 9] = "";  // oAccount.cpdc_defaultgreetingline == null ? "" : oAccount.cpdc_defaultgreetingline;   //Salutation
                    i++;
                }
            }

            return axEntities;
        }

        public string [,] GetAddressDetails(string xEntityID, string xAddressID)
        {

            //multiple contacts were selected
            //get Default address details
            if (xAddressID == "1")
            {
                xAddressID = GetDefaultAddressID(xEntityID);
            }

            // Create the QueryByAttribute object.
            QueryByAttribute query = new QueryByAttribute();

            // Set the properties of the QueryExpression object.
            query.ColumnSet = new AllColumns();
            query.EntityName = EntityName.customeraddress.ToString();
            query.Attributes = new string[] { "parentid","customeraddressid" };
            query.Values = new string[] { xEntityID, xAddressID };

            BusinessEntityCollection addresses = null;

            // Retrieve Address
            try
            {
                addresses = service.RetrieveMultiple(query);
            }
            catch {};

            string[,] axAddresses = null;
            if (addresses != null)
            {
                axAddresses = new string[addresses.BusinessEntities.Count(), 7];
                int i = 0;
                foreach (customeraddress address in addresses.BusinessEntities)      
                {
                    axAddresses[i, 0] = address.line1 == null ? "" : address.line1;
                    axAddresses[i, 1] = address.line2 == null ? "" : address.line2;
                    axAddresses[i, 2] = address.line3 == null ? "" : address.line3;
                    axAddresses[i, 3] = address.city == null ? "" : address.city;
                    axAddresses[i, 4] = address.stateorprovince == null ? "" : address.stateorprovince;
                    axAddresses[i, 5] = address.postalcode == null ? "" : address.postalcode;
                    axAddresses[i, 6] = address.country == null ? "" : address.country;
                    i++;
                }
            }
            else
            {
                axAddresses = new string[1, 7];
                axAddresses[0, 0] = "";
                axAddresses[0, 1] = "";
                axAddresses[0, 2] = "";
                axAddresses[0, 3] = "";
                axAddresses[0, 4] = "";
                axAddresses[0, 5] = "";
                axAddresses[0, 6] = "";
            }
            return axAddresses;
        }

        public string xGetCompanyName(string xAccountID)
        {
            // Create the QueryByAttribute object.
            QueryByAttribute query = new QueryByAttribute();

            // Set the properties of the QueryExpression object.
            query.ColumnSet = new AllColumns();
            query.EntityName = EntityName.account.ToString();
            query.Attributes = new string[] { "accountid" };
            query.Values = new string[] { xAccountID };

            BusinessEntityCollection accounts = null;

            // Retrieve Address
            try
            {
                accounts = service.RetrieveMultiple(query);
            }
            catch (System.Exception oE)
            {
                MessageBox.Show(oE.Message);
            }

            string xCompanyName = "";
            if (accounts != null)
            {
                if (accounts.BusinessEntities.Length > 0)
                {
                    account oAccount = (account)accounts.BusinessEntities[0];
                    xCompanyName = oAccount.name;
                }
            }
            //{
            //    int i = 0;
            //    foreach (account oAccount in accounts.BusinessEntities)
            //    {
            //        xCompanyName = oAccount.name;
            //        i++;
            //    }
            //}

            return xCompanyName;
        }

        //Return Account Ids
        public string[] GetAccountIDs(string xAccountName)
        {

            // Create the ConditionExpression.
            ConditionExpression condition1 = new ConditionExpression();

            // Set the condition
            condition1.AttributeName = "name";
            condition1.Operator = ConditionOperator.Like;
            condition1.Values = new string[] { xAccountName };

            // Create the FilterExpression.
            FilterExpression filter = new FilterExpression();

            // Set the properties of the filter.
            //filter.FilterOperator = LogicalOperator.And;
            filter.Conditions = new ConditionExpression[] { condition1 };

            // Create the ColumnSet that indicates the properties to be retrieved.
            ColumnSet cols = new ColumnSet();

            // Set the properties of the ColumnSet.
            cols.Attributes = new string[] { "accountid" };


            // Create the QueryExpression object.
            QueryExpression query = new QueryExpression();

            // Set the properties of the QueryExpression object.
            query.ColumnSet = new AllColumns();
            query.EntityName = EntityName.account.ToString();
            query.ColumnSet = cols;
            query.Criteria = filter;

            BusinessEntityCollection accounts = null;

            // Retrieve Accounts
            try
            {
                accounts = service.RetrieveMultiple(query);
            }
            catch (System.Exception oE)
            {
                MessageBox.Show(oE.Message);
            }

            string [] xAccountIDs = {""};
            if (accounts != null)
            {
                int i = 0;
                string[] xTempArray = new string[accounts.BusinessEntities.Count()];
                foreach (account oAccount in accounts.BusinessEntities)
                {
                    xTempArray[i] = oAccount.accountid.Value.ToString();
                    i++;
                }
                xAccountIDs = xTempArray;
            }
            return xAccountIDs;
        }

        public BusinessEntity GetRecordByEntityId(string entityName,
                                                         string linkEntityName,
                                                         string entityAttribute,
                                                         string linkEntityAttribute,
                                                         string linkEntityId)
        {
            ConditionExpression condition = new ConditionExpression();
            condition.AttributeName = linkEntityAttribute;
            condition.Operator = ConditionOperator.Equal;
            condition.Values = new object[] { linkEntityId };

            LinkEntity link = new LinkEntity();
            link.LinkCriteria = new FilterExpression();
            link.LinkCriteria.FilterOperator = LogicalOperator.And;
            link.LinkCriteria.Conditions = new ConditionExpression[] { condition };
            link.LinkFromEntityName = entityName;
            link.LinkFromAttributeName = entityAttribute;
            link.LinkToEntityName = linkEntityName;
            link.LinkToAttributeName = entityAttribute;

            QueryExpression expression = new QueryExpression();
            expression.EntityName = entityName;
            expression.ColumnSet = new AllColumns();
            expression.LinkEntities = new LinkEntity[] { link };
            BusinessEntityCollection entities =
               service.RetrieveMultiple(expression);
            if (entities.BusinessEntities.Length > 0)
                return entities.BusinessEntities[0];

            return null;
        }

        public static string GetValueFromProperty(BusinessEntity entity, string propertyName)
        {
            string value = string.Empty;
            Type type = entity.GetType();
            foreach (System.Reflection.PropertyInfo property in type.GetProperties())
            {
                if (string.Compare(property.Name, propertyName, true) == 0)
                {
                    switch (property.GetValue(entity, null).GetType().Name)
                    {
                        case "CrmDateTime":
                            value = ((CrmSdk.CrmDateTime)property.GetValue(
                               entity, null)).date;
                            break;
                        case "CrmBoolean":
                            value = ((CrmSdk.CrmBoolean)property.GetValue(
                               entity, null)).Value.ToString();
                            break;
                        case "CrmMoney":
                            value = ((CrmSdk.CrmMoney)property.GetValue(
                               entity, null)).formattedvalue;
                            break;
                        case "CrmDecimal":
                            value = ((CrmSdk.CrmDecimal)property.GetValue(
                               entity, null)).formattedvalue;
                            break;
                        case "Lookup":
                            value = ((CrmSdk.Lookup)property.GetValue(
                               entity, null)).dscSpecified.ToString();
                            break;
                        case "CrmNumber":
                            value = ((CrmSdk.CrmNumber)property.GetValue(
                               entity, null)).formattedvalue;
                            break;
                        case "CrmFloat":
                            value = ((CrmSdk.CrmFloat)property.GetValue(
                               entity, null)).formattedvalue;
                            break;
                        case "Owner":
                            value = ((CrmSdk.Owner)property.GetValue(
                               entity, null)).dscSpecified.ToString();
                            break;
                        case "Key":
                            value = ((CrmSdk.Key)property.GetValue(
                               entity, null)).Value.ToString();
                            break;
                        case "Status":
                            value = ((CrmSdk.Status)property.GetValue(
                                entity, null)).Value.ToString();
                            break;
                        case "Picklist":
                            value = ((CrmSdk.Picklist)property.GetValue(
                               entity, null)).Value.ToString();
                            break;
                        default:
                            value = property.GetValue(entity, null).ToString();
                            break;
                    }
                }
            }
            return value;
        }         
    }
        #endregion

    }
