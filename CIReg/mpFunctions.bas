Attribute VB_Name = "mpFunctions"
Option Explicit

Private Declare Function GetPrivateProfileString Lib "kernel32" Alias _
    "GetPrivateProfileStringA" (ByVal lpApplicationName As String, _
    ByVal lpKeyName As Any, ByVal lpDefault As String, ByVal _
    lpReturnedString As String, ByVal nSize As Long, ByVal lpFileName _
    As String) As Long

Private oFSO As New FileSystemObject

Function bRegister(Optional bIncludeOutlookSecurity As Boolean = True)

    Dim xMP90Path As String
    Dim xCIPath As String
    Dim xCIPath_OLD As String
    Dim xCommonPath As String
    Dim xCustTempWizardPath As String
    Dim xNumPath As String
    Dim xMPPUPCLIPath As String
    Dim I As Integer
    Dim oFile As File
    Dim oFolder As Folder
    Dim oFolder_OLD As Folder
    Dim xCommand As String
    Dim bSilent As Boolean
    Dim bUnregister As Boolean
    Dim xMessage As String
    Dim xOutlookSecurityPath As String
    
    On Error GoTo ProcError

    xCommand = Command()
    
    bSilent = InStr(UCase(xCommand), "/S")
    bUnregister = InStr(UCase(xCommand), "/U")

'   Get paths
    xMP90Path = App.Path & "\"
    
'   create MacPac lock file - this indicates that
'   this register function is currently executing
'   it will get deleted at end of function
    I = FreeFile
    Open xMP90Path & "mpLock.txt" For Output As #I
    Close #I
    
    If bIncludeOutlookSecurity Then
        'hardcode path for now
        xOutlookSecurityPath = "c:\Program Files\common files\Outlook Security Manager"
    End If
    
'   register CI files
    Set oFolder = oFSO.GetFolder(App.Path)
    
    If Not (oFolder Is Nothing) Then
        For Each oFile In oFolder.Files
            With oFile
                If (Right(UCase(.Name), 7) = "NET.DLL") Then
                    If bUnregister Then
                        bReRegisterAssembly "", xMP90Path & .Name
                    Else
                        bReRegisterAssembly xMP90Path & .Name
                    End If
                ElseIf (Right(UCase(.Name), 4) = ".OCX" Or _
                Right(UCase(.Name), 4) = ".DLL" Or _
                Right(UCase(.Name), 4) = ".EXE") Then
                    If bUnregister Then
                        bUnRegisterServer xMP90Path & .Name
                    Else
                        bReRegisterServer xMP90Path & .Name
                    End If
                End If
            End With
        Next oFile
    End If
    
    If xOutlookSecurityPath <> Empty Then
'       register Custom Template Wizard files
        On Error Resume Next
        Set oFolder = oFSO.GetFolder(xOutlookSecurityPath)
        On Error GoTo ProcError
        
        If Not (oFolder Is Nothing) Then
            For Each oFile In oFolder.Files
                With oFile
                    If (Right(UCase(.Name), 4) = ".OCX" Or _
                    Right(UCase(.Name), 4) = ".DLL" Or _
                    Right(UCase(.Name), 4) = ".EXE") Then
                        If bUnregister Then
                            'don't unregister 3rd party components
'                            bUnRegisterServer xOutlookSecurityPath & "\" & .Name
                        Else
                            bReRegisterServer xOutlookSecurityPath & "\" & .Name
                        End If
                    End If
                End With
            Next oFile
        End If
    End If
    
    bRegister = True
                
'   delete lock file
    Kill xMP90Path & "mpLock.txt"
    
    If Not bSilent Then
        If bUnregister Then
            xMessage = "All the Contact Integration components were unregistered."
        Else
            xMessage = "All the Contact Integration components were registered."
        End If
        MsgBox xMessage, vbInformation, "Legal MacPac"
    End If
    
    Exit Function
ProcError:
    If Err.Number <> 0 Then
        MsgBox "Unable to register some of the Contact Integration components.  " & _
                "Please contact your system administrator." & vbCr & vbCr & _
                "ERROR " & Err.Number & ": " & Err.Description, vbExclamation, App.Title
                
'       delete lock file
        Kill xMP90Path & "mpLock.txt"
    
        End
    End If
End Function

Function bReRegisterServer(sRegister As String, Optional sUnregister As String = "") As Boolean
    
    On Error GoTo Register_Error
    Static strRegCmd As String
    Static strUnRegCmd As String
    
    If strRegCmd = "" Then
        strRegCmd = oFSO.GetSpecialFolder(SystemFolder) & "\Regsvr32.exe"
        If Dir(strRegCmd) = "" Then
            strRegCmd = oFSO.GetSpecialFolder(WindowsFolder) & "\Regsvr32.exe"
            If Dir(strRegCmd) = "" Then
                MsgBox "Unable to register Contact Integration components, Regsvr32.exe not found" & vbCr & _
                       "Please contact your system administrator", vbExclamation, App.Title
                Set oFSO = Nothing
                End
            End If
        End If
#If bDebug Then
        strRegCmd = strRegCmd & " "
#Else
        strRegCmd = strRegCmd & " /s "
#End If
        strUnRegCmd = strRegCmd & "/u "
    End If
    ' Register DLLs and unregister existing versions if they exist
    If sUnregister <> "" And Dir(sUnregister) <> "" Then
        If InStr(UCase(sUnregister), ".EXE") > 0 Then
            Shell sUnregister & " /unregserver"
        Else
            Shell strUnRegCmd & Chr(34) & sUnregister & Chr(34)
        End If
        DoEvents
    End If
    
    If Dir(sRegister) <> "" Then
        If InStr(UCase(sRegister), ".EXE") > 0 Then
            Shell sRegister & " /regserver"
        Else
            Shell strRegCmd & Chr(34) & sRegister & Chr(34)
        End If
        DoEvents
    End If
    
    bReRegisterServer = True
    Exit Function
    
Register_Error:
    bReRegisterServer = False
End Function

Function bReRegisterAssembly(sRegister As String, Optional sUnregister As String = "") As Boolean
    
    On Error GoTo Register_Error
    Static strRegCmd As String
    Static strUnRegCmd As String
    Dim dRes As Double
    Dim xDir As String
    Dim xLookIn As String
    Dim xMsg As String
    
    If strRegCmd = "" Then
        
        strRegCmd = Environ$("WINDIR") & "\Microsoft.NET\Framework\v4.0.30319\" & "regasm.exe"
        If (Not oFSO.FileExists(strRegCmd)) Then
            MsgBox "Unable to register Contact Integration components, regasm.exe not found in the " & Environ$("WINDIR") & "\Microsoft.NET\Framework\v4.0.30319 folder." & vbCr & _
                   "Please contact your system administrator", vbExclamation, App.Title
            Set oFSO = Nothing
            End
        End If
        
        strRegCmd = strRegCmd & " /codebase "
'        strRegCmd = strRegCmd & sRegister
        
#If bDebug Then
        strRegCmd = strRegCmd & " "
#Else
        strRegCmd = strRegCmd & " /s "
#End If
        strUnRegCmd = strRegCmd & "/u "
    End If
    ' Register DLLs and unregister existing versions if they exist
    If sUnregister <> "" And Dir(sUnregister) <> "" Then
'        If InStr(UCase(sUnregister), ".EXE") > 0 Then
'            Shell sUnregister & " /unregserver"
'        Else
            Shell strUnRegCmd & Chr(34) & sUnregister & Chr(34)
'        End If
        DoEvents
    End If
    
    If sRegister <> "" And Dir(sRegister) <> "" Then
'        If InStr(UCase(sRegister), ".EXE") > 0 Then
'            Shell sRegister & " /regserver"
'        Else
            Shell strRegCmd & Chr(34) & sRegister & Chr(34)
'        End If
        DoEvents
    End If
    
    bReRegisterAssembly = True
    Exit Function
    
Register_Error:
    bReRegisterAssembly = False
End Function

Function sGetPathFromClassID(sClassName As String, Optional bEXE As Boolean = False)
    
    Dim sClass As String
    
    sClass = GetKeyValue(HKEY_CLASSES_ROOT, sClassName & "\Clsid", "")
    If sClass <> "" Then
        If Not bEXE Then
            sGetPathFromClassID = GetKeyValue(HKEY_CLASSES_ROOT, "CLSID\" & sClass & "\InProcServer32", "")
        Else
            sGetPathFromClassID = GetKeyValue(HKEY_CLASSES_ROOT, "CLSID\" & sClass & "\LocalServer32", "")
        End If
    Else
        sGetPathFromClassID = ""
    End If

End Function

Function bUnRegisterServer(sUnregister As String) As Boolean
    
    On Error GoTo Register_Error
    Static strRegCmd As String
    Static strUnRegCmd As String
    
    If strRegCmd = "" Then
        strRegCmd = oFSO.GetSpecialFolder(SystemFolder) & "\Regsvr32.exe"
        If Dir(strRegCmd) = "" Then
            strRegCmd = oFSO.GetSpecialFolder(WindowsFolder) & "\Regsvr32.exe"
            If Dir(strRegCmd) = "" Then
                MsgBox "Unable to unregister Contact Integration components, Regsvr32.exe not found" & vbCr & _
                       "Please contact your system administrator", vbExclamation, App.Title
                Set oFSO = Nothing
                End
            End If
        End If
#If bDebug Then
        strRegCmd = strRegCmd & " "
#Else
        strRegCmd = strRegCmd & " /s "
#End If
        strUnRegCmd = strRegCmd & "/u "
    End If
    'unregister existing versions if they exist
    If sUnregister <> "" And Dir(sUnregister) <> "" Then
        If InStr(UCase(sUnregister), ".EXE") > 0 Then
            Shell sUnregister & " /unregserver"
        Else
            Shell strUnRegCmd & Chr(34) & sUnregister & Chr(34)
        End If
        DoEvents
    End If
        
    bUnRegisterServer = True
    Exit Function
    
Register_Error:
    bUnRegisterServer = False
End Function

Function GetIni(xSection As String, _
                xKey As String, _
                xIni As String) As Variant
                    
'returns the value for the specified key in the
'specified ini. returns empty if
'no key exists.
    Dim xValue As String
    Dim iNullPos As Integer
    
    xValue = String(255, " ") & &O0
    
'   get requsestor from ini
    GetPrivateProfileString xSection, _
                            xKey, _
                            xValue, _
                            xValue, _
                            Len(xValue), _
                            xIni
                            
    If Left(xValue, 10) = String(10, " ") Then
        GetIni = ""
        Exit Function
    ElseIf xValue <> "" Then
        iNullPos = InStr(xValue, Chr(0))
    
        If iNullPos Then
            xValue = Left(xValue, iNullPos - 1)
        End If
    End If
    GetIni = xValue
End Function


