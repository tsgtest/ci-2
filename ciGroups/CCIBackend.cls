VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CGroups"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True

Option Explicit

Implements ICIBackend
Private m_iID As Integer
Private m_bConnected As Boolean
Private m_oCnn As ADODB.Connection
Private m_oFilter As CIO.CFilter
Private m_vCols(3, 1) As Variant
Private m_oEvents As CIO.CEventGenerator
Private m_oError As CIO.CError

Public Sub AddGroup(ByVal xName As String)
    Dim iRecs As Integer
    
    On Error GoTo ProcError
    
    If Not ICIBackend_IsConnected Then
        Connect
    End If
    
    m_oCnn.Execute "INSERT INTO tblContactGroups (Name) VALUES ('" & xName & "')", iRecs

    Exit Sub
ProcError:
    RaiseError "CIGroups.CCIBackend.AddGroup"
    Exit Sub
End Sub

Public Sub DeleteGroup(ByVal lID As Long)
    On Error GoTo ProcError
    m_oCnn.Execute "DELETE * FROM tblContactGroups WHERE ID =" & lID
    Exit Sub
ProcError:
    RaiseError "CIGroups.CCIBackend.DeleteGroup"
    Exit Sub
End Sub

Public Function AddMember(oContact As CIO.CContact, ByVal fldGroupID As Long) As Long
'adds contact as member of specified group - returns the ID of member record
    Dim xSQL As String
    Dim lRecs As Long
    Dim xDisplayName As String
    
    With oContact
        If .AddressTypeName <> Empty Then
            .DisplayName = .DisplayName & "  (" & .AddressTypeName & ")"
        Else
            .DisplayName = .DisplayName & "  (No address)"
        End If
        
        xSQL = "INSERT INTO tblContactGroupMembers(fldGroup,fldUNID,fldDisplayName," & _
            "fldCompany,fldLastName,fldFirstName,fldTitle,fldAddressType,fldAddressTypeName) VALUES(" & _
            fldGroupID & ",'" & .UNID & "','" & .DisplayName & "','" & .Company & "','" & _
            .LastName & "','" & .FirstName & "','" & .Title & "','" & .AddressTypeID & "','" & .AddressTypeName & "')"
            
        On Error Resume Next
        m_oCnn.Execute xSQL, lRecs
        On Error GoTo ProcError
        
        If lRecs = 0 Then
            Err.Raise cierrs.ciErr_CouldNotAddToCollection, , "Could not add the " & _
                "contact with display name '" & .DisplayName & "' to the group."
        End If
        
        'get ID of added record
        xSQL = "SELECT TOP 1 fldID FROM tblContactGroupMembers ORDER BY fldID DESC"
        Dim oRS As ADODB.Recordset
        Set oRS = New ADODB.Recordset
        oRS.Open xSQL, m_oCnn, adOpenForwardOnly, adLockReadOnly
        
        'return ID of newest record
        AddMember = oRS.Fields(0)
    End With
    Exit Function
ProcError:
    RaiseError "CIGRoups.CCIBackend.AddMember"
End Function

Public Sub DeleteMember(ByVal lID As Long)
    Dim xSQL As String
    Dim lNumAffected As Long
    
    On Error GoTo ProcError
    
    xSQL = "DELETE FROM tblContactGroupMembers WHERE fldID=" & lID
    m_oCnn.Execute xSQL, lNumAffected
    If lNumAffected = 0 Then
        Err.Raise cierrs.ciErr_CouldNotExecuteSQL, , "Could not delete " & _
            "the contact with ID=" & lID
    End If
    Exit Sub
ProcError:
    RaiseError "CIGroups.CCIBackend.DeleteMember"
End Sub

Public Function GetMembers(ByVal lGroupID As Long) As CIO.CListings
'returns the collection of listings that match the filter criteria and
'are stored in the specified folder
    Dim oRS As ADODB.Recordset
    Dim oListings As CIO.CListings
    Dim i As Long
    Dim lRecs As Long
    Dim Cancel As Boolean
    Dim xSQL As String
    Dim xFilterSort As String
    Dim oConst As CIO.CConstants
    
    On Error GoTo ProcError
    
'   get new listings
    Set oListings = New CIO.CListings
    Set oConst = New CIO.CConstants
    
    xSQL = "SELECT * FROM tblContactGroupMembers WHERE fldGroup=" & lGroupID & _
        " ORDER BY fldDisplayName"
    
    'get member records
    Set oRS = m_oCnn.Execute(xSQL, lRecs)

    If Not (oRS.BOF And oRS.EOF) Then
'       create a new listing for each record returned
        If Not Cancel Then
            oRS.MoveFirst
            While Not oRS.EOF
                AddListing oRS, oListings, m_iID & oConst.UNIDSep & _
                    oConst.UNIDSep & CStr(lGroupID), False
                i = i + 1
                If Cancel Then
                    Set GetMembers = oListings
                    Exit Function
                End If
                oRS.MoveNext
            Wend
        End If
    Else
'       return empty listings collection
        Set oListings = New CIO.CListings
    End If
    
    Set GetMembers = oListings
    Exit Function
ProcError:
    Err.Raise Err.Number, "CIGroups.CCIBackend.GetMembers", Err.Description
    Exit Function
End Function

Private Sub CreateFilter()
'creates an empty filter for backend based on ini definition
    Dim xKey As String
    Dim iPos As Integer
    Dim i As Integer
    Dim lCol As CIO.ciListingCols
    On Error GoTo ProcError
    
'   create empty filter
    Set m_oFilter = New CIO.CFilter
    
    For i = 1 To 4
'       get ini key for filter field of this backend
        xKey = Trim$(GetIni("Backend" & m_iID, "Filter" & i, App.Path & "\ci.ini"))
        
        If xKey = Empty Then
            Exit For
        End If
            
'       search for ',' - this delimits name from id in key
        iPos = InStr(xKey, ",")
        If iPos = 0 Or iPos = 1 Or iPos = Len(xKey) Then
'           no delimiter present, no name specified, or no id specified
            Err.Raise ciErr_MissingOrInvalidINIKey, , _
                "Key 'FilterFields" & m_iID & "\ " & _
                    CStr(i) & "' in CI.ini has an invalid value."
        End If
        
        With m_oFilter.FilterFields(i)
'           get name, id, operator of filter field
            .Name = Left(xKey, iPos - 1)
            .ID = Mid(xKey, iPos + 1)
            .Operator = ciSearchOperator_Contains And _
                ciSearchOperator_BeginsWith And ciSearchOperator_Equals
        End With
    Next
    
'   get sort column
    On Error Resume Next
    lCol = CLng(GetIni("Application", "Sort", App.Path & "\ciUser.ini"))
    On Error GoTo ProcError
    
    If Err.Number > 0 Then
        lCol = ciListingCols_DisplayName
    End If
    On Error GoTo ProcError
    
    m_oFilter.SortColumn = lCol
    Exit Sub
ProcError:
    RaiseError "CIGroups.CCIBackend.CreateFilter"
End Sub

Private Sub Connect()
'connects to the groups db
    Dim xPath As String
    
    On Error GoTo ProcError
    'get path to db
    xPath = GetIni("Backend" & m_iID, "DB", App.Path & "\CI.ini")
    
    If xPath = Empty Then
        'use default path
        xPath = App.Path & "\ci.mdb"
    End If
    
    'create connection
    Set m_oCnn = New ADODB.Connection
    m_oCnn.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;" & _
        "User ID=Admin;Data Source=" & xPath
        
    'open connection
    m_oCnn.Open
    
    Exit Sub
ProcError:
    RaiseError "CIMAPI.CCIBackend.ICIBackend_EditContact"
    Exit Sub
End Sub

Private Sub Class_Initialize()
    Set m_oEvents = CIO.CIEvents()
    Set m_oError = New CIO.CError
End Sub

Private Sub ICIBackend_AddContact()

End Sub


Private Property Get ICIBackend_Col1Name() As String
    ICIBackend_Col1Name = m_vCols(0, 0)
End Property

Private Property Get ICIBackend_Col2Name() As String
    ICIBackend_Col2Name = m_vCols(1, 0)
End Property

Private Property Get ICIBackend_Col3Name() As String
    ICIBackend_Col3Name = m_vCols(2, 0)
End Property

Private Property Get ICIBackend_Col4Name() As String
    ICIBackend_Col4Name = m_vCols(3, 0)
End Property

Private Property Get ICIBackend_DisplayName() As String
    ICIBackend_DisplayName = "Contact Groups"
End Property

Private Sub ICIBackend_EditContact(oListing As CListing)

End Sub

Private Function ICIBackend_Events() As CEventGenerator
    ICIBackend_Events = m_oEvents
End Function

Private Function ICIBackend_Filter() As CFilter
'returns the filter object for this backend
    On Error GoTo ProcError
    
    If m_oFilter Is Nothing Then
        CreateFilter
    End If
        
    Set ICIBackend_Filter = m_oFilter
    Exit Function
ProcError:
    RaiseError "CIGroups.CCIBackend.Filter"
End Function

Private Function ICIBackend_GetAddresses(Optional oListing As CListing) As CAddresses
    Dim oAddresses As CIO.CAddresses
    Dim oAddress As CIO.CAddress
    Dim xSQL As String
    
    On Error GoTo ProcError
    
    Set oAddresses = New CIO.CAddresses
    
    If oListing Is Nothing Then
        Set oAddress = New CAddress
        
        'specify the word 'various' in the list of available addresses
        oAddress.Name = "Various"
        oAddress.UNID = ""
    Else
        xSQL = "SELECT fldUNID,fldAddressTypeName FROM " & _
            "tblContactGroupMembers WHERE fldID=" & oListing.ID
        
        Dim oRS As ADODB.Recordset
        Set oRS = New ADODB.Recordset
        oRS.Open xSQL, m_oCnn, adOpenForwardOnly
        
        If Not (oRS.EOF And oRS.BOF) Then
            Set oAddress = New CAddress
            With oAddress
                .Name = oRS!fldAddressTypeName
                .UNID = oRS!fldUNID
            End With
            
        End If
    End If
    
    'add the address to the collection
    oAddresses.Add oAddress
    
    'return
    Set ICIBackend_GetAddresses = oAddresses
    Exit Function
ProcError:
    RaiseError "CIGroups.CCIBackend.ICIBackend_GetAddresses"
    Exit Function
End Function

Private Function ICIBackend_GetContacts(oListing As CListing, oAddress As CAddress, Optional ByVal vAddressType As Variant, Optional ByVal iIncludeData As ciRetrieveData = 1&, Optional ByVal iAlerts As ciAlerts = 1&) As CContacts
    On Error GoTo ProcError
    If Not IsMissing(vAddressType) Then
        Set ICIBackend_GetContacts = GetContacts(oListing, oAddress, vAddressType, iIncludeData, iAlerts)
    Else
        Set ICIBackend_GetContacts = GetContacts(oListing, oAddress, , iIncludeData, iAlerts)
    End If
    Exit Function
ProcError:
    RaiseError "CIGroups.CCIBackend.ICIBackend_GetContacts"
    Exit Function
End Function

Private Function ICIBackend_GetCustomFields(oListing As CListing) As CCustomFields
    On Error GoTo ProcError
    Err.Raise cierrs.ciErr_NotImplemented, , "GetCustomFields is not implemented for Groups backend."
    Exit Function
ProcError:
    RaiseError "CIGroups.CCIBackend.ICIBackend_GetCustomFields"
    Exit Function
End Function

Private Function ICIBackend_GetEMailNumber(oListing As CListing, ByVal vEMailID As Variant) As CContactNumber

End Function

Private Function ICIBackend_GetEMailNumbers(oListing As CListing, Optional ByVal vAddressType As Variant) As CContactNumbers

End Function

Private Function ICIBackend_GetFaxNumber(oListing As CListing, ByVal vFaxID As Variant) As CContactNumber

End Function

Private Function ICIBackend_GetFaxNumbers(oListing As CListing, Optional ByVal vAddressType As Variant) As CContactNumbers

End Function

Private Function ICIBackend_GetFolderListings(oFolder As CIO.CFolder, Optional oFilter As CIO.CFilter) As CIO.CListings
'returns the collection of listings that match the filter criteria and
'are stored in the specified folder
    Dim oRS As ADODB.Recordset
    Dim oListings As CIO.CListings
    Dim i As Long
    Dim lRecs As Long
    Dim Cancel As Boolean
    Dim xSQL As String
    Dim xFilterSort As String
        
    On Error GoTo ProcError
    
'   get new listings
    Set oListings = New CIO.CListings
    
    xSQL = "SELECT * FROM tblContactGroupMembers WHERE fldGroup=" & oFolder.ID
    
    If Not oFilter Is Nothing Then
        xSQL = xSQL & GetFilterSort(oFilter)
    Else
        xSQL = xSQL & " ORDER BY fldDisplayName"
    End If
    
    'get folder records
    Set oRS = m_oCnn.Execute(xSQL, lRecs)

    If Not (oRS.BOF And oRS.EOF) Then
        m_oEvents.RaiseBeforeListingsRetrieved lRecs, Cancel
                          
'       create a new listing for each record returned
        If Not Cancel Then
            oRS.MoveFirst
            While Not oRS.EOF
                AddListing oRS, oListings, oFolder.UNID, True
                
                i = i + 1
                m_oEvents.RaiseAfterListingAdded i, lRecs, Cancel
                If Cancel Then
                    m_oEvents.RaiseAfterListingsRetrieved i
                    Set ICIBackend_GetFolderListings = oListings
                    Exit Function
                End If
                oRS.MoveNext
                m_oEvents.RaiseAfterListingsRetrieved i
            Wend
        End If
    Else
'       return empty listings collection
        Set oListings = New CIO.CListings
    End If
    
    Set ICIBackend_GetFolderListings = oListings
    Exit Function
ProcError:
    Err.Raise Err.Number, "CIGroups.CCIBackend.GetFolderListings", Err.Description
    Exit Function
End Function

Private Function AddListing(oRS As ADODB.Recordset, oListings As CIO.CListings, ByVal xFolderUNID As String, ByVal bTrimParens As Boolean)
    Dim oConst As CConstants
    Dim xID As String
    Dim xDisplayName As String
    Dim xProp1 As String
    Dim xProp2 As String
    Dim xProp3 As String
    Dim iPos As Integer

    With oRS
        On Error Resume Next
'       clear out vars
        xID = Empty
        xDisplayName = Empty
        xProp1 = Empty
        xProp2 = Empty
        xProp3 = Empty

'       add entry to list
        xDisplayName = .Fields(m_vCols(0, 1))
        
        If bTrimParens Then
            'trim parens from display name
        
            'get last (
            iPos = InStrRev(xDisplayName, "  (")
            If iPos > 0 Then
                xDisplayName = Left$(xDisplayName, iPos - 1)
            End If
        End If
        
        xProp1 = .Fields(m_vCols(1, 1))
        xProp2 = .Fields(m_vCols(2, 1))
        xProp3 = .Fields(m_vCols(3, 1))
        On Error GoTo ProcError
        
        Set oConst = New CConstants
        xID = xFolderUNID & oConst.UNIDSep & .Fields("fldID")
        On Error GoTo 0
        
        oListings.Add xID, xDisplayName, xProp1, _
            xProp2, xProp3, ciListingType_Person
'        Debug.Print xDisplay & "  " & xID
    End With
    Exit Function
    
ProcError:
    Err.Raise Err.Number, "CIGroups.CCIBackend.AddListing", Err.Description
End Function

Private Function GetFilterSort(oFilter As CIO.CFilter) As String
'sets filter for supplied messages collection
    Dim i As Integer
    Dim xFilterSort As String
    Dim xVal As String
    
    For i = 1 To 4
        With oFilter.FilterFields(i)
            If .ID <> Empty And .Value <> Empty Then
                On Error Resume Next
                'modify value with wildcards if specified
                If .Operator = ciSearchOperator_BeginsWith Then
                    xVal = .Value & "*"
                ElseIf .Operator = ciSearchOperator_Contains Then
                    xVal = "*" & .Value & "*"
                Else
                    xVal = .Value
                End If
                
                xFilterSort = xFilterSort & _
                    " AND " & .ID & " LIKE '" & xVal & "' "
            End If
        End With
    Next i
    
    If oFilter.SortColumn <> Empty Then
'       sort descending by specified field
        xFilterSort = xFilterSort & " ORDER BY " & _
            m_vCols(oFilter.SortColumn, 1)
    Else
        xFilterSort = xFilterSort & " ORDER BY fldDisplayName"
    End If
    
    GetFilterSort = xFilterSort
    Exit Function
ProcError:
    RaiseError "CIMAPI.CCIBackend.SetFilterSort"
End Function

Private Function ICIBackend_GetFolders(oStore As CStore) As CFolders
    Dim oFolder As CIO.CFolder
    Dim oFolders As CIO.CFolders
    Dim xSQL As String
    
    On Error GoTo ProcError
    
    'create new, empty folders collection
    Set oFolders = New CIO.CFolders
    
    'connect to db if necessary
    If Not ICIBackend_IsConnected Then
        Connect
    End If
    
    'get contact groups from db
    xSQL = "SELECT Name, ID FROM tblContactGroups ORDER by Name"
    
    Dim oRS As Recordset
    Dim lRecs As Long
    Dim l As Long
    
    Set oRS = m_oCnn.Execute(xSQL, lRecs)
    
    With oRS
        If Not (.BOF And .EOF) Then
            'there are groups - cycle through,
            'adding a folder for each group
            While Not .EOF
                Set oFolder = New CIO.CFolder
                oFolder.UNID = m_iID & UNIDSep & UNIDSep & .Fields(1)
                oFolder.Name = .Fields(0)
                oFolders.Add oFolder
                .MoveNext
            Wend
        End If
    End With
    
    'return collection
    Set ICIBackend_GetFolders = oFolders
    Exit Function
ProcError:
    RaiseError "CIGroups.CCIBackend.ICIBackend_GetFolders"
End Function

Private Function ICIBackend_GetPhoneNumber(oListing As CListing, ByVal vPhoneID As Variant) As CContactNumber

End Function

Private Function ICIBackend_GetPhoneNumbers(oListing As CListing, Optional ByVal vAddressType As Variant) As CContactNumbers
'calls ICIBackend_GetPhoneNumbers in backend of specified listing
    'get backend of listing
    Dim oB As ICIBackend
    Set oB = oListing.BackendID
    
End Function

Private Function ICIBackend_GetStoreListings(oStore As CStore, Optional oFilter As CFilter) As CListings
    On Error GoTo ProcError
    Err.Raise cierrs.ciErr_NotImplemented, , "GetStoreListings is not implemented for Groups backend."
    Exit Function
ProcError:
    RaiseError "CIGroups.CCIBackend.ICIBackend_GetStoreListings"
    Exit Function
End Function

Private Function ICIBackend_GetStores() As CStores
    If Not ICIBackend_IsConnected Then
        Connect
    End If
End Function

Private Function ICIBackend_GetSubFolders(oFolder As CFolder) As CFolders
    On Error GoTo ProcError
    Err.Raise cierrs.ciErr_NotImplemented, , "GetSubFolders is not implemented for Groups backend."
    Exit Function
ProcError:
    RaiseError "CIGroups.CCIBackend.ICIBackend_GetSubFolders"
    Exit Function
End Function

Private Function ICIBackend_HasAddresses(oListing As CListing) As Boolean

End Function

Private Property Get ICIBackend_ID() As Integer
    ICIBackend_ID = m_iID
End Property

Private Property Let ICIBackend_ID(ByVal RHS As Integer)
    m_iID = RHS
End Property

Private Sub ICIBackend_Initialize(iID As Integer)
    Dim xName As String
    Dim xKey As String
    Dim xID As String
    Dim i As Integer
    Dim iPos As Integer
    
    On Error GoTo ProcError
    m_iID = iID
    
    'get column names/ids
    For i = 1 To 4
        xKey = GetIni("Backend" & iID, "Col" & i, App.Path & "\ci.ini")
        If xKey <> Empty Then
            'parse value
            iPos = InStr(xKey, ",")
            If iPos = 0 Then
                'no comma found - should be in form 'Name,ID'
                Err.Raise cierrs.ciErr_MissingOrInvalidINIKey, , _
                    "Invalid value in ci.ini for Backend" & iID & _
                    "\Col" & i & "."
            Else
                'get name and id from string
                xName = Left$(xKey, iPos - 1)
                xID = Mid$(xKey, iPos + 1)
                
                If xName = Empty Or xID = Empty Then
                    'missing some value
                    Err.Raise cierrs.ciErr_MissingOrInvalidINIKey, , _
                        "Invalid value in ci.ini for Backend" & iID & _
                        "\Col" & i & "."
                Else
                    'assign to columns array
                    m_vCols(i - 1, 0) = Trim$(xName)
                    m_vCols(i - 1, 1) = Trim$(xID)
                End If
            End If
        End If
    Next i
    Exit Sub
ProcError:
    RaiseError "CIGroups.CCIBackend.ICIBackend_Initialize"
    Exit Sub
End Sub

Private Property Get ICIBackend_IsConnected() As Boolean
     ICIBackend_IsConnected = m_bConnected
End Property

Private Property Get ICIBackend_Name() As String
    ICIBackend_Name = "Contact Groups"
End Property

Private Function ICIBackend_NumberPromptFormat() As ICINumberPromptFormat

End Function

Private Function ICIBackend_SearchFolder(oFolder As CFolder, bCancel As Boolean) As CListings

End Function

Private Function ICIBackend_SearchNative() As CListings
    On Error GoTo ProcError
    Err.Raise cierrs.ciErr_NotImplemented, , "SearchNative is not implemented for Groups backend."
    Exit Function
ProcError:
    RaiseError "CIGroups.CCIBackend.ICIBackend_SearchNative"
    Exit Function
End Function

Private Property Get ICIBackend_SearchOperators() As ciSearchOperators
    ICIBackend_SearchOperators = ciSearchOperator_BeginsWith Or _
        ciSearchOperator_Contains Or ciSearchOperator_Equals
End Property

Private Function ICIBackend_SearchStore(oStore As CStore) As CListings
    On Error GoTo ProcError
    Err.Raise cierrs.ciErr_NotImplemented, , "SearchStore is not implemented for Groups backend."
    Exit Function
ProcError:
    RaiseError "CIGroups.CCIBackend.ICIBackend_SearchStore"
    Exit Function
End Function

Private Property Get ICIBackend_SupportsContactAdd() As Boolean
    ICIBackend_SupportsContactAdd = False
End Property

Private Property Get ICIBackend_SupportsContactEdit() As Boolean
    ICIBackend_SupportsContactEdit = False
End Property

Private Property Get ICIBackend_SupportsFolders() As Boolean
    ICIBackend_SupportsFolders = True
End Property

Private Property Get ICIBackend_SupportsNativeSearch() As Boolean
    ICIBackend_SupportsNativeSearch = False
End Property

Private Property Get ICIBackend_SupportsNestedFolders() As Boolean
    ICIBackend_SupportsNestedFolders = False
End Property

Private Property Get ICIBackend_SupportsStoreLoad() As Boolean
    ICIBackend_SupportsStoreLoad = False
End Property

Private Property Get ICIBackend_SupportsStoreSearch() As Boolean
    ICIBackend_SupportsStoreSearch = False
End Property

Private Property Get ICIBackend_SupportsMultipleStores() As Boolean
    ICIBackend_SupportsMultipleStores = False
End Property

Private Function GetContacts(oListing As CIO.CListing, oAddress As CAddress, Optional ByVal vAddressType As Variant, Optional ByVal iIncludeData As ciRetrieveData = 1&, Optional ByVal iAlerts As ciAlerts = 1&) As CIO.CContacts
'returns the contact pointed to by the specified listing and address
    Dim oContacts As CIO.CContacts
    Dim oRS As ADODB.Recordset
    Dim xSQL As String
    Dim oNativeAddress As CIO.CAddress
    Dim oNativeListing As CIO.CListing
    Dim oUNID As CIO.CUNID
    
    On Error GoTo ProcError
    
    Set oContacts = New CIO.CContacts
    Set oUNID = New CIO.CUNID
    
    'get unid for the group listing
    xSQL = "SELECT fldUNID FROM tblContactGroupMembers WHERE fldID =" & oListing.ID
    
    Set oRS = New ADODB.Recordset
    
    With oRS
        .Open xSQL, m_oCnn, adOpenForwardOnly, adLockReadOnly
    
        If Not (.BOF And .EOF) Then
            'get unid for listing
            oListing.UNID = !fldUNID
            
            'create the unid of the listing and address
            'as it exists in its native application
            Set oNativeAddress = oUNID.GetAddress(!fldUNID, oAddress.Name)
            Set oNativeListing = oUNID.GetListing(!fldUNID, oListing.DisplayName)

            'ask cio to get the contact in its native app -
            'groups knows nothing of other backends - it has to
            'ask cio to get contacts
            m_oEvents.RaiseContactsRequested oNativeListing, oContacts, _
                oNativeAddress, vAddressType, iIncludeData, iAlerts
        Else
            Err.Raise cierrs.ciErr_InvalidUNID, , _
                "No UNID found for groups listing with ID=" & oListing.ID
        End If
    End With
    Set GetContacts = oContacts
    Exit Function
ProcError:
    RaiseError "CIGroups.CCIBackend.GetContacts"
    Exit Function
End Function

