VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CCIBackend"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Implements ICIBackend
Private m_iID As Integer
Private m_bConnected As Boolean
Private m_oCnn As ADODB.Connection

Public Sub AddGroup(ByVal xName As String)
    m_oCnn.Execute "INSERT INTO tblContactGroups (Name) VALUES (" & xName & ")"
End Sub

Public Sub DeleteGroup(ByVal lID As Long)
    m_oCnn.Execute "DELETE * FROM tblContactGroups WHERE ID =" & lID
End Sub

Private Sub Connect()
'connects to the groups db
    Dim xPath As String
    
    On Error GoTo ProcError
    'get path to db
    xPath = GetIni("Backend" & m_iID, "DB", App.Path & "\CI.ini")
    
    If xPath = Empty Then
        'use default path
        xPath = App.Path & "\ci.mdb"
    End If
    
    'create connection
    Set m_oCnn = New ADODB.Connection
    m_oCnn.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;" & _
        "User ID=Admin;Data Source=" & xPath
        
    'open connection
    m_oCnn.Open
    
    Exit Sub
ProcError:
    RaiseError "CIMAPI.CCIBackend.ICIBackend_EditContact"
    Exit Sub
End Sub

Private Sub ICIBackend_AddContact()

End Sub

Private Property Get ICIBackend_Col1Name() As String

End Property

Private Property Get ICIBackend_Col2Name() As String

End Property

Private Property Get ICIBackend_Col3Name() As String

End Property

Private Property Get ICIBackend_Col4Name() As String

End Property

Private Property Get ICIBackend_DisplayName() As String
    ICIBackend_DisplayName = "Contact Groups"
End Property

Private Sub ICIBackend_EditContact(oListing As CListing)

End Sub

Private Function ICIBackend_Events() As CEventGenerator

End Function

Private Function ICIBackend_Filter() As CFilter

End Function

Private Function ICIBackend_GetAddresses(Optional oListing As CListing) As CAddresses

End Function

Private Function ICIBackend_GetContacts(oListing As CListing, oAddress As CAddress, Optional ByVal iIncludeData As ciRetrieveData = 1&, Optional ByVal iAlerts As ciAlerts = 1&) As CContacts

End Function

Private Function ICIBackend_GetCustomFields(oListing As CListing) As CCustomFields

End Function

Private Function ICIBackend_GetEMailNumber(oListing As CListing, ByVal vEMailID As Variant) As CContactNumber

End Function

Private Function ICIBackend_GetEMailNumbers(oListing As CListing, Optional ByVal vAddressType As Variant) As CContactNumbers

End Function

Private Function ICIBackend_GetFaxNumber(oListing As CListing, ByVal vFaxID As Variant) As CContactNumber

End Function

Private Function ICIBackend_GetFaxNumbers(oListing As CListing, Optional ByVal vAddressType As Variant) As CContactNumbers

End Function

Private Function ICIBackend_GetFolderListings(oFolder As CFolder, Optional oFilter As CFilter) As CListings

End Function

Private Function ICIBackend_GetFolders(oStore As CStore) As CFolders

End Function

Private Function ICIBackend_GetPhoneNumber(oListing As CListing, ByVal vPhoneID As Variant) As CContactNumber

End Function

Private Function ICIBackend_GetPhoneNumbers(oListing As CListing, Optional ByVal vAddressType As Variant) As CContactNumbers

End Function

Private Function ICIBackend_GetStoreListings(oStore As CStore, Optional oFilter As CFilter) As CListings

End Function

Private Function ICIBackend_GetStores() As CStores
    If Not ICIBackend_IsConnected Then
        Connect
    End If
End Function

Private Function ICIBackend_GetSubFolders(oFolder As CFolder) As CFolders

End Function

Private Function ICIBackend_HasAddresses(oListing As CListing) As Boolean

End Function

Private Property Get ICIBackend_ID() As Integer
    ICIBackend_ID = m_iID
End Property

Private Property Let ICIBackend_ID(ByVal RHS As Integer)
    m_iID = RHS
End Property

Private Sub ICIBackend_Initialize(iID As Integer)
    m_iID = iID
End Sub

Private Property Get ICIBackend_IsConnected() As Boolean
     ICIBackend_IsConnected = m_bConnected
End Property

Private Property Get ICIBackend_Name() As String
    ICIBackend_Name = "Contact Groups"
End Property

Private Function ICIBackend_NumberPromptFormat() As ICINumberPromptFormat

End Function

Private Function ICIBackend_SearchFolder(oFolder As CFolder, bCancel As Boolean) As CListings

End Function

Private Function ICIBackend_SearchNative() As CListings

End Function

Private Property Get ICIBackend_SearchOperators() As ciSearchOperators

End Property

Private Function ICIBackend_SearchStore(oStore As CStore) As CListings

End Function

Private Property Get ICIBackend_SupportsContactAdd() As Boolean

End Property

Private Property Get ICIBackend_SupportsContactEdit() As Boolean

End Property

Private Property Get ICIBackend_SupportsFolders() As Boolean

End Property

Private Property Get ICIBackend_SupportsNativeSearch() As Boolean

End Property

Private Property Get ICIBackend_SupportsNestedFolders() As Boolean

End Property

Private Property Get ICIBackend_SupportsStoreLoad() As Boolean

End Property

Private Property Get ICIBackend_SupportsStoreSearch() As Boolean

End Property
