Attribute VB_Name = "mdlGlobal"
Option Explicit

Private Declare Function WritePrivateProfileSection Lib "kernel32" Alias _
    "WritePrivateProfileSectionA" (ByVal lpAppName As String, ByVal _
    lpString As String, ByVal lpFileName As String) As Long
    
Private Declare Function GetTickCount Lib "kernel32" () As Long

Public Const ciCRMInternalID As Integer = 114

Public g_vCols(3, 1) As Variant
Public g_iSortCol As ciListingCols
Public g_bDisplayByContactType As Boolean
Public g_oApp As CICRMnet.Application
'Public g_oProxiedUser As Interaction.IAUser
Public g_oSession As CIO.CSessionType

Public g_oError As CIO.CError
Public g_oGlobals As CIO.CGlobals
Public g_oIni As CIO.CIni
Public g_oReg As CIO.CRegistry
Public g_oConstants As CIO.CConstants
Public g_iID As Integer

Public Sub CorrectTDBComboMismatch(oTDBCombo As TDBCombo, iReposition As Integer)
'resets the tdb combo value to the previous match -
'this procedure should be called in the
'TDBCombo Mismatch even procedure only
    Dim bytStart As Byte
    
    iReposition = False
    
    With oTDBCombo
'       get current selection start
        bytStart = .SelStart
        
'       reset the text to the current list text
        If .ListField = Empty Then
            .Text = .Columns(0)
        Else
            .Text = .Columns(.ListField)
        End If
        
'       return selection to original selection
        .SelStart = Max(CDbl(bytStart - 1), 0)
        .SelLength = Len(.Text)
    End With
End Sub

Function Max(i As Double, j As Double) As Double
    If i > j Then
        Max = i
    Else
        Max = j
    End If
End Function

Function Min(i As Double, j As Double) As Double
    If i > j Then
        Min = j
    Else
        Min = i
    End If
End Function

Public Function bValidateBoundTDBCombo(oTDBCombo As TDBCombo) As Boolean
'returns FALSE if invalid author has been specified, else TRUE
    With oTDBCombo
        If .BoundText = "" Then
            MsgBox "Invalid entry.  Please select an item " & _
                   "from the list.", vbExclamation, g_oSession.AppTitle
        Else
'           set text of control equal to the
'           display text of the selected row
            .Text = .Columns(.ListField)
            bValidateBoundTDBCombo = True
        End If
    End With
End Function

Public Function GetMaxResults() As Long
'returns the maximum number of contacts that can be retrieved
    Dim xMaxresults As String
    Dim lMaxResults As Long
    
    On Error GoTo ProcError
    xMaxresults = g_oIni.GetIni("Backend" & g_iID, "MaxResults", g_oIni.CIIni)
    If xMaxresults = Empty Or Not IsNumeric(xMaxresults) Then
        lMaxResults = 500
    Else
        lMaxResults = CLng(xMaxresults)
    End If
    GetMaxResults = lMaxResults
    Exit Function
ProcError:
    g_oError.RaiseError "CICRM.CCIBackend.GetMaxResults"
    Exit Function
End Function



Public Function xSubstitute(ByVal xString As String, _
                     ByVal xSearch As String, _
                     ByVal xReplace As String) As String
'replaces xSearch in
'xString with xReplace -
'returns modified xString -
'NOTE: SEARCH IS NOT CASE SENSITIVE

    Dim iSeachPos As Integer
    Dim xNewString As String
    
'   get first char pos
    iSeachPos = InStr(UCase(xString), _
                      UCase(xSearch))
    
'   remove switch all chars
    While iSeachPos
        xNewString = xNewString & _
            Left(xString, iSeachPos - 1) & _
            xReplace
        xString = Mid(xString, iSeachPos + Len(xSearch))
        iSeachPos = InStr(UCase(xString), _
                          UCase(xSearch))
    
    Wend
    
    xNewString = xNewString & xString
    xSubstitute = xNewString
End Function

Public Function CurrentTick() As Long
    CurrentTick = GetTickCount()
End Function

Public Function ElapsedTime(lStartTick As Long) As Single
    ElapsedTime = Format((CurrentTick() - lStartTick) / 1000, "#,##0.0000")
End Function

