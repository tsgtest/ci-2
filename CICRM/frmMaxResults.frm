VERSION 5.00
Object = "{0D62353D-DBA2-11D1-B5DF-0060976089D0}#6.0#0"; "TDBL6.OCX"
Begin VB.Form frmMaxResults 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Set Maximum Number"
   ClientHeight    =   2220
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   4425
   Icon            =   "frmMaxResults.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2220
   ScaleWidth      =   4425
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin TrueDBList60.TDBList lstSetMax 
      Height          =   1845
      Left            =   150
      OleObjectBlob   =   "frmMaxResults.frx":000C
      TabIndex        =   3
      Top             =   210
      Width           =   1080
   End
   Begin VB.CommandButton btnCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   400
      Left            =   3015
      TabIndex        =   2
      Top             =   1650
      Width           =   1200
   End
   Begin VB.CommandButton btnOK 
      Caption         =   "O&K"
      Default         =   -1  'True
      Height          =   400
      Left            =   1725
      TabIndex        =   1
      Top             =   1650
      Width           =   1200
   End
   Begin VB.Label lblSetMax 
      BackStyle       =   0  'Transparent
      Caption         =   "Specify the maximum number of contacts to retrieve from an InterAction search or folder."
      Height          =   720
      Left            =   1695
      TabIndex        =   0
      Top             =   210
      Width           =   2565
   End
End
Attribute VB_Name = "frmMaxResults"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private m_bCancelled As Boolean

Public Property Get Cancelled()
    Cancelled = m_bCancelled
End Property

Private Sub btnCancel_Click()
    DoEvents
    Me.Hide
    m_bCancelled = True
End Sub

Private Sub btnOK_Click()
    DoEvents
    Me.Hide
    m_bCancelled = False
End Sub

Private Sub Form_Activate()
    Dim xMax As String
    xMax = g_oIni.GetUserIni("CIIA5", "MaxResults")
    If xMax = Empty Then
        xMax = "500"
    End If
    Me.lstSetMax.BoundText = xMax
End Sub

Private Sub Form_Load()
    Dim oXArray As XArrayDB
    Set oXArray = New XArrayDB
    
    With oXArray
        .ReDim 0, 7, 0, 0
        .Value(0, 0) = "50"
        .Value(1, 0) = "100"
        .Value(2, 0) = "500"
        .Value(3, 0) = "1,000"
        .Value(4, 0) = "2,000"
        .Value(5, 0) = "3,000"
        .Value(6, 0) = "5,000"
        .Value(7, 0) = "10,000"
    End With
    
    Me.lstSetMax.Array = oXArray
End Sub

Private Sub lstSetMax_DblClick()
    btnOK_Click
End Sub
