VERSION 5.00
Object = "{0D62353D-DBA2-11D1-B5DF-0060976089D0}#6.0#0"; "TDBL6.OCX"
Begin VB.Form frmProxyFor 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Act On Behalf Of"
   ClientHeight    =   4170
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5085
   Icon            =   "frmProxyFor.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4170
   ScaleWidth      =   5085
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton btnOK 
      Caption         =   "O&K"
      Height          =   420
      Left            =   2535
      TabIndex        =   3
      Top             =   3615
      Width           =   1100
   End
   Begin VB.CommandButton btnCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   420
      Left            =   3750
      TabIndex        =   2
      Top             =   3615
      Width           =   1100
   End
   Begin TrueDBList60.TDBList lstUsers 
      Height          =   2895
      Left            =   180
      OleObjectBlob   =   "frmProxyFor.frx":000C
      TabIndex        =   1
      Top             =   435
      Width           =   4680
   End
   Begin VB.Label lblUsers 
      BackStyle       =   0  'Transparent
      Caption         =   "&Users:"
      Height          =   300
      Left            =   240
      TabIndex        =   0
      Top             =   210
      Width           =   525
   End
End
Attribute VB_Name = "frmProxyFor"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private m_bCancelled As Boolean

Private Sub btnCancel_Click()
    m_bCancelled = True
    Me.Hide
End Sub

Private Sub btnOK_Click()
    m_bCancelled = False
    Me.Hide
End Sub

Private Sub Form_Activate()
    If Me.lstUsers.Array.Count(1) > 0 Then
        If Not g_oProxiedUser Is Nothing Then
            Me.lstUsers.Text = g_oProxiedUser.DisplayName
        Else
            Me.lstUsers.Text = g_oCnn.CurrentUser.DisplayName
        End If
    Else
        Me.lstUsers.Enabled = False
    End If
End Sub

Private Sub Form_Load()
    On Error GoTo ProcError
    LoadUsers
    Exit Sub
ProcError:
    g_oError.ShowError
    Exit Sub
End Sub

Public Property Get Cancelled() As Boolean
    Cancelled = m_bCancelled
End Property

Private Sub LoadUsers()
    Dim oArray As XArrayDB
    Dim iNum As Integer
    Dim i As Integer
    
    On Error GoTo ProcError
    'load additional stores into list
    Set oArray = New XArrayDB
    
    iNum = g_oCnn.Proxies.Count
    
    With oArray
        .ReDim 0, iNum - 1, 0, 1
        
        For i = 1 To iNum
            'get db name, server, file path
            .Value(i - 1, 0) = g_oCnn.Proxies(i).DisplayName
            .Value(i - 1, 1) = g_oCnn.Proxies(i).UserID
        Next i
    End With
    Me.lstUsers.Array = oArray
    Exit Sub
ProcError:
    g_oError.RaiseError "CINotes.frmRemoveStore.LoadUsers"
    Exit Sub
End Sub

Private Sub lstUsers_DblClick()
    btnOK_Click
End Sub

