VERSION 5.00
Object = "{3B008041-905A-11D1-B4AE-444553540000}#1.0#0"; "Vsocx6.ocx"
Object = "{0D62353D-DBA2-11D1-B5DF-0060976089D0}#6.0#0"; "TDBL6.OCX"
Begin VB.Form frmContactDetail 
   BackColor       =   &H00FFFFFF&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Contact Integration 2.0 Sample Client Application"
   ClientHeight    =   5928
   ClientLeft      =   48
   ClientTop       =   612
   ClientWidth     =   7632
   FillColor       =   &H8000000F&
   FillStyle       =   0  'Solid
   Icon            =   "frmContactDetail.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5928
   ScaleWidth      =   7632
   StartUpPosition =   2  'CenterScreen
   Begin vsOcx6LibCtl.vsIndexTab vsIndexTab1 
      Height          =   5955
      Left            =   2640
      TabIndex        =   2
      Top             =   -120
      Width           =   4980
      _ExtentX        =   8784
      _ExtentY        =   10499
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   7.8
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   -1  'True
      Appearance      =   1
      MousePointer    =   0
      _ConvInfo       =   1
      Version         =   600
      BackColor       =   16777215
      ForeColor       =   -2147483630
      FrontTabColor   =   16777215
      BackTabColor    =   12632256
      TabOutlineColor =   0
      FrontTabForeColor=   -2147483630
      Caption         =   "&Summary|&All Fields"
      Align           =   0
      Appearance      =   1
      CurrTab         =   1
      FirstTab        =   0
      Style           =   5
      Position        =   1
      AutoSwitch      =   -1  'True
      AutoScroll      =   -1  'True
      TabPreview      =   -1  'True
      ShowFocusRect   =   -1  'True
      TabsPerPage     =   0
      BorderWidth     =   0
      BoldCurrent     =   -1  'True
      DogEars         =   -1  'True
      MultiRow        =   0   'False
      MultiRowOffset  =   200
      CaptionStyle    =   0
      TabHeight       =   1
      Begin VB.Frame fraAllFields 
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   0  'None
         Height          =   5880
         Left            =   36
         TabIndex        =   6
         Top             =   36
         Width           =   4908
         Begin TrueDBList60.TDBList lstContactDetail 
            Height          =   5820
            Left            =   105
            OleObjectBlob   =   "frmContactDetail.frx":058A
            TabIndex        =   1
            Top             =   105
            Width           =   4800
         End
      End
      Begin VB.Frame fraSummary 
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   0  'None
         Height          =   5880
         Left            =   -5424
         TabIndex        =   3
         Top             =   36
         Width           =   4908
         Begin VB.TextBox txtSummary 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   0  'None
            Height          =   5000
            Left            =   555
            Locked          =   -1  'True
            MultiLine       =   -1  'True
            TabIndex        =   4
            Top             =   930
            Width           =   4170
         End
         Begin VB.Label lblAddressType 
            BackStyle       =   0  'Transparent
            DataSource      =   " "
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   7.8
               Charset         =   0
               Weight          =   400
               Underline       =   -1  'True
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   390
            Left            =   540
            TabIndex        =   5
            Top             =   510
            Width           =   4050
         End
      End
   End
   Begin VB.ListBox List1 
      BackColor       =   &H00E0E0E0&
      Height          =   5925
      IntegralHeight  =   0   'False
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   2640
   End
   Begin VB.Menu mnuFile 
      Caption         =   "&File"
      Begin VB.Menu mnuFile_RestartCI 
         Caption         =   "&Restart CI"
      End
      Begin VB.Menu mnuFile_Sep1 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFile_Exit 
         Caption         =   "E&xit"
      End
   End
   Begin VB.Menu mnuView 
      Caption         =   "&View"
      Begin VB.Menu mnuView_Summary 
         Caption         =   "&Summary"
      End
      Begin VB.Menu mnuView_AllFields 
         Caption         =   "&All Fields"
      End
   End
   Begin VB.Menu mnuContacts 
      Caption         =   "&Contacts"
      Begin VB.Menu mnuContacts_Retrieve 
         Caption         =   "&Retrieve..."
      End
      Begin VB.Menu mnuContacts_Connect 
         Caption         =   "Co&nnect..."
      End
      Begin VB.Menu mnuContacts_Sep1 
         Caption         =   "-"
      End
      Begin VB.Menu mnuContacts_CreateDataFile 
         Caption         =   "&Create Data File..."
      End
      Begin VB.Menu mnuContacts_Sep2 
         Caption         =   "-"
      End
      Begin VB.Menu mnuContacts_RetrieveUNIDs 
         Caption         =   "Retrieve &UNIDs..."
      End
      Begin VB.Menu mnuContacts_RetrieveFromUNID 
         Caption         =   "Retrieve &From UNID..."
      End
      Begin VB.Menu mnuContacts_CustomFunction 
         Caption         =   "Custom &Function"
      End
   End
   Begin VB.Menu mnuMain 
      Caption         =   "Main"
      Visible         =   0   'False
      Begin VB.Menu mnuMain_Copy 
         Caption         =   "&Copy"
      End
   End
End
Attribute VB_Name = "frmContactDetail"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private m_oContacts As CIO.CContacts
Private m_oSession As CIX.CSession
Private g_oError As CIO.CError
Private g_oIni As CIO.CIni

Public Property Set Contacts(oNew As CIO.CContacts)
    On Error GoTo ProcError
    Set m_oContacts = oNew
    Exit Property
ProcError:
    g_oError.RaiseError "CIX.frmContactDetail.Contacts"
    Exit Property
End Property

Public Property Get Contacts() As CIO.CContacts
    Set Contacts = m_oContacts
End Property

Private Sub CreateDataFile()
    On Error GoTo ProcError
    If m_oSession Is Nothing Then
        Set m_oSession = New CSession
    End If

    If Not m_oSession.Initialized Then
        Err.Raise 100, "Could not initialize ci session"
    End If
    
    Dim oContacts As CIO.CContacts
    m_oSession.CreateDataFile g_oIni.ApplicationDirectory & "\ci.dat"

    Me.List1.Clear
    Me.txtSummary.Text = Empty
    
    Shell "notepad.exe """ & g_oIni.ApplicationDirectory & "\ci.dat" & """", vbNormalFocus
    Exit Sub
ProcError:
    g_oError.RaiseError "CITest.frmContactDetail.CreateDataFile"
    Exit Sub
End Sub

Private Sub GetContacts()
    Dim xDesc As String
    
    On Error GoTo ProcError
    If m_oSession Is Nothing Then
        On Error Resume Next
        Set m_oSession = New CSession
        xDesc = Err.Description
        On Error GoTo ProcError
        
        If m_oSession Is Nothing Then
            Err.Raise 91, , "Could not create the object CIX.CSession.  " & xDesc
        End If
    End If

    If Not m_oSession.Initialized Then
        Err.Raise 100, "Could not initialize ci session"
    End If
    
    m_oSession.SessionType = ciSession_CI
    
    Dim oContacts As CIO.CContacts
    Set oContacts = m_oSession.GetContacts(ciRetrieveData_All, _
        ciRetrieveData_None, ciRetrieveData_Names, ciRetrieveData_Names)

    Me.List1.Clear
    Me.txtSummary.Text = Empty

    If Not oContacts Is Nothing Then
        Set Me.Contacts = oContacts
        
        Dim i As Integer
        Dim xList As String
        Dim oC As CIO.CContact
        Static iList As CIX.ciSelectionLists
        
        For i = 1 To Me.Contacts.Count
            Set oC = Me.Contacts.Item(i)
            If i = 1 Then iList = 0
            If iList <> oC.ContactType Then
                iList = oC.ContactType
                Select Case iList
                    Case CIX.ciSelectionList_BCC
                        xList = "BCC"
                    Case CIX.ciSelectionList_CC
                        xList = "CC"
                    Case CIX.ciSelectionList_From
                        xList = "FROM"
                    Case CIX.ciSelectionList_To
                        xList = "TO"
                End Select
                
                Me.List1.AddItem xList
            End If
            
            With Me.List1
                .AddItem "    " & oC.DisplayName
                .ItemData(.ListCount - 1) = i
            End With
        Next i
        
        On Error Resume Next
        Me.List1.ListIndex = 0
    End If
    Exit Sub
ProcError:
    g_oError.RaiseError "CITest.frmContactDetail.GetContacts"
    Exit Sub
End Sub

Private Sub GetConnect()
    Dim xDesc As String
    Dim oArray As XArrayDB
    Dim i As Integer
    
    On Error GoTo ProcError
    If m_oSession Is Nothing Then
        On Error Resume Next
        Set m_oSession = New CSession
        xDesc = Err.Description
        On Error GoTo ProcError
        
        If m_oSession Is Nothing Then
            Err.Raise 91, , "Could not create the object CIX.CSession.  " & xDesc
        End If
    End If

    m_oSession.SessionType = ciSession_Connect

    If Not m_oSession.Initialized Then
        Err.Raise 100, "Could not initialize ci session"
    End If
    
    Dim oContacts As CIO.CContacts

        'Fill Array of entities
        Set oArray = New XArrayDB

        With oArray
            .ReDim 0, -1, 0, 4

'            For i = 0 To 25
'                .Insert 1, i
'                .Value(i, 0) = "Atty_" & i
'                .Value(i, 1) = 8
'                .Value(i, 2) = Empty
'                .Value(i, 3) = Empty
            .Insert 1, 0
            .Value(0, 0) = "Attorney"
            .Value(0, 1) = 6
            .Value(0, 2) = Empty
            .Value(0, 3) = 1
            .Value(0, 4) = "UNID"
            .Insert 1, 1
            .Value(1, 0) = "Counsel"
            .Value(1, 1) = 6
            .Value(1, 2) = Empty
            .Value(1, 3) = 2
            .Value(1, 4) = "UNID"
            .Insert 1, 2
            .Value(2, 0) = "Beneficiary 1"
            .Value(2, 1) = 18
            .Value(2, 2) = "Mike Conner"
            .Value(2, 3) = 3
            .Value(2, 4) = "UNID"

        End With

        Set oContacts = m_oSession.GetContacts_Custom(oArray:=oArray)

    Me.List1.Clear
    Me.txtSummary.Text = Empty

    If Not oContacts Is Nothing Then
        Set Me.Contacts = oContacts
        
        Dim xList As String
        Dim oC As CIO.CContact
        Static iList As CIX.ciSelectionLists
        
        For i = 1 To Me.Contacts.Count
            Set oC = Me.Contacts.Item(i)
            If i = 1 Then iList = 0
            If iList <> oC.ContactType Then
                iList = oC.ContactType
                Select Case iList
                    Case CIX.ciSelectionList_BCC
                        xList = "BCC"
                    Case CIX.ciSelectionList_CC
                        xList = "CC"
                    Case CIX.ciSelectionList_From
                        xList = "FROM"
                    Case CIX.ciSelectionList_To
                        xList = "TO"
                    Case CIX.ciSelectionList_Custom
                        xList = "CUSTOM"
                End Select
                
                Me.List1.AddItem xList
            End If
            
            With Me.List1
                .AddItem "  " & oC.CustomTypeName & ": " & oC.DisplayName
                .ItemData(.ListCount - 1) = i
            End With
        Next i
        
        On Error Resume Next
        Me.List1.ListIndex = 0
    End If
    Exit Sub
ProcError:
    g_oError.RaiseError "CITest.frmContactDetail.GetConnect"
    Exit Sub
End Sub

Private Sub GetContactFromUNID()
    Dim oContact As CIO.CContact
    
    On Error GoTo ProcError
    If m_oSession Is Nothing Then
        Set m_oSession = New CSession
    End If

    If Not m_oSession.Initialized Then
        Err.Raise 100, "Could not initialize ci session"
    End If
    
    'prompt for UNID
    Dim oContacts As CIO.CContacts
    Dim xUNID As String
    Dim oForm As frmUNID
    Set oForm = New frmUNID
    With oForm
        .Show vbModal, Me
        On Error Resume Next
        xUNID = oForm.Text1
        On Error GoTo ProcError
    End With
    
    If xUNID <> Empty Then
        Set oContact = m_oSession(xUNID, ciRetrieveData_All, ciAlert_None)
    End If
    
    Me.List1.Clear
    Me.txtSummary.Text = Empty

    If Not oContact Is Nothing Then
        Set Me.Contacts = New CIO.CContacts
        Me.Contacts.Add oContact
        
        Dim i As Integer
        Dim xList As String
        Dim oC As CIO.CContact
        Static iList As CIX.ciSelectionLists
        
        For i = 1 To Me.Contacts.Count
            Set oC = Me.Contacts.Item(i)
            If i = 1 Then iList = 0
            If iList <> oC.ContactType Then
                iList = oC.ContactType
                Select Case iList
                    Case CIX.ciSelectionList_BCC
                        xList = "BCC"
                    Case CIX.ciSelectionList_CC
                        xList = "CC"
                    Case CIX.ciSelectionList_From
                        xList = "FROM"
                    Case CIX.ciSelectionList_To
                        xList = "TO"
                End Select
                
                Me.List1.AddItem xList
            End If
            
            With Me.List1
                .AddItem "    " & oC.DisplayName
                .ItemData(.ListCount - 1) = i
            End With
        Next i
        
        Me.List1.ListIndex = 0
    End If
    Exit Sub
ProcError:
    g_oError.RaiseError "CITest.frmContactDetail.GetContactFromUNID"
    Exit Sub
End Sub

Private Sub RetrieveUNIDs()
    On Error GoTo ProcError
    If m_oSession Is Nothing Then
        Set m_oSession = New CSession
    End If

    If Not m_oSession.Initialized Then
        Err.Raise 100, "Could not initialize ci session"
    End If
    
    Dim oContacts As CIO.CContacts
    Set oContacts = m_oSession.GetContacts(ciRetrieveData_All, _
        ciRetrieveData_None, ciRetrieveData_None, ciRetrieveData_None, , , _
        ciAlert_MultipleEAddresses + ciAlert_MultipleFaxes + ciAlert_MultiplePhones)

    Me.List1.Clear
    Me.txtSummary.Text = Empty

    If Not oContacts Is Nothing Then
        Set Me.Contacts = oContacts
        
        Dim i As Integer
        Dim xList As String
        Dim oC As CIO.CContact
        Static iList As CIX.ciSelectionLists
        
        For i = 1 To Me.Contacts.Count
            Set oC = Me.Contacts.Item(i)
            If i = 1 Then iList = 0
            
            xList = xList & oC.UNID & vbCrLf & vbCrLf
            
            With Me.List1
                .AddItem "    " & oC.DisplayName
                .ItemData(.ListCount - 1) = i
            End With
        Next i
        Me.txtSummary.Text = xList
    End If
    Exit Sub
ProcError:
    g_oError.RaiseError "CITest.frmContactDetail.RetrieveUNIDs"
    Exit Sub
End Sub

Private Sub btnRetrieveContacts_Click()
    GetContacts
End Sub

Private Sub Form_Load()
    Set g_oError = New CIO.CError
    Set g_oIni = New CIO.CIni
    Me.mnuView_Summary.Checked = True
    Me.vsIndexTab1.CurrTab = 0
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set g_oError = Nothing
    Set g_oIni = Nothing
    Set frmContactDetail = Nothing
End Sub

Private Sub List1_Click()
    Dim oContact As CIO.CContact
    Dim iItem As Integer
    
    On Error GoTo ProcError
    With Me.List1
        If .Text = "TO" Or .Text = "FROM" Or .Text = "CC" Or .Text = "BCC" Then
            .ListIndex = .ListIndex + 1
            Exit Sub
        End If
        
        iItem = .ItemData(Me.List1.ListIndex)
        If iItem <> Empty Then
            Set oContact = m_oContacts.Item(iItem)
            If Not oContact Is Nothing Then
                GetSummary oContact
                GetDetail oContact
            End If
        End If
    End With
    Exit Sub
ProcError:
    g_oError.ShowError
    Exit Sub
End Sub

Private Sub lstContactDetail_GotFocus()
    'Me.lstContacts.SetFocus
End Sub

Private Sub lstContactDetail_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Me.lstContactDetail.SelectedItem = Me.lstContactDetail.RowContaining(Y)
    'Me.PopupMenu Me.mnuMain
End Sub

Function lCountChrs(xSource As String, xSearch As String) As Long
'returns the number of instances
'of xSearch in xSource

    Dim iPos As Integer
    Dim l As Long
    
    iPos = InStr(xSource, xSearch)
    While iPos
        l = l + 1
        iPos = InStr(iPos + 1, xSource, xSearch)
    Wend
    lCountChrs = l
End Function

Public Sub GetSummary(oContact As CIO.CContact)
'gets a summary string for the specified contact
    On Error GoTo ProcError
    Dim xToken As String
    
    xToken = "{<ADDRESSTYPENAME> Address}"
    
    Me.lblAddressType.Caption = oContact.GetDetail(xToken)
    
    If Me.lblAddressType.Caption = Empty Then
        Me.lblAddressType.Caption = "No Address Found"
    End If
    
    'xToken = "{<PREFIX> }{<FIRSTNAME> }{<MIDDLENAME> }{<LASTNAME> }{<SUFFIX>}" & vbCrLf
    xToken = "{<FULLNAME>}" & vbCrLf
    If InStr(UCase(oContact.AddressTypeName), "BUSINESS") Then
        xToken = xToken & "{<TITLE>}" & vbCrLf & _
            "{<COMPANY>}" & vbCrLf
    End If
        
    xToken = xToken & "{<STREET1>}" & vbCrLf & _
            "{<STREET2>}" & vbCrLf & _
            "{<STREET3>}" & vbCrLf & _
            "{<CITY>{, }<STATE>}{  <ZIPCODE>}"
            
    Me.txtSummary.Text = oContact.GetDetail(xToken)
    
    xToken = "Phone: {<PHONE>}" & vbCrLf & _
        "Fax: {<FAX>}" & vbCrLf & _
        "EMail: {<EMAILADDRESS>}"
        
    Me.txtSummary.Text = Me.txtSummary.Text & vbCrLf & vbCrLf & _
        vbCrLf & oContact.GetDetail(xToken) & vbCrLf & vbCrLf & _
        vbCrLf & "Full Detail:" & vbCrLf & vbCrLf & Replace(oContact.GetDetail("<FULLDETAIL>"), vbCr, vbCrLf)
    Exit Sub
ProcError:
    g_oError.RaiseError "CIX.frmContactDetail.GetSummary"
    Exit Sub
End Sub

Public Sub GetDetail(oContact As CIO.CContact)
'loads the field values for the specified contact
    Dim xTemp As String
    Dim oCustFld As CIO.CCustomField
    Dim i As Integer
    Dim xVal0 As String
    Dim oDetail As XArrayDB
    
    On Error GoTo ProcError
    
    On Error Resume Next
    With oContact
        'name/value array not yet built - do it
        On Error GoTo ProcError
        Set oDetail = New XArrayDB
        oDetail.ReDim 0, (25 + .CustomFields.Count), 0, 1
    
        oDetail(0, 0) = "Display Name"
        oDetail(1, 0) = "Prefix"
        oDetail(2, 0) = "First Name"
        oDetail(3, 0) = "Middle Name"
        oDetail(4, 0) = "Last Name"
        oDetail(5, 0) = "Suffix"
        oDetail(6, 0) = "Full Name"
        oDetail(7, 0) = "Initials"
        oDetail(8, 0) = "Salutation"
        oDetail(9, 0) = "Title"
        oDetail(10, 0) = "Department"
        oDetail(11, 0) = "Company"
        oDetail(12, 0) = "Address Type"
        oDetail(13, 0) = "Street1"
        oDetail(14, 0) = "Street2"
        oDetail(15, 0) = "Street3"
        oDetail(16, 0) = "Additional"
        oDetail(17, 0) = "City"
        oDetail(18, 0) = "State"
        oDetail(19, 0) = "Zip Code"
        oDetail(20, 0) = "Country"
        oDetail(21, 0) = "Phone"
        oDetail(22, 0) = "Phone Extension"
        oDetail(23, 0) = "Fax"
        oDetail(24, 0) = "EMail Address"
        oDetail(25, 0) = "FullDetail"
        
        oDetail(0, 1) = .DisplayName
        oDetail(1, 1) = .Prefix
        oDetail(2, 1) = .FirstName
        oDetail(3, 1) = .MiddleName
        oDetail(4, 1) = .LastName
        oDetail(5, 1) = .Suffix
        oDetail(6, 1) = .FullName
        oDetail(7, 1) = .Initials
        oDetail(8, 1) = .Salutation
        oDetail(9, 1) = .Title
        oDetail(10, 1) = .Department
        oDetail(11, 1) = .Company
        oDetail(12, 1) = .AddressTypeName
        oDetail(13, 1) = .Street1
        oDetail(14, 1) = .Street2
        oDetail(15, 1) = .Street3
        oDetail(16, 1) = .AdditionalInformation
        oDetail(17, 1) = .City
        oDetail(18, 1) = .State
        oDetail(19, 1) = .ZipCode
        oDetail(20, 1) = .Country
        oDetail(21, 1) = .Phone
        oDetail(22, 1) = .PhoneExtension
        oDetail(23, 1) = .Fax
        oDetail(24, 1) = .EMailAddress
        
        'reformat fulldetail string
        xTemp = Replace(.FullDetail, vbCr, "|")
        xTemp = Replace(xTemp, vbCrLf, "|")
        oDetail(25, 1) = xTemp
        
        For Each oCustFld In .CustomFields
            i = i + 1
            oDetail(25 + i, 0) = oCustFld.Name
            oDetail(25 + i, 1) = oCustFld.Value
        Next oCustFld
    End With

    Me.lstContactDetail.Array = oDetail
    Me.lstContactDetail.ReBind
    Exit Sub
ProcError:
    g_oError.RaiseError "CIX.frmContactDetail.GetDetail"
    Exit Sub
End Sub

Private Sub mnuContacts_CreateDataFile_Click()
    On Error GoTo ProcError
    CreateDataFile
    Exit Sub
ProcError:
    g_oError.RaiseError "CIClient.frmContactDetail.mnuContacts_CreateDataFile_Click"
    Exit Sub
End Sub

Private Sub mnuContacts_CustomFunction_Click()
    Dim xDesc As String
    
    On Error GoTo ProcError
    If m_oSession Is Nothing Then
        On Error Resume Next
        Set m_oSession = New CSession
        xDesc = Err.Description
        On Error GoTo ProcError
        
        If m_oSession Is Nothing Then
            Err.Raise 91, , "Could not create the object CIX.CSession.  " & xDesc
        End If
    End If

    If Not m_oSession.Initialized Then
        Err.Raise 100, "Could not initialize ci session"
    End If
    
    Dim oContacts As CIO.CContacts
    Set oContacts = m_oSession.GetContacts(ciRetrieveData_Faxes + ciRetrieveData_Phones, _
        ciRetrieveData_None, ciRetrieveData_Names, ciRetrieveData_Names)

    Me.List1.Clear
    Me.txtSummary.Text = Empty

    If Not oContacts Is Nothing Then
        Set Me.Contacts = oContacts
        
        Dim i As Integer
        Dim xList As String
        Dim oC As CIO.CContact
        Static iList As CIX.ciSelectionLists
        
        For i = 1 To Me.Contacts.Count
            Set oC = Me.Contacts.Item(i)
            If i = 1 Then iList = 0
            If iList <> oC.ContactType Then
                iList = oC.ContactType
                Select Case iList
                    Case CIX.ciSelectionList_BCC
                        xList = "BCC"
                    Case CIX.ciSelectionList_CC
                        xList = "CC"
                    Case CIX.ciSelectionList_From
                        xList = "FROM"
                    Case CIX.ciSelectionList_To
                        xList = "TO"
                End Select
                
                Me.List1.AddItem xList
            End If
            
            With Me.List1
                .AddItem "    " & oC.DisplayName
                .ItemData(.ListCount - 1) = i
            End With
        Next i
        
        On Error Resume Next
        Me.List1.ListIndex = 0
    End If
    Exit Sub
ProcError:
    g_oError.RaiseError "CITest.frmContactDetail.GetContacts"
    Exit Sub
End Sub

Private Sub mnuContacts_Retrieve_Click()
    On Error GoTo ProcError
    GetContacts
    Exit Sub
ProcError:
    g_oError.ShowError
    Exit Sub
End Sub

Private Sub mnuContacts_Connect_Click()
    On Error GoTo ProcError
    GetConnect
    Exit Sub
ProcError:
    g_oError.ShowError
    Exit Sub
End Sub

Private Sub mnuContacts_RetrieveFromUNID_Click()
    On Error GoTo ProcError
    GetContactFromUNID
    Exit Sub
ProcError:
    g_oError.ShowError
    Exit Sub
End Sub

Private Sub mnuContacts_RetrieveUNIDs_Click()
    On Error GoTo ProcError
    RetrieveUNIDs
    Exit Sub
ProcError:
    g_oError.ShowError
    Exit Sub
End Sub

Private Sub mnuFile_Exit_Click()
    Set m_oContacts = Nothing
    Set m_oSession = Nothing
    Unload Me
End Sub

Private Sub mnuFile_RestartCI_Click()
    Set m_oSession = New CIX.CSession
End Sub

Private Sub tabDetail_Click(PreviousTab As Integer)
    On Error Resume Next
    Me.List1.SetFocus
End Sub

Private Sub ClearContacts()
    Set m_oContacts = Nothing
End Sub

Private Sub mnuView_AllFields_Click()
    Me.vsIndexTab1.CurrTab = 1
    Me.mnuView_AllFields.Checked = True
    Me.mnuView_Summary.Checked = False
End Sub

Private Sub mnuView_Summary_Click()
    Me.vsIndexTab1.CurrTab = 0
    Me.mnuView_AllFields.Checked = False
    Me.mnuView_Summary.Checked = True
End Sub

