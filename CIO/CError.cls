VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CError"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private Declare Sub OutputDebugString Lib "kernel32" Alias "OutputDebugStringA" (ByVal lpOutputString As String)

Public Enum ciErrs
    ciErr_MissingOrInvalidINIKey = vbError + 512 + 1
    ciErr_InvalidUNID
    ciErr_InvalidFilterField
    ciErr_NotImplemented
    ciErr_BackendLogonCancelled
    ciErr_CouldNotSetFilterField
    ciErr_MissingFile
    ciErr_InvalidListing
    ciErr_InvalidUserINI
    ciErr_invalidAddress
    ciErr_CouldNotAddToCollection
    ciErr_InvalidContactNumberType
    ciErr_InvalidContactDetailFormatString
    ciErr_DefaultFolderDoesNotExist
    ciErr_NoConnectedBackends
    ciErr_InvalidColumnParameter
    ciErr_ODBCError
    ciErr_CouldNotConnectToBackend
    ciErr_CouldNotExecuteSQL
    ciErr_CouldNotBootBackend
    ciErr_CouldNotReadRegistry
    ciErr_AccessDenied
    ciErr_InvalidAddressIndex
End Enum

Public Enum ciDebugMessageTypes
    ciDebugMessageType_Info
    ciDebugMessageType_Warning
    ciDebugMessageType_Error
End Enum
    
Private m_iDebugMode As Integer

'**********************************************************
'initializer/terminator
'**********************************************************
Dim xObjectID As String
Private Sub Class_Initialize()
    Math.Randomize
    xObjectID = "CError" & Format(Now, "hhmmss") & Math.Rnd()
    g_oObjects.Add xObjectID, xObjectID
    DebugPrint "Added - " & xObjectID & ": " & g_oObjects.Count
End Sub

Private Sub Class_Terminate()
    g_oObjects.Remove xObjectID
    DebugPrint "Removed - " & xObjectID & ": " & g_oObjects.Count
End Sub
'**********************************************************

Public Property Get DebugMode() As Boolean
    On Error GoTo ProcError
    Const DEBUG_ON As Byte = 1
    Const DEBUG_OFF As Byte = 2
    
    Dim oIni As CIO.CIni
    
    If m_iDebugMode = Empty Then
        'get whether we're in debug mode or not
        Set oIni = New CIO.CIni
        m_iDebugMode = IIf(Trim$(UCase$(oIni.GetIni("CIApplication", "Debug"))) = _
            "TRUE", DEBUG_ON, DEBUG_OFF)
        Set oIni = Nothing
    End If
    
    DebugMode = (m_iDebugMode = DEBUG_ON)
    Exit Property
ProcError:
    RaiseError "CIO.CError.DebugMode"
    Exit Property
End Property

Public Sub SendToDebug(ByVal xMessage As String, ByVal xSource As String, Optional ByVal iMsgType As ciDebugMessageTypes = ciDebugMessageType_Info)
'sends the supplied message to debug if ci is in debug mode
    Dim iFile As Integer
    Dim xMsgType As String
    Dim oIni As New CIO.CIni
    
    On Error GoTo ProcError
    
    If DebugMode = True Then
        'send message to debug log
        iFile = FreeFile()
        Open oIni.ApplicationDirectory & "\ciDebug.log" For Append As #iFile
        
        If iMsgType = ciDebugMessageType_Warning Then
            'print warning
            Print #iFile, Format(Now(), "mm-dd-yy hh:mm:ss") & "  ***Warning***  " & xSource
        ElseIf iMsgType = ciDebugMessageType_Error Then
            'print error
            Print #iFile, Format(Now(), "mm-dd-yy hh:mm:ss") & "  ***ERROR***  " & xSource
        Else
            'print info
            Print #iFile, Format(Now(), "mm-dd-yy hh:mm:ss") & "  " & xSource
        End If
        
        Print #iFile, "    " & xMessage
        
        'print blank line
        Print #iFile, ""
        
        Close #iFile
    End If
    Exit Sub
ProcError:
    RaiseError "CIO.cError.SendToDebug"
    Exit Sub
End Sub

Public Sub RaiseError(ByVal xNewSource As String)
'raises the current error, appending the source
    With Err
        If InStr(.Source, ".") Then
'           an originating source has already been specified - keep it
            .Raise .Number, .Source, .Description
        Else
'           no originating source has been specified - use the one supplied
            .Raise .Number, xNewSource, .Description
        End If
    End With
End Sub

Sub ShowError()
    Dim lNum As Long
    Dim xDesc As String
    Dim xSource As String
    
    With Err
        lNum = .Number
        xDesc = .Description
        xSource = .Source
    End With
        
    SendToDebug "ERROR #" & lNum & " - " & xDesc, _
        xSource, ciDebugMessageType_Error
    
    MsgBox "The following unexpected error occurred: " & vbCrLf & vbCrLf & _
        "Number:  " & lNum & vbCr & _
        "Description:  " & xDesc & vbCr & _
        "Source:  " & xSource, vbExclamation, g_oSessionType.AppTitle
End Sub


