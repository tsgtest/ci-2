VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "XHelper"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Description = "APEX XArray helper"
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
'**********************************************************
'   XHelper Class
'   Contains properties and methods that
'   sort and find items in an XArray object
'**********************************************************

Option Explicit

'**********************************************************
'initializer/terminator
'**********************************************************
Dim xObjectID As String
Private Sub Class_Initialize()
    Math.Randomize
    xObjectID = "XHelper" & Format(Now, "hhmmss") & Math.Rnd()
    g_oObjects.Add xObjectID, xObjectID
    DebugPrint "Added - " & xObjectID & ": " & g_oObjects.Count
End Sub

Private Sub Class_Terminate()
    g_oObjects.Remove xObjectID
    DebugPrint "Removed - " & xObjectID & ": " & g_oObjects.Count
End Sub
'**********************************************************

Public Function FindFirst(vArray As XArrayDB, SearchString As Variant, Col As Integer, Optional bMatchStart As Boolean = False) As Long
    Dim lRow As Long, icol As Integer
    Dim lRowFound As Long
    
    lRowFound = -1
    For lRow = vArray.LowerBound(1) To vArray.UpperBound(1)
        '***9.6.2 - compare starting substring instead of entire string
        If bMatchStart Then
            If InStr(SearchString, vArray(lRow, Col)) = 1 Then
                lRowFound = lRow
                Exit For
            End If
        Else
            If vArray(lRow, Col) = SearchString Then
                lRowFound = lRow
                Exit For
            End If
        End If
    Next lRow
    FindFirst = lRowFound
End Function

Public Sub Sort(vArray As XArrayDB, SortCol As Integer, Optional Order As Variant)
    If IsMissing(Order) Then Order = "ASC"
    QuickSort vArray, vArray.LowerBound(1), vArray.UpperBound(1), SortCol, CStr(Order)
End Sub

Private Sub QuickSort(vArray As XArrayDB, ByVal First As Long, ByVal Last As Long, Col As Integer, Order As String)
    Dim PivotIndex As Long
    
    If First < Last Then
        PivotIndex = lPartition(vArray, First, Last, Col, Order)
        QuickSort vArray, First, PivotIndex - 1, Col, Order
        QuickSort vArray, PivotIndex, Last, Col, Order
    End If
End Sub

Private Function lPartition(vArray As XArrayDB, ByVal First As Long, ByVal Last As Long, Col As Integer, Order As String) As Long
    Dim Pivot As Variant, vTemp As Variant
    Dim i As Integer
    Dim LB As Integer, UB As Integer
    
    LB = vArray.LowerBound(2)
    UB = vArray.UpperBound(2)
    Pivot = vArray((First + Last) \ 2, Col)
    While First <= Last
        If UCase(Order) = "DESC" Then
            While vArray(First, Col) > Pivot
                First = First + 1
            Wend
            
            While vArray(Last, Col) < Pivot
                Last = Last - 1
            Wend
        Else
            While vArray(First, Col) < Pivot
                First = First + 1
            Wend
            
            While vArray(Last, Col) > Pivot
                Last = Last - 1
            Wend
        End If
        
        If First <= Last Then
            For i = LB To UB
                vTemp = vArray(First, i)
                vArray(First, i) = vArray(Last, i)
                vArray(Last, i) = vTemp
            Next i
            First = First + 1
            Last = Last - 1
        End If
    Wend
    lPartition = First
End Function
