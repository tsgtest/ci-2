VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CListings"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'**********************************************************
'   CListings Collection Class
'   created 12/22/00 by Daniel Fisherman
'   Contains properties and methods that manage the
'   collection of listings in a folder or store
'**********************************************************

'**********************************************************
Option Explicit

Private Enum ciListingCols
    ciListingCols_UNID = 0
    ciListingCols_LastName = 1
    ciListingCols_Company = 2
    ciListingCols_DisplayName = 3
    ciListingCols_Type = 4
End Enum

Event ListingAdded(oListing As CIO.CListing)
Event ListingDeleted()
Private m_oArray As xArray

'**********************************************************

Public Sub Add(oL As CIO.CListing)
    m_oArray.Insert 1, m_oArray.UpperBound(1) + 1
    With m_oArray
        .Value(.UpperBound(1), ciListingCols_DisplayName) = oL.DisplayName
        .Value(.UpperBound(1), ciListingCols_LastName) = oL.LastName
        .Value(.UpperBound(1), ciListingCols_UNID) = oL.UNID
        .Value(.UpperBound(1), ciListingCols_Company) = oL.Company
        .Value(.UpperBound(1), ciListingCols_Type) = oL.ListingType
    End With
    RaiseEvent ListingAdded(oL)
    Exit Function
ProcError:
    g_oError.RaiseError "CListings.Item"
End Sub

Public Sub DeleteAll()
    m_oArray.Clear
End Sub

Public Function Count() As Long
    On Error Resume Next
    Count = m_oArray.Count(1)
End Function

Public Function Item(Index As Long) As CIO.CListing
    Dim oL As CIO.CListing
    Set oL = New CIO.CListing
    
    On Error GoTo ProcError
    
    Index = ciMax(CDbl(Index), 0)
    With m_oArray
        On Error Resume Next
        oL.DisplayName = .Value(Index, ciListingCols_DisplayName)
        oL.UNID = .Value(Index, ciListingCols_ID)
        oL.Company = .Value(Index, ciListingCols_Company)
        oL.LastName = .Value(Index, ciListingCols_LastName)
        oL.ListingType = .Value(Index, ciListingCols_Type)
    End With
    
    Set Item = oL
    Exit Function
ProcError:
    g_oError.RaiseError "CListings.Item"
End Function

Public Property Let xArray(xarNew As xArray)
    Set m_oArray = xarNew
End Property

Public Property Get xArray() As xArray
    Set xArray = m_oArray
End Property

Private Sub Class_Initialize()
    Set m_oArray = New xArray
    
End Sub

Private Sub Class_Terminate()
    Set m_oArray = Nothing
End Sub


