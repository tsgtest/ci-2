VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CUNID"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'**********************************************************
'   CApplication CollectionClass
'   created 10/11/00 by Daniel Fisherman
'   Contains properties and methods
'   that manipulate the CI objects
'**********************************************************
Option Explicit

Public Enum ciUNIDFields
    ciUNIDFields_Backend = 0
    ciUNIDFields_Store = 1
    ciUNIDFields_Folder = 2
    ciUNIDFields_Listing = 3
    ciUNIDFields_Address = 4
    ciUNIDFields_ContactNumbers = 5
End Enum

Public Enum ciUNIDTypes
    ciUNIDType_Backend = 0
    ciUNIDType_Store = 1
    ciUNIDType_Folder = 2
    ciUNIDType_Listing = 3
    ciUNIDType_Address = 4
    ciUNIDType_ContactNumbers = 5
End Enum

'**********************************************************
'initializer/terminator
'**********************************************************
Dim xObjectID As String
Private Sub Class_Initialize()
    Math.Randomize
    xObjectID = "CUNID" & Format(Now, "hhmmss") & Math.Rnd()
    g_oObjects.Add xObjectID, xObjectID
    DebugPrint "Added - " & xObjectID & ": " & g_oObjects.Count
End Sub

Private Sub Class_Terminate()
    g_oObjects.Remove xObjectID
    DebugPrint "Removed - " & xObjectID & ": " & g_oObjects.Count
End Sub
'**********************************************************

Public Function GetStore(ByVal xUNID As String, Optional ByVal xName As String) As CIO.CStore
'returns the store corresponding to the specified UNID
    Dim oStore As CIO.CStore
    
    On Error GoTo ProcError
    
    'get the part of the UNID that constitutes
    'the UNID for an address
    xUNID = GetUNIDSubstring(xUNID, ciUNIDType_Store)
    
    Set oStore = New CIO.CStore
    oStore.UNID = xUNID
    If Len(xName) Then
        oStore.Name = xName
    End If
    Set GetStore = oStore
    Exit Function
ProcError:
    g_oError.RaiseError "CIO.CUNID.GetStore"
End Function

Public Function GetFolder(ByVal xUNID As String, Optional ByVal xName As String) As CIO.CFolder
'returns the folder corresponding to the specified UNID
    Dim oFolder As CIO.CFolder
    
    On Error GoTo ProcError
    
    'get the part of the UNID that constitutes
    'the UNID for a folder
    xUNID = GetUNIDSubstring(xUNID, ciUNIDType_Folder)
    
    Set oFolder = New CIO.CFolder
    oFolder.UNID = xUNID
    If Len(xName) Then
        oFolder.Name = xName
    End If
    Set GetFolder = oFolder
    Exit Function
ProcError:
    g_oError.RaiseError "CIO.CUNID.GetFolder"
End Function

Public Function GetListing(ByVal xUNID As String, Optional ByVal xDisplayName As String) As CIO.CListing
'returns the Listing corresponding to the specified UNID
    Dim oListing As CIO.CListing
    
    On Error GoTo ProcError
    
    'get the part of the UNID that constitutes
    'the UNID for a listing
    xUNID = GetUNIDSubstring(xUNID, ciUNIDType_Listing)
    Dim oUNID As CUNID

    Set oListing = New CIO.CListing
    oListing.UNID = xUNID
    If Len(xDisplayName) Then
        oListing.DisplayName = xDisplayName
    End If
    Set GetListing = oListing
    Exit Function
ProcError:
    g_oError.RaiseError "CIO.CUNID.GetListing"
End Function

Public Function GetAddress(ByVal xUNID As String, Optional ByVal xName As String) As CIO.CAddress
'returns the Address corresponding to the specified UNID
    Dim oAddress As CIO.CAddress
    
    On Error GoTo ProcError
    
    'get the part of the UNID that constitutes
    'the UNID for an address
    xUNID = GetUNIDSubstring(xUNID, ciUNIDType_Address)
    
    Set oAddress = New CIO.CAddress
    oAddress.UNID = xUNID
    If Len(xName) Then
        oAddress.Name = xName
    End If
    Set GetAddress = oAddress
    Exit Function
ProcError:
    g_oError.RaiseError "CIO.CUNID.GetAddress"
End Function
'
'Public Function GetContactNumbersUNID(ByVal xUNID As String) As String
''returns the Phone corresponding to the specified UNID
'    Dim xPhoneID As String
'    Dim iPos As Integer
'
'    On Error GoTo ProcError
'
'    'get the part of the UNID that constitutes
'    'the UNID for contact numbers
'    xUNID = GetUNIDSubstring(xUNID, ciUNIDType_ContactNumbers)
'
'    'parse up to first semi-colon - that's the phone number
'    iPos = InStr(xUNID, ";")
'    xPhoneID = Left$(xUNID, iPos - 1)
'
'    'parse number and type from string
'    iPos = InStr(xPhoneID, "|")
'    GetPhoneID = Left$(xPhoneID, iPos - 1)
'    xPhoneType = Mid$(xPhoneID, iPos + 1)
'    Exit Function
'ProcError:
'    g_oError.RaiseError "CIO.CUNID.GetPhoneID"
'End Function

Public Function GetUNIDField(ByVal xUNID As String, ByVal iField As ciUNIDFields)
    On Error GoTo ProcError
    GetUNIDField = mdlGeneral.GetUNIDField(xUNID, iField)
    Exit Function
ProcError:
    g_oError.RaiseError "CBackends.GetUNIDField"
End Function

Public Function GetUNIDType(ByVal xUNID As String) As ciUNIDTypes
'returns the type of UNID represented by xUNID
    Dim bytNumSep As Byte
    Dim oStr As CStrings
    Dim oConst As CConstants
    
    On Error GoTo ProcError
    
    Set oStr = New CStrings
    Set oConst = New CConstants
    
    'count the number of separators
    bytNumSep = oStr.lCountChrs(xUNID, oConst.UNIDSep)
    
    'number of separators matches type id
    GetUNIDType = bytNumSep
    
    Exit Function
ProcError:
    g_oError.RaiseError "CIO.CUNID.GetUNIDType"
End Function

Private Function GetUNIDSubstring(ByVal xUNID As String, ByVal iType As ciUNIDTypes) As String
'returns the substring of the supplied UNID that
'constitutes the UNID of the desired type
    Dim iPos1 As Integer
    Dim iPos2 As Integer
    Dim oConst As CConstants
    Dim xDesc As String
    Dim i As Integer
    Dim xClass As String
    Dim iUNIDType As ciUNIDTypes
    Dim iUNIDDifference As Integer
    
    On Error GoTo ProcError
    Set oConst = New CConstants
    
    iUNIDType = GetUNIDType(xUNID)
    
    If iUNIDType < iType Then
        'can't get substring- the supplied UNID
        'does not contain the specified unid type
        Err.Raise ciErr_InvalidUNID, , "Invalid UNID. The UNID " & _
            "supplied does not have contain the desired sub-UNID."
    End If
    
'   get class name represented by field number
    'remove 'b' if necessary
    If Left(xUNID, 1) = "b" Then
        xUNID = Mid(xUNID, 2)
    End If
    
    'get the number of additional separators that the
    'containing UNID has over the desired UNID -
    'we'll trim the unid string below by that number
    'of separators
    iUNIDDifference = iUNIDType - iType
    
    'get starting position - find appropriate separator
    For i = 1 To iType + 1
        iPos1 = InStr(IIf(iPos1 = 0, 1, iPos1 + Len(oConst.UNIDSep)), _
            xUNID, oConst.UNIDSep)
        If iPos1 = 0 Then
            'get the whole string - the loop hit
            'went past the last UNID separator
            GetUNIDSubstring = xUNID
            Exit Function
        End If
    Next i
    
    GetUNIDSubstring = Left$(xUNID, iPos1 - 1)
    Exit Function
ProcError:
    g_oError.RaiseError "CIO.CUNID.GetUNIDSubstring"
End Function

