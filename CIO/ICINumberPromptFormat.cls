VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ICINumberPromptFormat"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'**********************************************************
'   ICIPromptFormat Class
'   created 10/05/01 by Daniel Fisherman
'   Contains properties and methods that
'   defining characters of a ContactNumber prompt dialog
'**********************************************************
Option Explicit

Enum ciNumberPromptTypes
    ciNumberPromptType_Missing = 1
    ciNumberPromptType_Multiple = 2
End Enum

Public Property Let NumberType(iNew As ciContactNumberTypes)
End Property

Public Property Get AddressTypeHeading() As String
End Property

Public Property Get CategoryHeading() As String
End Property

Public Property Get DescriptionHeading() As String
End Property

Public Property Get NumberHeading() As String
End Property

Public Property Get ExtensionHeading() As String
End Property

Public Property Get AddressTypeWidth() As Single
End Property

Public Property Get CategoryWidth() As Single
End Property

Public Property Get DescriptionWidth() As Single
End Property

Public Property Get NumberWidth() As Single
End Property

Public Property Get ExtensionWidth() As Single
End Property

Public Property Get MissingNumbersDialogTitle() As String
End Property

Public Property Get MissingNumbersDialogDescriptionText() As String
End Property

Public Property Get MultipleNumbersDialogTitle() As String
End Property

Public Property Get MultipleNumbersDialogDescriptionText() As String
End Property

Public Property Let PromptType(iNew As ciNumberPromptTypes)
End Property

Public Property Get PromptType() As ciNumberPromptTypes
End Property

Public Property Let ContactName(ByVal xNew As String)
End Property
Public Property Get ContactName() As String

End Property

