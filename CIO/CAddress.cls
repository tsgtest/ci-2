VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CAddress"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'**********************************************************
'   CAddresses Class
'   created 12/25/00 by Daniel Fisherman
'   Contains properties and methods that
'   define a listing's address - not the
'   actual address but the type, e.g. home address
'**********************************************************

'**********************************************************
Dim m_vUNID As Variant
'Dim m_vTypeID As Variant
Dim m_xName As String

'**********************************************************
'initializer/terminator
'**********************************************************
Dim xObjectID As String
Private Sub Class_Initialize()
    Math.Randomize
    xObjectID = "CAddress" & Format(Now, "hhmmss") & Math.Rnd()
    g_oObjects.Add xObjectID, xObjectID
    DebugPrint "Added - " & xObjectID & ": " & g_oObjects.Count
End Sub

Private Sub Class_Terminate()
    g_oObjects.Remove xObjectID
    DebugPrint "Removed - " & xObjectID & ": " & g_oObjects.Count
End Sub
'**********************************************************

'**********************************************************
'   Properties
'**********************************************************
Public Property Get UNID() As Variant
    UNID = m_vUNID
End Property

Public Property Let UNID(ByVal vNew As Variant)
    m_vUNID = vNew
End Property

Public Property Get ID() As Variant
'returns the ID of the address -
    On Error GoTo ProcError
    ID = GetUNIDField(Me.UNID, ciUNIDFields_Address)
    Exit Property
ProcError:
    g_oError.RaiseError "CAddress.ID"
End Property

Public Property Let Name(xNew As String)
    m_xName = xNew
End Property

Public Property Get Name() As String
    Name = m_xName
End Property

'Public Property Let TypeID(ByVal vNew As Variant)
'    m_vTypeID = vNew
'End Property
'
'Public Property Get TypeID() As Variant
'    TypeID = m_vTypeID
'End Property
'
