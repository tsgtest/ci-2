VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CGlobals"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'**********************************************************
'initializer/terminator
'**********************************************************
Dim xObjectID As String
Private Sub Class_Initialize()
    Math.Randomize
    xObjectID = "CGlobals" & Format(Now, "hhmmss") & Math.Rnd()
    g_oObjects.Add xObjectID, xObjectID
    DebugPrint "Added - " & xObjectID & ": " & g_oObjects.Count
    Set g_oSessionType = New CIO.CSessionType
End Sub

Private Sub Class_Terminate()
    Set g_oEvents = Nothing
    Set g_oError = Nothing
    Set g_oSessionType = Nothing
    g_oObjects.Remove xObjectID
    DebugPrint "Removed - " & xObjectID & ": " & g_oObjects.Count
End Sub
'**********************************************************

Public Property Get Objects() As Collection
    Set Objects = g_oObjects
End Property

Public Function CIEvents() As CIO.CEventGenerator
'returns the one event generator object -
'creates it if necessary
    On Error GoTo ProcError
    If g_oEvents Is Nothing Then
        Set g_oEvents = New CIO.CEventGenerator
    End If
    Set CIEvents = g_oEvents
    Exit Function
ProcError:
    g_oError.RaiseError "CIO.CGlobals.CIEvents"
    Exit Function
End Function
