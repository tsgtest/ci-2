VERSION 5.00
Object = "{0D62353D-DBA2-11D1-B5DF-0060976089D0}#6.0#0"; "TDBL6.OCX"
Begin VB.Form frmContactNumbers 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "##"
   ClientHeight    =   3324
   ClientLeft      =   4920
   ClientTop       =   19752
   ClientWidth     =   6420
   Icon            =   "frmContactNumbers.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3324
   ScaleWidth      =   6420
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin TrueDBList60.TDBList lstPhones 
      Height          =   1830
      Left            =   150
      OleObjectBlob   =   "frmContactNumbers.frx":000C
      TabIndex        =   1
      Top             =   600
      Width           =   6165
   End
   Begin VB.CommandButton btnCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   400
      Left            =   5160
      TabIndex        =   4
      Top             =   2745
      Width           =   1100
   End
   Begin VB.CommandButton btnOK 
      Caption         =   "O&K"
      Default         =   -1  'True
      Height          =   400
      Left            =   3960
      TabIndex        =   3
      Top             =   2745
      Width           =   1100
   End
   Begin VB.CheckBox chkPromptForMissingPhones 
      Caption         =   "&Continue to prompt for contact numbers when necessary"
      Height          =   390
      Left            =   195
      TabIndex        =   2
      Top             =   2400
      Value           =   1  'Checked
      Visible         =   0   'False
      Width           =   4635
   End
   Begin VB.Label lblPhones 
      Caption         =   "##"
      Height          =   450
      Left            =   135
      TabIndex        =   0
      Top             =   135
      Width           =   6165
   End
End
Attribute VB_Name = "frmContactNumbers"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Enum PhoneCols
    PhoneCols_AddrType = 0
    PhoneCols_Category = 1
    PhoneCols_Description = 2
    PhoneCols_Number = 3
    PhoneCols_Extension = 4
End Enum

Private m_oArray As XArrayDB
Private m_bCancelled As Boolean

'**********************************************************
'initializer/terminator
'**********************************************************
Dim xObjectID As String
Private Sub Class_Initialize()
    Math.Randomize
    xObjectID = "frmContactNumbers" & VBA.Strings.Format(Now, "hhmmss") & Math.Rnd()
    g_oObjects.Add xObjectID, xObjectID
    DebugPrint "Added - " & xObjectID & ": " & g_oObjects.Count
End Sub

Private Sub Class_Terminate()
    g_oObjects.Remove xObjectID
    DebugPrint "Removed - " & xObjectID & ": " & g_oObjects.Count
End Sub
'**********************************************************

'**********************************************************
'   Properties
'**********************************************************
Property Set Format(oNew As ICINumberPromptFormat)
    With Me
        If oNew.PromptType = ciNumberPromptType_Missing Then
            .Caption = oNew.MissingNumbersDialogTitle
            .lblPhones.Caption = Replace(oNew.MissingNumbersDialogDescriptionText, "&", "&&")
        Else
            .Caption = oNew.MultipleNumbersDialogTitle
            .lblPhones.Caption = Replace(oNew.MultipleNumbersDialogDescriptionText, "&", "&&")
        End If
    End With
    
    With Me.lstPhones.Columns
        If oNew.AddressTypeWidth Then
            .Item(PhoneCols_AddrType).Caption = oNew.AddressTypeHeading
            .Item(PhoneCols_AddrType).Width = oNew.AddressTypeWidth
        Else
            .Item(PhoneCols_AddrType).Visible = False
        End If
        
        If oNew.CategoryWidth Then
            .Item(PhoneCols_Category).Caption = oNew.CategoryHeading
            .Item(PhoneCols_Category).Width = oNew.CategoryWidth
        Else
            .Item(PhoneCols_Category).Visible = False
        End If
        
        If oNew.DescriptionWidth Then
            .Item(PhoneCols_Description).Caption = oNew.DescriptionHeading
            .Item(PhoneCols_Description).Width = oNew.DescriptionWidth
        Else
            .Item(PhoneCols_Description).Visible = False
        End If
        
        If oNew.NumberWidth Then
            .Item(PhoneCols_Number).Caption = oNew.NumberHeading
            .Item(PhoneCols_Number).Width = oNew.NumberWidth
        Else
            .Item(PhoneCols_Number).Visible = False
        End If
        
        If oNew.ExtensionWidth Then
            .Item(PhoneCols_Extension).Caption = oNew.ExtensionHeading
            .Item(PhoneCols_Extension).Width = oNew.ExtensionWidth
        Else
            .Item(PhoneCols_Extension).Visible = False
        End If
    End With
End Property
Public Property Get Cancelled() As Boolean
    Cancelled = m_bCancelled
End Property

Public Property Set Source(oNew As CIO.CContactNumbers)
    Dim oContactNum As CIO.CContactNumber
    Dim i As Integer
    
    On Error GoTo ProcError
    Set m_oArray = New XArrayDB
    
    m_oArray.ReDim 0, oNew.Count - 1, 0, 4
    
    For Each oContactNum In oNew
        With m_oArray
            .Value(i, PhoneCols_Category) = oContactNum.Category
            .Value(i, PhoneCols_Description) = oContactNum.Description
            .Value(i, PhoneCols_Number) = oContactNum.Number
            .Value(i, PhoneCols_Extension) = oContactNum.Extension
        End With
        i = i + 1
    Next oContactNum
    
    Me.lstPhones.Array = m_oArray
    Me.lstPhones.ReBind
    Exit Property
ProcError:
    g_oError.RaiseError "CIO.frmPhones.Source"
    Exit Property
End Property

'**********************************************************
'   Methods
'**********************************************************
Public Sub SetMessage(ByVal xContactName As String, _
                      ByVal xAddrType As String, _
                      ByVal xPhoneType As String, _
                      Optional ByVal bPromptForMultiple As Boolean = False, _
                      Optional ByVal ShowAsEAddresses As Boolean = False)
'set the message that's displayed when form is visible-
'two types of prompts - missing phones/ multiple phones
    If ShowAsEAddresses Then
        Me.Caption = "Choose an E-Mail Address for " & xContactName
        If bPromptForMultiple Then
            Me.lblPhones.Caption = "&There exist multiple e-mail" & _
                   " addresses for this address.  " & _
                   "Please select an alternate:"
            Me.chkPromptForMissingPhones.Visible = False
        Else
            Me.lblPhones.Caption = "&No e-mail" & _
                   " address is associated with the selected address for " & xContactName & _
                   ".  Please select an alternate:"
            Me.chkPromptForMissingPhones.Visible = True
        End If
    Else
        Me.Caption = "Choose a " & xPhoneType & " Number for " & xContactName
        If bPromptForMultiple Then
            Me.lblPhones.Caption = "&There exist multiple " & LCase(xPhoneType) & _
                   " numbers for this address.  " & _
                   "Please select an alternate:"
            Me.chkPromptForMissingPhones.Visible = False
        Else
            Me.lblPhones.Caption = "&No " & LCase(xPhoneType) & _
                   " number is associated with the selected address for " & xContactName & _
                   ".  Please select an alternate:"
            Me.chkPromptForMissingPhones.Visible = True
        End If
    End If
End Sub

'**********************************************************
'   Event Procedures
'**********************************************************
Private Sub btnOK_Click()
    Me.Hide
    DoEvents
     m_bCancelled = False
End Sub

Private Sub btnCancel_Click()
    m_bCancelled = True
    Me.Hide
    DoEvents
End Sub

Private Sub Form_Activate()
'select first item in list if possible
    Dim i As Integer

    On Error GoTo ProcError
    If Not (m_oArray Is Nothing) Then
        If m_oArray.Count(1) > 0 Then
'           select first row
            With Me.lstPhones
'               select first row
                .Row = 0
                .SetFocus
            End With
        End If
    End If
    
'   check prompting checkbox
    Me.chkPromptForMissingPhones.Value = 1
    Exit Sub
ProcError:
    g_oError.RaiseError "CIO.frmContactNumbers.Form_Activate"
    Exit Sub
End Sub

Private Sub Form_Load()
    Dim oSessionType As New CIO.CSessionType
    
    
     m_bCancelled = True
End Sub

'Private Sub Form_Paint()
'    'Load icon
'    With g_oSessionType
'        If (.SessionType = ciSession_Connect) Then
'            .SetIcon Me.hWnd, "CONNECTICON", False
'        Else
'            .SetIcon Me.hWnd, "TSGICON", False
'        End If
'    End With
'End Sub

Private Sub lstPhones_DblClick()
    btnOK_Click
End Sub
