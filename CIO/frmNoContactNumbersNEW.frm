VERSION 5.00
Begin VB.Form frmAlertNoContactNumbersNEW 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "##"
   ClientHeight    =   2028
   ClientLeft      =   48
   ClientTop       =   432
   ClientWidth     =   6180
   Icon            =   "frmNoContactNumbersNEW.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2028
   ScaleWidth      =   6180
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton btnNo 
      Caption         =   "&No"
      Height          =   345
      Left            =   3390
      TabIndex        =   4
      Top             =   1575
      Width           =   1125
   End
   Begin VB.CheckBox chkPrompt 
      Caption         =   "&Continue to alert when no contact numbers are found."
      Height          =   336
      Left            =   936
      TabIndex        =   2
      Top             =   1176
      Value           =   1  'Checked
      Width           =   5184
   End
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      DrawStyle       =   5  'Transparent
      ForeColor       =   &H80000008&
      Height          =   450
      Left            =   285
      Picture         =   "frmNoContactNumbersNEW.frx":000C
      ScaleHeight     =   456
      ScaleWidth      =   432
      TabIndex        =   0
      TabStop         =   0   'False
      Top             =   285
      Width           =   435
   End
   Begin VB.CommandButton btnYes 
      Caption         =   "&Yes"
      Height          =   345
      Left            =   2115
      TabIndex        =   3
      Top             =   1575
      Width           =   1125
   End
   Begin VB.Label lblMessage 
      Caption         =   "##"
      Height          =   696
      Left            =   948
      TabIndex        =   1
      Top             =   312
      Width           =   5136
   End
End
Attribute VB_Name = "frmAlertNoContactNumbersNEW"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private m_CancelInsert As Boolean

'**********************************************************
'initializer/terminator
'**********************************************************
Dim xObjectID As String
Private Sub Class_Initialize()
    Math.Randomize
    xObjectID = "frmAlertNoContactNumbersNEW" & Format(Now, "hhmmss") & Math.Rnd()
    g_oObjects.Add xObjectID, xObjectID
    DebugPrint "Added - " & xObjectID & ": " & g_oObjects.Count
End Sub

Private Sub Class_Terminate()
    g_oObjects.Remove xObjectID
    DebugPrint "Removed - " & xObjectID & ": " & g_oObjects.Count
End Sub
'**********************************************************

Public Property Get CancelInsert() As Boolean
    CancelInsert = m_CancelInsert
End Property

Public Property Let CancelInsert(bNew As Boolean)
    m_CancelInsert = bNew
End Property

Private Sub btnNo_Click()
    Me.Hide
    DoEvents
    Me.CancelInsert = True
End Sub

Private Sub btnYes_Click()
    Me.Hide
    DoEvents
    Me.CancelInsert = False
End Sub

'Private Sub Form_Paint()
'    'Load icon
'    With g_oSessionType
'        If (.SessionType = ciSession_Connect) Then
'            .SetIcon Me.hWnd, "CONNECTICON", False
'        Else
'            .SetIcon Me.hWnd, "TSGICON", False
'        End If
'    End With
'End Sub
