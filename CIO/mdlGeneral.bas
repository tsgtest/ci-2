Attribute VB_Name = "mdlGeneral"
Option Explicit

Public g_oError As CIO.CError
Public g_oEvents As CIO.CEventGenerator
Public g_xUserIni As String
Public g_oSessionType As CIO.CSessionType
Public g_bAlertNoContactNumbers As Boolean

'Contact Prefix/Suffix options
Public g_oArrSuffixSeparatorExclusions As XArrayDB
Public g_oArrPrefix As XArrayDB
Public g_oArrSuffix As XArrayDB
Public g_oArrSuffixToExclude As XArrayDB
Public g_oArrPrefixToExclude As XArrayDB
Public g_xSuffixSeparator As String

Public g_oObjects As Collection

Sub Main()
    'initialize a new error and session
    Set g_oObjects = New Collection
    
    'set this once a session
    g_bAlertNoContactNumbers = True
    LoadPrefixSuffixOptions
End Sub

Function GetUNIDField(ByVal vUNID As Variant, ByVal iField As ciUNIDFields)
'returns the value of the specified UNID field
    Dim iPos1 As Integer
    Dim iPos2 As Integer
    Dim xDesc As String
    Dim i As Integer
    Dim xClass As String
    Dim oConst As CIO.CConstants
    
    Set oConst = New CIO.CConstants
    
    On Error GoTo ProcError
    
'   get class name represented by field number
    Select Case iField
        Case ciUNIDFields.ciUNIDFields_Address
            xClass = "CAddress"
        Case ciUNIDFields.ciUNIDFields_Backend
            xClass = "CBackend"
        Case ciUNIDFields.ciUNIDFields_ContactNumbers
            xClass = "CContactNumbers"
        Case ciUNIDFields.ciUNIDFields_Folder
            xClass = "CFolder"
        Case ciUNIDFields.ciUNIDFields_Listing
            xClass = "CListing"
        Case ciUNIDFields.ciUNIDFields_Store
            xClass = "CStore"
    End Select
            
    'remove 'b' if necessary
    If Left(vUNID, 1) = "b" Then
        vUNID = Mid(vUNID, 2)
    End If
    
    'get starting position - find appropriate separator
    For i = 1 To iField
        iPos1 = InStr(iPos1 + Len(oConst.UNIDSep), CStr(vUNID), oConst.UNIDSep)
        If iPos1 = 0 Then
            'something is wrong - all listing elements UNIDs
            'should have four elements separated by three separators
            xDesc = "Invalid " & xClass & " UNID: " & CStr(vUNID)
            Err.Raise ciErr_InvalidUNID, , xDesc
        End If
    Next i
    
    If iPos1 Then
        'starting position is after separator
        iPos1 = iPos1 + Len(oConst.UNIDSep)
        
        'get ending position
        On Error Resume Next
        iPos2 = InStr(iPos1, CStr(vUNID), oConst.UNIDSep)
    Else
        'starting position is the first character
        iPos1 = 1
        
        'get ending position
        On Error Resume Next
        iPos2 = InStr(CStr(vUNID), oConst.UNIDSep)
    End If
    
    On Error GoTo ProcError
    
    'if ipos2 is 0, this is the last field -
    'set position as the last char
    If iPos2 = 0 Then
        GetUNIDField = Mid(CStr(vUNID), iPos1)
    Else
        'get position before last separator
        GetUNIDField = Mid(CStr(vUNID), iPos1, iPos2 - iPos1)
    End If
        
    On Error Resume Next
    If GetUNIDField = Empty Then
        'something is wrong - this
        'should not be empty
        xDesc = "Invalid " & xClass & " UNID: " & CStr(vUNID)
        Err.Raise ciErr_InvalidUNID, , xDesc
    End If
    Exit Function
ProcError:
    g_oError.RaiseError "mdlGeneral.GetUNIDField"
End Function

Private Sub LoadPrefixSuffixOptions()
    Dim oIni As New CIO.CIni
    Dim i As Integer
    
'   Contact Prefix/Suffix logic
    g_xSuffixSeparator = oIni.GetIni("Contact Suffix Formats", "SuffixSeparator")
    If g_xSuffixSeparator = "" Then
        g_xSuffixSeparator = ", "
    Else
        ' Strip Pipe delimiters - allows specifying empty separator using ||
        g_xSuffixSeparator = Replace(g_xSuffixSeparator, "|", "")
    End If
    
    'Initialize global objects
    Set g_oArrSuffixSeparatorExclusions = New XArrayDB
    g_oArrSuffixSeparatorExclusions.ReDim -1, -1, 0, 0
    Set g_oArrPrefix = New XArrayDB
    g_oArrPrefix.ReDim -1, -1, 0, 0
    Set g_oArrSuffix = New XArrayDB
    g_oArrSuffix.ReDim -1, -1, 0, 0
    Set g_oArrSuffixToExclude = New XArrayDB
    g_oArrSuffixToExclude.ReDim -1, -1, 0, 0
    Set g_oArrPrefixToExclude = New XArrayDB
    g_oArrPrefixToExclude.ReDim -1, -1, 0, 0
    
    Dim aSuffixSeparatorExclusions() As String
    Dim aSuffixExclusions() As String
    Dim aSuffixExclusionPrefixes() As String
    Dim aPrefixExclusions() As String
    Dim aPrefixExclusionSuffixes() As String
    
    On Error GoTo LeaveSub
    'Suffixes that don't use separators
    aSuffixSeparatorExclusions = Split(oIni.GetIni("Contact Suffix Formats", "SeparatorExclusions"), ",")
    For i = 0 To UBound(aSuffixSeparatorExclusions)
        g_oArrSuffixSeparatorExclusions.AppendRows 1
        g_oArrSuffixSeparatorExclusions.Value(i, 0) = aSuffixSeparatorExclusions(i)
    Next i
    
    'Exclude these suffixes when Prefix is in corresponding list
    aSuffixExclusionPrefixes = Split(oIni.GetIni("Contact Suffix Exclusions", "Prefix"), ",")
    For i = 0 To UBound(aSuffixExclusionPrefixes)
        g_oArrPrefix.AppendRows 1
        g_oArrPrefix.Value(i, 0) = aSuffixExclusionPrefixes(i)
    Next i
    aSuffixExclusions = Split(oIni.GetIni("Contact Suffix Exclusions", "SuffixToExclude"), ",")
    For i = 0 To UBound(aSuffixExclusions)
        g_oArrSuffixToExclude.AppendRows 1
        g_oArrSuffixToExclude.Value(i, 0) = aSuffixExclusions(i)
    Next i
    
    'Exclude these prefixes when Suffix is in corresponding list
    aPrefixExclusionSuffixes = Split(oIni.GetIni("Contact Prefix Exclusions", "Suffix"), ",")
    For i = 0 To UBound(aPrefixExclusionSuffixes)
        g_oArrSuffix.AppendRows 1
        g_oArrSuffix.Value(i, 0) = aPrefixExclusionSuffixes(i)
    Next i
    aPrefixExclusions = Split(oIni.GetIni("Contact Prefix Exclusions", "PrefixToExclude"), ",")
    For i = 0 To UBound(aPrefixExclusions)
        g_oArrPrefixToExclude.AppendRows 1
        g_oArrPrefixToExclude.Value(i, 0) = aPrefixExclusions(i)
    Next i
LeaveSub:
    
End Sub


