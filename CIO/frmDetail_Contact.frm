VERSION 5.00
Begin VB.Form frmContactDetail 
   Appearance      =   0  'Flat
   BackColor       =   &H00C0FFFF&
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   3120
   ClientLeft      =   15
   ClientTop       =   15
   ClientWidth     =   5625
   ControlBox      =   0   'False
   FillColor       =   &H00C0FFFF&
   FillStyle       =   0  'Solid
   BeginProperty Font 
      Name            =   "MS Sans Serif"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3120
   ScaleWidth      =   5625
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton btnClose 
      BackColor       =   &H00C0FFFF&
      Cancel          =   -1  'True
      Caption         =   " x"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   285
      Left            =   5265
      MaskColor       =   &H00C0FFFF&
      TabIndex        =   1
      Top             =   60
      Width           =   300
   End
   Begin VB.Line Line1 
      X1              =   110
      X2              =   5550
      Y1              =   405
      Y2              =   405
   End
   Begin VB.Label lblHeading 
      BackStyle       =   0  'Transparent
      Caption         =   "##"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   225
      Left            =   135
      TabIndex        =   2
      Top             =   105
      Width           =   5025
   End
   Begin VB.Label lblDetail 
      BackStyle       =   0  'Transparent
      Caption         =   "##"
      Height          =   2490
      Left            =   225
      TabIndex        =   0
      Top             =   525
      Width           =   5310
   End
End
Attribute VB_Name = "frmContactDetail"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub btnClose_Click()
    Unload Me
End Sub
