VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CFilter"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Public Enum ciSearchOperators
    ciSearchOperator_Equals = 1
    ciSearchOperator_BeginsWith = 2
    ciSearchOperator_Contains = 4
End Enum

Private m_oFilterFields(1 To 4) As CIO.CFilterField
Private m_lSortCol As Long

'**********************************************************
'initializer/terminator
'**********************************************************
Dim xObjectID As String
Private Sub Class_Initialize()
    Dim i As Integer
    For i = 1 To 4
        Set m_oFilterFields(i) = New CIO.CFilterField
    Next i
    Math.Randomize
    xObjectID = "CFilter" & Format(Now, "hhmmss") & Math.Rnd()
    g_oObjects.Add xObjectID, xObjectID
    DebugPrint "Added - " & xObjectID & ": " & g_oObjects.Count
End Sub

Private Sub Class_Terminate()
    Dim i As Integer
    For i = 1 To 4
        Set m_oFilterFields(i) = Nothing
    Next i
    g_oObjects.Remove xObjectID
    DebugPrint "Removed - " & xObjectID & ": " & g_oObjects.Count
End Sub
'**********************************************************

Public Function IsEmpty() As Boolean
'returns TRUE if no values have been set in the filter
    Dim i As Integer
    
    'cycle through filter fields
    For i = 1 To 4
        If FilterFields(i).Value <> Empty Then
            'filter is not empty - exit function
            IsEmpty = False
            Exit Function
        End If
    Next i
    
    'if we got here, no values have been set
    IsEmpty = True
End Function

Public Function Reset() As Boolean
'clears values in the filter
    Dim i As Integer
    
    'cycle through filter fields
    For i = 1 To 4
        FilterFields(i).Value = Empty
    Next i
End Function

Public Function FilterFields(iIndex As Integer) As CIO.CFilterField
    On Error GoTo ProcError

    If iIndex < 1 Or iIndex > 4 Then
        Err.Raise 9, , "Subscript out of range."
        Exit Function
    End If

    Set FilterFields = m_oFilterFields(iIndex)
    Exit Function
ProcError:
    Err.Raise Err.Number, "CIO.CFilter.FilterFields", Err.Description
End Function

Public Function CountFields() As Integer
'returns the number of filter fields specified
    Dim i As Integer
    
    For i = 1 To 4
        If FilterFields(i).ID <> Empty Then
            CountFields = CountFields + 1
        Else
            Exit For
        End If
    Next i
End Function

Public Property Let SortColumn(lNew As CIO.ciListingCols)
     m_lSortCol = lNew
End Property

Public Property Get SortColumn() As CIO.ciListingCols
     SortColumn = m_lSortCol
End Property
