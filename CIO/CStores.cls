VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CStores"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'**********************************************************
'   CStores Collection Class
'   created 12/19/00 by Daniel Fisherman
'   Contains properties and methods
'   that manage a collection of ci stores
'**********************************************************
Option Explicit

Private m_oCol As Collection

'**********************************************************
'initializer/terminator
'**********************************************************
Dim xObjectID As String
Private Sub Class_Initialize()
    Set m_oCol = New Collection
    Math.Randomize
    xObjectID = "CStores" & Format(Now, "hhmmss") & Math.Rnd()
    g_oObjects.Add xObjectID, xObjectID
    DebugPrint "Added - " & xObjectID & ": " & g_oObjects.Count
End Sub

Private Sub Class_Terminate()
    Set m_oCol = Nothing
    g_oObjects.Remove xObjectID
    DebugPrint "Removed - " & xObjectID & ": " & g_oObjects.Count
End Sub
'**********************************************************

Public Sub Add(oStore As CStore)
    Dim xDesc As String
    
    'add to collection
    On Error Resume Next
    Err.Clear
    m_oCol.Add oStore, oStore.UNID
    If Err Then
        'could not add to collection - alert and exit
        On Error GoTo ProcError
        xDesc = "Could not add store " & oStore.Name & _
            " to the collection of stores. " & Err.Description
        Err.Raise Err.Number, , xDesc
        Exit Sub
    End If
    Exit Sub
ProcError:
    g_oError.RaiseError "CIO.Stores.Add"
End Sub

Public Function Item(vIndex As Variant) As CIO.CStore
Attribute Item.VB_UserMemId = 0
'will return Nothing if an error is generated
    On Error Resume Next
    Set Item = m_oCol.Item(vIndex)
End Function

Public Function Count() As Integer
    Count = m_oCol.Count
End Function

Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
    Set NewEnum = m_oCol.[_NewEnum]
End Property


