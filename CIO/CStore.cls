VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CStore"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'**********************************************************
'   Store Class
'   created 12/20/00 by Daniel Fisherman
'   Contains properties and methods defining a
'   physical data store of a backend
'**********************************************************
Option Explicit

Private m_vUNID As Variant
Private m_xName As String

'**********************************************************
'initializer/terminator
'**********************************************************
Dim xObjectID As String
Private Sub Class_Initialize()
    Math.Randomize
    xObjectID = "CStore" & Format(Now, "hhmmss") & Math.Rnd()
    g_oObjects.Add xObjectID, xObjectID
    DebugPrint "Added - " & xObjectID & ": " & g_oObjects.Count
End Sub

Private Sub Class_Terminate()
    g_oObjects.Remove xObjectID
    DebugPrint "Removed - " & xObjectID & ": " & g_oObjects.Count
End Sub
'**********************************************************

Public Property Get UNID() As Variant
    UNID = m_vUNID
End Property

Public Property Let UNID(ByVal vNew As Variant)
    m_vUNID = vNew
End Property

Public Property Get BackendID() As Integer
    On Error GoTo ProcError
    BackendID = GetUNIDField(Me.UNID, ciUNIDFields_Backend)
    Exit Property
ProcError:
    g_oError.RaiseError "CListing.BackendID"
End Property

Public Property Get ID() As Variant
'returns the ID of the store -
'parses it from the UNID
    Dim iPos As Integer
    Dim oConst As CConstants
    
    On Error GoTo ProcError
    Set oConst = New CConstants
    
    iPos = InStr(CStr(m_vUNID), oConst.UNIDSep)
    
    If iPos Then
        ID = Mid(m_vUNID, iPos + Len(oConst.UNIDSep))
    Else
        'something is wrong - all store UNIDs
        'should have two elements separated by
        'a separator
        Dim xDesc As String
        xDesc = "Invalid Store UNID: " & CStr(m_vUNID)
        Err.Raise ciErr_InvalidUNID, , xDesc
    End If
    Exit Property
ProcError:
    g_oError.RaiseError "CStore.ID"
End Property

Public Property Get Name() As String
Attribute Name.VB_UserMemId = 0
    Name = m_xName
End Property

Public Property Let Name(ByVal xNew As String)
    m_xName = xNew
End Property
