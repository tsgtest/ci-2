VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CIni"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private Const CSIDL_LOCAL_APPDATA = &H1C&
Private Const CSIDL_FLAG_CREATE = &H8000&
Private Const CSIDL_COMMON_DOCUMENTS = &H2E

Private Const SHGFP_TYPE_CURRENT = 0
Private Const SHGFP_TYPE_DEFAULT = 1
Private Const MAX_PATH = 260

Private Declare Function SHGetFolderPath Lib "shfolder" _
    Alias "SHGetFolderPathA" _
    (ByVal hwndOwner As Long, ByVal nFolder As Long, _
    ByVal hToken As Long, ByVal dwFlags As Long, _
    ByVal pszPath As String) As Long

Private Declare Function GetPrivateProfileString Lib "kernel32" Alias _
    "GetPrivateProfileStringA" (ByVal lpApplicationName As String, _
    ByVal lpKeyName As Any, ByVal lpDefault As String, ByVal _
    lpReturnedString As String, ByVal nSize As Long, ByVal lpFileName _
    As String) As Long
    
Private Declare Function GetPrivateProfileSection Lib "kernel32" Alias _
    "GetPrivateProfileSectionA" (ByVal lpAppName As String, _
    ByVal lpReturnedString As String, ByVal nSize As Long, _
    ByVal lpFileName As String) As Long
    
Private Declare Function WritePrivateProfileString Lib "kernel32" Alias _
    "WritePrivateProfileStringA" (ByVal lpApplicationName As String, _
    ByVal lpKeyName As Any, ByVal lpString As Any, ByVal lpFileName As _
    String) As Long

Private Declare Function GetUserName Lib "advapi32.dll" Alias "GetUserNameA" ( _
    ByVal lpBuffer As String, nSize As Long) As Long
    
Private Const ciAppIni As String = "CI.ini"

'**********************************************************
'initializer/terminator
'**********************************************************
Dim xObjectID As String
Private Sub Class_Initialize()
    Math.Randomize
    xObjectID = "CIni" & Format(Now, "hhmmss") & Math.Rnd()
    g_oObjects.Add xObjectID, xObjectID
    DebugPrint "Added - " & xObjectID & ": " & g_oObjects.Count
End Sub

Private Sub Class_Terminate()
    g_oObjects.Remove xObjectID
    DebugPrint "Removed - " & xObjectID & ": " & g_oObjects.Count
End Sub
'**********************************************************

Public Function CIIni() As String
    CIIni = GetAppPath() & "\" & ciAppIni
End Function

Public Function ApplicationDirectory() As String
    ApplicationDirectory = GetAppPath()
End Function

Public Function ApplicationDirectoryMacPac() As String
    ApplicationDirectoryMacPac = GetMacPacAppPath()
End Function

Public Function ApplicationDirectoryMacPac10(Optional bIncludeWritable As Boolean = True) As String
    ApplicationDirectoryMacPac10 = GetMacPac10AppPath(bIncludeWritable)
End Function

Public Function IsForteLocal() As Boolean
    IsForteLocal = bIsForteLocal()
End Function

Public Function GetIni(xSection As String, _
                       xKey As String, _
                       Optional xINIPath As String) As String
    Dim xValue As String
    Dim xIni As String
    Dim iNullPos As Integer
    
    If xINIPath = "" Then
        xINIPath = CIIni
    End If
    
    xIni = xINIPath
    xValue = String(255, " ") & &O0
    
'   get requsestor from ini
    GetPrivateProfileString xSection, _
                            xKey, _
                            xValue, _
                            xValue, _
                            Len(xValue), _
                            xIni
                            
    If Left(xValue, 10) = String(10, " ") Then
        GetIni = ""
        Exit Function
    ElseIf xValue <> "" Then
        iNullPos = InStr(xValue, Chr(0))
    
        If iNullPos Then
            xValue = Left(xValue, iNullPos - 1)
        End If
    End If
    GetIni = xValue
End Function


Public Function SetIni(xSection As String, _
                       xKey As String, _
                       xValue As String, _
                       Optional xINIPath As String) As Boolean
    Dim xIni As String
    
    If xINIPath = "" Then
        xINIPath = GetAppPath() & "\" & ciAppIni
    End If
    
    xIni = xINIPath
    WritePrivateProfileString xSection, _
                              xKey, _
                              xValue, _
                              xIni

End Function

Public Function GetUserIni(ByVal xSection As String, _
                            ByVal xKey As String)
'returns the value for the specified key in the User.ini
    Dim xUserIni As String

    'get location of ci ini
    xUserIni = CIUserIni()

    If Len(Dir(xUserIni)) Then
        'user ini exists at specified location
        GetUserIni = GetIni(xSection, xKey, xUserIni)
    End If
End Function

Public Sub SetUserIni(ByVal xSection As String, _
                       ByVal xKey As String, _
                       ByVal xValue As String)
'sets the value of the specified key in User.ini
    Dim xUserIni As String
    
    'get location of ci ini
    xUserIni = CIUserIni()

    If Len(Dir(xUserIni)) Then
        SetIni xSection, xKey, xValue, xUserIni
    End If
End Sub

Public Function GetIniSection(ByVal xSection As String, _
                              Optional ByVal xIni As String) As Variant
'returns the value for the specified key in the
'specified ini. returns empty if
'no key exists.
    Dim xValue As String
    Dim iNullPos As Integer
    Dim i As Integer
    
    If xIni = "" Then
        xIni = GetAppPath() & "\" & ciAppIni
    End If
    
    xValue = String(1024, " ") & &O0
    
'   get requestor from ini
    GetPrivateProfileSection xSection, _
                             xValue, _
                             Len(xValue), _
                             xIni
                            
    If Left(xValue, 10) = String(10, " ") Then
        GetIniSection = ""
        Exit Function
    ElseIf xValue <> Empty Then
        Do
            i = iNullPos
            iNullPos = InStr(iNullPos + 1, xValue, Chr(0))
        Loop While iNullPos
        If i Then
            xValue = Left(xValue, i - 1)
        End If
    End If
    GetIniSection = xValue
End Function

Public Sub DeleteIniKey(xSection As String, _
                        xKey As String, _
                        xIni As String)
'deletes the specified key
'in the specified ini.
    WritePrivateProfileString xSection, _
                              xKey, _
                              vbNullString, _
                              xIni
End Sub

Public Function GetUserVarPath(ByVal xPath As String) As String
'substitutes user path for <UserName>
    Dim xBuf As String
    
    On Error GoTo ProcError
    
'   return supplied path if no variable is present
    If InStr(UCase(xPath), UCase("<UserName>")) = 0 Then
        GetUserVarPath = xPath
        Exit Function
    End If
    
'   get logon name
    xBuf = String(255, " ")
    GetUserName xBuf, 255
    
    If Len(xBuf) = 0 Then
'       alert to no logon name
        Err.Raise 5, , _
            "UserName is empty.  You might not be logged on to the system. " & _
                "Please log off, and log on again."
    End If
    
'   trim extraneous buffer chars
    Trim (xBuf)
    xBuf = RTrim(xBuf)
    xBuf = Left(xBuf, Len(xBuf) - 1)
    
'   substitute user name at specified point in path
    GetUserVarPath = Replace(xPath, "<UserName>", xBuf, , , vbTextCompare)
        
    Exit Function
ProcError:
    g_oError.RaiseError "CIO.CGlobals.GetUserVarPath"
End Function

Public Function MacPacUserIni() As String
    Dim xPersonalFiles As String

    xPersonalFiles = MacPacUserFilesDir()
    
    If xPersonalFiles <> Empty Then
        On Error Resume Next
        MacPacUserIni = xPersonalFiles & "user.ini"
        On Error GoTo ProcError
    End If
    Exit Function
ProcError:
    g_oError.RaiseError "CIO.CIni.MacPacUserIni"
    Exit Function
End Function

Public Function MacPacUserFilesDir() As String
    Dim xPath As String
    
    On Error GoTo ProcError
    
    xPath = GetMacPacAppPath()
    
    On Error Resume Next
    MacPacUserFilesDir = GetIni("File Paths", "UserDir", xPath & "\MacPac.ini")
    'CI2.3.5006 - for compatibility with pre 9.7.1
    If MacPacUserFilesDir = "" Then
        MacPacUserFilesDir = GetIni("General", "UserDir", xPath & "\MacPac.ini")
    End If
    If Right$(MacPacUserFilesDir, 1) <> "\" And MacPacUserFilesDir <> Empty Then
        MacPacUserFilesDir = MacPacUserFilesDir & "\"
    End If
    Exit Function
ProcError:
    g_oError.RaiseError "CIO.CIni.MacPacUserIni"
    Exit Function
End Function

Public Function CIUserIni() As String
    Dim oFSO As Scripting.FileSystemObject
    Dim oFolder As Scripting.Folder
    Dim oFile As Scripting.File
    Dim xCacheFolder As String
    Dim lPos As Long
'returns the location of the user ini
    
    On Error GoTo ProcError
    If g_xUserIni = Empty Then
        'CI 2.3.5003
        g_xUserIni = GetIni("CIApplication", "UserDir", GetAppPath() & "\ci.ini")
        If g_xUserIni <> "" Then
            g_xUserIni = IIf(Right(g_xUserIni, 1) = "\", g_xUserIni & "ciUser.ini", g_xUserIni & "\ciUser.ini")
        Else
            g_xUserIni = GetIni("CIApplication", "UserINI", GetAppPath() & "\ci.ini")   'for back compatibility - pre 2.3.5003
        End If
        
        If g_xUserIni = Empty Then
            'use the macpac user.ini if possible
            g_xUserIni = MacPacUserIni()
            
            If g_xUserIni = Empty Then
                'assume it's in the same folder as this code
                g_xUserIni = GetAppPath() & "\ciUser.ini"
            End If
        Else
            'an ini was specified - deal with any tokens in path
            If (InStr(UCase(g_xUserIni), "<USERNAME>") > 0) Or _
                    (InStr(UCase(g_xUserIni), "<USER>") > 0) Then
                'contains the UserName or User token -
                'get path from token string
                g_xUserIni = GetUserVarPath(g_xUserIni)
            Else
                'contains an environment variable other than UserName-
                'get path from token string
                g_xUserIni = GetEnvironVarPath(g_xUserIni)
            End If
        End If
        
        'v2.4.5
        Dim xUserDir As String
        '11 is len(\ciUser.ini)
        xUserDir = Left(g_xUserIni, Len(g_xUserIni) - 11)
        
        'Test for files and copy if necessary
        Set oFSO = New Scripting.FileSystemObject
        If (Not oFSO.FileExists(xUserDir & "\ci.mdb")) Or _
        (Not oFSO.FileExists(g_xUserIni)) Then
            'attempt to copy personal files from cache folder
            lPos = InStrRev(App.Path, "\")
            xCacheFolder = Left$(App.Path, lPos) & "Tools\UserCache"
            If oFSO.FolderExists(xCacheFolder) Then
                'cache folder exists - create specified directory if necessary
                bCreatePath xUserDir
                
                'if specified folder exists, copy personal files from cache;
                'don't overwrite existing
                If oFSO.FolderExists(xUserDir) Then
                    On Error Resume Next
                    For Each oFile In oFSO.GetFolder(xCacheFolder).Files
                        oFSO.CopyFile xCacheFolder & "\" & oFile.Name, _
                            xUserDir & "\" & oFile.Name, False
                    Next oFile
                    On Error GoTo ProcError
                End If
            End If
        End If
    
        If Dir(g_xUserIni) = Empty Then
            'ini doesn't exist at specified location
            Err.Raise ciErrs.ciErr_InvalidUserINI, , _
                "Invalid user ini file specified in ci.ini: " & vbCrLf & g_xUserIni
        End If
    End If
    CIUserIni = g_xUserIni
    Exit Function
ProcError:
    g_oError.RaiseError "CIO.CIni.CIUserIni"
    Exit Function
End Function

Public Function GetEnvironVarPath(ByVal xPath As String) As String
'substitutes environment variable for <xToken>;
'if variable doesn't exist, returns path unchanged
    Dim xToken As String
    Dim iPosStart As Integer
    Dim iPosEnd As Integer
    Dim xValue As String
    Dim xDelimiter As String
    
    On Error GoTo ProcError
    iPosStart = InStr(xPath, "<")
    iPosEnd = InStr(xPath, ">")
    
    If (iPosStart > 0) And (iPosEnd > 0) Then
        xToken = Mid(xPath, iPosStart + 1, iPosEnd - iPosStart - 1)
        xValue = Environ(xToken)
    Else
    'GLOG : 4062
    'v2.6.2001
        xDelimiter = "%"
        iPosStart = InStr(xPath, xDelimiter)
        iPosEnd = InStr(Mid(xPath, iPosStart + 1, Len(xPath)), xDelimiter)
        If iPosEnd Then
            iPosEnd = iPosEnd + iPosStart
        End If
        If (iPosStart > 0) And (iPosEnd > 0) Then
            xToken = Mid(xPath, iPosStart + 1, iPosEnd - iPosStart - 1)
            xValue = Environ(xToken)
        End If
    End If
    
    If xValue <> "" Then
        If xDelimiter = "" Then
            GetEnvironVarPath = Replace(xPath, "<" & xToken & ">", xValue)
        Else
            GetEnvironVarPath = Replace(xPath, xDelimiter & xToken & xDelimiter, xValue)
        End If
    Else
        GetEnvironVarPath = xPath
    End If
    
    Exit Function
ProcError:
    Err.Raise Err.Number, "CIO.CIni.GetEnvironVarPath"
    Exit Function
End Function

'************PRIVATE*************8

Private Function GetAppPath() As String
'Subsitute for App.Path call: first looks in the registry for Public documents location
'If not found, then uses regular App.Path command

    Dim oReg As CRegistry
    Dim vPath As Variant
    Dim lRet As Long
    Dim xTemp As String
    Static xDir As String
    
    On Error GoTo ProcError
    Set oReg = New CRegistry
    'get registry key value
    vPath = String(255, Chr(32))
    lRet = oReg.GetValue(HKEY_LOCAL_MACHINE, "Software\The Sackett Group\CI", "DataDirectory", vPath)
    vPath = RTrim(vPath)
    
    If Len(vPath) <= 0 Then
    'try old registry key value
        vPath = String(255, Chr(32))
        lRet = oReg.GetValue(HKEY_LOCAL_MACHINE, "Software\Legal MacPac\9.x", "ContactIntegrationData", vPath)
        vPath = RTrim(vPath)
    End If
    
    'exists, so set path to value
    If Len(vPath) > 0 Then
'       substitute real path for user var if it exists in string
        If (InStr(UCase(vPath), "<USERNAME>") > 0) Then
    '       use API to get Windows user name
            xTemp = GetUserVarPath(CStr(vPath))
        ElseIf (InStr(UCase(vPath), "<COMMON_DOCUMENTS>") > 0) Then
            xTemp = GetCommonDocumentsPath(CStr(vPath))
        Else
    '       use environmental variable
            xTemp = GetEnvironVarPath(CStr(vPath))
        End If
    Else
        'registry value empty, so set path to App.Path
        xTemp = App.Path
    End If
    
'   raise error if path is invalid
    If Not DirExists(xTemp) Then
        MsgBox IIf(xTemp = "", "", xTemp & "\") & "CI.ini could not be found.  Please contact your administrator.", _
               vbExclamation, g_oSessionType.AppTitle
        Exit Function
    End If
    
'   store for future retrieval
    xDir = xTemp
    GetAppPath = xTemp
    Exit Function
ProcError:
    g_oError.RaiseError "CIO.CIni.GetAppPath"
    Exit Function
End Function

Private Function GetMacPacAppPath() As String
'Looks for MacPac App.Path

    Dim oReg As CRegistry
    Dim vPath As Variant
    Dim lRet As Long
    Dim xTemp As String
    Static xDir As String
    
    On Error GoTo ProcError
    Set oReg = New CRegistry
    'get registry key value
    vPath = String(255, Chr(32))
    lRet = oReg.GetValue(HKEY_LOCAL_MACHINE, "Software\The Sackett Group\MacPac 9.0", "DataDirectory", vPath)
    vPath = RTrim(vPath)
    
    If Len(vPath) <= 0 Then
        'try old registry key value
        vPath = String(255, Chr(32))
        lRet = oReg.GetValue(HKEY_LOCAL_MACHINE, "Software\Legal MacPac\9.x", "MacPacData", vPath)
        vPath = RTrim(vPath)
    End If
    
    'exists, so set path to value
    If Len(vPath) > 0 Then
'       substitute real path for user var if it exists in string
        If (InStr(UCase(vPath), "<USERNAME>") > 0) Then
    '       use API to get Windows user name
            xTemp = GetUserVarPath(CStr(vPath))
        ElseIf (InStr(UCase(vPath), "<COMMON_DOCUMENTS>") > 0) Then
            xTemp = GetCommonDocumentsPath(CStr(vPath))
        Else
    '       use environmental variable
            xTemp = GetEnvironVarPath(CStr(vPath))
        End If
    Else
        'registry value empty, so set path to MacPac's Application Path
        xTemp = oReg.GetDLLOCXPath("MacPac90.Application")
    End If
    
'   raise error if path is invalid
    If Not DirExists(xTemp) Then
        MsgBox IIf(xTemp = "", "", xTemp & "\") & "CI.ini could not be found.  Please contact your administrator.", _
               vbExclamation, g_oSessionType.AppTitle
        Exit Function
    End If
    
'   store for future retrieval
    xDir = xTemp
    GetMacPacAppPath = xTemp
    Exit Function
ProcError:
    g_oError.RaiseError "CIO.CIni.GetMacPacAppPath"
    Exit Function
End Function

'GLOG : 8259 : ceh
Private Function bIsForteLocal() As Boolean
    Dim oReg As CRegistry
    Dim vPath As Variant
    Dim lRet As Long
    Dim bIsLocal As Boolean
    
    On Error GoTo ProcError
    Set oReg = New CRegistry
    
    vPath = String(255, Chr(32))
    lRet = oReg.GetValue(HKEY_LOCAL_MACHINE, "Software\The Sackett Group\Deca", "FullyLocal", vPath)
    vPath = RTrim(vPath)
            
    'exists, so set path to value
    If vPath = "1" Then
        bIsLocal = True
    End If
    
    bIsForteLocal = bIsLocal
    
    Exit Function
ProcError:
    g_oError.RaiseError "CIO.CIni.IsForteLocal"
    Exit Function
End Function

Private Function GetMacPac10AppPath(Optional IncludeWritable As Boolean = True) As String
'Looks for MacPac App.Path

    Dim oReg As CRegistry
    Dim vPath As Variant
    Dim lRet As Long
    Dim xTemp As String
    
    On Error GoTo ProcError
    Set oReg = New CRegistry
    
    If IncludeWritable Then
        'get registry key value
        'GLOG2 : 6732 : CEH
        vPath = String(255, Chr(32))
        lRet = oReg.GetValue(HKEY_LOCAL_MACHINE, "Software\The Sackett Group\Deca", "WritableDataDirectory", vPath)
        vPath = RTrim(vPath)
    End If
    
    If Len(vPath) <= 0 Then
        'try DataDirectory registry key value
        vPath = String(255, Chr(32))
        lRet = oReg.GetValue(HKEY_LOCAL_MACHINE, "Software\The Sackett Group\Deca", "DataDirectory", vPath)
        vPath = RTrim(vPath)
    
        If Len(vPath) <= 0 Then
            'try RootDirectory registry key value
            vPath = String(255, Chr(32))
            lRet = oReg.GetValue(HKEY_LOCAL_MACHINE, "Software\The Sackett Group\Deca", "RootDirectory", vPath)
            vPath = RTrim(vPath)
            If Len(vPath) > 0 Then
                vPath = vPath & "\Data"
            End If
        End If
    
    End If
        
    'exists, so set path to value
    If Len(vPath) > 0 Then
'       substitute real path for user var if it exists in string
        If (InStr(UCase(vPath), "<USERNAME>") > 0) Then
    '       use API to get Windows user name
            xTemp = GetUserVarPath(CStr(vPath))
        ElseIf (InStr(UCase(vPath), "<COMMON_DOCUMENTS>") > 0) Then
            xTemp = GetCommonDocumentsPath(CStr(vPath))
        Else
    '       use environmental variable
            xTemp = GetEnvironVarPath(CStr(vPath))
        End If
    End If
    
'   raise error if path is invalid
    If Not DirExists(xTemp) Then
        'GLOG : 7787 : ceh
        MsgBox IIf(xTemp = "", "", xTemp & "\") & " could not be found.  Please contact your administrator.", _
               vbExclamation, g_oSessionType.AppTitle
        xTemp = ""
        Exit Function
    End If
    
    GetMacPac10AppPath = xTemp
    Exit Function
ProcError:
    g_oError.RaiseError "CIO.CIni.GetMacPac10AppPath"
    Exit Function
End Function

Private Function GetCommonDocumentsPath(ByVal xPath As String) As String
'substitutes user path for <COMMON_DOCUMENTS>
    Dim xBuf As String
    
    On Error GoTo ProcError
    
'   get common documents folder
    xBuf = String(255, " ")
    
    SHGetFolderPath 0, CSIDL_COMMON_DOCUMENTS Or CSIDL_FLAG_CREATE, 0, SHGFP_TYPE_CURRENT, xBuf

    If Len(xBuf) = 0 Then
'       alert to no logon name
'       alert to no logon name
        Err.Raise 5, , _
            "Common_Documents is empty.  You might not be logged on to the system. " & _
                "Please log off, and log on again."
    End If
    
'   trim extraneous buffer chars
    xBuf = RTrim(xBuf)
    xBuf = Left(xBuf, Len(xBuf) - 1)
    
'   substitute common documents at specified point in path
    GetCommonDocumentsPath = Replace(xPath, "<COMMON_DOCUMENTS>", xBuf)
        
    Exit Function
ProcError:
    g_oError.RaiseError "CIO.CIni.GetCommonDocumentsPath"
    Exit Function
End Function


Private Function bCreatePath(xFullPath As String) As Boolean
    Dim oFSO As Scripting.FileSystemObject
    Dim xParent As String
    On Error GoTo ProcError
    Set oFSO = New Scripting.FileSystemObject
    If oFSO.FolderExists(xFullPath) Then  ' Folder already created
        bCreatePath = True
        Exit Function
    End If
    On Error Resume Next
    oFSO.CreateFolder xFullPath
    
    If Not oFSO.FolderExists(xFullPath) Then
        ' Couldn't create bottom level folder, call recursively using Parent Folder
        ' Only only the last level may created new
        xParent = oFSO.GetParentFolderName(xFullPath)
        If xParent = Empty Or Right(xParent, 1) = "\" Then ' Parent folder is Drive Root or nothing
            bCreatePath = False
        Else
            If bCreatePath(xParent) Then
                oFSO.CreateFolder xFullPath
                If oFSO.FolderExists(xFullPath) Then
                    bCreatePath = True
                End If
            End If
        End If
    Else
        bCreatePath = True
    End If
    Exit Function
ProcError:
    g_oError.RaiseError "CIO.CIni.bCreatePath"
End Function

