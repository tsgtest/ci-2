VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CListings"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'**********************************************************
'   CListings Collection Class
'   created 12/22/00 by Daniel Fisherman
'   Contains properties and methods that manage the
'   collection of listings in a folder or store
'**********************************************************

'**********************************************************
Option Explicit

Private Enum ciListingCols
    ciListingCols_DisplayName = 0
    ciListingCols_Company = 1
    ciListingCols_UNID = 2
    ciListingCols_LastName = 3
    ciListingCols_Type = 4
End Enum

Event ListingAdded(ByVal UNID As String, ByVal DisplayName As String, ByVal LastName As String, _
    ByVal FirstName As String, ByVal Company As String, ByVal ListingType As ciListingType)
Private m_aL() As Variant

'**********************************************************

Public Sub Add(ByVal UNID As String, ByVal DisplayName As String, ByVal LastName As String, _
    ByVal FirstName As String, ByVal Company As String, ByVal ListingType As ciListingType)

'adds listing to collection (array)
    Dim lUBound As Long

    If m_aL(0, 0) <> Empty Then
'       there is a first element in array -
'       add another element
        ReDim Preserve m_aL(5, UBound(m_aL, 2) + 1)
    End If
    
'   get new element
    lUBound = UBound(m_aL, 2)
    
'   fill element with listing detail
    m_aL(ciListingCols_DisplayName, lUBound) = DisplayName
    m_aL(ciListingCols_LastName, lUBound) = LastName
    m_aL(ciListingCols_UNID, lUBound) = UNID
    m_aL(ciListingCols_Company, lUBound) = Company
    m_aL(ciListingCols_Type, lUBound) = ListingType
    
    RaiseEvent ListingAdded(UNID, DisplayName, LastName, FirstName, Company, ListingType)
    Exit Sub
ProcError:
    g_oError.RaiseError "CListings.Add"
End Sub

Public Sub DeleteAll()
    ReDim m_aL(4, 0)
End Sub

Public Function Count() As Long
    On Error Resume Next
    Count = UBound(m_aL, 2)
End Function

Public Function Item(Index As Long) As CIO.CListing
Attribute Item.VB_UserMemId = 0
'retrieve listing specified by index
    Dim oL As CIO.CListing
    
    On Error GoTo ProcError
    
    If UBound(m_aL, 2) = 0 And m_aL(0, 0) = Empty Then
        'there are no items
        Exit Function
    Else
        'create listing
        Set oL = New CIO.CListing
        On Error Resume Next
        
        'fill listing
        With oL
            .DisplayName = m_aL(ciListingCols_DisplayName, Index)
            .UNID = m_aL(ciListingCols_UNID, Index)
            .Company = m_aL(ciListingCols_Company, Index)
            .LastName = m_aL(ciListingCols_LastName, Index)
            .ListingType = m_aL(ciListingCols_Type, Index)
        End With
    End If
    
    'return listing
    Set Item = oL
    Exit Function
ProcError:
    g_oError.RaiseError "CListings.Item"
End Function

Public Function ToArray() As Variant
'returns the collection as an array
    ToArray = m_aL
End Function

Private Sub Class_Initialize()
'   create initial array space
    ReDim m_aL(5, 0)
End Sub
