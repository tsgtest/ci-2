VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CFilterField"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

'**********************************************************
'   CFilterField Class
'   created 12/23/00 by Daniel Fisherman
'   Contains properties and methods defining
'   a filter field
'**********************************************************
    
'**********************************************************
Private m_vID As Variant
Private m_xName As String
Private m_vValue As Variant
Private m_iOp As ciSearchOperators
'**********************************************************

'**********************************************************
'initializer/terminator
'**********************************************************
Dim xObjectID As String
Private Sub Class_Initialize()
    Math.Randomize
    xObjectID = "CFilterField" & Format(Now, "hhmmss") & Math.Rnd()
    g_oObjects.Add xObjectID, xObjectID
    DebugPrint "Added - " & xObjectID & ": " & g_oObjects.Count
End Sub

Private Sub Class_Terminate()
    g_oObjects.Remove xObjectID
    DebugPrint "Removed - " & xObjectID & ": " & g_oObjects.Count
End Sub
'**********************************************************

'**********************************************************
'   Properties
'**********************************************************
Public Property Let ID(vNew As Variant)
    m_vID = vNew
End Property

Public Property Get ID() As Variant
    ID = m_vID
End Property

Public Property Let Value(vNew As Variant)
    m_vValue = vNew
End Property

Public Property Get Value() As Variant
    Value = m_vValue
End Property

Public Property Let Operator(iNew As ciSearchOperators)
    m_iOp = iNew
End Property

Public Property Get Operator() As ciSearchOperators
    Operator = m_iOp
End Property

Public Property Let Name(xNew As String)
    m_xName = xNew
End Property

Public Property Get Name() As String
    Name = m_xName
End Property
