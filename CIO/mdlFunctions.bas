Attribute VB_Name = "mdlFunctions"
Option Explicit

Private Declare Sub OutputDebugString Lib "kernel32" _
  Alias "OutputDebugStringA" _
  (ByVal lpOutputString As String)
 
Public Function DirExists(ByVal xDirName As String) As Boolean
    On Error Resume Next
    DirExists = (GetAttr(xDirName) And vbDirectory) = vbDirectory
    On Error GoTo 0
End Function
Public Sub DebugPrint(xOutput As String)
    Debug.Print xOutput
    OutputDebugString xOutput
End Sub


