VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CDebug"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'**********************************************************
'   CDebug Class
'   created 1/29/03 by Daniel Fisherman
'   Contains properties and methods of a CI contact
'**********************************************************

Option Explicit

'**********************************************************

Public Sub StartDebug()

End Sub

Public Sub EndDebug()

End Sub

Public Sub PrintMessage(ByVal xText As String, ByVal xSource As String)

End Sub

Public Sub ClearMessages()

End Sub

