VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CEventGenerator"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Public Event AfterListingAdded(ByVal lIndexAdded As Long, ByVal lTotal As Long, Cancel As Boolean)
Public Event BeforeStoreSearchFolderSearch(ByVal xFolderName As String, Cancel As Boolean)
Public Event BeforeListingsRetrieved(ByVal lTotal As Long, Cancel As Boolean)
Public Event AfterListingsRetrieved(ByVal lTotal As Long)
Public Event AfterStoreSearched()
Public Event BeforeExecutingListingsQuery()
Public Event AfterExecutingListingsQuery()
'Public Event ContactsRequested(oListing As CIO.CListing, oContacts As CIO.CContacts, oAddress As CAddress, ByVal vAddressType As Variant, ByVal iIncludeData As ciRetrieveData, ByVal iAlerts As ciAlerts)

'**********************************************************
'initializer/terminator
'**********************************************************
Dim xObjectID As String
Private Sub Class_Initialize()
    Math.Randomize
    xObjectID = "CEventGenerator" & Format(Now, "hhmmss") & Math.Rnd()
    g_oObjects.Add xObjectID, xObjectID
    DebugPrint "Added - " & xObjectID & ": " & g_oObjects.Count
End Sub

Private Sub Class_Terminate()
    g_oObjects.Remove xObjectID
    DebugPrint "Removed - " & xObjectID & ": " & g_oObjects.Count
End Sub
'**********************************************************

Public Sub RaiseAfterStoreSearched()
    RaiseEvent AfterStoreSearched
End Sub

Public Sub RaiseBeforeStoreSearchFolderSearch(ByVal xFolderName As String, Cancel As Boolean)
    RaiseEvent BeforeStoreSearchFolderSearch(xFolderName, Cancel)
End Sub

Public Sub RaiseAfterListingAdded(ByVal lIndexAdded As Long, ByVal lTotal As Long, Cancel As Boolean)
    RaiseEvent AfterListingAdded(lIndexAdded, lTotal, Cancel)
End Sub

Public Sub RaiseBeforeListingsRetrieved(ByVal lTotal As Long, Cancel As Boolean)
    RaiseEvent BeforeListingsRetrieved(lTotal, Cancel)
End Sub

Public Sub RaiseAfterListingsRetrieved(ByVal lTotal As Long)
    RaiseEvent AfterListingsRetrieved(lTotal)
End Sub

Public Sub RaiseBeforeExecutingListingsQuery()
    RaiseEvent BeforeExecutingListingsQuery
End Sub

Public Sub RaiseAfterExecutingListingsQuery()
    RaiseEvent AfterExecutingListingsQuery
End Sub
'
'Public Sub RaiseContactsRequested(oListing As CIO.CListing, oContacts As CIO.CContacts, oAddress As CAddress, Optional ByVal vAddressType As Variant, Optional ByVal iIncludeData As ciRetrieveData = 1&, Optional ByVal iAlerts As ciAlerts = 1&)
'    RaiseEvent ContactsRequested(oListing, oContacts, oAddress, vAddressType, iIncludeData, iAlerts)
'End Sub

