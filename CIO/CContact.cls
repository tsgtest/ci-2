VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CContact"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
'**********************************************************
'   CContact Class
'   created 12/19/00 by Daniel Fisherman
'   Contains properties and methods of a CI contact
'**********************************************************

Option Explicit

'**********************************************************
Private Const cioTagEmpty As String = "<EMPTY>"

Private m_vTag As Variant
Private m_iContactType As ciContactTypes
Private m_vAddressTypeID As Variant
Private m_xAddressTypeName As String
Private m_vAddressID As Variant

'universal id - unique contact id across all ci backends-
'describes the source location of the contact
Private m_vUNID As Variant

Private m_xDisplayName As String
Private m_xFullName As String
Private m_xFullNameWithSuffix As String
Private m_xFullNameWithPrefix As String
Private m_xFullNameWithPrefixAndSuffix As String
Private m_xPrefix As String
Private m_xSuffix As String
Private m_xFirstName As String
Private m_xLastName As String
Private m_xMiddleName As String
Private m_xInitials As String
Private m_xSalutation As String

Private m_xCompany As String
Private m_xDepartment As String
Private m_xTitle As String

Private m_xStreet1 As String
Private m_xStreet2 As String
Private m_xStreet3 As String
Private m_xAddInfo As String
Private m_xCity As String
Private m_xState As String
Private m_xZip As String
Private m_xCountry As String
Private m_vPhoneID As String
Private m_vFaxID As String
Private m_vGoesBy As String
Private m_vEAddressID As String
Private m_xPhone As String
Private m_xPhoneExt As String
Private m_xFax As String
Private m_xEMailAddress As String
Private m_oCustomFields As CIO.CCustomFields
Private m_xCustomTypeName As String
Private m_debugID As Long
Private m_xFullDetail As String
'**********************************************************
Private m_xCoreAddress As String

'**********************************************************
'initializer/terminator
'**********************************************************
Dim xObjectID As String
Private Sub Class_Initialize()
    Math.Randomize
    xObjectID = "CContact" & Format(Now, "hhmmss") & Math.Rnd()
    g_oObjects.Add xObjectID, xObjectID
    DebugPrint "Added - " & xObjectID & ": " & g_oObjects.Count
    Set m_oCustomFields = New CIO.CCustomFields
End Sub

Private Sub Class_Terminate()
    Set m_oCustomFields = Nothing
    g_oObjects.Remove xObjectID
    DebugPrint "Removed - " & xObjectID & ": " & g_oObjects.Count
End Sub
'**********************************************************

'**********************************************************
'   Properties
'**********************************************************
Public Property Let CoreAddress(ByVal xNew As String)
    If xNew <> m_xCoreAddress Then
        m_xCoreAddress = xNew
    End If
End Property
Public Property Get CoreAddress() As String
    CoreAddress = m_xCoreAddress
End Property
Public Property Let ContactType(iNew As ciContactTypes)
    m_iContactType = iNew
End Property

Public Property Get ContactType() As ciContactTypes
    ContactType = m_iContactType
End Property

Public Property Let AdditionalInformation(xNew As String)
    m_xAddInfo = xNew
End Property

Public Property Get AdditionalInformation() As String
    AdditionalInformation = m_xAddInfo
End Property

Public Property Let PhoneExtension(xNew As String)
    m_xPhoneExt = xNew
End Property

Public Property Get PhoneExtension() As String
    PhoneExtension = m_xPhoneExt
End Property

Public Property Let Salutation(xNew As String)
    m_xSalutation = xNew
End Property

Public Property Get Salutation() As String
    On Error GoTo ProcError
    
    If m_xSalutation = Empty Then
        'no salutation has been set, get the default salutation
        m_xSalutation = GetDefaultSalutation()
    End If
    
    Salutation = m_xSalutation
    Exit Property
ProcError:
    g_oError.RaiseError "CIO.CContact.Salutation"
    Exit Property
End Property

Private Function FormatDetail(xDetail As String, xDetailItem As String) As String
    Dim oIni As CIO.CIni
    Dim xFormat As String
    Dim xTemp As String
    Dim oStr As CStrings
    Dim lTempLen As Long
    Dim i As Integer
    Dim j As Integer
    Dim xTempArray() As String
    
    On Error GoTo ProcError
    
    Set oStr = New CStrings
    Set oIni = New CIni
    
    xFormat = oIni.GetIni("Formats", xDetailItem)
        
    If xFormat = Empty Then
    'no format has been set, return unformatted detail string
        xFormat = xDetail
    Else
        xTemp = oStr.StripNonNumeric(xDetail)
        
        'clean up format string
        'need to deal with phone extension?
        Select Case xDetailItem
            Case "Phone", "Fax"
                'deal with US like numbers with no area code
                If Len(xTemp) = 7 Then
                    For i = Len(xFormat) To 1 Step -1
                        If Mid(xFormat, i, 1) = "#" Then
                            j = j + 1
                        End If
                        If j = 7 Then
                            xFormat = Right(xFormat, (Len(xFormat) - i) + 1)
                            Exit For
                        End If
                    Next i
                ElseIf Len(xTemp) <> 10 Then
                    'add logic for larger & international numbers as well as phone extensions
                    'exit for now
                    FormatDetail = xDetail
                    Exit Function
                End If
            Case "Zip"
                If Len(xTemp) <= 5 Then
                    xFormat = Left(xFormat, Len(xTemp))
                ElseIf Len(xTemp) <> 9 Then
                    FormatDetail = xDetail
                    Exit Function
                End If
            Case Else
        End Select
        
        'replace format tokens
        For i = 1 To Len(xTemp)
            xFormat = oStr.xSubstituteFirst(xFormat, "#", Mid(xTemp, i, 1))
        Next i
        
    End If
    
    Set oIni = Nothing
    Set oStr = Nothing
    
    FormatDetail = xFormat
    
    Exit Function
ProcError:
    g_oError.RaiseError "CIO.CContact.FormatNumber"
    Exit Function
End Function


Private Function GetDefaultSalutation() As String
'returns the default salutation based on the default salutation
'token specified in ci.ini.  Returns first name if no token specified
    Static bRetrieved As Boolean
    Dim xToken As String
    
    On Error GoTo ProcError
    
    If Not bRetrieved Then
        Dim oIni As CIO.CIni
        Set oIni = New CIni
        
        xToken = oIni.GetIni("CIApplication", "DefaultSalutation")
        bRetrieved = True
    End If
        
    With Me
        If xToken = Empty Then
            xToken = .FirstName
        Else
            xToken = Replace(xToken, "<Prefix>", .Prefix)
            xToken = Replace(xToken, "<FirstName>", .FirstName)
            xToken = Replace(xToken, "<GoesBy>", .GoesBy)
            xToken = Replace(xToken, "<MiddleName>", .MiddleName)
            xToken = Replace(xToken, "<MiddleInitial>", Left$(.MiddleName, 1) & ".")
            xToken = Replace(xToken, "<LastName>", .LastName)
            xToken = Replace(xToken, "<Suffix>", .Suffix)
        End If
    End With
    
    GetDefaultSalutation = Trim$(xToken)
    Exit Function
ProcError:
    g_oError.RaiseError "CIO.CContact.GetDefaultSalutation"
    Exit Function
End Function

'Tag property is for internal use only
Public Property Let Tag(vNewTag As Variant)
    m_vTag = vNewTag
End Property

Public Property Get Tag() As Variant
Attribute Tag.VB_MemberFlags = "40"
    Tag = m_vTag
End Property

Public Property Let DisplayName(xNew As String)
    m_xDisplayName = xNew
End Property

Public Property Get DisplayName() As String
Attribute DisplayName.VB_UserMemId = 0
    DisplayName = m_xDisplayName
End Property

Public Property Let AddressTypeName(xNew As String)
    m_xAddressTypeName = xNew
End Property

Public Property Get AddressTypeName() As String
    AddressTypeName = m_xAddressTypeName
End Property

Public Property Let AddressTypeID(xNew As Variant)
    m_vAddressTypeID = xNew
End Property

Public Property Get AddressTypeID() As Variant
    AddressTypeID = m_vAddressTypeID
End Property

Public Property Let AddressID(vNew As Variant)
    m_vAddressID = vNew
End Property

Public Property Get AddressID() As Variant
    AddressID = m_vAddressID
End Property

Public Property Let PhoneID(vNew As Variant)
    m_vPhoneID = vNew
End Property

Public Property Get PhoneID() As Variant
    PhoneID = m_vPhoneID
End Property

Public Property Let FaxID(vNew As Variant)
    m_vFaxID = vNew
End Property

Public Property Get FaxID() As Variant
    FaxID = m_vFaxID
End Property

Public Property Let GoesBy(vNew As Variant)
    On Error Resume Next
    m_vGoesBy = vNew
End Property

Public Property Get GoesBy() As Variant
    GoesBy = m_vGoesBy
End Property

Public Property Let EAddressID(vNew As Variant)
    m_vEAddressID = vNew
End Property

Public Property Get EAddressID() As Variant
    EAddressID = m_vEAddressID
End Property

Property Get State() As String
    State = m_xState
End Property

Property Get Phone() As String
    Phone = m_xPhone
End Property

Property Get Street1() As String
    Street1 = m_xStreet1
End Property

Property Get Street2() As String
    Street2 = m_xStreet2
End Property

Property Get Street3() As String
    Street3 = m_xStreet3
End Property

Property Get ZipCode() As String
    ZipCode = m_xZip
End Property

Property Get Fax() As String
    Fax = m_xFax
End Property

Property Get Country() As String
    Country = m_xCountry
End Property

Property Let City(xNew As String)
    m_xCity = xNew
End Property

Property Let State(xNew As String)
    m_xState = xNew
End Property

Property Let Street1(xNew As String)
    m_xStreet1 = xNew
End Property

Property Let Street2(xNew As String)
    m_xStreet2 = xNew
End Property

Property Let Street3(xNew As String)
    m_xStreet3 = xNew
End Property

Property Let Fax(xNew As String)
    m_xFax = FormatDetail(xNew, "Fax")
End Property

Property Let Country(xNew As String)
    m_xCountry = xNew
End Property

Property Let ZipCode(xNew As String)
    m_xZip = FormatDetail(xNew, "Zip")
End Property

Property Let Phone(xNew As String)
    m_xPhone = FormatDetail(xNew, "Phone")
End Property

Property Get City() As String
    City = m_xCity
End Property

Public Property Get Company() As String
    Company = m_xCompany
End Property

Public Property Let Company(ByVal xNew As String)
    m_xCompany = xNew
End Property

Public Property Get Department() As String
    Department = m_xDepartment
End Property

Public Property Let Department(ByVal xNew As String)
    m_xDepartment = xNew
End Property

Public Property Get EMailAddress() As String
    EMailAddress = m_xEMailAddress
End Property

Public Property Let EMailAddress(ByVal xNew As String)
    m_xEMailAddress = xNew
End Property

Public Property Get FirstName() As String
    FirstName = m_xFirstName
End Property

Public Property Let FirstName(ByVal xNew As String)
    m_xFirstName = xNew
End Property

Public Property Get Prefix() As String
    Prefix = m_xPrefix
End Property

Public Property Let Prefix(ByVal xNew As String)
    m_xPrefix = xNew
End Property

Public Property Get Suffix() As String
    Suffix = m_xSuffix
End Property

Public Property Let Suffix(ByVal xNew As String)
    m_xSuffix = xNew
End Property

Public Property Get MiddleName() As String
    MiddleName = m_xMiddleName
End Property

Public Property Let MiddleName(ByVal xNew As String)
    m_xMiddleName = xNew
End Property

Public Property Get LastName() As String
    LastName = m_xLastName
End Property

Public Property Let LastName(ByVal xNew As String)
    m_xLastName = xNew
End Property

Public Property Get Title() As String
    Title = m_xTitle
End Property

Public Property Let Title(ByVal xNew As String)
    m_xTitle = xNew
End Property

Public Property Get Initials() As String
    Initials = m_xInitials
End Property

Public Property Let Initials(ByVal xNew As String)
    m_xInitials = xNew
End Property

Public Property Get UNID() As Variant
    UNID = m_vUNID
End Property

Public Property Let UNID(ByVal vNew As Variant)
    m_vUNID = vNew
End Property
Public Property Let FullName(ByVal xNew As String)
    m_xFullName = xNew
End Property

Public Property Get FullName() As String
    On Error GoTo ProcError
    If m_xFullName = Empty Then
        m_xFullName = GetFullName(False, False)
    End If
    FullName = m_xFullName
    Exit Property
ProcError:
    g_oError.RaiseError "CIO.CContact.FullName"
    Exit Property
End Property
Public Property Let FullNameWithSuffix(ByVal xNew As String)
    m_xFullNameWithSuffix = xNew
End Property

Public Property Get FullNameWithSuffix() As String
    
    On Error GoTo ProcError
    If m_xFullNameWithSuffix = Empty Then
        m_xFullNameWithSuffix = GetFullName(False, True)
    End If
    FullNameWithSuffix = m_xFullNameWithSuffix
    Exit Property
ProcError:
    g_oError.RaiseError "CIO.CContact.FullNameWithSuffix"
    Exit Property
End Property
Public Property Let FullNameWithPrefix(ByVal xNew As String)
    m_xFullNameWithPrefix = xNew
End Property

Public Property Get FullNameWithPrefix() As String
    On Error GoTo ProcError
    If m_xFullNameWithPrefix = Empty Then
        m_xFullNameWithPrefix = GetFullName(True, False)
    End If
    FullNameWithPrefix = m_xFullNameWithPrefix
    Exit Property
ProcError:
    g_oError.RaiseError "CIO.CContact.FullNameWithPrefix"
    Exit Property
End Property
Public Property Let FullNameWithPrefixAndSuffix(ByVal xNew As String)
    m_xFullNameWithPrefixAndSuffix = xNew
End Property

Public Property Get FullNameWithPrefixAndSuffix() As String
    On Error GoTo ProcError
    If m_xFullNameWithPrefixAndSuffix = Empty Then
        m_xFullNameWithPrefixAndSuffix = GetFullName(True, True)
    End If
    FullNameWithPrefixAndSuffix = m_xFullNameWithPrefixAndSuffix
    Exit Property
ProcError:
    g_oError.RaiseError "CIO.CContact.FullNameWithPrefixAndSuffix"
    Exit Property
End Property
'GLOG : 7794 : ceh
Public Property Get FullDetail() As String
    On Error GoTo ProcError
    If m_xFullDetail = Empty Then
        m_xFullDetail = GetFullDetail()
    End If
    FullDetail = m_xFullDetail
    Exit Property
ProcError:
    g_oError.RaiseError "CIO.CContact.FullDetail"
    Exit Property
End Property

Public Property Let FullDetail(ByVal xNew As String)
    m_xFullDetail = xNew
End Property

Public Property Get CustomTypeName() As String
    CustomTypeName = m_xCustomTypeName
End Property
Public Property Let CustomTypeName(ByVal xNew As String)
    m_xCustomTypeName = xNew
End Property
Public Function GetDetail(ByVal xFormatString As String, _
                          Optional ByVal bRemoveBlankLines As Boolean = True) As String
'returns the contact detail in the format specified
    Dim xTemp As String
    Dim oCustFld As CIO.CCustomField
    Dim i As Integer
    Dim xVal0 As String
    Static xFlds() As String
    
    On Error GoTo ProcError
    
    xTemp = xFormatString
    
    'test for existence of values
    'in the name/value array
    On Error Resume Next
    xVal0 = xFlds(0, 0)
    
    If xVal0 <> Empty Then
        GoTo skip
    End If
    
    With Me
        'If Err.Number > 0 Then
            'name/value array not yet built - do it
            On Error GoTo ProcError
            ReDim xFlds(33 + .CustomFields.Count, 1)
        
            xFlds(0, 0) = "<ADDITIONALINFORMATION>"
            xFlds(1, 0) = "<ADDRESSID>"
            xFlds(2, 0) = "<ADDRESSTYPENAME>"
            xFlds(3, 0) = "<CITY>"
            xFlds(4, 0) = "<COMPANY>"
            xFlds(5, 0) = "<COUNTRY>"
            xFlds(6, 0) = "<DEPARTMENT>"
            xFlds(7, 0) = "<DISPLAYNAME>"
            xFlds(8, 0) = "<EMAILADDRESS>"
            xFlds(9, 0) = "<FAX>"
            xFlds(10, 0) = "<FIRSTNAME>"
            xFlds(11, 0) = "<FULLNAME>"
            xFlds(12, 0) = "<FULLNAMEWITHPREFIX>"
            xFlds(13, 0) = "<FULLNAMEWITHSUFFIX>"
            xFlds(14, 0) = "<FULLNAMEWITHPREFIXANDSUFFIX>"
            xFlds(15, 0) = "<INITIALS>"
            xFlds(16, 0) = "<LASTNAME>"
            xFlds(17, 0) = "<MIDDLENAME>"
            xFlds(18, 0) = "<PHONE>"
            xFlds(19, 0) = "<PHONEEXTENSION>"
            xFlds(20, 0) = "<PREFIX>"
            xFlds(21, 0) = "<SALUTATION>"
            xFlds(22, 0) = "<STATE>"
            xFlds(23, 0) = "<STREET1>"
            xFlds(24, 0) = "<STREET2>"
            xFlds(25, 0) = "<STREET3>"
            xFlds(26, 0) = "<SUFFIX>"
            xFlds(27, 0) = "<TAG>"
            xFlds(28, 0) = "<TITLE>"
            xFlds(29, 0) = "<ZIPCODE>"
            xFlds(30, 0) = "<ZIP>"
            xFlds(31, 0) = "<STREET>"
            'v. 2.4.6002
            xFlds(32, 0) = "<COREADDRESS>"
            'v. 2.6.6005
	    'GLOG : 7794 : ceh
            xFlds(33, 0) = "<FULLDETAIL>"
            
            xFlds(0, 1) = .AdditionalInformation
            xFlds(1, 1) = .AddressID
            xFlds(2, 1) = .AddressTypeName
            xFlds(3, 1) = .City
            xFlds(4, 1) = .Company
            xFlds(5, 1) = .Country
            xFlds(6, 1) = .Department
            xFlds(7, 1) = .DisplayName
            xFlds(8, 1) = .EMailAddress
            xFlds(9, 1) = .Fax
            xFlds(10, 1) = .FirstName
            xFlds(11, 1) = .FullName
            xFlds(12, 1) = .FullNameWithPrefix
            xFlds(13, 1) = .FullNameWithSuffix
            xFlds(14, 1) = .FullNameWithPrefixAndSuffix
            xFlds(15, 1) = .Initials
            xFlds(16, 1) = .LastName
            xFlds(17, 1) = .MiddleName
            xFlds(18, 1) = .Phone
            xFlds(19, 1) = IIf(.PhoneExtension = Empty, .PhoneExtension, "x" & .PhoneExtension)
            xFlds(20, 1) = .Prefix
            xFlds(21, 1) = .Salutation
            xFlds(22, 1) = .State
            xFlds(23, 1) = .Street1
            xFlds(24, 1) = .Street2
            xFlds(25, 1) = .Street3
            xFlds(26, 1) = .Suffix
            xFlds(27, 1) = .Tag
            xFlds(28, 1) = .Title
            xFlds(29, 1) = .ZipCode
            xFlds(30, 1) = .ZipCode
            xFlds(31, 1) = .Street1 & vbCr & .Street2 & vbCr & .Street3
            xFlds(32, 1) = .CoreAddress
            xFlds(33, 1) = .FullDetail
            
            For Each oCustFld In .CustomFields
                i = i + 1
                xFlds(33 + i, 0) = "<" & UCase(oCustFld.Name) & ">"
                xFlds(33 + i, 1) = oCustFld.Value
            Next oCustFld
'        Else
'            On Error GoTo ProcError
'        End If
    End With

skip:
'   replace all detail fields with data
    Dim xReplacement As String
    For i = 0 To UBound(xFlds, 1)
        'if data is empty, replace with <EMPTY>-
        'this will be used later to delete blocks
        'specified to be deleted when the value of a
        'contact property is empty -
        'e.g. additional spaces, commas, etc.
        xReplacement = IIf(xFlds(i, 1) = Empty, cioTagEmpty, xFlds(i, 1))
        xTemp = Replace(xTemp, xFlds(i, 0), xReplacement)
    Next i
    
    xTemp = DeleteEMPTYBlocks(xTemp)
    
    'remove blank lines if specified
    If bRemoveBlankLines Then
        While InStr(xTemp, vbCr & vbCr) > 0
            xTemp = Replace(xTemp, vbCr & vbCr, vbCr)
        Wend
        While InStr(xTemp, vbCrLf & vbCrLf) > 0
            xTemp = Replace(xTemp, vbCrLf & vbCrLf, vbCrLf)
        Wend
        While InStr(xTemp, Chr(11) & Chr(11)) > 0
            xTemp = Replace(xTemp, Chr(11) & Chr(11), Chr(11))
        Wend
        While InStr(xTemp, vbCrLf & vbCr) > 0
            xTemp = Replace(xTemp, vbCrLf & vbCr, vbCrLf)
        Wend
        While InStr(xTemp, vbCrLf & Chr(11)) > 0
            xTemp = Replace(xTemp, vbCrLf & Chr(11), vbCrLf)
        Wend
        While InStr(xTemp, Chr(11) & vbCr) > 0
            xTemp = Replace(xTemp, Chr(11) & vbCr, vbCr)
        Wend
        While InStr(xTemp, Chr(11) & vbCrLf) > 0
            xTemp = Replace(xTemp, Chr(11) & vbCrLf, vbCrLf)
        Wend
        While InStr(xTemp, vbCr & Chr(11)) > 0
            xTemp = Replace(xTemp, vbCr & Chr(11), vbCr)
        Wend
        While InStr(xTemp, vbCr & vbCrLf) > 0
            xTemp = Replace(xTemp, vbCr & vbCrLf, vbCrLf)
        Wend
    End If
    
    GetDetail = xTemp
    Exit Function
ProcError:
    g_oError.RaiseError "CIO.CContact.GetDetail"
    Exit Function
End Function

Private Function DeleteEMPTYBlocks(ByVal xDetail As String) As String
'delete blocks defined by {} that contain <EMPTY> tags-
    Dim iPosTag As Integer
    Dim iPosBlockStart As Integer
    Dim iPosBlockEnd As Integer
    Dim iPosNestedStartBrace As Integer
    Dim iPosNestedEndBrace As Integer
    Dim xTemp As String
    Dim xNestedBraces As String
    Dim xBlock As String
    
    On Error GoTo ProcError
    xTemp = xDetail
    
    Do
        'search for <EMPTY> tag
        iPosTag = InStr(xTemp, cioTagEmpty)
        
        If iPosTag > 0 Then
            'empty tag exists - find surrounding
            'block start and end i.e.{}
            iPosBlockEnd = InStr(iPosTag, xTemp, "}", vbTextCompare)
            iPosBlockStart = InStrRev(xTemp, "{", iPosTag, vbTextCompare)
        
            If (iPosBlockStart > 0) And (iPosBlockEnd > 0) Then
                'there exists a block to delete -
                'get the block defined by the braces
                xBlock = Mid$(xTemp, iPosBlockStart + 1, iPosBlockEnd - iPosBlockStart - 1)
                
                'get positions of any nested braces in the block
                iPosNestedStartBrace = InStr(xBlock, "{")
                iPosNestedEndBrace = InStr(xBlock, "}")
                
                If iPosNestedStartBrace > 0 Then
                    'there is a nested start brace
                    If iPosNestedEndBrace > 0 Then
                        'there's also a nested end brace -
                        'see which is first
                        If iPosNestedEndBrace < iPosNestedStartBrace Then
                            'end brace before start brace
                            xNestedBraces = "}{"
                        Else
                            'start brace before end brace
                            xNestedBraces = "{}"
                        End If
                    Else
                        'no end brace, just start brace
                        xNestedBraces = "{"
                    End If
                ElseIf iPosNestedEndBrace > 0 Then
                    'end brace only
                    xNestedBraces = "}"
                Else
                    xNestedBraces = Empty
                End If
                        
                'delete the block, keeping any nested braces
                xTemp = Left$(xTemp, iPosBlockStart - 1) & _
                    xNestedBraces & Mid$(xTemp, iPosBlockEnd + 1)
            ElseIf (iPosBlockStart = 0) Or (iPosBlockEnd = 0) Then
                'no block exists - delete empty tag
                xTemp = Left$(xTemp, iPosTag - 1) & _
                    Mid$(xTemp, iPosTag + Len(cioTagEmpty))
            Else
'FIX: raise appropriate error
                Err.Raise "RAISE AN ERROR"
            End If
        End If
        'cycle until there are no more empty tags
    Loop Until (iPosTag = 0)
    
    'replace remaining braces - they're not needed
    'as they define blocks that don't contain empty data
    xTemp = Replace(xTemp, "{", "")
    xTemp = Replace(xTemp, "}", "")
    xTemp = Trim$(xTemp)
    DeleteEMPTYBlocks = xTemp
    Exit Function
ProcError:
    Err.Raise Err.Number, "CIO.CContact.DeleteEMPTYBlocks", Err.Description
    Exit Function
End Function

'Private Function Evaluate(ByVal xPhrase As String) As String
''returns the string to which xPhrase evaluates
'
''   get function name
'    If Left(xPhrase, 3) = "IF(" Then
'        Evaluate = EvaluateIF(xPhrase)
'    ElseIf InStr(xPhrase, "=") Then
'        Evaluate = EvaluateIdentity(xPhrase)
'    End If
'End Function

'Private Function EvaluateIdentity(ByVal xPhrase As String) As Boolean
''returns true if identity phrase evaluates to TRUE, else FALSE
'    Dim iPos As Integer
'    Dim xOperand1 As String
'    Dim xOperand2 As String
'
'    On Error GoTo ProcError
'
'    iPos = InStr(xPhrase, "=")
'
'    If iPos = 0 Then
''       not valid syntax - raise error
'        Err.Raise CIO.ciErr_InvalidContactDetailFormatString, , _
'            "Invalid syntax in phrase '" & xPhrase & "'."
'    End If
'
''   get operands
'    xOperand1 = Left(xPhrase, iPos - 1)
'    xOperand2 = Mid(xPhrase, iPos + 1)
'
''   TRUE is operands are identical strings
'    EvaluateIdentity = (xOperand1 = xOperand2)
'    Exit Function
'ProcError:
'    g_oError.RaiseError "CIO.CContact.EvaluateIdentity"
'    Exit Function
'End Function
'
'Private Function EvaluateIF(ByVal xPhrase As String) As String
''returns the string to which the supplied IF function evaluates
'    Dim xTemp As String
'    Dim bValid As Boolean
'    Dim xArgs() As String
'
''   test for proper syntax
'    On Error GoTo ProcError
'    bValid = Left(xPhrase, 3) = "IF(" And _
'        Right(xPhrase, 1) = ")"
'
'    If bValid Then
''       get args - trim off IF and ()
'        xTemp = Left(xPhrase, Len(xPhrase) - 1)
'        xTemp = Mid(xTemp, 4)
'
'        xArgs = Split(xTemp, ",")
'        bValid = UBound(xArgs) = 2
'    End If
'
'    If bValid Then
''       check for existence of each argument
'        bValid = xArgs(0) <> Empty And _
'            xArgs(1) <> Empty And _
'            xArgs(2) <> Empty
'    End If
'
'    If Not bValid Then
''       not valid syntax - raise error
'        Err.Raise CIO.ciErr_InvalidContactDetailFormatString, , _
'            "Invalid syntax in phrase '" & xPhrase & "'."
'    End If
'
''   evaluate arg 1
'    If Evaluate(xArgs(0)) = True Then
'        EvaluateIF = xArgs(1)
'    Else
'        EvaluateIF = xArgs(2)
'    End If
'    Exit Function
'ProcError:
'    g_oError.RaiseError "CIO.CContact.EvaluateIF"
'    Exit Function
'End Function

Public Property Set CustomFields(oNew As CIO.CCustomFields)
    Set m_oCustomFields = oNew
End Property

Public Property Get CustomFields() As CIO.CCustomFields
    Const mpThisFunction As String = "CIO.CContact.CustomFields"
    On Error GoTo ProcError
    If m_oCustomFields Is Nothing Then
        Set m_oCustomFields = New CIO.CCustomFields
    End If
    Set CustomFields = m_oCustomFields
    Exit Property
ProcError:
    g_oError.RaiseError mpThisFunction
    Exit Property
End Property

Private Function GetFullName(Optional bIncludePrefix = False, Optional bIncludeSuffix = False)
    Dim xName As String
    Dim oXHelp As XHelper
    Dim xTemp As String
    
    If Me.FirstName <> "" Then _
        xName = xName & Me.FirstName & " "
    If Me.MiddleName <> "" Then _
        xName = xName & Me.MiddleName & " "
    If Me.LastName <> "" Then _
        xName = xName & Me.LastName
    
    xName = Trim(xName)
    If xName = "" Then
        'Use Company if no name included
        GetFullName = Me.Company
        Exit Function
    End If
    
    Set oXHelp = New XHelper

'   handle Prefix
    If bIncludePrefix And Me.Prefix <> "" Then
        If bIncludeSuffix And Me.Suffix <> "" Then
            If oXHelp.FindFirst(g_oArrSuffix, Me.Suffix, 0) <> -1 Then     'suffix found
                If (g_oArrPrefixToExclude.Value(0, 0) <> "") And _
                (oXHelp.FindFirst(g_oArrPrefixToExclude, Me.Prefix, 0) = -1) Then      'prefix to exclude was not found
                    xName = Me.Prefix & " " & xName
                End If
            Else
                xName = Me.Prefix & " " & xName
            End If
        Else
            xName = Me.Prefix & " " & xName
        End If
    End If
    
'   handle Suffix
    If bIncludeSuffix And Me.Suffix <> "" Then
    
        If bIncludePrefix And Me.Prefix <> "" Then
            If oXHelp.FindFirst(g_oArrPrefix, Me.Prefix, 0) <> -1 Then     'prefix found
                If (g_oArrSuffixToExclude.Value(0, 0) <> "") And _
                (oXHelp.FindFirst(g_oArrSuffixToExclude, Me.Suffix, 0)) = -1 Then      'suffix to exclude was not found
                    xTemp = Me.Suffix
                End If
            Else
                xTemp = Me.Suffix
            End If
        Else
            xTemp = Me.Suffix
        End If

'       Suffix Separator
        If xTemp <> "" Then
            ' Check if Suffix matches an exclusion, or starts with
            ' an excluded item followed by a space or comma
            If oXHelp.FindFirst(g_oArrSuffixSeparatorExclusions, Me.Suffix, 0) <> -1 Or _
                oXHelp.FindFirst(g_oArrSuffixSeparatorExclusions, Me.Suffix & " ", 0, True) <> -1 Or _
                oXHelp.FindFirst(g_oArrSuffixSeparatorExclusions, Me.Suffix & ",", 0, True) <> -1 Then
                xName = xName & " " & xTemp
            Else
                ' ****9.6.2 - 3847
                ' Don't duplicate separator if it's already included
                ' at the start of the suffix
                If InStr(xTemp, g_xSuffixSeparator) = 1 Then
                    xName = xName & xTemp
                Else
                    xName = xName & g_xSuffixSeparator & xTemp
                End If
            End If
        End If
        
    End If
    GetFullName = xName

End Function

'GLOG : 7794 : ceh
Private Function GetFullDetail() As String
    Dim xDetail As String
    Dim xTemp As String
    Dim xCustomFormat As String
        
    'logic to find out if contact's country matches ini
    xCustomFormat = GetCustomFormat() 'get format from CI.ini
    
    If xCustomFormat <> Empty Then
        xDetail = Replace(GetDetail(xCustomFormat), "|", vbCr)
    Else
        'get default
        'JTS 1/21/16: Include Title and Company if used
        xDetail = Me.FullNameWithPrefixAndSuffix & vbCr & _
            IIf(Me.Title <> "", Me.Title & vbCr, "") & _
            IIf(Me.Company <> "", Me.Company & vbCr, "") & vbCr & Me.CoreAddress
    End If
    
    GetFullDetail = xDetail
End Function
'GLOG : 7794 : ceh
Private Function GetCustomFormat() As String
    Dim i As Integer
    Dim xFormatCountry As String
    Dim xToken As String
    Dim oIni As CIO.CIni

    On Error GoTo ProcError
    
    Set oIni = New CIni
    
    'cycle through Custom Formats until
    'country match is found.  If found,
    'return FullDetail token
    
    Do
        i = i + 1

        xFormatCountry = oIni.GetIni("CIApplication", "Format" & i & "Country")
        
        If xFormatCountry = "" Then
            Exit Function
        Else
            'JTS 1/21/16: Convert Unicode character tokens
            Dim iPos As Integer
            Dim iPos2 As Integer
            Dim xCode As String
            Dim lCode As Long
            Dim xCountryString As String
            On Error GoTo SkipConversion
            iPos = InStr(UCase(xFormatCountry), "CHRW(")
            While iPos > 0
                iPos2 = InStr(iPos, xFormatCountry, ")")
                If iPos2 > 0 Then
                    xCode = Mid(xFormatCountry, iPos + 5, iPos2 - (iPos + 5))
                    lCode = CLng(xCode)
                    xCountryString = xCountryString & ChrW(lCode)
                    iPos = InStr(iPos2, UCase(xFormatCountry), "CHRW(")
                End If
            Wend
            If xCountryString <> "" Then
                xFormatCountry = xCountryString
            End If
SkipConversion:
            If xFormatCountry = "" Then
                Exit Function
            ElseIf (UCase(xFormatCountry) = UCase(Me.Country)) Then
                xToken = oIni.GetIni("CIApplication", "Format" & i & "FullDetail")
            End If
        End If
        
    Loop While (xToken = "")
    
    GetCustomFormat = xToken
    
    Exit Function
ProcError:
    g_oError.RaiseError "CIO.CContact.GetCustomFormat"
End Function
