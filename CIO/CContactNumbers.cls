VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CContactNumbers"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'**********************************************************
'   CContactNumbers Collection Class
'   created 10/05/01 by Daniel Fisherman
'   Contains properties and methods that manage the
'   collection of contact numbers (i.e. phones/faxes/emails)
'   of a contact
'**********************************************************

Option Explicit

'**********************************************************
Private m_colContactNumbers As Collection
'**********************************************************

'**********************************************************
'initializer/terminator
'**********************************************************
Dim xObjectID As String
Private Sub Class_Initialize()
    Math.Randomize
    xObjectID = "CContactNumbers" & Format(Now, "hhmmss") & Math.Rnd()
    g_oObjects.Add xObjectID, xObjectID
    DebugPrint "Added - " & xObjectID & ": " & g_oObjects.Count
    Set m_colContactNumbers = New Collection
End Sub

Private Sub Class_Terminate()
    g_oObjects.Remove xObjectID
    DebugPrint "Removed - " & xObjectID & ": " & g_oObjects.Count
    Set m_colContactNumbers = Nothing
End Sub
'**********************************************************

Public Function Add(ByVal vID As Variant, ByVal xDescription As String, ByVal xNumber As String, _
                    ByVal xExtension As String, ByVal iType As ciContactNumberTypes, ByVal xCategory As String) As CIO.CContactNumber
'   add contact number to collection of numbers
    Dim oContactNumber As CIO.CContactNumber
    
    On Error GoTo ProcError

'   create new number
    Set oContactNumber = New CIO.CContactNumber
    
'   set properties of object
    With oContactNumber
        .Description = xDescription
        .Number = xNumber
        .Extension = xExtension
        .NumberType = iType
        .ID = vID
        .Category = xCategory
    End With
    
'   add to collection
    On Error Resume Next
    m_colContactNumbers.Add oContactNumber, CStr(vID)
    
    If Err Then
        On Error GoTo ProcError
        Err.Raise ciErr_CouldNotAddToCollection, , _
            "Could not add ContactNumber with Key Value '" & _
            iType & xDescription & "'"
    Else
        On Error GoTo ProcError
    End If
    
'   return
    Set Add = oContactNumber
    Exit Function
ProcError:
    g_oError.RaiseError "CIO.CContactNumbers.Add"
    Exit Function
End Function

Friend Function Delete(vKey As Variant)
    m_colContactNumbers.Remove vKey
End Function

Public Function Count() As Integer
    Count = m_colContactNumbers.Count
End Function

Public Function Item(vKey As Variant) As CIO.CContactNumber
    On Error Resume Next
    Set Item = m_colContactNumbers.Item(vKey)
End Function

Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
    Set NewEnum = m_colContactNumbers.[_NewEnum]
End Property

Public Function Prompt(oFormat As CIO.ICINumberPromptFormat) As CIO.CContactNumber
'   prompts user for a contact number - phone, fax, email
    Dim oForm As frmContactNumbers
    Dim oNum As CIO.CContactNumber
    
    On Error GoTo ProcError
    Set oForm = New frmContactNumbers
    With oForm
'       give contact numbers to form
        Set .Source = Me
        
'       set contact number type - phone, fax, email
        oFormat.NumberType = Me.Item(1).NumberType

'       set the format for the dialog box
        Set .Format = oFormat
        
'       show '&'
        oFormat.ContactName = Replace(oFormat.ContactName, "&", "&&")
        
        .Show vbModal
        
        If Not .Cancelled Then
'           get the selected contact number
            Set Prompt = Me.Item(.lstPhones.SelectedItem + 1)
        Else
            Set Prompt = Nothing
        End If
    End With
    
'   close form
    On Error Resume Next
    Set oForm = Nothing
    Exit Function
ProcError:
    g_oError.RaiseError "CIO.CContactNumbers.Prompt"
    Exit Function
End Function

Public Sub AlertNoneIfSpecified(ByVal iType As ciContactNumberTypes, ByVal xContactName As String)
'alerts user to no numbers if the user hasn't specified not to do so.
    If g_bAlertNoContactNumbers Then
        Dim oForm As frmAlertNoContactNumbers
        Set oForm = New frmAlertNoContactNumbers
        
        xContactName = Replace(xContactName, "&", "&&")
        
        With oForm
            'set alert message
            If iType = ciContactNumberType_Phone Then
                .Caption = " No Phone Numbers"
                .lblMessage.Caption = "No phone numbers were found for " & xContactName & "."
            ElseIf iType = ciContactNumberType_Fax Then
                .Caption = " No Fax Numbers"
                .lblMessage.Caption = "No fax numbers were found for " & xContactName & "."
            Else
                .Caption = " No EMail Addresses"
                .lblMessage.Caption = "No email addresses were found for " & xContactName & "."
            End If
            
            'show dialog
            .Show vbModal, Screen.ActiveForm
            
            'cancel alerts if specified by user
            g_bAlertNoContactNumbers = .chkPrompt.Value = vbChecked
        End With
    End If
End Sub
Public Function AlertNoneIfSpecifiedNew(ByVal iType As ciContactNumberTypes, _
                                        ByVal xContactName As String, _
                                        bCancel As Boolean)
    Dim xMessage As String
    
'alerts user to no numbers if the user hasn't specified not to do so.
    If g_bAlertNoContactNumbers Then
        Dim oForm As frmAlertNoContactNumbersNEW
        Set oForm = New frmAlertNoContactNumbersNEW
        
        xContactName = Replace(xContactName, "&", "&&")
        
        With oForm
            'set alert message
            If iType = ciContactNumberType_Phone Then
                .Caption = " No Phone Numbers"
                xMessage = "No phone numbers were found for " & xContactName & "."
            ElseIf iType = ciContactNumberType_Fax Then
                .Caption = "No Fax Numbers"
                xMessage = "No fax numbers were found for " & xContactName & "."
            Else
                .Caption = " No EMail Addresses"
                xMessage = "No email addresses were found for " & xContactName & "."
            End If
            
            .lblMessage.Caption = xMessage & vbCr & vbCr & _
                                  "Do you want to insert the contact with the available information?"
            
            'show dialog
            .Show vbModal, Screen.ActiveForm
            
            bCancel = .CancelInsert
            
            'cancel alerts if specified by user
            g_bAlertNoContactNumbers = .chkPrompt.Value = vbChecked
        End With
    End If
End Function

