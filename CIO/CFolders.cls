VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CFolders"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'**********************************************************
'   CFolders Collection Class
'   created 12/19/00 by Daniel Fisherman
'   Contains properties and methods
'   that manage a collection of ci folders
'**********************************************************
Option Explicit

Private m_oCol As Collection

'**********************************************************
'initializer/terminator
'**********************************************************
Dim xObjectID As String
Private Sub Class_Initialize()
    Math.Randomize
    xObjectID = "CFolders" & Format(Now, "hhmmss") & Math.Rnd()
    g_oObjects.Add xObjectID, xObjectID
    DebugPrint "Added - " & xObjectID & ": " & g_oObjects.Count
    Set m_oCol = New Collection
End Sub

Private Sub Class_Terminate()
    g_oObjects.Remove xObjectID
    DebugPrint "Removed - " & xObjectID & ": " & g_oObjects.Count
    Set m_oCol = Nothing
End Sub
'**********************************************************

Public Sub Add(oFolder As CFolder, Optional ByVal bSkipDuplicates As Boolean = False)
    Dim xDesc As String
    'add to collection
    On Error Resume Next
    Err.Clear
     m_oCol.Add oFolder, oFolder.UNID
    If Err And Not bSkipDuplicates Then
        'could not add to collection - alert and exit
        On Error GoTo ProcError
        xDesc = "Could not add folder " & oFolder.Name & _
            " to the collection of Folders. " & Err.Description
        Err.Raise Err.Number, , xDesc
        Exit Sub
    End If
    Exit Sub
ProcError:
    g_oError.RaiseError "CIO.Folders.Add"
End Sub

Public Function Item(vIndexID As Variant) As CIO.CFolder
Attribute Item.VB_UserMemId = 0
'will return Nothing if an error is generated
    On Error Resume Next
    Set Item = m_oCol.Item(vIndexID)
End Function

Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
    Set NewEnum = m_oCol.[_NewEnum]
End Property

Public Function Count() As Integer
    Count = m_oCol.Count
End Function




