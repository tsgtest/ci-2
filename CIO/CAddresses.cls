VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CAddresses"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'**********************************************************
'   CAddresses Collection Class
'   created 12/25/00 by Daniel Fisherman
'   Contains properties and methods that manage the
'   collection of CAddresses a listing
'**********************************************************

'**********************************************************
Private m_oAddresses As Collection

'**********************************************************

'**********************************************************
'initializer/terminator
'**********************************************************
Dim xObjectID As String
Private Sub Class_Initialize()
    Math.Randomize
    xObjectID = "CAddresses" & Format(Now, "hhmmss") & Math.Rnd()
    g_oObjects.Add xObjectID, xObjectID
    DebugPrint "Added - " & xObjectID & ": " & g_oObjects.Count
    Set m_oAddresses = New Collection
End Sub

Private Sub Class_Terminate()
    g_oObjects.Remove xObjectID
    DebugPrint "Removed - " & xObjectID & ": " & g_oObjects.Count
    Set m_oAddresses = Nothing
End Sub
'**********************************************************

Public Function Add(oAddress As CIO.CAddress, Optional Before As Integer)
    On Error Resume Next
    If Before = Empty Then
        m_oAddresses.Add oAddress, oAddress.Name
    Else
        m_oAddresses.Add oAddress, , Before
    End If
End Function

Public Function Delete(vKey As Variant)
    m_oAddresses.Remove vKey
End Function

Public Function Count() As Integer
    Count = m_oAddresses.Count
End Function

Public Function Item(vKey As Variant) As CIO.CAddress
    On Error GoTo ProcError
    Set Item = m_oAddresses.Item(vKey)
    Exit Function
ProcError:
    g_oError.RaiseError "CAddresses.Item"
    Resume Next
End Function

Public Function GetArray() As XArrayDB
    Dim oAddress As CIO.CAddress
    Dim oArr As XArrayDB
    Dim iUBound As Integer
    
    On Error GoTo ProcError
    Set oArr = New XArrayDB
    With oArr
        .ReDim 0, -1, 0, 1
    
        For Each oAddress In m_oAddresses
            iUBound = .UpperBound(1) + 1
            .Insert 1, iUBound
            .Value(iUBound, 0) = oAddress.Name
            .Value(iUBound, 1) = oAddress.ID
        Next oAddress
    End With
    
    Set GetArray = oArr
    Exit Function
ProcError:
    g_oError.RaiseError "CIO.CAddresses.GetArray"
    Exit Function
End Function

Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
    Set NewEnum = m_oAddresses.[_NewEnum]
End Property
