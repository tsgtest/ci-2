VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CConstants"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Public Enum ciRetrieveData
    ciRetrieveData_None = 0
    ciRetrieveData_All = 1
    ciRetrieveData_Names = 2
    ciRetrieveData_Addresses = 4
    ciRetrieveData_Phones = 8
    ciRetrieveData_Faxes = 16
    ciRetrieveData_EAddresses = 32
    ciRetrieveData_CustomFields = 64
    ciRetrieveData_Salutation = 128
End Enum

Public Enum ciPrompts
    ciPrompt_MissingAddress = 1
    ciPrompt_MissingPhones = 2
    ciPrompt_MissingFax = 4
    ciPrompt_MissingEAddresses = 8
    ciPrompt_MultiplePhones = 16
    ciPrompt_MulitpleFax = 32
    ciPrompt_MulitpleEAddresses = 64
End Enum

Private Const xUNIDSep As String = "||"

'**********************************************************
'initializer/terminator
'**********************************************************
Dim xObjectID As String
Private Sub Class_Initialize()
    Math.Randomize
    xObjectID = "CConstants" & Format(Now, "hhmmss") & Math.Rnd()
    g_oObjects.Add xObjectID, xObjectID
    DebugPrint "Added - " & xObjectID & ": " & g_oObjects.Count
End Sub

Private Sub Class_Terminate()
    g_oObjects.Remove xObjectID
    DebugPrint "Removed - " & xObjectID & ": " & g_oObjects.Count
End Sub
'**********************************************************

Public Property Get UNIDSep() As String
    UNIDSep = xUNIDSep
End Property

Public Property Get EmptyListingName() As String
    EmptyListingName = "No name specified."
End Property

Public Property Get MultiplePhones() As String
    MultiplePhones = "zzciMultiplePhoneszz"
End Property
