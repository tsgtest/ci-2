VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CContacts"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

'**********************************************************
'   CContacts Collection Class
'   created 8/4/98 by Daniel Fisherman
'   Contains properties and methods that manage the
'   collection of store objects
'**********************************************************

'**********************************************************
Public Enum ciMergeFileFormats
    ciMergeFileFormat_Word = 1
    ciMergeFileFormat_CSV = 2
    ciMergeFileFormat_TabDelim = 3
End Enum

Enum ciContactTypes
    ciContactType_To = 1
    ciContactType_From = 2
    ciContactType_CC = 4
    ciContactType_BCC = 8
    ciContactType_Other = 16
End Enum

Private m_oContactsCol As Collection

'**********************************************************
'initializer/terminator
'**********************************************************
Dim xObjectID As String
Private Sub Class_Initialize()
    Math.Randomize
    xObjectID = "CContacts" & Format(Now, "hhmmss") & Math.Rnd()
    g_oObjects.Add xObjectID, xObjectID
    DebugPrint "Added - " & xObjectID & ": " & g_oObjects.Count
    Set m_oContactsCol = New Collection
End Sub

Private Sub Class_Terminate()
    g_oObjects.Remove xObjectID
    DebugPrint "Removed - " & xObjectID & ": " & g_oObjects.Count
    Set m_oContactsCol = Nothing
End Sub
'**********************************************************
'**********************************************************
'   Properties
'**********************************************************
Public Sub Add(oContact As CContact, Optional Before As Long)
'   key modification allows same contact to be
'   added once per selection list - not truly unique
'   but for all practical purposes, it is so.
    Dim xAddressID As String
    Dim xKey As String
    Dim i As Integer
    
    On Error Resume Next
        
    Randomize
    Dim xRnd As String
    xRnd = Rnd()
    
'    xKey = oContact.UNID & xAddressID
    xKey = oContact.UNID & "+" & xRnd
    
'   the "before" param is here so that we can
'   move contacts around in the collection
    If Before <> Empty Then
        m_oContactsCol.Add oContact, xKey, Before
    Else
        m_oContactsCol.Add oContact, xKey
    End If
    
    'already added at least once - change key to allow contact to be added again
    While Err.Number = 457
        Err.Clear
        i = i + 1
        xKey = xKey & i
        
        If Before <> Empty Then
            m_oContactsCol.Add oContact, xKey, Before
        Else
            m_oContactsCol.Add oContact, xKey
        End If
    Wend
    
    If Err.Number > 0 Then
        Err.Raise Err.Number, , Err.Description
    End If
    
    Exit Sub
ProcError:
    g_oError.RaiseError "CContacts.Add"
    Exit Sub
End Sub

Public Function Delete(vKey As Variant)
    On Error GoTo ProcError
    m_oContactsCol.Remove vKey
    Exit Function
ProcError:
    g_oError.RaiseError "CContacts.Delete"
    Exit Function
End Function

Public Sub DeleteAll()
    On Error GoTo ProcError
    Set m_oContactsCol = New Collection
    Exit Sub
ProcError:
    g_oError.RaiseError "CContacts.DeleteAll"
    Exit Sub
End Sub

Public Function Count() As Integer
    On Error GoTo ProcError
    Count = m_oContactsCol.Count
    Exit Function
ProcError:
    g_oError.RaiseError "CContacts.Count"
    Exit Function
End Function

'**********************************************************
'   Methods
'**********************************************************
Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
    Set NewEnum = m_oContactsCol.[_NewEnum]
End Property

Public Sub FillList(ctlP As Object)
    Dim i As Integer

    On Error GoTo ProcError
    For i = 1 To Me.Count
        ctlP.AddItem Me.Item(i).DisplayName
    Next i
    Exit Sub
ProcError:
    g_oError.RaiseError "CContacts.FillList"
    Exit Sub
End Sub

Public Function Item(vKey As Variant) As CContact
Attribute Item.VB_UserMemId = 0
    On Error GoTo ProcError
    Set Item = m_oContactsCol.Item(vKey)
    Exit Function
ProcError:
    g_oError.RaiseError "CContacts.Item"
    Exit Function
End Function

Public Function GetList(Optional ByVal iContactType As ciContactTypes, Optional bSort As Boolean = False) As XArrayDB
    Dim oArr As XArrayDB
    Dim oContact As CIO.CContact
    Dim iNewUpper As Integer
    
    On Error GoTo ProcError
    
    'create new, empty, 2 column array
    'array will store name, key
    Set oArr = New XArrayDB
    oArr.ReDim 0, -1, 0, 1
    
    'cycle through contacts,
    'adding each to array
    For Each oContact In Me
        If iContactType > 0 Then
            'get only those contacts of the specified type
            If oContact.ContactType = iContactType Then
                'contact is of the specified type
                iNewUpper = oArr.UpperBound(1) + 1
                oArr.Insert 1, iNewUpper
                oArr.Value(iNewUpper, 0) = oContact.DisplayName
                'oArr.Value(iNewUpper, 1) = oContact.UNID
                oArr.Value(iNewUpper, 1) = iNewUpper + 1
            End If
        Else
            'get all contacts in collection
            iNewUpper = oArr.UpperBound(1) + 1
            oArr.Insert 1, iNewUpper
            oArr.Value(iNewUpper, 0) = oContact.DisplayName
            'oArr.Value(iNewUpper, 1) = oContact.UNID
            oArr.Value(iNewUpper, 1) = iNewUpper + 1
        End If
    Next oContact
    
    If bSort Then
        'sort by display name
        oArr.QuickSort 0, iNewUpper, 0, XORDER_ASCEND, XTYPE_STRING
    End If
    
    Set GetList = oArr
    Exit Function
ProcError:
    g_oError.RaiseError "CIO.CContacts.GetList"
    Exit Function
End Function

Public Sub CreateDataFile(ByVal xFile As String)
'creates a merge data file from contacts in collection
    Const ciDataFileHeader As String = "AddressType|ContactType|DisplayName|Prefix|FirstName|MiddleName|LastName|Suffix|Initials|Street1|Street2|Street3|City|State|Zip|Country|Company|Department|Title|EMail|Phone|PhoneExt|Fax"
    Dim iFile As Integer
    Dim xHeader As String
    Dim oCustFlds As CCustomFields
    Dim i As Integer
    Dim j As Integer
    Dim xRecord As String
    Dim oIni As CIO.CIni
    Dim oStr As CStrings

    Set oIni = New CIni
    Set oStr = New CStrings
    
    xHeader = oIni.GetIni("CIApplication", "MergeFileHeader")

    On Error GoTo ProcError
    
    'get file channel
    iFile = FreeFile()
    
    'create file
    Open xFile For Output As #iFile
    
    'create header; use ini setting only if accurate
    If Not (xHeader <> "" And oStr.lCountChrs(xHeader, "|") = 22) Then
        xHeader = ciDataFileHeader
    End If
    
    'get custom fields - we can use the first backend since each backend has the
    'same custom fields (although some fields may not be linked to a data field)
    Set oCustFlds = Me.Item(1).CustomFields
    For j = 1 To oCustFlds.Count
        xHeader = xHeader & "|" & oCustFlds(j).Name
    Next j
        
    Print #iFile, xHeader
    
    'cycle through contacts, writing to file
    For i = 1 To Me.Count
        With Me.Item(i)
            xRecord = """" & .AddressTypeName & """" & _
                        "|" & .ContactType & _
                        "|""" & .DisplayName & """" & _
                        "|""" & .Prefix & """" & _
                        "|""" & .FirstName & """" & _
                        "|""" & .MiddleName & """" & _
                        "|""" & .LastName & """" & _
                        "|""" & .Suffix & """" & _
                        "|""" & .Initials & """" & _
                        "|""" & .Street1 & """" & _
                        "|""" & .Street2 & """" & _
                        "|""" & .Street3 & """" & _
                        "|""" & .City & """" & _
                        "|""" & .State & """" & _
                        "|""" & .ZipCode & """" & _
                        "|""" & .Country & """" & _
                        "|""" & .Company & """" & _
                        "|""" & .Department & """" & _
                        "|""" & .Title & """" & _
                        "|""" & .EMailAddress & """" & _
                        "|""" & .Phone & """" & _
                        "|""" & .PhoneExtension & """" & _
                        "|""" & .Fax & """"
            
            For j = 1 To .CustomFields.Count
                xRecord = xRecord & "|""" & .CustomFields(j).Value & """"
            Next j
            
            xRecord = Replace(xRecord, vbCrLf, "<1310>")
            xRecord = Replace(xRecord, vbCr, "<13>")
            xRecord = Replace(xRecord, Chr$(11), "<11>")
                        
            Print #iFile, xRecord
        End With
    Next i
    
    Set oIni = Nothing

    'close file
    Close #iFile
    Exit Sub
ProcError:
    g_oError.RaiseError "CIO.CContacts.CreateDataFile"
    Exit Sub
End Sub


'**********************************************************
'   Event Procedures
'**********************************************************

