VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CFolder"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'**********************************************************
'   Folder Class
'   created 12/20/00 by Daniel Fisherman
'   Contains properties and methods defining a
'   logical grouping of listings in a store
'**********************************************************
Option Explicit

Private m_vUNID As Variant
Private m_xName As String

'**********************************************************
'initializer/terminator
'**********************************************************
Dim xObjectID As String
Private Sub Class_Initialize()
    Math.Randomize
    xObjectID = "CFolder" & Format(Now, "hhmmss") & Math.Rnd()
    g_oObjects.Add xObjectID, xObjectID
    DebugPrint "Added - " & xObjectID & ": " & g_oObjects.Count
End Sub

Private Sub Class_Terminate()
    g_oObjects.Remove xObjectID
    DebugPrint "Removed - " & xObjectID & ": " & g_oObjects.Count
End Sub
'**********************************************************

Public Property Get UNID() As Variant
    UNID = m_vUNID
End Property

Public Property Let UNID(ByVal vNew As Variant)
    m_vUNID = vNew
End Property

Public Property Get Name() As String
Attribute Name.VB_UserMemId = 0
    Name = m_xName
End Property

Public Property Let Name(ByVal xNew As String)
    m_xName = xNew
End Property

Public Property Get ID() As Variant
'returns the ID of the folder -parsed from the UNID
    On Error GoTo ProcError
    ID = GetUNIDField(Me.UNID, ciUNIDFields_Folder)
    
    Exit Property
ProcError:
    g_oError.RaiseError "CFolder.ID"
End Property

Public Property Get StoreID() As Variant
'returns the ID of the store containing
'this folder- parsed from UNID
    On Error GoTo ProcError
    StoreID = GetUNIDField(Me.UNID, ciUNIDFields_Store)
    Exit Property
ProcError:
    g_oError.RaiseError "CStore.ID"
End Property

Public Property Get BackendID() As Integer
    On Error GoTo ProcError
    BackendID = GetUNIDField(Me.UNID, ciUNIDFields_Backend)
    Exit Property
ProcError:
    g_oError.RaiseError "CListing.BackendID"
End Property
