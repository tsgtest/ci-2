VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CBackend"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'**********************************************************
'   CBackend Class
'   created 12/19/00 by Daniel Fisherman
'   Contains properties and methods of a CI backend-
'   there are a number of unimplemented properties and methods-
'   an actual backend will have to implement these
'**********************************************************
Option Explicit

Private m_iID As Integer
Private m_xName As String
Private m_xProgID As String
Private m_bShowRootNode As Boolean

'******************************************************************
'Properties
'******************************************************************
'CI Backend ID - this corresponds to the ini backend section index
Friend Property Let ID(iNew As Integer)
    m_iID = iNew
End Property

Public Property Get ID() As Integer
    ID = m_iID
End Property

Friend Property Let Name(xNew As String)
    m_xName = xNew
End Property

Public Property Get Name() As String
    Name = m_xName
End Property

Friend Property Let ProgID(xNew As String)
    m_xProgID = xNew
End Property

Public Property Get ProgID() As String
    ProgID = m_xProgID
End Property

Friend Property Let ShowRootNode(bNew As Boolean)
    m_bShowRootNode = bNew
End Property

Public Property Get ShowRootNode() As Boolean
    ShowRootNode = m_bShowRootNode
End Property

'******************************************************************
'Methods
'******************************************************************
Public Function FilterFields() As CIO.CFilterFields
'returns the collection of filter fields for this backend
    Dim oIni As CIO.CIni
    Dim oFFs As CIO.CFilterFields
    Dim xFFSec As String
    Dim xTemp As String
    Dim iPos As Integer
    Dim iPosEqual As Integer
    Dim xName As String
    Dim xID As String
    
    On Error GoTo ProcError
    
    'create new empty filter fields collection
    Set oFFs = New CIO.CFilterFields
    
    'get ini section for filter fields for this backend
    Set oIni = New CIO.CIni
    xFFSec = oIni.GetIniSection("FilterFields" & m_iID, oIni.CIIni)
    
    While Len(xFFSec)
        iPos = InStr(xFFSec, Chr(0))
        If iPos Then
            'parse key from section string
            xTemp = Left(xFFSec, iPos - 1)
            xFFSec = Mid(xFFSec, iPos + 1)
        Else
            'no separators left, take last key
            xTemp = xFFSec
            xFFSec = ""
        End If
        
        'add field from ini key
        iPosEqual = InStr(xTemp, "=")
        If iPosEqual Then
            xName = Left(xTemp, iPosEqual - 1)
            xID = Mid(xTemp, iPosEqual + 1)
            If Len(xName) And Len(xID) Then
                'both name and ID values exist -
                'add to collection
                oFFs.Add xName, xID
            Else
                'name and/or id is missing - alert
                Err.Raise ciErr_MissingOrInvalidINIKey, , _
                    "Invalid key in FilterFields" & _
                    m_iID & " section of CI.ini"
            End If
        Else
            '=' sign is missing - alert
            Err.Raise ciErr_MissingOrInvalidINIKey, , _
                "Invalid key in FilterFields" & _
                m_iID & " section of CI.ini"
        End If
    Wend
    Set FilterFields = oFFs
    Exit Function
ProcError:
    g_oError.RaiseError "CBackend.FilterFields"
End Function

'******************************************************************
'CIBackend Interface
'******************************************************************
Public Property Get SupportsMultipleStores() As Boolean
    
End Property

Public Property Get SupportsFolders() As Boolean
    
End Property

Public Property Get SupportsNestedFolders() As Boolean
    
End Property

Public Function GetStores() As CIO.CStores

End Function

Public Function GetFolders(oStore As CIO.CStore) As CIO.CFolders

End Function

Public Function GetSubFolders(oFolder As CIO.CFolder) As CIO.CFolders

End Function

Public Function GetFolderListings(oFolder As CIO.CFolder, Optional oFilter As CIO.CFilter) As CIO.CListings

End Function

Public Function GetStoreListings(oStore As CIO.CStore, Optional oFilter As CIO.CFilter) As CIO.CListings

End Function

Public Function GetAddresses(oListing As CIO.CListing) As CIO.CAddresses

End Function

Public Function GetContact(oListing As CIO.CListing, oAddress As CIO.CAddress) As CIO.CContact

End Function

Public Sub EditContact(oListing As CIO.CListing)

End Sub

