VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CListings"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'**********************************************************
'   CListings Collection Class
'   created 12/22/00 by Daniel Fisherman
'   Contains properties and methods that manage the
'   collection of listings in a folder or store
'**********************************************************

'**********************************************************
Option Explicit

Public Enum ciListingCols
    ciListingCols_DisplayName = 0
    ciListingCols_AdditionalProperty1 = 1
    ciListingCols_AdditionalProperty2 = 2
    ciListingCols_AdditionalProperty3 = 3
    ciListingCols_UNID = 4
    ciListingCols_Type = 5
    ciListingCols_FolderPath = 6
End Enum

Event ListingAdded(ByVal UNID As String, ByVal DisplayName As String, _
            ByVal Company As String, ByVal LastName As String, _
            ByVal FirstName As String, ByVal ListingType As ciListingType)
Private m_oArray As XArrayDBObject.XArrayDB

'**********************************************************
'initializer/terminator
'**********************************************************
Dim xObjectID As String
Private Sub Class_Initialize()
    Math.Randomize
    xObjectID = "CListings" & Format(Now, "hhmmss") & Math.Rnd()
    g_oObjects.Add xObjectID, xObjectID
    DebugPrint "Added - " & xObjectID & ": " & g_oObjects.Count
'   create initial space
    Set m_oArray = New XArrayDB
    m_oArray.ReDim 1, 0, 0, 6
End Sub

Private Sub Class_Terminate()
    g_oObjects.Remove xObjectID
    DebugPrint "Removed - " & xObjectID & ": " & g_oObjects.Count
    Set m_oArray = Nothing
End Sub
'**********************************************************

'**********************************************************

Public Sub Add(ByVal UNID As String, ByVal DisplayName As String, ByVal AdditionalProperty1 As String, _
    ByVal AdditionalProperty2 As String, ByVal AdditionalProperty3 As String, ByVal ListingType As ciListingType, _
    Optional ByVal FolderPath As String)
'adds listing to collection (array)
    Dim lUBound As Long

'   add array row
    m_oArray.AppendRows

    lUBound = m_oArray.UpperBound(1)

'   fill element with listing detail
    With m_oArray
        .Value(lUBound, ciListingCols_DisplayName) = DisplayName
        .Value(lUBound, ciListingCols_AdditionalProperty1) = AdditionalProperty1
        .Value(lUBound, ciListingCols_AdditionalProperty2) = AdditionalProperty2
        .Value(lUBound, ciListingCols_AdditionalProperty3) = AdditionalProperty3
        .Value(lUBound, ciListingCols_UNID) = UNID
        .Value(lUBound, ciListingCols_Type) = ListingType
        If FolderPath <> Empty Then
            .Value(lUBound, ciListingCols_FolderPath) = FolderPath
        End If
    End With

    Exit Sub
ProcError:
    g_oError.RaiseError "CIO.CListings.Add"
End Sub

Public Sub SetCount(NumberOfListings As Long)
'creates specified number of rows in xarray
    m_oArray.ReDim 1, NumberOfListings, 0, 6
End Sub

Public Sub DeleteAll()
    m_oArray.Clear
End Sub

Public Function Count() As Long
    Count = m_oArray.Count(1)
End Function

Public Function Item(ByVal Index As Long) As CIO.CListing
Attribute Item.VB_UserMemId = 0
'retrieve listing specified by index
    Dim oL As CIO.CListing
    
    On Error GoTo ProcError
    
    If m_oArray.Count(1) = 0 Then
'       there are no items
        Exit Function
    ElseIf Index < 1 Or Index > m_oArray.Count(1) Then
        Exit Function
    Else
'       create listing
        Set oL = New CIO.CListing
        On Error Resume Next
        
'       fill listing
        With oL
            .UNID = m_oArray.Value(Index, ciListingCols_UNID)
            .DisplayName = m_oArray.Value(Index, ciListingCols_DisplayName)
            .AdditionalProperty1 = m_oArray.Value(Index, ciListingCols_AdditionalProperty1)
            .AdditionalProperty2 = m_oArray.Value(Index, ciListingCols_AdditionalProperty2)
            .AdditionalProperty3 = m_oArray.Value(Index, ciListingCols_AdditionalProperty3)
            .FolderPath = m_oArray.Value(Index, ciListingCols_FolderPath)
            .ListingType = m_oArray.Value(Index, ciListingCols_Type)
        End With
    End If
    
    'return listing
    Set Item = oL
    Exit Function
ProcError:
    g_oError.RaiseError "CListings.Item"
End Function

Public Function ToArray() As XArrayDB
    Set ToArray = m_oArray
End Function

Public Function Sort(Optional ByVal icol As ciListingCols = ciListingCols_DisplayName)
    With m_oArray
        If icol = ciListingCols_DisplayName Then
            .QuickSort 1, .Count(1), icol, _
                XORDER_ASCEND, XTYPE_STRING
        Else
            .QuickSort 1, .Count(1), icol, XORDER_ASCEND, _
                XTYPE_STRING, 0, XORDER_ASCEND, XTYPE_STRING
        End If
    End With
End Function
