VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CListing"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'**********************************************************
'   CListing Class
'   created 12/22/00 by Daniel Fisherman
'   Contains properties and methods that
'   define a CI listing
'**********************************************************
Option Explicit

Enum ciListingType
    ciListingType_Person = 1
    ciListingType_Group = 2
End Enum

Private m_vUNID As Variant
Private m_xDisplayName As String
Private m_xCol1 As String
Private m_xCol2 As String
Private m_xCol3 As String
Private m_xCol4 As String
Private m_iListingType As ciListingType

'**********************************************************
'initializer/terminator
'**********************************************************
Dim xObjectID As String
Private Sub Class_Initialize()
    Math.Randomize
    xObjectID = "CListings" & Format(Now, "hhmmss") & Math.Rnd()
    g_oObjects.Add xObjectID, xObjectID
    DebugPrint "Added - " & xObjectID & ": " & g_oObjects.Count
End Sub

Private Sub Class_Terminate()
    g_oObjects.Remove xObjectID
    DebugPrint "Removed - " & xObjectID & ": " & g_oObjects.Count
End Sub
'**********************************************************

Public Property Get BackendID() As Integer
    On Error GoTo ProcError
    BackendID = GetUNIDField(Me.UNID, ciUNIDFields_Backend)
    Exit Property
ProcError:
    g_oError.RaiseError "CListing.BackendID"
End Property

Public Property Get StoreID() As Variant
    On Error GoTo ProcError
    StoreID = GetUNIDField(Me.UNID, ciUNIDFields_Store)
    Exit Property
ProcError:
    g_oError.RaiseError "CListing.StoreID"
End Property

Public Property Get FolderID() As Variant
    On Error GoTo ProcError
    FolderID = GetUNIDField(Me.UNID, ciUNIDFields_Folder)
    Exit Property
ProcError:
    g_oError.RaiseError "CListing.FolderID"
End Property

Public Property Get ID() As Variant
    On Error GoTo ProcError
    ID = GetUNIDField(Me.UNID, ciUNIDFields_Listing)
    Exit Property
ProcError:
    g_oError.RaiseError "CListing.ID"
End Property

Public Property Get UNID() As Variant
    UNID = m_vUNID
End Property

Public Property Let UNID(ByVal vNew As Variant)
    m_vUNID = vNew
End Property

Public Property Get DisplayName() As String
Attribute DisplayName.VB_UserMemId = 0
    DisplayName = m_xDisplayName
End Property

Public Property Let DisplayName(ByVal xNew As String)
    m_xDisplayName = xNew
End Property

Public Property Get AdditionalProperty1() As String
    AdditionalProperty1 = m_xCol1
End Property

Public Property Let AdditionalProperty1(ByVal xNew As String)
    m_xCol1 = xNew
End Property

Public Property Get AdditionalProperty2() As String
    AdditionalProperty2 = m_xCol2
End Property

Public Property Let AdditionalProperty2(ByVal xNew As String)
    m_xCol2 = xNew
End Property

Public Property Get AdditionalProperty3() As String
    AdditionalProperty3 = m_xCol3
End Property

Public Property Let AdditionalProperty3(ByVal xNew As String)
    m_xCol3 = xNew
End Property

Public Property Get FolderPath() As String
    FolderPath = m_xCol4
End Property

Public Property Let FolderPath(ByVal xNew As String)
    m_xCol4 = xNew
End Property

Public Property Get ListingType() As ciListingType
    ListingType = m_iListingType
End Property

Public Property Let ListingType(ByVal iNew As ciListingType)
    m_iListingType = iNew
End Property

Public Function BuildDisplayName(oContact As CIO.CContact, Optional ByVal xDefaultDisplay As String) As String
'constructs and returns a name for display purposes
    Dim xName As String
    
    On Error GoTo ProcError
    
    With oContact
        If .LastName <> "" And .FirstName <> "" And .MiddleName <> "" Then
            xName = .LastName & ", " & .FirstName & " " & .MiddleName
        ElseIf .LastName <> "" And .FirstName <> "" Then
            xName = .LastName & ", " & .FirstName
        ElseIf .LastName <> "" Then
            xName = .LastName
        ElseIf .FirstName <> "" Then
            xName = .FirstName
        ElseIf .Company <> "" Then
            xName = .Company
        ElseIf xDefaultDisplay <> Empty Then
            xName = xDefaultDisplay
        Else
            xName = ""
        End If
    End With
    
    BuildDisplayName = xName
    Exit Function
ProcError:
    g_oError.RaiseError "CListing.BuildDisplayName"
End Function


