VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CRegistry"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private Declare Function RegOpenKey Lib "advapi32.dll" Alias "RegOpenKeyA" _
    (ByVal hKey As Long, ByVal lpSubKey As String, phkResult As Long) _
        As Long
Private Declare Function RegOpenKeyEx Lib "advapi32.dll" Alias "RegOpenKeyExA" _
    (ByVal hKey As Long, ByVal lpSubKey As String, _
    ByVal ulOptions As Long, ByVal samDesired As Long, _
    phkResult As Long) As Long
    
Private Declare Function RegCloseKey Lib "advapi32.dll" (ByVal hKey As Long) As Long

Private Declare Function RegQueryValue Lib "advapi32.dll" Alias "RegQueryValueA" _
    (ByVal hKey As Long, ByVal lpSubKey As String, _
    ByVal lpValue As String, lpcbValue As Long) As Long
    
 Private Declare Function RegQueryValueExLong Lib "advapi32.dll" Alias _
   "RegQueryValueExA" (ByVal hKey As Long, ByVal lpValueName As _
   String, ByVal lpReserved As Long, lpType As Long, lpData As _
   Long, lpcbData As Long) As Long
   
Private Declare Function RegQueryValueExString Lib "advapi32.dll" Alias _
   "RegQueryValueExA" (ByVal hKey As Long, ByVal lpValueName As _
   String, ByVal lpReserved As Long, lpType As Long, ByVal lpData _
   As String, lpcbData As Long) As Long
   
Private Declare Function RegQueryValueEx Lib "advapi32.dll" Alias _
    "RegQueryValueExA" (ByVal hKey As Long, ByVal lpValueName As String, _
    ByVal lpReserved As Long, lpType As Long, lpData As Any, _
    lpcbData As Long) As Long         ' Note that if you declare the lpData parameter as String, you must pass it By Value.

 Private Declare Function RegQueryValueExNULL Lib "advapi32.dll" Alias _
   "RegQueryValueExA" (ByVal hKey As Long, ByVal lpValueName As _
   String, ByVal lpReserved As Long, lpType As Long, ByVal lpData _
   As Long, lpcbData As Long) As Long

Private Declare Function RegSetValueExString Lib "advapi32.dll" Alias _
   "RegSetValueExA" (ByVal hKey As Long, ByVal lpValueName As String, _
   ByVal Reserved As Long, ByVal dwType As Long, ByVal lpValue As _
   String, ByVal cbData As Long) As Long

Private Declare Function RegSetValueExLong Lib "advapi32.dll" Alias _
   "RegSetValueExA" (ByVal hKey As Long, ByVal lpValueName As String, _
   ByVal Reserved As Long, ByVal dwType As Long, lpValue As Long, _
   ByVal cbData As Long) As Long
   
Enum Errors
     ERROR_NONE = 0
     ERROR_BADDB = 1
     ERROR_BADKEY = 2
     ERROR_CANTOPEN = 3
     ERROR_CANTREAD = 4
     ERROR_CANTWRITE = 5
     ERROR_OUTOFMEMORY = 6
     ERROR_INVALID_PARAMETER = 7
     ERROR_ACCESS_DENIED = 8
     ERROR_INVALID_PARAMETERS = 87
     ERROR_NO_MORE_ITEMS = 259
End Enum

Enum Reg
    REG_SZ = 1
    REG_EXPAND_SZ = 2
    REG_DWORD = 4
    REG_OPTION_NON_VOLATILE = 0
End Enum

Enum hKey
    HKEY_CLASSES_ROOT = &H80000000
    HKEY_CURRENT_USER = &H80000001
    HKEY_LOCAL_MACHINE = &H80000002
    HKEY_USERS = &H80000003
    KEY_ALL_ACCESS = &H3F
End Enum

Private Const KEY_QUERY_VALUE As Long = &H1
Private Const KEY_SET_VALUE As Long = &H2
Private Const KEY_CREATE_SUB_KEY  As Long = &H4
Private Const KEY_ENUMERATE_SUB_KEYS As Long = &H8
Private Const KEY_NOTIFY As Long = &H10
Private Const KEY_CREATE_LINK As Long = &H20
Private Const READ_CONTROL As Long = &H20000

Private Const WRITE_DAC As Long = &H40000
Private Const WRITE_OWNER As Long = &H80000
Private Const SYNCHRONIZE As Long = &H100000

Private Const STANDARD_RIGHTS_REQUIRED As Long = &HF0000
Private Const STANDARD_RIGHTS_READ As Long = READ_CONTROL
Private Const STANDARD_RIGHTS_WRITE As Long = READ_CONTROL
Private Const STANDARD_RIGHTS_EXECUTE As Long = READ_CONTROL

Private Const KEY_READ As Long = STANDARD_RIGHTS_READ Or _
                                KEY_QUERY_VALUE Or _
                                KEY_ENUMERATE_SUB_KEYS Or _
                                KEY_NOTIFY

Private Const KEY_WRITE As Long = STANDARD_RIGHTS_WRITE Or _
                                KEY_SET_VALUE Or _
                                KEY_CREATE_SUB_KEY

Private Const KEY_EXECUTE As Long = KEY_READ

'**********************************************************
'initializer/terminator
'**********************************************************
Dim xObjectID As String
Private Sub Class_Initialize()
    Math.Randomize
    xObjectID = "CRegistry" & Format(Now, "hhmmss") & Math.Rnd()
    g_oObjects.Add xObjectID, xObjectID
    DebugPrint "Added - " & xObjectID & ": " & g_oObjects.Count
End Sub

Private Sub Class_Terminate()
    g_oObjects.Remove xObjectID
    DebugPrint "Removed - " & xObjectID & ": " & g_oObjects.Count
End Sub
'**********************************************************

Private Function SetValueEx(ByVal Path As Long, Key As String, _
   lType As Long, Value As Variant) As Long
       Dim lValue As Long
       Dim sValue As String
       Select Case lType
           Case REG_SZ, REG_EXPAND_SZ
               sValue = Value & Chr$(0)
               SetValueEx = RegSetValueExString(Path, Key, 0&, _
                                              lType, sValue, Len(sValue))
           Case REG_DWORD
               lValue = Value
               SetValueEx = RegSetValueExLong(Path, Key, 0&, _
   lType, lValue, 4)
           End Select
   End Function

Public Function OpenKey(ByVal RootKey As Long, ByVal Path As String, Ret As Variant) As Long
       Dim cch As Long
       Dim lrc As Long
       Dim lType As Long
       Dim lValue As Long
       Dim sValue As String

       On Error GoTo QueryValueExError

       ' Determine the size and type of data to be read
       lrc = RegQueryValueExNULL(RootKey, Path, 0&, lType, 0&, cch)
       If lrc <> ERROR_NONE Then Error 5

       Select Case lType
           ' For strings
           Case REG_SZ, REG_EXPAND_SZ
               sValue = String(cch, 0)
                lrc = RegQueryValueExString(RootKey, Path, 0&, lType, _
                sValue, cch)
               If lrc = ERROR_NONE Then
                   Ret = Left$(sValue, cch - 1)
               Else
                   Ret = Empty
               End If
           ' For DWORDS
           Case REG_DWORD:
                lrc = RegQueryValueExLong(RootKey, Path, 0&, lType, _
                lValue, cch)
               If lrc = ERROR_NONE Then Ret = lValue
           Case Else
               'all other data types not supported
               lrc = -1
       End Select

QueryValueExExit:
       OpenKey = lrc
       Exit Function
QueryValueExError:
       Resume QueryValueExExit
   End Function


Public Function GetValue(hRootKey As Long, Path As String, ValueName As String, Ret As Variant) As Long
'opens specified key, returns specified value, closes key
    Dim lRet As Long
    Dim hKey As Long
    
    lRet = RegOpenKeyEx(hRootKey, Path, 0, KEY_READ, hKey)
    lRet = QueryValueEx(hKey, ValueName, Ret)
    lRet = RegCloseKey(hKey)
    GetValue = lRet
End Function

Public Function SetValue(hRootKey As Long, Path As String, ValueName As String, DataType As Long, Value As Variant) As Long
'opens specified key, sets specified value, closes key

    Dim lRet As Long
    Dim hKey As Long
    
    lRet = RegOpenKeyEx(hRootKey, Path, 0, KEY_WRITE, hKey)
    lRet = SetValueEx(hKey, ValueName, DataType, Value)
    lRet = RegCloseKey(hKey)
    SetValue = lRet
End Function

Private Function QueryValueEx(ByVal lhKey As Long, ByVal szValueName As _
   String, vValue As Variant) As Long
       Dim cch As Long
       Dim lrc As Long
       Dim lType As Long
       Dim lValue As Long
       Dim sValue As String

       On Error GoTo QueryValueExError

       ' Determine the size and type of data to be read
       lrc = RegQueryValueExNULL(lhKey, szValueName, 0&, lType, 0&, cch)
       If lrc <> ERROR_NONE Then Error 5

       Select Case lType
           ' For strings
           Case REG_SZ, REG_EXPAND_SZ
               sValue = String(cch, 0)
   lrc = RegQueryValueExString(lhKey, szValueName, 0&, lType, _
   sValue, cch)
               If lrc = ERROR_NONE Then
                   vValue = Left$(sValue, cch - 1)
               Else
                   vValue = Empty
               End If
           ' For DWORDS
           Case REG_DWORD:
   lrc = RegQueryValueExLong(lhKey, szValueName, 0&, lType, _
   lValue, cch)
               If lrc = ERROR_NONE Then vValue = lValue
           Case Else
               'all other data types not supported
               lrc = -1
       End Select

QueryValueExExit:
       QueryValueEx = lrc
       Exit Function
QueryValueExError:
       Resume QueryValueExExit
End Function

Public Function GetDLLOCXPath(ByVal xProgID As String) As String
'returns the location of the dll, ocx, or exe
'that contains the specified class
    Dim vCLSID As Variant
    Dim vPath As Variant
    Dim lRet As Long
    Dim iPos As Integer
    Dim iLastPos As Integer
    
'   get guid from prog id
    vCLSID = String(255, Chr(32))
    lRet = GetValue(HKEY_CLASSES_ROOT, xProgID & "\Clsid", "", vCLSID)
    
'   get location from guid
    vPath = String(255, Chr(32))
    lRet = GetValue(HKEY_CLASSES_ROOT, "CLSID\" & _
            vCLSID & "\InprocServer32", "", vPath)
            
'   trim file name
    iPos = 1
    While InStr(iPos, vPath, "\") > 0
        iLastPos = InStr(iPos, vPath, "\")
        iPos = iLastPos + 1
    Wend
    
    GetDLLOCXPath = Left(vPath, iLastPos)
End Function


