VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ICIBackend"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Public Enum ciAlerts
    ciAlert_None = 0
    ciAlert_All = 1
    ciAlert_MultiplePhones = 2
    ciAlert_NoPhones = 4
    ciAlert_MultipleFaxes = 8
    ciAlert_NoFaxes = 16
    ciAlert_MultipleEAddresses = 32
    ciAlert_NoEAddresses = 64
    ciAlert_NoAddresses = 128
End Enum

Public Property Let DefaultSortColumn(iNew As ciListingCols)

End Property

Public Property Get DefaultSortColumn() As ciListingCols

End Property

Public Property Get IsLoadableEntity(ByVal xUNID As String) As Boolean

End Property

Public Property Get IsSearchableEntity(ByVal xUNID As String) As Boolean

End Property

Public Property Get InternalID() As Integer

End Property

Public Property Let ID(ByVal iID As Integer)

End Property

Public Property Get ID() As Integer

End Property

Public Property Get Name() As String

End Property

Public Property Get DisplayName() As String

End Property

Public Property Get SupportsNativeSearch() As Boolean
    
End Property

Public Property Get IsConnected() As Boolean
    
End Property

Public Property Get Exists() As Boolean
    
End Property

Public Property Get SupportsFolders() As Boolean
    
End Property

Public Property Get SupportsNestedFolders() As Boolean
    
End Property
'
'Public Property Get SupportsStoreSearch() As Boolean
'
'End Property

'Public Property Get SupportsStoreLoad() As Boolean
'
'End Property

Public Property Get SearchOperators() As ciSearchOperators

End Property

Public Sub Initialize(iID As Integer)
Attribute Initialize.VB_Description = "Allows the backend to execute any initialization procedures"

End Sub

Public Function Events() As CIO.CEventGenerator

End Function

Public Function GetStores() As CIO.CStores

End Function

Public Function Filter() As CIO.CFilter
Attribute Filter.VB_Description = "returns the collection of filter fields for the backend - delegate to CBackends.FilterFields if filter fields are listed in FilterFieldsX section of ci.ini."

End Function

Public Function GetFolders(oStore As CIO.CStore) As CIO.CFolders

End Function

Public Function GetSubFolders(oFolder As CIO.CFolder) As CIO.CFolders

End Function

Public Function GetFolderListings(oFolder As CIO.CFolder, Optional oFilter As CIO.CFilter) As CIO.CListings

End Function

Public Function SearchFolder(oFolder As CIO.CFolder, ByRef bCancel As Boolean) As CIO.CListings

End Function

Public Function SearchNative() As CIO.CListings

End Function

Public Function GetStoreListings(oStore As CIO.CStore, Optional oFilter As CIO.CFilter) As CIO.CListings

End Function

Public Function SearchStore(oStore As CIO.CStore) As CIO.CListings

End Function

Public Function GetAddresses(Optional oListing As CIO.CListing) As CIO.CAddresses
End Function

Public Function GetContacts(oListing As CIO.CListing, oAddress As CIO.CAddress, Optional ByVal vAddressType As Variant, Optional ByVal iIncludeData As ciRetrieveData = ciRetrieveData_All, Optional ByVal iAlerts As ciAlerts = ciAlert_All) As CIO.CContacts

End Function

Public Sub EditContact(oListing As CIO.CListing)

End Sub

Public Function GetCustomFields(oListing As CIO.CListing) As CIO.CCustomFields

End Function

Public Function GetPhoneNumbers(oListing As CIO.CListing, Optional ByVal vAddressType As Variant) As CIO.CContactNumbers

End Function

Public Function GetFaxNumbers(oListing As CIO.CListing, Optional ByVal vAddressType As Variant) As CIO.CContactNumbers

End Function

Public Function GetEMailNumbers(oListing As CIO.CListing, Optional ByVal vAddressType As Variant) As CIO.CContactNumbers

End Function

Public Function HasAddresses(oListing As CIO.CListing) As Boolean

End Function

Public Function NumberPromptFormat() As ICINumberPromptFormat

End Function

Public Function GetPhoneNumber(oListing As CIO.CListing, ByVal vPhoneID As Variant) As CIO.CContactNumber

End Function

Public Function GetFaxNumber(oListing As CIO.CListing, ByVal vFaxID As Variant) As CIO.CContactNumber

End Function

Public Function GetEMailNumber(oListing As CIO.CListing, ByVal vEMailID As Variant) As CIO.CContactNumber

End Function

Public Property Get SupportsContactEdit() As Boolean

End Property

Public Property Get SupportsContactAdd() As Boolean

End Property

Public Sub AddContact()

End Sub

Public Property Get Col1Name() As String

End Property

Public Property Get Col2Name() As String

End Property

Public Property Get Col3Name() As String

End Property

Public Property Get Col4Name() As String

End Property

Public Property Get SupportsMultipleStores() As Boolean

End Property

Public Sub CustomProcedure1()

End Sub

Public Sub CustomProcedure2()

End Sub

Public Sub CustomProcedure3()

End Sub

Public Sub CustomProcedure4()

End Sub

Public Sub CustomProcedure5()

End Sub

Public Function CustomMenuItem1() As String

End Function

Public Function CustomMenuItem2() As String

End Function

Public Function CustomMenuItem3() As String

End Function

Public Function CustomMenuItem4() As String

End Function

Public Function CustomMenuItem5() As String

End Function

