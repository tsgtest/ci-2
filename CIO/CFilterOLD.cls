VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CFilter"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

'**********************************************************
'   Filter Class
'   created 8/19/98 by Daniel Fisherman
'   Contains properties and methods concerning
'   application filter
'**********************************************************

'**********************************************************

Public Enum ciSearchOperators
    ciSearchOperator_Equals = 1
    ciSearchOperator_BeginsWith = 2
    ciSearchOperator_Contains = 4
End Enum

Enum ciSearchDomains
    ciSearchDomain_CurListingSource = 1
    ciSearchDomain_CurStore = 2
End Enum

Private m_Fields As CIO.FilterFields
Private m_Stores As CIO.Stores
Private m_xSort As String
Private m_iDisplay As Integer
Private m_xOperator As String
Private m_bInCurrentFolder As Boolean
Private m_iSearchType As ciSearchTypes
Private m_iDomain As ciSearchDomains

Private m_debugID As Long
'**********************************************************

'**********************************************************
'   Properties
'**********************************************************

Public Property Get Applied() As Boolean
'returns true if one of the filter fields
'has a value,else returns false
    Dim i As Integer
    For i = 1 To Me.Fields.Count
        If (Me.Fields(i).Value <> Empty) Then
            Applied = True
            Exit For
        End If
    Next i
End Property

Public Property Let Domain(iNew As ciSearchDomains)
    m_iDomain = iNew
End Property

Public Property Get Domain() As ciSearchDomains
    Domain = m_iDomain
End Property

Public Property Let Stores(stsNew As mpCI.Stores)
    Set m_Stores = stsNew
End Property

Public Property Get Stores() As mpCI.Stores
    Set Stores = m_Stores
End Property

Public Property Let Display(iNew As Integer)
    m_iDisplay = iNew
End Property

Public Property Get Display() As Integer
    Display = m_iDisplay
End Property

Public Property Let Sort(xNew As String)
    m_xSort = xNew
    If Application.PreferenceSource = ciPreferenceSource_INI Then
        If UseUserIni() Then
            SetUserIni "Application", "DefaultSort", xNew
        Else
            SetIni "Application", "DefaultSort", xNew
        End If
    Else
        SetUserValue "DefaultSort", xNew
    End If
End Property

Public Property Get Sort() As String
    Sort = m_xSort
End Property

Public Property Get Where() As String
'returns a where string describing filter
    Dim xWhere As String
    
    xWhere = "You have to rebuild Filter.where"
    Where = xWhere
End Property

Public Property Let SearchType(iNew As ciSearchTypes)
    m_iSearchType = iNew
End Property

Public Property Get SearchType() As ciSearchTypes
    SearchType = m_iSearchType
End Property

'**********************************************************
'   Methods
'**********************************************************
Public Function Fields() As mpCI.FilterFields
    If m_Fields Is Nothing Then
        GetFields
    End If
    Set Fields = m_Fields
End Function


Public Function Clear() As mpCI.Filter
'returns a filter object whose properties
'reflect the status of the dialog
    Dim fltNew As mpCI.Filter
    Dim i As Integer
    
    With Me
'       clear field values
        For i = 1 To .Fields.Count
            .Fields(i).Value = ""
        Next i
    End With
        
End Function

'**********************************************************
'   Event Procedures
'**********************************************************
Private Sub Class_Initialize()
    If g_bTrace Then
        m_debugID = LogObjectCreate("Filter")
    End If
    If Application.PreferenceSource = ciPreferenceSource_INI Then
        If UseUserIni() Then
            Me.Sort = GetUserIni("Application", "DefaultSort")
            Me.SearchType = GetUserIni("Application", "SearchType")
        Else
            Me.Sort = GetIni("Application", "DefaultSort")
            Me.SearchType = GetIni("Application", "SearchType")
        End If
    Else
        Me.Sort = GetUserValue("DefaultSort")
        Me.SearchType = GetUserValue("SearchType")
    End If
    Me.Domain = ciSearchDomain_CurListingSource
End Sub

Private Sub Class_Terminate()
    If g_bTrace Then
        RemoveObjectFromLog m_debugID
    End If
    If Not (m_Fields Is Nothing) Then
        m_Fields.TearDown
    End If
    Set m_Fields = Nothing
End Sub

'**********************************************************
'   Internal Procedures
'**********************************************************
Private Sub GetFields()
    Dim i As Integer
    Dim xKey As String
    Dim xName As String
    
    Set m_Fields = New CIO.FilterFields
    
    i = 1
    xKey = "FilterField" & i
    
    xName = GetIni("Application", xKey)
    
    While xName <> ""
        m_Fields.Add xName, i
        i = i + 1
        xKey = "FilterField" & i
        xName = GetIni("Application", xKey)
    Wend
End Sub
