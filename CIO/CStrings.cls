VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CStrings"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'**********************************************************
'initializer/terminator
'**********************************************************
Dim xObjectID As String
Private Sub Class_Initialize()
    Math.Randomize
    xObjectID = "CStrings" & Format(Now, "hhmmss") & Math.Rnd()
    g_oObjects.Add xObjectID, xObjectID
    DebugPrint "Added - " & xObjectID & ": " & g_oObjects.Count
End Sub

Private Sub Class_Terminate()
    g_oObjects.Remove xObjectID
    DebugPrint "Removed - " & xObjectID & ": " & g_oObjects.Count
End Sub
'**********************************************************

Function lCharacterPosition(xSource As String, xSearch As String, lInstances As Long) As Long
'returns the position of the specified instances of xSource
'If specified index does not exist returns Len(xSource)

    Dim iPos As Integer
    Dim l As Long
    Dim lv As Long
    
'---validate lInstances
    lv = lCountChrs(xSource, xSearch)
    If lv < lInstances Then
        lCharacterPosition = Len(xSource)
        Exit Function
    End If
    
    iPos = InStr(xSource, xSearch)
    
    While iPos
        l = l + 1
        If l = lInstances Then GoTo wExit
        iPos = InStr(iPos + 1, xSource, xSearch)
    Wend
wExit:
    lCharacterPosition = iPos
End Function

Function xSplit(xText As String, _
                xSplitAfter As String, _
                Optional bIncludeComma As Boolean = True) As String
'inserts new line chr in xText after xSplitAfter-
'if bIncludeComma then leave trailing char at end
'of first line

    Dim iSplitPos As Integer
    Dim iSplitLength As Integer
    Dim xFirstLine As String
    Dim xSecondLine As String
    
    iSplitPos = InStr(xText, xSplitAfter)
    
    If iSplitPos Then
        iSplitLength = Len(xSplitAfter)
        xFirstLine = Trim(Left(xText, iSplitPos - 1))
        If Not bIncludeComma Then
            If Right(xFirstLine, 1) = "," Then
                xFirstLine = Left(xFirstLine, Len(xFirstLine) - 1)
            End If
        End If
        xSecondLine = Trim(Mid(xText, iSplitPos + iSplitLength))
        xSplit = xFirstLine & Chr(11) & xSecondLine
    Else
        xSplit = xText
    End If
End Function

Function StripNonNumeric(xInput As String) As String
    Dim iPos As Integer
    Dim xNew As String
    Dim iTemp As Integer
    
    xInput = Trim(xInput)
    If xInput <> "" Then
        iPos = 1
        iTemp = Len(xInput)
        While iTemp >= iPos
            If IsNumeric(Mid(xInput, iPos, 1)) = True Then
                xNew = xNew & Mid(xInput, iPos, 1)
            End If
            iPos = iPos + 1
        Wend
    Else
        xNew = ""
    End If
    StripNonNumeric = xNew
End Function

Function lCountChrs(xSource As String, xSearch As String) As Long
'returns the number of instances
'of xSearch in xSource

    Dim iPos As Integer
    Dim l As Long
    
    iPos = InStr(xSource, xSearch)
    While iPos
        l = l + 1
        iPos = InStr(iPos + Len(xSearch), xSource, xSearch)
    Wend
    lCountChrs = l
End Function

Function xNullToString(vValue As Variant) As String
    If Not IsNull(vValue) Then _
        xNullToString = CStr(vValue)
End Function

Function xStringToArray(ByVal xString As String, _
                        arrP() As String, _
                        Optional iNumCols As Integer = 1, _
                        Optional xSep As String = ",") As String

    Dim iNumSeps As Integer
    Dim iNumEntries As Integer
    Dim iSepPos As Integer
    Dim xEntry As String
    Dim i As Integer
    Dim j As Integer
    
'   get ubound of arrP - count delimiter
'   then divide by iNumCols
    iNumSeps = lCountChrs(xString, xSep)
    iNumEntries = (iNumSeps + 1) / iNumCols
    
    ReDim arrP(iNumEntries - 1, iNumCols - 1)
    
    For i = 0 To iNumEntries - 1
        For j = 0 To iNumCols - 1
'           get next entry & store
            iSepPos = InStr(xString, xSep)
            If iSepPos Then
                xEntry = Left(xString, iSepPos - 1)
            Else
                xEntry = xString
            End If
            arrP(i, j) = xEntry
            
'           remove entry from xstring
            If iSepPos Then _
                xString = Mid(xString, iSepPos + 1)
        Next j
    Next i
End Function

Function xTrimTrailingChrs(ByVal xText As String, _
                           Optional ByVal xChr As String = "", _
                           Optional bTrimStart As Boolean = False, _
                           Optional bTrimEnd As Boolean = True) As String
                           
'   removes trailing xChr from xText -
'   if xchr = "" then trim last char

    If xChr <> "" Then
        If bTrimStart Then
            While Left(xText, Len(xChr)) = xChr
                xText = Mid(xText, Len(xChr) + 1)
            Wend
        End If
        If bTrimEnd Then
            While Right(xText, Len(xChr)) = xChr
                xText = Left(xText, Len(xText) - Len(xChr))
            Wend
        End If
    Else
        xText = Left(xText, Len(xText) - 1)
    End If
        
    xTrimTrailingChrs = xText
End Function

Function bStringToArray(ByVal xString As String, _
                    arrP() As String, _
                    Optional ByVal xSep As String = ",") As Boolean
'fills arrP with items
'separated by xSep in xString
    Dim iNewUBound As Integer
    Dim xNewItem As String
    Dim iCurSepPos As Integer
    
'   get current delimiter position
    iCurSepPos = InStr(xString, xSep)
    
    While iCurSepPos
    
'       dim to one greater if last item
'       in array is not empty, else use
'       last item
        If arrP(UBound(arrP)) <> "" Then
            iNewUBound = UBound(arrP) + 1
            ReDim Preserve arrP(iNewUBound)
        Else
            iNewUBound = UBound(arrP)
        End If
        
'       isolate item from string
        xNewItem = Left(xString, iCurSepPos - 1)

'       fill array with new item
        arrP(iNewUBound) = xNewItem
        
'       trim item from original string
        xString = Mid(xString, iCurSepPos + Len(xSep))
                      
'       search remainder of string for delimiter
        iCurSepPos = InStr(xString, xSep)
    Wend
    
'   dim to one greater if last item
'   in array is not empty, else use
'   last item
    If arrP(UBound(arrP)) <> "" Then
        iNewUBound = UBound(arrP) + 1
        ReDim Preserve arrP(iNewUBound)
    Else
        iNewUBound = UBound(arrP)
    End If
        
'   fill array with new item
    arrP(iNewUBound) = xString

    bStringToArray = True
    
End Function

Function iStringToArray(ByVal xString As String, _
                    arrP() As String, _
                    Optional ByVal xSep As String = ",") As Integer
'fills arrP with items
'separated by xSep in xString
    Dim iNewUBound As Integer
    Dim xNewItem As String
    Dim iCurSepPos As Integer
    
   
'   get current delimiter position
    iCurSepPos = InStr(xString, xSep)
    
    While iCurSepPos
    
'       dim to one greater if last item
'       in array is not empty, else use
'       last item
        If arrP(UBound(arrP)) <> "" Then
            iNewUBound = UBound(arrP) + 1
            ReDim Preserve arrP(iNewUBound)
        Else
            iNewUBound = UBound(arrP)
        End If
        
'       isolate item from string
        xNewItem = Left(xString, iCurSepPos - 1)

'       fill array with new item
        arrP(iNewUBound) = xNewItem
        
'       trim item from original string
        xString = Mid(xString, iCurSepPos + _
                      Len(xSep))
                      
'       search remainder of string for delimiter
        iCurSepPos = InStr(xString, xSep)
    Wend
    
'   dim to one greater if last item
'   in array is not empty, else use
'   last item
    If arrP(UBound(arrP)) <> "" Then
        iNewUBound = UBound(arrP) + 1
        ReDim Preserve arrP(iNewUBound)
    Else
        iNewUBound = UBound(arrP)
    End If
        
'   fill array with new item
    If xString <> "" Then
        arrP(iNewUBound) = xString
        iNewUBound = UBound(arrP) + 1
    End If
    
    iStringToArray = iNewUBound
    
End Function



Function arrPToString(arrP() As String, _
                        Optional ByVal xSep As String = ",") As String
    Dim i As Integer
    Dim xString As String
    
    On Error Resume Next
    
'   concatenate array items to string
    For i = LBound(arrP) To UBound(arrP)
        xString = xString & arrP(i) & xSep
    Next i
    
'   trim final separator
    xString = Left(xString, Len(xString) - _
                            Len(xSep))
    
    arrPToString = xString
End Function


Function xSubstitute(ByVal xString As String, _
                     xSearch As String, _
                     xReplace As String) As String
'replaces xSearch in
'xString with xReplace -
'returns modified xString -
'NOTE: SEARCH IS NOT CASE SENSITIVE

    Dim iSeachPos As Integer
    Dim xNewString As String
    
'   get first char pos
    iSeachPos = InStr(UCase(xString), _
                      UCase(xSearch))
    
'   remove switch all chars
    While iSeachPos
        xNewString = xNewString & _
            Left(xString, iSeachPos - 1) & _
            xReplace
        xString = Mid(xString, iSeachPos + Len(xSearch))
        iSeachPos = InStr(UCase(xString), _
                          UCase(xSearch))
    
    Wend
    
    xNewString = xNewString & xString
    xSubstitute = xNewString
End Function

Function xSubstituteFirst(ByVal xString As String, _
                          xSearch As String, _
                          xReplace As String) As String
'replaces first xSearch in
'xString with xReplace -
'returns modified xString -
'NOTE: SEARCH IS NOT CASE SENSITIVE

    Dim iSeachPos As Long
    Dim xNewString As String
    
'   get first char pos
    iSeachPos = InStr(UCase(xString), _
                      UCase(xSearch))
    
'   remove switch all chars
    If iSeachPos Then
        xNewString = xNewString & _
            Left(xString, iSeachPos - 1) & _
            xReplace
        xString = Mid(xString, iSeachPos + Len(xSearch))
    End If
    
    xNewString = xNewString & xString
    xSubstituteFirst = xNewString
End Function

Function xTrimSpaces(ByVal xString As String) As String

    xTrimSpaces = xSubstitute(xString, _
                              Chr$(32), _
                              "")
End Function

Function bIniToBoolean(xTFString As String) As Boolean
    If UCase(xTFString) = "TRUE" Then
        bIniToBoolean = True
    Else
        bIniToBoolean = False
    End If

End Function

Public Function Version() As String
    On Error Resume Next
    Version = App.Major & "." & App.Minor & "." & App.Revision
End Function
