VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CBackends"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'**********************************************************
'   ICIBackends CollectionClass
'   created 12/19/00 by Daniel Fisherman
'   Contains properties and methods
'   that manage a collection of ci backends
'**********************************************************
Option Explicit

Public Enum ciUNIDFields
    ciUNIDFields_Backend = 0
    ciUNIDFields_Store = 1
    ciUNIDFields_Folder = 2
    ciUNIDFields_Listing = 3
    ciUNIDFields_Address = 4
    ciUNIDFields_Contact = 5
End Enum

Public Enum ciUNIDTypes
    ciUNIDType_Backend = 0
    ciUNIDType_Store = 1
    ciUNIDType_Folder = 2
    ciUNIDType_Listing = 3
    ciUNIDType_Address = 4
    ciUNIDType_Contact = 5
End Enum

Private m_oCol As Collection

'******************************************************************
'Methods
'******************************************************************
Public Function ProgID(ByVal iBackendID As Integer) As String
    Dim xProgID As String
    Dim xDesc As String
    Dim oIni As CIni
    
    On Error GoTo ProcError
    Set oIni = New CIni
    
    'get from ini
    xProgID = oIni.GetIni("Backend" & iBackendID, "ProgID")
    
    'raise error if ini value is missing
    If Len(xProgID) = 0 Then
        xDesc = "Invalid Backend" & iBackendID & "\ProgID key in ci.ini."
        Err.Raise ciErr_MissingOrInvalidINIKey, , xDesc
        Exit Function
    End If
    
    ProgID = xProgID
    Exit Function
ProcError:
    g_oError.RaiseError "CBackends.ProgID"
End Function

Friend Function Add(ByVal iID As Integer) As CIO.ICIBackend
'creates a new backend with id = iID, then
'adds it to the collection.
'returns the new backend
    Dim xProgID As String
    Dim xDesc As String
    Dim oIni As CIni
    Dim oB As CIO.ICIBackend
    
    On Error GoTo ProcError
    Set oIni = New CIni
    
    'get ini required ini values
    xProgID = Me.ProgID(iID)
    
    'create backend object
    Set oB = CreateObject(xProgID)
    
    'initialize
    oB.Initialize iID
    
    'add to collection
    On Error Resume Next
    Err.Clear
    m_oCol.Add oB, CStr(iID)
    If Err Then
        'could not add to collection - alert and exit
        On Error GoTo ProcError
        xDesc = "Could not add backend " & iID & "to collection of backends. " & _
            Err.Description
        Err.Raise Err.Number, , xDesc
        Exit Function
    Else
        On Error GoTo ProcError
        Set Add = oB
    End If
    Exit Function
ProcError:
    g_oError.RaiseError "CIO.ICIBackends.Add"
End Function

Public Function Item(ByVal iID As Integer) As CIO.ICIBackend
Attribute Item.VB_UserMemId = 0
'will return Nothing if an error is generated
    On Error Resume Next
    Set Item = m_oCol.Item(CStr(iID))
End Function

Public Function Count() As Integer
    Count = m_oCol.Count
End Function

Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
    Set NewEnum = m_oCol.[_NewEnum]
End Property

Private Sub Class_Initialize()
    Set m_oCol = New Collection
End Sub

Private Sub Class_Terminate()
    Set m_oCol = Nothing
End Sub
