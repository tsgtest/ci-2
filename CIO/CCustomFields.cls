VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CCustomFields"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

'**********************************************************
'   CustomFields Collection Class
'   created 6/30/99 by Daniel Fisherman
'   Contains properties and methods that manage the
'   collection of CustomFields of a contact
'**********************************************************

'**********************************************************
Private m_CustomField As CIO.CCustomField
Private m_colCustomFields As Collection
Private m_debugID As Long
'**********************************************************

'**********************************************************
'initializer/terminator
'**********************************************************
Dim xObjectID As String
Private Sub Class_Initialize()
    Math.Randomize
    xObjectID = "CCustomFields" & Format(Now, "hhmmss") & Math.Rnd()
    g_oObjects.Add xObjectID, xObjectID
    DebugPrint "Added - " & xObjectID & ": " & g_oObjects.Count
    Set m_colCustomFields = New Collection
End Sub

Private Sub Class_Terminate()
    g_oObjects.Remove xObjectID
    DebugPrint "Removed - " & xObjectID & ": " & g_oObjects.Count
    Set m_CustomField = Nothing
    Set m_colCustomFields = Nothing
End Sub
'**********************************************************

Public Function Add(ByVal vID As Variant, _
                    ByVal xName As String, _
                    Optional ByVal xValue As String) As CIO.CCustomField
    On Error GoTo ProcError
    Set m_CustomField = New CIO.CCustomField
    With m_CustomField
        .ID = vID
        .Name = xName
        .Value = xValue
    End With
    m_colCustomFields.Add m_CustomField, xName
    Set Add = m_CustomField
    Exit Function
ProcError:
    g_oError.RaiseError "CIO.CCustomFields.Add"
    Exit Function
End Function

Friend Function Delete(vKey As Variant)
    On Error GoTo ProcError
    m_colCustomFields.Remove vKey
    Exit Function
ProcError:
    g_oError.RaiseError "CIO.CCustomFields.Delete"
    Exit Function
End Function

Public Function Count() As Integer
    On Error GoTo ProcError
    Count = m_colCustomFields.Count
    Exit Function
ProcError:
    g_oError.RaiseError "CIO.CCustomFields.Count"
    Exit Function
End Function

Public Function Item(vKey As Variant) As CIO.CCustomField
Attribute Item.VB_UserMemId = 0
    On Error Resume Next
    Set Item = m_colCustomFields.Item(vKey)
End Function

Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
    Set NewEnum = m_colCustomFields.[_NewEnum]
End Property
