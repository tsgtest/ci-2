VERSION 5.00
Begin VB.Form frmAlertNoContactNumbers 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "##"
   ClientHeight    =   1704
   ClientLeft      =   48
   ClientTop       =   432
   ClientWidth     =   5544
   Icon            =   "frmNoContactNumbers.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1704
   ScaleWidth      =   5544
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CheckBox chkPrompt 
      Caption         =   "&Continue to alert when no contact numbers are found."
      Height          =   336
      Left            =   996
      TabIndex        =   1
      Top             =   780
      Value           =   1  'Checked
      Width           =   4500
   End
   Begin VB.PictureBox Picture1 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      DrawStyle       =   5  'Transparent
      ForeColor       =   &H80000008&
      Height          =   450
      Left            =   285
      Picture         =   "frmNoContactNumbers.frx":000C
      ScaleHeight     =   456
      ScaleWidth      =   432
      TabIndex        =   3
      Top             =   420
      Width           =   435
   End
   Begin VB.CommandButton btnYes 
      Caption         =   "O&K"
      Height          =   345
      Left            =   2190
      TabIndex        =   0
      Top             =   1215
      Width           =   1125
   End
   Begin VB.Label lblMessage 
      Caption         =   "##"
      Height          =   420
      Left            =   960
      TabIndex        =   2
      Top             =   312
      Width           =   4512
   End
End
Attribute VB_Name = "frmAlertNoContactNumbers"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private m_CancelInsert As Boolean

'**********************************************************
'initializer/terminator
'**********************************************************
Dim xObjectID As String
Private Sub Class_Initialize()
    Math.Randomize
    xObjectID = "frmAlertNoContactNumbers" & Format(Now, "hhmmss") & Math.Rnd()
    g_oObjects.Add xObjectID, xObjectID
    DebugPrint "Added - " & xObjectID & ": " & g_oObjects.Count
End Sub

Private Sub Class_Terminate()
    g_oObjects.Remove xObjectID
    DebugPrint "Removed - " & xObjectID & ": " & g_oObjects.Count
End Sub
'**********************************************************

Public Property Get CancelInsert() As Boolean
    CancelInsert = m_CancelInsert
End Property

Public Property Let CancelInsert(bNew As Boolean)
    m_CancelInsert = bNew
End Property

Private Sub btnYes_Click()
    Me.Hide
    DoEvents
    Me.CancelInsert = False
End Sub

'Private Sub Form_Paint()
'    'Load icon
'    With g_oSessionType
'        If (.SessionType = ciSession_Connect) Then
'            .SetIcon Me.hWnd, "CONNECTICON", False
'        Else
'            .SetIcon Me.hWnd, "TSGICON", False
'        End If
'    End With
'
'End Sub
