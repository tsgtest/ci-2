VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CFilterFields"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Attribute VB_Ext_KEY = "SavedWithClassBuilder" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

'**********************************************************
'   CFilterFields Collection Class
'   created 12/23/00 by Daniel Fisherman
'   Contains properties and methods that manage a
'   collection of CFilterFields
'**********************************************************

'**********************************************************
Private m_oFF As CIO.CFilterField
Private m_oFFCol As Collection
'**********************************************************

Friend Function Add(xName As String, xID As Variant) As CIO.CFilterField
'adds a filter field to collection
    On Error GoTo ProcError
    
    'create the filter field
    Set m_oFF = New CIO.CFilterField
    With m_oFF
        .ID = xID
        .Name = xName
    End With
    
    'attempt to add to collection
    On Error Resume Next
    m_oFFCol.Add m_oFF, m_oFF.Name
    If Err Then
        On Error GoTo ProcError
        Dim xDesc As String
        xDesc = "Could not add '" & xName & _
            "' as a filter field.  " & Err.Description
        Err.Raise Err.Number, , xDesc
    End If
    Set Add = m_oFF
    Exit Function
ProcError:
    g_oError.RaiseError "CFilterFields.Add"
End Function

Friend Function Delete(vKey As Variant)
    m_oFFCol.Remove vKey
End Function

Public Function Count() As Integer
    Count = m_oFFCol.Count
End Function

Public Function NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
    Set NewEnum = m_oFFCol.[_NewEnum]
End Function

Public Function Item(vKey As Variant) As CIO.CFilterField
Attribute Item.VB_UserMemId = 0
    Set Item = m_oFFCol.Item(vKey)
End Function

Private Sub Class_Initialize()
    Set m_oFFCol = New Collection
End Sub

Private Sub Class_Terminate()
    Set m_oFF = Nothing
    Set m_oFFCol = Nothing
End Sub
