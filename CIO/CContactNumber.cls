VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CContactNumber"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Public Enum ciContactNumberTypes
    ciContactNumberType_Phone = 1
    ciContactNumberType_Fax = 2
    ciContactNumberType_EMail = 3
End Enum

Private m_xDesc As String
Private m_xNumber As String
Private m_xExtension As String
Private m_xCategory As String
Private m_iType As Integer
Private m_vID As Variant

'**********************************************************
'initializer/terminator
'**********************************************************
Dim xObjectID As String
Private Sub Class_Initialize()
    Math.Randomize
    xObjectID = "CContactNumber" & Format(Now, "hhmmss") & Math.Rnd()
    g_oObjects.Add xObjectID, xObjectID
    DebugPrint "Added - " & xObjectID & ": " & g_oObjects.Count
End Sub

Private Sub Class_Terminate()
    g_oObjects.Remove xObjectID
    DebugPrint "Removed - " & xObjectID & ": " & g_oObjects.Count
End Sub
'**********************************************************

Public Property Let ID(vNew As Variant)
    m_vID = vNew
End Property

Public Property Get ID() As Variant
    ID = m_vID
End Property

Public Property Let NumberType(iNew As ciContactNumberTypes)
    m_iType = iNew
End Property

Public Property Get NumberType() As ciContactNumberTypes
    NumberType = m_iType
End Property

Public Property Let Category(xNew As String)
    m_xCategory = xNew
End Property

Public Property Get Category() As String
    Category = m_xCategory
End Property

Public Property Let Description(xNew As String)
    m_xDesc = xNew
End Property

Public Property Get Description() As String
    Description = m_xDesc
End Property

Public Property Let Number(xNew As String)
    m_xNumber = xNew
End Property

Public Property Get Number() As String
    Number = m_xNumber
End Property

Public Property Let Extension(xNew As String)
    m_xExtension = xNew
End Property

Public Property Get Extension() As String
    Extension = m_xExtension
End Property

