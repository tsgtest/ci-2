VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CCustomField"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'**********************************************************
'   CustomField Class
'   created 6/30/99 by Daniel Fisherman
'   Contains properties and methods concerning
'   a contact's custom fields - the custom
'   fields collection is a member of the contact class
'**********************************************************

Private m_xName As String
Private m_xValue As String
Private m_vID As Variant

'**********************************************************
'initializer/terminator
'**********************************************************
Dim xObjectID As String
Private Sub Class_Initialize()
    Math.Randomize
    xObjectID = "CCustomField" & Format(Now, "hhmmss") & Math.Rnd()
    g_oObjects.Add xObjectID, xObjectID
    DebugPrint "Added - " & xObjectID & ": " & g_oObjects.Count
End Sub

Private Sub Class_Terminate()
    g_oObjects.Remove xObjectID
    DebugPrint "Removed - " & xObjectID & ": " & g_oObjects.Count
End Sub
'**********************************************************

'**********************************************************
'Properties
'**********************************************************
Public Property Let ID(vID As Variant)
    m_vID = vID
End Property

Public Property Get ID() As Variant
    ID = m_vID
End Property

Public Property Let Name(xNew As String)
    m_xName = xNew
End Property

Public Property Get Name() As String
    Name = m_xName
End Property

Public Property Let Value(xNew As String)
    m_xValue = xNew
End Property

Public Property Get Value() As String
Attribute Value.VB_UserMemId = 0
    Value = m_xValue
End Property

