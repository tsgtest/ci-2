VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CApplication"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'**********************************************************
'   CApplication CollectionClass
'   created 10/11/00 by Daniel Fisherman
'   Contains properties and methods
'   that manipulate the CI objects
'**********************************************************
Option Explicit

Public Enum ciUNIDFields
    ciUNIDFields_Backend = 0
    ciUNIDFields_Store = 1
    ciUNIDFields_Folder = 2
    ciUNIDFields_Listing = 3
    ciUNIDFields_Address = 4
    ciUNIDFields_Contact = 5
End Enum

Public Enum ciUNIDTypes
    ciUNIDType_Backend = 0
    ciUNIDType_Store = 1
    ciUNIDType_Folder = 2
    ciUNIDType_Listing = 3
    ciUNIDType_Address = 4
    ciUNIDType_Contact = 5
End Enum

Public Function GetStore(ByVal xUNID As String, Optional ByVal xName As String) As CIO.CStore
'returns the store corresponding to the specified UNID
    Dim oStore As CIO.CStore
    
    On Error GoTo ProcError
    
    Set oStore = New CIO.CStore
    oStore.UNID = xUNID
    If Len(xName) Then
        oStore.Name = xName
    End If
    Set GetStore = oStore
    Exit Function
ProcError:
    g_oError.RaiseError "CSession.GetStore"
End Function

Public Function GetFolder(xUNID As String, Optional ByVal xName As String) As CIO.CFolder
'returns the folder corresponding to the specified UNID
    Dim oFolder As CIO.CFolder
    
    On Error GoTo ProcError
    
    Set oFolder = New CIO.CFolder
    oFolder.UNID = xUNID
    If Len(xName) Then
        oFolder.Name = xName
    End If
    Set GetFolder = oFolder
    Exit Function
ProcError:
    g_oError.RaiseError "CSession.GetFolder"
End Function

Public Function GetListing(xUNID As String, Optional ByVal xDisplayName As String) As CIO.CListing
'returns the Listing corresponding to the specified UNID
    Dim oListing As CIO.CListing
    
    On Error GoTo ProcError
    
    Set oListing = New CIO.CListing
    oListing.UNID = xUNID
    If Len(xDisplayName) Then
        oListing.DisplayName = xDisplayName
    End If
    Set GetListing = oListing
    Exit Function
ProcError:
    g_oError.RaiseError "CSession.GetListing"
End Function

Public Function GetAddress(xUNID As String, Optional ByVal xName As String) As CIO.CAddress
'returns the Address corresponding to the specified UNID
    Dim oAddress As CIO.CAddress
    
    On Error GoTo ProcError
    
    Set oAddress = New CIO.CAddress
    oAddress.UNID = xUNID
    If Len(xName) Then
        oAddress.Name = xName
    End If
    Set GetAddress = oAddress
    Exit Function
ProcError:
    g_oError.RaiseError "CSession.GetAddress"
End Function

Public Function GetUNIDField(ByVal vUNID As Variant, ByVal iField As ciUNIDFields)
    On Error GoTo ProcError
    GetUNIDField = mdlGeneral.GetUNIDField(vUNID, iField)
    Exit Function
ProcError:
    g_oError.RaiseError "CBackends.GetUNIDField"
End Function

Public Function GetBackendName(ByVal iBackendID As Integer) As String
    Dim xName As String
    Dim xDesc As String
    Dim oIni As CIni
    
    On Error GoTo ProcError
    Set oIni = New CIni
    
    'get from ini
    xName = oIni.GetIni("Backend" & iBackendID, "Name")
    
    'raise error if ini value is missing
    If Len(xName) = 0 Then
        xDesc = "Invalid Backend" & iBackendID & "\Name key in ci.ini."
        Err.Raise ciErr_MissingOrInvalidINIKey, , xDesc
        Exit Function
    End If
    
    GetBackendName = xName
    Exit Function
ProcError:
    g_oError.RaiseError "CBackends.Name"
End Function

Public Function GetUNIDType(xUNID As String) As ciUNIDTypes
'returns the type of UNID represented by xUNID
    Dim bytNumSep As Byte
    Dim oStr As CStrings
    Dim oConst As CConstants
    
    On Error GoTo ProcError
    
    Set oStr = New CStrings
    Set oConst = New CConstants
    
    'count the number of separators
    bytNumSep = oStr.lCountChrs(xUNID, oConst.UNIDSep)
    
    'number of separators matches type id
    GetUNIDType = bytNumSep
    
    Exit Function
ProcError:
    g_oError.RaiseError "CSession.GetUNIDType"
End Function

Public Function GetFilter(ByVal iBackendID As Integer) As CIO.CFilter
'returns the collection of filter fields for specified backend
    On Error GoTo ProcError
    Set GetFilter = g_oSession.Backends(iBackendID).Filter()
    Exit Function
ProcError:
    g_oError.RaiseError "CSession.GetFilter"
End Function

