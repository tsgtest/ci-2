VERSION 5.00
Object = "{0D62353D-DBA2-11D1-B5DF-0060976089D0}#6.0#0"; "TDBL6.OCX"
Begin VB.Form frmFilter 
   BackColor       =   &H80000016&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   " Find"
   ClientHeight    =   4512
   ClientLeft      =   48
   ClientTop       =   336
   ClientWidth     =   6768
   Icon            =   "frmFilter.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4512
   ScaleWidth      =   6768
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin TrueDBList60.TDBCombo cmbSearchTypes 
      Height          =   360
      Left            =   480
      OleObjectBlob   =   "frmFilter.frx":000C
      TabIndex        =   9
      Top             =   3810
      Width           =   4005
   End
   Begin VB.CommandButton btnSearch 
      Caption         =   "O&K"
      Default         =   -1  'True
      Height          =   400
      Left            =   5325
      TabIndex        =   10
      ToolTipText     =   "Find Contacts"
      Top             =   3375
      Width           =   1200
   End
   Begin VB.Frame fraFilter 
      BackColor       =   &H80000016&
      Caption         =   "Display Contacts Where:"
      Height          =   3135
      Left            =   285
      TabIndex        =   12
      Top             =   210
      Width           =   4485
      Begin VB.TextBox txtFilterField 
         Height          =   325
         Index           =   0
         Left            =   195
         TabIndex        =   1
         Top             =   585
         Width           =   4000
      End
      Begin VB.TextBox txtFilterField 
         Height          =   325
         Index           =   1
         Left            =   195
         TabIndex        =   3
         Top             =   1230
         Width           =   4000
      End
      Begin VB.TextBox txtFilterField 
         Height          =   325
         Index           =   2
         Left            =   180
         TabIndex        =   5
         Top             =   1890
         Width           =   4000
      End
      Begin VB.TextBox txtFilterField 
         Height          =   325
         Index           =   3
         Left            =   180
         TabIndex        =   7
         Top             =   2550
         Width           =   4000
      End
      Begin VB.Label lblFilterField 
         BackStyle       =   0  'Transparent
         Caption         =   "##"
         Height          =   225
         Index           =   3
         Left            =   255
         TabIndex        =   6
         Top             =   2325
         Width           =   3975
      End
      Begin VB.Label lblFilterField 
         BackStyle       =   0  'Transparent
         Caption         =   "##"
         Height          =   225
         Index           =   2
         Left            =   255
         TabIndex        =   4
         Top             =   1665
         Width           =   3975
      End
      Begin VB.Label lblFilterField 
         BackStyle       =   0  'Transparent
         Caption         =   "##"
         Height          =   225
         Index           =   1
         Left            =   255
         TabIndex        =   2
         Top             =   1020
         Width           =   3975
      End
      Begin VB.Label lblFilterField 
         BackStyle       =   0  'Transparent
         Caption         =   "##"
         Height          =   225
         Index           =   0
         Left            =   255
         TabIndex        =   0
         Top             =   360
         Width           =   3975
      End
   End
   Begin VB.CommandButton btnCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   400
      Left            =   5325
      TabIndex        =   11
      Top             =   3870
      Width           =   1200
   End
   Begin VB.Label lblType 
      BackStyle       =   0  'Transparent
      Caption         =   "F&ind Contacts:"
      Height          =   240
      Left            =   525
      TabIndex        =   8
      Top             =   3585
      Width           =   1725
   End
End
Attribute VB_Name = "frmFilter"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public Enum ciFilterActions
    ciFilterAction_Cancel = 0
    ciFilterAction_Search = 1
    ciFilterAction_OutlookSearch = 2
    ciFilterAction_ClearSearch = 3
End Enum

Private m_bCancelled As Boolean
Private m_bSearchNow As Boolean
Private xMsg As String
Private m_oBackend As CIO.ICIBackend
Private m_iFilterAction As ciFilterActions
Private m_iSortCol As ciListingCols

Public Property Set Backend(oNew As CIO.ICIBackend)
    Set m_oBackend = oNew
End Property

Private Property Let Action(iNew As ciFilterActions)
    m_iFilterAction = iNew
End Property

Public Property Get Action() As ciFilterActions
    Action = m_iFilterAction
End Property

Private Sub btnCancel_Click()
    Me.Hide
    Action = ciFilterAction_Cancel
End Sub

Private Sub btnSearch_Click()
    Action = ciFilterAction_Search
    
    'write sort and search type to user ini
    g_oIni.SetUserIni "CIPL", "SearchType", Me.cmbSearchTypes.BoundText
    On Error Resume Next
    Me.Hide
    DoEvents
End Sub

Private Sub cmbSearchTypes_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cmbSearchTypes, Reposition
    Exit Sub
ProcError:
    g_oError.ShowError
    Exit Sub
End Sub

Private Sub cmbSearchTypes_Validate(Cancel As Boolean)
    On Error GoTo ProcError
    If Me.cmbSearchTypes.Text <> Empty Then
        bValidateBoundTDBCombo Me.cmbSearchTypes
    End If
    Exit Sub
ProcError:
    g_oError.ShowError
    Exit Sub
End Sub

Private Sub Form_Activate()
    Dim i As Integer
    Dim xSort As String
    Dim xSearchType As String
    
'   select contents of first criterion
    On Error GoTo ProcError
    With Me.txtFilterField(0)
        .SetFocus
        .SelStart = 0
        .SelLength = Len(.Text)
    End With
    
    xSearchType = g_oIni.GetUserIni("CIPL", "SearchType")
    If xSearchType = Empty Then
        'no default set - use first col (Display Name)
        xSearchType = ciSearchOperator_BeginsWith
    End If
        
    Me.cmbSearchTypes.BoundText = xSearchType
    
'TODO:
'   ensure correct sort field - can
'   be changed elsewhere in interface
    Exit Sub
ProcError:
    g_oError.ShowError
    Exit Sub
End Sub

Private Sub Form_Load()
    Dim i As Integer
    Dim iDisplay As Integer
    Dim iNumFields As Integer
    Dim oFilter As CIO.CFilter
    Dim oArray As XArrayDB
    
    Action = ciFilterAction_Cancel
    
    Set oArray = New XArrayDB
    With oArray
        .ReDim 0, 2, 0, 1
        .Value(0, 0) = "That begin with..."
        .Value(0, 1) = ciSearchOperator_BeginsWith
        .Value(1, 0) = "That contain..."
        .Value(1, 1) = ciSearchOperator_Contains
        .Value(2, 0) = "That have the value..."
        .Value(2, 1) = ciSearchOperator_Equals
    End With
    
    Me.cmbSearchTypes.Array = oArray
    ResizeTDBCombo Me.cmbSearchTypes, 3
    
    With m_oBackend.Filter
        iNumFields = .CountFields
        
        For i = 1 To iNumFields
            With .FilterFields(i)
'               set caption for each filter field control
                Me.lblFilterField(i - 1).Caption = "&" & .Name & ":"
            End With
            
        Next i
        
'       show only those controls corresponding
'       to existing filter fields
        For i = 1 To 4
            Me.lblFilterField(i - 1).Visible = i <= iNumFields
            Me.txtFilterField(i - 1).Visible = i <= iNumFields
        Next i
        
        With Me.fraFilter
'           adjust frame for number of filter field controls
            If iNumFields < 4 Then
                .Height = .Height - ((4 - iNumFields) * (.Height / 4)) + _
                          (4 - iNumFields) * 100
            End If
        End With
    End With
    
    
End Sub

Public Sub ResetFilter()
    Dim i As Integer
    On Error Resume Next
    With Me.txtFilterField
        For i = .LBound To .UBound
            .Item(i).Text = ""
        Next i
    End With
End Sub

Private Sub txtFilterField_GotFocus(Index As Integer)
    With Me.txtFilterField(Index)
        .SelStart = 0
        .SelLength = Len(.Text)
    End With
End Sub

Public Sub ResizeTDBCombo(tdbCBX As TrueDBList60.TDBCombo, Optional iMaxRows As Integer)
'resizes the dropdown of the true db combo
'to display the specified number of rows.
    Dim oArr As XArrayDB
    Dim iRows As Integer
    
    On Error GoTo ProcError
    With tdbCBX
        Set oArr = .Array
        
        If oArr.Count(1) < iMaxRows Then
            iRows = oArr.Count(1)
        Else
            iRows = iMaxRows
        End If
            
        On Error Resume Next
        .DropdownHeight = ((iRows) * .RowHeight + 8)
        
    End With
    Exit Sub
ProcError:
    Err.Raise Err.Number, "CIX.mdlMain.ResizeTDBCombo", Err.Description
    Exit Sub
End Sub

