Attribute VB_Name = "mdlGlobal"
'*****************************************************************
'Notes backend for CI 2.x
'Created by Daniel Fisherman on 7/21/03
'*****************************************************************
Private Declare Function GetTickCount Lib "kernel32" () As Long

Public g_oError As CIO.CError
Public g_oGlobals As CIO.CGlobals
Public g_oIni As CIO.CIni
Public g_oReg As CIO.CRegistry
Public g_oConstants As CIO.CConstants
Public g_iID As Integer
Public g_oSessionType As CIO.CSessionType


Public g_vCols(3, 1) As Variant
Public g_oSession As NotesSession
Public g_xStoreDetail() As String


Public Sub CorrectTDBComboMismatch(oTDBCombo As TDBCombo, iReposition As Integer)
'resets the tdb combo value to the previous match -
'this procedure should be called in the
'TDBCombo Mismatch even procedure only
    Dim bytStart As Byte
    
    iReposition = False
    
    With oTDBCombo
'       get current selection start
        bytStart = .SelStart
        
'       reset the text to the current list text
        If .ListField = Empty Then
            .Text = .Columns(0)
        Else
            .Text = .Columns(.ListField)
        End If
        
'       return selection to original selection
        .SelStart = Max(CDbl(bytStart - 1), 0)
        .SelLength = Len(.Text)
    End With
End Sub

Function Max(i As Double, j As Double) As Double
    If i > j Then
        Max = i
    Else
        Max = j
    End If
End Function

Function Min(i As Double, j As Double) As Double
    If i > j Then
        Min = j
    Else
        Min = i
    End If
End Function

Public Function bValidateBoundTDBCombo(oTDBCombo As TDBCombo) As Boolean
'returns FALSE if invalid author has been specified, else TRUE
    With oTDBCombo
        If .BoundText = "" Then
            MsgBox "Invalid entry.  Please select an item " & _
                   "from the list.", vbExclamation, g_oSessionType.AppTitle
        Else
'           set text of control equal to the
'           display text of the selected row
            .Text = .Columns(.ListField)
            bValidateBoundTDBCombo = True
        End If
    End With
End Function

Function CurrentTick() As Long
    CurrentTick = GetTickCount()
End Function

Function IsEven(dNum As Double) As Boolean
    If dNum / 2 = CLng(dNum / 2) Then
        IsEven = True
    End If
End Function

Function ElapsedTime(lStartTick As Long) As Single
'returns the time elapsed from lStartTick-
'precision in milliseconds
    ElapsedTime = Format((CurrentTick() - lStartTick) / 1000, "#,##0.0000")
End Function

Public Function StoreExists(ByVal xServer As String, ByVal xFilePath As String) As Boolean
'returns True iff a store having the specified server and file path already exists
    Dim i As Integer
    
    On Error GoTo ProcError
    'cycle through store detail looking for a match
    For i = 0 To UBound(g_xStoreDetail, 2)
        If g_xStoreDetail(1, i) = xServer And _
            InStr(g_xStoreDetail(2, i), xFilePath) Then
                StoreExists = True
                Exit Function
        End If
    Next i
    Exit Function
ProcError:
    g_oError.RaiseError "CINotes.mdlGlobal.StoreExists"
    Exit Function
End Function


Public Function GetAlertThreshold() As Long
'returns the maximum number of contacts that can be retrieved before warning
    Dim xAlertThreshold As String
    Dim lAlertThreshold As Long
    
    If g_oIni Is Nothing Then
        Set g_oIni = New CIO.CIni
    End If
    
    On Error Resume Next
    xAlertThreshold = g_oIni.GetIni("Backend" & g_iID, "AlertThreshold", g_oIni.CIIni)
    On Error GoTo ProcError
    If xAlertThreshold = Empty Or Not IsNumeric(xAlertThreshold) Then
        lAlertThreshold = 500
    Else
        lAlertThreshold = CLng(xAlertThreshold)
    End If
    GetAlertThreshold = lAlertThreshold
    Exit Function
ProcError:
    g_oError.RaiseError "CINotes.mdlGlobal.GetAlertThreshold"
    Exit Function
End Function

Public Function GetNumberOfAdditionalStores() As Integer
    Dim i As Integer
    Dim xVal As String
    
    On Error GoTo ProcError
    i = 1
    xVal = g_oIni.GetUserIni("AdditionalNotesStores", i)
    While Len(xVal)
        i = i + 1
        xVal = ""
        xVal = g_oIni.GetUserIni("AdditionalNotesStores", i)
        GetNumberOfAdditionalStores = i - 1
    Wend
    Exit Function
ProcError:
    g_oError.RaiseError "CINotes.mdlGlobal.GetNumberOfAdditionalStores"
    Exit Function
End Function

