VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.3#0"; "Comctl32.ocx"
Begin VB.Form frmGetNotesDB 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Add Notes Database"
   ClientHeight    =   4680
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5220
   Icon            =   "frmGetNotesDB.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4680
   ScaleWidth      =   5220
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin ComctlLib.TreeView tvDB 
      Height          =   2790
      Left            =   165
      TabIndex        =   1
      Top             =   645
      Width           =   4815
      _ExtentX        =   8493
      _ExtentY        =   4921
      _Version        =   327682
      LabelEdit       =   1
      Sorted          =   -1  'True
      Style           =   7
      Appearance      =   1
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.CommandButton btnCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   420
      Left            =   3885
      TabIndex        =   4
      Top             =   4140
      Width           =   1100
   End
   Begin VB.CommandButton btnOK 
      Caption         =   "O&K"
      Height          =   420
      Left            =   2670
      TabIndex        =   3
      Top             =   4140
      Width           =   1100
   End
   Begin VB.TextBox txtFilePath 
      Height          =   360
      Left            =   165
      Locked          =   -1  'True
      TabIndex        =   2
      TabStop         =   0   'False
      Top             =   3555
      Width           =   4815
   End
   Begin VB.ComboBox cmbServer 
      Height          =   330
      Left            =   165
      Style           =   2  'Dropdown List
      TabIndex        =   0
      Top             =   210
      Width           =   4815
   End
   Begin ComctlLib.ImageList ilTree 
      Left            =   0
      Top             =   0
      _ExtentX        =   1005
      _ExtentY        =   1005
      BackColor       =   -2147483643
      ImageWidth      =   16
      ImageHeight     =   16
      MaskColor       =   12632256
      _Version        =   327682
      BeginProperty Images {0713E8C2-850A-101B-AFC0-4210102A8DA7} 
         NumListImages   =   3
         BeginProperty ListImage1 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmGetNotesDB.frx":000C
            Key             =   "Backend"
         EndProperty
         BeginProperty ListImage2 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmGetNotesDB.frx":0326
            Key             =   "ClosedFolder"
         EndProperty
         BeginProperty ListImage3 {0713E8C3-850A-101B-AFC0-4210102A8DA7} 
            Picture         =   "frmGetNotesDB.frx":0640
            Key             =   "OpenFolder"
         EndProperty
      EndProperty
   End
End
Attribute VB_Name = "frmGetNotesDB"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private m_xSrvs() As String
Private m_oArr As XArrayDB
Private m_bCancelled As Boolean
Private m_bSvrChanged As Boolean
Private m_aDBs() As NotesDatabase

Public Property Get Cancelled() As Boolean
    Cancelled = m_bCancelled
End Property

Public Property Get Server() As String
    If Me.cmbServer <> "Local" Then
        Server = Me.cmbServer
    End If
End Property

Public Property Get FilePath() As String
    FilePath = Me.txtFilePath
End Property

Private Sub btnCancel_Click()
    Me.Hide
    DoEvents
    m_bCancelled = True
End Sub

Private Sub btnOK_Click()
    
    On Error GoTo ProcError
    'validate
    Dim oDB As NotesDatabase
    
    On Error Resume Next
    Set oDB = g_oSession.GetDatabase(m_xSrvs(1, Me.cmbServer.ListIndex), Me.txtFilePath, False)
    On Error GoTo ProcError
    
    If oDB Is Nothing Then
        MsgBox "Selection is not a database. Please select an address book database.", vbExclamation, g_oSessionType.AppTitle
        Exit Sub
    Else
        If Not oDB.IsOpen Then
            oDB.Open
        End If
        
        If oDB.Type <> DBTYPE_ADDR_BOOK Then
            MsgBox "The selected database is not an address book database. " & _
                "Please select an address book database.", vbExclamation, g_oSessionType.AppTitle
            Exit Sub
        End If
    End If
    
    If StoreExists(Me.Server, Me.FilePath) Then
        MsgBox "The selected database already exists in the " & _
            "list of available databases.", vbExclamation, g_oSessionType.AppTitle
    Else
        Me.Hide
        DoEvents
        m_bCancelled = False
    End If
    
    Exit Sub
ProcError:
    g_oError.ShowError
    Exit Sub
End Sub

Private Sub cmbServer_Change()
    m_bSvrChanged = True
End Sub

Private Sub cmbServer_Click()
    If Me.cmbServer.ListIndex < Me.cmbServer.ListCount - 1 Then
        Me.tvDB.Nodes.Clear
        GetDBs Nothing
    Else
        AddServer
    End If
    Exit Sub
ProcError:
    Dim lErr As Long
    Dim xDesc As String

    lErr = Err.Number
    xDesc = Err.Description

    Screen.MousePointer = vbDefault
    
    Err.Number = lErr
    Err.Description = xDesc
    
    g_oError.RaiseError "frmGetNotesDB.cmbServer.Click"
End Sub

Private Sub AddServer()
    Dim xServer As String
    Dim oDBDir As NotesDbDirectory
    Dim oDB As NotesDatabase
    
    'prompt for server input
    xServer = InputBox("Enter Server Name:", g_oSessionType.AppTitle)
    
    'check for entry, then validate
    While Len(xServer)
        Me.Refresh
        Screen.MousePointer = vbHourglass
        
        'validate server - see if we can get a db on the server
        On Error Resume Next
        Set oDBDir = g_oSession.GetDbDirectory(xServer)
        Set oDB = oDBDir.GetFirstDatabase(NOTES_DATABASE)
        On Error GoTo ProcError
        
        Screen.MousePointer = vbDefault
        
        If (oDB Is Nothing) Then
            'invalid server
            Dim xMsg As String
            xMsg = "Could not connect to '" & xServer & "'.  " & _
                "Please check the name that you entered."
            MsgBox xMsg, vbExclamation
            xServer = InputBox("Enter Server Name:", g_oSessionType.AppTitle, xServer)
        Else
            'valid server - add to list
            
            'add server before "other" -  this overwrites current "other"
            m_xSrvs(0, UBound(m_xSrvs, 2)) = xServer
            m_xSrvs(1, UBound(m_xSrvs, 2)) = xServer
            
            ReDim Preserve m_xSrvs(1, UBound(m_xSrvs, 2) + 1)
            
            'add back "other"
            m_xSrvs(0, UBound(m_xSrvs, 2)) = "Other..."
            
            'add to list before "other", which is the last entry
            With Me.cmbServer
                .AddItem xServer, .ListCount - 1
                .ListIndex = .ListCount - 2
            End With
            Exit Sub
        End If
    Wend
    Exit Sub
ProcError:
    Err.Raise Err.Number, "frmGetNotesDB.AddServer", Err.Description
End Sub

Private Sub GetDBs(oParentNode As ComctlLib.Node)
    Dim oDBDir As NotesDbDirectory
    Dim oDB As NotesDatabase
    Dim i As Integer
    
    Me.Refresh
    Me.MousePointer = vbHourglass
    
    If oParentNode Is Nothing Then
        'server has been switched - get dbs for first time
        'create enough space for dbs - trim later
        ReDim m_aDBs(10000)
    End If
    
    If m_aDBs(0) Is Nothing Then
        'dbs haven't been stored in array -
        'get and store for future use
        Set oDBDir = g_oSession.GetDbDirectory( _
                    m_xSrvs(1, Me.cmbServer.ListIndex))
        
        On Error Resume Next
        
        Set oDB = oDBDir.GetFirstDatabase(NOTES_DATABASE)
        
        On Error GoTo ProcError
        
        While Not oDB Is Nothing
            Set m_aDBs(i) = oDB
            On Error Resume Next
            CreateNode oDB.FilePath, oDB.Title
            On Error GoTo ProcError
            
            Set oDB = Nothing
            On Error Resume Next
            Set oDB = oDBDir.GetNextDatabase()
            
            i = i + 1
            On Error GoTo ProcError
        Wend
        
        If i > 0 Then
            ReDim Preserve m_aDBs(i - 1)
        End If
    Else
        'dbs are in array
        For i = 0 To UBound(m_aDBs)
            On Error Resume Next
            CreateNode m_aDBs(i).FilePath, m_aDBs(i).Title
            On Error GoTo ProcError
            
            Set oDB = Nothing
        Next i
    End If
    Me.MousePointer = vbDefault
    Exit Sub
ProcError:
    Dim lErr As Long
    Dim xDesc As String

    lErr = Err.Number
    xDesc = Err.Description

    Me.MousePointer = vbDefault
    
    Err.Number = lErr
    Err.Description = xDesc
    g_oError.RaiseError "CINotes.frmGetNotesDB.GetDBs"
End Sub

Private Sub CreateNode(ByVal xFilePath As String, ByVal xTitle As String)
'creates the appropriate nodes specified by xFile Path and xTitle
    Dim xPath As String
    Dim xDir As String
    Dim xFileName As String
    Dim iPos As Integer
    Dim oParentNode As Node
    Dim oNode As Node
    
    'get directory path
    iPos = InStrRev(xFilePath, "\")
    If iPos > 0 Then
        xPath = Left$(xFilePath, iPos - 1)
        xFileName = Mid$(xFilePath, iPos + 1)
    Else
        xPath = Empty
        xFileName = xFilePath
    End If
    
    'create a node for each directory in the path, if one doesn't already exist
    While xPath <> Empty
        'get directory
        iPos = InStr(xPath, "\")
        If iPos > 0 Then
            xDir = Left$(xPath, iPos - 1)
            
            'get remainder of path
            xPath = Mid$(xPath, iPos + 1)
        Else
            xDir = xPath
            xPath = ""
        End If
        
        If oParentNode Is Nothing Then
            'check for directory node at top level
            Set oNode = Nothing
            On Error Resume Next
            Set oNode = Me.tvDB.Nodes(xDir)
            On Error GoTo 0
            
            If oNode Is Nothing Then
                'node does not yet exist - add to top level
                Set oNode = Me.tvDB.Nodes.Add(, , xDir, xDir, 2, 3)
            End If
            
            'set node as parent node for next iteration
            Set oParentNode = oNode
        Else
            'check for directory node as child of parent node
            Dim xNodeKey As String
            
            xNodeKey = oParentNode.Key & "\" & xDir
            
            Set oNode = Nothing
            On Error Resume Next
            Set oNode = Me.tvDB.Nodes.Item(xNodeKey)
            On Error GoTo 0
            
            If oNode Is Nothing Then
                'node does not yet exist - add as child to parent node
                Set oNode = Me.tvDB.Nodes.Add(oParentNode, tvwChild, xNodeKey, xDir, 2, 3)
            End If
            
            'set node as parent node for next iteration
            Set oParentNode = oNode
        End If
    Wend
        
    'add db to last directory in path
    If oParentNode Is Nothing Then
        'add db to top level
        Set oNode = Me.tvDB.Nodes.Add(, , xFileName, xTitle, 1, 1)
    Else
        'add db to parent node
        Set oNode = Me.tvDB.Nodes.Add(oNode, tvwChild, xFilePath, xTitle, 1, 1)
    End If
End Sub

'Private Sub CreateNodeOLD(oParentNode As ComctlLib.Node, ByVal xFilePath As String, ByVal xTitle As String)
''creates the appropriate node as a child of oParentNode
'    Dim iPos As Integer
'    Dim xNode As String
'    Dim xParsedFilePath As String
'
'    If oParentNode Is Nothing Then
'        'we're at the root - get first separator
'        iPos = InStr(xFilePath, "\")
'        If iPos Then
'            'db is in a sub dir - create sub dir node if necessary
'            xNode = Left(xFilePath, iPos)
'        Else
'            'db is at root - add db node
'            xNode = xFilePath
'        End If
'
'        If Right(xNode, 1) = "\" Then
'            'add folder node
'            Me.tvDB.Nodes.Add , , xNode, Left(xNode, Len(xNode) - 1), 2, 3
'        ElseIf Len(xTitle) Then
'            'add database node
'            Me.tvDB.Nodes.Add , , xNode, xTitle, 1, 1
'        End If
'    Else
'        'parent node is a sub dir - parse out parent path
'        If Left(UCase(xFilePath), UCase(Len(oParentNode.Key))) = UCase(oParentNode.Key) Then
'            'db is located in this node or sub dir of this node
'
'            'parse out parent node from node path
'            xParsedFilePath = Mid(xFilePath, Len(oParentNode.Key) + 1)
'
'            'create node
'            iPos = InStr(xParsedFilePath, "\")
'            If iPos Then
'                'db is in a sub dir - create sub dir node if necessary
'                xNode = Left(xParsedFilePath, iPos - 1)
'            Else
'                'db is at root - add db node
'                xNode = xParsedFilePath
'            End If
'
'            If Right(xNode, 1) = "\" Then
'                'add folder node
'                Me.tvDB.Nodes.Add oParentNode, tvwChild, xNode, Left(xNode, Len(xNode) - 1), 2, 3
'            ElseIf Len(xTitle) Then
'                'add database node
'                Me.tvDB.Nodes.Add oParentNode, tvwChild, xNode, xTitle, 1, 1
'            End If
'        End If
'    End If
'End Sub

Private Sub cmbServer_LostFocus()
    If m_bSvrChanged Then
        m_bSvrChanged = False
        GetDBs Nothing
    End If
End Sub

Private Sub Form_Load()
    Dim xServer As String
    
    m_bCancelled = True
    
    Me.tvDB.ImageList = Me.ilTree
    
    xServer = g_oSession.GetEnvironmentString("MailServer", True)

    If Len(xServer) Then
        xServer = Replace(xServer, "CN=", "")
        xServer = Replace(xServer, "O=", "")
        xServer = Replace(xServer, "OU=", "")
        ReDim m_xSrvs(1, 2)
        m_xSrvs(0, 0) = "Local"
        m_xSrvs(1, 0) = ""
        m_xSrvs(0, 1) = xServer
        m_xSrvs(1, 1) = xServer
        m_xSrvs(0, 2) = "Other..."
        m_xSrvs(1, 2) = ""
    Else
        ReDim m_xSrvs(1, 1)
        m_xSrvs(0, 0) = "Local"
        m_xSrvs(1, 0) = ""
        m_xSrvs(0, 1) = "Other..."
        m_xSrvs(1, 1) = "Other..."
    End If
    
    With Me.cmbServer
        .AddItem m_xSrvs(0, 0)
        .AddItem m_xSrvs(0, 1)
        On Error Resume Next
        .AddItem m_xSrvs(0, 2)
    End With
    
    Me.cmbServer.ListIndex = 0
    Exit Sub
ProcError:
    Err.Raise Err.Number, Err.Source & ":frmGetNotesDB.Form_Load", Err.Description
End Sub

Private Sub tvDB_DblClick()
    btnOK_Click
End Sub

Private Sub tvDB_NodeClick(ByVal Node As ComctlLib.Node)
    Dim oSelNode As ComctlLib.Node
    
    On Error GoTo ProcError
    Set oSelNode = Me.tvDB.SelectedItem
    
    If oSelNode Is Nothing Then
        Exit Sub
    End If
    
    With oSelNode
        If .Children = 0 Then
            If Right(oSelNode.Key, 1) = "\" Then
                'get subdirs
                GetDBs oSelNode
                If .Children Then
                    .Expanded = True
                End If
            End If
        Else
            .Expanded = Not .Expanded
        End If
        
        Dim xFile As String
        Dim iPos As Integer
        
        iPos = InStr(oSelNode.Key, "|")
        xFile = .Key
        
        Me.txtFilePath = xFile
'        If .Parent Is Nothing Then
'            Me.txtFilePath = xFile
'        Else
'            Me.txtFilePath = .Parent.FullPath & "\" & xFile
'        End If
    End With
    Exit Sub
ProcError:
    Err.Raise Err.Number, "frmGetNotesDB.tvDB_NodeClick"
End Sub
