VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Notes"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit
'**********************************************************
'   Class for use with LOTUS NOTES 5.x
'   created 12/5/00 by Daniel Fisherman-
'   Notes support needed to be built in to
'   CI due to the use of Domino Objects, which
'   are neither MAPI nor ODBC based.
'**********************************************************

Public Enum ciNotesStoresCols
    ciNotesStoresCols_ID = 0
    ciNotesStoresCols_Name = 1
    ciNotesStoresCols_Server = 2
    ciNotesStoresCols_DB = 3
    ciNotesStoresCols_FileName = 4
End Enum

Private m_oSession As NotesSession
Private m_xUserName As String
Private m_xUserID As Variant
Private m_xServer As String
Private m_xDB As String

Public Property Get Session() As NotesSession
    Set Session = m_oSession
End Property

Public Property Get Connect(Driver As String, Server As String, DBName As String) As String
    Dim xDataSource As String
'   sets the store's connection string
    Connect = ("DRIVER={Lotus NotesSQL Driver (*.nsf)}" & _
              ";SERVER=" & m_xServer & _
              ";DATABASE=" & m_xDB & _
              ";DSN=''")
End Property

Public Function InitSession() As Long
'establishes a notes session
    Dim oAddrBk As NotesDatabase

    Dim xMsg As String
    
    'start a notes session
    Set m_oSession = New NotesSession
    With m_oSession
        On Error Resume Next
        'log in
        .Initialize
        If Err.Number = -2147182808 Then
            Exit Function
        ElseIf Err.Number Then
            xMsg = "Could not initialize a Notes session.  The following error was " & _
                "returned by the Notes library: " & vbCr & _
                "Number: " & Err.Number & vbCr & _
                "Description: " & Err.Description
            MsgBox xMsg, vbExclamation
            Exit Function
        End If
        
        'get first notes address book
        On Error Resume Next
        On Error GoTo ProcError
        Dim v As Variant
        v = .AddressBooks
        Set oAddrBk = .AddressBooks(0)
        On Error GoTo ProcError
        
        If oAddrBk Is Nothing Then
            'no address books found - alert and exit
            xMsg = "No Notes Address Books were found.  " & _
                "Please contact your administrator."
            MsgBox xMsg, vbExclamation
            Exit Function
        ElseIf oAddrBk.Server <> "" Then
            'get name of server and db
            m_xServer = oAddrBk.Server
            m_xDB = oAddrBk.FileName
            InitSession = True
        Else
            'get local server and db
            m_xServer = "local"
            m_xDB = oAddrBk.FileName
            InitSession = True
        End If
        
        'get user info
        m_xUserID = .UserName
        m_xUserName = .CommonUserName
    End With
    Exit Function
    
ProcError:
    InitSession = lRet
    Exit Function
End Function

Public Function Show(ListingID As Variant, _
                    StoreID As Variant, _
                    Optional ListingSourceID As Variant, _
                    Optional xShell As String, _
                    Optional WindowState As Integer = vbMinimizedFocus) As Long
    Exit Function
ProcError:
    Show = Err.Number
    Exit Function
End Function

Public Function GetListings(oLS As mpCI.ListingSource, Optional ByVal xSearch As String) As mpCI.Listings
'returns the listings as array
    Dim oDB As NotesDatabase
    Dim oCol As NotesDocumentCollection
    Dim i As Integer
    Dim j As Integer
    Dim oListings As mpCI.Listings
    Dim lNumMsgs As Long
    Dim Cancel As Boolean
    Dim oDoc As Domino.NotesDocument
    Dim oStore As mpCI.Store
    
    Set oStore = oLS.Parent
    
    Set oDB = m_oSession.GetDatabase(oStore.Server, oStore.Database)
    
    If Not oDB.IsOpen Then
        oDB.Open
    End If

    Set oCol = oDB.Search("Type=""Person""", Nothing, 0)
    lNumMsgs = oCol.Count
    If lNumMsgs Then
        Set oListings = New mpCI.Listings
        oListings.Parent = oStore.ListingSources.Item(1)
    End If
        
    On Error Resume Next
    Set oDoc = oCol.GetFirstDocument
    On Error GoTo 0
    
    While Not (oDoc Is Nothing)
        'create array space
        oListings.xArray.ReDim 0, j, 0, 5
        
'       add entry to list
        With oListings.xArray
            .Value(j, xarCols_LastName) = oDoc.GetItemValue("LastName")(0)
            .Value(j, xarCols_FirstName) = oDoc.GetItemValue("FirstName")(0)
            .Value(j, xarCols_Company) = oDoc.GetItemValue("CompanyName")(0)
            .Value(j, xarCols_DisplayName) = oDoc.GetItemValue("DisplayName")(0)
            .Value(j, xarCols_ID) = oDoc.GetItemValue("ID")(0)
            .Value(j, xarCols_Parent) = oLS
        End With
        
'        RaiseEvent OnListingRefreshAddition(j, lNumMsgs, Cancel)
'        If Cancel Then
'            oListings.xArray.ReDim 0, i - 1, 0, 5
'            Exit Function
'        End If

        j = j + 1
        On Error Resume Next
        Set oDoc = oCol.GetNextDocument(oDoc)
        On Error GoTo 0
    Wend
    
    Set GetListings = oListings
    
End Function

Public Function GetStores(oStores As mpCI.Stores, oBke As mpCI.Backend) As Variant
'fills oStores with available notes address books
    Dim xSQL    As String
    Dim xServer As String
    Dim xFile   As String
    Dim oDB     As Domino.NotesDatabase
    Dim i       As Integer
    
    On Error GoTo ProcError
    'cycle through address books, adding as stores
    On Error Resume Next
    Set oDB = m_oSession.AddressBooks(i)
    On Error GoTo ProcError
    
    While Not (oDB Is Nothing)
        'attempt to open db if not already open
        If Not oDB.IsOpen Then
            On Error Resume Next
            oDB.Open
            On Error GoTo ProcError
        End If
        
        'add to list of stores only if db can be opened by user
        If oDB.IsOpen Then
            'was able to open db - user has rights to an existing db
            'get db info
            xServer = oDB.Server
        
            'create new store
            Dim storNew As mpCI.Store
            Set storNew = New mpCI.Store

            'set store properties
            With storNew
                .Parent = oBke
'                .ID = i + 1
                .ID = oDB.ReplicaID
                If Len(xServer) Then
                    .Name = oDB.Title & " on " & xServer
                Else
                    .Name = oDB.Title
                End If
                .Server = xServer
                .Database = oDB.FilePath
            End With
        
            'add store to collection
            oStores.Add storNew
        End If
        
        'get next address book
        i = i + 1
        On Error Resume Next
        Set oDB = Nothing
        Set oDB = m_oSession.AddressBooks(i)
        On Error GoTo ProcError
        
        If oDB Is Nothing Then
            Dim j As Integer
            Dim xName As String
            Dim xIniVal As String
            Dim iPos As Integer
            
            j = j + 1
            
            'get additional stores specified in ini
            xIniVal = GetUserIni("AdditionalNotesStores", j)
            
            If Len(xIniVal) Then
                'split ini value into server and filename of db - separated by ";"
                iPos = InStr(xIniVal, ";")
                If iPos Then
                    xServer = Left(xIniVal, iPos - 1)
                    xName = Mid(xIniVal, iPos + 1)
                Else
                    xServer = ""
                    xName = xIniVal
                End If
                
                Set oDB = m_oSession.GetDatabase(xServer, xName, False)
            Else
                'no additional stores specified
                Set oDB = Nothing
            End If
            
        End If
        
    Wend
    Exit Function
ProcError:
    Err.Raise Err.Number, Err.Source & ":Notes.GetSource"
End Function

Public Property Get UserName() As String
    UserName = m_xUserName
End Property

Public Property Get UserID() As Variant
    UserID = m_xUserID
End Property

Public Function AddNotesStore() As Boolean
'adds the database specified by xFullName as a Notes store-
'this allows users to retrieve contacts from non-server,
'non-local address books
    Dim oFm As frmGetNotesDB
    Dim oDB As NotesDatabase
    Dim xMsg As String
    
    Set oFm = New frmGetNotesDB
    
    With oFm
        'prompt user for db to add
        .Show vbModal
        If Not .Cancelled Then
            'ensure that user exists and has rights to db
            On Error Resume Next
            Set oDB = m_oSession.GetDatabase(.Server, .FilePath, False)
            On Error GoTo ProcError
            If oDB Is Nothing Then
                xMsg = "Could not open database. The database does not exist or you may not have access to it."
                MsgBox xMsg, vbExclamation
                Exit Function
            End If
            
            If Not oDB.IsOpen Then
                On Error Resume Next
                oDB.Open
                On Error GoTo ProcError
            End If
            If Not oDB.IsOpen Then
                xMsg = "Could not open database. You may not have access to this database."
                MsgBox xMsg, vbExclamation
                Exit Function
            Else
                'add to ini
                SetUserIni "AdditionalNotesStores", GetNumberOfAdditionalStores() + 1, .Server & ";" & oDB.FilePath
            End If
        End If
        AddNotesStore = True
    End With
    Exit Function
ProcError:
    Err.Raise Err.Number, Err.Source & ": Notes.AddNotesStore", Err.Description
End Function

Function GetNumberOfAdditionalStores() As Integer
    Dim i As Integer
    Dim xVal As String
    
    i = 1
    xVal = GetUserIni("AdditionalNotesStores", i)
    While Len(xVal)
        i = i + 1
        xVal = ""
        xVal = GetUserIni("AdditionalNotesStores", i)
        GetNumberOfAdditionalStores = i - 1
    Wend
End Function

Function RemoveNotesDatabase(xServer As String, xFilePath As String) As Boolean
'removes additional notes database by deleting its ini key, then refreshing
'returns TRUE if database was removed from list
    Dim xID As String
    Dim i As Integer
    Dim iRemoveIndex As Integer
    Dim iTotal As Integer
    Dim xVal As String
    Dim xNextVal As String
    
    xID = xServer & ";" & xFilePath
    
    i = 1
    xVal = GetUserIni("AdditionalNotesStores", i)
    While Len(xVal)
        'test to see if store matches ini value
        If xVal = xID Then
            'this is the one to remove - end loop
            xVal = ""
            iRemoveIndex = i
        Else
            xVal = ""
            i = i + 1
            'test next entry
            xVal = GetUserIni("AdditionalNotesStores", i)
        End If
    Wend
    
'   alert if entry was not found in list
    If iRemoveIndex = 0 Then
        xMsg = "Could not remove the database from the list.  Only databases that were " & _
            "added to the list by a user can be removed."
        MsgBox xMsg, vbExclamation
        Exit Function
    End If
    
'   get total number of entries
    iTotal = GetNumberOfAdditionalStores()
    
'   remove entry i by moving all subsequent entries up
    For i = iRemoveIndex To iTotal - 1
        xNextVal = GetUserIni("AdditionalNotesStores", i + 1)
        SetUserIni "AdditionalNotesStores", i, xNextVal
    Next i
    
'   clear out last entry
    SetUserIni "AdditionalNotesStores", iTotal, ""
    
    RemoveNotesDatabase = True
    Exit Function
ProcError:
    Err.Raise Err.Number, Err.Source & ":Notes.RemoveNotesDatabase", Err.Description
End Function
