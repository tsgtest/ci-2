VERSION 5.00
Object = "{0D62353D-DBA2-11D1-B5DF-0060976089D0}#6.0#0"; "TDBL6.OCX"
Begin VB.Form frmRemoveStore 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Remove Notes Database"
   ClientHeight    =   4164
   ClientLeft      =   48
   ClientTop       =   336
   ClientWidth     =   5088
   Icon            =   "frmRemoveStore.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4164
   ScaleWidth      =   5088
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton btnOK 
      Caption         =   "O&K"
      Height          =   420
      Left            =   2550
      TabIndex        =   3
      Top             =   3615
      Width           =   1100
   End
   Begin VB.CommandButton btnCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   420
      Left            =   3765
      TabIndex        =   2
      Top             =   3615
      Width           =   1100
   End
   Begin TrueDBList60.TDBList lstStores 
      Height          =   2895
      Left            =   180
      OleObjectBlob   =   "frmRemoveStore.frx":000C
      TabIndex        =   1
      Top             =   480
      Width           =   4680
   End
   Begin VB.Label lblStores 
      BackStyle       =   0  'Transparent
      Caption         =   "&Databases:"
      Height          =   300
      Left            =   240
      TabIndex        =   0
      Top             =   255
      Width           =   900
   End
End
Attribute VB_Name = "frmRemoveStore"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private m_bCancelled As Boolean

Private Sub btnCancel_Click()
    m_bCancelled = True
    Me.Hide
End Sub

Private Sub btnOK_Click()
    m_bCancelled = False
    Me.Hide
End Sub

Private Sub Form_Activate()
    If Me.lstStores.Array.Count(1) > 0 Then
        Me.lstStores.Row = 0
    Else
        Me.lstStores.Enabled = False
    End If
End Sub

Private Sub Form_Load()
    On Error GoTo ProcError
    LoadStores
    Exit Sub
ProcError:
    g_oError.ShowError
    Exit Sub
End Sub

Public Property Get Cancelled() As Boolean
    Cancelled = m_bCancelled
End Property

Private Sub LoadStores()
    Dim oArray As XArrayDB
    Dim oDB As Domino.NotesDatabase
    Dim iNumStores As Integer
    Dim xStore As String
    Dim vDetail As Variant
    Dim i As Integer
    
    On Error GoTo ProcError
    'load additional stores into list
    Set oArray = New XArrayDB
    
    iNumStores = GetNumberOfAdditionalStores()
    
    With oArray
        .ReDim 0, iNumStores - 1, 0, 2
        
        For i = 1 To iNumStores
            xStore = g_oIni.GetUserIni("AdditionalNotesStores", CStr(i))
            vDetail = Split(xStore, ";")
            Set oDB = g_oSession.GetDatabase(vDetail(0), vDetail(1), False)
            
            If oDB Is Nothing Then
               'remove from ini
            Else
                'get db name, server, file path
                .Value(i - 1, 0) = oDB.Title
                .Value(i - 1, 1) = vDetail(0)
                .Value(i - 1, 2) = vDetail(1)
            End If
        Next i
    End With
    Me.lstStores.Array = oArray
    Exit Sub
ProcError:
    g_oError.RaiseError "CINotes.frmRemoveStore.LoadStores"
    Exit Sub
End Sub

Private Sub lstStores_DblClick()
    btnOK_Click
End Sub
