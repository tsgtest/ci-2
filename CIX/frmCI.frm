VERSION 5.00
Begin VB.Form frmCI 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   " Contact Integration"
   ClientHeight    =   6924
   ClientLeft      =   4008
   ClientTop       =   2556
   ClientWidth     =   9504
   Icon            =   "frmCI.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6924
   ScaleWidth      =   9504
   StartUpPosition =   2  'CenterScreen
   Begin CIX.CI CI1 
      Height          =   6810
      Left            =   15
      TabIndex        =   0
      Top             =   -30
      Width           =   9465
      _ExtentX        =   16701
      _ExtentY        =   11409
   End
End
Attribute VB_Name = "frmCI"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private m_oContacts As CIO.CContacts
Private m_bCancelled As Boolean

'**********************************************************
'initializer/terminator
'**********************************************************
Dim xObjectID As String
'**********************************************************

Public Property Get Cancelled() As Boolean
    Cancelled = m_bCancelled
End Property

Public Property Get Contacts() As CIO.CContacts
    On Error GoTo ProcError
    Set Contacts = m_oContacts
    Exit Property
ProcError:
    g_oError.RaiseError "CIX.frmCI.Contacts"
    Exit Property
End Property

Private Sub CI1_InsertionCancelled()
    m_bCancelled = True
    Me.Hide
End Sub

Private Sub CI1_InsertionOK(oContacts As CIO.CContacts)
    On Error GoTo ProcError
    m_bCancelled = False
    Set m_oContacts = oContacts
    Me.Hide
    Exit Sub
ProcError:
    g_oError.RaiseError "CIX.frmCI.CI1_InsertionOK"
    Exit Sub
End Sub

Private Sub CI1_PanelChange(bFolderView As Boolean)
    On Error GoTo ProcError
    
    If bFolderView Then
        Me.Caption = " " & g_oSessionType.AppTitle & " - Folder View"
    Else
        Me.Caption = " " & g_oSessionType.AppTitle & " - Contact View"
    End If
    
    Exit Sub
ProcError:
    g_oError.RaiseError "CIX.frmCI.CI1_PanelChange"
    Exit Sub
End Sub

Private Sub Form_Paint()
    'Load icon
    With g_oSessionType
        If (.SessionType = ciSession_Connect) Then
            .SetIcon Me.hwnd, "CONNECTICON", False
        Else
            .SetIcon Me.hwnd, "TSGICON", False
        End If
    End With
End Sub

Private Sub Form_Initialize()
    Math.Randomize
    xObjectID = "frmCI" & Format(Now, "hhmmss") & Math.Rnd()
    g_oGlobals.Objects.Add xObjectID, xObjectID
    DebugPrint "Added - " & xObjectID & ": " & g_oGlobals.Objects.Count
End Sub

Private Sub Form_Load()
    Dim xUseMoFoCI As String
    xUseMoFoCI = g_oIni.GetIni("CIApplication", "UseMofoCI")
    If UCase$(xUseMoFoCI) = "TRUE" Then
        Me.CI1.Height = 7245
        Me.Height = 7640
    End If
    
    Me.CI1.Initialize
    m_bCancelled = True
End Sub

Private Sub Form_Terminate()
    g_oGlobals.Objects.Remove xObjectID
    DebugPrint "Removed - " & xObjectID & ": " & g_oGlobals.Objects.Count
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set m_oContacts = Nothing
    Set frmCI = Nothing
End Sub
