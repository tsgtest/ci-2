VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmProgress 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Loading Contacts"
   ClientHeight    =   1425
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4560
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1425
   ScaleWidth      =   4560
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton btnCancel 
      Caption         =   "Cancel"
      Height          =   390
      Left            =   1650
      TabIndex        =   1
      Top             =   900
      Width           =   1215
   End
   Begin MSComctlLib.ProgressBar pbContacts 
      Height          =   285
      Left            =   270
      TabIndex        =   0
      Top             =   465
      Width           =   3975
      _ExtentX        =   7011
      _ExtentY        =   503
      _Version        =   393216
      Appearance      =   1
      Max             =   1
   End
   Begin VB.Label lblMsg 
      Caption         =   "###"
      Height          =   240
      Left            =   300
      TabIndex        =   2
      Top             =   150
      Width           =   3975
   End
End
Attribute VB_Name = "frmProgress"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public Cancelled As Boolean

Private Sub btnCancel_Click()
    Cancelled = True
End Sub

Private Sub Form_Load()
    Cancelled = False
End Sub
