VERSION 5.00
Begin VB.UserControl StatusBox 
   BackStyle       =   0  'Transparent
   ClientHeight    =   1800
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   4905
   LockControls    =   -1  'True
   ScaleHeight     =   1800
   ScaleWidth      =   4905
   Begin VB.PictureBox picProgress 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   1680
      Left            =   0
      ScaleHeight     =   1680
      ScaleWidth      =   4740
      TabIndex        =   0
      Top             =   0
      Width           =   4740
      Begin VB.CommandButton btnCancel 
         Caption         =   "Cancel"
         Height          =   390
         Left            =   1770
         TabIndex        =   3
         Top             =   1140
         Width           =   1215
      End
      Begin VB.Image imgP 
         Height          =   480
         Left            =   315
         Picture         =   "StatusBox.ctx":0000
         Top             =   495
         Width           =   480
      End
      Begin VB.Label lblMsg 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   1275
         TabIndex        =   2
         Top             =   630
         Width           =   3090
      End
      Begin VB.Line Line7 
         BorderColor     =   &H00404040&
         X1              =   4695
         X2              =   4695
         Y1              =   30
         Y2              =   2000
      End
      Begin VB.Label lblTitle 
         BackStyle       =   0  'Transparent
         Caption         =   "Legal MacPac"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000014&
         Height          =   210
         Left            =   105
         TabIndex        =   1
         Top             =   75
         Width           =   1560
      End
      Begin VB.Line Line3 
         BorderColor     =   &H00FFFFFF&
         X1              =   15
         X2              =   15
         Y1              =   30
         Y2              =   2000
      End
      Begin VB.Line Line4 
         BorderColor     =   &H00808080&
         X1              =   30
         X2              =   4705
         Y1              =   1650
         Y2              =   1650
      End
      Begin VB.Line Line5 
         BorderColor     =   &H00808080&
         X1              =   4680
         X2              =   4680
         Y1              =   30
         Y2              =   2000
      End
      Begin VB.Line Line6 
         BorderColor     =   &H00404040&
         X1              =   30
         X2              =   4710
         Y1              =   1665
         Y2              =   1665
      End
      Begin VB.Line Line8 
         BorderColor     =   &H00FFFFFF&
         X1              =   15
         X2              =   4690
         Y1              =   15
         Y2              =   15
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H80000002&
         BackStyle       =   1  'Opaque
         BorderStyle     =   0  'Transparent
         Height          =   300
         Left            =   45
         Top             =   45
         Width           =   4635
      End
   End
End
Attribute VB_Name = "StatusBox"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Public Event OnShow()
Public Event OnHide()
Public Event Canceled()

Public Canceled As Boolean

Public Property Let Message(ByVal xNew As String)
    UserControl.lblMsg.Caption = xNew
End Property

Public Property Get Message() As String
    Message = UserControl.lblMsg.Caption
End Property

Public Property Let Title(ByVal xNew As String)
    UserControl.lblTitle.Caption = xNew
End Property

Public Property Get Title() As String
    Title = UserControl.lblTitle.Caption
End Property

Private Sub btnCancel_Click()
    Canceled = True
    RaiseEvent Canceled
End Sub

Private Sub UserControl_InitProperties()
    Me.Title = "Contact Integration"
End Sub

Private Sub UserControl_ReadProperties(PropBag As PropertyBag)
    With PropBag
        Me.Title = .ReadProperty("Title", "Contact Integration")
        Me.Message = .ReadProperty("Message", "")
    End With
End Sub

Private Sub UserControl_WriteProperties(PropBag As PropertyBag)
    With PropBag
        .WriteProperty "Title", Me.Title, "Contact Integration"
        .WriteProperty "Message", Me.Message, ""
    End With
End Sub

Private Sub UserControl_Hide()
    RaiseEvent OnHide
End Sub

Private Sub UserControl_Show()
    RaiseEvent OnShow
End Sub

