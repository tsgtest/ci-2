VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "COptions"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

'**********************************************************
'   Options Class
'   created 8/12/98 by Daniel Fisherman
'   Contains properties and methods concerning
'   application options
'**********************************************************

Enum ciEmptyAddress
    ciEmptyAddress_ReturnEmpty = 0
    ciEmptyAddress_Warn = 1
    ciEmptyAddress_PromptForExisting = 2
End Enum

'**********************************************************
Private m_iContactTypes As Integer
Private m_bAutoload As Boolean
Private m_lLoadThreshold As Long
Private m_lAddThreshold As Long
Private m_OnEmptyAddresses As Integer
Private m_bDisplayAlerts As Boolean
Private m_bPromptForMissingPhones As Boolean
Private m_bPromptForMultiplePhones As Boolean
Private m_bDisplayNativeAddressListsOnly As Boolean
Private m_iLogInTimeout As Integer
Private m_iConnectionLoginTimeout As Integer
Private m_bReloadDlgOnRetrieve As Boolean
Private m_bShowFavesOnly As Boolean
Private m_bAllowNativeAppEdits As Boolean
Private m_bAllowMultipleSearchTypes As Boolean
Private m_debugID As Long
Private m_iQueryTimeout As Integer
Private m_iSelContactList As ciSelectionLists

'**********************************************************

'**********************************************************
'   Properties
'**********************************************************

Public Property Let DefaultSelectionList(iNew As ciSelectionLists)
    m_iSelContactList = iNew
End Property

Public Property Get DefaultSelectionList() As ciSelectionLists
    DefaultSelectionList = m_iSelContactList
End Property

Public Property Get AllowNativeAppEdits() As Boolean
    AllowNativeAppEdits = m_bAllowNativeAppEdits
End Property

Public Property Let AllowNativeAppEdits(bNew As Boolean)
    m_bAllowNativeAppEdits = bNew
End Property

Public Property Get OnEmptyAddresses() As Integer
Attribute OnEmptyAddresses.VB_Description = "Sets/returns the value that determines the behavior of Contacts Integration when a requested address does not exist."
    OnEmptyAddresses = m_OnEmptyAddresses
End Property

Public Property Let OnEmptyAddresses(iNew As Integer)
    m_OnEmptyAddresses = iNew
End Property

Public Property Let QueryTimeout(iNew As Integer)
Attribute QueryTimeout.VB_Description = "Sets/returns the maximum time allowed for connection process. An error is generated if the timeout is reached."
    m_iQueryTimeout = iNew
End Property

Public Property Get QueryTimeout() As Integer
    If m_iQueryTimeout < 10 Then
        QueryTimeout = 10
    Else
        QueryTimeout = m_iQueryTimeout
    End If
End Property

Public Property Let ConnectionLoginTimeout(iNew As Integer)
    m_iConnectionLoginTimeout = iNew
End Property

Public Property Get ConnectionLoginTimeout() As Integer
    ConnectionLoginTimeout = m_iConnectionLoginTimeout
End Property

Public Property Let LoginTimeout(iNew As Integer)
Attribute LoginTimeout.VB_Description = "Sets/returns the amount of time allowed to load the ODBC-based front-end"
    m_iLogInTimeout = iNew
End Property

Public Property Get LoginTimeout() As Integer
    LoginTimeout = m_iLogInTimeout
End Property

Public Property Get DisplayAlerts() As Boolean
    DisplayAlerts = m_bDisplayAlerts
End Property

Public Property Let DisplayAlerts(bNew As Boolean)
    m_bDisplayAlerts = bNew
End Property

Public Property Get AutoLoadDefaultFolder() As Boolean
Attribute AutoLoadDefaultFolder.VB_Description = "Sets/returns a value that determines whether the listings in the default folder are loaded on start of Contacts Integration."
    AutoLoadDefaultFolder = m_bAutoload
End Property

Public Property Let AutoLoadDefaultFolder(bNew As Boolean)
    m_bAutoload = bNew
End Property

Public Property Get LoadThreshold() As Long
Attribute LoadThreshold.VB_Description = "Sets/returns the number of listings that will generate a user alert upon the loading of a folder."
    LoadThreshold = m_lLoadThreshold
End Property

Public Property Let LoadThreshold(lNew As Long)
    m_lLoadThreshold = lNew
End Property

Public Property Get AddThreshold() As Long
Attribute AddThreshold.VB_Description = "sets/returns an integer such that the user is alerted  if the pending number of additions is greater than this number."
    AddThreshold = m_lAddThreshold
End Property

Public Property Let AddThreshold(lNew As Long)
    m_lAddThreshold = lNew
End Property

Public Property Get PromptForMissingPhones() As Boolean
Attribute PromptForMissingPhones.VB_Description = "Sets/returns a value that determines whether the user is prompted for missing phone/fax information when that information is requested by Contacts Integration"
    PromptForMissingPhones = m_bPromptForMissingPhones
End Property

Public Property Let PromptForMissingPhones(bNew As Boolean)
    m_bPromptForMissingPhones = bNew
End Property

Friend Property Get PromptForMultiplePhones() As Boolean
    PromptForMultiplePhones = m_bPromptForMultiplePhones
End Property

Friend Property Let PromptForMultiplePhones(bNew As Boolean)
    m_bPromptForMultiplePhones = bNew
End Property

Public Property Get AllowLists() As Boolean
Attribute AllowLists.VB_Description = "Allow user to select from different contact lists"
'allow lists only if there is one backend,
'and it has no directories and one store
    With Backends
        AllowLists = (.Count > 1) Or _
                     (.Item(1).MultipleListingSources) Or _
                     (.Item(1).MultipleStores)
    End With
End Property

Public Property Let SelectionLists(iNew As Integer)
Attribute SelectionLists.VB_Description = "Sets/returns an integer representing the types of contacts that can be returned."
'see enum ciGlobal.ciSelectionLists for valid values-
'Mix & match - bit values
    m_iContactTypes = iNew
End Property

Public Property Get SelectionLists() As Integer
'see enum ciGlobal.ciSelectionLists for valid values-
'Mix & match - bit values
    SelectionLists = m_iContactTypes
End Property

Public Property Let DisplayExchangeBusinessAddr(bNew As Boolean)
Attribute DisplayExchangeBusinessAddr.VB_Description = "sets/returns a value determining whether the business address is displayed as an address type when using Contacts Integration with the Microsoft Exchange Server."
    m_bDisplayExchangeBusinessAddr = bNew
End Property

Public Property Get DisplayExchangeBusinessAddr() As Boolean
    DisplayExchangeBusinessAddr = m_bDisplayExchangeBusinessAddr
End Property

Public Property Let AllowMultipleSearchTypes(bNew As Boolean)
    m_bAllowMultipleSearchTypes = bNew
End Property

Public Property Get AllowMultipleSearchTypes() As Boolean
    AllowMultipleSearchTypes = m_bAllowMultipleSearchTypes
End Property

Public Property Let ReloadDlgOnRetrieve(bNew As Boolean)
    m_bReloadDlgOnRetrieve = bNew
End Property

Public Property Get ReloadDlgOnRetrieve() As Boolean
    ReloadDlgOnRetrieve = m_bReloadDlgOnRetrieve
End Property

Private Sub Class_Initialize()
    Dim xNativeApps As String
    Dim xKey As String
    
    If g_bTrace Then
        m_debugID = LogObjectCreate("Options")
    End If
    #If compHandleErrors Then
        On Error GoTo ProcError
    #End If
    
    Me.NamesOnlyForTo = False
    
    With Me
        .DisplayAlerts = True
        If Application.PreferenceSource = ciPreferenceSource_INI Then
            If UseUserIni() Then
                .AutoLoadDefaultFolder = GetUserIni("Application", "AutoLoadDefaultFolder")
                .LoadThreshold = GetUserIni("Application", "LoadThreshold")
                On Error Resume Next
                .ShowFavoritesOnly = GetUserIni("Application", "ShowODBCFavoritesOnly")
                On Error GoTo 0
            Else
                .AutoLoadDefaultFolder = GetIni("Application", "AutoLoadDefaultFolder")
                .LoadThreshold = GetIni("Application", "LoadThreshold")
                On Error Resume Next
                .ShowFavoritesOnly = GetIni("Application", "ShowODBCFavoritesOnly")
                On Error GoTo 0
            End If
        Else
            .AutoLoadDefaultFolder = GetUserValue("AutoLoadDefaultFolder")
            .LoadThreshold = GetUserValue("LoadThreshold")
            On Error Resume Next
            .ShowFavoritesOnly = GetUserValue("ShowODBCFavoritesOnly")
            On Error GoTo 0
        End If
        
        .AddThreshold = GetIni("Application", "AddThreshold")
        .OnEmptyAddresses = GetIni("Application", "OnEmptyAddresses")
        .PromptForMissingPhones = GetIni("Application", "PromptForMissingPhones")
        .DisplayNativeAddressListsOnly = GetIni("Application", "DisplayNativeAddressListsOnly")
        .DisplayExchangeBusinessAddr = GetIni("Application", "DisplayExchangeBusinessAddress")
        .LoginTimeout = GetIni("Application", "LoginTimeout")
        .ConnectionLoginTimeout = GetIni("Application", "ConnectionLoginTimeout")
        .AllowMultipleSearchTypes = GetIni("Application", "AllowMultipleSearchTypes")
        
        On Error Resume Next
        If GetIni("Application", "ReloadDlgOnRetrieve") = "" Then
            .ReloadDlgOnRetrieve = True
        Else
            .ReloadDlgOnRetrieve = GetIni("Application", "ReloadDlgOnRetrieve")
        End If
        .QueryTimeout = GetIni("Application", "QueryTimeout")
        .UseAltODBCRetrieval = GetIni("Application", "UseAltODBCRetrieval")
        xNativeApps = GetIni("Application", "AllowNativeAppEdits")
        If xNativeApps = "" Then
            xNativeApps = "True"
        End If
        .AllowNativeAppEdits = xNativeApps
        On Error GoTo 0
        
'       this property is not exposed in any way-
'       users will always be prompted for multiple
'       phones - internally, the code sets it false
'       when displaying the Detail_Listing form.
        .PromptForMultiplePhones = True
        
'       default to include all contact info -
'       client may override
        .IncludeFax = True
        .IncludePhone = True
        .IncludeAddress = True
        .IncludeEAddress = False
        .IncludeCustomFields = False
    End With
    Exit Sub
    
ProcError:
    Err.Raise ciError_ErrorReadingIniOptions, "Options::Initialize"
    Exit Sub
End Sub

Private Sub Class_Terminate()
    If g_bTrace Then
        RemoveObjectFromLog m_debugID
    End If
End Sub

Public Sub SetDefaultListingSource(ByVal xFullPath As String)
    On Error GoTo ProcError
    With Application
        .DefaultListingSource = xFullPath
        If Application.PreferenceSource = ciPreferenceSource_INI Then
            If UseUserIni() Then
                SetUserIni "Application", _
                       "DefaultListingSource", _
                       xFullPath
            Else
                SetIni "Application", _
                       "DefaultListingSource", _
                       xFullPath
            End If
        Else
            SetUserValue "DefaultListingSource", xFullPath
        End If
    End With
    Exit Sub
ProcError:
    Err.Raise Err.Number, "Options.SetDefaultListingSource", Err.Description
End Sub
