VERSION 5.00
Begin VB.Form frmOptions 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Options"
   ClientHeight    =   3915
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6240
   ControlBox      =   0   'False
   BeginProperty Font 
      Name            =   "MS Sans Serif"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3915
   ScaleWidth      =   6240
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.TextBox txtLoadThreshold 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      Height          =   315
      Left            =   1815
      TabIndex        =   9
      Top             =   2955
      Width           =   690
   End
   Begin VB.Frame fraMissingData 
      Caption         =   "Behavior"
      Height          =   255
      Left            =   30
      TabIndex        =   13
      Top             =   4635
      Visible         =   0   'False
      Width           =   5085
      Begin VB.OptionButton optEmptyContact 
         Caption         =   "prompt for available addresses"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   345
         Index           =   2
         Left            =   405
         TabIndex        =   23
         Top             =   2790
         Visible         =   0   'False
         Width           =   3240
      End
      Begin VB.OptionButton optEmptyContact 
         Caption         =   "Do &not insert the contact, and alert user"
         Height          =   345
         Index           =   1
         Left            =   315
         TabIndex        =   16
         Top             =   840
         Width           =   3240
      End
      Begin VB.OptionButton optEmptyContact 
         Caption         =   "&Insert the contact without an address"
         Height          =   345
         Index           =   0
         Left            =   315
         TabIndex        =   15
         Top             =   555
         Width           =   3240
      End
      Begin VB.CheckBox chkPromptForMissingPhones 
         Caption         =   "&Prompt for missing phone and fax numbers"
         Height          =   345
         Left            =   150
         TabIndex        =   17
         Top             =   795
         Visible         =   0   'False
         Width           =   3540
      End
      Begin VB.Label lblIfAddressMissing 
         Caption         =   "If a contact does not have an address of the requested type:"
         Height          =   420
         Left            =   135
         TabIndex        =   14
         Top             =   315
         Width           =   4545
      End
   End
   Begin VB.Frame fraDefFolder 
      Caption         =   "Default Folder"
      Height          =   2730
      Left            =   165
      TabIndex        =   0
      Top             =   90
      Width           =   5880
      Begin VB.CommandButton btnSet 
         Caption         =   "Set Current Folder As &Default"
         Height          =   390
         Left            =   1620
         TabIndex        =   7
         Top             =   2160
         Width           =   2295
      End
      Begin VB.TextBox txtCurrentFolder 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         Height          =   345
         Left            =   195
         Locked          =   -1  'True
         TabIndex        =   4
         TabStop         =   0   'False
         Top             =   1200
         Width           =   5445
      End
      Begin VB.CommandButton btnRemove 
         Caption         =   "&Remove Default"
         Height          =   390
         Left            =   195
         TabIndex        =   6
         Top             =   2160
         Width           =   1335
      End
      Begin VB.TextBox txtDefFolder 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         Height          =   345
         Left            =   195
         Locked          =   -1  'True
         TabIndex        =   2
         TabStop         =   0   'False
         Top             =   510
         Width           =   5445
      End
      Begin VB.CheckBox chkAutoload 
         Caption         =   "&Load contacts in default folder at startup"
         Height          =   300
         Left            =   210
         TabIndex        =   5
         Top             =   1695
         Width           =   3700
      End
      Begin VB.Label lblCurrentFolder 
         Caption         =   "Current Folder:"
         Height          =   285
         Left            =   225
         TabIndex        =   3
         Top             =   990
         Width           =   2505
      End
      Begin VB.Label lblDefFolder 
         Caption         =   "Default Folder:"
         Height          =   285
         Left            =   225
         TabIndex        =   1
         Top             =   315
         Width           =   2505
      End
   End
   Begin VB.Frame fraDisplay 
      Caption         =   "Display"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   870
      Left            =   5895
      TabIndex        =   18
      Top             =   6075
      Visible         =   0   'False
      Width           =   2385
      Begin VB.ComboBox cbxSortBy 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         ItemData        =   "frmOptions.frx":0000
         Left            =   135
         List            =   "frmOptions.frx":0025
         Style           =   2  'Dropdown List
         TabIndex        =   20
         Top             =   510
         Width           =   3000
      End
      Begin VB.ComboBox cbxDisplay 
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         ItemData        =   "frmOptions.frx":0044
         Left            =   135
         List            =   "frmOptions.frx":005A
         Style           =   2  'Dropdown List
         TabIndex        =   19
         Top             =   1215
         Width           =   3000
      End
      Begin VB.Label Label2 
         Caption         =   "&Sort By:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   300
         Left            =   180
         TabIndex        =   22
         Top             =   285
         Width           =   585
      End
      Begin VB.Label Label1 
         Caption         =   "&Display Format:"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   150
         TabIndex        =   21
         Top             =   990
         Width           =   1200
      End
   End
   Begin VB.CommandButton btnCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   385
      Left            =   4920
      TabIndex        =   12
      Top             =   3420
      Width           =   1100
   End
   Begin VB.CommandButton btnOK 
      Caption         =   "O&K"
      Default         =   -1  'True
      Height          =   385
      Left            =   3750
      TabIndex        =   11
      Top             =   3420
      Width           =   1100
   End
   Begin VB.Label lblLoadThreshold2 
      Caption         =   "contacts will be retrieved"
      Height          =   240
      Left            =   2640
      TabIndex        =   10
      Top             =   3000
      Width           =   1920
   End
   Begin VB.Label lblLoadThreshold 
      Caption         =   "&Alert when more than"
      Height          =   240
      Left            =   195
      TabIndex        =   8
      Top             =   3000
      Width           =   1575
   End
End
Attribute VB_Name = "frmOptions"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private m_bCancelled As Boolean
Private xMsg As String

Public Property Get Cancelled() As Boolean
    Cancelled = m_bCancelled
End Property

Public Property Let Cancelled(bNew As Boolean)
    m_bCancelled = bNew
End Property

Private Sub btnCancel_Click()
     Me.Hide
     Me.Cancelled = True
End Sub

Private Sub btnOK_Click()
    lRet = ValidateOptions()
    If lRet Then
        Me.Cancelled = False
        On Error Resume Next
        Me.Hide
    End If
End Sub

Private Sub btnSet_Click()
    Options.SetDefaultListingSource Me.txtCurrentFolder
    Me.txtDefFolder = Me.txtCurrentFolder
    Me.chkAutoload.Enabled = True
End Sub

Private Sub btnRemove_Click()
    With Application
        Options.SetDefaultListingSource ""
        Me.txtDefFolder = ""
        Me.chkAutoload = False
        If Application.PreferenceSource = ciPreferenceSource_INI Then
            SetIni "Application", "AutoLoadDefaultFolder", False
        Else
            SetUserValue "AutoLoadDefaultFolder", False
        End If
        With Me.chkAutoload
            .Value = False
            .Enabled = False
        End With
    End With
End Sub

Private Sub Form_Activate()
    Me.cbxSearchTypes.SelectedItem = Application.Filter.SearchType
End Sub

Private Sub Form_Load()
    Dim i As Integer
    Dim xarSearchTypes As xArray
    
    Me.cbxSearchTypes.Visible = Options.AllowMultipleSearchTypes
    Me.lblSearchTypes.Visible = Options.AllowMultipleSearchTypes
    If Options.AllowMultipleSearchTypes Then
        Set xarSearchTypes = New xArray
        With xarSearchTypes
            .ReDim 0, 2, 0, 2
            .Value(0, 0) = "Standard Wildcard Searching"
            .Value(0, 1) = "M = M  and  M* = M*"
            .Value(0, 2) = ciSearchType_StandardWildcards
            .Value(1, 0) = "Find contacts that begin with the text..."
            .Value(1, 1) = "M = M*"
            .Value(1, 2) = ciSearchType_BeginsWith
            .Value(2, 0) = "Find contacts that contain the text..."
            .Value(2, 1) = "M = *M*"
            .Value(2, 2) = ciSearchType_Contains
        End With
    
        With Me.cbxSearchTypes
            .Array = xarSearchTypes
            .ReBind
        End With
        
        ResizeTDBCombo Me.cbxSearchTypes, 3
    Else
'       reposition controls to acct for invisible search type combo
        Me.fraDefFolder.Top = Me.fraDefFolder.Top + 100
        Me.btnOK.Top = Me.btnOK.Top - 300
        Me.btnCancel.Top = Me.btnCancel.Top - 300
        Me.lblLoadThreshold.Top = Me.lblLoadThreshold.Top - 300
        Me.lblLoadThreshold2.Top = Me.lblLoadThreshold2.Top - 300
        Me.txtLoadThreshold.Top = Me.txtLoadThreshold.Top - 300
        Me.chkShowFavoritesOnly.Top = Me.chkShowFavoritesOnly.Top - 300
        Me.Height = Me.Height - 200
    End If

'   get default and current listing source paths
    Me.txtDefFolder = Application.DefaultListingSource
    Me.txtCurrentFolder = frmContacts.treeListingSources.SelectedItem.FullPath
    
    With Options
        Me.chkAutoload = Abs(.AutoLoadDefaultFolder)
        Me.txtLoadThreshold = Abs(.LoadThreshold)
        Me.optEmptyContact(.OnEmptyAddresses).Value = True
        Me.chkPromptForMissingPhones = Abs(.PromptForMissingPhones)
        Me.chkShowFavoritesOnly = Abs(.ShowFavoritesOnly)
    End With
    
'   disable autoload if no default folder
    Me.chkAutoload.Enabled = (Application.DefaultListingSource <> Empty)
End Sub

Private Sub txtLoadThreshold_GotFocus()
    With Me.txtLoadThreshold
        .SelStart = 0
        .SelLength = Len(.Text)
    End With
End Sub

Private Sub txtLoadThreshold_LostFocus()
'validate load threshold input
    Dim xNum As String
    xNum = Me.txtLoadThreshold
    If xNum <> "" Then
        If Not IsNumeric(xNum) Then
            xMsg = "Value must be numeric."
            MsgBox xMsg, vbExclamation, Application.Caption
            With Me.txtLoadThreshold
                .SelStart = 0
                .SelLength = Len(.Text)
                .SetFocus
            End With
        Else
            Me.txtLoadThreshold = Int(xNum)
        End If
    End If
End Sub

Private Function ValidateOptions() As Boolean
    ValidateOptions = True
    If IsNumeric(Me.txtLoadThreshold) Then
        Me.txtLoadThreshold = Int(Me.txtLoadThreshold)
    Else
        With Me.txtLoadThreshold
            .SetFocus
            .SelStart = 0
            .SelLength = Len(.Text)
        End With
        MsgBox "Invalid value.  Load threshold must be numeric.", _
               vbExclamation, Application.Caption
        ValidateOptions = False
    End If
End Function
