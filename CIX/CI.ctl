VERSION 5.00
Object = "{0D62353D-DBA2-11D1-B5DF-0060976089D0}#6.0#0"; "TDBL6.OCX"
Object = "{00028CDA-0000-0000-0000-000000000046}#6.0#0"; "tdbg6.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.UserControl CI 
   Alignable       =   -1  'True
   BackStyle       =   0  'Transparent
   ClientHeight    =   7332
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   9492
   DefaultCancel   =   -1  'True
   KeyPreview      =   -1  'True
   MaskColor       =   &H00000000&
   ScaleHeight     =   7332
   ScaleWidth      =   9492
   ToolboxBitmap   =   "CI.ctx":0000
   Begin VB.Frame fraFolders 
      BorderStyle     =   0  'None
      Caption         =   "FOLDERS"
      Height          =   6228
      Left            =   12
      TabIndex        =   51
      Top             =   552
      Width           =   9480
      Begin VB.Frame fraQuickFilter 
         BorderStyle     =   0  'None
         Height          =   810
         Left            =   72
         TabIndex        =   62
         Top             =   5388
         Width           =   9195
         Begin VB.PictureBox picBFilter 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            ForeColor       =   &H80000008&
            Height          =   255
            Index           =   26
            Left            =   8085
            Picture         =   "CI.ctx":0312
            ScaleHeight     =   252
            ScaleWidth      =   252
            TabIndex        =   120
            Tag             =   "z"
            Top             =   45
            Width           =   255
         End
         Begin VB.PictureBox picBFilter 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            ForeColor       =   &H80000008&
            Height          =   255
            Index           =   25
            Left            =   7785
            Picture         =   "CI.ctx":0654
            ScaleHeight     =   252
            ScaleWidth      =   252
            TabIndex        =   119
            Tag             =   "y"
            Top             =   45
            Width           =   255
         End
         Begin VB.PictureBox picBFilter 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            ForeColor       =   &H80000008&
            Height          =   255
            Index           =   24
            Left            =   7485
            Picture         =   "CI.ctx":0996
            ScaleHeight     =   252
            ScaleWidth      =   252
            TabIndex        =   118
            Tag             =   "x"
            Top             =   45
            Width           =   255
         End
         Begin VB.PictureBox picBFilter 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            ForeColor       =   &H80000008&
            Height          =   255
            Index           =   23
            Left            =   7185
            Picture         =   "CI.ctx":0CD8
            ScaleHeight     =   252
            ScaleWidth      =   252
            TabIndex        =   117
            Tag             =   "w"
            Top             =   45
            Width           =   255
         End
         Begin VB.PictureBox picBFilter 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            ForeColor       =   &H80000008&
            Height          =   255
            Index           =   22
            Left            =   6885
            Picture         =   "CI.ctx":105A
            ScaleHeight     =   252
            ScaleWidth      =   252
            TabIndex        =   116
            Tag             =   "v"
            Top             =   45
            Width           =   255
         End
         Begin VB.PictureBox picBFilter 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            ForeColor       =   &H80000008&
            Height          =   255
            Index           =   21
            Left            =   6585
            Picture         =   "CI.ctx":139C
            ScaleHeight     =   252
            ScaleWidth      =   252
            TabIndex        =   115
            Tag             =   "u"
            Top             =   45
            Width           =   255
         End
         Begin VB.PictureBox picBFilter 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            ForeColor       =   &H80000008&
            Height          =   255
            Index           =   20
            Left            =   6285
            Picture         =   "CI.ctx":16DE
            ScaleHeight     =   252
            ScaleWidth      =   252
            TabIndex        =   114
            Tag             =   "t"
            Top             =   45
            Width           =   255
         End
         Begin VB.PictureBox picBFilter 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            ForeColor       =   &H80000008&
            Height          =   255
            Index           =   19
            Left            =   5985
            Picture         =   "CI.ctx":1A20
            ScaleHeight     =   252
            ScaleWidth      =   252
            TabIndex        =   113
            Tag             =   "s"
            Top             =   45
            Width           =   255
         End
         Begin VB.PictureBox picBFilter 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            ForeColor       =   &H80000008&
            Height          =   255
            Index           =   18
            Left            =   5685
            Picture         =   "CI.ctx":1D62
            ScaleHeight     =   252
            ScaleWidth      =   252
            TabIndex        =   112
            Tag             =   "r"
            Top             =   45
            Width           =   255
         End
         Begin VB.PictureBox picBFilter 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            ForeColor       =   &H80000008&
            Height          =   255
            Index           =   17
            Left            =   5385
            Picture         =   "CI.ctx":20A4
            ScaleHeight     =   252
            ScaleWidth      =   252
            TabIndex        =   111
            Tag             =   "q"
            Top             =   45
            Width           =   255
         End
         Begin VB.PictureBox picBFilter 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            ForeColor       =   &H80000008&
            Height          =   255
            Index           =   16
            Left            =   5085
            Picture         =   "CI.ctx":23E6
            ScaleHeight     =   252
            ScaleWidth      =   252
            TabIndex        =   110
            Tag             =   "p"
            Top             =   45
            Width           =   255
         End
         Begin VB.PictureBox picBFilter 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            ForeColor       =   &H80000008&
            Height          =   255
            Index           =   15
            Left            =   4785
            Picture         =   "CI.ctx":2728
            ScaleHeight     =   252
            ScaleWidth      =   252
            TabIndex        =   109
            Tag             =   "o"
            Top             =   45
            Width           =   255
         End
         Begin VB.PictureBox picBFilter 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            ForeColor       =   &H80000008&
            Height          =   255
            Index           =   14
            Left            =   4485
            Picture         =   "CI.ctx":2A6A
            ScaleHeight     =   252
            ScaleWidth      =   252
            TabIndex        =   108
            Tag             =   "n"
            Top             =   45
            Width           =   255
         End
         Begin VB.PictureBox picBFilter 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            ForeColor       =   &H80000008&
            Height          =   255
            Index           =   13
            Left            =   4185
            Picture         =   "CI.ctx":2DAC
            ScaleHeight     =   252
            ScaleWidth      =   252
            TabIndex        =   107
            Tag             =   "m"
            Top             =   45
            Width           =   255
         End
         Begin VB.PictureBox picBFilter 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            ForeColor       =   &H80000008&
            Height          =   255
            Index           =   12
            Left            =   3885
            Picture         =   "CI.ctx":30EE
            ScaleHeight     =   252
            ScaleWidth      =   252
            TabIndex        =   106
            Tag             =   "l"
            Top             =   45
            Width           =   255
         End
         Begin VB.PictureBox picBFilter 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            ForeColor       =   &H80000008&
            Height          =   255
            Index           =   11
            Left            =   3585
            Picture         =   "CI.ctx":3430
            ScaleHeight     =   252
            ScaleWidth      =   252
            TabIndex        =   105
            Tag             =   "k"
            Top             =   45
            Width           =   255
         End
         Begin VB.PictureBox picBFilter 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            ForeColor       =   &H80000008&
            Height          =   255
            Index           =   10
            Left            =   3285
            Picture         =   "CI.ctx":3772
            ScaleHeight     =   252
            ScaleWidth      =   252
            TabIndex        =   104
            Tag             =   "j"
            Top             =   45
            Width           =   255
         End
         Begin VB.PictureBox picBFilter 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            ForeColor       =   &H80000008&
            Height          =   255
            Index           =   9
            Left            =   2985
            Picture         =   "CI.ctx":3AB4
            ScaleHeight     =   252
            ScaleWidth      =   252
            TabIndex        =   103
            Tag             =   "i"
            Top             =   45
            Width           =   255
         End
         Begin VB.PictureBox picBFilter 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            ForeColor       =   &H80000008&
            Height          =   255
            Index           =   8
            Left            =   2685
            Picture         =   "CI.ctx":3DF6
            ScaleHeight     =   252
            ScaleWidth      =   252
            TabIndex        =   102
            Tag             =   "h"
            Top             =   45
            Width           =   255
         End
         Begin VB.PictureBox picBFilter 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            ForeColor       =   &H80000008&
            Height          =   255
            Index           =   7
            Left            =   2385
            Picture         =   "CI.ctx":4138
            ScaleHeight     =   252
            ScaleWidth      =   252
            TabIndex        =   101
            Tag             =   "g"
            Top             =   45
            Width           =   255
         End
         Begin VB.PictureBox picBFilter 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            ForeColor       =   &H80000008&
            Height          =   255
            Index           =   6
            Left            =   2085
            Picture         =   "CI.ctx":447A
            ScaleHeight     =   252
            ScaleWidth      =   252
            TabIndex        =   100
            Tag             =   "f"
            Top             =   45
            Width           =   255
         End
         Begin VB.PictureBox picBFilter 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            ForeColor       =   &H80000008&
            Height          =   255
            Index           =   2
            Left            =   885
            Picture         =   "CI.ctx":47BC
            ScaleHeight     =   252
            ScaleWidth      =   252
            TabIndex        =   99
            Tag             =   "b"
            Top             =   45
            Width           =   255
         End
         Begin VB.PictureBox picBFilter 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            ForeColor       =   &H80000008&
            Height          =   255
            Index           =   3
            Left            =   1185
            Picture         =   "CI.ctx":4AFE
            ScaleHeight     =   252
            ScaleWidth      =   252
            TabIndex        =   98
            Tag             =   "c"
            Top             =   45
            Width           =   255
         End
         Begin VB.PictureBox picBFilter 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            ForeColor       =   &H80000008&
            Height          =   255
            Index           =   4
            Left            =   1485
            Picture         =   "CI.ctx":4E40
            ScaleHeight     =   252
            ScaleWidth      =   252
            TabIndex        =   97
            Tag             =   "d"
            Top             =   45
            Width           =   255
         End
         Begin VB.PictureBox picBFilter 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            ForeColor       =   &H80000008&
            Height          =   255
            Index           =   5
            Left            =   1785
            Picture         =   "CI.ctx":5182
            ScaleHeight     =   252
            ScaleWidth      =   252
            TabIndex        =   96
            Tag             =   "e"
            Top             =   45
            Width           =   255
         End
         Begin VB.PictureBox picBFilter 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            ForeColor       =   &H80000008&
            Height          =   255
            Index           =   1
            Left            =   585
            Picture         =   "CI.ctx":54C4
            ScaleHeight     =   252
            ScaleWidth      =   252
            TabIndex        =   95
            Tag             =   "a"
            Top             =   45
            Width           =   255
         End
         Begin VB.PictureBox picBFilter 
            Appearance      =   0  'Flat
            BorderStyle     =   0  'None
            ForeColor       =   &H80000008&
            Height          =   235
            Index           =   0
            Left            =   135
            Picture         =   "CI.ctx":5806
            ScaleHeight     =   240
            ScaleWidth      =   456
            TabIndex        =   94
            Tag             =   "^123^"
            Top             =   45
            Width           =   450
         End
         Begin VB.OptionButton optQuickFilter 
            Caption         =   "Option1"
            Height          =   255
            Index           =   3
            Left            =   5508
            TabIndex        =   93
            Top             =   420
            Visible         =   0   'False
            Width           =   1700
         End
         Begin VB.OptionButton optQuickFilter 
            Caption         =   "Option1"
            Height          =   255
            Index           =   2
            Left            =   3768
            TabIndex        =   92
            Top             =   420
            Visible         =   0   'False
            Width           =   1700
         End
         Begin VB.OptionButton optQuickFilter 
            Caption         =   "Option1"
            Height          =   255
            Index           =   1
            Left            =   2028
            TabIndex        =   91
            Top             =   420
            Visible         =   0   'False
            Width           =   1700
         End
         Begin VB.OptionButton optQuickFilter 
            Caption         =   "Option1"
            Height          =   255
            Index           =   0
            Left            =   288
            TabIndex        =   90
            Top             =   420
            Value           =   -1  'True
            Visible         =   0   'False
            Width           =   1700
         End
         Begin VB.CommandButton btnBFilter 
            Caption         =   "b"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.4
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Index           =   27
            Left            =   1440
            TabIndex        =   89
            Top             =   840
            Width           =   300
         End
         Begin VB.CommandButton btnBFilter 
            Caption         =   "y"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.4
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Index           =   28
            Left            =   8340
            TabIndex        =   88
            Top             =   840
            Width           =   300
         End
         Begin VB.CommandButton btnBFilter 
            Caption         =   "z"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.4
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Index           =   29
            Left            =   8640
            TabIndex        =   87
            Top             =   840
            Width           =   300
         End
         Begin VB.CommandButton btnBFilter 
            Caption         =   "c"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.4
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Index           =   30
            Left            =   1740
            TabIndex        =   86
            Top             =   840
            Width           =   300
         End
         Begin VB.CommandButton btnBFilter 
            Caption         =   "d"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.4
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Index           =   31
            Left            =   2040
            TabIndex        =   85
            Top             =   840
            Width           =   300
         End
         Begin VB.CommandButton btnBFilter 
            Caption         =   "e"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.4
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Index           =   32
            Left            =   2340
            TabIndex        =   84
            Top             =   840
            Width           =   300
         End
         Begin VB.CommandButton btnBFilter 
            Caption         =   "f"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.4
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Index           =   33
            Left            =   2640
            TabIndex        =   83
            Top             =   840
            Width           =   300
         End
         Begin VB.CommandButton btnBFilter 
            Caption         =   "g"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.4
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Index           =   34
            Left            =   2940
            TabIndex        =   82
            Top             =   840
            Width           =   300
         End
         Begin VB.CommandButton btnBFilter 
            Caption         =   "h"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.4
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Index           =   35
            Left            =   3240
            TabIndex        =   81
            Top             =   840
            Width           =   300
         End
         Begin VB.CommandButton btnBFilter 
            Caption         =   "i"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.4
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Index           =   36
            Left            =   3540
            TabIndex        =   80
            Top             =   840
            Width           =   300
         End
         Begin VB.CommandButton btnBFilter 
            Caption         =   "j"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.4
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Index           =   37
            Left            =   3840
            TabIndex        =   79
            Top             =   840
            Width           =   300
         End
         Begin VB.CommandButton btnBFilter 
            Caption         =   "k"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.4
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Index           =   38
            Left            =   4140
            TabIndex        =   78
            Top             =   840
            Width           =   300
         End
         Begin VB.CommandButton btnBFilter 
            Caption         =   "l"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.4
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Index           =   39
            Left            =   4440
            TabIndex        =   77
            Top             =   840
            Width           =   300
         End
         Begin VB.CommandButton btnBFilter 
            Caption         =   "123"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.4
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Index           =   40
            Left            =   540
            TabIndex        =   76
            Top             =   840
            Width           =   570
         End
         Begin VB.CommandButton btnBFilter 
            Appearance      =   0  'Flat
            Caption         =   "a"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.4
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Index           =   41
            Left            =   1140
            Picture         =   "CI.ctx":5D48
            TabIndex        =   75
            Top             =   840
            Width           =   300
         End
         Begin VB.CommandButton btnBFilter 
            Caption         =   "o"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.4
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Index           =   42
            Left            =   5340
            TabIndex        =   74
            Top             =   840
            Width           =   300
         End
         Begin VB.CommandButton btnBFilter 
            Caption         =   "p"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.4
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Index           =   43
            Left            =   5640
            TabIndex        =   73
            Top             =   840
            Width           =   300
         End
         Begin VB.CommandButton btnBFilter 
            Caption         =   "q"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.4
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Index           =   44
            Left            =   5940
            TabIndex        =   72
            Top             =   840
            Width           =   300
         End
         Begin VB.CommandButton btnBFilter 
            Caption         =   "r"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.4
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Index           =   45
            Left            =   6240
            TabIndex        =   71
            Top             =   840
            Width           =   300
         End
         Begin VB.CommandButton btnBFilter 
            Caption         =   "s"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.4
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Index           =   46
            Left            =   6540
            TabIndex        =   70
            Top             =   840
            Width           =   300
         End
         Begin VB.CommandButton btnBFilter 
            Caption         =   "t"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.4
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Index           =   47
            Left            =   6840
            TabIndex        =   69
            Top             =   840
            Width           =   300
         End
         Begin VB.CommandButton btnBFilter 
            Caption         =   "u"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.4
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Index           =   48
            Left            =   7140
            TabIndex        =   68
            Top             =   840
            Width           =   300
         End
         Begin VB.CommandButton btnBFilter 
            Caption         =   "v"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.4
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Index           =   49
            Left            =   7440
            TabIndex        =   67
            Top             =   840
            Width           =   300
         End
         Begin VB.CommandButton btnBFilter 
            Caption         =   "w"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.4
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Index           =   50
            Left            =   7740
            TabIndex        =   66
            Top             =   840
            Width           =   300
         End
         Begin VB.CommandButton btnBFilter 
            Caption         =   "x"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.4
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Index           =   51
            Left            =   8040
            TabIndex        =   65
            Top             =   840
            Width           =   300
         End
         Begin VB.CommandButton btnBFilter 
            Caption         =   "m"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.4
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Index           =   52
            Left            =   4740
            TabIndex        =   64
            Top             =   840
            Width           =   300
         End
         Begin VB.CommandButton btnBFilter 
            Appearance      =   0  'Flat
            Caption         =   "n"
            BeginProperty Font 
               Name            =   "Tahoma"
               Size            =   8.4
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   345
            Index           =   53
            Left            =   5040
            TabIndex        =   63
            Top             =   840
            Width           =   300
         End
      End
      Begin VB.TextBox txtFilterValue1 
         Appearance      =   0  'Flat
         Height          =   285
         Left            =   2880
         TabIndex        =   5
         Top             =   5730
         Width           =   2655
      End
      Begin VB.CommandButton btnQuickSearch 
         Height          =   315
         Left            =   5580
         MaskColor       =   &H8000000F&
         Picture         =   "CI.ctx":608A
         Style           =   1  'Graphical
         TabIndex        =   6
         TabStop         =   0   'False
         ToolTipText     =   "Quick Search"
         Top             =   5715
         UseMaskColor    =   -1  'True
         Width           =   450
      End
      Begin MSComctlLib.ImageList ilTree 
         Left            =   7755
         Top             =   60
         _ExtentX        =   995
         _ExtentY        =   995
         BackColor       =   -2147483643
         ImageWidth      =   16
         ImageHeight     =   16
         MaskColor       =   12632256
         UseMaskColor    =   0   'False
         _Version        =   393216
         BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
            NumListImages   =   8
            BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "CI.ctx":633C
               Key             =   "Backend"
            EndProperty
            BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "CI.ctx":6798
               Key             =   "ClosedDefaultFolder"
            EndProperty
            BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "CI.ctx":6BEC
               Key             =   ""
            EndProperty
            BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "CI.ctx":7040
               Key             =   ""
            EndProperty
            BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "CI.ctx":7494
               Key             =   "ClosedStore"
            EndProperty
            BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "CI.ctx":78F0
               Key             =   "OpenStore"
            EndProperty
            BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "CI.ctx":7D4C
               Key             =   "OpenFolder"
            EndProperty
            BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "CI.ctx":81A8
               Key             =   "ClosedFolder"
            EndProperty
         EndProperty
      End
      Begin TrueDBList60.TDBCombo cmbFilterFld1 
         Height          =   345
         Left            =   150
         OleObjectBlob   =   "CI.ctx":8604
         TabIndex        =   3
         Top             =   5715
         Width           =   1410
      End
      Begin TrueDBList60.TDBCombo cmbOp1 
         Height          =   330
         Left            =   1530
         OleObjectBlob   =   "CI.ctx":A511
         TabIndex        =   4
         Top             =   5715
         Width           =   1395
      End
      Begin MSComctlLib.TreeView tvFolders 
         Height          =   4875
         Left            =   135
         TabIndex        =   1
         Top             =   405
         Width           =   9210
         _ExtentX        =   16235
         _ExtentY        =   8594
         _Version        =   393217
         HideSelection   =   0   'False
         Indentation     =   706
         LabelEdit       =   1
         LineStyle       =   1
         Style           =   7
         ImageList       =   "ilTree"
         BorderStyle     =   1
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   9.6
            Charset         =   204
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Line Line2 
         BorderColor     =   &H80000005&
         X1              =   15
         X2              =   9405
         Y1              =   0
         Y2              =   0
      End
      Begin VB.Label lblQuickSearch 
         BackStyle       =   0  'Transparent
         Caption         =   "Find Contacts &Where:"
         Height          =   285
         Left            =   150
         TabIndex        =   2
         Top             =   5505
         Width           =   2025
      End
      Begin VB.Label lblContactLists 
         BackColor       =   &H8000000A&
         BackStyle       =   0  'Transparent
         Caption         =   "Contact Folders/&Directories:"
         DataField       =   "s"
         Height          =   240
         Left            =   150
         TabIndex        =   0
         Top             =   165
         Width           =   2130
      End
      Begin VB.Label lblStatusMsg 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         ForeColor       =   &H00FF0000&
         Height          =   270
         Left            =   3930
         TabIndex        =   54
         Top             =   60
         Visible         =   0   'False
         Width           =   4305
      End
   End
   Begin VB.CommandButton cmdCancel 
      BackColor       =   &H00E0E0E0&
      Caption         =   "Cancel"
      Height          =   435
      Left            =   8205
      TabIndex        =   16
      Top             =   6315
      Width           =   1100
   End
   Begin VB.CommandButton btnGetMofoContacts 
      Caption         =   "Other Mailbo&xes"
      Height          =   420
      Left            =   7110
      TabIndex        =   57
      Top             =   6825
      Width           =   1980
   End
   Begin VB.Frame fraToolbar 
      BorderStyle     =   0  'None
      Height          =   660
      Left            =   0
      TabIndex        =   53
      Top             =   -105
      Width           =   9255
      Begin VB.CommandButton btnHelp 
         Height          =   555
         Left            =   7875
         MaskColor       =   &H00000000&
         Picture         =   "CI.ctx":C413
         Style           =   1  'Graphical
         TabIndex        =   15
         Top             =   120
         UseMaskColor    =   -1  'True
         Width           =   585
      End
      Begin VB.CheckBox chkGroups 
         Caption         =   "&Groups"
         Enabled         =   0   'False
         Height          =   555
         Left            =   6870
         Style           =   1  'Graphical
         TabIndex        =   14
         TabStop         =   0   'False
         ToolTipText     =   "Edit Contact Integration Groups"
         Top             =   120
         Width           =   1020
      End
      Begin VB.CommandButton btnOptions 
         Caption         =   "&Options"
         Height          =   555
         Left            =   6015
         Style           =   1  'Graphical
         TabIndex        =   13
         TabStop         =   0   'False
         Top             =   120
         UseMaskColor    =   -1  'True
         Width           =   870
      End
      Begin VB.CommandButton btnEdit 
         Appearance      =   0  'Flat
         Caption         =   "&Edit"
         Enabled         =   0   'False
         Height          =   555
         Left            =   5205
         Style           =   1  'Graphical
         TabIndex        =   12
         TabStop         =   0   'False
         ToolTipText     =   "Edit selected available contact in native application."
         Top             =   120
         UseMaskColor    =   -1  'True
         Width           =   825
      End
      Begin VB.CommandButton btnAddContactInNative 
         Caption         =   "&New"
         Height          =   555
         Left            =   4395
         Style           =   1  'Graphical
         TabIndex        =   11
         TabStop         =   0   'False
         ToolTipText     =   "Add contact to selected application"
         Top             =   120
         UseMaskColor    =   -1  'True
         Width           =   825
      End
      Begin VB.CommandButton btnRefresh 
         Caption         =   "&Refresh"
         Height          =   555
         Left            =   3495
         MaskColor       =   &H00808080&
         Style           =   1  'Graphical
         TabIndex        =   10
         TabStop         =   0   'False
         ToolTipText     =   "Clear any existing filter and refresh both folders and available contacts (F5)"
         Top             =   120
         UseMaskColor    =   -1  'True
         Width           =   915
      End
      Begin VB.CommandButton btnSearchNative 
         Caption         =   "Nati&ve Find"
         Height          =   555
         Left            =   2415
         MaskColor       =   &H00808080&
         Style           =   1  'Graphical
         TabIndex        =   9
         TabStop         =   0   'False
         ToolTipText     =   "Find contacts in the selected application"
         Top             =   120
         UseMaskColor    =   -1  'True
         Width           =   1095
      End
      Begin VB.CommandButton btnSearch 
         Caption         =   "&Find"
         Height          =   555
         Left            =   1515
         MaskColor       =   &H00808080&
         Style           =   1  'Graphical
         TabIndex        =   8
         TabStop         =   0   'False
         ToolTipText     =   "Find contacts in the selected folder or database"
         Top             =   120
         UseMaskColor    =   -1  'True
         Width           =   915
      End
      Begin VB.CommandButton btnTogglePanels 
         Caption         =   "&Show Contacts"
         Height          =   555
         Left            =   0
         MaskColor       =   &H00FFFFFF&
         Style           =   1  'Graphical
         TabIndex        =   7
         TabStop         =   0   'False
         Top             =   120
         UseMaskColor    =   -1  'True
         Width           =   1530
      End
   End
   Begin VB.Frame fraListings 
      BorderStyle     =   0  'None
      Caption         =   "LISTINGS"
      Height          =   6210
      Left            =   45
      TabIndex        =   39
      Top             =   555
      Visible         =   0   'False
      Width           =   9375
      Begin VB.CommandButton cmdOK 
         Cancel          =   -1  'True
         Caption         =   "&Insert"
         Height          =   435
         Left            =   7005
         MaskColor       =   &H00808080&
         Style           =   1  'Graphical
         TabIndex        =   61
         Top             =   5760
         Width           =   1100
      End
      Begin VB.Frame fraContacts 
         BorderStyle     =   0  'None
         Caption         =   "Contacts"
         Height          =   5415
         Left            =   4440
         TabIndex        =   40
         Top             =   195
         Width           =   4845
         Begin VB.CommandButton btnDelete 
            Enabled         =   0   'False
            Height          =   284
            Index           =   4
            Left            =   60
            Picture         =   "CI.ctx":C9D5
            Style           =   1  'Graphical
            TabIndex        =   60
            Top             =   1200
            UseMaskColor    =   -1  'True
            Width           =   405
         End
         Begin VB.CommandButton btnAdd 
            Appearance      =   0  'Flat
            Enabled         =   0   'False
            Height          =   284
            Index           =   4
            Left            =   60
            Picture         =   "CI.ctx":CB0B
            Style           =   1  'Graphical
            TabIndex        =   59
            Top             =   870
            UseMaskColor    =   -1  'True
            Width           =   405
         End
         Begin TrueDBGrid60.TDBGrid grdEntityList 
            Height          =   4455
            Left            =   540
            OleObjectBlob   =   "CI.ctx":CC29
            TabIndex        =   22
            Top             =   870
            Width           =   4290
         End
         Begin VB.CommandButton btnMoveDown 
            Height          =   225
            Left            =   3465
            Picture         =   "CI.ctx":FEC4
            Style           =   1  'Graphical
            TabIndex        =   55
            Top             =   630
            UseMaskColor    =   -1  'True
            Visible         =   0   'False
            Width           =   255
         End
         Begin VB.CommandButton btnMoveUp 
            Height          =   225
            Left            =   3210
            Picture         =   "CI.ctx":FFB6
            Style           =   1  'Graphical
            TabIndex        =   56
            Top             =   630
            UseMaskColor    =   -1  'True
            Visible         =   0   'False
            Width           =   255
         End
         Begin VB.ListBox lstContacts 
            Height          =   240
            Index           =   0
            Left            =   570
            MultiSelect     =   2  'Extended
            OLEDropMode     =   1  'Manual
            TabIndex        =   26
            Top             =   870
            Width           =   3250
         End
         Begin VB.ListBox lstContacts 
            Height          =   240
            Index           =   1
            Left            =   570
            MultiSelect     =   2  'Extended
            OLEDropMode     =   1  'Manual
            TabIndex        =   30
            Top             =   2040
            Width           =   3250
         End
         Begin VB.ListBox lstContacts 
            Height          =   240
            Index           =   2
            Left            =   570
            MultiSelect     =   2  'Extended
            OLEDropMode     =   1  'Manual
            TabIndex        =   34
            Top             =   3225
            Width           =   3250
         End
         Begin VB.ListBox lstContacts 
            Height          =   432
            Index           =   3
            Left            =   570
            MultiSelect     =   2  'Extended
            OLEDropMode     =   1  'Manual
            TabIndex        =   38
            Top             =   4425
            Width           =   3250
         End
         Begin VB.CommandButton btnAdd 
            Appearance      =   0  'Flat
            Enabled         =   0   'False
            Height          =   284
            Index           =   0
            Left            =   30
            Picture         =   "CI.ctx":100A8
            Style           =   1  'Graphical
            TabIndex        =   23
            Top             =   870
            UseMaskColor    =   -1  'True
            Width           =   405
         End
         Begin VB.CommandButton btnDelete 
            Enabled         =   0   'False
            Height          =   284
            Index           =   0
            Left            =   30
            Picture         =   "CI.ctx":101C6
            Style           =   1  'Graphical
            TabIndex        =   24
            Top             =   1200
            UseMaskColor    =   -1  'True
            Width           =   405
         End
         Begin VB.CommandButton btnDelete 
            Enabled         =   0   'False
            Height          =   284
            Index           =   1
            Left            =   60
            Picture         =   "CI.ctx":102FC
            Style           =   1  'Graphical
            TabIndex        =   28
            Top             =   2385
            UseMaskColor    =   -1  'True
            Width           =   405
         End
         Begin VB.CommandButton btnAdd 
            Enabled         =   0   'False
            Height          =   284
            Index           =   1
            Left            =   60
            Picture         =   "CI.ctx":10432
            Style           =   1  'Graphical
            TabIndex        =   27
            Top             =   2055
            UseMaskColor    =   -1  'True
            Width           =   405
         End
         Begin VB.CommandButton btnDelete 
            Enabled         =   0   'False
            Height          =   284
            Index           =   2
            Left            =   60
            Picture         =   "CI.ctx":10550
            Style           =   1  'Graphical
            TabIndex        =   32
            Top             =   3570
            UseMaskColor    =   -1  'True
            Width           =   405
         End
         Begin VB.CommandButton btnAdd 
            Enabled         =   0   'False
            Height          =   284
            Index           =   2
            Left            =   60
            Picture         =   "CI.ctx":10686
            Style           =   1  'Graphical
            TabIndex        =   31
            Top             =   3240
            UseMaskColor    =   -1  'True
            Width           =   405
         End
         Begin VB.CommandButton btnDelete 
            Enabled         =   0   'False
            Height          =   284
            Index           =   3
            Left            =   60
            Picture         =   "CI.ctx":107A4
            Style           =   1  'Graphical
            TabIndex        =   36
            Top             =   4785
            UseMaskColor    =   -1  'True
            Width           =   405
         End
         Begin VB.CommandButton btnAdd 
            Enabled         =   0   'False
            Height          =   284
            Index           =   3
            Left            =   60
            Picture         =   "CI.ctx":108DA
            Style           =   1  'Graphical
            TabIndex        =   35
            Top             =   4440
            UseMaskColor    =   -1  'True
            Width           =   405
         End
         Begin VB.Label lblEntities 
            BackStyle       =   0  'Transparent
            Caption         =   "En&tities:"
            Height          =   225
            Left            =   585
            TabIndex        =   21
            Top             =   330
            Width           =   4140
         End
         Begin VB.Label lblSelContacts 
            BackStyle       =   0  'Transparent
            Caption         =   "&To:"
            ForeColor       =   &H00000000&
            Height          =   276
            Index           =   0
            Left            =   600
            TabIndex        =   25
            Top             =   648
            Width           =   2004
         End
         Begin VB.Label lblSelContacts 
            BackStyle       =   0  'Transparent
            Caption         =   "Fro&m:"
            ForeColor       =   &H00000000&
            Height          =   255
            Index           =   1
            Left            =   600
            TabIndex        =   29
            Top             =   1830
            Width           =   2000
         End
         Begin VB.Label lblSelContacts 
            BackStyle       =   0  'Transparent
            Caption         =   "&CC:"
            ForeColor       =   &H00000000&
            Height          =   255
            Index           =   2
            Left            =   585
            TabIndex        =   33
            Top             =   3015
            Width           =   2000
         End
         Begin VB.Label lblSelContacts 
            BackStyle       =   0  'Transparent
            Caption         =   "&BCC:"
            Height          =   255
            Index           =   3
            Left            =   600
            TabIndex        =   37
            Top             =   4215
            Width           =   2000
         End
      End
      Begin VB.ComboBox cbxAddressType 
         Height          =   288
         Left            =   90
         Style           =   2  'Dropdown List
         TabIndex        =   18
         Top             =   360
         Width           =   4320
      End
      Begin VB.Frame fraGroups 
         BorderStyle     =   0  'None
         Caption         =   "GROUPS"
         Height          =   5340
         Left            =   4455
         TabIndex        =   41
         Top             =   195
         Visible         =   0   'False
         Width           =   4065
         Begin VB.CommandButton btnDeleteGroup 
            Enabled         =   0   'False
            Height          =   315
            Left            =   3315
            Picture         =   "CI.ctx":109F8
            Style           =   1  'Graphical
            TabIndex        =   45
            ToolTipText     =   "Delete A Group"
            Top             =   165
            UseMaskColor    =   -1  'True
            Width           =   330
         End
         Begin VB.CommandButton btnAddGroup 
            Height          =   315
            Left            =   2985
            Picture         =   "CI.ctx":10C42
            Style           =   1  'Graphical
            TabIndex        =   44
            ToolTipText     =   "Add A Group"
            Top             =   165
            UseMaskColor    =   -1  'True
            Width           =   330
         End
         Begin VB.CommandButton btnAddMember 
            Height          =   284
            Left            =   75
            Picture         =   "CI.ctx":10E8C
            Style           =   1  'Graphical
            TabIndex        =   49
            Top             =   870
            UseMaskColor    =   -1  'True
            Width           =   405
         End
         Begin VB.ComboBox cbxGroups 
            Height          =   315
            Left            =   600
            Style           =   2  'Dropdown List
            TabIndex        =   43
            Top             =   165
            Width           =   2355
         End
         Begin VB.CommandButton btnDeleteMember 
            Height          =   284
            Left            =   75
            Picture         =   "CI.ctx":10FAA
            Style           =   1  'Graphical
            TabIndex        =   48
            Top             =   1200
            UseMaskColor    =   -1  'True
            Width           =   405
         End
         Begin VB.ListBox lstGroupMembers 
            Height          =   3504
            Left            =   600
            MultiSelect     =   2  'Extended
            TabIndex        =   47
            Top             =   870
            Width           =   3030
         End
         Begin VB.Label lblGroupMembers 
            Caption         =   "Group &Members:"
            Height          =   285
            Left            =   645
            TabIndex        =   46
            Top             =   660
            Width           =   1260
         End
         Begin VB.Label lblGroup 
            BackColor       =   &H8000000A&
            BackStyle       =   0  'Transparent
            Caption         =   "Grou&p:"
            Height          =   240
            Left            =   630
            TabIndex        =   42
            Top             =   -35
            Width           =   855
         End
      End
      Begin TrueDBList60.TDBList lstListings 
         Height          =   4455
         Left            =   90
         OleObjectBlob   =   "CI.ctx":110E0
         TabIndex        =   20
         Top             =   1065
         Width           =   4320
      End
      Begin VB.Label lblType 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Height          =   228
         Left            =   2964
         TabIndex        =   121
         Top             =   852
         Width           =   1392
      End
      Begin VB.Line Line1 
         BorderColor     =   &H80000005&
         X1              =   720
         X2              =   9235
         Y1              =   0
         Y2              =   0
      End
      Begin VB.Label lblStatus 
         Appearance      =   0  'Flat
         BackColor       =   &H8000000A&
         BackStyle       =   0  'Transparent
         ForeColor       =   &H80000008&
         Height          =   390
         Left            =   120
         TabIndex        =   52
         Top             =   5625
         Width           =   5565
      End
      Begin VB.Label lblAddressType 
         BackColor       =   &H8000000A&
         BackStyle       =   0  'Transparent
         Caption         =   "Address T&ype:"
         Height          =   255
         Left            =   135
         TabIndex        =   17
         Top             =   150
         Width           =   1980
      End
      Begin VB.Label lblFilter 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000A&
         ForeColor       =   &H000000FF&
         Height          =   240
         Left            =   6135
         TabIndex        =   50
         Top             =   390
         Visible         =   0   'False
         Width           =   990
      End
      Begin VB.Label lblEntries 
         BackStyle       =   0  'Transparent
         Caption         =   "Cont&acts:"
         Height          =   228
         Left            =   120
         TabIndex        =   19
         Top             =   840
         Width           =   3492
      End
   End
   Begin CIX.ProgressBox pbxLoad 
      Height          =   1695
      Left            =   1680
      TabIndex        =   58
      Top             =   2070
      Visible         =   0   'False
      Width           =   4725
      _ExtentX        =   8340
      _ExtentY        =   2985
      Title           =   "Loading Contacts"
   End
   Begin VB.Menu mnuFolders 
      Caption         =   "Folders"
      Visible         =   0   'False
      Begin VB.Menu mnuFolders_SetAsDefault 
         Caption         =   "&Select Current Folder On Startup"
      End
      Begin VB.Menu mnuFolders_LoadOnStartup 
         Caption         =   "&Load Current Folder On Startup"
      End
      Begin VB.Menu mnuFolders_RemoveDefault 
         Caption         =   "&Remove Default Folder"
      End
      Begin VB.Menu mnuFolders_Sep2 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFolders_ToggleQuickFilterType 
         Caption         =   "&Toggle Quick Filter Type"
      End
      Begin VB.Menu mnuFolders_Sep1 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFolders_Custom1 
         Caption         =   "Custom Menu Item 1"
      End
      Begin VB.Menu mnuFolders_Custom2 
         Caption         =   "Custom Menu Item 2"
      End
      Begin VB.Menu mnuFolders_Custom3 
         Caption         =   "Custom Menu Item 3"
      End
      Begin VB.Menu mnuFolders_Custom4 
         Caption         =   "Custom Menu Item 4"
      End
      Begin VB.Menu mnuFolders_Custom5 
         Caption         =   "Custom Menu Item 5"
      End
   End
   Begin VB.Menu mnuContacts 
      Caption         =   "Contacts"
      Visible         =   0   'False
      Begin VB.Menu mnuContacts_ToggleContactList 
         Caption         =   "Expand Available Contacts &List"
         Shortcut        =   ^W
      End
      Begin VB.Menu mnuContacts_SelectAll 
         Caption         =   "Select &All"
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuContacts_ShowDetail 
         Caption         =   "Show &Detail"
      End
      Begin VB.Menu mnuContacts_Sep1 
         Caption         =   "-"
      End
      Begin VB.Menu mnuContacts_MoveUp 
         Caption         =   "Move Selected Contacts &Up"
      End
      Begin VB.Menu mnuContacts_MoveDown 
         Caption         =   "Move Selected Contacts Do&wn"
      End
      Begin VB.Menu mnuContacts_Sep2 
         Caption         =   "-"
      End
      Begin VB.Menu mnuContacts_AddGroup 
         Caption         =   "Add &Group"
         Enabled         =   0   'False
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuContacts_DeleteGroup 
         Caption         =   "Delete Gro&up"
         Enabled         =   0   'False
         Shortcut        =   ^U
      End
   End
   Begin VB.Menu mnuHelp 
      Caption         =   "Help"
      Visible         =   0   'False
      Begin VB.Menu mnuHelp_PDF 
         Caption         =   ""
         Index           =   0
         Shortcut        =   {F1}
         Visible         =   0   'False
      End
      Begin VB.Menu mnuHelp_Sep1 
         Caption         =   "-"
      End
      Begin VB.Menu mnuHelp_About 
         Caption         =   "&About Contact Integration"
         Shortcut        =   ^{F6}
      End
   End
End
Attribute VB_Name = "CI"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private Declare Function GetTickCount Lib "kernel32" () As Long

Enum CIPanels
    ciPanel_folders = 1
    ciPanel_Contacts = 2
    ciPanel_Groups = 3
End Enum

Enum ciSearchTypes
    ciSearchType_None = 0
    ciSearchType_Dialog = 1
    ciSearchType_Native = 2
    ciSearchType_Quick = 3
    ciSearchType_Refresh = 4
End Enum

Private Enum ciListBoxes
    ciListBoxes_Listings = -1
    ciListBoxes_To = 0
    ciListBoxes_From = 1
    ciListBoxes_CC = 2
    ciListBoxes_BCC = 3
    ciListBoxes_GroupMembers = 4
End Enum

Enum cixErrs
    cixErr_ObjectExpected = vbError + 3512 + 1
End Enum

Private Const ciSecsPerDay As Long = 86400

Private Const ciUserIniKey_LoadDefaultOnStartup As String = "LoadDefaultOnStartup"
Private Const ciUserIniKey_DefaultNode As String = "DefaultNode"
Private Const ciListingsNarrow As Single = 4320
Private Const ciUserIniKey_DefaultQuickFilterType As String = "DefaultQuickFilterType"

Private m_oListings As CIO.CListings
Private m_oAddresses As CIO.CAddresses
Private m_oContacts As CIO.CContacts
Private m_oUNID As CIO.CUNID
Private m_oEntityList As XArrayDB
Private WithEvents m_oEvents As CIO.CEventGenerator
Attribute m_oEvents.VB_VarHelpID = -1
Private m_iDataTo As CIO.ciRetrieveData
Private m_iDataFrom As CIO.ciRetrieveData
Private m_iDataCC As CIO.ciRetrieveData
Private m_iDataBCC As CIO.ciRetrieveData
Private m_bShowBackendNodes As Boolean
Private m_iAlerts As CIO.ciAlerts
Private m_iSelectionLists As CIX.ciSelectionLists
Private m_bMouseDown As Boolean
Private m_bLoadCancelled As Boolean
Private m_iMaxContacts As Integer
Private m_bInitialized As Boolean
Private m_iCurList As ciListBoxes
Private m_bShowedContacts As Boolean
Private m_xCustomFunction1 As String
Private m_xCustomFunction2 As String
Private m_bMenuRequested As Boolean
Private m_lMaxTo As Long
Private m_lMaxFrom As Long
Private m_lMaxCC As Long
Private m_lMaxBCC As Long

Public Event InsertionCancelled()
Public Event InsertionOK(oContacts As CIO.CContacts)
Public Event PanelChange(bFolderView As Boolean)

Public Enum ciSelectionLists
    ciSelectionList_To = 1
    ciSelectionList_From = 2
    ciSelectionList_CC = 4
    ciSelectionList_BCC = 8
    ciSelectionList_Custom = 16
End Enum

Public Enum ciQuickFilterType
    ciQuickFilterType_Text = 1
    ciQuickFilterType_AlphaNumeric = 2
End Enum

Public g_iQuickFilterType As ciQuickFilterType
Public g_iQuickFilterBtnIndex As Integer
Public g_iQuickFilterOptionIndex As Integer

'**********************************************************
'initializer/terminator
'**********************************************************
Dim xObjectID As String

'**********************************************************

Public Property Let MaxContacts(ByVal iNew As Integer)
    m_iMaxContacts = iNew
End Property

Public Property Get MaxContacts() As Integer
    MaxContacts = m_iMaxContacts
End Property

Public Property Let OKButtonCaption(xNew As String)
    UserControl.cmdOK.Caption = xNew
End Property

Public Property Get OKButtonCaption() As String
    OKButtonCaption = UserControl.cmdOK.Caption
End Property
'*c
Public Property Let LabelTo(xNew As String)
    UserControl.lblSelContacts(0).Caption = xNew
End Property

Public Property Get LabelTo() As String
    LabelTo = UserControl.lblSelContacts(0).Caption
End Property
Public Property Let LabelFrom(xNew As String)
    UserControl.lblSelContacts(1).Caption = xNew
End Property

Public Property Get LabelFrom() As String
    LabelFrom = UserControl.lblSelContacts(1).Caption
End Property
Public Property Let LabelCC(xNew As String)
    UserControl.lblSelContacts(2).Caption = xNew
End Property

Public Property Get LabelCC() As String
    LabelCC = UserControl.lblSelContacts(2).Caption
End Property
Public Property Let LabelBCC(xNew As String)
    UserControl.lblSelContacts(3).Caption = xNew
End Property

Public Property Get LabelBCC() As String
    LabelBCC = UserControl.lblSelContacts(3).Caption
End Property

Public Property Let MaxTo(lNew As Long)
    m_lMaxTo = lNew
End Property
Public Property Get MaxTo() As Long
    MaxTo = m_lMaxTo
End Property
Public Property Let MaxFrom(lNew As Long)
    m_lMaxFrom = lNew
End Property
Public Property Get MaxFrom() As Long
    MaxFrom = m_lMaxFrom
End Property
Public Property Let MaxCC(lNew As Long)
    m_lMaxCC = lNew
End Property
Public Property Get MaxCC() As Long
    MaxCC = m_lMaxCC
End Property
Public Property Let MaxBCC(lNew As Long)
    m_lMaxBCC = lNew
End Property
Public Property Get MaxBCC() As Long
    MaxBCC = m_lMaxBCC
End Property
'*********
Public Property Let ShowBackendNodes(bNew As Boolean)
    m_bShowBackendNodes = bNew
End Property

Public Property Get ShowBackendNodes() As Boolean
    ShowBackendNodes = m_bShowBackendNodes
End Property

Public Property Let DataRetrievedForTo(iNew As ciRetrieveData)
    m_iDataTo = iNew
End Property

Public Property Get DataRetrievedForTo() As ciRetrieveData
    DataRetrievedForTo = m_iDataTo
End Property

Public Property Let DataRetrievedForFrom(iNew As ciRetrieveData)
    m_iDataFrom = iNew
End Property

Public Property Get DataRetrievedForFrom() As ciRetrieveData
    DataRetrievedForFrom = m_iDataFrom
End Property

Public Property Let DataRetrievedForCC(iNew As ciRetrieveData)
    m_iDataCC = iNew
End Property

Public Property Get DataRetrievedForCC() As ciRetrieveData
    DataRetrievedForCC = m_iDataCC
End Property

Public Property Let DataRetrievedForBCC(iNew As ciRetrieveData)
    m_iDataBCC = iNew
End Property

Public Property Get DataRetrievedForBCC() As ciRetrieveData
    DataRetrievedForBCC = m_iDataBCC
End Property

Public Property Let Alerts(iNew As ciAlerts)
    m_iAlerts = iNew
End Property

Public Property Get Alerts() As ciAlerts
    Alerts = m_iAlerts
End Property

Public Property Let EntityList(oNew As XArrayDB)
    Set m_oEntityList = oNew
End Property

Public Property Get EntityList() As XArrayDB
    Set EntityList = m_oEntityList
End Property

Public Property Let SelectionLists(iNew As ciSelectionLists)
    m_iSelectionLists = iNew
    SetSelectionLists iNew
End Property

Public Property Get SelectionLists() As ciSelectionLists
    SelectionLists = m_iSelectionLists
End Property

Public Property Get SelectedContacts() As CIO.CContacts
    Set SelectedContacts = m_oContacts
End Property

Private Sub btnAdd_Click(Index As Integer)
    With UserControl.btnAdd
        If Not .Item(Index).Visible Then
            Exit Sub
        End If
        
        'ensure that clicked button is set as the default button
        .Item(Index).Default = True
        
        'GLOG : 7598 : CEH
'        Select Case Index
'            Case 1
'                Me.SelectionList = ciSelectionList_From
'                If Me.MaxFrom > 0 Then
'                    Me.MaxContacts = CInt(Me.MaxFrom)
'                Else
'                    Me.MaxContacts = g_iDefaultMaxContacts
'                End If
'            Case 2
'                Me.SelectionList = ciSelectionList_CC
'                If Me.MaxCC > 0 Then
'                    Me.MaxContacts = CInt(Me.MaxCC)
'                Else
'                    Me.MaxContacts = g_iDefaultMaxContacts
'                End If
'            Case 3
'                Me.SelectionList = ciSelectionList_BCC
'                If Me.MaxBCC > 0 Then
'                    Me.MaxContacts = CInt(Me.MaxBCC)
'                Else
'                    Me.MaxContacts = g_iDefaultMaxContacts
'                End If
'            Case 4
'                Me.SelectionList = ciSelectionList_Custom
'                Me.MaxContacts = g_iDefaultMaxContacts
'            Case Else
'                Me.SelectionList = ciSelectionList_To
'                If Me.MaxTo > 0 Then
'                    Me.MaxContacts = CInt(Me.MaxTo)
'                Else
'                    Me.MaxContacts = g_iDefaultMaxContacts
'                End If
'        End Select
    End With
    AddSelContacts
End Sub

Private Property Get SelectionListControl() As VB.ListBox
    On Error GoTo ProcError
    With UserControl.lstContacts
        Select Case Me.SelectionList
            Case ciSelectionList_From
                Set SelectionListControl = .Item(1)
            Case ciSelectionList_CC
                Set SelectionListControl = .Item(2)
            Case ciSelectionList_BCC
                Set SelectionListControl = .Item(3)
            Case Else
                Set SelectionListControl = .Item(0)
        End Select
    End With
    Exit Property
ProcError:
    g_oError.RaiseError "CIX.CI.SelectionListControl"
    Exit Property
End Property

Public Property Let SelectionList(iNew As ciSelectionLists)
'sets up dialog to highlight new selection list
    Dim i As Integer
    Dim iSelListBoxIndex As Integer
    Dim iPrevSelListBoxIndex As Integer
    Static iPrevSelList As Integer
    
    On Error GoTo ProcError
    
    If iNew = ciSelectionList_Custom Then
        Exit Property
    End If
    
    If iPrevSelList = iNew Then
        'selection list has not changed - exit
        Exit Property
    ElseIf (iNew And SelectionLists) = 0 Then
        'the selection list that has been requested
        'to be the default selection list is not
        'among the selection lists that are displayed
        'in the dialog
        Exit Property
    End If
    
    'get list box that corresponds to selection list
    Select Case iNew
        Case ciSelectionList_From
            iSelListBoxIndex = 1
        Case ciSelectionList_CC
            iSelListBoxIndex = 2
        Case ciSelectionList_BCC
            iSelListBoxIndex = 3
        Case Else
            iSelListBoxIndex = 0
    End Select

    'get list box that corresponds to
    'the previous selection list
    Select Case iPrevSelList
        Case ciSelectionList_From
            iPrevSelListBoxIndex = 1
        Case ciSelectionList_CC
            iPrevSelListBoxIndex = 2
        Case ciSelectionList_BCC
            iPrevSelListBoxIndex = 3
        Case Else
            iPrevSelListBoxIndex = 0
    End Select

    With UserControl
        If iPrevSelList > 0 Then
            'remove all selections from previous listbox
            With .lstContacts(iPrevSelListBoxIndex)
                For i = 0 To .ListCount - 1
                    .Selected(i) = False
                Next
            End With
            
            'change previous list label color to black
            .lblSelContacts _
                .Item(iPrevSelListBoxIndex).ForeColor = vbButtonText
        End If
        
        'ensure that the appropriate
        'add button is made the default
        .btnAdd.Item(iSelListBoxIndex).Default = True
        
        'set new selection list label color to blue
        .lblSelContacts.Item(iSelListBoxIndex).ForeColor = vbBlue
        
        'enable/disable delete buttons
        For i = 0 To 3
            .btnDelete.Item(i).Enabled = (iSelListBoxIndex = i) And _
                (.lstContacts(i).ListCount > 0) And (.lstContacts(i).ListIndex > -1)
        Next i
        
        'move up down buttons into correct position
        With UserControl
            .btnMoveUp.Top = .lstContacts(iSelListBoxIndex).Top - _
                             .btnMoveUp.Height
            .btnMoveDown.Top = .btnMoveUp.Top
            .btnMoveUp.Visible = True
            .btnMoveDown.Visible = True
        End With
    
        iPrevSelList = iNew
    End With
    Exit Property
ProcError:
    g_oError.RaiseError "CIX.CI.SelectionList"
    Exit Property
End Property

Public Property Get SelectionList() As ciSelectionLists
'returns the current selection list - i.e. the
'list that currently receives double-clicked listings
    On Error GoTo ProcError
    With UserControl.btnAdd
        If .Item(1).Default Then
            SelectionList = ciSelectionList_From
        ElseIf .Item(2).Default Then
            SelectionList = ciSelectionList_CC
        ElseIf .Item(3).Default Then
            SelectionList = ciSelectionList_BCC
        ElseIf .Item(4).Default Then
            SelectionList = ciSelectionList_Custom
        Else
            SelectionList = ciSelectionList_To
        End If
    End With
    Exit Property
ProcError:
    g_oError.RaiseError "CIX.CI.SelectionList"
    Exit Property
End Property

Private Sub AddSelContacts()
'adds the selected listings to the specified list
    Dim oBEnd As CIO.ICIBackend
    Dim oContacts As CIO.CContacts
    Dim oC As CIO.CContact
    Dim oL As CIO.CListing
    Dim oAddr As CIO.CAddress
    Dim lBookmark As Long
    Dim l As Long
    Dim lNumSel As Long
    Dim lAlertThreshold As Long
    Dim iChoice As VbMsgBoxResult
    Dim iNumTo As Integer
    Dim iNumFrom As Integer
    Dim iNumCC As Integer
    Dim iNumBCC As Integer
    Dim lInsertAt As Long
    Dim bMaxReached As Boolean
    
    'used to generate a number sequence below
    Static iID As Integer
    
    On Error GoTo ProcError
    
    With UserControl
        With .lstListings
            'select 1st row if necessary
            If .SelBookmarks.Count = 0 Then
                If .Row > -1 Then
                    .SelBookmarks.Add .FirstRow + .Row
                End If
            ElseIf .SelBookmarks.Count = 1 And .SelBookmarks(0) = 0 Then
                If .Row > -1 Then
                    .SelBookmarks.Add .FirstRow + .Row
                End If
            End If
        End With
        
        'UserControl.MousePointer = vbHourglass
        
        lNumSel = .lstListings.SelBookmarks.Count
        
        'get existing count and check max amount in each group
        With UserControl.lstContacts
            iNumTo = .Item(0).ListCount
            iNumFrom = .Item(1).ListCount
            iNumCC = .Item(2).ListCount
            iNumBCC = .Item(3).ListCount
        End With
        
'v.2.4.4007
        Select Case Me.SelectionList
            Case CIX.ciSelectionList_To
                If Me.MaxTo And Me.MaxTo <= iNumTo Then
                    bMaxReached = True
                End If
            Case CIX.ciSelectionList_From
                If Me.MaxFrom And Me.MaxFrom <= iNumFrom Then
                    bMaxReached = True
                End If
            Case CIX.ciSelectionList_CC
                If Me.MaxCC And Me.MaxCC <= iNumCC Then
                    bMaxReached = True
                End If
            Case CIX.ciSelectionList_BCC
                If Me.MaxBCC And Me.MaxBCC <= iNumBCC Then
                    bMaxReached = True
                End If
'            Case CIX.ciSelectionList_Custom
'                'get data retrieved info from entity array
        End Select
        
        If bMaxReached Then
            MsgBox "You have reached the maximum number of contacts allowed in this field.", vbExclamation, g_oSessionType.AppTitle
            UserControl.MousePointer = vbDefault
            Exit Sub
        End If
        
        'get the threshold number of selected contacts
        'for which the user should be prompted.
        If lAlertThreshold = 0 Then
            On Error Resume Next
            lAlertThreshold = g_oIni.GetIni("CIApplication", "AlertThreshold")
            On Error GoTo ProcError
        End If
        
        If lAlertThreshold = 0 Then
            lAlertThreshold = 15
        End If
        
        If lNumSel > lAlertThreshold Then
            'prompt before executing - alert threshold has been triggered
            iChoice = MsgBox("You are about to add " & lNumSel & _
                " contacts.  Do you want to continue?", vbQuestion + vbYesNo)
            If iChoice = vbNo Then
                UserControl.MousePointer = vbDefault
                Exit Sub
            Else
                With UserControl.pbxLoad
                    .Cancel = False
                    .Visible = True
                End With
            End If
       End If
        
        SetupForInputState False
        
        For l = 0 To lNumSel - 1
            If lNumSel > lAlertThreshold Then
                If l = 0 Then
                    SetupForInputState False
                    With UserControl.pbxLoad
                        .Title = "Adding Contacts"
                        .ShowCancelled = True
                        .Value = 0
                        .Message = "Adding contacts. Please wait... 0 of " & lNumSel
                        .Visible = True
                        .ZOrder 0
                        DoEvents
                    End With
                End If
            End If
            
            'check that we haven't reached the
            'maximum number of contacts allowed
            If Me.MaxContacts > 0 Then
                If m_oContacts.Count = Me.MaxContacts Then
                    If Me.MaxContacts = 1 Then
                        MsgBox "No more than 1 contact may be " & _
                            "inserted in this situation.", vbExclamation, g_oSessionType.AppTitle
                    Else
                        MsgBox "No more than " & Me.MaxContacts & _
                            " contacts may be inserted in this situation.", vbExclamation, g_oSessionType.AppTitle
                    End If
                    UserControl.MousePointer = vbDefault
                    SetupForInputState True
                    Exit Sub
                End If
            End If
            
            On Error GoTo ProcError
            lBookmark = .lstListings.SelBookmarks(l)
'           get selected listing
            Set oL = m_oListings.Item(lBookmark)
            If oL Is Nothing Then
                Err.Raise CIO.ciErr_InvalidListing, , _
                    "Listing is invalid."
            End If
            
            If m_oAddresses Is Nothing Then
                MsgBox g_xUnvalidAddress
                ResetUserControlForInput
                Exit Sub
            ElseIf m_oAddresses.Count > 0 Then
'               get selected address
                On Error Resume Next
                
                'exit if still no type is chosen
                If .cbxAddressType.ListIndex = -1 Then
                    LoadAddresses
                    UserControl.MousePointer = vbDefault
                    Exit Sub
                End If
                
                Set oAddr = m_oAddresses.Item(.cbxAddressType.ListIndex + 1)
                On Error GoTo ProcError
                If oAddr Is Nothing Then
                    MsgBox g_xUnvalidAddress
                    ResetUserControlForInput
                    Exit Sub
                End If
            Else
                MsgBox g_xUnvalidAddress
                ResetUserControlForInput
                Exit Sub
            End If
            
'           get contact from listing and address
            Dim iDataRetrieved As CIO.ciRetrieveData
            Select Case Me.SelectionList
                Case CIX.ciSelectionList_To
                    iDataRetrieved = Me.DataRetrievedForTo
                Case CIX.ciSelectionList_From
                    iDataRetrieved = Me.DataRetrievedForFrom
                Case CIX.ciSelectionList_CC
                    iDataRetrieved = Me.DataRetrievedForCC
                Case CIX.ciSelectionList_BCC
                    iDataRetrieved = Me.DataRetrievedForBCC
                Case CIX.ciSelectionList_Custom
                    'get data retrieved info from entity array
                    iDataRetrieved = Me.EntityList(UserControl.grdEntityList.Bookmark, 1)
            End Select
            
            Set oContacts = ListingBackend(oL).GetContacts( _
                oL, oAddr, , iDataRetrieved, Me.Alerts)
            
            If (oContacts Is Nothing) Then
'                SetupForInputState True
'
'                With UserControl
'                    .pbxLoad.Visible = False
'                    .MousePointer = vbDefault
'                End With
'
'                Exit Sub
'            Else
                GoTo skip
            End If
            
'           add to contact collection
            For Each oC In oContacts
'               set selection list for contact
                oC.ContactType = Me.SelectionList
                
                'GLOG : 7764 : ceh
'               set Address Type Name for contact
                oC.AddressTypeName = oAddr.Name
                    
'               find the last contact in collection that has the
'               same contact type - if none, add as first of group
                
                'get number of contacts in each group
                With UserControl.lstContacts
                    iNumTo = .Item(0).ListCount
                    iNumFrom = .Item(1).ListCount
                    iNumCC = .Item(2).ListCount
                    iNumBCC = .Item(3).ListCount
                End With
                
                Select Case oC.ContactType
                    Case CIX.ciSelectionList_To
                        'insert after last 'To' contact
                        lInsertAt = iNumTo + 1
                    Case CIX.ciSelectionList_From
                        lInsertAt = iNumTo + iNumFrom + 1
                    Case CIX.ciSelectionList_CC
                        lInsertAt = iNumTo + iNumFrom + iNumCC + 1
                    Case CIX.ciSelectionList_BCC
                        lInsertAt = iNumTo + iNumFrom + iNumCC + iNumBCC + 1
                    Case CIX.ciSelectionList_Custom
                        lInsertAt = m_oContacts.Count + 1
                End Select
                
                'delete existing if necessary
                If oC.ContactType = CIX.ciSelectionList_Custom Then
                    If Me.EntityList.Value(UserControl.grdEntityList.Bookmark, 3) <> Empty Then
                        Dim j As Integer
            '           remove from collection - use tag property as id -
            '           key value is concatenation of name and listing id, and
            '           is used for uniqueness
                        For j = 1 To m_oContacts.Count
                            If m_oContacts.Item(j).Tag = Me.EntityList.Value(UserControl.grdEntityList.Bookmark, 3) Then
                                m_oContacts.Delete j
                                Exit For
                            End If
                        Next j
                    End If
                End If
                
                On Error Resume Next
                If lInsertAt <= m_oContacts.Count Then
                    m_oContacts.Add oC, lInsertAt
                Else
                    m_oContacts.Add oC
                End If
                
                If Err.Number = 0 Then
                    'get next number in the sequence
                    iID = iID + 1
                    
                    'tag the contact with this unique number
                    oC.Tag = iID
                    
                    'update display name & custom type/entity name
                    If oC.ContactType = CIX.ciSelectionList_Custom Then
                        With UserControl.grdEntityList
                            oC.CustomTypeName = Me.EntityList.Value(UserControl.grdEntityList.Bookmark, 0)
                            UserControl.btnDelete(4).Enabled = True
                            Me.EntityList.Value(UserControl.grdEntityList.Bookmark, 2) = IIf(oC.DisplayName <> "", oC.DisplayName, oC.Company)
                            Me.EntityList.Value(UserControl.grdEntityList.Bookmark, 3) = iID
                            .Array = Me.EntityList
                            .ReBind
                        End With
                    Else
'                       add full name to selected list
                        With SelectionListControl
                            .AddItem oC.DisplayName
                            .ItemData(.ListCount - 1) = iID
                        End With
                    End If
                    
                ElseIf Err.Number = 457 Then
                    MsgBox oC.FullName & " has already been added.", vbExclamation, g_oSessionType.AppTitle
                Else
                    Err.Raise Err.Number, , Err.Description
                End If
            Next oC
            
           If lNumSel > lAlertThreshold Then
                UserControl.pbxLoad.Message = _
                    "Adding contacts. Please wait... " & l + 1 & " of " & lNumSel
                UserControl.pbxLoad.Value = (l + 1) / lNumSel
            End If

            If m_bLoadCancelled Then
                MsgBox "You canceled this operation.  " & l + 1 & " contacts have been added.", vbExclamation
                
                'reset for next iteration
                m_bLoadCancelled = False
                Exit For
            End If
skip:
        Next l
    End With
    
    SetupForInputState True
    
    With UserControl
        .pbxLoad.Visible = False
        .lstListings.SetFocus
        .MousePointer = vbDefault
    End With
    
'   enable insert btn only if contacts exist
    UserControl.cmdOK.Enabled = m_oContacts.Count
    
    Exit Sub
ProcError:
    Dim lErr As Long
    Dim xDesc As String
    
    lErr = Err.Number
    xDesc = Err.Description
    
'   enable insert btn only if contacts exist
    UserControl.cmdOK.Enabled = m_oContacts.Count

    SetupForInputState True
    
    With UserControl
        .pbxLoad.Visible = False
        .MousePointer = vbDefault
    End With
    
    Err.Number = lErr
    Err.Description = xDesc
    g_oError.RaiseError "CIX.frmListings.AddSelContacts"
    Exit Sub
End Sub

Private Sub ResetUserControlForInput()
'   enable insert btn only if contacts exist
    UserControl.cmdOK.Enabled = m_oContacts.Count

    SetupForInputState True
    
    With UserControl
        .pbxLoad.Visible = False
        .MousePointer = vbDefault
    End With

End Sub
Private Sub DeleteSelContacts(Optional ByVal bPrompt As Boolean = True)
    Dim i As Integer
    Dim j As Integer
    Dim k As Integer
    Dim iChoice As VbMsgBoxResult
    Dim oContact As CIO.CContact
    Dim xMsg As String
    
    On Error GoTo ProcError
    
    With SelectionListControl()
        If bPrompt Then
            Select Case .SelCount
                Case 0
                    Exit Sub
                Case 1
                    xMsg = "Delete selected contact?"
                Case Else
                    xMsg = "Delete selected contacts?"
            End Select
            iChoice = MsgBox(xMsg, vbQuestion + vbYesNo, g_oSessionType.AppTitle)
        End If
        
        If iChoice = vbYes Or (Not bPrompt) Then
            For i = .ListCount - 1 To 0 Step -1
                If .Selected(i) Then
'                   remove from collection - use tag property as id -
'                   key value is concatenation of name and listing id, and
'                   is used for uniqueness
                    For j = 1 To m_oContacts.Count
                        If m_oContacts.Item(j).Tag = .ItemData(i) Then
                            m_oContacts.Delete j
                            Exit For
                        End If
                    Next j
            
'                   remove from list
                    .RemoveItem i
                    
'                   tag this index as the last removed item-
'                   will be used below to select new item
'                   when all is done
                    k = i
                End If
            Next i
    
'           select new item
            If .ListCount Then
                .Selected(ciMin(.ListCount - 1, CDbl(k))) = True
                .SetFocus
            Else
                UserControl.lstListings.SetFocus
            End If

'           disable delete button if nothing is selected
            UserControl.btnDelete(.Index).Enabled = .ListCount
        End If
    End With
    
''   enable insert btn only if contacts exist
'    UserControl.cmdOK.Enabled = m_oContacts.Count
        
    Exit Sub

ProcError:
    Dim lErr As Long
    Dim xDesc As String

    lErr = Err.Number
    xDesc = Err.Description
    
'   enable insert btn only if contacts exist
    UserControl.cmdOK.Enabled = m_oContacts.Count
    
    Err.Number = lErr
    Err.Description = xDesc
        
    g_oError.RaiseError "CIX.CI.DeleteSelContacts"
    Exit Sub
End Sub


Private Sub DeleteSelContacts_Custom(Optional ByVal bPrompt As Boolean = True)
    Dim i As Integer
    Dim j As Integer
    Dim k As Integer
    Dim iChoice As VbMsgBoxResult
    Dim oContact As CIO.CContact
    Dim xMsg As String
    
    On Error GoTo ProcError
    
    With UserControl.grdEntityList
        If bPrompt Then
            xMsg = "Delete selected contact?"
            iChoice = MsgBox(xMsg, vbQuestion + vbYesNo, g_oSessionType.AppTitle)
        End If
        
        If iChoice = vbYes Or (Not bPrompt) Then
            
            With UserControl.grdEntityList
    '           remove from collection - use tag property as id -
    '           key value is concatenation of name and listing id, and
    '           is used for uniqueness
                For j = 1 To m_oContacts.Count
                    If m_oContacts.Item(j).Tag = Me.EntityList.Value(UserControl.grdEntityList.Bookmark, 3) Then
                        m_oContacts.Delete j
                        Exit For
                    End If
                Next j

'               remove display name/id from entity row
                Me.EntityList.Value(UserControl.grdEntityList.Bookmark, 2) = ""
                Me.EntityList.Value(UserControl.grdEntityList.Bookmark, 3) = ""
                .Array = Me.EntityList
                .ReBind

            
            End With
            
'           disable delete button since nothing is selected
            UserControl.btnDelete.Item(4).Enabled = False
        End If
    End With
    
'   enable insert btn only if contacts exist
'    UserControl.cmdOK.Enabled = m_oContacts.Count
    
    Exit Sub

ProcError:
    Dim lErr As Long
    Dim xDesc As String

    lErr = Err.Number
    xDesc = Err.Description
    
'   enable insert btn only if contacts exist
    UserControl.cmdOK.Enabled = m_oContacts.Count
    
    Err.Number = lErr
    Err.Description = xDesc
        
    g_oError.RaiseError "CIX.CI.DeleteSelContacts_Custom"
    Exit Sub
End Sub


Private Function LoadContacts(Optional ByVal iSearchType As ciSearchTypes = ciSearchType_None) As Long
'loads the contacts in the selected folder,
'using the specified search method - note that
'some search methods ignore the selected folder
    Dim oFolder As CIO.CFolder
    Dim oFilter As CIO.CFilter
    Dim oNode As MSComctlLib.node
    Dim oStore As CIO.CStore
    Dim iBackendID As Integer
    Dim oFld1 As CIO.CFilterField
    Dim oFld2 As CIO.CFilterField
    Dim bCancelled As Boolean
    Dim xUNID As String
    Dim xStoreName As String
    Dim oB As CIO.ICIBackend
    Dim oListings As CIO.CListings
    Dim xSearchValue As String
    
    Static iPrevBackendID As Integer
    Static iPrevSearchType As ciSearchTypes
    
    On Error GoTo ProcError
    Set oNode = UserControl.tvFolders.SelectedItem
    
    If oNode Is Nothing Then
        Exit Function
    End If
    
    xUNID = oNode.Key
    xStoreName = oNode.Text
    
    iBackendID = m_oUNID.GetUNIDField(xUNID, ciUNIDFields_Backend)
    
    'exit if the search type is not a native search
    'and the node is a backend - no backend searching allowed
    If (iSearchType <> ciSearchType_Native) And (Backends.ItemFromID(iBackendID).SupportsFolders) Then
        If m_oUNID.GetUNIDType(xUNID) = ciUNIDType_Backend Then
            Exit Function
        End If
    End If
    
    If (iPrevBackendID <> iBackendID) And (iSearchType = ciSearchType_Refresh) Then
        'can't refresh if backend was switched - alert
        MsgBox "You cannot execute the 'Refresh' function after you " & _
            "have changed backends.", vbExclamation, g_oSessionType.AppTitle
        Exit Function
    ElseIf (iPrevSearchType = ciSearchType_Native) And _
        (iSearchType = ciSearchType_Refresh) Then
        MsgBox "You cannot execute the 'Refresh' function after executing " & _
            "'Native Find'.  Please rerun 'Native Find'.", _
            vbExclamation, g_oSessionType.AppTitle
        Exit Function
    End If
    
    Set oB = mdlMain.GetBackendFromID(iBackendID)
        
    SetupForInputState False

'   get filter
    Set oFilter = mdlMain.GetFilter(iBackendID)
                
    Dim iUNIDType As ciUNIDTypes
    iUNIDType = m_oUNID.GetUNIDType(xUNID)
        
    Select Case iSearchType
        Case ciSearchType_Native
            Set oListings = oB.SearchNative()
        Case ciSearchType_Quick
            With UserControl
                'set filter based on Filter type
                If g_iQuickFilterType = ciQuickFilterType_Text Then
                    'Quick Filter text
                    Set oFld1 = oFilter.FilterFields(.cmbFilterFld1.SelectedItem + 1)
                    oFld1.Value = .txtFilterValue1.Text
                    oFld1.Operator = .cmbOp1.BoundText
                Else
                    'Quick Filter buttons
                    Set oFld1 = oFilter.FilterFields(g_iQuickFilterOptionIndex + 1)
                    oFld1.Value = UserControl.picBFilter(g_iQuickFilterBtnIndex).Tag
                    oFld1.Operator = ciSearchOperator_BeginsWith
                End If
            End With
            
            If iUNIDType = ciUNIDType_Store And _
                oB.IsSearchableEntity(xUNID) Then
                'selected node is a store and store searches
                'are supported by the backend - search store
                Set oStore = m_oUNID.GetStore(xUNID, xStoreName)
                Set oListings = oB.GetStoreListings(oStore, oFilter)
            ElseIf iUNIDType = ciUNIDType_Folder And _
                oB.IsSearchableEntity(xUNID) Then
                'selected node is a folder - search folder
                Set oFolder = m_oUNID.GetFolder(xUNID)
                oFolder.Name = UserControl.tvFolders.SelectedItem.Text
                Set oListings = oB.GetFolderListings(oFolder, oFilter)
            End If
            If Not oFilter Is Nothing Then
                oFilter.Reset
            End If
        Case ciSearchType_Dialog
            If iUNIDType = ciUNIDType_Store And _
                oB.IsSearchableEntity(xUNID) Then
                'this is not allowed - must *search* in a store
                Set oStore = m_oUNID.GetStore(xUNID, xStoreName)
                Set oListings = oB.SearchStore(oStore)
            ElseIf iUNIDType = ciUNIDType_Folder And _
                oB.IsSearchableEntity(xUNID) Then
                'selected node is a folder - search folder
                Set oFolder = m_oUNID.GetFolder(xUNID)
                oFolder.Name = UserControl.tvFolders.SelectedItem.Text
                Set oListings = oB.SearchFolder(oFolder, bCancelled)
            End If
        Case ciSearchType_None
            'clear filter conditions
            oFilter.Reset
            
            If iUNIDType = ciUNIDType_Store And _
                oB.IsLoadableEntity(xUNID) Then
                'selected node is a store and store contact loads
                'are supported by the backend - search store
                Set oStore = m_oUNID.GetStore(xUNID, xStoreName)
                Set oListings = oB.GetStoreListings(oStore, oFilter)
            ElseIf iUNIDType = ciUNIDType_Folder And _
                oB.IsLoadableEntity(xUNID) Then
                'selected node is a folder - get all listing in folder
                Set oFolder = m_oUNID.GetFolder(xUNID)
                oFolder.Name = UserControl.tvFolders.SelectedItem.Text
                Set oListings = oB.GetFolderListings(oFolder, oFilter)
            ElseIf iUNIDType = ciUNIDType_Backend And oB.IsLoadableEntity(xUNID) Then
                Set oStore = m_oUNID.GetStore(xUNID, xStoreName)
                Set oListings = oB.GetStoreListings(oStore, oFilter)
            End If
        Case ciSearchType_Refresh
            If oB.IsLoadableEntity(xUNID) Then
                If iUNIDType = ciUNIDType_Store Then
                    Set oStore = m_oUNID.GetStore(xUNID, xStoreName)
                    oFilter.Reset
                    Set oListings = oB.GetStoreListings(oStore, oFilter)
                    UserControl.MousePointer = vbDefault
                ElseIf iUNIDType = ciUNIDType_Folder And _
                    oB.IsLoadableEntity(xUNID) Then
                    'selected node is a folder - get all listing in folder
                    Set oFolder = m_oUNID.GetFolder(xUNID)
                    oFilter.Reset
                    oFolder.Name = UserControl.tvFolders.SelectedItem.Text
                    Set oListings = oB.GetFolderListings(oFolder, oFilter)
                End If
                
            Else
                MsgBox "Can't remove filter because the selected folder or filing cabinet is not loadable.", vbOKOnly + vbExclamation, g_oSessionType.AppTitle
            End If
    End Select
    
    If (Not bCancelled) And Not (oListings Is Nothing) Then
        'assign listings to module level listings collection, which
        'is the collection that is displayed in the control
        Set m_oListings = oListings
        
        'clear out listings
        With UserControl.lstListings
            If Not .Array Is Nothing Then
                .Array.Clear
                .Refresh
            End If
            
            If iPrevBackendID <> iBackendID Then
                'backend has changed - set column names
                With .Columns
                    If oB.Col1Name = Empty Then
                        Err.Raise ciErrs.ciErr_InvalidColumnParameter, , _
                            "No column 1 Name specified."
                    Else
                        .Item(0).Caption = oB.Col1Name
                        .Item(0).Visible = True
                    End If
                    
                    If oB.Col2Name = Empty Then
                        .Item(1).Visible = False
                        .Item(1).AllowSizing = False
                    Else
                        .Item(1).Caption = oB.Col2Name
                        .Item(1).Visible = True
                        .Item(1).AllowSizing = True
                    End If
                    
                    If oB.Col3Name = Empty Then
                        .Item(2).Visible = False
                        .Item(2).AllowSizing = False
                    Else
                        .Item(2).Caption = oB.Col3Name
                        .Item(2).Visible = True
                        .Item(2).AllowSizing = True
                    End If
                    
                    If oB.Col4Name = Empty Then
                        .Item(3).Visible = False
                        .Item(3).AllowSizing = False
                    Else
                        .Item(3).Caption = oB.Col4Name
                        .Item(3).Visible = True
                        .Item(3).AllowSizing = True
                    End If
                End With
            End If
        End With
        
        'store these for next execution of LoadContacts
        iPrevBackendID = iBackendID
        iPrevSearchType = iSearchType
    
        'clear address list selection
        With UserControl.cbxAddressType
            .Clear
            .ListIndex = -1
        End With
        
        With UserControl
            'set label showing folder of current contacts
            .lblStatus.Caption = " " & oNode.FullPath
            
            'bind to list box
           .Refresh
            If Not m_oListings Is Nothing Then
                With .lstListings
                    .Array = m_oListings.ToArray
                    .ReBind
       
                    'stop here if there are no contact in set
                    If m_oListings.Count = 0 Then
                        m_oEvents_AfterListingsRetrieved 0
                        UserControl.MousePointer = vbDefault
                        
                        If iSearchType <> ciSearchType_None Then
                            MsgBox "No contacts in '" & oNode.Text & _
                                "' matched the supplied search criteria.", vbExclamation, g_oSessionType.AppTitle
                        End If
                        
                        SetupForInputState True
                        
                        If iSearchType = ciSearchType_Quick Then
                            'return focus to quick search to try again
                            UserControl.tvFolders.SetFocus
                        End If
                        
                        Exit Function
                    End If
                    
                   If .Array.Count(1) > 0 Then
                        On Error Resume Next
                        If .SelBookmarks.Count = 0 Then
                            .Row = 0
                            .SelBookmarks.Add 1
                        End If
                       
                        On Error GoTo ProcError
                   End If
                    
                    DoEvents
                    
                    'show appropriate panel
                    If UserControl.chkGroups.Value Then
                        ShowPanel ciPanel_Groups
                    Else
                        ShowPanel ciPanel_Contacts
                    End If
                End With
                .Refresh
            End If
        End With
        
        'save sort column in user ini
        SetUserIni "CIApplication", "Sort", oFilter.SortColumn
        
        LoadContacts = m_oListings.Count
    End If  'not bCancelled
    
    SetupForInputState True
    UserControl.MousePointer = vbDefault
    RaiseEvent PanelChange(False)
    Exit Function
ProcError:
    Dim lErr As Long
    Dim xDesc As String
    
    lErr = Err.Number
    xDesc = Err.Description
    
    SetupForInputState True
    UserControl.MousePointer = vbDefault
    
    Err.Number = lErr
    Err.Description = xDesc
    g_oError.RaiseError "CIX.CI.LoadContacts"
End Function

Private Sub FixTDBCombo(oCombo As TDBCombo)
    'this is needed to make the
    'bound text stick programmatically
    On Error GoTo ProcError
    With oCombo
        .OpenCombo
        .CloseCombo
    End With
    Exit Sub
ProcError:
    g_oError.RaiseError "CIX.CI.FixTDBCombo"
    Exit Sub
End Sub

Private Sub LoadAddresses()
'loads available addresses for selected listing
    Dim oListing As CIO.CListing
    Dim oAddress As CIO.CAddress
    Dim i As Integer
    Dim xOrigAddress As String
    
    On Error GoTo ProcError
    SetupForInputState False
    UserControl.MousePointer = vbHourglass
    
    With UserControl
'       get existing address selection
        xOrigAddress = .cbxAddressType.Text
        
'       clear address list
        .cbxAddressType.Clear
        
        If .lstListings.SelBookmarks.Count > 1 Then
            'get all address types for the backend
            Set m_oAddresses = CurrentBackend.GetAddresses()
        Else
            'get addresses for listing
            If .lstListings.BoundText = Empty Then
                'no listing is selected
                SetupForInputState True
                Exit Sub
            End If
            
            'a row has been selected - get listing from UNID, which is bound text
            Set oListing = m_oUNID.GetListing(.lstListings.BoundText)
            
            If oListing Is Nothing Then
                'couldn't get listing from bound text - something is wrong
                Err.Raise ciErr_InvalidListing, , "Listing with Display Name '" & _
                    .lstListings.Columns(0) & "' is invalid."
            End If
    
            'get addresses from listing
            Set m_oAddresses = GetBackendFromID(oListing.BackendID) _
                                .GetAddresses(oListing)
        End If
        
        If m_oAddresses Is Nothing Then
'           couldn't get addresses from listing - error
            Err.Raise ciErr_InvalidUNID, , _
                "Invalid address object from listing."
        End If
        
        Dim bMailingAddressIndex As Boolean
        Dim xMailingAddress As String
        
'       add addresses to list
        With .cbxAddressType
            For i = 1 To m_oAddresses.Count
                Set oAddress = m_oAddresses.Item(i)
                If Not (oAddress Is Nothing) Then
                    .AddItem oAddress.Name
                    .Tag = oAddress.UNID
                    If InStr(UCase$(oAddress.Name), "MAILING") > 0 Then
                        xMailingAddress = oAddress.Name
                    End If
                End If
            Next i
        
'           select address
            If .ListCount > 0 Then
                'left in old address selection code, just in case we want to return
                If True Then
                    If xMailingAddress <> "" Then
                        .Text = xMailingAddress
                    Else
                        .ListIndex = 0
                    End If
                Else
                    On Error Resume Next
                    xOrigAddress = Trim$(Replace(xOrigAddress, _
                        "(mailing)", Empty, , , vbTextCompare))
                        
                    .Text = xOrigAddress
                    On Error GoTo ProcError
                    
                    If .ListIndex = -1 Then
                        'original address is not in list-
                        'select first address in list
                        .ListIndex = 0
                    End If
                End If
            End If
        End With
    End With
    
    SetupForInputState True
    UserControl.MousePointer = vbDefault
    Exit Sub
ProcError:
    Dim lErr As Long
    Dim xDesc As String

    lErr = Err.Number
    xDesc = Err.Description
    
    SetupForInputState True
    UserControl.MousePointer = vbDefault
    
    Err.Number = lErr
    Err.Description = xDesc
    g_oError.RaiseError "CIX.CI.LoadAddresses"
End Sub

Private Sub btnAddContactInNative_Click()
    On Error GoTo ProcError
    If Ambient.UserMode = False Then Exit Sub
    UserControl.MousePointer = vbHourglass
    CurrentBackend.AddContact
    UserControl.MousePointer = vbDefault
    Exit Sub
ProcError:
    g_oError.ShowError
    Exit Sub
End Sub

Private Sub btnAddGroup_Click()
    On Error GoTo ProcError
    AddGroup
    Exit Sub
ProcError:
    g_oError.ShowError
    Exit Sub
End Sub

Private Sub AddGroup()
'creates a new contact group
    Dim oForm As frmAddGroup
    Dim oGroups As CIX.CGroups
    
    On Error GoTo ProcError
    Set oForm = New frmAddGroup
    
    With oForm
        'show add group dialog
        .Show vbModal
        
        If Not .Canceled Then
            'add group
            Set oGroups = Me.GroupsBackend
            oGroups.AddGroup .txtGroupName.Text
            
            'refresh groups list
            RefreshGroupsList
            
            'select new group in list
            UserControl.cbxGroups.Text = .txtGroupName.Text
            
            'update tree
            RefreshTreeBranch UserControl.tvFolders.Nodes.Item("b" & Me.GroupsBackend.InternalID)
        End If
    End With
    Unload oForm
    Set oForm = Nothing
    Exit Sub
ProcError:
    Dim lErr As Long
    Dim xDesc As String

    lErr = Err.Number
    xDesc = Err.Description
    
    Unload oForm
    Set oForm = Nothing
    
    Err.Number = lErr
    Err.Description = xDesc
    g_oError.RaiseError "CIX.CI.AddGroup"
    Exit Sub
End Sub

Private Sub DeleteGroup()
    Dim oGroups As CIX.CGroups
    Dim iChoice As VbMsgBoxResult
    
    With UserControl.cbxGroups
        If .ListIndex <> -1 Then
            'item is selected - check to see if this group is active (loaded)
            Set oGroups = Me.GroupsBackend
            If bCurrentGroupIsLoadedFolder() Then
                'don't allow the folder to be deleted
                MsgBox "You can't delete this group because it is currently your active " & _
                    "contacts folder." & vbCr & "Switch to a different folder, " & _
                    "then try deleting this group.", vbExclamation
                Exit Sub
            Else
                'get currently selected tree node
                Dim xSelKey As String
                xSelKey = UserControl.tvFolders.SelectedItem.Key
                
                'prompt to delete it
                iChoice = MsgBox("Delete the group '" & _
                    UserControl.cbxGroups.Text & "'?", vbQuestion + vbYesNo, g_oSessionType.AppTitle)
            End If
            
            If iChoice = vbYes Then
                oGroups.DeleteGroup .ItemData(.ListIndex)
            
                'refresh groups list
                RefreshGroupsList
                
                'clear members
                UserControl.lstGroupMembers.Clear
                
                'update tree
                RefreshTreeBranch UserControl.tvFolders.Nodes _
                    .Item("b" & Me.GroupsBackend.InternalID)
            End If
        End If
    End With
    Exit Sub
ProcError:
    g_oError.RaiseError "CIX.CI.DeleteGroup"
End Sub
Private Sub RefreshGroupsList()
'refresh the Groups combo
    Dim oFolders As CIO.CFolders
    Dim oFolder As CIO.CFolder
    
    On Error GoTo ProcError
    
    'get "groups" folder
    Set oFolders = Me.GroupsBackend.GetFolders(Nothing)
        
    With UserControl.cbxGroups
        .Clear
        For Each oFolder In oFolders
            .AddItem oFolder
            .ItemData(.ListCount - 1) = oFolder.ID
        Next oFolder
    End With
    Exit Sub
ProcError:
    g_oError.RaiseError "CIX.CI.RefreshGroupsList"
End Sub

Private Sub btnAddMember_Click()
    On Error GoTo ProcError
    AddMembers
    Exit Sub
ProcError:
    g_oError.ShowError
    Exit Sub
End Sub

Private Sub AddMembers()
'adds the selected listings to the specified contact group
    Dim oBEnd As CIO.ICIBackend
    Dim oContacts As CIO.CContacts
    Dim oC As CIO.CContact
    Dim oL As CIO.CListing
    Dim oAddr As CIO.CAddress
    Dim lBookmark As Long
    Dim l As Long
    Dim lGroupID As Long
    Dim lID As Long
    Dim oUNID As CUNID
    Dim xUNID As String
    Dim iID As Integer
    Dim oGroups As CIX.CGroups
    
    On Error GoTo ProcError
    
    With UserControl
        With .cbxGroups
            'alert and exit if no group selected
            If .ListIndex = -1 Then
                MsgBox "Please select a group before adding a group member.", _
                    vbExclamation, g_oSessionType.AppTitle
                Exit Sub
            End If
            
            'get group ID
            lGroupID = .ItemData(.ListIndex)
            
            'alert and don't allow if loaded folder is a group -
            'users can't add group members to other groups
            xUNID = UserControl.tvFolders.SelectedItem.Key
            Set oUNID = New CUNID
            
            iID = oUNID.GetUNIDField(xUNID, ciUNIDFields_Backend)
            If iID = Me.GroupsBackend.ID Then
                MsgBox "You can't add members of one group to another group.", vbExclamation
                Exit Sub
            End If
        End With
        
        If .lstListings.SelBookmarks.Count = 0 Then
            MsgBox "Please select a contact before adding a group member.", _
                vbOKOnly + vbExclamation, g_oSessionType.AppTitle
            Exit Sub
        End If
        
        UserControl.MousePointer = vbHourglass
        
        For l = 0 To .lstListings.SelBookmarks.Count - 1
            lBookmark = .lstListings.SelBookmarks(l)
            
'           get selected listing
            Set oL = m_oListings.Item(lBookmark)
            If oL Is Nothing Then
                Err.Raise CIO.ciErr_InvalidListing, , _
                    "Please select a contact before adding a group member."
            End If
            
            If m_oAddresses.Count > 0 Then
'               get selected address
                On Error Resume Next
                
                'exit if still no type is chosen
                If .cbxAddressType.ListIndex = -1 Then
                    Exit Sub
                End If
                
                Set oAddr = m_oAddresses.Item(.cbxAddressType.ListIndex + 1)
                On Error GoTo ProcError
                If oAddr Is Nothing Then
                    Err.Raise CIO.ciErr_invalidAddress, , _
                        "Selected address is invalid."
                End If
            End If
            
'           get contact from listing and address
            Set oContacts = ListingBackend(oL).GetContacts(oL, oAddr, _
                 , ciRetrieveData_Names + ciRetrieveData_Addresses, ciAlert_NoAddresses)
                             
            If Not (oContacts Is Nothing) Then
'               add to contact collection
                For Each oC In oContacts
                    On Error Resume Next
                                    
                    Set oGroups = Me.GroupsBackend
                    lID = oGroups.AddMember(oC, lGroupID)
    
    '               add full name to selected list
                    With UserControl.lstGroupMembers
                        If (oC.AddressTypeName <> Empty) Then
                            .AddItem oC.DisplayName & "  (" & oC.AddressTypeName & ")"
                        Else
                            .AddItem oC.DisplayName & "  (No Address)"
                        End If
                        
                        .ItemData(.ListCount - 1) = lID
                    End With
                    DoEvents
                Next oC
            End If
        Next l
    End With
    
    UserControl.lstListings.SetFocus
    UserControl.MousePointer = vbDefault
        
    Exit Sub
ProcError:
    Dim lErr As Long
    Dim xDesc As String

    lErr = Err.Number
    xDesc = Err.Description
    
    UserControl.MousePointer = vbDefault
    
    Err.Number = lErr
    Err.Description = xDesc
    g_oError.RaiseError "CIX.frmListings.AddSelContacts"
End Sub

Private Sub btnDelete_Click(Index As Integer)
'delete the selected contact in the specified list
    On Error GoTo ProcError
    If Index = 4 Then
        DeleteSelContacts_Custom
    Else
        DeleteSelContacts
    End If
    Exit Sub
ProcError:
    g_oError.ShowError
    Exit Sub
End Sub

Private Sub btnDelete_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)
    On Error GoTo ProcError
    If KeyCode = vbKeyDelete Then
        DeleteSelContacts
    End If
    Exit Sub
ProcError:
    g_oError.ShowError
    Exit Sub
End Sub

Private Sub btnDeleteGroup_Click()
    On Error GoTo ProcError
    DeleteGroup
    Exit Sub
ProcError:
    g_oError.ShowError
    Exit Sub
End Sub

Private Sub btnDeleteMember_Click()
    On Error GoTo ProcError
    DeleteMembers
    Exit Sub
ProcError:
    g_oError.ShowError
    Exit Sub
End Sub

Private Function bCurrentGroupIsLoadedFolder() As Boolean
'returns True if the current group is also the loaded contacts folder
    Dim oUNID As CIO.CUNID
    Dim iBEndID As Integer
    Dim lActiveFolderID As Long
    Dim lCurGroupsFolderID As Long
    Dim xActiveFolderUNID As String
    
    On Error GoTo ProcError
    Set oUNID = New CIO.CUNID
    
    'get UNID of loaded (active folder)
    xActiveFolderUNID = UserControl.tvFolders.SelectedItem.Key
    
    'get backend of active folder
    iBEndID = oUNID.GetUNIDField(xActiveFolderUNID, ciUNIDFields_Backend)
    
    If iBEndID = Me.GroupsBackend.InternalID Then
        'backend is groups backend - test for folder
        lActiveFolderID = oUNID.GetUNIDField(xActiveFolderUNID, ciUNIDFields_Folder)
        
        With UserControl.cbxGroups
            lCurGroupsFolderID = .ItemData(.ListIndex)
        End With
        
        bCurrentGroupIsLoadedFolder = lActiveFolderID = lCurGroupsFolderID
    End If
    Exit Function
ProcError:
    g_oError.RaiseError "CIX.CI.bCurrentGroupIsLoadedFolder"
    Exit Function
End Function
Private Sub DeleteMembers()
    Dim oGroups As CIX.CGroups
    Dim iChoice As VbMsgBoxResult
    Dim iSelCount As Integer
    Dim iSelItem As Integer
    Dim i As Integer
    
    On Error GoTo ProcError
    
    'get number of selected group members
    iSelCount = UserControl.lstGroupMembers.SelCount
    
    If iSelCount = 0 Then
        MsgBox "Please select one or more group members to delete.", vbExclamation
        Exit Sub
    End If
    
'   alert and don't allow if loaded folder is the group
'   from which the user is attempting to delete members
    If bCurrentGroupIsLoadedFolder() Then
        MsgBox "You can't edit this group because it is " & _
            "currently your active contacts folder." & vbCr & "Switch to a different folder, " & _
            "then try editing this group.", vbExclamation
        Exit Sub
    End If

'   prompt to delete
    If iSelCount = 1 Then
        iChoice = MsgBox("Delete '" & UserControl.lstGroupMembers.Text & "' from this group?", vbYesNo + vbExclamation)
    Else
        iChoice = MsgBox("Delete the selected group members from this group?", vbYesNo + vbExclamation)
    End If
    
    If iChoice = vbYes Then
        'do delete
        Set oGroups = Me.GroupsBackend
        
        With UserControl.lstGroupMembers
            For i = .ListCount - 1 To 0 Step -1
                If .Selected(i) Then
                    oGroups.DeleteMember .ItemData(i)
                    
                    'update list
                    .RemoveItem i
                    
                    'mark for later use
                    iSelItem = i
                End If
            Next i
            
            .SetFocus
            
            'reselect an item
            If .ListCount > 0 Then
                If iSelCount > 1 Then
                    .Selected(0) = True
                Else
                    If iSelItem > .ListCount - 1 Then
                        .Selected(.ListCount - 1) = True
                    Else
                        .Selected(iSelItem) = True
                    End If
                End If
            End If
        End With
    End If
    Exit Sub
ProcError:
    g_oError.RaiseError "CIX.CI.DeleteMember"
End Sub

Private Sub btnEdit_Click()
    On Error GoTo ProcError
    If Ambient.UserMode = False Then Exit Sub
    EditContact
    Exit Sub
ProcError:
    g_oError.ShowError
    Exit Sub
End Sub

Private Sub EditContact()
    Dim oB As CIO.ICIBackend
    Dim oL As CIO.CListing
    Dim lBmk As Long
    
    On Error GoTo ProcError
    
    With UserControl.lstListings.SelBookmarks
        If .Count = 0 Then
            MsgBox "Please select a contact from the list " & _
                "of available contacts before running this function.", _
                vbExclamation, g_oSessionType.AppTitle
            Exit Sub
        End If
        
        'get the backend of the contact
        lBmk = .Item(0)
    End With
    
'   get selected listing
    Set oL = m_oListings.Item(lBmk)
    
    Set oB = GetBackendFromID(oL.BackendID)
    
    If oB.SupportsContactEdit() Then
        'run the edit method of the backend
        oB.EditContact oL
    Else
        'alert
        MsgBox "You can't use " & g_oSessionType.AppTitle & _
            " to edit contacts stored in " & oB.Name & ".", _
            vbExclamation, g_oSessionType.AppTitle
    End If
    Exit Sub
ProcError:
    g_oError.RaiseError "CIX.CI.EditContact"
    Exit Sub
End Sub

Private Sub ResetQuickSearch()
    On Error GoTo ProcError
    UserControl.txtFilterValue1.Text = Empty
    CurrentBackend.Filter.Reset
    'UserControl.txtFilterValue2.Text = Empty
    Exit Sub
ProcError:
    g_oError.RaiseError "CIX.CI.ResetQuickSearch"
    Exit Sub
End Sub

Private Sub btnGetMofoContacts_Click()
    Const mpThisFunction As String = "CIX.CI.btnGetMofoContacts_Click"
    
    Static o As Object
    Dim oBackend As Object
    Dim oCol As Collection
    Dim oCIContact As CIO.CContact
    Dim oOutlookContact As Object
    
    On Error Resume Next
    Set o = CreateObject("MofoCI.Contacts")
    On Error GoTo ProcError
    
    If o Is Nothing Then
        Err.Raise 429, , "Could not create Mofo Contacts object 'MoFoCI.Contacts'"
    End If
    
    'Screen.ActiveForm.Hide
    DoEvents
    
    Set oCol = o.Retrieve()
    
    If oCol.Count = 0 Then
        Exit Sub
    Else
        ShowPanel ciPanel_Contacts
    End If
    
    On Error Resume Next
'    Set oBackend = CreateObject("CIMAPI.CCIBackend")

    On Error Resume Next

    If (g_oBackend Is Nothing) Then
        Set g_oBackend = CreateObject("CIOutlookOM.CCIBackend")
        
        If (g_oBackend Is Nothing) Then
            Set g_oBackend = CreateObject("CIMAPI.CCIBackend")
        End If
    End If
        
    On Error GoTo ProcError
    
    Dim iDataRetrieved As Integer
    
    Select Case Me.SelectionList
        Case CIX.ciSelectionList_To
            iDataRetrieved = Me.DataRetrievedForTo
        Case CIX.ciSelectionList_From
            iDataRetrieved = Me.DataRetrievedForFrom
        Case CIX.ciSelectionList_CC
            iDataRetrieved = Me.DataRetrievedForCC
        Case CIX.ciSelectionList_BCC
            iDataRetrieved = Me.DataRetrievedForBCC
    End Select
    
    
    For Each oOutlookContact In oCol
'        Set oCIContact = oBackend.GetCIContactFromOutlookContact( _
'            oOutlookContact, iDataRetrieved)
        Set oCIContact = g_oBackend.GetCIContactFromOutlookContact( _
            oOutlookContact, iDataRetrieved)


'       set selection list for contact
        oCIContact.ContactType = Me.SelectionList
                    
'       find the last contact in collection that has the
'       same contact type - if none, add as first of group
        Dim iNumTo As Integer
        Dim iNumFrom As Integer
        Dim iNumCC As Integer
        Dim iNumBCC As Integer
        Dim lInsertAt As Long
        Dim iID As Integer
        
        'get number of contacts in each group
        With UserControl.lstContacts
            iNumTo = .Item(0).ListCount
            iNumFrom = .Item(1).ListCount
            iNumCC = .Item(2).ListCount
            iNumBCC = .Item(3).ListCount
        End With
        
        Select Case oCIContact.ContactType
            Case CIX.ciSelectionList_To
                'insert after last 'To' contact
                lInsertAt = iNumTo + 1
            Case CIX.ciSelectionList_From
                lInsertAt = iNumTo + iNumFrom + 1
            Case CIX.ciSelectionList_CC
                lInsertAt = iNumTo + iNumFrom + iNumCC + 1
            Case CIX.ciSelectionList_BCC
                lInsertAt = iNumTo + iNumFrom + iNumCC + iNumBCC + 1
        End Select
            
        On Error Resume Next
        If lInsertAt <= m_oContacts.Count Then
            m_oContacts.Add oCIContact, lInsertAt
        Else
            m_oContacts.Add oCIContact
        End If
        
        If Err.Number = 0 Then
            'get next number in the sequence
            iID = iID + 1
            
            'tag the contact with this unique number
            oCIContact.Tag = iID
            
'                   add full name to selected list
            With SelectionListControl
                .AddItem oCIContact.DisplayName
                .ItemData(.ListCount - 1) = iID
            End With
        ElseIf Err.Number = 457 Then
            MsgBox oCIContact.FullName & " has already been added.", vbExclamation, g_oSessionType.AppTitle
        Else
            Err.Raise Err.Number, , Err.Description
        End If
    Next
    'RaiseEvent InsertionOK(m_oContacts)
    Exit Sub
ProcError:
    g_oError.RaiseError mpThisFunction
    Exit Sub
End Sub

Private Sub btnHelp_Click()
    On Error GoTo ProcError
    If Ambient.UserMode = False Then Exit Sub
    If Not (UserControl.mnuHelp.Visible) Then
        If g_oSessionType.SessionType = ciSession_Connect Then
'            mnuHelp_PDF.Caption = "&Connect Forms Help..."
            mnuHelp_About.Caption = "&About " & g_oSessionType.AppTitle
        End If
        ShowHelpMenu
    End If
    Exit Sub
ProcError:
    g_oError.RaiseError "CIX.CI.btnOptions_Click"
    Exit Sub
End Sub

Private Sub btnMoveDown_Click()
    MoveSelContactDown
End Sub

Private Sub btnMoveUp_Click()
    MoveSelContactUp
End Sub

Private Sub btnOptions_Click()
    On Error GoTo ProcError
    If Ambient.UserMode = False Then Exit Sub
    If Not (mnuFolders.Visible Or mnuContacts.Visible) Then
        ShowMenu
    End If
    Exit Sub
ProcError:
    g_oError.RaiseError "CIX.CI.btnOptions_Click"
    Exit Sub
End Sub

Private Sub ShowMenu()
    Dim sMnuTop As Single
    Dim sMnuLeft As Single
    On Error GoTo ProcError
    With UserControl
        sMnuTop = .btnOptions.Top - 100 + .btnOptions.Height
        sMnuLeft = .btnOptions.Left + .btnOptions.Width
        
        If UserControl.fraFolders.Visible Then
            If Not (.tvFolders.SelectedItem Is Nothing) Then
                .PopupMenu mnuFolders, vbPopupMenuRightAlign, sMnuLeft, sMnuTop
            End If
        Else
            .PopupMenu mnuContacts, vbPopupMenuRightAlign, sMnuLeft, sMnuTop
        End If
    End With
    Exit Sub
ProcError:
    g_oError.RaiseError "CIX.CI.ShowMenu"
    Exit Sub
End Sub

Private Sub ShowHelpMenu()
    Dim sMnuTop As Single
    Dim sMnuLeft As Single
    Dim xPath As String
    Dim i As Integer
    Dim oFolder As Scripting.Folder
    Dim oFile As Scripting.File
    Dim iCount As Integer
    
    On Error GoTo ProcError
    With UserControl
    
    '   setup Help menu
        Set oFSO = New FileSystemObject
        If oFSO.FolderExists(g_xHelpPath) Then
            Set oFolder = oFSO.GetFolder(g_xHelpPath)
            iCount = oFolder.Files.Count
            If iCount > 0 Then
                ReDim g_xHelpFiles(iCount - 1)
                For Each oFile In oFolder.Files
                    g_xHelpFiles(i) = Mid$(oFile.Name, 1, InStr(oFile.Name, ".") - 1)
                    i = i + 1
                Next oFile
            End If
        End If
        
        
        'populate help menu
        If iCount > 0 Then
            For i = 0 To UBound(g_xHelpFiles)
                If i > 0 Then
                    Load mnuHelp_PDF(i)
                End If
                .mnuHelp_PDF(i).Caption = Replace(g_xHelpFiles(i), "_MacPac Contact Integration", "")
                .mnuHelp_PDF(i).Visible = True
            Next i
        End If

        sMnuTop = .btnHelp.Top - 100 + .btnHelp.Height
        sMnuLeft = .btnHelp.Left + .btnHelp.Width

        .PopupMenu mnuHelp, vbPopupMenuRightAlign, sMnuLeft, sMnuTop
    End With
    Exit Sub
ProcError:
    g_oError.RaiseError "CIX.CI.ShowHelpMenu"
    Exit Sub
End Sub

Private Sub btnQuickSearch_Click()
    On Error GoTo ProcError
    If Ambient.UserMode = False Then Exit Sub
    LoadContacts ciSearchType_Quick
    ResetQuickSearch
    Exit Sub
ProcError:
    g_oError.ShowError
    Exit Sub
End Sub

Private Sub btnRefresh_Click()
    On Error GoTo ProcError
    Refresh
    Exit Sub
ProcError:
    g_oError.ShowError
    Exit Sub
End Sub

Private Sub Refresh()
'refreshes either the folder tree branch
'or contacts depending on visible panel
    Dim oNode As MSComctlLib.node
    
    On Error GoTo ProcError
    If UserControl.fraFolders.Visible Then
        'refresh the selected folder tree branch
        Set oNode = UserControl.tvFolders.SelectedItem
        
        If Not oNode Is Nothing Then
            RefreshTreeBranch oNode
        End If
    Else
        'refresh contacts
        LoadContacts ciSearchType_Refresh
    End If

    Exit Sub
ProcError:
    g_oError.RaiseError "CIX.CI.Refresh"
    Exit Sub
End Sub

Private Sub btnSearch_Click()
    On Error GoTo ProcError
    LoadContacts ciSearchType_Dialog
    Exit Sub
ProcError:
    g_oError.ShowError
End Sub

Private Sub btnSearchNative_Click()
    On Error GoTo ProcError
    If Ambient.UserMode = False Then Exit Sub
    
    If CurrentBackend().SupportsNativeSearch Then
        LoadContacts ciSearchType_Native
        SetupForInputState True
    End If
    Exit Sub
ProcError:
    g_oError.ShowError
End Sub

Private Sub btnTogglePanels_Click()
    On Error GoTo ProcError
    If Ambient.UserMode = False Then Exit Sub
    If UserControl.fraFolders.Visible Then
        LoadContacts
    Else
        ShowPanel ciPanel_folders
    End If
    Exit Sub
ProcError:
    g_oError.ShowError
End Sub

Private Sub cbxGroups_Click()
    If Ambient.UserMode = False Then Exit Sub
    With UserControl
        .btnDeleteGroup.Enabled = .cbxGroups.ListIndex <> -1
        .btnAddMember.Default = True
        LoadGroupMembers
        .cmdOK.Enabled = True
    End With
End Sub

Private Sub LoadGroupMembers()
'loads the group members of the selected
'group into the group members list

    Dim oListings As CIO.CListings
    Dim oFolder As CIO.CFolder
    Dim oUNID As CIO.CUNID
    Dim lID As Long
    Dim oGroups As CIX.CGroups
    Dim l As Long
    
    With UserControl.cbxGroups
        If .ListIndex = -1 Then
            Exit Sub
        End If
        
        'get id of selected group
        lID = .ItemData(.ListIndex)
    End With
    
    UserControl.lstGroupMembers.Clear
    
    Set oUNID = New CIO.CUNID
    Set oGroups = Me.GroupsBackend
    With oGroups
        Set oListings = .GetMembers(lID)
        
        If oListings.Count Then
            For l = 1 To oListings.Count
                With UserControl.lstGroupMembers
                    .AddItem oListings.Item(l).DisplayName
                    .ItemData(.ListCount - 1) = oListings.Item(l).ID
                End With
            Next l
        End If
    End With
    Exit Sub
ProcError:
    g_oError.RaiseError "CIX.CI.LoadGroupMembers"
End Sub

Private Sub chkGroups_Click()
    On Error GoTo ProcError
    If Ambient.UserMode = False Then Exit Sub
    If UserControl.chkGroups.Value = vbChecked Then
        'refresh groups list
        RefreshGroupsList
        UserControl.lstGroupMembers.Clear
        ShowPanel ciPanel_Groups
    Else
        ShowPanel ciPanel_Contacts
    End If
    Exit Sub
ProcError:
    g_oError.ShowError
End Sub

Private Sub cmbFilterFld1_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch UserControl.cmbFilterFld1, Reposition
    Exit Sub
ProcError:
    g_oError.ShowError
    Exit Sub
End Sub

Private Sub cmbOp1_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch UserControl.cmbOp1, Reposition
    Exit Sub
ProcError:
    g_oError.ShowError
    Exit Sub
End Sub

Private Sub cmbOp1_Validate(Cancel As Boolean)
    On Error GoTo ProcError
    Cancel = Not bValidateBoundTDBCombo(UserControl.cmbOp1)
    Exit Sub
ProcError:
    g_oError.ShowError
    Exit Sub
End Sub

Private Sub cmbFilterFld1_Validate(Cancel As Boolean)
    On Error GoTo ProcError
    Cancel = Not bValidateBoundTDBCombo(UserControl.cmbFilterFld1)
    Exit Sub
ProcError:
    g_oError.ShowError
    Exit Sub
End Sub

Private Sub cmdCancel_Click()
    On Error Resume Next
    SaveColumns
    tvFolders.SetFocus
    RaiseEvent InsertionCancelled
    On Error GoTo 0
End Sub

Private Sub cmdCancel_KeyPress(KeyAscii As Integer)
    If KeyAscii = 9 Then    'Tab
        If m_bShowedContacts Then
            On Error Resume Next
            tvFolders.SetFocus
            On Error GoTo 0
        End If
    End If
End Sub

Private Sub cmdCancel_LostFocus()
    If Not m_bShowedContacts Then
        On Error Resume Next
        tvFolders.SetFocus
        On Error GoTo 0
    End If
End Sub

Private Sub cmdOK_Click()
    If UserControl.chkGroups.Value = vbUnchecked Then
        'not in groups mode - this button should insert contacts
        SaveColumns
        RaiseEvent InsertionOK(m_oContacts)
    Else
        'in groups mode - this button should load the current group
        LoadGroup
    End If
End Sub

Private Sub LoadGroup()
    Dim xGroup As String
    Dim lNum As Long
    
    On Error GoTo ProcError
    'get current group
    xGroup = UserControl.cbxGroups.Text
    
    If xGroup <> Empty Then
        'load the group by selecting node, then loading contacts
        SelectNode Me.GroupsBackend.Name & "\" & xGroup
        
        'load contacts
        lNum = LoadContacts()
        
        If lNum > 0 Then
            'exit group mode
            UserControl.chkGroups.Value = vbUnchecked
        End If
    Else
        MsgBox "Please select a group to load.", vbExclamation
    End If
    Exit Sub
ProcError:
    g_oError.RaiseError "CIX.CI.LoadGroup"
    Exit Sub
End Sub


Private Sub grdEntityList_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    With UserControl.grdEntityList
        If Button = 2 And m_oContacts.Count Then
            ShowContactDetail .Bookmark + 1
        ElseIf m_oContacts.Count = 0 Then
            Exit Sub
        Else
            On Error Resume Next
            UserControl.btnDelete.Item(4).Enabled = Me.EntityList(Int(Y / 227) - 1, 2) <> ""
        End If
    End With
End Sub

Private Sub lstContacts_Click(Index As Integer)
    With UserControl.lstContacts(Index)
        UserControl.btnDelete(Index).Enabled = .ListCount > 0 And .ListIndex > -1
    End With
End Sub

Private Sub lstContacts_GotFocus(Index As Integer)
    On Error GoTo ProcError
    
    'mark that this contact list is the current list
    m_iCurList = Index
    
    'enable irrelevant menu items
    UserControl.mnuContacts_MoveDown.Enabled = True
    UserControl.mnuContacts_MoveUp.Enabled = True
    
    'set the selection list to correspond to
    'the list box that is being clicked on
    SelectionList = SelListFromControlIndex(Index)

    ClearListingsSelections
    Exit Sub
ProcError:
    g_oError.ShowError
    Exit Sub
End Sub

Private Sub ClearListingsSelections()
    Dim i As Integer
    
    On Error GoTo ProcError
    With UserControl.lstListings.SelBookmarks
        For i = .Count - 1 To 0 Step -1
            .Remove i
        Next i
    End With
    
    'UserControl.lstListings.SelectedItem = Null
    Exit Sub
ProcError:
    g_oError.RaiseError "CIX.CI.ClearListingsSelections"
    Exit Sub
End Sub

Private Function SelListFromControlIndex(ByVal iIndex As Integer) As Integer
    'selection list is 2 exponent lstContacts index
    SelListFromControlIndex = 2 ^ iIndex
End Function

Private Sub MoveSelContactDown()
    Dim oLB As VB.ListBox
    Dim iIndex As Integer
    Dim xDisplayText As String
    Dim xItemData As Long
    Dim i As Integer
    
    On Error GoTo ProcError
    
    'get list box
    Set oLB = SelectionListControl()
    
    'exit if the last item in the list is selected
    If oLB.SelCount = 0 Then
        Exit Sub
    ElseIf oLB.Selected(oLB.ListCount - 1) = True Then
        Exit Sub
    End If
    
    With UserControl
        .btnMoveDown.Enabled = False
        .btnMoveUp.Enabled = False
    End With
    
    'for each selected index, move down
    For i = oLB.ListCount - 2 To 0 Step -1
        If oLB.Selected(i) Then
            'get selected index data
            xDisplayText = oLB.List(i)
            xItemData = oLB.ItemData(i)
            
            oLB.Selected(i) = False
            
            'delete selected index
            oLB.RemoveItem i
            
            'add new item at index one below
            oLB.AddItem xDisplayText, i + 1
            oLB.ItemData(i + 1) = xItemData
            DoEvents
            oLB.Selected(i + 1) = True
            
            'get current index of contact in collection
            With UserControl.lstContacts
                Select Case oLB.Index
                    Case 0
                        iIndex = i + 1
                    Case 1
                        iIndex = .Item(0).ListCount + i + 1
                    Case 2
                        iIndex = .Item(0).ListCount + _
                            .Item(1).ListCount + i + 1
                    Case 3
                        iIndex = .Item(0).ListCount + _
                            .Item(1).ListCount + .Item(2).ListCount + i + 1
                End Select
            End With
            
            Dim oContact As CIO.CContact
            
            'move contact one down in collection
            Set oContact = m_oContacts.Item(iIndex)
            
            'delete contact from collection
            m_oContacts.Delete iIndex
            
            'add contact at new position
            If iIndex + 1 > m_oContacts.Count Then
                'contact needs to be added to the end
                m_oContacts.Add oContact
            Else
                m_oContacts.Add oContact, iIndex + 1
            End If
        End If
    Next i
    
    With UserControl
        .btnMoveDown.Enabled = True
        .btnMoveUp.Enabled = True
    End With
    
    'give list focus again
    oLB.SetFocus
    
    Exit Sub
ProcError:
    Dim lErr As Long
    Dim xDesc As String

    lErr = Err.Number
    xDesc = Err.Description
    
    With UserControl
        .btnMoveDown.Enabled = True
        .btnMoveUp.Enabled = True
    End With
    
    Err.Number = lErr
    Err.Description = xDesc

    g_oError.RaiseError "CIX.CI.MoveSelContactDown"
    Exit Sub
End Sub

Private Sub MoveSelContactUp()
    Dim oLB As VB.ListBox
    Dim iIndex As Integer
    Dim xDisplayText As String
    Dim xItemData As Long
    Dim i As Integer
    
    On Error GoTo ProcError
    'get list box
    Set oLB = SelectionListControl()
    
    'exit if the first item in the list is selected
    If oLB.SelCount = 0 Then
        Exit Sub
    ElseIf oLB.Selected(0) = True Then
        Exit Sub
    End If
    
    With UserControl
        .btnMoveDown.Enabled = False
        .btnMoveUp.Enabled = False
    End With
    
    'for each selected index, move down
    For i = 1 To oLB.ListCount - 1
        If oLB.Selected(i) Then
            'get selected index data
            xDisplayText = oLB.List(i)
            xItemData = oLB.ItemData(i)
            
            oLB.Selected(i) = False
            
            'delete selected index
            oLB.RemoveItem i
            
            'add new item at index one below
            oLB.AddItem xDisplayText, i - 1
            oLB.ItemData(i - 1) = xItemData
            oLB.Selected(i - 1) = True
            
            'get current index of contact in collection
            With UserControl.lstContacts
                Select Case oLB.Index
                    Case 0
                        iIndex = i + 1
                    Case 1
                        iIndex = .Item(0).ListCount + i + 1
                    Case 2
                        iIndex = .Item(0).ListCount + _
                            .Item(1).ListCount + i + 1
                    Case 3
                        iIndex = .Item(0).ListCount + _
                            .Item(1).ListCount + .Item(2).ListCount + i + 1
                End Select
            End With
            
            Dim oContact As CIO.CContact
            
            'move contact one up in collection
            Set oContact = m_oContacts.Item(iIndex)
            m_oContacts.Delete (iIndex)
            m_oContacts.Add oContact, iIndex - 1
        End If
    Next i
    
    With UserControl
        .btnMoveDown.Enabled = True
        .btnMoveUp.Enabled = True
    End With
    
    'give list focus again
    oLB.SetFocus
    
    Exit Sub
ProcError:
    Dim lErr As Long
    Dim xDesc As String

    lErr = Err.Number
    xDesc = Err.Description
    
    With UserControl
        .btnMoveDown.Enabled = True
        .btnMoveUp.Enabled = True
    End With
    
    Err.Number = lErr
    Err.Description = xDesc
    g_oError.RaiseError "CIX.CI.MoveSelContactUp"
    Exit Sub
End Sub

Private Sub lstContacts_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)
    
    On Error GoTo ProcError
    
    If IsPressed(KEY_CTL) Then
        If KeyCode = 65 Then
            DoEvents
            SelectAllContacts
            KeyCode = 0
        ElseIf KeyCode = vbKeyDown Then
            MoveSelContactDown
            KeyCode = 0
        ElseIf KeyCode = vbKeyUp Then
            MoveSelContactUp
            KeyCode = 0
        End If
    Else
        If KeyCode = vbKeyDelete Then
            DeleteSelContacts
            KeyCode = 0
        ElseIf Not (KeyCode = vbKeyDown Or KeyCode = vbKeyUp) Then
            KeyCode = 0
        End If
    End If
    
    Exit Sub
ProcError:
    g_oError.ShowError
    Exit Sub
End Sub

Private Sub SelectAllContacts()
    Dim l As Long
    
    On Error GoTo ProcError
    
    'select all
    With UserControl.lstContacts.Item(m_iCurList)
        For l = 0 To .ListCount - 1
            .Selected(l) = True
        Next l
    End With

    Exit Sub
ProcError:
    g_oError.RaiseError "CIX.CI.SelectAllContacts"
    Exit Sub
End Sub

Private Sub SelectAllGroupMembers()
    Dim l As Long
    
    On Error GoTo ProcError
    
    'select all
    With UserControl.lstGroupMembers
        For l = 0 To .ListCount - 1
            .Selected(l) = True
        Next l
    End With

    Exit Sub
ProcError:
    g_oError.RaiseError "CIX.CI.SelectAllGroupMembers"
    Exit Sub
End Sub

Private Sub lstContacts_MouseDown(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    Dim sCtlTop As Single
    Dim iNewIndex As Integer
    Dim i As Integer
    
    On Error GoTo ProcError
    m_bMouseDown = True

    With UserControl.lstContacts.Item(Index)
        If .ListCount = 0 Then
            Exit Sub
        Else
            .SetFocus
            If Button = 2 Then
                'delete previous selections - select item - show detail
                sCtlTop = .Top
                iNewIndex = Int(Y / 220) + .TopIndex
                For i = 0 To .ListCount - 1
                    .Selected(i) = False
                Next i
                
                If iNewIndex > .ListCount - 1 Then
                    iNewIndex = .ListCount - 1
                End If
                
                .Selected(iNewIndex) = True
                DoEvents
                
                If .ListIndex > -1 Then
                    ShowContactDetail .ItemData(iNewIndex)
                End If
            End If
        End If
    End With
    Exit Sub
ProcError:
    g_oError.ShowError
    Exit Sub
End Sub

Private Sub lstContacts_MouseUp(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    m_bMouseDown = False
End Sub

Private Sub ShowContactDetail(ByVal iUniqueTagNumber As Integer)
'shows the detail for the contact having the specified tag number-
'number is stored in tag property and is issued when the
'contact is added to right side list
    Dim oForm As frmContactDetail
    Dim oContact As CIO.CContact
    Dim i As Integer
    
    If m_oContacts.Count = 0 Then
        Exit Sub
    End If
    
    Set oForm = New frmContactDetail
    
    
    With oForm
        'assign contacts to form
        Set .Contacts = m_oContacts
        
        On Error Resume Next
        
        If iUniqueTagNumber = 0 Then
            'no contact is selected in the list - select the first one
            'to display in the detail dialog
            'Set .SelectedContact = .Contacts.Item(1)
            .SelectedContactIndex = 1
        Else
            'select the right contact -
            'cycle through all until the one
            'with the specified tag is found
            For i = 1 To .Contacts.Count
                If .Contacts.Item(i).Tag = iUniqueTagNumber Then
                    'this is the one
                    'Set .SelectedContact = .Contacts.Item(i)
                    .SelectedContactIndex = i
                    Exit For
                End If
            Next i
        End If
        
        'show contact detail form
        .Show vbModal, Screen.ActiveForm
        Unload oForm
        Set oForm = Nothing
    End With
End Sub

Private Sub lstGroupMembers_GotFocus()
    On Error GoTo ProcError
    
    m_iCurList = ciListBoxes_GroupMembers
    
    'disable irrelevant menu items
    With UserControl
        .mnuContacts_MoveDown.Enabled = False
        .mnuContacts_MoveUp.Enabled = False
    
        ClearListingsSelections
        .btnDeleteMember.Enabled = (.lstGroupMembers.ListIndex > -1) _
        And (.lstGroupMembers.ListCount > 0)
    End With
    Exit Sub
ProcError:
    g_oError.ShowError
    Exit Sub
End Sub

Private Sub lstGroupMembers_KeyDown(KeyCode As Integer, Shift As Integer)
    On Error GoTo ProcError
    
    If IsPressed(KEY_CTL) Then
        If KeyCode = 65 Then
            DoEvents
            SelectAllGroupMembers
        End If
    Else
        If KeyCode = vbKeyDelete Then
            DeleteSelContacts
        End If
    End If

    Exit Sub
ProcError:
    g_oError.ShowError
    Exit Sub
End Sub

Private Sub lstGroupMembers_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Dim sCtlTop As Single
    Dim iNewIndex As Integer
    Dim i As Integer

    On Error GoTo ProcError
    m_bMouseDown = True

    With UserControl.lstGroupMembers
        If .ListCount = 0 Then
            Exit Sub
        Else
            .SetFocus
            If Button = 2 Then
                'delete previous selections - select item - show detail
                sCtlTop = .Top
                iNewIndex = Int(Y / 220) + .TopIndex
                For i = 0 To .ListCount - 1
                    .Selected(i) = False
                Next i

                If iNewIndex > .ListCount - 1 Then
                    iNewIndex = .ListCount - 1
                End If

                .Selected(iNewIndex) = True
                DoEvents

                If .ListIndex > -1 Then
                    ShowMemberDetail .ItemData(.ListIndex)
                End If
            End If
        End If
    End With
    Exit Sub
ProcError:
    g_oError.ShowError
    Exit Sub
End Sub

Private Sub ShowMemberDetail(ByVal lID As Long)
'shows the detail form for the first selected listing
    Dim oForm As frmListingDetail
    Dim lRow As Long
    Dim oUNID As CUNID
    Dim xUNID As String
    
    On Error GoTo ProcError
    
    UserControl.MousePointer = vbHourglass
    
    'get member UNID
    Set oUNID = New CUNID
    xUNID = Backends.GroupsBackend.InternalID & g_oConstants.UNIDSep & g_oConstants.UNIDSep & g_oConstants.UNIDSep & lID
    
    'show detail form
    Set oForm = New frmListingDetail
    With oForm
        Set .Listing = oUNID.GetListing(xUNID)
        Set .Addresses = Backends.GroupsBackend.GetAddresses(.Listing)
        
        If .Addresses.Count > 0 Then
'            .AddressIndex = 0
        End If
        .Show vbModal, Screen.ActiveForm
        UserControl.MousePointer = vbDefault
    End With
        
    Unload oForm
    UserControl.lstGroupMembers.SetFocus
    Set oForm = Nothing
    Exit Sub
ProcError:
    g_oError.RaiseError "CIX.CI.ShowListingDetail"
    Exit Sub
End Sub

'GLOG : 4719 : ceh
Private Sub ShowListingStatus(oListing As CIO.CListing)
'displays listing type (Private or Public) for MP9 & MP10 only
    Dim oL As CIO.CListing
    Dim oContact As CIO.CContact
    Dim xManualEntryIDThreshold As String
    Dim lThreshold As Long
    
    On Error GoTo ProcError
    
    
    Exit Sub
ProcError:
    g_oError.RaiseError "CIX.frmListingDetail.ShowListingStatus"
    Exit Sub
End Sub


Private Sub lstListings_ColMove(ByVal Position As Integer, Cancel As Integer)
    On Error GoTo ProcError
    UserControl.lstListings.ClearSelCols
    Exit Sub
ProcError:
    g_oError.ShowError
End Sub

Private Sub lstListings_DblClick()
    On Error GoTo ProcError
    If lstListings.Row > -1 Then
        OnListingsDblClick
    End If
    Exit Sub
ProcError:
    g_oError.ShowError
End Sub

Private Sub OnListingsDblClick()
    On Error GoTo ProcError
    If UserControl.chkGroups.Value = 0 Then
        If UserControl.btnAdd.Item(4).Visible Then
            UserControl.btnAdd.Item(4).Default = True
        End If
        AddSelContacts
    Else
        AddMembers
    End If
    Exit Sub
ProcError:
    g_oError.RaiseError "CIX.CI.OnListingsDblClick"
    Exit Sub
End Sub

Private Sub lstListings_GotFocus()
    Dim i As Integer
    Dim j As Integer
    
    On Error GoTo ProcError
    
    'mark that this is the current list
    m_iCurList = ciListBoxes_Listings
    
    'disable irrelevant menu items
    UserControl.mnuContacts_MoveDown.Enabled = False
    UserControl.mnuContacts_MoveUp.Enabled = False
    
    'get the contact list that is active
    With UserControl
        For i = 0 To 3
            If .lblSelContacts(i).ForeColor = vbBlue Then
                'this is the active list - remove
                'selection, disable delete button
                With .lstContacts(i)
                    For j = 0 To .ListCount - 1
                        .Selected(j) = False
                    Next
                End With
                .btnDelete(i).Enabled = False
                Exit For
            End If
        Next i
        
        'remove group members selections, disable delete button
        With .lstGroupMembers
            For j = 0 To .ListCount - 1
                .Selected(j) = False
            Next
        End With
        .btnDeleteMember.Enabled = False
    End With
    Exit Sub
ProcError:
    g_oError.ShowError
    Exit Sub
End Sub

Private Sub lstListings_HeadClick(ByVal ColIndex As Integer)
    On Error GoTo ProcError
    'reserve CTL+HeadClick for column selection
    If Not IsPressed(KEY_CTL) Then
        Sort ColIndex
    End If
    Exit Sub
ProcError:
    g_oError.ShowError
    Exit Sub
End Sub

Private Sub Sort(ByVal ColIndex As Integer)
    On Error GoTo ProcError
    With UserControl.lstListings
        'exit if there aren't any listings
        If .Array.Count(1) = 0 Then
            Exit Sub
        End If
        
        'get first bookmark of selection
        Dim xBoundText As String
        xBoundText = .BoundText
        
        .ClearSelCols
        DoEvents
        
        'do sort
        m_oListings.Sort ColIndex
        
        'refresh list
        .Refresh
        
        'set this column as the default sort column for this backend
        CurrentBackend.DefaultSortColumn = ColIndex
        
        .ListField = .Columns(ColIndex).DataField
        
        'reselect previous - select
        'first row if no previous selection
        If xBoundText = Empty Then
            .SelBookmarks.Add 0
        Else
            .BoundText = xBoundText
        End If
    End With
    Exit Sub
ProcError:
    g_oError.RaiseError "CIX.CI.Sort"
    Exit Sub
End Sub

Private Sub lstListings_KeyDown(KeyCode As Integer, Shift As Integer)
    Dim l As Long
    If IsPressed(KEY_CTL) Then
        If KeyCode = 65 Then
            SelectAllListings
        End If
    Else
        Select Case KeyCode
            Case vbKeyHome
                KeyCode = 0
                ClearListingSelections
                With UserControl.lstListings
                    .FirstRow = 1
                    .Row = 0
                    .SelBookmarks.Add 0
                End With
            Case vbKeyEnd
                KeyCode = 0
                ClearListingSelections
                With UserControl.lstListings
                    .FirstRow = m_oListings.Count - 1
                    .Row = .VisibleRows - 1
                    .SelBookmarks.Add .RowBookmark(.Row)
                End With
            Case vbKeyLeft, vbKeyRight
                KeyCode = 0
        End Select
    End If
End Sub

Private Sub SelectAllListings()
    Dim l As Long
    
    On Error GoTo ProcError
    DoEvents
    
    UserControl.MousePointer = vbHourglass
    
    With UserControl.lstListings
        For l = 1 To .Array.Count(1)
            .SelBookmarks.Add l
        Next l
        If .Row = -1 Then
            .Row = 0
        End If
    End With
    
    OnListingsRowChange
    UserControl.MousePointer = vbDefault
    Exit Sub
ProcError:
    Dim lErr As Long
    Dim xDesc As String

    lErr = Err.Number
    xDesc = Err.Description
    
    UserControl.MousePointer = vbDefault
    
    Err.Number = lErr
    Err.Description = xDesc
    g_oError.RaiseError "CIX.CI.SelectAllListings"
    Exit Sub
End Sub

Private Sub ClearListingSelections()
    Dim i As Integer
    
'   remove listing selections
    With UserControl.lstListings.SelBookmarks
        For i = .Count - 1 To 0 Step -1
            .Remove i
        Next
    End With
    'UserControl.lstListings.Row = Null
End Sub

Private Sub ClearListingSelectionsButLast()
    Dim i As Integer
    
'   remove listing selections
    With UserControl.lstListings.SelBookmarks
        If .Count > 1 Then
            .Add .Item(.Count - 1)
            For i = .Count - 2 To 1 Step -1
                .Remove i
            Next
        End If
    End With
End Sub

Private Sub ShowListingDetail()
'shows the detail form for the first selected listing
    Dim oForm As frmListingDetail
    Dim lRow As Long
    
    On Error GoTo ProcError
    
    If m_oListings.Count = 0 Then
        Exit Sub
    ElseIf UserControl.lstListings.Bookmark = 0 Then
        Exit Sub
    End If
    
    UserControl.MousePointer = vbHourglass
    
    'show detail form
    Set oForm = New frmListingDetail
    With oForm
        Set .Listing = m_oListings.Item( _
            UserControl.lstListings.Bookmark)
            
        If UserControl.lstListings.SelBookmarks.Count = 1 Then
            'only one contact is selected - we can use the
            'addresses in the address list - if there are multiple
            'selections, we can't use these - the form will get
            'the addresses from the supplied listing
            Set .Addresses = m_oAddresses
        End If
        
        If UserControl.cbxAddressType.ListCount > 0 Then
            '.AddressIndex = UserControl.cbxAddressType.ListIndex
            .AddressName = UserControl.cbxAddressType.Text
        End If
        .Show vbModal, Screen.ActiveForm
        UserControl.MousePointer = vbDefault
    
        'select the address that was selected
        'in the listing detail form
        Dim iIndex As Integer
    End With
        
    If Not oForm.Canceled Then
        On Error Resume Next
        'iIndex = oForm.AddressIndex
        
        On Error Resume Next
        UserControl.cbxAddressType.Text = Mid$(oForm.AddressName, 2)
        On Error GoTo ProcError
        Unload oForm
    End If
    
    UserControl.lstListings.SetFocus
    Set oForm = Nothing
    Exit Sub
ProcError:
    g_oError.RaiseError "CIX.CI.ShowListingDetail"
    Exit Sub
End Sub

Private Sub lstListings_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Dim oForm As frmListingDetail
    Dim lRow As Long
    Dim lNumSel As Long
    
    On Error GoTo ProcError
    
    If Button = 2 Then
        'user right-clicked on listing -
        
        'get original number of selections -
        'we'll need this to force a row change-
        'this will give us the correct addresses
        'for the selected listing
        lNumSel = UserControl.lstListings.SelBookmarks.Count
        
        With UserControl.lstListings
            'select listing
            lRow = .RowContaining(Y) + .FirstRow
            If lRow > 0 Then
                'clear existing listings
                ClearSelectedListings
                .SelBookmarks.Add lRow
                .SelectedItem = lRow
                If lNumSel > 1 Then
                    OnListingsRowChange
                End If
        
                DoEvents
                ShowListingDetail
            End If
        End With
    Else
        On Error Resume Next
        lRow = UserControl.lstListings.RowContaining(Y) + UserControl.lstListings.FirstRow
        On Error GoTo ProcError
        If lRow = 0 Then
            'UserControl.lstListings.
        End If
    End If
    Exit Sub
ProcError:
    Dim lErr As Long
    Dim xDesc As String
    Dim xSource As String
    
    lErr = Err.Number
    xDesc = Err.Description
    xSource = Err.Source
    
    UserControl.MousePointer = vbDefault
    
    
    Err.Number = lErr
    Err.Description = xDesc
    Err.Source = xSource
    g_oError.ShowError
    Exit Sub
End Sub

Private Sub ClearSelectedListings()
'clears the selected listings
    Dim i As Integer
    On Error GoTo ProcError
    With UserControl.lstListings.SelBookmarks
        For i = .Count - 1 To 0 Step -1
            .Remove i
        Next i
    End With
    Exit Sub
ProcError:
    g_oError.RaiseError "CIX.CI.ClearSelectedListings"
    Exit Sub
End Sub

Private Sub OnListingsRowChange()
'executes all necessary functionality
'when a listing is selected
    Dim xTempPath As String
    Dim xBackendName As String
    Dim xFolderPath As String
    
    On Error GoTo ProcError
    With UserControl
        If .lstListings.Row = -1 Then
            Exit Sub
        End If
    
        If .lstListings.BoundText <> Empty Then

            '******************************************************
'            'get folder path of selected listing
            If .lstListings.SelBookmarks.Count > 0 Then
                Dim lBmk As Long
                lBmk = .lstListings.SelBookmarks(0)
                If lBmk > 0 Then
                    xTempPath = m_oListings.Item( _
                        .lstListings.SelBookmarks.Item(0)).FolderPath
                Else
                    xTempPath = m_oListings.Item(1).FolderPath
                End If
            Else
                xTempPath = m_oListings.Item(1).FolderPath
            End If
            
            If xTempPath <> Empty Then
                xBackendName = Left(.tvFolders.SelectedItem.FullPath, InStr(.tvFolders.SelectedItem.FullPath, "\"))
            End If
            
            If xBackendName <> Empty Then
                xFolderPath = xBackendName & xTempPath
            Else
                xFolderPath = .tvFolders.SelectedItem.FullPath
            End If
            
            .lblStatus.Caption = xFolderPath
            
            LoadAddresses
            
            EnableAddButtons True
            '.btnEdit.Enabled = True
            'enable/disable edit button per backend support
            .btnEdit.Enabled = CurrentBackend.SupportsContactEdit
            
            ShowListingStatus m_oListings.Item(UserControl.lstListings.Bookmark)
        Else
            EnableAddButtons False
            UserControl.btnEdit.Enabled = False
        End If
    End With
    UserControl.lstListings.SetFocus
    Exit Sub
ProcError:
    g_oError.RaiseError "CIX.CI.OnListingsRowChange"
    Exit Sub
End Sub
Private Sub EnableAddButtons(ByVal bEnable As Boolean)
'enables/disables all add buttons
    Dim i As Integer
    
    On Error GoTo ProcError
    
    With UserControl.btnAdd
        For i = .LBound To .UBound
            .Item(i).Enabled = bEnable
        Next i
    End With
    UserControl.btnAddMember.Enabled = bEnable
    Exit Sub
ProcError:
    g_oError.RaiseError "CIX.CI.EnableButtons"
End Sub

Private Function ShowTreeTop() As Long
'creates first level nodes on tree
    Dim oBackend As CIO.ICIBackend
    Dim oStores As CIO.CStores
    Dim oStore As CIO.CStore
    
    On Error GoTo ProcError
    
    If Me.ShowBackendNodes Or (mdlMain.Backends.Count > 1) Then
        'show backend nodes
        For Each oBackend In mdlMain.Backends
            DebugPrint "Adding Backend"
            If oBackend.SupportsFolders Or oBackend.SupportsMultipleStores Then
                'create backend node-key can not convert to an integer
                UserControl.tvFolders.Nodes.Add _
                    Text:=oBackend.DisplayName, Key:="b" & oBackend.InternalID, _
                    Image:="Backend", SelectedImage:="Backend"
            Else
                'backend has only one store - show the store as a backend node
                Set oStores = oBackend.GetStores
                UserControl.tvFolders.Nodes.Add _
                    Text:=oBackend.DisplayName, Key:=oStores.Item(1).UNID, _
                    Image:="Backend", SelectedImage:="Backend"
            End If
        Next oBackend
    Else
        'show store nodes of backend 1 - get stores
        Set oBackend = GetBackendFromIndex(1)
        Set oStores = oBackend.GetStores
        
        'populate branch
        For Each oStore In oStores
            UserControl.tvFolders.Nodes.Add _
                Key:=oStore.UNID, Text:=oStore.Name, _
                Image:="ClosedStore", SelectedImage:="OpenStore"
        Next oStore
    End If
    Exit Function
ProcError:
    g_oError.RaiseError "CIX.CI.ShowTreeTop"
End Function

Private Function ShowTreeBranch(oNode As MSComctlLib.node) As Long
'creates next level nodes on tree
    Dim i As Integer
    Dim iNumNodes As Integer
    Dim oBackend As CIO.ICIBackend
    Dim oStore As CIO.CStore
    Dim oFolder As CIO.CFolder
    Dim oStores As CIO.CStores
    Dim oFolders As CIO.CFolders
    Dim vStoreID As Variant
    Dim vFolderID As Variant
    Dim xUNID As String
    
    On Error GoTo ProcError
'   get child node based on type of node
    xUNID = oNode.Key
    
    Select Case m_oUNID.GetUNIDType(xUNID)
        Case ciUNIDType_Folder
            'is folder - get subfolders
            Set oBackend = mdlMain.GetBackendFromUNID(xUNID)
            
            If oBackend.SupportsNestedFolders Then
                'create folder from UNID
                Set oFolder = m_oUNID.GetFolder(xUNID)
                Set oFolders = oBackend.GetSubFolders(oFolder)
                
                If Not oFolders Is Nothing Then
                    'populate branch
                    If Not oFolders Is Nothing Then
                        For Each oFolder In oFolders
                            UserControl.tvFolders.Nodes.Add _
                            oNode, tvwChild, oFolder.UNID, oFolder.Name, _
                            "ClosedFolder", "OpenFolder"
                        Next oFolder
                    End If
                End If
            End If
        Case ciUNIDType_Store
            'is store - get folders
            Set oBackend = mdlMain.GetBackendFromUNID(xUNID)
            
            If oBackend.SupportsFolders Then
                'create store from UNID
                Set oStore = m_oUNID.GetStore(xUNID)
                Set oFolders = oBackend.GetFolders(oStore)
            
                If Not oFolders Is Nothing Then
                    'populate branch
                    For Each oFolder In oFolders
                        UserControl.tvFolders.Nodes.Add _
                            oNode, tvwChild, oFolder.UNID, oFolder.Name, _
                            "ClosedFolder", "OpenFolder"
                    Next oFolder
                End If
            End If
        Case ciUNIDType_Backend
            'is backend - get stores
            Set oBackend = mdlMain.GetBackendFromUNID(xUNID)
                
            If oBackend.SupportsMultipleStores Then
                Set oStores = oBackend.GetStores
                
                If Not oStores Is Nothing Then
                    'populate branch
                    For Each oStore In oStores
                        UserControl.tvFolders.Nodes.Add _
                            oNode, tvwChild, oStore.UNID, oStore.Name, _
                                "ClosedStore", "OpenStore"
                    Next oStore
                End If
            Else
                Set oFolders = oBackend.GetFolders(Nothing)
            
                If Not oFolders Is Nothing Then
                    'populate branch
                    For Each oFolder In oFolders
                        UserControl.tvFolders.Nodes.Add _
                            oNode, tvwChild, oFolder.UNID, oFolder.Name, _
                            "ClosedFolder", "OpenFolder"
                    Next oFolder
                End If
           End If
    End Select
    Exit Function
ProcError:
    g_oError.RaiseError "CIX.CI.ShowTreeBranch"
End Function

Private Sub DeleteBranch(ByVal node As MSComctlLib.node)
'deletes an entire branch of the tree and all the associated
'NodeDetail objects - does so by recursive iterations
    
    On Error GoTo ProcError
    If node.Children Then
        While node.Children
            DeleteSubBranch node.Child
        Wend
    End If
    Exit Sub
ProcError:
    g_oError.RaiseError "CIX.CI.DeleteBranch"
End Sub

Private Sub DeleteSubBranch(ByVal node As MSComctlLib.node)
'deletes an entire branch of the tree and all the associated
'NodeDetail objects - does so by recursive iterations
    
    On Error GoTo ProcError
    If node.Children Then
        While node.Children
            DeleteSubBranch node.Child
        Wend
    Else
        Dim lIndex As Long
        lIndex = node.Index
        UserControl.tvFolders.Nodes.Remove lIndex
    End If
    Exit Sub
ProcError:
    g_oError.RaiseError "CIX.CI.DeleteBranch"
End Sub

Private Sub RefreshTreeBranch(ByVal node As MSComctlLib.node)
    Dim xKey As String
    Dim iIndex As Integer
    
    On Error GoTo ProcError
    xKey = UserControl.tvFolders.SelectedItem.Key
    iIndex = UserControl.tvFolders.SelectedItem.Index
    
    'get currently selected node
    
'   delete existing branch
    DeleteBranch node
        
'   requery for updated tree branch
    ShowTreeBranch node
                        
    'reselect previously selected node
    On Error Resume Next
    UserControl.tvFolders.Nodes.Item(xKey).Selected = True
    If Err.Number > 0 Then
        UserControl.tvFolders.Nodes.Item(iIndex - 1).Selected = True
    End If
    
'   refresh toolbar if the refreshed node is the currently selected node
    If node Is UserControl.tvFolders.SelectedItem Then
        SetupDialogForNode node
    End If

    Exit Sub
ProcError:
    g_oError.RaiseError "CIX.CI.RefreshTreeBranch"
End Sub

Private Sub lstListings_RowChange()
    On Error GoTo ProcError
    With UserControl.lstListings
        If (Not IsPressed(KEY_CTL)) And _
            (Not IsPressed(KEY_SHIFT)) Then
            ClearListingSelections
            DoEvents
        End If
        
        .SelBookmarks.Add .Row + .FirstRow
        DoEvents
        
        OnListingsRowChange
        
    End With
    Exit Sub
ProcError:
    g_oError.ShowError
    Exit Sub
End Sub

Private Sub m_oEvents_AfterExecutingListingsQuery()
    On Error GoTo ProcError
    With UserControl.lblStatusMsg
        .Visible = False
        .Caption = Empty
    End With
    Exit Sub
ProcError:
    g_oError.ShowError
    Exit Sub
End Sub

Private Sub m_oEvents_AfterListingAdded(ByVal lIndexAdded As Long, ByVal lTotal As Long, Cancel As Boolean)
'update only every time the progress
'bar would add another square.
    Dim xMsg As String
    Dim sPercentComplete As Single
    Static dEstimatedTime As Double
        
    Static iContactsPerBarSquare As Integer
    Static t0 As Long
    Static t1 As Long
    
    On Error GoTo ProcError
    
    DoEvents
    
    If lTotal = 0 Then
        Exit Sub
    End If
    
    'get the estimated time to load 5% of contacts
    If lIndexAdded = 1 Then
        UserControl.MousePointer = vbDefault
        
        'get the time when #1 is added
        t0 = GetTickCount()
        iContactsPerBarSquare = CInt(lTotal / 26)
        dEstimatedTime = 0
    End If
    
    With UserControl.pbxLoad
        If (lIndexAdded / lTotal) >= 0.05 And dEstimatedTime = 0 Then
            '1% have been added - get time
            t1 = GetTickCount()
            
            'get the estimated time for all
            dEstimatedTime = CDbl((t1 - t0) / 0.05)
            If dEstimatedTime > 1000 Then
                'estimated time is more than a
                'second, show progress bar
                If Not .Visible Then
                    'show progress bar dialog
                    .Title = "Loading Contacts"
                    .ZOrder 0
                    .ShowCancelled = True
                    .Message = "Loading contacts.  (0%) complete."
                    .Value = 0
                    .Visible = True
                    DoEvents
                End If
            End If
        End If
    
        On Error Resume Next
        
        If .Visible Then
            'update the progress bar
            If lIndexAdded = lTotal Then
                'last contact has been added - close up
                .Message = "Loading contacts.  (100%) complete."
                .Value = 1
                DoEvents
                .Visible = False
                .Value = 0
            ElseIf (lIndexAdded Mod iContactsPerBarSquare) = 0 Then
                'enough contacts have been added to force
                'the display of another progress bar square
                sPercentComplete = (lIndexAdded / lTotal)
                .Message = "Loading contacts.  (" & _
                    Format(sPercentComplete, "#0%") & ") complete."
                .Value = (sPercentComplete)
            End If
            
            DoEvents
            
            If m_bLoadCancelled Then
    '           user cancelled the process -
                m_bLoadCancelled = False
                Cancel = True
                .Visible = False
                .Value = 0
                xMsg = "You have cancelled this operation. " & lIndexAdded & _
                    " of " & lTotal & " contacts have been loaded."
                MsgBox xMsg, vbInformation, g_oSessionType.AppTitle
            End If
        End If
    End With
    Exit Sub
ProcError:
    g_oError.ShowError
    Exit Sub
End Sub

Private Sub m_oEvents_AfterListingsRetrieved(ByVal lTotal As Long)
    On Error GoTo ProcError
    With UserControl
        .lblEntries.Caption = "&Available Contacts: (" & lTotal & ")"
        .pbxLoad.Visible = False
'        With UserControl.sbxSearch
'            .Visible = False
'            .Message = Empty
'        End With
        UserControl.MousePointer = vbDefault
    End With
    Exit Sub
ProcError:
    g_oError.ShowError
    Exit Sub
End Sub

'Private Sub m_oEvents_AfterStoreSearched()
'    On Error GoTo ProcError
'    With UserControl.lblStatusMsg
'        .Visible = False
'        .Caption = Empty
'    End With
'    Exit Sub
'ProcError:
'    g_oError.ShowError
'    Exit Sub
'End Sub

Private Sub m_oEvents_AfterStoreSearched()
    On Error GoTo ProcError
'    With UserControl.sbxSearch
'        .Visible = False
'        .Message = Empty
'    End With
    Exit Sub
ProcError:
    g_oError.ShowError
    Exit Sub
End Sub

Private Sub m_oEvents_BeforeExecutingListingsQuery()
    With UserControl.lblStatusMsg
        UserControl.MousePointer = vbHourglass
        .Caption = "Preparing to retrieve contacts.  Please wait..."
        .Visible = True
        DoEvents
    End With
End Sub

Private Sub m_oEvents_BeforeStoreSearchFolderSearch(ByVal xFolderName As String, ByRef Cancel As Boolean)
    Dim xMsg As String

    On Error GoTo ProcError

    If UserControl.pbxLoad.Visible Then
        Exit Sub
    End If

'    xMsg = "Searching folder '" & xFolderName & "'."
'
'    With UserControl.lblStatusMsg
'        .Caption = xMsg
'        .Visible = True
'    End With
'
'    DoEvents
    
'    With UserControl.sbxSearch
'        If Not .Visible Then
'            'reset status dialog canceled
'            'value for next search
'            .Canceled = False
'
'            'show status dialog
'            .Message = xMsg
'            .ZOrder 0
'            .Visible = True
'        Else
'            .Message = xMsg
'        End If
'
'        Cancel = .Canceled
'
'        DoEvents
'    End With
    Exit Sub
ProcError:
    g_oError.ShowError
    Exit Sub
End Sub

Private Sub mnuHelp_About_Click()
    On Error GoTo ProcError
    ShowAbout
    Exit Sub
ProcError:
    g_oError.ShowError
    Exit Sub
End Sub

Private Sub mnuContacts_AddGroup_Click()
    On Error GoTo ProcError
    AddGroup
    Exit Sub
ProcError:
    g_oError.ShowError
    Exit Sub
End Sub

Private Sub mnuContacts_DeleteGroup_Click()
    On Error GoTo ProcError
    DeleteGroup
    Exit Sub
ProcError:
    g_oError.ShowError
    Exit Sub
End Sub

Private Sub mnuContacts_MoveDown_Click()
    MoveSelContactDown
End Sub

Private Sub mnuContacts_MoveUp_Click()
    MoveSelContactUp
End Sub

Private Sub mnuContacts_SelectAll_Click()
    On Error GoTo ProcError
    Select Case m_iCurList
        Case ciListBoxes_Listings
            SelectAllListings
        Case ciListBoxes_GroupMembers
            SelectAllGroupMembers
        Case Else
            SelectAllContacts
    End Select
    Exit Sub
ProcError:
    g_oError.ShowError
    Exit Sub
End Sub

Private Sub mnuContacts_ShowDetail_Click()
    If m_iCurList = ciListBoxes_Listings Then
        ShowListingDetail
    Else
        ShowContactDetail 0
    End If
End Sub

Private Sub mnuContacts_ToggleContactList_Click()
    On Error GoTo ProcError
    ToggleListingsWidth
    Exit Sub
ProcError:
    g_oError.ShowError
    Exit Sub
End Sub
Private Sub mnuFolders_Custom1_Click()
'runs the first custom menu item
    On Error GoTo ProcError
    
    CurrentBackend.CustomProcedure1
    
    'refresh tree
    RefreshBackendBranch
    
    Exit Sub
ProcError:
    g_oError.ShowError
    Exit Sub
End Sub

Private Sub mnuFolders_Custom2_Click()
'runs the second custom menu item
    Dim vFunction As Variant
    
    On Error GoTo ProcError
    CurrentBackend.CustomProcedure2
    
    'refresh tree
    RefreshBackendBranch
    
    Exit Sub
ProcError:
    g_oError.ShowError
    Exit Sub
End Sub

Private Sub mnuFolders_Custom3_Click()
'runs the first custom menu item
    On Error GoTo ProcError
    
    CurrentBackend.CustomProcedure3
    
    'refresh tree
    RefreshBackendBranch
    
    Exit Sub
ProcError:
    g_oError.ShowError
    Exit Sub
End Sub

Private Sub mnuFolders_Custom4_Click()
'runs the second custom menu item
    Dim vFunction As Variant
    
    On Error GoTo ProcError
    CurrentBackend.CustomProcedure4
    
    'refresh tree
    RefreshBackendBranch
    
    Exit Sub
ProcError:
    g_oError.ShowError
    Exit Sub
End Sub

Private Sub mnuFolders_Custom5_Click()
'runs the second custom menu item
    Dim vFunction As Variant
    
    On Error GoTo ProcError
    CurrentBackend.CustomProcedure5
    
    'refresh tree
    RefreshBackendBranch
    
    Exit Sub
ProcError:
    g_oError.ShowError
    Exit Sub
End Sub

Private Sub RefreshBackendBranch()
'refreshes the selected backend branch of the tree
    Dim oNode As Object
    
    On Error GoTo ProcError
    
    UserControl.Parent.MousePointer = vbHourglass
    UserControl.Refresh
    
    'select top parent
    With UserControl.tvFolders
        While Not (.SelectedItem.Parent Is Nothing)
            .SelectedItem = .SelectedItem.Parent
        Wend
        Set oNode = .SelectedItem
    End With
    
    RefreshTreeBranch oNode
    UserControl.Parent.MousePointer = vbDefault
    Exit Sub
ProcError:
    g_oError.RaiseError "CIX.CI.RefreshBackendBranch"
    Exit Sub
End Sub

Private Sub mnuFolders_LoadOnStartup_Click()
    On Error GoTo ProcError
    SetDefaultFolder
    UserControl.mnuFolders_LoadOnStartup.Checked = True
    SetUserIni "CIApplication", ciUserIniKey_LoadDefaultOnStartup, True
    Exit Sub
ProcError:
    g_oError.ShowError
    Exit Sub
End Sub

Private Sub mnuFolders_RemoveDefault_Click()
    On Error GoTo ProcError
    SetUserIni "CIApplication", ciUserIniKey_DefaultNode, Empty
    UserControl.mnuFolders_LoadOnStartup.Checked = False
    SetUserIni "CIApplication", ciUserIniKey_LoadDefaultOnStartup, False
    UserControl.btnOptions.ToolTipText = "Default Folder: None specified"
    Exit Sub
ProcError:
    g_oError.ShowError
    Exit Sub
End Sub

Private Sub mnuFolders_SetAsDefault_Click()
    On Error GoTo ProcError
    SetDefaultFolder
    UserControl.mnuFolders_LoadOnStartup.Checked = False
    SetUserIni "CIApplication", ciUserIniKey_LoadDefaultOnStartup, False
    Exit Sub
ProcError:
    g_oError.ShowError
    Exit Sub
End Sub

Private Sub mnuFolders_ToggleQuickFilterType_Click()
    On Error GoTo ProcError
    If g_iQuickFilterType = ciQuickFilterType_Text Then
        g_iQuickFilterType = ciQuickFilterType_AlphaNumeric
    Else
        g_iQuickFilterType = ciQuickFilterType_Text
    End If
    SetQuickFilterType
    Exit Sub
ProcError:
    g_oError.ShowError
    Exit Sub
End Sub

Private Sub SetQuickFilterType()
    UserControl.fraQuickFilter.Visible = (g_iQuickFilterType <> ciQuickFilterType_Text)
End Sub
Private Sub SetDefaultFolder()
    Dim xFolder As String
    
    On Error GoTo ProcError
    xFolder = UserControl.tvFolders.SelectedItem.FullPath
    
    SetUserIni "CIApplication", ciUserIniKey_DefaultNode, _
        xFolder
        
    UserControl.btnOptions.ToolTipText = "Default Folder: " & xFolder
    Exit Sub
ProcError:
    g_oError.RaiseError "CIX.CI.SetDefaultFolder"
    Exit Sub
End Sub

Private Sub optQuickFilter_Click(Index As Integer)
    g_iQuickFilterOptionIndex = Index
End Sub

Private Sub pbxLoad_Cancelled()
    m_bLoadCancelled = True
    DoEvents
End Sub

Private Sub pbxLoad_OnHide()
    With UserControl
        .fraFolders.Enabled = True
        .fraToolbar.Enabled = True
    End With
End Sub

Private Sub pbxLoad_OnShow()
    With UserControl
        .fraFolders.Enabled = False
        .fraToolbar.Enabled = False
    End With
End Sub

Private Sub sbxSearch_OnHide()
    With UserControl
        .fraFolders.Enabled = True
        .fraToolbar.Enabled = True
    End With
End Sub

Private Sub sbxSearch_OnShow()
    With UserControl
        .fraFolders.Enabled = False
        .fraToolbar.Enabled = False
    End With
End Sub

Private Sub picBFilter_Click(Index As Integer)
    On Error GoTo ProcError
    g_iQuickFilterBtnIndex = Index
    If Ambient.UserMode = False Then Exit Sub
    LoadContacts ciSearchType_Quick
    Exit Sub
ProcError:
    g_oError.ShowError
    Exit Sub
End Sub

Private Sub tvFolders_DblClick()
    On Error GoTo ProcError
    If CurrentBackend.IsLoadableEntity(UserControl.tvFolders.SelectedItem.Key) Then
        LoadContacts
    End If
    Exit Sub
ProcError:
    g_oError.ShowError
End Sub

Private Sub tvFolders_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Dim oNode As MSComctlLib.node

    On Error GoTo ProcError
    If Button = 2 Then
        'right click occurred - select node, then show menu
        Set oNode = UserControl.tvFolders.HitTest(X, Y)
        If Not (oNode Is Nothing) Then
            oNode.Selected = True
            tvFolders_NodeClick oNode
            UserControl.PopupMenu mnuFolders
        End If
    End If
    Exit Sub
ProcError:
    g_oError.ShowError
    Exit Sub
End Sub

Private Sub tvFolders_NodeClick(ByVal node As MSComctlLib.node)
    On Error GoTo ProcError
    SetupDialogForNode node
    Exit Sub
ProcError:
    g_oError.ShowError
    Exit Sub
End Sub

Private Sub SetupDialogForNode(ByVal node As MSComctlLib.node)
    Dim iUNIDType As ciUNIDTypes
    Dim i As Integer
    Dim oCtl As VB.Control
    Dim xContainer As String
    Dim bShowQuickFilterOption As Boolean
    Dim bIsLoadable As Boolean
    Dim bIsSearchable As Boolean
    Dim datStart As Double
    Dim iloadTimeOut As Integer

    On Error GoTo ProcError
    
    With UserControl
        'enable native search if current backend supports it
        .btnSearchNative.Enabled = CurrentBackend.SupportsNativeSearch
        .btnAddContactInNative.Enabled = CurrentBackend.SupportsContactAdd
        
        'get type of node
        iUNIDType = m_oUNID.GetUNIDType(node.Key)
        
        
        bIsLoadable = CurrentBackend.IsLoadableEntity(node.Key)
        bIsSearchable = CurrentBackend.IsSearchableEntity(node.Key)
        
        'enable 'find' button for searchable nodes
        .btnSearch.Enabled = bIsSearchable
        .btnQuickSearch.Enabled = bIsSearchable
        .txtFilterValue1.Enabled = bIsSearchable
        .cmbFilterFld1.Enabled = bIsSearchable
        .cmbOp1.Enabled = bIsSearchable
        .lblQuickSearch.Enabled = bIsSearchable
        
        'enable alpha/numeric quick filter controls
        .fraQuickFilter.Enabled = bIsSearchable
        For i = 0 To .picBFilter.Count - 1
            .picBFilter(i).Enabled = bIsSearchable
        Next i
        
        Dim oFilter As CIO.CFilter
        Dim oField As CIO.CFilterField
        Set oFilter = CurrentBackend.Filter
        For i = 0 To oFilter.CountFields - 1
            Set oField = oFilter.FilterFields(i + 1)
            'setup quick filter option control caption
            With .optQuickFilter(i)
                .Caption = "&" & oField.Name
                .Visible = True
            End With
        Next i
        
        'GLOG : 5389 : ceh
        For i = (oFilter.CountFields + 1) To 4
            .optQuickFilter(i - 1).Visible = False
        Next i

        For i = 0 To .optQuickFilter.Count - 1
            .optQuickFilter(i).Enabled = bIsSearchable
        Next i

        'enable 'show contacts' button for loadable nodes
        .btnTogglePanels.Enabled = bIsLoadable
        
        .mnuFolders_LoadOnStartup.Enabled = bIsLoadable
    End With
    
    If Not node.Expanded Then
        UserControl.Parent.MousePointer = vbHourglass
        ExpandNode node
        'Get Favorite Contacts load timeout value if necessary
        If UCase(node.Text) Like "*MAILBOX - *" Then
            On Error Resume Next
            iloadTimeOut = g_oIni.GetIni("Backend" & CurrentBackend.ID, "FavContactsLoadTimeout", g_oIni.CIIni)
            On Error GoTo ProcError
            If iloadTimeOut Then
    '           mark time - give timeout for log-in
                datStart = Empty
                datStart = Now()
                While Now() < datStart + (iloadTimeOut / ciSecsPerDay)
                    DoEvents
                Wend
            End If
        End If
        
    End If
    
    'refresh quick search controls based
    'on backend of selected node
    DoEvents
    
    'temporary
    'bShowQuickFilterOption = bIsQuickFilterBackend(CurrentBackend.InternalID)
        
    'GLOG : 5390 : ceh
    g_iDefaultFilter = 0
    If Not bIsQuickFilterBackend(CurrentBackend.InternalID) Then
        g_iQuickFilterType = ciQuickFilterType_Text
    Else
        g_iQuickFilterType = ciQuickFilterType_AlphaNumeric
    End If
    
    SetQuickFilterType
    
    RefreshQuickSearchControls
    
    'setup Options menu
    Select Case CurrentBackend.InternalID
        Case 100, 104, 111      'Outlook OM, MP9, MP10
            bShowQuickFilterOption = True
        Case Else
            bShowQuickFilterOption = False
    End Select

    UserControl.mnuFolders_ToggleQuickFilterType.Visible = bShowQuickFilterOption
    UserControl.mnuFolders_Sep2.Visible = bShowQuickFilterOption
    '- end of temporary
    
    'setup custom menu items
    SetupCustomMenuItems
    
    UserControl.Parent.MousePointer = vbDefault
    Exit Sub
ProcError:
    Dim lErr As Long
    Dim xDesc As String
    
    lErr = Err.Number
    xDesc = Err.Description
    
    UserControl.Parent.MousePointer = vbDefault
    
    Err.Number = lErr
    Err.Description = xDesc
    g_oError.RaiseError "CIX.CI.SetupDialogForNode"
End Sub

Private Sub SetupCustomMenuItems()
'adds/removes menu items from popup menu based on backend-
'configuration string is of the form 'MenuCaption,FunctionName,NodeType
    Dim xMenuCaption As String
    Dim i As Integer
    Dim xConfigArray() As String
    
    On Error GoTo ProcError
    i = 1
    
    'remove any previous menus
    With UserControl
        .mnuFolders_Sep1.Visible = False
        .mnuFolders_Custom1.Visible = False
        .mnuFolders_Custom2.Visible = False
        .mnuFolders_Custom3.Visible = False
        .mnuFolders_Custom4.Visible = False
        .mnuFolders_Custom5.Visible = False
    End With
    
    'get first custom menu item caption
    xMenuCaption = CurrentBackend.CustomMenuItem1
    
    If xMenuCaption <> Empty Then
        With UserControl.mnuFolders_Custom1
            .Visible = True
            .Caption = xMenuCaption
        End With
    End If
    
    'get second custom menu item caption
    xMenuCaption = CurrentBackend.CustomMenuItem2
    
    If xMenuCaption <> Empty Then
        With UserControl.mnuFolders_Custom2
            .Visible = True
            .Caption = xMenuCaption
        End With
    End If
    
    'get third custom menu item caption
    xMenuCaption = CurrentBackend.CustomMenuItem3
    
    If xMenuCaption <> Empty Then
        With UserControl.mnuFolders_Custom3
            .Visible = True
            .Caption = xMenuCaption
        End With
    End If
    
    'get fourth custom menu item caption
    xMenuCaption = CurrentBackend.CustomMenuItem4
    
    If xMenuCaption <> Empty Then
        With UserControl.mnuFolders_Custom4
            .Visible = True
            .Caption = xMenuCaption
        End With
    End If
    
    'get fifth custom menu item caption
    xMenuCaption = CurrentBackend.CustomMenuItem5
    
    If xMenuCaption <> Empty Then
        With UserControl.mnuFolders_Custom5
            .Visible = True
            .Caption = xMenuCaption
        End With
    End If
    
    With UserControl
        .mnuFolders_Sep1.Visible = (.mnuFolders_Custom1.Visible Or _
            .mnuFolders_Custom2.Visible Or .mnuFolders_Custom3.Visible Or _
            .mnuFolders_Custom4.Visible Or .mnuFolders_Custom5.Visible)
    End With
    
    Exit Sub
ProcError:
    g_oError.RaiseError "CIX.CI.SetupCustomMenuItems"
    Exit Sub
End Sub

Private Sub ExpandNode(ByVal node As MSComctlLib.node)
'expands the specified tree node
    On Error GoTo ProcError
    If node.Children = 0 Then
        ShowTreeBranch node
    End If
    node.Expanded = True
    Exit Sub
ProcError:
    Err.Raise Err.Number, "CIX.CI.ExpandNode", Err.Description
End Sub

Public Sub RefreshQuickSearchControls()
'refreshes the quick search fields and operators
'based on the backend of selected node
    Dim oBackend As CIO.ICIBackend
    Dim xUNID As String
    Dim xSelFilter1 As String
    Dim xSelOp1 As String
    Dim xSelFilter2 As String
    Dim xSelOp2 As String
    Dim oFilter As CIO.CFilter
    Dim iOps As ciSearchOperators
    Dim i As Integer
    Dim oFilterFlds As XArrayDB
    Dim oFilterOps As XArrayDB
    Dim oFld As CIO.CFilterField
    
    Static iPrevBackendID As Integer
    
'   get backend
    Set oBackend = CurrentBackend()

    'exit if backend didn't change
    If iPrevBackendID = oBackend.ID Then
        Exit Sub
    Else
        'set static var for next iteration
        iPrevBackendID = oBackend.ID
    End If
    
'   get currently selected filter -
'   used below to reselect in new lists
    On Error GoTo ProcError
    With UserControl
        xSelFilter1 = .cmbFilterFld1.Text
        xSelOp1 = .cmbOp1.Text
    End With
    
'   get filter for backend
    Set oFilter = oBackend.Filter

'   add each filter field from filter to xarray
    Set oFilterFlds = New XArrayDB
    With oFilterFlds
        .ReDim 0, -1, 0, 1
        For i = 1 To oFilter.CountFields
            .Insert 1, i - 1
            Set oFld = oFilter.FilterFields(i)
            .Value(i - 1, 0) = oFld.Name
            .Value(i - 1, 1) = oFld.ID
            'setup quick filter option control caption
            With UserControl.optQuickFilter(i - 1)
                .Caption = "&" & oFld.Name
                .Visible = True
            End With
        Next i
    End With
    
    'bind array to both filter fields lists
    With UserControl
        .cmbFilterFld1.Array = oFilterFlds
        .cmbFilterFld1.ReBind
        
        'resize dropdowns
        ResizeTDBCombo .cmbFilterFld1, 4
    End With
    
'   add each filter operator for this backend to xarray
    Set oFilterOps = New XArrayDB
    iOps = oBackend.SearchOperators
    With oFilterOps
        .ReDim 0, -1, 0, 1
        If iOps And ciSearchOperator_BeginsWith Then
            i = .UpperBound(1) + 1
            .Insert 1, i
            .Value(i, 0) = "Begins With"
            .Value(i, 1) = ciSearchOperator_BeginsWith
        End If
        If iOps And ciSearchOperator_Contains Then
            i = .UpperBound(1) + 1
            .Insert 1, i
            .Value(i, 0) = "Contains"
            .Value(i, 1) = ciSearchOperator_Contains
        End If
        If iOps And ciSearchOperator_Equals Then
            i = .UpperBound(1) + 1
            .Insert 1, i
            .Value(i, 0) = "Is"
            .Value(i, 1) = ciSearchOperator_Equals
        End If
    End With
    
    'bind array to both filter fields lists
    With UserControl
        .cmbOp1.Array = oFilterOps
        .cmbOp1.ReBind
        '.cmbOp2.Array = oFilterOps
    
        'resize dropdowns
        ResizeTDBCombo .cmbOp1, 4
        'ResizeTDBCombo .cmbOp2, 4
    End With
    
'   return to previous selections if possible
    With UserControl.cmbFilterFld1
        If xSelFilter1 <> Empty Then
            .BoundText = xSelFilter1
        End If
        If IsNull(.SelectedItem) Then
            'no selection exists - use default
            .ReBind
'FIX: get default from user.ini
            If Not .Visible Then
                FixTDBCombo UserControl.cmbFilterFld1
            End If
            
            .SelectedItem = 0
        End If
    End With
    
'    With UserControl.cmbFilterFld2
'        If xSelFilter2 <> Empty Then
'            .BoundText = xSelFilter2
'        End If
'        If IsNull(.SelectedItem) Then
'            .ReBind
'            'no selection exists - use default
''FIX: get default from user.ini
'            If Not .Visible Then
'                FixTDBCombo UserControl.cmbFilterFld2
'            End If
'            .SelectedItem = 1
'        End If
'    End With
    
    With UserControl.cmbOp1
        If xSelOp1 <> Empty Then
            .BoundText = xSelOp1
        End If
        If IsNull(.SelectedItem) Then
            'no selection exists - use default
            .ReBind
'FIX: get default from user.ini
            If Not .Visible Then
                FixTDBCombo UserControl.cmbOp1
            End If
            .SelectedItem = 0
        End If
    End With
    Exit Sub
ProcError:
    g_oError.RaiseError "CIX.CI.RefreshQuickSearchControls"
    Exit Sub
End Sub

Private Function CurrentBackend() As CIO.ICIBackend
'returns the backend of the selected node
    Dim oNode As MSComctlLib.node
    Dim xUNID As String
    
    Set oNode = UserControl.tvFolders.SelectedItem
    
    If Not oNode Is Nothing Then
'       get UNID
        xUNID = GetUNIDNoPrefix(oNode.Key)

'       get backend from UNID
        Set CurrentBackend = mdlMain.GetBackendFromUNID(xUNID)
    End If
End Function

Public Function GetUNIDNoPrefix(ByVal xUNID As String) As String
'returns the supplied UNID without
'the 'b' prefix if it exists
    On Error GoTo ProcError
    If Left(xUNID, 1) = "b" Then
        GetUNIDNoPrefix = Mid(xUNID, 2)
    Else
        GetUNIDNoPrefix = xUNID
    End If
    Exit Function
ProcError:
    g_oError.RaiseError "CIX.CI.GetUNIDNoPrefix"
    Exit Function
End Function

Private Sub txtFilterValue1_GotFocus()
    UserControl.btnQuickSearch.Default = True
End Sub

Private Sub txtFilterValue1_LostFocus()
    UserControl.btnQuickSearch.Default = False
End Sub

Private Sub txtFilterValue2_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        LoadContacts ciSearchType_Quick
    End If
End Sub

Public Sub Initialize()
    Dim bLoadOnStartup As Boolean
    Dim oNode As MSComctlLib.node
    Dim i As Integer
    Dim xQuickFilterType As String
    Dim bShowQuickFilterOption As Boolean
    
    On Error GoTo ProcError
    
'   set the event generator to the event generator
'   of the backend of this store - we want to handle
'   events raised by that backend
    Set m_oEvents = g_oGlobals.CIEvents
            
    Set m_oUNID = New CIO.CUNID
    Set m_oContacts = New CIO.CContacts
    
    SetUpColumns
    
    'setting the mask at run time ensures that button graphics
    'have transparency - this is a problem with switching
    'between Windows XP and Windows Classic themes
    For i = 0 To 3
        UserControl.btnAdd(i).MaskColor = &HC0C0C0
    Next
    
    UserControl.btnHelp.MaskColor = &H8000000F
    UserControl.btnQuickSearch.MaskColor = &HC0C0C0
    UserControl.btnAddMember.MaskColor = &HC0C0C0
    UserControl.btnMoveDown.MaskColor = &HC0C0C0
    UserControl.btnMoveUp.MaskColor = &HC0C0C0
    
    'set Options button tooltip to display current default folder
    Dim xDefFolder As String
    xDefFolder = g_oIni.GetIni("CIApplication", "DefaultNode", g_oIni.CIUserIni())
    With UserControl
        If xDefFolder <> Empty Then
            .btnOptions.ToolTipText = "Default Folder: " & xDefFolder
        Else
            .btnOptions.ToolTipText = "Default Folder: None specified"
        End If
        
        .grdEntityList.Columns(0).Caption = g_oIni.GetIni("CICustom", "CustomCol1Caption")
        .grdEntityList.Columns(2).Caption = g_oIni.GetIni("CICustom", "CustomCol2Caption")
        
    End With
    
    'set User Doc path
    g_xHelpPath = App.Path
    
    'If running from App folder, set base to parent MacPac Folder
    i = InStrRev(UCase(g_xHelpPath), "\APP")
    If i > 0 Then _
        g_xHelpPath = Left(g_xHelpPath, i - 1)
    g_xHelpPath = g_xHelpPath & "\Tools\User Documentation\pdf"

    
    'show first level of tree
    ShowTreeTop
    
    'select a default node if specified
    Set oNode = SelectNode()
    
    If Not (oNode Is Nothing) Then
        'a default node has been selected
        On Error Resume Next
        bLoadOnStartup = CBool(GetUserIni("CIApplication", _
            ciUserIniKey_LoadDefaultOnStartup))
        On Error GoTo ProcError
        
        If bLoadOnStartup Then
            'user has specified to load contacts in default folder
            UserControl.mnuFolders_LoadOnStartup.Checked = True
            LoadContacts ciSearchType_None
            RaiseEvent PanelChange(False)
        Else
            RaiseEvent PanelChange(True)
        End If
        
        If Not (oNode Is Nothing) Then
            'set the proper quick search list items for the
            'specified node - these are backend specific
            RefreshQuickSearchControls
        End If
    Else
        RaiseEvent PanelChange(True)
    End If
        
    'reset global var for default filter
    g_iDefaultFilter = 0
    
    'temporary
    bShowQuickFilterOption = bIsQuickFilterBackend(CurrentBackend.InternalID)
        
    If bShowQuickFilterOption Then
        xQuickFilterType = g_oIni.GetIni( _
                "CIApplication", "DefaultQuickFilterType", g_oIni.CIIni)
                
        Select Case xQuickFilterType
            Case "1", "2"
                g_iQuickFilterType = CInt(xQuickFilterType)
            Case Else
                g_iQuickFilterType = ciQuickFilterType_Text
        End Select
    Else
        g_iQuickFilterType = ciQuickFilterType_Text
    End If
    
    g_iDefaultFilter = g_iQuickFilterType
    SetQuickFilterType
    
    m_bInitialized = True
    Exit Sub
ProcError:
    g_oError.ShowError
End Sub
Private Sub SetUpToolbar()
    Dim oB As ICIBackend
    Dim oLastVisibleBtn As VB.CommandButton
    
    If Not Ambient.UserMode Then
        Exit Sub
    End If
    
    On Error GoTo ProcError
    
    With UserControl
        .mnuContacts_SelectAll.Caption = "Select &All" ' & vbTab & "(Ctrl + A)"
        .mnuContacts_ToggleContactList.Caption = "Expand Available Contacts &List" ' & vbTab & "(Ctrl + W)"
        .mnuContacts_ShowDetail.Caption = "Show &Detail" & vbTab & "Right Click on Contact"
        .mnuContacts_MoveUp.Caption = "Move Selected Contacts &Up" & vbTab & "Ctrl + Up Arrow"
        .mnuContacts_MoveDown.Caption = "Move Selected Contacts Do&wn" & vbTab & "Ctrl + Down Arrow"
        .btnHelp.MaskColor = &H0&
    End With
    
    'hide 'Native Find' button if no backend supports this functionality
    With UserControl.btnSearchNative
        Dim bShowNativeSearch As Boolean
        
        'cycle through backend, finding one
        'backend that supports NativeSearch
        For Each oB In Backends()
            If oB.SupportsNativeSearch Then
                bShowNativeSearch = True
                Exit For
            End If
        Next oB
        
        If bShowNativeSearch Then
            .Visible = True
        Else
            .Visible = False
            'move refresh button
            UserControl.btnRefresh.Left = .Left
        End If
    End With
    
    Set oLastVisibleBtn = UserControl.btnRefresh
    
    'hide 'New' button if no backend supports this functionality
    With UserControl.btnAddContactInNative
        For Each oB In Backends()
            If oB.SupportsContactAdd Then
                .Visible = True
                .Left = oLastVisibleBtn.Left + oLastVisibleBtn.Width - 15
                Set oLastVisibleBtn = UserControl.btnAddContactInNative
                Exit For
            Else
                .Visible = False
            End If
        Next oB
    End With
    
    'hide 'Edit' button if no backend supports this functionality
    With UserControl.btnEdit
        For Each oB In Backends()
            If oB.SupportsContactEdit Then
                .Visible = True
                .Left = oLastVisibleBtn.Left + oLastVisibleBtn.Width - 15
                Set oLastVisibleBtn = UserControl.btnEdit
                Exit For
            Else
                .Visible = False
            End If
        Next oB
    End With
    
    UserControl.btnOptions.Left = oLastVisibleBtn.Left + oLastVisibleBtn.Width - 15
    Set oLastVisibleBtn = UserControl.btnOptions
    
    'hide 'Groups' button if appropriate
    With UserControl.chkGroups
        If Not (Backends().GroupsBackend Is Nothing) Then
            .Visible = True
            .Left = oLastVisibleBtn.Left + oLastVisibleBtn.Width - 15
        Else
            .Visible = False
        End If
    End With
        
    
    Exit Sub
ProcError:
    g_oError.RaiseError "CIX.CI.SetUpToolbar"
    Exit Sub
End Sub

Private Sub UserControl_Initialize()
    If Not g_oGlobals Is Nothing Then
        Math.Randomize
        xObjectID = "CI" & Format(Now, "hhmmss") & Math.Rnd()
        g_oGlobals.Objects.Add xObjectID, xObjectID
        DebugPrint "Added - " & xObjectID & ": " & g_oGlobals.Objects.Count
    End If
End Sub

Private Sub UserControl_InitProperties()
    On Error GoTo ProcError
    InitProperties
    Exit Sub
ProcError:
    g_oError.ShowError
    Exit Sub
End Sub

Private Sub UserControl_ReadProperties(PropBag As PropertyBag)
    On Error GoTo ProcError
    ReadProperties PropBag
    Exit Sub
ProcError:
    g_oError.ShowError
    Exit Sub
End Sub

Private Sub UserControl_Resize()
    On Error GoTo ProcError
    ResizeControls
    Exit Sub
ProcError:
    g_oError.ShowError
    Exit Sub
End Sub

Private Sub ResizeControls()
    On Error GoTo ProcError
    With UserControl
        'If g_bIsWord15 Then
            '.tvFolders.Width = .Width - 500
            '.fraFolders.Width = .Width
            '.fraListings.Width = .Width
            '.cmdCancel.Left = .Width - 1470
            '.cmdOK.Left = .Width - 2670
            '.fraToolbar.Width = .Width - 185
            '.btnHelp.Left = .fraToolbar.Width - .btnHelp.Width
        'Else
            .tvFolders.Width = .Width - 350
            .fraFolders.Width = .Width
            .fraListings.Width = .Width
            .cmdCancel.Left = .Width - 1270
            .cmdOK.Left = .Width - 2470
            .fraToolbar.Width = .Width
            .btnHelp.Left = .fraToolbar.Width - .btnHelp.Width
        'End If
        
        With .Line1
            .X1 = 0
            .X2 = UserControl.Width
        End With
        With .Line2
            .X1 = 0
            .X2 = UserControl.Width
        End With
        
    End With
    Exit Sub
ProcError:
    g_oError.RaiseError "CIX.CI.ResizeControls"
    Exit Sub
End Sub

Private Sub UserControl_Show()
    Dim i As Integer
    
    If Ambient.UserMode = True Then
        'this is required to fix some funky behavior -
        'if missing, any default node other than IA will
        'cause the folders frame to disable!!!!
        With UserControl
            .fraFolders.Enabled = True
            .fraToolbar.Enabled = True
            .fraListings.Enabled = True
        End With

        SetUpToolbar
    End If
    
    On Error Resume Next
    UserControl.tvFolders.SetFocus
End Sub

Private Sub UserControl_WriteProperties(PropBag As PropertyBag)
    On Error GoTo ProcError
    WriteProperties PropBag
    Exit Sub
ProcError:
    g_oError.RaiseError "CIX.CI.UserControl_WriteProperties"
    Exit Sub
End Sub

Private Sub InitProperties()
    On Error GoTo ProcError
    Me.OKButtonCaption = "&Insert"
    Me.ShowBackendNodes = True
    Me.DataRetrievedForTo = ciRetrieveData_All
    Me.DataRetrievedForFrom = ciRetrieveData_All
    Me.DataRetrievedForCC = ciRetrieveData_All
    Me.DataRetrievedForBCC = ciRetrieveData_All
    Me.SelectionList = CIX.ciSelectionList_To
    Me.LabelTo = "&To:"
    Me.LabelFrom = "Fro&m:"
    Me.LabelCC = "&CC:"
    Me.LabelBCC = "&BCC:"
    Exit Sub
ProcError:
    g_oError.RaiseError "CIX.CI.ReadProperties"
    Exit Sub
End Sub

Private Sub WriteProperties(PropBag As PropertyBag)
    On Error GoTo ProcError
    With PropBag
        PropBag.WriteProperty "OKButtonCaption", Me.OKButtonCaption, "&Insert"
        PropBag.WriteProperty "DataRetrievedForTo", Me.DataRetrievedForTo, ciRetrieveData_All
        PropBag.WriteProperty "DataRetrievedForFrom", Me.DataRetrievedForFrom, ciRetrieveData_All
        PropBag.WriteProperty "DataRetrievedForCC", Me.DataRetrievedForCC, ciRetrieveData_All
        PropBag.WriteProperty "DataRetrievedForBCC", Me.DataRetrievedForBCC, ciRetrieveData_All
        PropBag.WriteProperty "SelectionList", Me.SelectionList, CIX.ciSelectionList_To
        PropBag.WriteProperty "MaxContacts", m_iMaxContacts, 0
        PropBag.WriteProperty "SelectionLists", Me.SelectionList, CIX.ciSelectionList_To
        PropBag.WriteProperty "ShowBackendNodes", Me.ShowBackendNodes, True
        PropBag.WriteProperty "LabelTo", Me.LabelTo, "&To:"
        PropBag.WriteProperty "LabelFrom", Me.LabelFrom, "Fro&m:"
        PropBag.WriteProperty "LabelCC", Me.LabelCC, "&CC:"
        PropBag.WriteProperty "LabelBCC", Me.LabelBCC, "&BCC:"
    End With
    Exit Sub
ProcError:
    g_oError.RaiseError "CIX.CI.ReadProperties"
    Exit Sub
End Sub

Private Sub ReadProperties(PropBag As PropertyBag)
    On Error GoTo ProcError
    With PropBag
        Me.OKButtonCaption = PropBag.ReadProperty("OKButtonCaption", "&Insert")
        Me.DataRetrievedForTo = PropBag.ReadProperty("DataRetrievedForTo", ciRetrieveData_All)
        Me.DataRetrievedForFrom = PropBag.ReadProperty("DataRetrievedForFrom", ciRetrieveData_All)
        Me.DataRetrievedForCC = PropBag.ReadProperty("DataRetrievedForCC", ciRetrieveData_All)
        Me.DataRetrievedForBCC = PropBag.ReadProperty("DataRetrievedForBCC", ciRetrieveData_All)
        Me.SelectionList = PropBag.ReadProperty("SelectionList", CIX.ciSelectionList_To)
        Me.MaxContacts = PropBag.ReadProperty("MaxContacts", 0)
        Me.SelectionLists = PropBag.ReadProperty("SelectionLists", CIX.ciSelectionList_To)
        Me.ShowBackendNodes = PropBag.ReadProperty("ShowBackendNodes", True)
        Me.LabelTo = PropBag.ReadProperty("LabelTo", "&To:")
        Me.LabelFrom = PropBag.ReadProperty("LabelFrom", "Fro&m:")
        Me.LabelCC = PropBag.ReadProperty("LabelCC", "&CC:")
        Me.LabelBCC = PropBag.ReadProperty("LabelBCC", "&BCC:")
    End With
    Exit Sub
ProcError:
    g_oError.RaiseError "CIX.CI.ReadProperties"
    Exit Sub
End Sub

Friend Function SetSelectionLists(ByVal iSelectionLists As ciSelectionLists)
'sets right-side selection lists visible/invisible
'based on iSelectionLists
    Const OneListPos1 As Integer = 870
    Const TwoListsPos1 As Integer = 870
    Const TwoListsPos2 As Integer = 3375
    Const ThreeListsPos1 As Integer = 870
    Const ThreeListsPos2 As Integer = 2437
    Const ThreeListsPos3 As Integer = 4005
    Const FourListsPos1 As Integer = 870
    Const FourListsPos2 As Integer = 2040
    Const FourListsPos3 As Integer = 3225
    Const FourListsPos4 As Integer = 4425

    Const OneListHeight As Integer = 4470
    Const TwoListsHeight As Integer = 1950
    Const ThreeListsHeight As Integer = 1320
    Const FourListsHeight As Integer = 900

    Const ButtonSeparation As Integer = 330
    Const LabelSeparation As Integer = 210
    
    Dim Lists() As Integer
    Dim iNumLists As Integer
    Dim i As Integer
    Dim ctlP As VB.Control
    
    ReDim Lists(0)
    
'   reset controls - make invisible and clean out tags
    For i = 0 To 3
        With UserControl
            .lblSelContacts(i).Visible = False
            .btnAdd(i).Visible = False
            .btnDelete(i).Visible = False
            .lstContacts(i).Visible = False
        End With
    Next i
    
    '*c - test
    With UserControl
        .btnAdd(4).Visible = False
        .btnDelete(4).Visible = False
        .grdEntityList.Visible = False
        .lblEntities.Visible = False
    End With
    
    If iSelectionLists = ciSelectionList_Custom Then
        With UserControl
            .grdEntityList.Visible = True
            .grdEntityList.Array = Me.EntityList
            .lblEntities.Caption = "Available En&tities: (" & Me.EntityList.Count(1) & ")"
            .grdEntityList.ReBind
            .btnAdd(4).Visible = True
            .btnDelete(4).Visible = True
            .lblEntities.Top = 660
            .lblEntities.Visible = True
            
'           recreate m_oContacts
            
            
            
'           enable insert btn only if contacts exist
'            .cmdOK.Enabled = m_oContacts.Count
            'Hide irrelevant menu items
            .mnuContacts_MoveDown.Visible = False
            .mnuContacts_MoveUp.Visible = False
            .mnuContacts_Sep1.Visible = False
            .btnMoveDown.Visible = False
            .btnMoveUp.Visible = False

        End With
    Else
        'get specified lists - if list is to be shown,
        'add list to array of lists
        If iSelectionLists >= ciSelectionList_BCC Then
            Lists(0) = 3
            iSelectionLists = iSelectionLists - ciSelectionList_BCC
        End If
        
        If iSelectionLists >= ciSelectionList_CC Then
            If Lists(0) <> Empty Then
                ReDim Preserve Lists(UBound(Lists) + 1)
            End If
            Lists(UBound(Lists)) = 2
            iSelectionLists = iSelectionLists - ciSelectionList_CC
        End If
        
        If iSelectionLists >= ciSelectionList_From Then
            If Lists(0) <> Empty Then
                ReDim Preserve Lists(UBound(Lists) + 1)
            End If
            Lists(UBound(Lists)) = 1
            iSelectionLists = iSelectionLists - ciSelectionList_From
        End If
        
        If iSelectionLists >= ciSelectionList_To Then
            If Lists(0) <> Empty Then
                ReDim Preserve Lists(UBound(Lists) + 1)
            End If
            Lists(UBound(Lists)) = 0
        End If
        
    '   count number of lists
        iNumLists = UBound(Lists) + 1
        
        With UserControl
            Select Case iNumLists
                Case 1
                    i = Lists(0)
                    .lblSelContacts(i).Top = OneListPos1 - LabelSeparation
                    .btnAdd(i).Top = OneListPos1 + 15
                    .btnDelete(i).Top = .btnAdd(i).Top + ButtonSeparation
                    
                    .lblSelContacts(i).Visible = True
                    .btnAdd(i).Visible = True
                    .btnDelete(i).Visible = True
                    
                    With .lstContacts(i)
                        .Top = OneListPos1
                        .Height = OneListHeight
                        .Visible = True
                        .Visible = True
                    End With
                Case 2
                    i = Lists(0)
                    .lblSelContacts(i).Top = TwoListsPos2 - LabelSeparation
                    .lstContacts(i).Top = TwoListsPos2
                    .btnAdd(i).Top = TwoListsPos2 + 15
                    .btnDelete(i).Top = .btnAdd(i).Top + ButtonSeparation
                
                    .lblSelContacts(i).Visible = True
                    .btnAdd(i).Visible = True
                    .btnDelete(i).Visible = True
                    
                    With .lstContacts(i)
                        .Top = TwoListsPos2
                        .Height = TwoListsHeight
                        .Visible = True
                    End With
                
                    i = Lists(1)
                    .lblSelContacts(i).Top = TwoListsPos1 - LabelSeparation
                    .lstContacts(i).Top = TwoListsPos1
                    .btnAdd(i).Top = TwoListsPos1 + 15
                    .btnDelete(i).Top = .btnAdd(i).Top + ButtonSeparation
                    
                    .lblSelContacts(i).Visible = True
                    .btnAdd(i).Visible = True
                    .btnDelete(i).Visible = True
                    
                    With .lstContacts(i)
                        .Top = TwoListsPos1
                        .Height = TwoListsHeight
                        .Visible = True
                    End With
                
                Case 3
                    i = Lists(0)
                    'position controls for 3rd list
                    .lblSelContacts(i).Top = ThreeListsPos3 - LabelSeparation
                    .lstContacts(i).Top = ThreeListsPos3
                    .btnAdd(i).Top = ThreeListsPos3 + 15
                    .btnDelete(i).Top = .btnAdd(i).Top + ButtonSeparation
                
                    .lblSelContacts(i).Visible = True
                    .btnAdd(i).Visible = True
                    .btnDelete(i).Visible = True
                    
                    With .lstContacts(i)
                        .Top = ThreeListsPos3
                        .Height = ThreeListsHeight
                        .Visible = True
                        .Visible = True
                    End With
                
                    i = Lists(1)
                    .lblSelContacts(i).Top = ThreeListsPos2 - LabelSeparation
                    .lstContacts(i).Top = ThreeListsPos2
                    .btnAdd(i).Top = ThreeListsPos2 + 15
                    .btnDelete(i).Top = .btnAdd(i).Top + ButtonSeparation
                
                    .lblSelContacts(i).Visible = True
                    .btnAdd(i).Visible = True
                    .btnDelete(i).Visible = True
                    
                    With .lstContacts(i)
                        .Top = ThreeListsPos2
                        .Height = ThreeListsHeight
                        .Visible = True
                        .Visible = True
                    End With
                
                    i = Lists(2)
                    .lblSelContacts(i).Top = ThreeListsPos1 - LabelSeparation
                    .lstContacts(i).Top = ThreeListsPos1
                    .btnAdd(i).Top = ThreeListsPos1 + 15
                    .btnDelete(i).Top = .btnAdd(i).Top + ButtonSeparation
                
                    .lblSelContacts(i).Visible = True
                    .btnAdd(i).Visible = True
                    .btnDelete(i).Visible = True
                    
                    With .lstContacts(i)
                        .Top = ThreeListsPos1
                        .Height = ThreeListsHeight
                        .Visible = True
                        .Visible = True
                    End With
                
                Case 4
                    i = Lists(0)
                    .lblSelContacts(i).Top = FourListsPos4 - LabelSeparation
                    .lstContacts(i).Top = FourListsPos4
                    .btnAdd(i).Top = FourListsPos4 + 15
                    .btnDelete(i).Top = .btnAdd(i).Top + ButtonSeparation
            
                    .lblSelContacts(i).Visible = True
                    .btnAdd(i).Visible = True
                    .btnDelete(i).Visible = True
                    
                    With .lstContacts(i)
                        .Top = FourListsPos4
                        .Height = FourListsHeight
                        .Visible = True
                    End With
                
                    i = Lists(1)
                    .lblSelContacts(i).Top = FourListsPos3 - LabelSeparation
                    .lstContacts(i).Top = FourListsPos3
                    .btnAdd(i).Top = FourListsPos3 + 15
                    .btnDelete(i).Top = .btnAdd(i).Top + ButtonSeparation
            
                    .lblSelContacts(i).Visible = True
                    .btnAdd(i).Visible = True
                    .btnDelete(i).Visible = True
                    
                    With .lstContacts(i)
                        .Top = FourListsPos3
                        .Height = FourListsHeight
                        .Visible = True
                    End With
                
                    i = Lists(2)
                    .lblSelContacts(i).Top = FourListsPos2 - LabelSeparation
                    .lstContacts(i).Top = FourListsPos2
                    .btnAdd(i).Top = FourListsPos2 + 15
                    .btnDelete(i).Top = .btnAdd(i).Top + ButtonSeparation
            
                    .lblSelContacts(i).Visible = True
                    .btnAdd(i).Visible = True
                    .btnDelete(i).Visible = True
                    
                    With .lstContacts(i)
                        .Top = FourListsPos2
                        .Height = FourListsHeight
                        .Visible = True
                    End With
                
                    i = Lists(3)
                    .lblSelContacts(i).Top = FourListsPos1 - LabelSeparation
                    .lstContacts(i).Top = FourListsPos1
                    .btnAdd(i).Top = FourListsPos1 + 15
                    .btnDelete(i).Top = .btnAdd(i).Top + ButtonSeparation
                    
                    .lblSelContacts(i).Visible = True
                    .btnAdd(i).Visible = True
                    .btnDelete(i).Visible = True
                    
                    With .lstContacts(i)
                        .Top = FourListsPos1
                        .Height = FourListsHeight
                        .Visible = True
                    End With
            End Select
        End With
    End If
    
End Function

Public Sub SetUpColumns()
'sets column width and position based on ini settings
    Dim xValue As String
    Dim i As Integer
    Dim sWidth As Single
    Dim iPos As Integer
    Dim iIndex As Integer
    Dim xWidth As String
    
    On Error GoTo ProcError
    For i = 0 To 3
        'get ini value
        xValue = GetUserIni("CIColumns", i)
        If xValue <> Empty Then
            
            'parse into width and position
            iPos = InStr(xValue, ",")
            If iPos > 0 Then
                iIndex = Left(xValue, iPos - 1)
                xWidth = Mid$(xValue, iPos + 1)
                
                'localize to regional setting
                sWidth = xLocalizeNumericString(xWidth)
            End If
            
            With UserControl.lstListings.Columns(i)
                .Order = iIndex
                .Width = sWidth
            End With
        End If
    Next i
    
    'set custom contact grid column width based on ini settings
    'get ini value
    For i = 0 To 2 Step 2
        xValue = GetUserIni("CICustomColumns", i)
        If xValue <> Empty Then
            'localize to regional setting
            sWidth = xLocalizeNumericString(xValue)
            UserControl.grdEntityList.Columns(i).Width = sWidth
        End If
    Next i

    
    Exit Sub
ProcError:
    g_oError.RaiseError "CIX.CI.SetUpColumns"
    Exit Sub
End Sub

Public Sub SaveColumns()
'save column width and position to ini
    Dim i As Integer
    
    On Error GoTo ProcError
    'cycle through columns
    For i = 0 To 3
        With UserControl.lstListings.Columns(i)
            'write settings in form 'Order,width'
            SetUserIni "CIColumns", i, .Order & "," & .Width
        End With
    Next i
    
    For i = 0 To 2 Step 2
        'save custom contact grid column width
        With UserControl.grdEntityList.Columns
            SetUserIni "CICustomColumns", i, .Item(i).Width
        End With
    Next i
    
    Exit Sub
ProcError:
    g_oError.RaiseError "CIX.CI.SaveColumns"
    Exit Sub
End Sub

Public Function SelectNode(Optional ByVal xPath As String) As MSComctlLib.node
'selects folder specified in xPath
'if not supplied, ini default is selected-
'if no ini default specified, first node is selected-
'expands tree as necessary
    Dim iSepPos As Integer
    Dim i As Integer
    Dim xCurLevel As String
    Dim xRemainingLevels As String
    Dim oCurNode As MSComctlLib.node
    Dim oChildNode As MSComctlLib.node
    
    On Error GoTo ProcError
    
    If xPath = Empty Then
'       not supplied - get from ini
        xPath = GetUserIni("CIApplication", ciUserIniKey_DefaultNode)
        If xPath = Empty Then
            'no default path - select first node
            With UserControl.tvFolders
                If .Nodes.Count > 0 Then
                    Set .SelectedItem = .Nodes.Item(1)
                    tvFolders_NodeClick .SelectedItem
                End If
            End With
            Exit Function
        End If
    End If
    
    With UserControl.tvFolders
'       select level while sep exists-
'       expand tree as necessary
        iSepPos = InStr(xPath, "\")
        If iSepPos Then
'           parse xPath
            xCurLevel = Left(xPath, iSepPos - 1)
            xRemainingLevels = Mid(xPath, iSepPos + 1)
        
            For i = 1 To .Nodes.Count
                If .Nodes(i) = xCurLevel Then
                    Set oCurNode = .Nodes(i)
                    oCurNode.Selected = True
                    Exit For
                End If
            Next i
            
            If oCurNode Is Nothing Then
                If UserControl.tvFolders.Nodes.Count > 0 Then
                    UserControl.tvFolders.Nodes.Item(1).Selected = True
                    Set SelectNode = UserControl.tvFolders.SelectedItem
                End If
                
                Err.Raise ciErr_DefaultFolderDoesNotExist
            End If
            
'           expand tree if necessary
            If Not (oCurNode.Children > 0) Then
                ShowTreeBranch oCurNode
            End If
            
            If oCurNode.Children > 0 Then
                oCurNode.Expanded = True
            End If
            
            If oCurNode.Expanded Then
    '           get next separator
                iSepPos = InStr(xRemainingLevels, "\")
            
                While iSepPos
    '               index first child
                    Set oChildNode = oCurNode.Child
                
                    If oChildNode Is Nothing Then
                        Err.Raise ciErr_DefaultFolderDoesNotExist
                    End If
        
    '               parse xPath
                    xCurLevel = Left(xRemainingLevels, iSepPos - 1)
                    xRemainingLevels = Mid(xRemainingLevels, iSepPos + 1)
                
    '               loop through child folders until xCurLevel is found
                    While oChildNode <> xCurLevel
                        Set oChildNode = oChildNode.Next
                        If oChildNode Is Nothing Then
                            Err.Raise ciErr_DefaultFolderDoesNotExist
                        End If
                    Wend
                    
                    Set oCurNode = oChildNode
                    oCurNode.Selected = True
                    
    '               expand tree
                    ShowTreeBranch oCurNode
                
    '               get next separator
                    iSepPos = InStr(xRemainingLevels, "\")
                Wend
            
    '           select final level folder node-
    '           index first child
                Set oChildNode = oCurNode.Child
                oCurNode.Selected = True
                
                If oChildNode Is Nothing Then _
                    Err.Raise ciErr_DefaultFolderDoesNotExist
        
    '           parse xPath- search for trailing NULL chr
                iSepPos = InStr(xRemainingLevels, Chr(0))
                If iSepPos Then
                    xCurLevel = Left(xRemainingLevels, iSepPos - 1)
                Else
                    xCurLevel = xRemainingLevels
                End If
            
    '           loop through child folders until xCurLevel is found
                While oChildNode <> xCurLevel
                    Set oChildNode = oChildNode.Next
                    If oChildNode Is Nothing Then
                        Err.Raise ciErr_DefaultFolderDoesNotExist
                    End If
                Wend
                
                Set oCurNode = oChildNode
            End If
        Else
'           select first level node
            For i = 1 To .Nodes.Count
                If UCase(.Nodes(i)) = UCase(xPath) Then
                    Set oCurNode = .Nodes(i)
                    Exit For
                End If
            Next i
            If oCurNode Is Nothing Then
                Err.Raise ciErr_DefaultFolderDoesNotExist
            End If
        End If
            
'       select node
        If Not (oCurNode Is Nothing) Then
            With oCurNode
                .Selected = True
                tvFolders_NodeClick oCurNode
            End With
        End If
        
        'return selected node
        Set SelectNode = oCurNode
    End With
    Exit Function
    
ProcError:
    Dim lErr As Long
    Dim xDesc As String
    
    lErr = Err.Number
    xDesc = Err.Description
    
    If Err.Number = ciErrs.ciErr_DefaultFolderDoesNotExist Then
        With UserControl.tvFolders
            If Not .SelectedItem Is Nothing Then
                RefreshQuickSearchControls
            End If
        End With
        
        'setup custom menu items
        SetupCustomMenuItems

        MsgBox "The default folder '" & xPath & _
            "' does not exist.  Please reset your default folder.", _
            vbExclamation, g_oSessionType.AppTitle
        Exit Function
    End If
    
    Err.Number = lErr
    Err.Description = xDesc
    g_oError.RaiseError "CIX.CI.SelectNode"
    Exit Function
End Function

Private Sub ShowPanel(iPanel As CIPanels)
'displays the specified panel
    Dim iUNIDType As CIO.ciUNIDTypes
    
    On Error GoTo ProcError
    With UserControl
        If iPanel = ciPanel_folders Then
            .fraFolders.Visible = True
            .fraContacts.Visible = False
            .fraGroups.Visible = False
            .fraListings.Visible = False
            .btnTogglePanels.Caption = "&Show Contacts"
            RaiseEvent PanelChange(True)
            
            'get type of node
            iUNIDType = m_oUNID.GetUNIDType(.tvFolders.SelectedItem.Key)
            
            .btnTogglePanels.Enabled = (iUNIDType = ciUNIDType_Folder)
            
            'enable 'find' button for 1)folders and
            '2)stores that support store searching
            .btnSearch.Enabled = CurrentBackend.IsSearchableEntity( _
                .tvFolders.SelectedItem.Key)
                
            'enable 'find' button for 1)folders and
            '2)stores that support store loading
            .btnTogglePanels.Enabled = CurrentBackend.IsLoadableEntity( _
                .tvFolders.SelectedItem.Key)
                
            .btnEdit.Enabled = False
        ElseIf iPanel = ciPanel_Contacts Then
            m_bShowedContacts = True
            
            'set up for contacts
            .fraFolders.Visible = False
            .fraListings.Visible = True
            .fraContacts.Visible = True
            .fraGroups.Visible = False
            .btnTogglePanels.Caption = "&Show Folders"
            RaiseEvent PanelChange(False)
            .btnTogglePanels.Enabled = True
            .btnSearch.Enabled = True
            .cmdOK.Caption = "&Insert"
            .mnuContacts_AddGroup.Enabled = False
            .mnuContacts_DeleteGroup.Enabled = False
            
            'show contacts frame if listings box is not Wide
            .fraContacts.Visible = (.lstListings.Width = ciListingsNarrow)
        Else
            'set up for groups
            .fraFolders.Visible = False
            .fraListings.Visible = True
            .fraContacts.Visible = False
            .fraGroups.Visible = True
            .btnTogglePanels.Caption = "&Show Folders"
            RaiseEvent PanelChange(False)
            .btnTogglePanels.Enabled = True
            .btnSearch.Enabled = True
            .cmdOK.Caption = "Load Gro&up"
            .btnAddMember.Default = True
            .btnDeleteMember.Enabled = False
            .mnuContacts_AddGroup.Enabled = True
            .mnuContacts_DeleteGroup.Enabled = True
            
            'show groups frame if listings box is not Wide
            .fraGroups.Visible = (.lstListings.Width = ciListingsNarrow)
        End If
        .chkGroups.Enabled = iPanel <> ciPanel_folders
    End With
    UserControl.Refresh
    Exit Sub
ProcError:
    Err.Raise Err.Number, "CIX.CI.ShowPanel", Err.Description
End Sub

Private Sub UserControl_KeyDown(KeyCode As Integer, Shift As Integer)
    Dim l As Long

    If Ambient.UserMode = True Then
        On Error GoTo ProcError
        If IsPressed(KEY_CTL) Then
            If KeyCode = vbKeyF6 Then 'F6
                ShowAbout
            ElseIf KeyCode = vbKeyW And UserControl.lstListings.Visible Then 'w
                ToggleListingsWidth
            ElseIf KeyCode = vbKeyG Then
                If UserControl.mnuContacts_AddGroup.Enabled Then
                    AddGroup
                End If
            ElseIf KeyCode = vbKeyU Then
                If UserControl.mnuContacts_DeleteGroup.Enabled Then
                    DeleteGroup
                End If
            End If
        Else
            If KeyCode = vbKeyF5 Then
                LoadContacts ciSearchType_Refresh
            ElseIf KeyCode = vbKeyF1 Then
                mnuHelp_PDF_Click (0)
            End If
        End If
    End If
    Exit Sub
ProcError:
    g_oError.ShowError
End Sub

Private Sub mnuHelp_PDF_Click(iIndex As Integer)
    Dim xFirstHelpDoc As String
    
    On Error Resume Next
    xFirstHelpDoc = g_xHelpFiles(0)
    On Error GoTo 0
    
    If xFirstHelpDoc <> "" Then
        LaunchDocumentByExtension g_xHelpPath & "\" & g_xHelpFiles(iIndex) & ".pdf"
    End If
End Sub



Private Sub ShowAbout()
    Dim oForm As frmAbout
    On Error GoTo ProcError
    Set oForm = New frmAbout
    
    With oForm
        .Show vbModal
    End With
    
    Set oForm = Nothing
    Exit Sub
ProcError:
    g_oError.RaiseError "CIX.CI.ShowAbout"
    Exit Sub
End Sub

Private Sub UserControl_Terminate()
    On Error GoTo ProcError
    Set m_oListings = Nothing
    Set m_oAddresses = Nothing
    Set m_oContacts = Nothing
    Set m_oUNID = Nothing
    Set m_oEntityList = Nothing
    Set m_oEvents = Nothing
    If Not g_oGlobals Is Nothing Then
        g_oGlobals.Objects.Remove xObjectID
        DebugPrint "Removed - " & xObjectID & ": " & g_oGlobals.Objects.Count
    End If
    Exit Sub
ProcError:
    g_oError.ShowError
    Exit Sub
End Sub

Sub ToggleListingsWidth()
'toggles the width of the left-side list box
    On Error GoTo ProcError
    Dim bEnable As Boolean
    Dim i As Integer
    Dim sRightBorder As Single
    Dim sWide As Single

    With UserControl
        sRightBorder = .fraContacts.Left + .lstContacts(0).Left + .lstContacts(0).Width
        sWide = sRightBorder - .lstListings.Left
            
        If .lstListings.Width = sWide Then
            .lstListings.Width = ciListingsNarrow
            .mnuContacts_ToggleContactList.Caption = "Expand Available Contact &List" '     (Ctrl+w)"
        Else
            .lstListings.Width = sWide
            .mnuContacts_ToggleContactList.Caption = "Collapse Available Contact &List" '     (Ctrl+w)"
        End If
        
        bEnable = (.lstListings.Width = ciListingsNarrow)
        
        If UserControl.chkGroups.Value = vbUnchecked Then
            'show/hide contacts frame
            .fraContacts.Visible = bEnable
            .fraGroups.Visible = False
        Else
            'show/hide groups frame
            .fraGroups.Visible = bEnable
            .fraContacts.Visible = False
        End If
        
        UserControl.chkGroups.Enabled = bEnable
    End With
    DoEvents
    Exit Sub
ProcError:
    g_oError.RaiseError "CIX.CI.ToggleListingsWidth"
    Exit Sub
End Sub

Sub ClearContacts()
'clears the contacts chosen - clears lists
    On Error GoTo ProcError
    
    If Not m_oContacts Is Nothing Then
        m_oContacts.DeleteAll
    End If
    
    With UserControl.lstContacts
        .Item(0).Clear
        .Item(1).Clear
        .Item(2).Clear
        .Item(3).Clear
    End With
    
    With UserControl.lstListings
        On Error Resume Next
        .ClearSelCols
        .Row = 0
        On Error GoTo ProcError
    End With
    Exit Sub
ProcError:
    g_oError.RaiseError "CIX.CI.ClearContacts"
    Exit Sub
End Sub

Public Function GroupsBackend() As ICIBackend
'returns the groups backend
    On Error GoTo ProcError
    Set GroupsBackend = mdlMain.Backends.GroupsBackend
    Exit Function
ProcError:
    g_oError.RaiseError "CIX.CI.Groups"
    Exit Function
End Function

Public Sub OnSelListChange(iListIndex As Integer)
    Dim i As Integer
    Dim j As Integer
    
    On Error GoTo ProcError
    With UserControl.lstContacts
'       deselect all entries, disable
'       delete buttons, and format controls
'       in inactive lists
        For i = .LBound To .UBound
            If i <> iListIndex Then
                For j = 0 To .Item(i).ListCount - 1
                    .Item(i).Selected(j) = False
                Next j
                UserControl.lblSelContacts.Item(i).ForeColor = vbBlack
                UserControl.btnDelete(i).Enabled = False
            End If
        Next i
    End With
    
'   set properties of active list controls
    UserControl.btnAdd(iListIndex).Default = True
    
'   if multiple lists are visible, make active one blue
    Select Case Me.SelectionLists
        Case ciSelectionList_To, _
             ciSelectionList_From, _
             ciSelectionList_CC, _
             ciSelectionList_BCC
            UserControl.lblSelContacts.Item(iListIndex).ForeColor = vbBlack
        Case Else
            UserControl.lblSelContacts.Item(iListIndex).ForeColor = vbBlue
    End Select
    
'   select first item in list if no selections exist
    With UserControl.lstContacts(iListIndex)
        If .SelCount = 0 And .ListCount Then
            .Selected(0) = True
        End If
    End With
    
'   enable delete button if an item is selected
    UserControl.btnDelete(iListIndex).Enabled = _
            UserControl.lstContacts(iListIndex).ListCount
    Exit Sub
ProcError:
    g_oError.RaiseError "CIX.CI.OnSelListChange"
    Exit Sub
End Sub

Private Sub SetupForInputState(ByVal bIsReadyForInput As Boolean)
    On Error GoTo ProcError
    With UserControl
'        .Enabled = bIsReadyForInput
        .fraContacts.Enabled = bIsReadyForInput
        .fraFolders.Enabled = bIsReadyForInput
        .fraGroups.Enabled = bIsReadyForInput
        .fraListings.Enabled = bIsReadyForInput
        .fraToolbar.Enabled = bIsReadyForInput
        .cmdCancel.Cancel = bIsReadyForInput
        .cmdCancel.Enabled = bIsReadyForInput
    End With
    Exit Sub
ProcError:
    g_oError.RaiseError "CIX.CI.SetupForInputState"
    Exit Sub
End Sub
