VERSION 5.00
Begin VB.Form frmListingDetail 
   BackColor       =   &H00E0E0E0&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Contact Detail"
   ClientHeight    =   4035
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6465
   FillColor       =   &H00C0FFFF&
   FillStyle       =   0  'Solid
   BeginProperty Font 
      Name            =   "MS Sans Serif"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmListingDetailOLD.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4035
   ScaleWidth      =   6465
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.VScrollBar VScroll1 
      Height          =   3030
      Left            =   1920
      TabIndex        =   13
      Top             =   960
      Width           =   195
   End
   Begin VB.OptionButton optAddress 
      BackColor       =   &H00E0E0E0&
      Caption         =   "Option1"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   420
      Index           =   0
      Left            =   120
      TabIndex        =   10
      Top             =   960
      Visible         =   0   'False
      Width           =   2025
   End
   Begin VB.OptionButton optAddress 
      BackColor       =   &H00E0E0E0&
      Caption         =   "Option1"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   420
      Index           =   8
      Left            =   120
      TabIndex        =   9
      Top             =   3555
      Visible         =   0   'False
      Width           =   2025
   End
   Begin VB.OptionButton optAddress 
      BackColor       =   &H00E0E0E0&
      Caption         =   "Option1"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   420
      Index           =   7
      Left            =   120
      TabIndex        =   8
      Top             =   3235
      Visible         =   0   'False
      Width           =   2025
   End
   Begin VB.OptionButton optAddress 
      BackColor       =   &H00E0E0E0&
      Caption         =   "Option1"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   420
      Index           =   6
      Left            =   120
      TabIndex        =   7
      Top             =   2910
      Visible         =   0   'False
      Width           =   2025
   End
   Begin VB.OptionButton optAddress 
      BackColor       =   &H00E0E0E0&
      Caption         =   "Option1"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   420
      Index           =   5
      Left            =   120
      TabIndex        =   6
      Top             =   2585
      Visible         =   0   'False
      Width           =   2025
   End
   Begin VB.OptionButton optAddress 
      BackColor       =   &H00E0E0E0&
      Caption         =   "Option1"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   420
      Index           =   4
      Left            =   120
      TabIndex        =   5
      Top             =   2260
      Visible         =   0   'False
      Width           =   2025
   End
   Begin VB.OptionButton optAddress 
      BackColor       =   &H00E0E0E0&
      Caption         =   "Option1"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   420
      Index           =   3
      Left            =   120
      TabIndex        =   4
      Top             =   1935
      Visible         =   0   'False
      Width           =   2025
   End
   Begin VB.OptionButton optAddress 
      BackColor       =   &H00E0E0E0&
      Caption         =   "Option1"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   420
      Index           =   2
      Left            =   120
      TabIndex        =   3
      Top             =   1610
      Visible         =   0   'False
      Width           =   2025
   End
   Begin VB.OptionButton optAddress 
      BackColor       =   &H00E0E0E0&
      Caption         =   "Option1"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   420
      Index           =   1
      Left            =   120
      TabIndex        =   2
      Top             =   1285
      Visible         =   0   'False
      Width           =   2025
   End
   Begin VB.CommandButton btnClose 
      BackColor       =   &H00E0E0E0&
      Cancel          =   -1  'True
      Caption         =   "Close"
      Default         =   -1  'True
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   390
      Left            =   5055
      MaskColor       =   &H00C0FFFF&
      TabIndex        =   0
      Top             =   3510
      Width           =   1215
   End
   Begin VB.Image imgCopySummary 
      Appearance      =   0  'Flat
      Height          =   240
      Left            =   6105
      Picture         =   "frmListingDetailOLD.frx":030A
      ToolTipText     =   "Copy Contact Summary (Ctrl + C)"
      Top             =   120
      Width           =   255
   End
   Begin VB.Label lblAddresses 
      BackStyle       =   0  'Transparent
      Caption         =   "Addresses:"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   270
      Left            =   150
      TabIndex        =   12
      Top             =   645
      Width           =   1875
   End
   Begin VB.Label lblHeading 
      BackColor       =   &H00FFFFFF&
      BackStyle       =   0  'Transparent
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   315
      Left            =   165
      TabIndex        =   11
      Top             =   180
      Width           =   5865
   End
   Begin VB.Label lblDetail 
      BackStyle       =   0  'Transparent
      Height          =   2535
      Left            =   2400
      TabIndex        =   1
      Top             =   660
      Width           =   3780
   End
   Begin VB.Shape Shape2 
      BackColor       =   &H00FFFFFF&
      BackStyle       =   1  'Opaque
      BorderStyle     =   0  'Transparent
      Height          =   4155
      Left            =   2145
      Top             =   -30
      Width           =   4500
   End
End
Attribute VB_Name = "frmListingDetail"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private m_oListing As CIO.CListing
Private m_oAddresses As CIO.CAddresses
Private m_bPromptForMissingPhones As Boolean
Private m_bCanceled As Boolean

Public Property Get Canceled() As Boolean
    Canceled = m_bCanceled
End Property

Public Property Let AddressIndex(iNew As Integer)
    If iNew = -1 Then iNew = 0
    Me.optAddress.Item(iNew).Value = True
End Property

Public Property Get AddressIndex() As Integer
    Dim i As Integer
    On Error GoTo ProcError
    AddressIndex = -1
    For i = 0 To 8
        If Me.optAddress.Item(i).Value = True Then
            AddressIndex = i
            Exit For
        End If
    Next i
    Exit Property
ProcError:
    RaiseError "CIX.frmListingDetail.AddressIndex"
    Exit Property
End Property

Public Property Set Addresses(oAddresses As CIO.CAddresses)
    Set m_oAddresses = oAddresses
End Property

Public Property Get Addresses() As CIO.CAddresses
    Set Addresses = m_oAddresses
End Property

Public Property Set Listing(oNew As CIO.CListing)
    Set m_oListing = oNew
End Property

Public Property Get Listing() As CIO.CListing
    Set Listing = m_oListing
End Property

Private Sub btnClose_Click()
    m_bCanceled = False
    Me.Hide
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set m_oListing = Nothing
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    Me.Hide
End Sub

Private Sub Form_Load()
    Dim oAddress As CAddress
'   get addresses from listing
    On Error GoTo ProcError
    
    m_bCanceled = True
    
    If m_oAddresses Is Nothing Then
        'addresses have not already been specified -
        'retrieve from listing
        Set m_oAddresses = GetBackendFromID(Me.Listing.BackendID) _
                            .GetAddresses(Me.Listing)
    End If
                            
    If m_oAddresses Is Nothing Then
'       couldn't get addresses from listing - error
        Err.Raise ciErr_InvalidUNID, , _
            "Invalid address object from listing."
    End If
        
    If m_oAddresses.Count = 0 Then
        ShowListingDetail
        Me.lblAddresses.Caption = "No Addresses"
    Else
        'add addresses to list box
        Dim i As Integer
        For i = 0 To Min(m_oAddresses.Count - 1, Me.optAddress.Count - 1)
            With Me.optAddress
                .Item(i).Visible = True
                .Item(i).Caption = "&" & m_oAddresses.Item(i + 1).Name
            End With
        Next i
        
        If m_oAddresses.Count > Me.optAddress.Count Then
            'there are more addresses to display - show scroll bar
            
        Me.optAddress.Item(0).Value = True
    End If
    Exit Sub
ProcError:
    ShowError
    Exit Sub
End Sub

Private Sub ShowListingDetail()
'displays detail form with detail for
'specified listing with selected address
    Dim oAddress As CIO.CAddress
    Dim iIndex As Integer
    Dim oL As CIO.CListing
    Dim oContact As CIO.CContact
    Dim xTemp As String
    Dim oB As CIO.ICIBackend
    
'   get address type from dlg
    On Error GoTo ProcError
    iIndex = Me.AddressIndex
    
    Me.MousePointer = vbHourglass
    Set oL = Me.Listing
    Set oB = GetBackendFromID(oL.BackendID)
        
    If iIndex > -1 Then
        If iIndex > m_oAddresses.Count Then
            iIndex = 0
        End If
        Set oAddress = m_oAddresses.Item(iIndex + 1)
    End If
    
    Set oContact = oB.GetContacts(oL, oAddress, , _
              ciRetrieveData_Names + ciRetrieveData_Addresses, _
             ciAlert_None).Item(1)
         
    With oContact
'           build detail string
        If .Prefix <> "" Then _
            xTemp = xTemp & .Prefix
        If .FirstName <> "" Then _
            xTemp = xTemp & " " & .FirstName
        If .MiddleName <> "" Then _
            xTemp = xTemp & " " & .MiddleName
        If .LastName <> "" Then _
            xTemp = xTemp & " " & .LastName
        If .Suffix <> "" Then _
            xTemp = xTemp & " " & .Suffix
        If xTemp <> "" Then _
            xTemp = xTemp & vbCrLf
            
        Dim xAddrType As String
        xAddrType = UCase(.AddressTypeName)
        
        If xAddrType Like "BUSINESS" Or _
            xAddrType Like "OFFICE" Or xAddrType Like "WORK" Or _
            xAddrType Like "MAIN" Or xAddrType Like "DIRECT" Or _
            xAddrType Like "MAILING" Or xAddrType = Empty Then
            If .Title <> Empty Then _
                xTemp = xTemp & .Title & vbCrLf
                
            If .Company <> Empty Then _
                xTemp = xTemp & .Company & vbCrLf
        End If
        
        If .CoreAddress <> Empty Then
            'use core address if there is one
            xTemp = xTemp & .CoreAddress
        Else
            If .Street1 <> "" Then _
                xTemp = xTemp & .Street1 & vbCrLf
            If .Street2 <> "" Then _
                xTemp = xTemp & .Street2 & vbCrLf
            If .Street3 <> "" Then _
                xTemp = xTemp & .Street3 & vbCrLf
            If .AdditionalInformation <> "" Then _
                xTemp = xTemp & .AdditionalInformation & vbCrLf
            If .City <> "" Then _
                xTemp = xTemp & .City
            If .State <> "" Then _
                xTemp = xTemp & ", " & .State
            If .ZipCode <> "" Then _
                xTemp = xTemp & " " & .ZipCode
            If .Country <> "" Then _
                xTemp = xTemp & vbCrLf & .Country
        End If
        
        If xTemp <> "" Then _
            xTemp = xTemp & vbCrLf & vbCrLf
            
        xTemp = xTemp & String(ciMax(9 - lCountChrs(xTemp, vbCrLf), 0), vbCrLf)
        Dim oNums As CIO.CContactNumbers
        
        If oContact.AddressTypeID = Empty Then
            Set oNums = oB.GetPhoneNumbers(oL)
        Else
            Set oNums = oB.GetPhoneNumbers(oL, oContact.AddressTypeID)
        End If
        
        If oNums.Count = 0 Then
            xTemp = xTemp & "Phone:  " & vbCrLf
        ElseIf oNums.Count = 1 Then
'               display phone if there is exactly one
            With oNums.Item(1)
                xTemp = xTemp & "Phone:  " & .Number
                xTemp = xTemp & IIf(.Extension <> Empty, _
                    "  x" & .Extension, "")
            End With
            xTemp = xTemp & vbCrLf
        Else
'               display "multiple numbers"
            xTemp = xTemp & "Phone:  Multiple Numbers" & vbCrLf
        End If
        
        If oContact.AddressTypeID = Empty Then
            Set oNums = oB.GetFaxNumbers(oL)
        Else
            Set oNums = oB.GetFaxNumbers(oL, oContact.AddressTypeID)
        End If
        
        If oNums.Count = 0 Then
            xTemp = xTemp & "Fax:  " & vbCrLf
        ElseIf oNums.Count = 1 Then
            xTemp = xTemp & "Fax:  " & oNums.Item(1).Number & vbCrLf
        Else
            xTemp = xTemp & "Fax:  Multiple Numbers" & vbCrLf
        End If
        
        If oContact.AddressTypeID = Empty Then
            Set oNums = oB.GetEMailNumbers(oL)
        Else
            Set oNums = oB.GetEMailNumbers(oL, oContact.AddressTypeID)
        End If
        
        If oNums.Count = 0 Then
            xTemp = xTemp & "E-Mail:  " & vbCrLf
        ElseIf oNums.Count = 1 Then
            xTemp = xTemp & "E-Mail:  " & oNums.Item(1).Number & vbCrLf
        Else
            xTemp = xTemp & "E-Mail:  Multiple Numbers" & vbCrLf
        End If
    End With

'   set and show form - ensure "&" appear as "&", not as hotkeys
    With Me
        .lblHeading.Caption = Replace(oContact.DisplayName, "&", "&&")
        .lblDetail.Caption = Trim$(Replace(xTemp, "&", "&&"))
        DoEvents
    End With
    Me.MousePointer = vbDefault
    Exit Sub
ProcError:
    Me.MousePointer = vbDefault
    RaiseError "CIX.frmListingDetail.ShowListingDetail"
    Exit Sub
End Sub

Private Sub lstAddressTypes_DblClick()
'do what happens when OK is clicked
    On Error GoTo ProcError
    btnClose_Click
    Exit Sub
ProcError:
    g_oError.ShowError
    Exit Sub
End Sub

Private Sub imgCopySummary_Click()
    Dim xSummary As String
    
    On Error GoTo ProcError
    xSummary = Me.lblDetail.Caption

    'remove all double paragraphs
    While xSummary Like "*" & vbCrLf & vbCrLf & "*"
        xSummary = Replace(xSummary, vbCrLf & vbCrLf, vbCrLf)
    Wend
    
    While xSummary Like "*" & vbCr & vbCr & "*"
        xSummary = Replace(xSummary, vbCr & vbCr, vbCrLf)
    Wend
    
    With VB.Clipboard
        .Clear
        .SetText Trim$(xSummary)
    End With
    Exit Sub
ProcError:
    ShowError
    Exit Sub
End Sub

Private Sub imgCopySummary_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Me.imgCopySummary.BorderStyle = 1
End Sub

Private Sub imgCopySummary_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Me.imgCopySummary.BorderStyle = 0
End Sub

Private Sub optAddress_Click(Index As Integer)
    On Error GoTo ProcError
    ShowListingDetail
    Exit Sub
ProcError:
    g_oError.ShowError
    Exit Sub
End Sub
