VERSION 5.00
Begin VB.Form frmAddGroup 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   " Add Group"
   ClientHeight    =   1410
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   4650
   Icon            =   "frmAddGroup.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1410
   ScaleWidth      =   4650
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CommandButton txtCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   400
      Left            =   3390
      TabIndex        =   3
      Top             =   915
      Width           =   1140
   End
   Begin VB.CommandButton btnOK 
      Caption         =   "O&K"
      Default         =   -1  'True
      Enabled         =   0   'False
      Height          =   400
      Left            =   2190
      TabIndex        =   2
      Top             =   915
      Width           =   1140
   End
   Begin VB.TextBox txtGroupName 
      Appearance      =   0  'Flat
      Height          =   330
      Left            =   135
      MaxLength       =   100
      TabIndex        =   1
      Top             =   375
      Width           =   4410
   End
   Begin VB.Label Label1 
      Caption         =   "&Name:"
      Height          =   195
      Left            =   165
      TabIndex        =   0
      Top             =   165
      Width           =   510
   End
End
Attribute VB_Name = "frmAddGroup"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private m_xGroupName As String
Private m_bCanceled As Boolean

Public Property Get Canceled() As Boolean
    Canceled = m_bCanceled
End Property

Public Property Get GroupName() As String
    GroupName = m_xGroupName
End Property

Public Property Let GroupName(xNew As String)
    m_xGroupName = xNew
End Property

Private Sub btnOK_Click()
    Me.GroupName = Me.txtGroupName
    m_bCanceled = False
    Me.Hide
    DoEvents
End Sub

Private Sub Form_Load()
    m_bCanceled = True
    
    
End Sub

Private Sub txtCancel_Click()
    Me.GroupName = Empty
    m_bCanceled = True
    Me.Hide
    DoEvents
End Sub

Private Sub txtGroupName_Change()
'   enable OK btn only when there
'   is text in the Name txt box
    If txtGroupName.Text = "" Then
        btnOK.Enabled = False
    Else
        btnOK.Enabled = True
    End If
End Sub

'Private Function bNameIsValid() As Boolean
''returns true if name is not duped -
''no other validation is necessary
'
'    Dim i As Integer
'
'    With frmContacts.cbxGroups
'        For i = 0 To .ListCount - 1
'            If UCase(Me.txtGroupName) = UCase(.List(i)) Then
'                Exit Function
'            End If
'        Next i
'    End With
'
'    bNameIsValid = True
'End Function

Private Sub txtGroupName_GotFocus()
    With Me.txtGroupName
        .SelStart = 0
        .SelLength = Len(.Text)
    End With
End Sub
