VERSION 5.00
Begin VB.Form frmAbout 
   Appearance      =   0  'Flat
   BackColor       =   &H80000005&
   BorderStyle     =   1  'Fixed Single
   ClientHeight    =   4935
   ClientLeft      =   225
   ClientTop       =   1185
   ClientWidth     =   8025
   ClipControls    =   0   'False
   ControlBox      =   0   'False
   Icon            =   "frmAbout.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form2"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4935
   ScaleWidth      =   8025
   StartUpPosition =   2  'CenterScreen
   Begin VB.PictureBox pctTSGLogo_small 
      Appearance      =   0  'Flat
      AutoSize        =   -1  'True
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   420
      Left            =   4005
      Picture         =   "frmAbout.frx":030A
      ScaleHeight     =   420
      ScaleWidth      =   420
      TabIndex        =   11
      Top             =   3870
      Width           =   420
   End
   Begin VB.PictureBox pctCFLogo 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   3540
      Left            =   150
      Picture         =   "frmAbout.frx":07E9
      ScaleHeight     =   3540
      ScaleWidth      =   3600
      TabIndex        =   10
      Top             =   180
      Width           =   3600
   End
   Begin VB.PictureBox pctTSGLogo 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   3540
      Left            =   150
      Picture         =   "frmAbout.frx":2A1D3
      ScaleHeight     =   3540
      ScaleWidth      =   3600
      TabIndex        =   6
      Top             =   180
      Width           =   3600
   End
   Begin VB.PictureBox pctClose 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   270
      Left            =   7710
      Picture         =   "frmAbout.frx":53BBD
      ScaleHeight     =   270
      ScaleWidth      =   285
      TabIndex        =   5
      Top             =   90
      Width           =   285
   End
   Begin VB.PictureBox pctTSGTagline 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   390
      Left            =   240
      Picture         =   "frmAbout.frx":53F89
      ScaleHeight     =   390
      ScaleWidth      =   4305
      TabIndex        =   4
      Top             =   4470
      Width           =   4305
   End
   Begin VB.PictureBox pctTSGInc 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   450
      Left            =   195
      Picture         =   "frmAbout.frx":54957
      ScaleHeight     =   450
      ScaleWidth      =   3870
      TabIndex        =   3
      Top             =   4050
      Width           =   3870
   End
   Begin VB.Label lblCompatibility2 
      Alignment       =   2  'Center
      BackColor       =   &H00FFFFFF&
      Caption         =   "Office 2016, 2013, 365 ProPlus, and 2010"
      BeginProperty Font 
         Name            =   "Gill Sans MT"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   372
      Left            =   3552
      TabIndex        =   9
      Top             =   3420
      Width           =   4536
   End
   Begin VB.Label lblCompatibility1 
      Alignment       =   2  'Center
      BackColor       =   &H00FFFFFF&
      Caption         =   "Compatible with 32-bit versions of"
      BeginProperty Font 
         Name            =   "Gill Sans MT"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   300
      Left            =   3552
      TabIndex        =   8
      Top             =   3156
      Width           =   4536
   End
   Begin VB.Label lblVersion 
      Alignment       =   2  'Center
      BackColor       =   &H00FFFFFF&
      Caption         =   "Version automatic"
      BeginProperty Font 
         Name            =   "Gill Sans MT"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   405
      Left            =   3555
      TabIndex        =   2
      Top             =   2130
      Width           =   4530
   End
   Begin VB.Label lblCopyright 
      Alignment       =   2  'Center
      BackColor       =   &H00FFFFFF&
      Caption         =   "�1990 - 2017"
      BeginProperty Font 
         Name            =   "Gill Sans MT"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   3555
      TabIndex        =   0
      Top             =   2625
      Width           =   4530
   End
   Begin VB.Label lblAppName1 
      Alignment       =   2  'Center
      BackColor       =   &H00FFFFFF&
      Caption         =   "Contact"
      BeginProperty Font 
         Name            =   "Gill Sans MT"
         Size            =   36
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   930
      Left            =   3555
      TabIndex        =   1
      Top             =   300
      Width           =   4530
   End
   Begin VB.Label lblAppName2 
      Alignment       =   2  'Center
      BackColor       =   &H00FFFFFF&
      Caption         =   "Integration"
      BeginProperty Font 
         Name            =   "Gill Sans MT"
         Size            =   36
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1020
      Left            =   3540
      TabIndex        =   7
      Top             =   975
      Width           =   4530
   End
End
Attribute VB_Name = "frmAbout"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private m_xMP90Ver As String

Public Property Let MacPac90Version(xNew As String)
    m_xMP90Ver = xNew
End Property

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    If KeyCode = vbKeyEscape Then
        Unload Me
    End If
End Sub

Private Sub Form_Load()
    If g_oSessionType.SessionType = ciSession_Connect Then
        Me.pctCFLogo.Visible = True
        Me.pctTSGLogo_small.Visible = True
        Me.pctTSGLogo.Visible = False
        Me.lblAppName1.Caption = "Connect"
        Me.lblAppName2.Caption = "Forms"
    Else
        Me.pctCFLogo.Visible = False
        Me.pctTSGLogo_small.Visible = False
        Me.pctTSGLogo.Visible = True
    End If
    On Error Resume Next
    Me.lblVersion.Caption = "Version " & App.Major & "." & App.Minor & "." & App.Revision
End Sub

Private Sub pctTSGLogo_DblClick()
    MsgBox "ProgramDirectory Dir - " & App.Path
End Sub

Private Sub pctClose_Click()
    Unload Me
End Sub
