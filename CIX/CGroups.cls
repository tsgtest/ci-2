VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CGroups"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Implements ICIBackend

Private Const ciGroupsInternalID As Integer = 99

Private m_iID As Integer
Private m_bConnected As Boolean
Private m_bExists As Boolean
Private m_oCnn As ADODB.Connection
Private m_oFilter As CIO.CFilter
Private m_oEvents As CIO.CEventGenerator
Private m_oError As CIO.CError
Private m_iSortCol As Integer

'**********************************************************
'initializer/terminator
'**********************************************************
Dim xObjectID As String
Private Sub Class_Initialize()
    Math.Randomize
    xObjectID = "CGroups" & Format(Now, "hhmmss") & Math.Rnd()
    g_oGlobals.Objects.Add xObjectID, xObjectID
    DebugPrint "Added - " & xObjectID & ": " & g_oGlobals.Objects.Count
    Set m_oEvents = g_oGlobals.CIEvents()
    Set m_oError = New CIO.CError
End Sub

Private Sub Class_Terminate()
    g_oGlobals.Objects.Remove xObjectID
    DebugPrint "Removed - " & xObjectID & ": " & g_oGlobals.Objects.Count
    Set m_oCnn = Nothing
    Set m_oEvents = Nothing
    Set m_oError = Nothing
    Set m_oFilter = Nothing
End Sub
'**********************************************************

Public Property Get ID() As Integer
    ID = m_iID
End Property

Public Sub AddGroup(ByVal xName As String)
    Dim iRecs As Integer
    
    On Error GoTo ProcError
    
    If Not ICIBackend_IsConnected Then
        Connect
    End If
    
    m_oCnn.Execute "INSERT INTO tblContactGroups (fldName) VALUES ('" & xName & "')", iRecs

    Exit Sub
ProcError:
    g_oError.RaiseError "CIX.CGroups.AddGroup"
    Exit Sub
End Sub

Public Sub DeleteGroup(ByVal lID As Long)
    On Error GoTo ProcError
    With m_oCnn
        'delete both the members of the group and the group itself
        .Execute "DELETE * FROM tblContactGroupMembers WHERE fldGroup =" & lID
        .Execute "DELETE * FROM tblContactGroups WHERE fldID =" & lID
    End With
    Exit Sub
ProcError:
    g_oError.RaiseError "CIX.CGroups.DeleteGroup"
    Exit Sub
End Sub

Public Function AddMember(oContact As CIO.CContact, ByVal fldGroupID As Long) As Long
'adds contact as member of specified group - returns the ID of member record
    Dim xSQL As String
    Dim lRecs As Long
    Dim xDisplayName As String
    
    With oContact
        xSQL = "INSERT INTO tblContactGroupMembers(fldGroup,fldUNID,fldDisplayName," & _
            "fldCompany,fldLastName,fldFirstName,fldTitle,fldAddressType,fldAddressTypeName) VALUES(" & _
            fldGroupID & ",""" & .UNID & """,""" & .DisplayName & """,""" & .Company & """,""" & _
            .LastName & """,""" & .FirstName & """,""" & .Title & """,""" & .AddressTypeID & """,""" & .AddressTypeName & """)"
            
        On Error Resume Next
        m_oCnn.Execute xSQL, lRecs
        On Error GoTo ProcError
        
        If lRecs = 0 Then
            Err.Raise ciErrs.ciErr_CouldNotAddToCollection, , "Could not add the " & _
                "contact with display name '" & .DisplayName & "' to the group."
        End If
        
        'get ID of added record
        xSQL = "SELECT TOP 1 fldID FROM tblContactGroupMembers ORDER BY fldID DESC"
        Dim oRS As ADODB.Recordset
        Set oRS = New ADODB.Recordset
        oRS.Open xSQL, m_oCnn, adOpenForwardOnly, adLockReadOnly
        
        'return ID of newest record
        AddMember = oRS.Fields(0)
    End With
    Exit Function
ProcError:
    g_oError.RaiseError "CIX.CGroups.AddMember"
End Function

Public Sub DeleteMember(ByVal lID As Long)
    Dim xSQL As String
    Dim lNumAffected As Long
    
    On Error GoTo ProcError
    
    xSQL = "DELETE FROM tblContactGroupMembers WHERE fldID=" & lID
    m_oCnn.Execute xSQL, lNumAffected
    If lNumAffected = 0 Then
        Err.Raise ciErrs.ciErr_CouldNotExecuteSQL, , "Could not delete " & _
            "the contact with ID=" & lID
    End If
    Exit Sub
ProcError:
    g_oError.RaiseError "CIX.CGroups.DeleteMember"
End Sub

Public Function GetMembers(ByVal lGroupID As Long) As CIO.CListings
'returns the collection of listings that match the filter criteria and
'are stored in the specified folder
    Dim oRS As ADODB.Recordset
    Dim oListings As CIO.CListings
    Dim i As Long
    Dim lRecs As Long
    Dim Cancel As Boolean
    Dim xSQL As String
    Dim xFilterSort As String
    Dim oConst As CIO.CConstants
    
    On Error GoTo ProcError
    
'   get new listings
    Set oListings = New CIO.CListings
    Set oConst = New CIO.CConstants
    
    xSQL = "SELECT fldDisplayName & '  (' & iif(fldAddressTypeName=''," & _
        "'No Address',fldAddressTypeName) & ')' as " & _
        "fldDisplayName,fldID,fldCompany,fldFirstName,fldLastName,fldTitle," & _
        "fldAddressTypeName,fldAddressType FROM tblContactGroupMembers WHERE fldGroup=" & lGroupID & _
        " ORDER BY fldDisplayName"
    
    'get member records
    Set oRS = m_oCnn.Execute(xSQL, lRecs)

    If Not (oRS.BOF And oRS.EOF) Then
'       create a new listing for each record returned
        If Not Cancel Then
            oRS.MoveFirst
            While Not oRS.EOF
                AddListing oRS, oListings, ciGroupsInternalID & oConst.UNIDSep & _
                    oConst.UNIDSep & CStr(lGroupID), False
                i = i + 1
                If Cancel Then
                    Set GetMembers = oListings
                    Exit Function
                End If
                oRS.MoveNext
            Wend
        End If
    Else
'       return empty listings collection
        Set oListings = New CIO.CListings
    End If
    
    Set GetMembers = oListings
    Exit Function
ProcError:
    Err.Raise Err.Number, "CIX.CGroups.GetMembers", Err.Description
    Exit Function
End Function

Private Sub CreateFilter()
'creates an empty filter for backend based on ini definition
    Dim xKey As String
    Dim iPos As Integer
    Dim i As Integer
    Dim lCol As CIO.ciListingCols
    On Error GoTo ProcError
'   create empty filter
    Set m_oFilter = New CIO.CFilter
    
    For i = 1 To 4
'       get ini key for filter field of this backend
        xKey = Trim$(g_oIni.GetIni("Backend" & m_iID, "Filter" & i, g_oIni.CIIni))
        
        If xKey = Empty Then
            Exit For
        End If
            
'       search for ',' - this delimits name from id in key
        iPos = InStr(xKey, ",")
        If iPos = 0 Or iPos = 1 Or iPos = Len(xKey) Then
'           no delimiter present, no name specified, or no id specified
            Err.Raise ciErr_MissingOrInvalidINIKey, , _
                "Key 'Backend" & m_iID & "\FilterFields" & _
                    CStr(i) & "' in CI.ini has an invalid value."
        End If
        
        With m_oFilter.FilterFields(i)
'           get name, id, operator of filter field
            .Name = Left(xKey, iPos - 1)
            .ID = Mid(xKey, iPos + 1)
            .Operator = ciSearchOperator_Contains And _
                ciSearchOperator_BeginsWith And ciSearchOperator_Equals
        End With
    Next
    
'   get sort column
    On Error Resume Next
    lCol = CLng(g_oIni.GetIni("CIApplication", "Sort", g_oIni.CIUserIni))
    On Error GoTo ProcError
    
    If Err.Number > 0 Then
        lCol = ciListingCols_DisplayName
    End If
    On Error GoTo ProcError
    
    m_oFilter.SortColumn = lCol
    Exit Sub
ProcError:
    g_oError.RaiseError "CIX.CGroups.CreateFilter"
End Sub

Private Sub Connect()
'connects to the groups db
    Dim xPath As String
    Dim xAppPath As String
    Dim xUserIni As String
    
    On Error GoTo ProcError
    'get path to db - if ci.ini is empty see if
    'mpPrivate exists - if so, use it for connection
    xPath = g_oIni.GetIni("Backend" & m_iID, "DB", g_oIni.CIIni)
    
    If xPath = Empty Then
        xPath = g_oIni.MacPacUserFilesDir & "mpPrivate.mdb"
        
        xPath = GetEnvironVarPath(xPath)
        
        If Dir(xPath) = Empty Then
            'use default path
            xPath = g_oIni.ApplicationDirectory & "\ci.mdb"
        End If
    Else
        xPath = GetEnvironVarPath(xPath)
'        'this line should copy files from Tools\UserCache if necessary
'        xUserIni = g_oIni.CIUserIni
    End If
    
    If Dir(xPath) = Empty Then
        'groups db file is missing
        Err.Raise ciErrs.ciErr_MissingFile, , "'" & xPath & _
            "' could not be found.  Please contact your administrator."
    End If
    
    'create connection
    Set m_oCnn = New ADODB.Connection
    m_oCnn.ConnectionString = "Provider=Microsoft.Jet.OLEDB.4.0;" & _
        "User ID=Admin;Data Source=" & xPath
        
    'open connection
    m_oCnn.Open
    
    'create 'groups' tables if necessary
    CreateTablesIfNecessary
    Exit Sub
ProcError:
    g_oError.RaiseError "CIX.CGroups.Connect"
    Exit Sub
End Sub

Public Function GetEnvironVarPath(ByVal xPath As String) As String
'substitutes environment variable for <xToken>;
'if variable doesn't exist, returns path unchanged
    Dim xToken As String
    Dim iPosStart As Integer
    Dim iPosEnd As Integer
    Dim xValue As String
    
    On Error GoTo ProcError
    iPosStart = InStr(xPath, "<")
    iPosEnd = InStr(xPath, ">")
    
    If (iPosStart > 0) And (iPosEnd > 0) Then
        xToken = Mid$(xPath, iPosStart + 1, iPosEnd - iPosStart - 1)
        
        If UCase$(xToken) = "USERNAME" Then
            'we don't use environ for username
            'because it's not an environment variable
            'in Windows 98
            xValue = GetLogonName()
        Else
            xValue = Environ(xToken)
        End If
    End If
    
    If xValue <> "" Then
        GetEnvironVarPath = Replace(xPath, "<" & xToken & ">", xValue)
    Else
        GetEnvironVarPath = xPath
    End If
    
    Exit Function
ProcError:
    Err.Raise Err.Number, "CIX.CGroups.GetEnvironVarPath"
    Exit Function
End Function

Public Function GetLogonName() As String
'returns the current system user name
    Dim xBuf As String
    
    On Error GoTo ProcError
    
'   get logon name
    xBuf = String(255, " ")
    GetUserName xBuf, 255
    
    If Len(xBuf) = 0 Then
'       alert to no logon name
        Err.Raise 5, , _
            "UserName is empty.  You might not be logged on to the system. " & _
                "Please log off, and log on again."
    End If
    
'   trim extraneous buffer chars
    Trim (xBuf)
    xBuf = RTrim$(xBuf)
    xBuf = Left$(xBuf, Len(xBuf) - 1)
    
    GetLogonName = xBuf
    Exit Function
ProcError:
    Err.Raise Err.Number, "CIX.CGroups.GetLogonName"
End Function

Private Sub CreateTablesIfNecessary()
'creates the two 'groups' tables if they
'don't exist in the connected db
    Dim oRS As ADODB.Recordset
    Dim xSQL As String
    
    On Error GoTo ProcError
    
    With m_oCnn
        'test for tblContactGroups
        Set oRS = .OpenSchema(adSchemaTables, Array( _
            Empty, Empty, "tblContactGroups"))
        If oRS.BOF And oRS.EOF Then
            'table doesn't exist - create it
            xSQL = "CREATE TABLE tblContactGroups ( " & _
                "fldID int NOT NULL IDENTITY(1,1), " & _
                "fldName varchar(50) NOT NULL)"
            Set oRS = m_oCnn.Execute(xSQL)
        End If
        
        'test for tblContactGroupMembers
        Set oRS = .OpenSchema(adSchemaTables, Array( _
            Empty, Empty, "tblContactGroupMembers"))
        If oRS.BOF And oRS.EOF Then
            'table doesn't exist - create it
            xSQL = "CREATE TABLE tblContactGroupMembers (" & _
                "fldID int NOT NULL IDENTITY(1,1)," & _
                "fldGroup int NOT NULL," & _
                "fldUNID memo NOT NULL, " & _
                "fldDisplayName varchar(100) NOT NULL," & _
                "fldCompany varchar(100)," & _
                "fldFirstName varchar(30)," & _
                "fldLastName varchar(70)," & _
                "fldTitle varchar(100)," & _
                "fldAddressTypeName varchar(30), " & _
                "fldAddressType varchar(40))"

            Set oRS = m_oCnn.Execute(xSQL)
        End If
    End With
    Exit Sub
ProcError:
    g_oError.RaiseError "CIX.CGroups.CreateTablesIfNecessary"
    Exit Sub
End Sub

Private Sub ICIBackend_AddContact()
    On Error GoTo ProcError
    Err.Raise ciErrs.ciErr_NotImplemented, , _
        "AddContact is not implemented for CIX.CGroups."
    Exit Sub
ProcError:
    g_oError.RaiseError "CIX.CGroups.ICIBackend_AddContact"
    Exit Sub
End Sub

Private Property Get ICIBackend_Col1Name() As String
    ICIBackend_Col1Name = g_vCols(0, 0)
End Property

Private Property Get ICIBackend_Col2Name() As String
    ICIBackend_Col2Name = g_vCols(1, 0)
End Property

Private Property Get ICIBackend_Col3Name() As String
    ICIBackend_Col3Name = g_vCols(2, 0)
End Property

Private Property Get ICIBackend_Col4Name() As String
    ICIBackend_Col4Name = g_vCols(3, 0)
End Property

Private Property Get ICIBackend_DisplayName() As String
    ICIBackend_DisplayName = g_oIni.GetIni("Backend" & m_iID, "Name", g_oIni.CIIni)
End Property

Private Sub ICIBackend_EditContact(oListing As CListing)
    On Error GoTo ProcError
    Err.Raise ciErrs.ciErr_NotImplemented, , _
        "EditContact is not implemented for CIX.CGroups."
    Exit Sub
ProcError:
    g_oError.RaiseError "CIX.CGroups.ICIBackend_EditContact"
    Exit Sub
End Sub

Private Function ICIBackend_Events() As CEventGenerator
    ICIBackend_Events = m_oEvents
End Function

Private Function ICIBackend_Filter() As CFilter
'returns the filter object for this backend
    On Error GoTo ProcError
    
    If m_oFilter Is Nothing Then
        CreateFilter
    End If
        
    Set ICIBackend_Filter = m_oFilter
    Exit Function
ProcError:
    g_oError.RaiseError "CIX.CGroups.Filter"
End Function

Private Property Get ICIBackend_IsLoadableEntity(ByVal xUNID As String) As Boolean
'returns TRUE iff the specified UNID can contain contacts

    Dim oUNID As CIO.CUNID
    Dim bIsFolder As Boolean
    
    On Error GoTo ProcError
    Set oUNID = New CIO.CUNID

    'all folders are loadable entities
    ICIBackend_IsLoadableEntity = (oUNID.GetUNIDType(xUNID) = ciUNIDType_Folder)
    
    Exit Property
ProcError:
    g_oError.RaiseError "CIX.CGroups.ICIBackend_IsLoadableEntity"
    Exit Property
End Property

Private Property Get ICIBackend_IsSearchableEntity(ByVal xUNID As String) As Boolean
'returns TRUE iff the specified UNID can contain contacts

'all folders are searchable entities
    Dim oUNID As CIO.CUNID
    Dim iType As CIO.ciUNIDTypes
    
    On Error GoTo ProcError
    Set oUNID = New CIO.CUNID
    iType = oUNID.GetUNIDType(xUNID)
    ICIBackend_IsSearchableEntity = (iType = ciUNIDType_Folder)
    
    Exit Property
ProcError:
    g_oError.RaiseError "CIX.CGroups.ICIBackend_IsSearchableEntity"
    Exit Property
End Property

Private Function ICIBackend_GetAddresses(Optional oListing As CListing) As CAddresses
    Dim oAddresses As CIO.CAddresses
    Dim oAddress As CIO.CAddress
    Dim xSQL As String
    
    On Error GoTo ProcError
    
    Set oAddresses = New CIO.CAddresses
    
    If oListing Is Nothing Then
        Set oAddress = New CAddress
        
        'specify the word 'various' in the list of available addresses
        oAddress.Name = "Various"
        oAddress.UNID = ""
    Else
        xSQL = "SELECT fldUNID,fldAddressTypeName FROM " & _
            "tblContactGroupMembers WHERE fldID=" & oListing.ID & " AND fldAddressType <> """""
        
        Dim oRS As ADODB.Recordset
        Set oRS = New ADODB.Recordset
        oRS.Open xSQL, m_oCnn, adOpenForwardOnly
        
        If Not (oRS.EOF And oRS.BOF) Then
            Set oAddress = New CAddress
            With oAddress
                .Name = oRS!fldAddressTypeName
                .UNID = oRS!fldUNID
            End With
        End If
        
        oRS.Close
        Set oRS = Nothing
    End If
    
    'add the address to the collection
    oAddresses.Add oAddress
    
    'return
    Set ICIBackend_GetAddresses = oAddresses
    Exit Function
ProcError:
    g_oError.RaiseError "CIX.CGroups.ICIBackend_GetAddresses"
    Exit Function
End Function

Private Function ICIBackend_GetContacts(oListing As CListing, oAddress As CAddress, Optional ByVal vAddressType As Variant, Optional ByVal iIncludeData As ciRetrieveData = 1&, Optional ByVal iAlerts As ciAlerts = 1&) As CContacts
    On Error GoTo ProcError
    If Not IsMissing(vAddressType) Then
        Set ICIBackend_GetContacts = GetContacts(oListing, oAddress, vAddressType, iIncludeData, iAlerts)
    Else
        Set ICIBackend_GetContacts = GetContacts(oListing, oAddress, , iIncludeData, iAlerts)
    End If
    Exit Function
ProcError:
    g_oError.RaiseError "CIX.CGroups.ICIBackend_GetContacts"
    Exit Function
End Function

Private Function ICIBackend_GetCustomFields(oListing As CListing) As CCustomFields
    On Error GoTo ProcError
    Err.Raise ciErrs.ciErr_NotImplemented, , "GetCustomFields is not implemented for Groups backend."
    Exit Function
ProcError:
    g_oError.RaiseError "CIX.CGroups.ICIBackend_GetCustomFields"
    Exit Function
End Function

Private Function ICIBackend_GetEMailNumber(oListing As CListing, ByVal vEMailID As Variant) As CContactNumber

End Function

Private Function ICIBackend_GetEMailNumbers(oListing As CListing, Optional ByVal vAddressType As Variant) As CContactNumbers
'calls ICIBackend_GetEMailNumbers in backend of specified listing
    Dim oB As CIO.ICIBackend
    
    On Error GoTo ProcError
    'get backend of listing
    Set oB = mdlMain.Backends.ItemFromID(oListing.BackendID)
    
    Set ICIBackend_GetEMailNumbers = oB.GetEMailNumbers(oListing, vAddressType)
    Exit Function
ProcError:
    g_oError.RaiseError "CIX.CGroups.ICIBackend_GetEMailNumbers"
    Exit Function
End Function

Private Function ICIBackend_GetFaxNumber(oListing As CListing, ByVal vFaxID As Variant) As CContactNumber

End Function

Private Function ICIBackend_GetFaxNumbers(oListing As CListing, Optional ByVal vAddressType As Variant) As CContactNumbers
'calls ICIBackend_GetFaxNumbers in backend of specified listing
    Dim oB As CIO.ICIBackend
    
    On Error GoTo ProcError
    Set ICIBackend_GetFaxNumbers = ListingBackend(oListing) _
        .GetFaxNumbers(oListing, vAddressType)
    Exit Function
ProcError:
    g_oError.RaiseError "CIX.CGroups.ICIBackend_GetFaxNumbers"
    Exit Function
End Function

Private Function ICIBackend_GetFolderListings(oFolder As CIO.CFolder, Optional oFilter As CIO.CFilter) As CIO.CListings
'returns the collection of listings that match the filter criteria and
'are stored in the specified folder
    Dim oRS As ADODB.Recordset
    Dim oListings As CIO.CListings
    Dim i As Long
    Dim lRecs As Long
    Dim Cancel As Boolean
    Dim xSQL As String
    Dim xFilterSort As String
        
    On Error GoTo ProcError
    
'   get new listings
    Set oListings = New CIO.CListings
    
    xSQL = "SELECT * FROM tblContactGroupMembers WHERE fldGroup=" & oFolder.ID
    
    If Not oFilter Is Nothing Then
        xSQL = xSQL & GetFilterSort(oFilter)
    Else
        xSQL = xSQL & " ORDER BY fldDisplayName"
    End If
    
    'get folder records
    Set oRS = New ADODB.Recordset
    oRS.Open xSQL, m_oCnn, adOpenStatic, adLockReadOnly

    If Not (oRS.BOF And oRS.EOF) Then
        m_oEvents.RaiseBeforeListingsRetrieved lRecs, Cancel
                          
'       create a new listing for each record returned
        If Not Cancel Then
            oRS.MoveFirst
            While Not oRS.EOF
                AddListing oRS, oListings, oFolder.UNID, True
                
                i = i + 1
                m_oEvents.RaiseAfterListingAdded i, lRecs, Cancel
                If Cancel Then
                    m_oEvents.RaiseAfterListingsRetrieved i
                    Set ICIBackend_GetFolderListings = oListings
                    Exit Function
                End If
                oRS.MoveNext
                m_oEvents.RaiseAfterListingsRetrieved i
            Wend
        End If
    Else
'       return empty listings collection
        Set oListings = New CIO.CListings
    End If
    
    Set ICIBackend_GetFolderListings = oListings
    Exit Function
ProcError:
    Err.Raise Err.Number, "CIX.CGroups.GetFolderListings", Err.Description
    Exit Function
End Function

Private Function AddListing(oRS As ADODB.Recordset, oListings As CIO.CListings, ByVal xFolderUNID As String, ByVal bTrimParens As Boolean)
    Dim oConst As CConstants
    Dim xID As String
    Dim xDisplayName As String
    Dim xProp1 As String
    Dim xProp2 As String
    Dim xProp3 As String
    Dim iPos As Integer

    With oRS
        On Error Resume Next
'       clear out vars
        xID = Empty
        xDisplayName = Empty
        xProp1 = Empty
        xProp2 = Empty
        xProp3 = Empty

'       add entry to list
        xDisplayName = .Fields(g_vCols(0, 1))
        
        If bTrimParens Then
            'trim parens from display name
        
            'get last (
            iPos = InStrRev(xDisplayName, "  (")
            If iPos > 0 Then
                xDisplayName = Left$(xDisplayName, iPos - 1)
            End If
        End If
        
        xProp1 = .Fields(g_vCols(1, 1))
        xProp2 = .Fields(g_vCols(2, 1))
        xProp3 = .Fields(g_vCols(3, 1))
        On Error GoTo ProcError
        
        Set oConst = New CConstants
        xID = xFolderUNID & oConst.UNIDSep & .Fields("fldID")
        On Error GoTo 0
        
        oListings.Add xID, xDisplayName, xProp1, _
            xProp2, xProp3, ciListingType_Person
    End With
    Exit Function
    
ProcError:
    Err.Raise Err.Number, "CIX.CGroups.AddListing", Err.Description
End Function

Private Property Let ICIBackend_DefaultSortColumn(iNew As ciListingCols)
    m_iSortCol = iNew
    g_oIni.SetIni "CIGroups", "SortColumn", CStr(iNew), g_oIni.CIUserIni()
End Property

Private Property Get ICIBackend_DefaultSortColumn() As ciListingCols
    On Error GoTo ProcError
    If m_iSortCol = Empty Then
        On Error Resume Next
        m_iSortCol = g_oIni.GetIni("CIGroups", "SortColumn", g_oIni.CIUserIni())
        On Error GoTo ProcError
    End If
    If m_iSortCol = Empty Then
        m_iSortCol = ciListingCols_DisplayName
    End If
    ICIBackend_DefaultSortColumn = m_iSortCol
    Exit Property
ProcError:
    g_oError.RaiseError "CIX.CGroups.ICIBackend_DefaultSortColumn"
    Exit Property
End Property

Private Function PromptForFilter() As CIO.CFilter
'prompts user for filter/sort conditions
    Dim oForm As frmFilter
    Dim oBackend As CIO.ICIBackend
    Dim i As Integer
    
    On Error GoTo ProcError
    Set oForm = New CIX.frmFilter
    
    With oForm
        Set .Backend = Me
        
        .SortColumn = ICIBackend_DefaultSortColumn()
'       show filter form
        .Show vbModal
        
        If Not .Canceled Then
            Set oBackend = Me
            
            'set sort column
            ICIBackend_DefaultSortColumn = .cmbSortBy.ListIndex
            
            With oBackend.Filter
                For i = 1 To .CountFields
                    'set filter field values
                    .FilterFields(i).Value = oForm.txtFilterField(i - 1).Text
                    .FilterFields(i).Operator = oForm.cmbSearchType _
                        .ItemData(oForm.cmbSearchType.ListIndex)
                Next i
                .SortColumn = ICIBackend_DefaultSortColumn
            End With
        
            'return filter
            Set PromptForFilter = oBackend.Filter
        End If
        Unload oForm
    End With
    Exit Function
ProcError:
    Dim lErr As Long
    Dim xDesc As String

    lErr = Err.Number
    xDesc = Err.Description
    
    Unload oForm
    
    Err.Number = lErr
    Err.Description = xDesc
    g_oError.RaiseError "CIX.CGroups.PromptForFilter"
    Exit Function
End Function
Private Function GetFilterSort(oFilter As CIO.CFilter) As String
'sets filter for supplied messages collection
    Dim i As Integer
    Dim xFilterSort As String
    Dim xVal As String
    
    For i = 1 To 4
        With oFilter.FilterFields(i)
            If .ID <> Empty And .Value <> Empty Then
                On Error Resume Next
                'modify value with wildcards if specified
                If .Operator = ciSearchOperator_BeginsWith Then
                    xVal = .Value & "%"
                    xFilterSort = xFilterSort & _
                        " AND " & .ID & " LIKE '" & xVal & "' "
                ElseIf .Operator = ciSearchOperator_Contains Then
                    xVal = "%" & .Value & "%"
                    xFilterSort = xFilterSort & _
                        " AND " & .ID & " LIKE '" & xVal & "' "
                Else
                    xVal = .Value
                    xFilterSort = xFilterSort & _
                        " AND " & .ID & "= """ & xVal & """ "
                End If
                
            End If
        End With
    Next i
    
    If oFilter.SortColumn <> Empty Then
'       sort descending by specified field
        xFilterSort = xFilterSort & " ORDER BY " & _
            g_vCols(oFilter.SortColumn, 1)
    Else
        If ICIBackend_DefaultSortColumn <> Empty Then
            xFilterSort = xFilterSort & " ORDER BY " & _
                g_vCols(ICIBackend_DefaultSortColumn(), 1)
        Else
            xFilterSort = xFilterSort & " ORDER BY fldDisplayName"
        End If
    End If
    
    GetFilterSort = xFilterSort
    Exit Function
ProcError:
    g_oError.RaiseError "CIX.CGroups.GetFilterSort"
End Function

Private Function ICIBackend_GetFolders(oStore As CStore) As CFolders
    Dim oFolder As CIO.CFolder
    Dim oFolders As CIO.CFolders
    Dim xSQL As String
    
    On Error GoTo ProcError
    
    'create new, empty folders collection
    Set oFolders = New CIO.CFolders
    
    'connect to db if necessary
    If Not ICIBackend_IsConnected Then
        Connect
    End If
    
    'get contact groups from db
    xSQL = "SELECT fldName, fldID FROM tblContactGroups ORDER by fldName"
    
    Dim oRS As Recordset
    Dim lRecs As Long
    Dim l As Long
    
    Set oRS = m_oCnn.Execute(xSQL, lRecs)
    
    With oRS
        If Not (.BOF And .EOF) Then
            'there are groups - cycle through,
            'adding a folder for each group
            While Not .EOF
                Set oFolder = New CIO.CFolder
                oFolder.UNID = ciGroupsInternalID & g_oConstants.UNIDSep & g_oConstants.UNIDSep & .Fields(1)
                oFolder.Name = .Fields(0)
                oFolders.Add oFolder
                .MoveNext
            Wend
        End If
    End With
    
    'return collection
    Set ICIBackend_GetFolders = oFolders
    Exit Function
ProcError:
    g_oError.RaiseError "CIX.CGroups.ICIBackend_GetFolders"
End Function

Private Function ICIBackend_GetPhoneNumber(oListing As CListing, ByVal vPhoneID As Variant) As CContactNumber

End Function

Private Function ICIBackend_GetPhoneNumbers(oListing As CListing, Optional ByVal vAddressType As Variant) As CContactNumbers
'calls ICIBackend_GetPhoneNumbers in backend of specified listing
    Dim oB As CIO.ICIBackend
    
    On Error GoTo ProcError
    Set ICIBackend_GetPhoneNumbers = ListingBackend(oListing) _
        .GetPhoneNumbers(oListing, vAddressType)
    Exit Function
ProcError:
    g_oError.RaiseError "CIX.CGroups.ICIBackend_GetPhoneNumbers"
    Exit Function
End Function

Private Function ICIBackend_GetStoreListings(oStore As CStore, Optional oFilter As CFilter) As CListings
    On Error GoTo ProcError
    Err.Raise ciErrs.ciErr_NotImplemented, , "GetStoreListings is not implemented for Groups backend."
    Exit Function
ProcError:
    g_oError.RaiseError "CIX.CGroups.ICIBackend_GetStoreListings"
    Exit Function
End Function

Private Function ICIBackend_GetStores() As CStores
    If Not ICIBackend_IsConnected Then
        Connect
    End If
End Function

Private Function ICIBackend_GetSubFolders(oFolder As CFolder) As CFolders
    On Error GoTo ProcError
    Err.Raise ciErrs.ciErr_NotImplemented, , "GetSubFolders is not implemented for Groups backend."
    Exit Function
ProcError:
    g_oError.RaiseError "CIX.CGroups.ICIBackend_GetSubFolders"
    Exit Function
End Function

Private Function ICIBackend_HasAddresses(oListing As CListing) As Boolean

End Function

Private Property Get ICIBackend_ID() As Integer
    ICIBackend_ID = m_iID
End Property

Private Property Let ICIBackend_ID(ByVal RHS As Integer)
    m_iID = RHS
End Property

Private Sub ICIBackend_Initialize(iID As Integer)
    Dim xName As String
    Dim xKey As String
    Dim xID As String
    Dim i As Integer
    Dim iPos As Integer
    
    On Error GoTo ProcError
    m_iID = iID
    
    'get column names/ids
    For i = 1 To 4
        xKey = g_oIni.GetIni("Backend" & iID, "Col" & i, g_oIni.CIIni)
        If xKey <> Empty Then
            'parse value
            iPos = InStr(xKey, ",")
            If iPos = 0 Then
                'no comma found - should be in form 'Name,ID'
                Err.Raise ciErrs.ciErr_MissingOrInvalidINIKey, , _
                    "Invalid value in ci.ini for Backend" & iID & _
                    "\Col" & i & "."
            Else
                'get name and id from string
                xName = Left$(xKey, iPos - 1)
                xID = Mid$(xKey, iPos + 1)
                
                If xName = Empty Or xID = Empty Then
                    'missing some value
                    Err.Raise ciErrs.ciErr_MissingOrInvalidINIKey, , _
                        "Invalid value in ci.ini for Backend" & iID & _
                        "\Col" & i & "."
                Else
                    'assign to columns array
                    g_vCols(i - 1, 0) = Trim$(xName)
                    g_vCols(i - 1, 1) = Trim$(xID)
                End If
            End If
        End If
    Next i
    Exit Sub
ProcError:
    g_oError.RaiseError "CIX.CGroups.ICIBackend_Initialize"
    Exit Sub
End Sub

Private Property Get ICIBackend_InternalID() As Integer
    ICIBackend_InternalID = ciGroupsInternalID
End Property

Private Property Get ICIBackend_IsConnected() As Boolean
     ICIBackend_IsConnected = m_bConnected
End Property
Private Property Get ICIBackend_Exists() As Boolean
    'force True value
    ICIBackend_Exists = True
End Property

Private Property Get ICIBackend_Name() As String
    Dim xName As String
    Dim xDesc As String
    
    On Error GoTo ProcError
    
    'get from ini
    xName = g_oIni.GetIni("Backend" & m_iID, "Name", g_oIni.CIIni)
    
    'raise error if ini value is missing
    If Len(xName) = 0 Then
        xDesc = "Invalid Backend" & m_iID & "\Name key in ci.ini."
        Err.Raise ciErr_MissingOrInvalidINIKey, , xDesc
        Exit Function
    End If
    ICIBackend_Name = xName
    Exit Function
    Exit Property
ProcError:
    g_oError.RaiseError "CIX.CGroups.ICIBackend_Name"
    Exit Property
End Property

Private Function ICIBackend_NumberPromptFormat() As ICINumberPromptFormat

End Function

Private Function ICIBackend_SearchFolder(oFolder As CFolder, bCancel As Boolean) As CListings
    Dim oFilter As CIO.CFilter

    On Error GoTo ProcError
    Set oFilter = PromptForFilter()
    
    If Not oFilter Is Nothing Then
        Set ICIBackend_SearchFolder = ICIBackend_GetFolderListings(oFolder, oFilter)
    End If
    Exit Function
ProcError:
    g_oError.RaiseError "CIX.CGroups.ICIBackend_SearchFolder"
    Exit Function
End Function

Private Function ICIBackend_SearchNative() As CListings
    On Error GoTo ProcError
    Err.Raise ciErrs.ciErr_NotImplemented, , "SearchNative is not implemented for Groups backend."
    Exit Function
ProcError:
    g_oError.RaiseError "CIX.CGroups.ICIBackend_SearchNative"
    Exit Function
End Function

Private Property Get ICIBackend_SearchOperators() As ciSearchOperators
    ICIBackend_SearchOperators = ciSearchOperator_BeginsWith Or _
        ciSearchOperator_Contains Or ciSearchOperator_Equals
End Property

Private Function ICIBackend_SearchStore(oStore As CStore) As CListings
    On Error GoTo ProcError
    Err.Raise ciErrs.ciErr_NotImplemented, , "SearchStore is not implemented for Groups backend."
    Exit Function
ProcError:
    g_oError.RaiseError "CIX.CGroups.ICIBackend_SearchStore"
    Exit Function
End Function

Private Property Get ICIBackend_SupportsContactAdd() As Boolean
    ICIBackend_SupportsContactAdd = False
End Property

Private Property Get ICIBackend_SupportsContactEdit() As Boolean
    ICIBackend_SupportsContactEdit = False
End Property

Private Property Get ICIBackend_SupportsFolders() As Boolean
    ICIBackend_SupportsFolders = True
End Property

Private Property Get ICIBackend_SupportsNativeSearch() As Boolean
    ICIBackend_SupportsNativeSearch = False
End Property

Private Property Get ICIBackend_SupportsNestedFolders() As Boolean
    ICIBackend_SupportsNestedFolders = False
End Property

Private Property Get ICIBackend_SupportsStoreLoad() As Boolean
    ICIBackend_SupportsStoreLoad = False
End Property

Private Property Get ICIBackend_SupportsStoreSearch() As Boolean
    ICIBackend_SupportsStoreSearch = False
End Property

Private Property Get ICIBackend_SupportsMultipleStores() As Boolean
    ICIBackend_SupportsMultipleStores = False
End Property

Private Function GetContacts(oListing As CIO.CListing, oAddress As CAddress, Optional ByVal vAddressType As Variant, Optional ByVal iIncludeData As ciRetrieveData = 1&, Optional ByVal iAlerts As ciAlerts = 1&) As CIO.CContacts
'returns the contact pointed to by the specified listing and address
    Dim oContacts As CIO.CContacts
    Dim oRS As ADODB.Recordset
    Dim xSQL As String
    Dim oNativeAddress As CIO.CAddress
    Dim oNativeListing As CIO.CListing
    Dim oUNID As CIO.CUNID
    
    On Error GoTo ProcError
    
    Set oContacts = New CIO.CContacts
    Set oUNID = New CIO.CUNID
    
    'get unid for the group listing
    xSQL = "SELECT fldUNID FROM tblContactGroupMembers WHERE fldID =" & oListing.ID
    
    Set oRS = New ADODB.Recordset
    
    With oRS
        .Open xSQL, m_oCnn, adOpenForwardOnly, adLockReadOnly
    
        If Not (.BOF And .EOF) Then
            'get unid for listing
            oListing.UNID = !fldUNID
            
            'create the unid of the listing and address
            'as it exists in its native application
            On Error Resume Next
            Set oNativeAddress = oUNID.GetAddress(!fldUNID, oAddress.Name)
            On Error GoTo ProcError
            
            Set oNativeListing = oUNID.GetListing(!fldUNID, oListing.DisplayName)

            Set oContacts = ListingBackend(oListing).GetContacts(oNativeListing, oNativeAddress, vAddressType, iIncludeData, iAlerts)
        Else
            Err.Raise ciErrs.ciErr_InvalidUNID, , _
                "No UNID found for groups listing with ID=" & oListing.ID
        End If
    End With
    Set GetContacts = oContacts
    Exit Function
ProcError:
    g_oError.RaiseError "CIX.CGroups.GetContacts"
    Exit Function
End Function

Private Function ICIBackend_CustomMenuItem1() As String
    ICIBackend_CustomMenuItem1 = Empty
End Function

Private Function ICIBackend_CustomMenuItem2() As String
    ICIBackend_CustomMenuItem2 = Empty
End Function

Private Function ICIBackend_CustomMenuItem3() As String
    ICIBackend_CustomMenuItem3 = Empty
End Function

Private Function ICIBackend_CustomMenuItem4() As String
    ICIBackend_CustomMenuItem4 = Empty
End Function

Private Function ICIBackend_CustomMenuItem5() As String
    ICIBackend_CustomMenuItem5 = Empty
End Function

Private Sub ICIBackend_CustomProcedure1()
    Const mpThisFunction As String = "CIX.CGroups.ICIBackend_CustomProcedure1"
    On Error GoTo ProcError
    Err.Raise ciErrs.ciErr_NotImplemented, , "Procedure not implemented."
    Exit Sub
ProcError:
    g_oError.RaiseError mpThisFunction
    Exit Sub
End Sub

Private Sub ICIBackend_CustomProcedure2()
    Const mpThisFunction As String = "CIX.CGroups.ICIBackend_CustomProcedure2"
    On Error GoTo ProcError
    Err.Raise ciErrs.ciErr_NotImplemented, , "Procedure not implemented."
    Exit Sub
ProcError:
    g_oError.RaiseError mpThisFunction
    Exit Sub
End Sub

Private Sub ICIBackend_CustomProcedure3()
    Const mpThisFunction As String = "CIX.CGroups.ICIBackend_CustomProcedure3"
    On Error GoTo ProcError
    Err.Raise ciErrs.ciErr_NotImplemented, , "Procedure not implemented."
    Exit Sub
ProcError:
    g_oError.RaiseError mpThisFunction
    Exit Sub
End Sub

Private Sub ICIBackend_CustomProcedure4()
    Const mpThisFunction As String = "CIX.CGroups.ICIBackend_CustomProcedure4"
    On Error GoTo ProcError
    Err.Raise ciErrs.ciErr_NotImplemented, , "Procedure not implemented."
    Exit Sub
ProcError:
    g_oError.RaiseError mpThisFunction
    Exit Sub
End Sub

Private Sub ICIBackend_CustomProcedure5()
    Const mpThisFunction As String = "CIX.CGroups.ICIBackend_CustomProcedure5"
    On Error GoTo ProcError
    Err.Raise ciErrs.ciErr_NotImplemented, , "Procedure not implemented."
    Exit Sub
ProcError:
    g_oError.RaiseError mpThisFunction
    Exit Sub
End Sub
