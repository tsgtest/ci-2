VERSION 5.00
Object = "{0AFE7BE0-11B7-4A3E-978D-D4501E9A57FE}#1.0#0"; "C1Sizer.ocx"
Object = "{0D62353D-DBA2-11D1-B5DF-0060976089D0}#6.0#0"; "TDBL6.OCX"
Begin VB.Form frmContactDetail 
   BackColor       =   &H00FFFFFF&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   " Contact Detail"
   ClientHeight    =   4572
   ClientLeft      =   48
   ClientTop       =   336
   ClientWidth     =   7680
   FillColor       =   &H8000000F&
   FillStyle       =   0  'Solid
   Icon            =   "frmContactDetail.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4572
   ScaleWidth      =   7680
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton btnClose 
      BackColor       =   &H00E0E0E0&
      Cancel          =   -1  'True
      Caption         =   "&Close"
      Default         =   -1  'True
      Height          =   315
      Left            =   6495
      MaskColor       =   &H00C0FFFF&
      TabIndex        =   0
      Top             =   4155
      Width           =   1100
   End
   Begin C1SizerLibCtl.C1Tab vsIndexTab1 
      Height          =   4440
      Left            =   2790
      TabIndex        =   5
      Top             =   30
      Width           =   4875
      _ExtentX        =   8594
      _ExtentY        =   7832
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   7.8
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Enabled         =   -1  'True
      Appearance      =   0
      MousePointer    =   0
      Version         =   600
      BackColor       =   16777215
      ForeColor       =   -2147483630
      FrontTabColor   =   16777215
      BackTabColor    =   12632256
      TabOutlineColor =   16777215
      FrontTabForeColor=   -2147483630
      Caption         =   "&Summary|&All Fields"
      Align           =   0
      Appearance      =   0
      CurrTab         =   1
      FirstTab        =   0
      Style           =   5
      Position        =   1
      AutoSwitch      =   -1  'True
      AutoScroll      =   -1  'True
      TabPreview      =   -1  'True
      ShowFocusRect   =   -1  'True
      TabsPerPage     =   0
      BorderWidth     =   0
      BoldCurrent     =   -1  'True
      DogEars         =   0   'False
      MultiRow        =   0   'False
      MultiRowOffset  =   200
      CaptionStyle    =   0
      TabHeight       =   0
      Begin VB.Frame fraAllFields 
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   0  'None
         Height          =   4128
         Left            =   15
         TabIndex        =   9
         Top             =   15
         Width           =   4845
         Begin TrueDBList60.TDBList lstContactDetail 
            Height          =   3600
            Left            =   60
            OleObjectBlob   =   "frmContactDetail.frx":058A
            TabIndex        =   10
            Top             =   216
            Width           =   4680
         End
      End
      Begin VB.Frame fraSummary 
         BackColor       =   &H00FFFFFF&
         BorderStyle     =   0  'None
         Height          =   4128
         Left            =   -5340
         TabIndex        =   6
         Top             =   15
         Width           =   4845
         Begin VB.TextBox txtSummary 
            Appearance      =   0  'Flat
            BackColor       =   &H00FFFFFF&
            BorderStyle     =   0  'None
            Height          =   2715
            Left            =   555
            Locked          =   -1  'True
            MultiLine       =   -1  'True
            TabIndex        =   7
            Top             =   930
            Width           =   4170
         End
         Begin VB.Image imgCopySummary 
            Appearance      =   0  'Flat
            Height          =   192
            Left            =   4440
            Picture         =   "frmContactDetail.frx":2712
            ToolTipText     =   "Copy Contact Summary (Ctrl + C)"
            Top             =   108
            Width           =   204
         End
         Begin VB.Label lblAddressType 
            BackStyle       =   0  'Transparent
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   7.8
               Charset         =   0
               Weight          =   400
               Underline       =   -1  'True
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   390
            Left            =   540
            TabIndex        =   8
            Top             =   510
            Width           =   4050
         End
      End
   End
   Begin VB.OptionButton optView 
      Appearance      =   0  'Flat
      BackColor       =   &H00E0E0E0&
      Caption         =   "&All Fields"
      ForeColor       =   &H80000008&
      Height          =   390
      Index           =   1
      Left            =   4995
      Style           =   1  'Graphical
      TabIndex        =   4
      Top             =   4365
      Visible         =   0   'False
      Width           =   1100
   End
   Begin VB.OptionButton optView 
      Caption         =   "&Summary"
      Height          =   390
      Index           =   0
      Left            =   2940
      Style           =   1  'Graphical
      TabIndex        =   3
      Top             =   4470
      Visible         =   0   'False
      Width           =   1100
   End
   Begin TrueDBList60.TDBList lstContacts 
      Height          =   4305
      Left            =   165
      OleObjectBlob   =   "frmContactDetail.frx":2854
      TabIndex        =   2
      Top             =   165
      Width           =   2520
   End
   Begin VB.Shape Shape1 
      BackColor       =   &H00E0E0E0&
      BackStyle       =   1  'Opaque
      BorderStyle     =   0  'Transparent
      Height          =   4605
      Left            =   15
      Top             =   -15
      Width           =   2700
   End
   Begin VB.Label lblHeading 
      BackStyle       =   0  'Transparent
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   7.8
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   315
      Left            =   225
      TabIndex        =   1
      Top             =   225
      Width           =   5985
   End
   Begin VB.Menu mnuMain 
      Caption         =   "Main"
      Visible         =   0   'False
      Begin VB.Menu mnuMain_Copy 
         Caption         =   "&Copy"
      End
   End
End
Attribute VB_Name = "frmContactDetail"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private m_oContacts As CIO.CContacts

'**********************************************************
'initializer/terminator
'**********************************************************
Dim xObjectID As String
Private Sub Class_Initialize()
    Math.Randomize
    xObjectID = "CContactDetail" & Format(Now, "hhmmss") & Math.Rnd()
    g_oGlobals.Objects.Add xObjectID, xObjectID
    DebugPrint "Added - " & xObjectID & ": " & g_oGlobals.Objects.Count
End Sub

Private Sub Class_Terminate()
    g_oGlobals.Objects.Remove xObjectID
    DebugPrint "Removed - " & xObjectID & ": " & g_oGlobals.Objects.Count
End Sub
'**********************************************************

Public Property Set Contacts(oNew As CIO.CContacts)
    On Error GoTo ProcError
    Set m_oContacts = oNew
    Exit Property
ProcError:
    g_oError.RaiseError "CIX.frmContactDetail.Contacts"
    Exit Property
End Property

Public Property Get Contacts() As CIO.CContacts
    Set Contacts = m_oContacts
End Property

Public Property Let SelectedContactIndex(ByVal iIndex As Integer)
    Me.lstContacts.BoundText = iIndex
    lstContacts_RowChange
End Property
'
'Public Property Set SelectedContact(oNew As CIO.CContact)
'    Me.lstContacts.BoundText = oNew.UNID
'    lstContacts_RowChange
'End Property

Private Sub btnClose_Click()
    Me.Hide
End Sub

Private Sub btnCopySummary_Click()
    On Error GoTo ProcError
    VB.Clipboard.SetText Me.txtSummary.Text
    Exit Sub
ProcError:
    g_oError.ShowError
    Exit Sub
End Sub

Private Sub Form_Load()
    Me.lstContacts.Array = Me.Contacts.GetList()
    Me.optView.Item(0).Value = True


End Sub

Private Sub Form_Paint()
    'Load icon
    With g_oSessionType
        If (.SessionType = ciSession_Connect) Then
            .SetIcon Me.hwnd, "CONNECTICON", False
        Else
            .SetIcon Me.hwnd, "TSGICON", False
        End If
    End With

End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set m_oContacts = Nothing
End Sub

Private Sub Image1_Click()

End Sub

Private Sub imgCopySummary_Click()
    Dim xSummary As String
    
    On Error GoTo ProcError
    
    xSummary = Me.txtSummary.Text
    
    'remove all double paragraphs
    While xSummary Like "*" & vbCrLf & vbCrLf & vbCrLf & "*"
        xSummary = Replace(xSummary, vbCrLf & vbCrLf & vbCrLf, vbCrLf)
    Wend
        
    With VB.Clipboard
        .Clear
        .SetText Trim$(xSummary)
    End With
    Exit Sub
ProcError:
    g_oError.ShowError
    Exit Sub
End Sub

Private Sub imgCopySummary_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Me.imgCopySummary.BorderStyle = 1
End Sub

Private Sub imgCopySummary_MouseUp(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Me.imgCopySummary.BorderStyle = 0
End Sub

Private Sub lstContactDetail_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    On Error Resume Next
    Me.lstContactDetail.SelectedItem = Me.lstContactDetail.RowContaining(Y)
End Sub

Private Sub Form_Activate()
    Me.vsIndexTab1.CurrTab = 0
    
    'Me.lstContacts.BoundText = Me.SelectedContactIndex
    If Me.lstContacts.Array.Count(1) > 0 Then
        Me.lstContacts.SetFocus
    End If
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
'    If IsPressed(KEY_CTL) Then
'        If UCase$(Chr$(KeyCode)) = "C" Then
'            On Error GoTo ProcError
'            VB.Clipboard.SetText Me.txtSummary.Text
'        End If
'    ElseIf KeyCode = vbKeyReturn Then
'        Me.Hide
'    End If
    If KeyCode = vbKeyC And Shift = 2 Then
        imgCopySummary_Click
    End If
    Exit Sub
ProcError:
    g_oError.ShowError
    Exit Sub
End Sub

Function lCountChrs(xSource As String, xSearch As String) As Long
'returns the number of instances
'of xSearch in xSource

    Dim iPos As Integer
    Dim l As Long
    
    iPos = InStr(xSource, xSearch)
    While iPos
        l = l + 1
        iPos = InStr(iPos + 1, xSource, xSearch)
    Wend
    lCountChrs = l
End Function

Public Sub GetSummary(oContact As CIO.CContact)
'gets a summary string for the specified contact
    Dim xToken As String
    
    On Error GoTo ProcError
    
    xToken = "{<ADDRESSTYPENAME> Address}"
    Me.lblAddressType.Caption = oContact.GetDetail(xToken)
    
    If Me.lblAddressType.Caption = Empty Then
        Me.lblAddressType.Caption = vbCrLf
    End If
    
    Me.txtSummary.Text = mdlMain.GetSummary(oContact)
    Exit Sub
ProcError:
    g_oError.RaiseError "CIX.frmContactDetail.GetSummary"
    Exit Sub
End Sub
Public Sub GetDetail(oContact As CIO.CContact)
'loads the field values for the specified contact
    Dim xTemp As String
    Dim oCustFld As CIO.CCustomField
    Dim i As Integer
    Dim xVal0 As String
    Dim oDetail As XArrayDB
    
    On Error GoTo ProcError
    
    On Error Resume Next
    With oContact
        'name/value array not yet built - do it
        On Error GoTo ProcError
        Set oDetail = New XArrayDB
        oDetail.ReDim 0, (24 + .CustomFields.Count), 0, 1
    
        oDetail(0, 0) = "Display Name"
        oDetail(1, 0) = "Prefix"
        oDetail(2, 0) = "First Name"
        oDetail(3, 0) = "Middle Name"
        oDetail(4, 0) = "Last Name"
        oDetail(5, 0) = "Suffix"
        oDetail(6, 0) = "Full Name"
        oDetail(7, 0) = "Initials"
        oDetail(8, 0) = "Salutation"
        oDetail(9, 0) = "Title"
        oDetail(10, 0) = "Department"
        oDetail(11, 0) = "Company"
        oDetail(12, 0) = "Address Type"
        oDetail(13, 0) = "Street1"
        oDetail(14, 0) = "Street2"
        oDetail(15, 0) = "Street3"
        oDetail(16, 0) = "Additional"
        oDetail(17, 0) = "City"
        oDetail(18, 0) = "State"
        oDetail(19, 0) = "Zip Code"
        oDetail(20, 0) = "Country"
        oDetail(21, 0) = "Phone"
        oDetail(22, 0) = "Phone Extension"
        oDetail(23, 0) = "Fax"
        oDetail(24, 0) = "EMail Address"
        
        oDetail(0, 1) = .DisplayName
        oDetail(1, 1) = .Prefix
        oDetail(2, 1) = .FirstName
        oDetail(3, 1) = .MiddleName
        oDetail(4, 1) = .LastName
        oDetail(5, 1) = .Suffix
        oDetail(6, 1) = .FullName
        oDetail(7, 1) = .Initials
        oDetail(8, 1) = .Salutation
        oDetail(9, 1) = .Title
        oDetail(10, 1) = .Department
        oDetail(11, 1) = .Company
        oDetail(12, 1) = .AddressTypeName
        oDetail(13, 1) = .Street1
        oDetail(14, 1) = .Street2
        oDetail(15, 1) = .Street3
        oDetail(16, 1) = .AdditionalInformation
        oDetail(17, 1) = .City
        oDetail(18, 1) = .State
        oDetail(19, 1) = .ZipCode
        oDetail(20, 1) = .Country
        oDetail(21, 1) = .Phone
        oDetail(22, 1) = .PhoneExtension
        oDetail(23, 1) = .Fax
        oDetail(24, 1) = .EMailAddress
        
        For Each oCustFld In .CustomFields
            i = i + 1
            oDetail(24 + i, 0) = oCustFld.Name
            oDetail(24 + i, 1) = oCustFld.Value
        Next oCustFld
    End With

    Me.lstContactDetail.Array = oDetail
    Me.lstContactDetail.ReBind
    Exit Sub
ProcError:
    g_oError.RaiseError "CIX.frmContactDetail.GetDetail"
    Exit Sub
End Sub

Private Sub lstAddressTypes_DblClick()
'do what happens when OK is clicked
    btnClose_Click
End Sub

Private Sub lstContacts_RowChange()
    Dim oContact As CIO.CContact
    Dim iFirstRow As Integer
    Static vPrevID As Variant
    
    On Error GoTo ProcError
    If Me.lstContacts.BoundText = vPrevID Then
        Exit Sub
    Else
        vPrevID = Me.lstContacts.BoundText
    End If
    
    If Me.lstContacts.BoundText <> Empty Then
        Set oContact = m_oContacts.Item(CInt(Me.lstContacts.BoundText))
        
        'get first row in detail list
        On Error Resume Next
        iFirstRow = Me.lstContactDetail.FirstRow
        On Error GoTo ProcError
        
        If Not oContact Is Nothing Then
            GetSummary oContact
            GetDetail oContact
        End If
        
        Me.lstContactDetail.FirstRow = iFirstRow
    End If
    
    Exit Sub
ProcError:
    g_oError.ShowError
    Exit Sub
End Sub

Private Sub tabDetail_Click(PreviousTab As Integer)
    On Error Resume Next
    Me.lstContacts.SetFocus
End Sub

Private Sub ClearContacts()
    Set m_oContacts = Nothing
End Sub

Private Sub optView_Click(Index As Integer)
'    Me.fraAllFields.Visible = Index = 1
'    Me.fraSummary.Visible = Index = 0
End Sub

