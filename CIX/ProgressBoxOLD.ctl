VERSION 5.00
Begin VB.UserControl ProgressBox 
   BackStyle       =   0  'Transparent
   ClientHeight    =   1755
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   4785
   DefaultCancel   =   -1  'True
   LockControls    =   -1  'True
   ScaleHeight     =   1755
   ScaleWidth      =   4785
   Begin VB.PictureBox picProgress 
      Appearance      =   0  'Flat
      AutoRedraw      =   -1  'True
      BorderStyle     =   0  'None
      ForeColor       =   &H80000008&
      Height          =   1680
      Left            =   0
      ScaleHeight     =   1680
      ScaleWidth      =   4710
      TabIndex        =   0
      Top             =   0
      Width           =   4710
      Begin VB.CommandButton btnCancel 
         Cancel          =   -1  'True
         Caption         =   "&Cancel"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   390
         Left            =   1770
         TabIndex        =   1
         Top             =   1140
         Width           =   1215
      End
      Begin VB.PictureBox pbContacts 
         Height          =   285
         Left            =   375
         ScaleHeight     =   225
         ScaleWidth      =   3915
         TabIndex        =   2
         Top             =   705
         Width           =   3975
      End
      Begin VB.Label lblMsg 
         Height          =   240
         Left            =   405
         TabIndex        =   4
         Top             =   450
         Width           =   3975
      End
      Begin VB.Line Line7 
         BorderColor     =   &H00404040&
         X1              =   4695
         X2              =   4695
         Y1              =   0
         Y2              =   1660
      End
      Begin VB.Label lblTitle 
         BackStyle       =   0  'Transparent
         Caption         =   "Loading Contacts"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000014&
         Height          =   210
         Left            =   105
         TabIndex        =   3
         Top             =   75
         Width           =   1560
      End
      Begin VB.Line Line3 
         BorderColor     =   &H00FFFFFF&
         X1              =   15
         X2              =   15
         Y1              =   30
         Y2              =   1665
      End
      Begin VB.Line Line4 
         BorderColor     =   &H00808080&
         X1              =   15
         X2              =   4690
         Y1              =   1650
         Y2              =   1650
      End
      Begin VB.Line Line5 
         BorderColor     =   &H00808080&
         X1              =   4680
         X2              =   4680
         Y1              =   15
         Y2              =   1675
      End
      Begin VB.Line Line6 
         BorderColor     =   &H00404040&
         X1              =   30
         X2              =   4710
         Y1              =   1665
         Y2              =   1665
      End
      Begin VB.Line Line8 
         BorderColor     =   &H00FFFFFF&
         X1              =   15
         X2              =   4690
         Y1              =   15
         Y2              =   15
      End
      Begin VB.Shape Shape1 
         BackColor       =   &H80000002&
         BackStyle       =   1  'Opaque
         BorderStyle     =   0  'Transparent
         Height          =   300
         Left            =   45
         Top             =   45
         Width           =   4635
      End
   End
End
Attribute VB_Name = "ProgressBox"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Public Event Cancelled()
Public Event OnShow()
Public Event OnHide()

Private m_bShowCancelled As Boolean

Public Property Let ShowCancelled(ByVal bNew As Boolean)
    With UserControl
        .btnCancel.Visible = bNew
        .btnCancel.Enabled = bNew
        If bNew Then
            .lblMsg.Top = 450
            .pbContacts.Top = 705
        Else
            .lblMsg.Top = 585
            .pbContacts.Top = 840
        End If
    End With
    m_bShowCancelled = bNew
End Property

Public Property Get ShowCancelled() As Boolean
    ShowCancelled = m_bShowCancelled
End Property

Public Property Let Value(ByVal sNew As Single)
    On Error Resume Next
    UserControl.pbContacts.Value = sNew
    DoEvents
End Property

Public Property Get Value() As Single
    Value = UserControl.pbContacts.Value
End Property

Public Property Let Message(ByVal xNew As String)
    UserControl.lblMsg.Caption = xNew
End Property

Public Property Get Message() As String
    Message = UserControl.lblMsg.Caption
End Property

Public Property Let Title(ByVal xNew As String)
    UserControl.lblTitle.Caption = xNew
End Property

Public Property Get Title() As String
    Title = UserControl.lblTitle.Caption
End Property

Private Sub btnCancel_Click()
    RaiseEvent Cancelled
End Sub

Private Sub UserControl_Hide()
    RaiseEvent OnHide
End Sub

Private Sub UserControl_Show()
    RaiseEvent OnShow
End Sub

Private Sub UserControl_InitProperties()
    Me.Title = "Contact Integration"
End Sub

Private Sub UserControl_ReadProperties(PropBag As PropertyBag)
    With PropBag
        Me.Title = .ReadProperty("Title", "Contact Integration")
        Me.Message = .ReadProperty("Message", "")
        Me.Value = .ReadProperty("Value", 0)
        Me.ShowCancelled = .ReadProperty("ShowCancelled", False)
    End With
End Sub

Private Sub UserControl_WriteProperties(PropBag As PropertyBag)
    With PropBag
        .WriteProperty "Title", Me.Title, "Contact Integration"
        .WriteProperty "Message", Me.Message, ""
        .WriteProperty "Value", Me.Value, 0
        .WriteProperty "ShowCancelled", Me.ShowCancelled, False
    End With
End Sub
