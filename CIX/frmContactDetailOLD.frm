VERSION 5.00
Object = "{0D62353D-DBA2-11D1-B5DF-0060976089D0}#6.0#0"; "TDBL6.OCX"
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "TABCTL32.OCX"
Begin VB.Form frmContactDetail 
   BackColor       =   &H00C0C0C0&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Contact Detail"
   ClientHeight    =   4530
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7680
   FillColor       =   &H8000000F&
   FillStyle       =   0  'Solid
   BeginProperty Font 
      Name            =   "MS Sans Serif"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmContactDetailOLD.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4530
   ScaleWidth      =   7680
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin TrueDBList60.TDBList lstContacts 
      Height          =   3780
      Left            =   30
      OleObjectBlob   =   "frmContactDetailOLD.frx":000C
      TabIndex        =   5
      Top             =   180
      Width           =   2595
   End
   Begin TabDlg.SSTab tabDetail 
      Height          =   3810
      Left            =   2625
      TabIndex        =   2
      Top             =   150
      Width           =   4980
      _ExtentX        =   8784
      _ExtentY        =   6720
      _Version        =   393216
      Style           =   1
      Tabs            =   2
      TabsPerRow      =   2
      TabHeight       =   520
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      TabCaption(0)   =   "&Summary"
      TabPicture(0)   =   "frmContactDetailOLD.frx":2177
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "lblAddressType"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "txtSummary"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).ControlCount=   2
      TabCaption(1)   =   "&All Fields"
      TabPicture(1)   =   "frmContactDetailOLD.frx":2193
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "lstContactDetail"
      Tab(1).ControlCount=   1
      Begin VB.TextBox txtSummary 
         Appearance      =   0  'Flat
         BackColor       =   &H8000000F&
         BorderStyle     =   0  'None
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2715
         Left            =   240
         Locked          =   -1  'True
         MultiLine       =   -1  'True
         TabIndex        =   4
         Text            =   "frmContactDetailOLD.frx":21AF
         Top             =   975
         Width           =   4515
      End
      Begin TrueDBList60.TDBList lstContactDetail 
         Height          =   3345
         Left            =   -74865
         OleObjectBlob   =   "frmContactDetailOLD.frx":21B5
         TabIndex        =   3
         Top             =   405
         Width           =   4800
      End
      Begin VB.Label lblAddressType 
         Caption         =   "Label1"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   -1  'True
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   390
         Left            =   240
         TabIndex        =   6
         Top             =   555
         Width           =   4515
      End
   End
   Begin VB.CommandButton btnClose 
      BackColor       =   &H00E0E0E0&
      Cancel          =   -1  'True
      Caption         =   "Close"
      Default         =   -1  'True
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   390
      Left            =   6505
      MaskColor       =   &H00C0FFFF&
      TabIndex        =   0
      Top             =   4050
      Width           =   1100
   End
   Begin VB.Label lblHeading 
      BackStyle       =   0  'Transparent
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   315
      Left            =   225
      TabIndex        =   1
      Top             =   225
      Width           =   5985
   End
   Begin VB.Menu mnuMain 
      Caption         =   "Main"
      Visible         =   0   'False
      Begin VB.Menu mnuMain_Copy 
         Caption         =   "&Copy"
      End
   End
End
Attribute VB_Name = "frmContactDetail"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private m_oContacts As cio.CContacts

Public Property Set Contacts(oNew As cio.CContacts)
    On Error GoTo ProcError
    Set m_oContacts = oNew
    Exit Property
ProcError:
    RaiseError "CIX.frmContactDetail.Contacts"
    Exit Property
End Property

Public Property Get Contacts() As cio.CContacts
    Set Contacts = m_oContacts
End Property

Public Property Set SelectedContact(oNew As cio.CContact)
    Me.lstContacts.BoundText = oNew.UNID
    lstContacts_RowChange
End Property

Private Sub btnClose_Click()
    Me.Hide
End Sub

Private Sub Form_Load()
    Me.lstContacts.Array = Me.Contacts.GetList()
    Me.tabDetail.Tab = 0
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set m_oContacts = Nothing
End Sub

Private Sub lstContactDetail_GotFocus()
    'Me.lstContacts.SetFocus
End Sub

Private Sub lstContactDetail_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Me.lstContactDetail.SelectedItem = Me.lstContactDetail.RowContaining(Y)
    'Me.PopupMenu Me.mnuMain
End Sub

Private Sub Form_Activate()
    If Me.lstContacts.Array.Count(1) > 0 Then
        Me.lstContacts.SetFocus
    End If
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    Me.Hide
End Sub

Function lCountChrs(xSource As String, xSearch As String) As Long
'returns the number of instances
'of xSearch in xSource

    Dim iPos As Integer
    Dim l As Long
    
    iPos = InStr(xSource, xSearch)
    While iPos
        l = l + 1
        iPos = InStr(iPos + 1, xSource, xSearch)
    Wend
    lCountChrs = l
End Function

Public Sub GetSummary(oContact As cio.CContact)
'gets a summary string for the specified contact
    On Error GoTo ProcError
    Dim xToken As String
    
    xToken = "{<ADDRESSTYPENAME> Address}"
    
    Me.lblAddressType.Caption = oContact.GetDetail(xToken)
    
    If Me.lblAddressType.Caption = Empty Then
        Me.lblAddressType.Caption = vbCrLf
    End If
    
    xToken = "{<PREFIX> }{<FIRSTNAME> }{<MIDDLENAME> }{<LASTNAME> }{<SUFFIX>}" & vbCrLf
    If InStr(UCase(oContact.AddressTypeName), "BUSINESS") Then
        xToken = xToken & "{<TITLE>}" & vbCrLf & _
            "{<COMPANY>}" & vbCrLf
    End If
        
    xToken = xToken & "{<STREET1>}" & vbCrLf & _
            "{<STREET2>}" & vbCrLf & _
            "{<STREET3>}" & vbCrLf & _
            "{<CITY>{, }<STATE>}{  <ZIPCODE>}"
            
    Me.txtSummary.Text = oContact.GetDetail(xToken)
    
    xToken = "Phone: {<PHONE>}" & vbCrLf & _
        "Fax: {<FAX>}" & vbCrLf & _
        "EMail: {<EMAILADDRESS>}"
        
    Me.txtSummary.Text = Me.txtSummary.Text & vbCrLf & vbCrLf & _
        vbCrLf & oContact.GetDetail(xToken)
    Exit Sub
ProcError:
    RaiseError "CIX.frmContactDetail.GetSummary"
    Exit Sub
End Sub
'
'Public Sub GetSummary(oContact As CIO.CContact)
''gets a summary string for the specified contact
'    On Error GoTo ProcError
'    Dim xToken As String
'    xToken = "{<ADDRESSTYPENAME> Address}" & vbCrLf & _
'        "{<PREFIX> }{<FIRSTNAME> }{<MIDDLENAME> }{<LASTNAME> }{<SUFFIX>}" & vbCrLf
'    If InStr(UCase(oContact.AddressTypeName), "BUSINESS") Then
'        xToken = xToken & "{<TITLE>}" & vbCrLf & _
'            "{<COMPANY>}" & vbCrLf
'    End If
'
'    xToken = xToken & "{<STREET1>}" & vbCrLf & _
'            "{<STREET2>}" & vbCrLf & _
'            "{<STREET3>}" & vbCrLf & _
'            "{<CITY>{, }<STATE>}{  <ZIPCODE>}"
'
'    Me.txtSummary.Text = oContact.GetDetail(xToken)
'
'    xToken = "Phone: {<PHONE>}" & vbCrLf & _
'        "Fax: {<FAX>}" & vbCrLf & _
'        "EMail: {<EMAILADDRESS>}"
'    Me.txtSummaryNumbers.Text = oContact.GetDetail(xToken)
'    Exit Sub
'ProcError:
'    RaiseError "CIX.frmContactDetail.GetSummary"
'    Exit Sub
'End Sub

Public Sub GetDetail(oContact As cio.CContact)
'loads the field values for the specified contact
    Dim xTemp As String
    Dim oCustFld As cio.CCustomField
    Dim i As Integer
    Dim xVal0 As String
    Dim oDetail As XArrayDB
    
    On Error GoTo ProcError
    
    On Error Resume Next
    With oContact
        'name/value array not yet built - do it
        On Error GoTo ProcError
        Set oDetail = New XArrayDB
        oDetail.ReDim 0, (24 + .CustomFields.Count), 0, 1
    
        oDetail(0, 0) = "Display Name"
        oDetail(1, 0) = "Prefix"
        oDetail(2, 0) = "First Name"
        oDetail(3, 0) = "Middle Name"
        oDetail(4, 0) = "Last Name"
        oDetail(5, 0) = "Suffix"
        oDetail(6, 0) = "Full Name"
        oDetail(7, 0) = "Initials"
        oDetail(8, 0) = "Salutation"
        oDetail(9, 0) = "Title"
        oDetail(10, 0) = "Department"
        oDetail(11, 0) = "Company"
        oDetail(12, 0) = "Address Type"
        oDetail(13, 0) = "Street1"
        oDetail(14, 0) = "Street2"
        oDetail(15, 0) = "Street3"
        oDetail(16, 0) = "Additional"
        oDetail(17, 0) = "City"
        oDetail(18, 0) = "State"
        oDetail(19, 0) = "Zip Code"
        oDetail(20, 0) = "Country"
        oDetail(21, 0) = "Phone"
        oDetail(22, 0) = "Phone Extension"
        oDetail(23, 0) = "Fax"
        oDetail(24, 0) = "EMail Address"
        
        oDetail(0, 1) = .DisplayName
        oDetail(1, 1) = .Prefix
        oDetail(2, 1) = .FirstName
        oDetail(3, 1) = .MiddleName
        oDetail(4, 1) = .LastName
        oDetail(5, 1) = .Suffix
        oDetail(6, 1) = .FullName
        oDetail(7, 1) = .Initials
        oDetail(8, 1) = .Salutation
        oDetail(9, 1) = .Title
        oDetail(10, 1) = .Department
        oDetail(11, 1) = .Company
        oDetail(12, 1) = .AddressTypeName
        oDetail(13, 1) = .Street1
        oDetail(14, 1) = .Street2
        oDetail(15, 1) = .Street3
        oDetail(16, 1) = .AdditionalInformation
        oDetail(17, 1) = .City
        oDetail(18, 1) = .State
        oDetail(19, 1) = .ZipCode
        oDetail(20, 1) = .Country
        oDetail(21, 1) = .Phone
        oDetail(22, 1) = .PhoneExtension
        oDetail(23, 1) = .Fax
        oDetail(24, 1) = .EMailAddress
        
        For Each oCustFld In .CustomFields
            i = i + 1
            oDetail(24 + i, 0) = oCustFld.Name
            oDetail(24 + i, 1) = oCustFld.Value
        Next oCustFld
    End With

    Me.lstContactDetail.Array = oDetail
    Me.lstContactDetail.ReBind
    Exit Sub
ProcError:
    g_oError.RaiseError "CIX.frmContactDetail.GetDetail"
    Exit Sub
End Sub

Private Sub lstAddressTypes_DblClick()
'do what happens when OK is clicked
    btnClose_Click
End Sub

Private Sub lstContacts_RowChange()
    Dim oContact As cio.CContact
    
    On Error GoTo ProcError
    If Me.lstContacts.BoundText <> Empty Then
        Set oContact = m_oContacts.Item(Me.lstContacts.BoundText)
    End If
    
    If Not oContact Is Nothing Then
        GetSummary oContact
        GetDetail oContact
    End If
    Exit Sub
ProcError:
    ShowError
    Exit Sub
End Sub

Private Sub tabDetail_Click(PreviousTab As Integer)
    On Error Resume Next
    Me.lstContacts.SetFocus
End Sub

Private Sub ClearContacts()
    Set m_oContacts = Nothing
End Sub

