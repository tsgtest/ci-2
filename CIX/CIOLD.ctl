VERSION 5.00
Object = "{0D62353D-DBA2-11D1-B5DF-0060976089D0}#6.0#0"; "TDBL6.OCX"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.UserControl CI 
   Alignable       =   -1  'True
   BackStyle       =   0  'Transparent
   ClientHeight    =   6510
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   8205
   DefaultCancel   =   -1  'True
   BeginProperty Font 
      Name            =   "MS Sans Serif"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   KeyPreview      =   -1  'True
   LockControls    =   -1  'True
   ScaleHeight     =   6510
   ScaleWidth      =   8205
   ToolboxBitmap   =   "CIOLD.ctx":0000
   Begin VB.CommandButton btnGetMofoContacts 
      Caption         =   "Other Mailbo&xes"
      Height          =   420
      Left            =   6090
      TabIndex        =   56
      Top             =   6525
      Width           =   1980
   End
   Begin VB.CommandButton cmdCancel 
      BackColor       =   &H00E0E0E0&
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   400
      Left            =   6975
      TabIndex        =   15
      Top             =   6030
      Width           =   1100
   End
   Begin VB.Frame fraFolders 
      BorderStyle     =   0  'None
      Caption         =   "FOLDERS"
      Height          =   6240
      Left            =   0
      TabIndex        =   48
      Top             =   345
      Width           =   8580
      Begin VB.TextBox txtFilterValue1 
         Appearance      =   0  'Flat
         Height          =   315
         Left            =   2880
         TabIndex        =   5
         Top             =   5730
         Width           =   2655
      End
      Begin VB.CommandButton btnQuickSearch 
         Height          =   315
         Left            =   5580
         MaskColor       =   &H8000000F&
         Picture         =   "CIOLD.ctx":0312
         Style           =   1  'Graphical
         TabIndex        =   6
         TabStop         =   0   'False
         ToolTipText     =   "Quick Search"
         Top             =   5730
         UseMaskColor    =   -1  'True
         Width           =   450
      End
      Begin MSComctlLib.ImageList ilTree 
         Left            =   7755
         Top             =   60
         _ExtentX        =   1005
         _ExtentY        =   1005
         BackColor       =   -2147483643
         ImageWidth      =   16
         ImageHeight     =   16
         MaskColor       =   12632256
         UseMaskColor    =   0   'False
         _Version        =   393216
         BeginProperty Images {2C247F25-8591-11D1-B16A-00C0F0283628} 
            NumListImages   =   8
            BeginProperty ListImage1 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "CIOLD.ctx":05C4
               Key             =   "Backend"
            EndProperty
            BeginProperty ListImage2 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "CIOLD.ctx":0A20
               Key             =   "ClosedDefaultFolder"
            EndProperty
            BeginProperty ListImage3 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "CIOLD.ctx":0E74
               Key             =   ""
            EndProperty
            BeginProperty ListImage4 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "CIOLD.ctx":12C8
               Key             =   ""
            EndProperty
            BeginProperty ListImage5 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "CIOLD.ctx":171C
               Key             =   "ClosedStore"
            EndProperty
            BeginProperty ListImage6 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "CIOLD.ctx":1B78
               Key             =   "OpenStore"
            EndProperty
            BeginProperty ListImage7 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "CIOLD.ctx":1FD4
               Key             =   "OpenFolder"
            EndProperty
            BeginProperty ListImage8 {2C247F27-8591-11D1-B16A-00C0F0283628} 
               Picture         =   "CIOLD.ctx":2430
               Key             =   "ClosedFolder"
            EndProperty
         EndProperty
      End
      Begin MSComctlLib.TreeView tvFolders 
         Height          =   4965
         Left            =   120
         TabIndex        =   1
         Top             =   405
         Width           =   7980
         _ExtentX        =   14076
         _ExtentY        =   8758
         _Version        =   393217
         HideSelection   =   0   'False
         Indentation     =   706
         LabelEdit       =   1
         LineStyle       =   1
         Style           =   7
         ImageList       =   "ilTree"
         BorderStyle     =   1
         Appearance      =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin TrueDBList60.TDBCombo cmbFilterFld1 
         Height          =   375
         Left            =   105
         OleObjectBlob   =   "CIOLD.ctx":288C
         TabIndex        =   3
         Top             =   5715
         Width           =   1455
      End
      Begin TrueDBList60.TDBCombo cmbOp1 
         Height          =   330
         Left            =   1530
         OleObjectBlob   =   "CIOLD.ctx":4740
         TabIndex        =   4
         Top             =   5715
         Width           =   1395
      End
      Begin VB.Line Line2 
         BorderColor     =   &H80000005&
         X1              =   15
         X2              =   8530
         Y1              =   0
         Y2              =   0
      End
      Begin VB.Label lblStatusMsg 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         ForeColor       =   &H00FF0000&
         Height          =   240
         Left            =   3705
         TabIndex        =   52
         Top             =   165
         Visible         =   0   'False
         Width           =   4320
      End
      Begin VB.Label lblQuickSearch 
         BackStyle       =   0  'Transparent
         Caption         =   "Find Contacts &Where:"
         Height          =   285
         Left            =   150
         TabIndex        =   2
         Top             =   5505
         Width           =   2025
      End
      Begin VB.Label lblContactLists 
         BackColor       =   &H8000000A&
         BackStyle       =   0  'Transparent
         Caption         =   "Contact Folders/&Directories:"
         DataField       =   "s"
         Height          =   240
         Left            =   165
         TabIndex        =   0
         Top             =   165
         Width           =   2130
      End
   End
   Begin VB.Frame fraToolbar 
      BorderStyle     =   0  'None
      Height          =   450
      Left            =   -15
      TabIndex        =   51
      Top             =   -105
      Width           =   8265
      Begin VB.CommandButton btnHelp 
         Height          =   345
         Left            =   7890
         MaskColor       =   &H8000000F&
         Picture         =   "CIOLD.ctx":65E9
         Style           =   1  'Graphical
         TabIndex        =   55
         Top             =   120
         Width           =   345
      End
      Begin VB.CheckBox chkGroups 
         Caption         =   "&Groups"
         Enabled         =   0   'False
         Height          =   345
         Left            =   6885
         Style           =   1  'Graphical
         TabIndex        =   14
         TabStop         =   0   'False
         ToolTipText     =   "Edit Contact Integration Groups"
         Top             =   120
         Width           =   1020
      End
      Begin VB.CommandButton btnOptions 
         Caption         =   "&Options"
         Height          =   345
         Left            =   6030
         Style           =   1  'Graphical
         TabIndex        =   13
         TabStop         =   0   'False
         Top             =   120
         UseMaskColor    =   -1  'True
         Width           =   870
      End
      Begin VB.CommandButton btnEdit 
         Appearance      =   0  'Flat
         Caption         =   "&Edit"
         Enabled         =   0   'False
         Height          =   345
         Left            =   5220
         Style           =   1  'Graphical
         TabIndex        =   12
         TabStop         =   0   'False
         ToolTipText     =   "Edit selected available contact in native application."
         Top             =   120
         UseMaskColor    =   -1  'True
         Width           =   825
      End
      Begin VB.CommandButton btnAddContactInNative 
         Caption         =   "&New"
         Height          =   345
         Left            =   4410
         Style           =   1  'Graphical
         TabIndex        =   11
         TabStop         =   0   'False
         ToolTipText     =   "Add contact to selected application"
         Top             =   120
         UseMaskColor    =   -1  'True
         Width           =   825
      End
      Begin VB.CommandButton btnRefresh 
         Caption         =   "&Refresh"
         Height          =   345
         Left            =   3510
         MaskColor       =   &H00808080&
         Style           =   1  'Graphical
         TabIndex        =   10
         TabStop         =   0   'False
         ToolTipText     =   "Clear any existing filter and refresh both folders and available contacts (F5)"
         Top             =   120
         UseMaskColor    =   -1  'True
         Width           =   915
      End
      Begin VB.CommandButton btnSearchNative 
         Caption         =   "Nati&ve Find"
         Height          =   345
         Left            =   2430
         MaskColor       =   &H00808080&
         Style           =   1  'Graphical
         TabIndex        =   9
         TabStop         =   0   'False
         ToolTipText     =   "Find contacts in the selected application"
         Top             =   120
         UseMaskColor    =   -1  'True
         Width           =   1095
      End
      Begin VB.CommandButton btnSearch 
         Caption         =   "&Find"
         Height          =   345
         Left            =   1530
         MaskColor       =   &H00808080&
         Style           =   1  'Graphical
         TabIndex        =   8
         TabStop         =   0   'False
         ToolTipText     =   "Find contacts in the selected folder or database"
         Top             =   120
         UseMaskColor    =   -1  'True
         Width           =   915
      End
      Begin VB.CommandButton btnTogglePanels 
         Caption         =   "&Show Contacts"
         Height          =   345
         Left            =   15
         Style           =   1  'Graphical
         TabIndex        =   7
         TabStop         =   0   'False
         Top             =   120
         UseMaskColor    =   -1  'True
         Width           =   1530
      End
   End
   Begin VB.Frame fraListings 
      BorderStyle     =   0  'None
      Caption         =   "LISTINGS"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   6195
      Left            =   30
      TabIndex        =   36
      Top             =   345
      Visible         =   0   'False
      Width           =   8190
      Begin VB.Frame fraContacts 
         BorderStyle     =   0  'None
         Caption         =   "Contacts"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   5325
         Left            =   4470
         TabIndex        =   37
         Top             =   195
         Width           =   4065
         Begin VB.CommandButton btnMoveDown 
            Height          =   225
            Left            =   3315
            Picture         =   "CIOLD.ctx":695B
            Style           =   1  'Graphical
            TabIndex        =   53
            Top             =   630
            UseMaskColor    =   -1  'True
            Visible         =   0   'False
            Width           =   255
         End
         Begin VB.CommandButton btnMoveUp 
            Height          =   225
            Left            =   3060
            Picture         =   "CIOLD.ctx":6A4D
            Style           =   1  'Graphical
            TabIndex        =   54
            Top             =   630
            UseMaskColor    =   -1  'True
            Visible         =   0   'False
            Width           =   255
         End
         Begin VB.ListBox lstContacts 
            Height          =   900
            Index           =   0
            Left            =   570
            MultiSelect     =   2  'Extended
            OLEDropMode     =   1  'Manual
            TabIndex        =   23
            Top             =   870
            Width           =   3030
         End
         Begin VB.ListBox lstContacts 
            Height          =   900
            Index           =   1
            Left            =   570
            MultiSelect     =   2  'Extended
            OLEDropMode     =   1  'Manual
            TabIndex        =   27
            Top             =   2040
            Width           =   3030
         End
         Begin VB.ListBox lstContacts 
            Height          =   900
            Index           =   2
            Left            =   570
            MultiSelect     =   2  'Extended
            OLEDropMode     =   1  'Manual
            TabIndex        =   31
            Top             =   3225
            Width           =   3030
         End
         Begin VB.ListBox lstContacts 
            Height          =   900
            Index           =   3
            Left            =   570
            MultiSelect     =   2  'Extended
            OLEDropMode     =   1  'Manual
            TabIndex        =   35
            Top             =   4425
            Width           =   3030
         End
         Begin VB.CommandButton btnAdd 
            Appearance      =   0  'Flat
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   284
            Index           =   0
            Left            =   30
            Picture         =   "CIOLD.ctx":6B3F
            Style           =   1  'Graphical
            TabIndex        =   20
            Top             =   870
            UseMaskColor    =   -1  'True
            Width           =   405
         End
         Begin VB.CommandButton btnDelete 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   284
            Index           =   0
            Left            =   30
            Picture         =   "CIOLD.ctx":6C5D
            Style           =   1  'Graphical
            TabIndex        =   21
            Top             =   1200
            UseMaskColor    =   -1  'True
            Width           =   405
         End
         Begin VB.CommandButton btnDelete 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   284
            Index           =   1
            Left            =   30
            Picture         =   "CIOLD.ctx":6D93
            Style           =   1  'Graphical
            TabIndex        =   25
            Top             =   2385
            UseMaskColor    =   -1  'True
            Width           =   405
         End
         Begin VB.CommandButton btnAdd 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   284
            Index           =   1
            Left            =   30
            Picture         =   "CIOLD.ctx":6EC9
            Style           =   1  'Graphical
            TabIndex        =   24
            Top             =   2055
            UseMaskColor    =   -1  'True
            Width           =   405
         End
         Begin VB.CommandButton btnDelete 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   284
            Index           =   2
            Left            =   30
            Picture         =   "CIOLD.ctx":6FE7
            Style           =   1  'Graphical
            TabIndex        =   29
            Top             =   3570
            UseMaskColor    =   -1  'True
            Width           =   405
         End
         Begin VB.CommandButton btnAdd 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   284
            Index           =   2
            Left            =   30
            Picture         =   "CIOLD.ctx":711D
            Style           =   1  'Graphical
            TabIndex        =   28
            Top             =   3240
            UseMaskColor    =   -1  'True
            Width           =   405
         End
         Begin VB.CommandButton btnDelete 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   284
            Index           =   3
            Left            =   30
            Picture         =   "CIOLD.ctx":723B
            Style           =   1  'Graphical
            TabIndex        =   33
            Top             =   4785
            UseMaskColor    =   -1  'True
            Width           =   405
         End
         Begin VB.CommandButton btnAdd 
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   284
            Index           =   3
            Left            =   30
            Picture         =   "CIOLD.ctx":7371
            Style           =   1  'Graphical
            TabIndex        =   32
            Top             =   4440
            UseMaskColor    =   -1  'True
            Width           =   405
         End
         Begin VB.Label lblSelContacts 
            BackStyle       =   0  'Transparent
            Caption         =   "&To:"
            ForeColor       =   &H00000000&
            Height          =   270
            Index           =   0
            Left            =   600
            TabIndex        =   22
            Top             =   660
            Width           =   345
         End
         Begin VB.Label lblSelContacts 
            BackStyle       =   0  'Transparent
            Caption         =   "Fro&m:"
            ForeColor       =   &H00000000&
            Height          =   255
            Index           =   1
            Left            =   600
            TabIndex        =   26
            Top             =   1830
            Width           =   495
         End
         Begin VB.Label lblSelContacts 
            BackStyle       =   0  'Transparent
            Caption         =   "&CC:"
            ForeColor       =   &H00000000&
            Height          =   255
            Index           =   2
            Left            =   585
            TabIndex        =   30
            Top             =   3015
            Width           =   345
         End
         Begin VB.Label lblSelContacts 
            BackStyle       =   0  'Transparent
            Caption         =   "&BCC:"
            Height          =   255
            Index           =   3
            Left            =   600
            TabIndex        =   34
            Top             =   4215
            Width           =   420
         End
      End
      Begin TrueDBList60.TDBList lstListings 
         Height          =   4455
         Left            =   90
         OleObjectBlob   =   "CIOLD.ctx":748F
         TabIndex        =   19
         Top             =   1065
         Width           =   4320
      End
      Begin VB.CommandButton cmdOK 
         Caption         =   "&Insert"
         Height          =   405
         Left            =   5775
         TabIndex        =   50
         Top             =   5685
         Width           =   1100
      End
      Begin VB.ComboBox cbxAddressType 
         Height          =   330
         Left            =   90
         Style           =   2  'Dropdown List
         TabIndex        =   17
         Top             =   360
         Width           =   4320
      End
      Begin VB.Frame fraGroups 
         BorderStyle     =   0  'None
         Caption         =   "GROUPS"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   5340
         Left            =   4455
         TabIndex        =   38
         Top             =   195
         Visible         =   0   'False
         Width           =   4065
         Begin VB.CommandButton btnDeleteGroup 
            Enabled         =   0   'False
            Height          =   315
            Left            =   3315
            Picture         =   "CIOLD.ctx":A8C4
            Style           =   1  'Graphical
            TabIndex        =   42
            ToolTipText     =   "Delete A Group"
            Top             =   165
            UseMaskColor    =   -1  'True
            Width           =   330
         End
         Begin VB.CommandButton btnAddGroup 
            Height          =   315
            Left            =   2985
            Picture         =   "CIOLD.ctx":ABD6
            Style           =   1  'Graphical
            TabIndex        =   41
            ToolTipText     =   "Add A Group"
            Top             =   165
            UseMaskColor    =   -1  'True
            Width           =   330
         End
         Begin VB.CommandButton btnAddMember 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   284
            Left            =   75
            Picture         =   "CIOLD.ctx":AF8C
            Style           =   1  'Graphical
            TabIndex        =   46
            Top             =   870
            UseMaskColor    =   -1  'True
            Width           =   405
         End
         Begin VB.ComboBox cbxGroups 
            Height          =   330
            Left            =   600
            Style           =   2  'Dropdown List
            TabIndex        =   40
            Top             =   165
            Width           =   2355
         End
         Begin VB.CommandButton btnDeleteMember 
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   284
            Left            =   75
            Picture         =   "CIOLD.ctx":B0AA
            Style           =   1  'Graphical
            TabIndex        =   45
            Top             =   1200
            UseMaskColor    =   -1  'True
            Width           =   405
         End
         Begin VB.ListBox lstGroupMembers 
            Height          =   4260
            Left            =   600
            MultiSelect     =   2  'Extended
            TabIndex        =   44
            Top             =   870
            Width           =   3030
         End
         Begin VB.Label lblGroupMembers 
            Caption         =   "Group &Members:"
            BeginProperty Font 
               Name            =   "MS Sans Serif"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   645
            TabIndex        =   43
            Top             =   660
            Width           =   1260
         End
         Begin VB.Label lblGroup 
            BackColor       =   &H8000000A&
            BackStyle       =   0  'Transparent
            Caption         =   "Grou&p:"
            Height          =   240
            Left            =   630
            TabIndex        =   39
            Top             =   -45
            Width           =   855
         End
      End
      Begin VB.Line Line1 
         BorderColor     =   &H80000005&
         X1              =   0
         X2              =   8515
         Y1              =   0
         Y2              =   0
      End
      Begin VB.Label lblStatus 
         Appearance      =   0  'Flat
         BackColor       =   &H8000000A&
         BackStyle       =   0  'Transparent
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H80000008&
         Height          =   390
         Left            =   120
         TabIndex        =   49
         Top             =   5625
         Width           =   5565
      End
      Begin VB.Label lblAddressType 
         BackColor       =   &H8000000A&
         BackStyle       =   0  'Transparent
         Caption         =   "Address T&ype:"
         Height          =   255
         Left            =   120
         TabIndex        =   16
         Top             =   150
         Width           =   1980
      End
      Begin VB.Label lblFilter 
         Alignment       =   1  'Right Justify
         BackColor       =   &H8000000A&
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   240
         Left            =   6135
         TabIndex        =   47
         Top             =   390
         Visible         =   0   'False
         Width           =   990
      End
      Begin VB.Label lblEntries 
         BackStyle       =   0  'Transparent
         Caption         =   "Cont&acts:"
         Height          =   225
         Left            =   120
         TabIndex        =   18
         Top             =   855
         Width           =   4230
      End
   End
   Begin CIX.ProgressBox pbxLoad 
      Height          =   1695
      Left            =   1680
      TabIndex        =   57
      Top             =   2070
      Visible         =   0   'False
      Width           =   4725
      _ExtentX        =   8334
      _ExtentY        =   2990
      Title           =   "Loading Contacts"
   End
   Begin VB.Menu mnuFolders 
      Caption         =   "Folders"
      Visible         =   0   'False
      Begin VB.Menu mnuFolders_SetAsDefault 
         Caption         =   "&Select Current Folder On Startup"
      End
      Begin VB.Menu mnuFolders_LoadOnStartup 
         Caption         =   "&Load Current Folder On Startup"
      End
      Begin VB.Menu mnuFolders_RemoveDefault 
         Caption         =   "&Remove Default Folder"
      End
      Begin VB.Menu mnuFolders_Sep1 
         Caption         =   "-"
      End
      Begin VB.Menu mnuFolders_Custom1 
         Caption         =   "Custom Menu Item 1"
      End
      Begin VB.Menu mnuFolders_Custom2 
         Caption         =   "Custom Menu Item 2"
      End
      Begin VB.Menu mnuFolders_Custom3 
         Caption         =   "Custom Menu Item 3"
      End
      Begin VB.Menu mnuFolders_Custom4 
         Caption         =   "Custom Menu Item 4"
      End
      Begin VB.Menu mnuFolders_Custom5 
         Caption         =   "Custom Menu Item 5"
      End
   End
   Begin VB.Menu mnuContacts 
      Caption         =   "Contacts"
      Visible         =   0   'False
      Begin VB.Menu mnuContacts_ToggleContactList 
         Caption         =   "Expand Available Contacts &List"
         Shortcut        =   ^W
      End
      Begin VB.Menu mnuContacts_SelectAll 
         Caption         =   "Select &All"
         Shortcut        =   ^A
      End
      Begin VB.Menu mnuContacts_ShowDetail 
         Caption         =   "Show &Detail"
      End
      Begin VB.Menu mnuContacts_Sep1 
         Caption         =   "-"
      End
      Begin VB.Menu mnuContacts_MoveUp 
         Caption         =   "Move Selected Contacts &Up"
      End
      Begin VB.Menu mnuContacts_MoveDown 
         Caption         =   "Move Selected Contacts Do&wn"
      End
      Begin VB.Menu mnuContacts_Sep2 
         Caption         =   "-"
      End
      Begin VB.Menu mnuContacts_AddGroup 
         Caption         =   "Add &Group"
         Enabled         =   0   'False
         Shortcut        =   ^G
      End
      Begin VB.Menu mnuContacts_DeleteGroup 
         Caption         =   "Delete Gro&up"
         Enabled         =   0   'False
         Shortcut        =   ^U
      End
   End
   Begin VB.Menu mnuHelp 
      Caption         =   "Help"
      Visible         =   0   'False
      Begin VB.Menu mnuHelp_Contents 
         Caption         =   "&Contact Integration Help..."
         Shortcut        =   {F1}
      End
      Begin VB.Menu mnuHelp_Sep1 
         Caption         =   "-"
      End
      Begin VB.Menu mnuHelp_About 
         Caption         =   "&About Contact Integration"
         Shortcut        =   ^{F6}
      End
   End
End
Attribute VB_Name = "CI"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Private Declare Function GetTickCount Lib "kernel32" () As Long

Enum CIPanels
    ciPanel_folders = 1
    ciPanel_Contacts = 2
    ciPanel_Groups = 3
End Enum

Enum ciSearchTypes
    ciSearchType_None = 0
    ciSearchType_Dialog = 1
    ciSearchType_Native = 2
    ciSearchType_Quick = 3
    ciSearchType_Refresh = 4
End Enum

Private Enum ciListBoxes
    ciListBoxes_Listings = -1
    ciListBoxes_To = 0
    ciListBoxes_From = 1
    ciListBoxes_CC = 2
    ciListBoxes_BCC = 3
    ciListBoxes_GroupMembers = 4
End Enum

Enum cixErrs
    cixErr_ObjectExpected = vbError + 3512 + 1
End Enum

Private Const ciUserIniKey_LoadDefaultOnStartup As String = "LoadDefaultOnStartup"
Private Const ciUserIniKey_DefaultNode As String = "DefaultNode"
Private Const ciListingsNarrow As Single = 4320

Private m_oListings As CIO.CListings
Private m_oAddresses As CIO.CAddresses
Private m_oContacts As CIO.CContacts
Private m_oUNID As CIO.CUNID
Private m_iDataTo As CIO.ciRetrieveData
Private m_iDataFrom As CIO.ciRetrieveData
Private m_iDataCC As CIO.ciRetrieveData
Private m_iDataBCC As CIO.ciRetrieveData
Private m_bShowBackendNodes As Boolean
Private m_iAlerts As CIO.ciAlerts
Private m_iSelectionLists As CIX.ciSelectionLists
Private m_bMouseDown As Boolean
Private m_bLoadCancelled As Boolean
Private m_iMaxContacts As Integer
Private m_bInitialized As Boolean
Private m_iCurList As ciListBoxes
Private m_bShowedContacts As Boolean
Private m_xCustomFunction1 As String
Private m_xCustomFunction2 As String
Private m_bMenuRequested As Boolean
Private WithEvents m_oEvents As CIO.CEventGenerator
Attribute m_oEvents.VB_VarHelpID = -1

Public Event InsertionCancelled()
Public Event InsertionOK(oContacts As CIO.CContacts)

Public Enum ciSelectionLists
    ciSelectionList_To = 1
    ciSelectionList_From = 2
    ciSelectionList_CC = 4
    ciSelectionList_BCC = 8
End Enum

Public Property Let MaxContacts(ByVal iNew As Integer)
    m_iMaxContacts = iNew
End Property

Public Property Get MaxContacts() As Integer
    MaxContacts = m_iMaxContacts
End Property

Public Property Let OKButtonCaption(xNew As String)
    UserControl.cmdOK.Caption = xNew
End Property

Public Property Get OKButtonCaption() As String
    OKButtonCaption = UserControl.cmdOK.Caption
End Property

Public Property Let ShowBackendNodes(bNew As Boolean)
    m_bShowBackendNodes = bNew
End Property

Public Property Get ShowBackendNodes() As Boolean
    ShowBackendNodes = m_bShowBackendNodes
End Property

Public Property Let DataRetrievedForTo(iNew As ciRetrieveData)
    m_iDataTo = iNew
End Property

Public Property Get DataRetrievedForTo() As ciRetrieveData
    DataRetrievedForTo = m_iDataTo
End Property

Public Property Let DataRetrievedForFrom(iNew As ciRetrieveData)
    m_iDataFrom = iNew
End Property

Public Property Get DataRetrievedForFrom() As ciRetrieveData
    DataRetrievedForFrom = m_iDataFrom
End Property

Public Property Let DataRetrievedForCC(iNew As ciRetrieveData)
    m_iDataCC = iNew
End Property

Public Property Get DataRetrievedForCC() As ciRetrieveData
    DataRetrievedForCC = m_iDataCC
End Property

Public Property Let DataRetrievedForBCC(iNew As ciRetrieveData)
    m_iDataBCC = iNew
End Property

Public Property Get DataRetrievedForBCC() As ciRetrieveData
    DataRetrievedForBCC = m_iDataBCC
End Property

Public Property Let Alerts(iNew As ciAlerts)
    m_iAlerts = iNew
End Property

Public Property Get Alerts() As ciAlerts
    Alerts = m_iAlerts
End Property

Public Property Let SelectionLists(iNew As ciSelectionLists)
    m_iSelectionLists = iNew
    SetSelectionLists iNew
End Property

Public Property Get SelectionLists() As ciSelectionLists
    SelectionLists = m_iSelectionLists
End Property

Public Property Get SelectedContacts() As CIO.CContacts
    Set SelectedContacts = m_oContacts
End Property

Private Sub btnAdd_Click(Index As Integer)
    With UserControl.btnAdd
        If Not .Item(Index).Visible Then
            Exit Sub
        End If
        
        Select Case Index
            Case 1
                Me.SelectionList = ciSelectionList_From
            Case 2
                Me.SelectionList = ciSelectionList_CC
            Case 3
                Me.SelectionList = ciSelectionList_BCC
            Case Else
                Me.SelectionList = ciSelectionList_To
        End Select
    End With
    AddSelContacts
End Sub

Private Property Get SelectionListControl() As VB.ListBox
    On Error GoTo ProcError
    With UserControl.lstContacts
        Select Case Me.SelectionList
            Case ciSelectionList_From
                Set SelectionListControl = .Item(1)
            Case ciSelectionList_CC
                Set SelectionListControl = .Item(2)
            Case ciSelectionList_BCC
                Set SelectionListControl = .Item(3)
            Case Else
                Set SelectionListControl = .Item(0)
        End Select
    End With
    Exit Property
ProcError:
    RaiseError "CIX.CI.SelectionListControl"
    Exit Property
End Property

Public Property Let SelectionList(iNew As ciSelectionLists)
'sets up dialog to highlight new selection list
    Dim i As Integer
    Dim iSelListBoxIndex As Integer
    Dim iPrevSelListBoxIndex As Integer
    Static iPrevSelList As Integer
    
    On Error GoTo ProcError
    
    If iPrevSelList = iNew Then
        'selection list has not changed - exit
        Exit Property
    ElseIf (iNew And SelectionLists) = 0 Then
        'the selection list that has been requested
        'to be the default selection list is not
        'among the selection lists that are displayed
        'in the dialog
        Exit Property
    End If
    
    'get list box that corresponds to selection list
    Select Case iNew
        Case ciSelectionList_From
            iSelListBoxIndex = 1
        Case ciSelectionList_CC
            iSelListBoxIndex = 2
        Case ciSelectionList_BCC
            iSelListBoxIndex = 3
        Case Else
            iSelListBoxIndex = 0
    End Select

    'get list box that corresponds to
    'the previous selection list
    Select Case iPrevSelList
        Case ciSelectionList_From
            iPrevSelListBoxIndex = 1
        Case ciSelectionList_CC
            iPrevSelListBoxIndex = 2
        Case ciSelectionList_BCC
            iPrevSelListBoxIndex = 3
        Case Else
            iPrevSelListBoxIndex = 0
    End Select

    With UserControl
        If iPrevSelList > 0 Then
            'remove all selections from previous listbox
            With .lstContacts(iPrevSelListBoxIndex)
                For i = 0 To .ListCount - 1
                    .Selected(i) = False
                Next
            End With
            
            'change previous list label color to black
            .lblSelContacts _
                .Item(iPrevSelListBoxIndex).ForeColor = vbButtonText
        End If
        
        'ensure that the appropriate
        'add button is made the default
        .btnAdd.Item(iSelListBoxIndex).Default = True
        
        'set new selection list label color to blue
        .lblSelContacts.Item(iSelListBoxIndex).ForeColor = vbBlue
            
        'enable/disable delete buttons
        For i = 0 To 3
            .btnDelete.Item(i).Enabled = (iSelListBoxIndex = i) And _
                (.lstContacts(i).ListCount > 0) And (.lstContacts(i).ListIndex > -1)
        Next i
        
        'move up down buttons into correct position
        With UserControl
            .btnMoveUp.Top = .lstContacts(iSelListBoxIndex).Top - _
                             .btnMoveUp.Height
            .btnMoveDown.Top = .btnMoveUp.Top
            .btnMoveUp.Visible = True
            .btnMoveDown.Visible = True
        End With
    
        iPrevSelList = iNew
    End With
    Exit Property
ProcError:
    RaiseError "CIX.CI.SelectionList"
    Exit Property
End Property

Public Property Get SelectionList() As ciSelectionLists
'returns the current selection list - i.e. the
'list that currently receives double-clicked listings
    On Error GoTo ProcError
    With UserControl.btnAdd
        If .Item(1).Default Then
            SelectionList = ciSelectionList_From
        ElseIf .Item(2).Default Then
            SelectionList = ciSelectionList_CC
        ElseIf .Item(3).Default Then
            SelectionList = ciSelectionList_BCC
        Else
            SelectionList = ciSelectionList_To
        End If
    End With
    Exit Property
ProcError:
    RaiseError "CIX.CI.SelectionList"
    Exit Property
End Property

Private Sub AddSelContacts()
'adds the selected listings to the specified list
    Dim oBEnd As CIO.ICIBackend
    Dim oContacts As CIO.CContacts
    Dim oC As CIO.CContact
    Dim oL As CIO.CListing
    Dim oAddr As CIO.CAddress
    Dim lBookmark As Long
    Dim l As Long
    Dim lNumSel As Long
    Dim lAlertThreshold As Long
    Dim iChoice As VbMsgBoxResult
    
    'used to generate a number sequence below
    Static iID As Integer
    
    On Error GoTo ProcError
    
    With UserControl
        With .lstListings
            'select 1st row if necessary
            If .SelBookmarks.Count = 0 Then
                If .Row > -1 Then
                    .SelBookmarks.Add .FirstRow + .Row
                End If
            ElseIf .SelBookmarks.Count = 1 And .SelBookmarks(0) = 0 Then
                If .Row > -1 Then
                    .SelBookmarks.Add .FirstRow + .Row
                End If
            End If
        End With
        
        'UserControl.MousePointer = vbHourglass
        
        lNumSel = .lstListings.SelBookmarks.Count
        
        'get the threshold number of selected contacts
        'for which the user should be prompted.
        If lAlertThreshold = 0 Then
            Dim oIni As CIO.CIni
            Set oIni = New CIO.CIni
            On Error Resume Next
            lAlertThreshold = oIni.GetIni("CIApplication", "AlertThreshold")
            On Error GoTo ProcError
            Set oIni = Nothing
        End If
        
        If lAlertThreshold = 0 Then
            lAlertThreshold = 15
        End If
        
        If lNumSel > lAlertThreshold Then
            'prompt before executing - alert threshold has been triggered
            iChoice = MsgBox("You are about to add " & lNumSel & _
                " contacts.  Do you want to continue?", vbQuestion + vbYesNo)
            If iChoice = vbNo Then
                UserControl.MousePointer = vbDefault
                Exit Sub
            Else
                With UserControl.pbxLoad
                    .Cancel = False
                    .Visible = True
                End With
            End If
       End If
        
        SetupForInputState False
        
        For l = 0 To lNumSel - 1
            If lNumSel > lAlertThreshold Then
                If l = 0 Then
                    SetupForInputState False
                    With UserControl.pbxLoad
                        .Title = "Adding Contacts"
                        .ShowCancelled = True
                        .Value = 0
                        .Message = "Adding contacts. Please wait... 0 of " & lNumSel
                        .Visible = True
                        .ZOrder 0
                        DoEvents
                    End With
                End If
            End If
            
            'check that we haven't reached the
            'maximum number of contacts allowed
            If Me.MaxContacts > 0 Then
                If m_oContacts.Count = Me.MaxContacts Then
                    If Me.MaxContacts = 1 Then
                        MsgBox "No more than 1 contact may be " & _
                            "inserted in this situation.", vbExclamation, App.Title
                    Else
                        MsgBox "No more than " & Me.MaxContacts & _
                            " contacts may be inserted in this situation.", vbExclamation, App.Title
                    End If
                    UserControl.MousePointer = vbDefault
                    SetupForInputState True
                    Exit Sub
                End If
            End If
            
            On Error GoTo ProcError
            lBookmark = .lstListings.SelBookmarks(l)
'           get selected listing
            Set oL = m_oListings.Item(lBookmark)
            If oL Is Nothing Then
                Err.Raise CIO.ciErr_InvalidListing, , _
                    "Listing is invalid."
            End If
            
            If m_oAddresses.Count > 0 Then
'               get selected address
                On Error Resume Next
                
                'exit if still no type is chosen
                If .cbxAddressType.ListIndex = -1 Then
                    LoadAddresses
                    UserControl.MousePointer = vbDefault
                    Exit Sub
                End If
                
                Set oAddr = m_oAddresses.Item(.cbxAddressType.ListIndex + 1)
                On Error GoTo ProcError
                If oAddr Is Nothing Then
                    Err.Raise CIO.ciErr_invalidAddress, , _
                        "Selected address is invalid."
                End If
            End If
            
'           get contact from listing and address
            Dim iDataRetrieved As CIO.ciRetrieveData
            Select Case Me.SelectionList
                Case CIX.ciSelectionList_To
                    iDataRetrieved = Me.DataRetrievedForTo
                Case CIX.ciSelectionList_From
                    iDataRetrieved = Me.DataRetrievedForFrom
                Case CIX.ciSelectionList_CC
                    iDataRetrieved = Me.DataRetrievedForCC
                Case CIX.ciSelectionList_BCC
                    iDataRetrieved = Me.DataRetrievedForBCC
            End Select
            
            Set oContacts = ListingBackend(oL).GetContacts( _
                oL, oAddr, , iDataRetrieved, Me.Alerts)
            
            If oContacts Is Nothing Then
                SetupForInputState True
                
                With UserControl
                    .pbxLoad.Visible = False
                    .MousePointer = vbDefault
                End With
                
                Exit Sub
            End If
            
'           add to contact collection
            For Each oC In oContacts
'               set selection list for contact
                oC.ContactType = Me.SelectionList
                    
'               find the last contact in collection that has the
'               same contact type - if none, add as first of group
                Dim iNumTo As Integer
                Dim iNumFrom As Integer
                Dim iNumCC As Integer
                Dim iNumBCC As Integer
                Dim lInsertAt As Long
                
                'get number of contacts in each group
                With UserControl.lstContacts
                    iNumTo = .Item(0).ListCount
                    iNumFrom = .Item(1).ListCount
                    iNumCC = .Item(2).ListCount
                    iNumBCC = .Item(3).ListCount
                End With
                
                Select Case oC.ContactType
                    Case CIX.ciSelectionList_To
                        'insert after last 'To' contact
                        lInsertAt = iNumTo + 1
                    Case CIX.ciSelectionList_From
                        lInsertAt = iNumTo + iNumFrom + 1
                    Case CIX.ciSelectionList_CC
                        lInsertAt = iNumTo + iNumFrom + iNumCC + 1
                    Case CIX.ciSelectionList_BCC
                        lInsertAt = iNumTo + iNumFrom + iNumCC + iNumBCC + 1
                End Select
                
                On Error Resume Next
                If lInsertAt <= m_oContacts.Count Then
                    m_oContacts.Add oC, lInsertAt
                Else
                    m_oContacts.Add oC
                End If
                
                If Err.Number = 0 Then
                    'get next number in the sequence
                    iID = iID + 1
                    
                    'tag the contact with this unique number
                    oC.Tag = iID
                    
'                   add full name to selected list
                    With SelectionListControl
                        .AddItem oC.DisplayName
                        .ItemData(.ListCount - 1) = iID
                    End With
                ElseIf Err.Number = 457 Then
                    MsgBox oC.FullName & " has already been added.", vbExclamation, App.Title
                Else
                    Err.Raise Err.Number, , Err.Description
                End If
            Next oC
            
           If lNumSel > lAlertThreshold Then
                UserControl.pbxLoad.Message = _
                    "Adding contacts. Please wait... " & l + 1 & " of " & lNumSel
                UserControl.pbxLoad.Value = (l + 1) / lNumSel
            End If

            If m_bLoadCancelled Then
                MsgBox "You canceled this operation.  " & l + 1 & " contacts have been added.", vbExclamation
                
                'reset for next iteration
                m_bLoadCancelled = False
                Exit For
            End If
        Next l
    End With
    
    SetupForInputState True
    
    With UserControl
        .pbxLoad.Visible = False
        .lstListings.SetFocus
        .MousePointer = vbDefault
    End With
    Exit Sub
ProcError:
    Dim lErr As Long
    Dim xDesc As String
    
    lErr = Err.Number
    xDesc = Err.Description
    
    SetupForInputState True
    
    With UserControl
        .pbxLoad.Visible = False
        .MousePointer = vbDefault
    End With
    
    Err.Number = lErr
    Err.Description = xDesc
    RaiseError "CIX.frmListings.AddSelContacts"
    Exit Sub
    Resume
End Sub

Private Sub DeleteSelContacts(Optional ByVal bPrompt As Boolean = True)
    Dim i As Integer
    Dim j As Integer
    Dim k As Integer
    Dim iChoice As VbMsgBoxResult
    Dim oContact As CIO.CContact
    Dim xMsg As String
    
    On Error GoTo ProcError
    
    With SelectionListControl()
        If bPrompt Then
            Select Case .SelCount
                Case 0
                    Exit Sub
                Case 1
                    xMsg = "Delete selected contact?"
                Case Else
                    xMsg = "Delete selected contacts?"
            End Select
            
            iChoice = MsgBox(xMsg, vbQuestion + vbYesNo, App.Title)
        End If
        
        If iChoice = vbYes Or (Not bPrompt) Then
            For i = .ListCount - 1 To 0 Step -1
                If .Selected(i) Then
'                   remove from collection - use tag property as id -
'                   key value is concatenation of name and listing id, and
'                   is used for uniqueness
                    For j = 1 To m_oContacts.Count
                        If m_oContacts.Item(j).Tag = .ItemData(i) Then
                            m_oContacts.Delete j
                            Exit For
                        End If
                    Next j
            
'                   remove from list
                    .RemoveItem i
                    
'                   tag this index as the last removed item-
'                   will be used below to select new item
'                   when all is done
                    k = i
                End If
            Next i
    
'           select new item
            If .ListCount Then
                .Selected(ciMin(.ListCount - 1, CDbl(k))) = True
                .SetFocus
            Else
                UserControl.lstListings.SetFocus
            End If

'           disable delete button if nothing is selected
            UserControl.btnDelete(.Index).Enabled = .ListCount
        End If
    End With
    
''   enable insert btn only if contacts exist
'    UserControl.cmdOK.Enabled = m_oContacts.Count
        
    Exit Sub

ProcError:
    Dim lErr As Long
    Dim xDesc As String

    lErr = Err.Number
    xDesc = Err.Description
    
'   enable insert btn only if contacts exist
    UserControl.cmdOK.Enabled = m_oContacts.Count
    
    Err.Number = lErr
    Err.Description = xDesc
        
    RaiseError "CIX.CI.DeleteSelContacts"
    Exit Sub
End Sub

Private Function LoadContacts(Optional ByVal iSearchType As ciSearchTypes = ciSearchType_None) As Long
'loads the contacts in the selected folder,
'using the specified search method - note that
'some search methods ignore the selected folder
    Dim oFolder As CIO.CFolder
    Dim oFilter As CIO.CFilter
    Dim oNode As MSComctlLib.node
    Dim oStore As CIO.CStore
    Dim iBackendID As Integer
    Dim oFld1 As CIO.CFilterField
    Dim oFld2 As CIO.CFilterField
    Dim bCancelled As Boolean
    Dim xUNID As String
    Dim xStoreName As String
    Dim oB As CIO.ICIBackend
    Dim oListings As CIO.CListings
    
    Static iPrevBackendID As Integer
    Static iPrevSearchType As ciSearchTypes
    
    On Error GoTo ProcError
    Set oNode = UserControl.tvFolders.SelectedItem
    
    If oNode Is Nothing Then
        Exit Function
    End If
    
    xUNID = oNode.Key
    xStoreName = oNode.Text
    
    iBackendID = m_oUNID.GetUNIDField(xUNID, ciUNIDFields_Backend)
    
    'exit if the search type is not a native search
    'and the node is a backend - no backend searching allowed
    If (iSearchType <> ciSearchType_Native) And (Backends.ItemFromID(iBackendID).SupportsFolders) Then
        If m_oUNID.GetUNIDType(xUNID) = ciUNIDType_Backend Then
            Exit Function
        End If
    End If
    
    If (iPrevBackendID <> iBackendID) And (iSearchType = ciSearchType_Refresh) Then
        'can't refresh if backend was switched - alert
        MsgBox "You cannot execute the 'Refresh' function after you " & _
            "have changed backends.", vbExclamation, App.Title
        Exit Function
    ElseIf (iPrevSearchType = ciSearchType_Native) And _
        (iSearchType = ciSearchType_Refresh) Then
        MsgBox "You cannot execute the 'Refresh' function after executing " & _
            "'Native Find'.  Please rerun 'Native Find'.", _
            vbExclamation, App.Title
        Exit Function
    End If
    
    Set oB = mdlMain.GetBackendFromID(iBackendID)
        
    SetupForInputState False

'   get filter
    Set oFilter = mdlMain.GetFilter(iBackendID)
                
    Dim iUNIDType As ciUNIDTypes
    iUNIDType = m_oUNID.GetUNIDType(xUNID)
        
    Select Case iSearchType
        Case ciSearchType_Native
            Set oListings = oB.SearchNative()
        Case ciSearchType_Quick
            With UserControl
                'set filter
                Set oFld1 = oFilter.FilterFields(.cmbFilterFld1.SelectedItem + 1)
                oFld1.Value = .txtFilterValue1.Text
                oFld1.Operator = .cmbOp1.BoundText
            End With
            
            If iUNIDType = ciUNIDType_Store And _
                oB.IsSearchableEntity(xUNID) Then
                'selected node is a store and store searches
                'are supported by the backend - search store
                Set oStore = m_oUNID.GetStore(xUNID, xStoreName)
                Set oListings = oB.GetStoreListings(oStore, oFilter)
            ElseIf iUNIDType = ciUNIDType_Folder And _
                oB.IsSearchableEntity(xUNID) Then
                'selected node is a folder - search folder
                Set oFolder = m_oUNID.GetFolder(xUNID)
                oFolder.Name = UserControl.tvFolders.SelectedItem.Text
                Set oListings = oB.GetFolderListings(oFolder, oFilter)
            End If
            If Not oFilter Is Nothing Then
                oFilter.Reset
            End If
        Case ciSearchType_Dialog
            If iUNIDType = ciUNIDType_Store And _
                oB.IsSearchableEntity(xUNID) Then
                'this is not allowed - must *search* in a store
                Set oStore = m_oUNID.GetStore(xUNID, xStoreName)
                Set oListings = oB.SearchStore(oStore)
            ElseIf iUNIDType = ciUNIDType_Folder And _
                oB.IsSearchableEntity(xUNID) Then
                'selected node is a folder - search folder
                Set oFolder = m_oUNID.GetFolder(xUNID)
                oFolder.Name = UserControl.tvFolders.SelectedItem.Text
                Set oListings = oB.SearchFolder(oFolder, bCancelled)
            End If
        Case ciSearchType_None
            'clear filter conditions
            oFilter.Reset
            
            If iUNIDType = ciUNIDType_Store And _
                oB.IsLoadableEntity(xUNID) Then
                'selected node is a store and store contact loads
                'are supported by the backend - search store
                Set oStore = m_oUNID.GetStore(xUNID, xStoreName)
                Set oListings = oB.GetStoreListings(oStore, oFilter)
            ElseIf iUNIDType = ciUNIDType_Folder And _
                oB.IsLoadableEntity(xUNID) Then
                'selected node is a folder - get all listing in folder
                Set oFolder = m_oUNID.GetFolder(xUNID)
                oFolder.Name = UserControl.tvFolders.SelectedItem.Text
                Set oListings = oB.GetFolderListings(oFolder, oFilter)
            ElseIf iUNIDType = ciUNIDType_Backend And oB.IsLoadableEntity(xUNID) Then
                Set oStore = m_oUNID.GetStore(xUNID, xStoreName)
                Set oListings = oB.GetStoreListings(oStore, oFilter)
            End If
        Case ciSearchType_Refresh
            If iUNIDType = ciUNIDType_Store And _
                oB.IsLoadableEntity(xUNID) Then
                Set oStore = m_oUNID.GetStore(xUNID, xStoreName)
                oFilter.Reset
                Set oListings = oB.GetStoreListings(oStore, oFilter)
                UserControl.MousePointer = vbDefault
            ElseIf iUNIDType = ciUNIDType_Folder And _
                oB.IsLoadableEntity(xUNID) Then
                'selected node is a folder - get all listing in folder
                Set oFolder = m_oUNID.GetFolder(xUNID)
                oFolder.Name = UserControl.tvFolders.SelectedItem.Text
                Set oListings = oB.GetFolderListings(oFolder, oFilter)
            End If
    End Select
    
    If (Not bCancelled) And Not (oListings Is Nothing) Then
        'assign listings to module level listings collection, which
        'is the collection that is displayed in the control
        Set m_oListings = oListings
        
        'clear out listings
        With UserControl.lstListings
            If Not .Array Is Nothing Then
                .Array.Clear
                .Refresh
            End If
            
            If iPrevBackendID <> iBackendID Then
                'backend has changed - set column names
                With .Columns
                    If oB.Col1Name = Empty Then
                        Err.Raise ciErrs.ciErr_InvalidColumnParameter, , _
                            "No column 1 Name specified."
                    Else
                        .Item(0).Caption = oB.Col1Name
                        .Item(0).Visible = True
                    End If
                    
                    If oB.Col2Name = Empty Then
                        .Item(1).Visible = False
                        .Item(1).AllowSizing = False
                    Else
                        .Item(1).Caption = oB.Col2Name
                        .Item(1).Visible = True
                        .Item(1).AllowSizing = True
                    End If
                    
                    If oB.Col3Name = Empty Then
                        .Item(2).Visible = False
                        .Item(2).AllowSizing = False
                    Else
                        .Item(2).Caption = oB.Col3Name
                        .Item(2).Visible = True
                        .Item(2).AllowSizing = True
                    End If
                    
                    If oB.Col4Name = Empty Then
                        .Item(3).Visible = False
                        .Item(3).AllowSizing = False
                    Else
                        .Item(3).Caption = oB.Col4Name
                        .Item(3).Visible = True
                        .Item(3).AllowSizing = True
                    End If
                End With
            End If
        End With
        
        'store these for next execution of LoadContacts
        iPrevBackendID = iBackendID
        iPrevSearchType = iSearchType
    
        'clear address list selection
        With UserControl.cbxAddressType
            .Clear
            .ListIndex = -1
        End With
        
        With UserControl
            'set label showing folder of current contacts
            .lblStatus.Caption = " " & oNode.FullPath
            
            'bind to list box
           .Refresh
            If Not m_oListings Is Nothing Then
                With .lstListings
                    .Array = m_oListings.ToArray
                    .ReBind
       
                    'stop here if there are no contact in set
                    If m_oListings.Count = 0 Then
                        m_oEvents_AfterListingsRetrieved 0
                        UserControl.MousePointer = vbDefault
                        
                        MsgBox "No contacts in '" & oNode.Text & _
                            "' matched the supplied search criteria.", vbExclamation, App.Title
                        
                        SetupForInputState True
                        
                        If iSearchType = ciSearchType_Quick Then
                            'return focus to quick search to try again
                            UserControl.tvFolders.SetFocus
                        End If
                        
                        Exit Function
                    End If
                    
                   If .Array.Count(1) > 0 Then
                        On Error Resume Next
                        If .SelBookmarks.Count = 0 Then
                            .Row = 0
                            .SelBookmarks.Add 1
                        End If
                       
                        On Error GoTo ProcError
                   End If
                    
                    DoEvents
                    
                    'show appropriate panel
                    If UserControl.chkGroups.Value Then
                        ShowPanel ciPanel_Groups
                    Else
                        ShowPanel ciPanel_Contacts
                    End If
                End With
                .Refresh
            End If
        End With
        
        'save sort column in user ini
        SetUserIni "CIApplication", "Sort", oFilter.SortColumn
        
        LoadContacts = m_oListings.Count
    End If  'not bCancelled
    
    SetupForInputState True
    UserControl.MousePointer = vbDefault
    
    Exit Function
ProcError:
    Dim lErr As Long
    Dim xDesc As String
    
    lErr = Err.Number
    xDesc = Err.Description
    
    SetupForInputState True
    UserControl.MousePointer = vbDefault
    
    Err.Number = lErr
    Err.Description = xDesc
    g_oError.RaiseError "CIX.CI.LoadContacts"
End Function

Private Sub FixTDBCombo(oCombo As TDBCombo)
    'this is needed to make the
    'bound text stick programmatically
    On Error GoTo ProcError
    With oCombo
        .OpenCombo
        .CloseCombo
    End With
    Exit Sub
ProcError:
    RaiseError "CIX.CI.FixTDBCombo"
    Exit Sub
End Sub

Private Sub LoadAddresses()
'loads available addresses for selected listing
    Dim oListing As CIO.CListing
    Dim oAddress As CIO.CAddress
    Dim i As Integer
    Dim xOrigAddress As String
    
    On Error GoTo ProcError
    SetupForInputState False
    UserControl.MousePointer = vbHourglass
    
    With UserControl
'       get existing address selection
        xOrigAddress = .cbxAddressType.Text
        
'       clear address list
        .cbxAddressType.Clear
        
        If .lstListings.SelBookmarks.Count > 1 Then
            'get all address types for the backend
            Set m_oAddresses = CurrentBackend.GetAddresses()
        Else
            'get addresses for listing
            If .lstListings.BoundText = Empty Then
                'no listing is selected
                SetupForInputState True
                Exit Sub
            End If
            
            'a row has been selected - get listing from UNID, which is bound text
            Set oListing = m_oUNID.GetListing(.lstListings.BoundText)
            
            If oListing Is Nothing Then
                'couldn't get listing from bound text - something is wrong
                Err.Raise ciErr_InvalidListing, , "Listing with Display Name '" & _
                    .lstListings.Columns(0) & "' is invalid."
            End If
    
            'get addresses from listing
            Set m_oAddresses = GetBackendFromID(oListing.BackendID) _
                                .GetAddresses(oListing)
        End If
        
        If m_oAddresses Is Nothing Then
'           couldn't get addresses from listing - error
            Err.Raise ciErr_InvalidUNID, , _
                "Invalid address object from listing."
        End If
        
'       add addresses to list
        With .cbxAddressType
            For i = 1 To m_oAddresses.Count
                Set oAddress = m_oAddresses.Item(i)
                .AddItem oAddress.Name
                .Tag = oAddress.UNID
            Next i
        
'           select address
            If .ListCount > 0 Then
                On Error Resume Next
                xOrigAddress = Trim$(Replace(xOrigAddress, _
                    "(mailing)", Empty, , , vbTextCompare))
                    
                .Text = xOrigAddress
                On Error GoTo ProcError
                
                If .ListIndex = -1 Then
'                   original address is not in list-
'                   select first address in list
                    .ListIndex = 0
                End If
            End If
        End With
    End With
    
    SetupForInputState True
    UserControl.MousePointer = vbDefault
    Exit Sub
ProcError:
    Dim lErr As Long
    Dim xDesc As String

    lErr = Err.Number
    xDesc = Err.Description
    
    SetupForInputState True
    UserControl.MousePointer = vbDefault
    
    Err.Number = lErr
    Err.Description = xDesc
    RaiseError "CIX.CI.LoadAddresses"
End Sub

Private Sub btnAddContactInNative_Click()
    On Error GoTo ProcError
    UserControl.MousePointer = vbHourglass
    CurrentBackend.AddContact
    UserControl.MousePointer = vbDefault
    Exit Sub
ProcError:
    ShowError
    Exit Sub
End Sub

Private Sub btnAddGroup_Click()
    On Error GoTo ProcError
    AddGroup
    Exit Sub
ProcError:
    ShowError
    Exit Sub
End Sub

Private Sub AddGroup()
'creates a new contact group
    Dim oForm As frmAddGroup
    Dim oGroups As CIX.CGroups
    
    On Error GoTo ProcError
    Set oForm = New frmAddGroup
    
    With oForm
        'show add group dialog
        .Show vbModal
        
        If Not .Canceled Then
            'add group
            Set oGroups = Me.GroupsBackend
            oGroups.AddGroup .txtGroupName.Text
            
            'refresh groups list
            RefreshGroupsList
            
            'select new group in list
            UserControl.cbxGroups.Text = .txtGroupName.Text
            
            'update tree
            RefreshTreeBranch UserControl.tvFolders.Nodes.Item("b" & Me.GroupsBackend.InternalID)
        End If
    End With
    Unload oForm
    Set oForm = Nothing
    Exit Sub
ProcError:
    Dim lErr As Long
    Dim xDesc As String

    lErr = Err.Number
    xDesc = Err.Description
    
    Unload oForm
    Set oForm = Nothing
    
    Err.Number = lErr
    Err.Description = xDesc
    RaiseError "CIX.CI.AddGroup"
    Exit Sub
End Sub

Private Sub DeleteGroup()
    Dim oGroups As CIX.CGroups
    Dim iChoice As VbMsgBoxResult
    
    With UserControl.cbxGroups
        If .ListIndex <> -1 Then
            'item is selected - check to see if this group is active (loaded)
            Set oGroups = Me.GroupsBackend
            If bCurrentGroupIsLoadedFolder() Then
                'don't allow the folder to be deleted
                MsgBox "You can't delete this group because it is currently your active " & _
                    "contacts folder." & vbCr & "Switch to a different folder, " & _
                    "then try deleting this group.", vbExclamation
                Exit Sub
            Else
                'prompt to delete it
                iChoice = MsgBox("Delete the group '" & _
                    UserControl.cbxGroups.Text & "'?", vbQuestion + vbYesNo, App.Title)
            End If
            
            If iChoice = vbYes Then
                oGroups.DeleteGroup .ItemData(.ListIndex)
            
                'refresh groups list
                RefreshGroupsList
                
                'clear members
                UserControl.lstGroupMembers.Clear
                
                'update tree
                RefreshTreeBranch UserControl.tvFolders.Nodes _
                    .Item("b" & Me.GroupsBackend.InternalID)
            End If
        End If
    End With
    Exit Sub
ProcError:
    RaiseError "CIX.CI.DeleteGroup"
End Sub
Private Sub RefreshGroupsList()
'refresh the Groups combo
    Dim oFolders As CIO.CFolders
    Dim oFolder As CIO.CFolder
    
    On Error GoTo ProcError
    
    'get "groups" folder
    Set oFolders = Me.GroupsBackend.GetFolders(Nothing)
        
    With UserControl.cbxGroups
        .Clear
        For Each oFolder In oFolders
            .AddItem oFolder
            .ItemData(.ListCount - 1) = oFolder.ID
        Next oFolder
    End With
    Exit Sub
ProcError:
    RaiseError "CIX.CI.RefreshGroupsList"
End Sub

Private Sub btnAddMember_Click()
    On Error GoTo ProcError
    AddMembers
    Exit Sub
ProcError:
    ShowError
    Exit Sub
End Sub

Private Sub AddMembers()
'adds the selected listings to the specified contact group
    Dim oBEnd As CIO.ICIBackend
    Dim oContacts As CIO.CContacts
    Dim oC As CIO.CContact
    Dim oL As CIO.CListing
    Dim oAddr As CIO.CAddress
    Dim lBookmark As Long
    Dim l As Long
    Dim lGroupID As Long
    Dim lID As Long
    Dim oUNID As CUNID
    Dim xUNID As String
    Dim iID As Integer
    Dim oGroups As CIX.CGroups
    
    On Error GoTo ProcError
    
    With UserControl
        With .cbxGroups
            'alert and exit if no group selected
            If .ListIndex = -1 Then
                MsgBox "Please select a group before adding a group member.", _
                    vbExclamation, App.Title
                Exit Sub
            End If
            
            'get group ID
            lGroupID = .ItemData(.ListIndex)
            
            'alert and don't allow if loaded folder is a group -
            'users can't add group members to other groups
            xUNID = UserControl.tvFolders.SelectedItem.Key
            Set oUNID = New CUNID
            
            iID = oUNID.GetUNIDField(xUNID, ciUNIDFields_Backend)
            If iID = Me.GroupsBackend.ID Then
                MsgBox "You can't add members of one group to another group.", vbExclamation
                Exit Sub
            End If
        End With
        
        If .lstListings.SelBookmarks.Count = 0 Then
            If .lstListings.Row > -1 Then
                .lstListings.SelBookmarks.Add .lstListings.Row
            End If
        End If
        
        UserControl.MousePointer = vbHourglass
        
        For l = 0 To .lstListings.SelBookmarks.Count - 1
            lBookmark = .lstListings.SelBookmarks(l)
            
'           get selected listing
            Set oL = m_oListings.Item(lBookmark)
            If oL Is Nothing Then
                Err.Raise CIO.ciErr_InvalidListing, , _
                    "Listing with Display Name '" & _
                        oL.DisplayName & "' is invalid."
            End If
            
            If m_oAddresses.Count > 0 Then
'               get selected address
                On Error Resume Next
                
                'exit if still no type is chosen
                If .cbxAddressType.ListIndex = -1 Then
                    Exit Sub
                End If
                
                Set oAddr = m_oAddresses.Item(.cbxAddressType.ListIndex + 1)
                On Error GoTo ProcError
                If oAddr Is Nothing Then
                    Err.Raise CIO.ciErr_invalidAddress, , _
                        "Selected address is invalid."
                End If
            End If
            
'           get contact from listing and address
            Set oContacts = ListingBackend(oL).GetContacts(oL, oAddr, _
                 , ciRetrieveData_Names + ciRetrieveData_Addresses, ciAlert_NoAddresses)
                             
'           add to contact collection
            For Each oC In oContacts
                On Error Resume Next
                                
                Set oGroups = Me.GroupsBackend
                lID = oGroups.AddMember(oC, lGroupID)

'               add full name to selected list
                With UserControl.lstGroupMembers
                    If (oC.AddressTypeName <> Empty) Then
                        .AddItem oC.DisplayName & "  (" & oC.AddressTypeName & ")"
                    Else
                        .AddItem oC.DisplayName & "  (No Address)"
                    End If
                    
                    .ItemData(.ListCount - 1) = lID
                End With
                DoEvents
            Next oC
        Next l
    End With
    
    UserControl.lstListings.SetFocus
    UserControl.MousePointer = vbDefault
        
    Exit Sub
ProcError:
    Dim lErr As Long
    Dim xDesc As String

    lErr = Err.Number
    xDesc = Err.Description
    
    UserControl.MousePointer = vbDefault
    
    Err.Number = lErr
    Err.Description = xDesc
    RaiseError "CIX.frmListings.AddSelContacts"
End Sub

Private Sub btnDelete_Click(Index As Integer)
'delete the selected contact in the specified list
    On Error GoTo ProcError
    DeleteSelContacts
    Exit Sub
ProcError:
    ShowError
    Exit Sub
End Sub

Private Sub btnDelete_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)
    On Error GoTo ProcError
    If KeyCode = vbKeyDelete Then
        DeleteSelContacts
    End If
    Exit Sub
ProcError:
    ShowError
    Exit Sub
End Sub

Private Sub btnDeleteGroup_Click()
    On Error GoTo ProcError
    DeleteGroup
    Exit Sub
ProcError:
    ShowError
    Exit Sub
End Sub

Private Sub btnDeleteMember_Click()
    On Error GoTo ProcError
    DeleteMembers
    Exit Sub
ProcError:
    ShowError
    Exit Sub
End Sub

Private Function bCurrentGroupIsLoadedFolder() As Boolean
'returns True if the current group is also the loaded contacts folder
    Dim oUNID As CIO.CUNID
    Dim iBEndID As Integer
    Dim lActiveFolderID As Long
    Dim lCurGroupsFolderID As Long
    Dim xActiveFolderUNID As String
    
    On Error GoTo ProcError
    Set oUNID = New CIO.CUNID
    
    'get UNID of loaded (active folder)
    xActiveFolderUNID = UserControl.tvFolders.SelectedItem.Key
    
    'get backend of active folder
    iBEndID = oUNID.GetUNIDField(xActiveFolderUNID, ciUNIDFields_Backend)
    
    If iBEndID = Me.GroupsBackend.InternalID Then
        'backend is groups backend - test for folder
        lActiveFolderID = oUNID.GetUNIDField(xActiveFolderUNID, ciUNIDFields_Folder)
        
        With UserControl.cbxGroups
            lCurGroupsFolderID = .ItemData(.ListIndex)
        End With
        
        bCurrentGroupIsLoadedFolder = lActiveFolderID = lCurGroupsFolderID
    End If
    Exit Function
ProcError:
    RaiseError "CIX.CI.bCurrentGroupIsLoadedFolder"
    Exit Function
End Function
Private Sub DeleteMembers()
    Dim oGroups As CIX.CGroups
    Dim iChoice As VbMsgBoxResult
    Dim iSelCount As Integer
    Dim iSelItem As Integer
    Dim i As Integer
    
    On Error GoTo ProcError
    
    'get number of selected group members
    iSelCount = UserControl.lstGroupMembers.SelCount
    
    If iSelCount = 0 Then
        MsgBox "Please select one or more group members to delete.", vbExclamation
        Exit Sub
    End If
    
'   alert and don't allow if loaded folder is the group
'   from which the user is attempting to delete members
    If bCurrentGroupIsLoadedFolder() Then
        MsgBox "You can't edit this group because it is " & _
            "currently your active contacts folder." & vbCr & "Switch to a different folder, " & _
            "then try editing this group.", vbExclamation
        Exit Sub
    End If

'   prompt to delete
    If iSelCount = 1 Then
        iChoice = MsgBox("Delete '" & UserControl.lstGroupMembers.Text & "' from this group?", vbYesNo + vbExclamation)
    Else
        iChoice = MsgBox("Delete the selected group members from this group?", vbYesNo + vbExclamation)
    End If
    
    If iChoice = vbYes Then
        'do delete
        Set oGroups = Me.GroupsBackend
        
        With UserControl.lstGroupMembers
            For i = .ListCount - 1 To 0 Step -1
                If .Selected(i) Then
                    oGroups.DeleteMember .ItemData(i)
                    
                    'update list
                    .RemoveItem i
                    
                    'mark for later use
                    iSelItem = i
                End If
            Next i
            
            .SetFocus
            
            'reselect an item
            If .ListCount > 0 Then
                If iSelCount > 1 Then
                    .Selected(0) = True
                Else
                    If iSelItem > .ListCount - 1 Then
                        .Selected(.ListCount - 1) = True
                    Else
                        .Selected(iSelItem) = True
                    End If
                End If
            End If
        End With
    End If
    Exit Sub
ProcError:
    RaiseError "CIX.CI.DeleteMember"
End Sub

Private Sub btnEdit_Click()
    On Error GoTo ProcError
    EditContact
    Exit Sub
ProcError:
    ShowError
    Exit Sub
End Sub

Private Sub EditContact()
    Dim oB As CIO.ICIBackend
    Dim oL As CIO.CListing
    Dim lBmk As Long
    
    On Error GoTo ProcError
    
    With UserControl.lstListings.SelBookmarks
        If .Count = 0 Then
            MsgBox "Please select a contact from the list " & _
                "of available contacts before running this function.", _
                vbExclamation, App.Title
            Exit Sub
        End If
        
        'get the backend of the contact
        lBmk = .Item(0)
    End With
    
'   get selected listing
    Set oL = m_oListings.Item(lBmk)
    
    Set oB = GetBackendFromID(oL.BackendID)
    
    If oB.SupportsContactEdit() Then
        'run the edit method of the backend
        oB.EditContact oL
    Else
        'alert
        MsgBox "You can't use Contact Integration to " & _
            "edit contacts stored in " & oB.Name & ".", _
            vbExclamation, App.Title
    End If
    Exit Sub
ProcError:
    RaiseError "CIX.CI.EditContact"
    Exit Sub
End Sub

Private Sub ResetQuickSearch()
    On Error GoTo ProcError
    UserControl.txtFilterValue1.Text = Empty
    CurrentBackend.Filter.Reset
    'UserControl.txtFilterValue2.Text = Empty
    Exit Sub
ProcError:
    RaiseError "CIX.CI.ResetQuickSearch"
    Exit Sub
End Sub

Private Sub btnGetMofoContacts_Click()
    Const mpThisFunction As String = "CIX.CI.btnGetMofoContacts_Click"
    
    Static o As Object
    Dim oBackend As Object
    Dim oCol As Collection
    Dim oCIContact As CIO.CContact
    Dim oOutlookContact As Object
    
    On Error Resume Next
    Set o = CreateObject("MofoCI.Contacts")
    On Error GoTo ProcError
    
    If o Is Nothing Then
        Err.Raise 429, , "Could not create Mofo Contacts object 'MoFoCI.Contacts'"
    End If
    
    'Screen.ActiveForm.Hide
    DoEvents
    
    Set oCol = o.Retrieve()
    
    If oCol.Count = 0 Then
        Exit Sub
    Else
        ShowPanel ciPanel_Contacts
    End If
    
    Set oBackend = CreateObject("CIMAPI.CCIBackend")
    
    Dim iDataRetrieved As Integer
    
    Select Case Me.SelectionList
        Case CIX.ciSelectionList_To
            iDataRetrieved = Me.DataRetrievedForTo
        Case CIX.ciSelectionList_From
            iDataRetrieved = Me.DataRetrievedForFrom
        Case CIX.ciSelectionList_CC
            iDataRetrieved = Me.DataRetrievedForCC
        Case CIX.ciSelectionList_BCC
            iDataRetrieved = Me.DataRetrievedForBCC
    End Select
    
    
    For Each oOutlookContact In oCol
        Set oCIContact = oBackend.GetCIContactFromOutlookContact( _
            oOutlookContact, iDataRetrieved)

'       set selection list for contact
        oCIContact.ContactType = Me.SelectionList
                    
'       find the last contact in collection that has the
'       same contact type - if none, add as first of group
        Dim iNumTo As Integer
        Dim iNumFrom As Integer
        Dim iNumCC As Integer
        Dim iNumBCC As Integer
        Dim lInsertAt As Long
        Dim iID As Integer
        
        'get number of contacts in each group
        With UserControl.lstContacts
            iNumTo = .Item(0).ListCount
            iNumFrom = .Item(1).ListCount
            iNumCC = .Item(2).ListCount
            iNumBCC = .Item(3).ListCount
        End With
        
        Select Case oCIContact.ContactType
            Case CIX.ciSelectionList_To
                'insert after last 'To' contact
                lInsertAt = iNumTo + 1
            Case CIX.ciSelectionList_From
                lInsertAt = iNumTo + iNumFrom + 1
            Case CIX.ciSelectionList_CC
                lInsertAt = iNumTo + iNumFrom + iNumCC + 1
            Case CIX.ciSelectionList_BCC
                lInsertAt = iNumTo + iNumFrom + iNumCC + iNumBCC + 1
        End Select
            
        On Error Resume Next
        If lInsertAt <= m_oContacts.Count Then
            m_oContacts.Add oCIContact, lInsertAt
        Else
            m_oContacts.Add oCIContact
        End If
        
        If Err.Number = 0 Then
            'get next number in the sequence
            iID = iID + 1
            
            'tag the contact with this unique number
            oCIContact.Tag = iID
            
'                   add full name to selected list
            With SelectionListControl
                .AddItem oCIContact.DisplayName
                .ItemData(.ListCount - 1) = iID
            End With
        ElseIf Err.Number = 457 Then
            MsgBox oCIContact.FullName & " has already been added.", vbExclamation, App.Title
        Else
            Err.Raise Err.Number, , Err.Description
        End If
    Next
    'RaiseEvent InsertionOK(m_oContacts)
    Exit Sub
ProcError:
    RaiseError mpThisFunction
    Exit Sub
End Sub

Private Sub btnHelp_Click()
    Debug.Print "click"
    On Error GoTo ProcError
    If Not (UserControl.mnuHelp.Visible) Then
        ShowHelpMenu
    End If
    Exit Sub
ProcError:
    RaiseError "CIX.CI.btnOptions_Click"
    Exit Sub
End Sub

Private Sub btnMoveDown_Click()
    MoveSelContactDown
End Sub

Private Sub btnMoveUp_Click()
    MoveSelContactUp
End Sub

Private Sub btnOptions_Click()
    On Error GoTo ProcError
    If Not (mnuFolders.Visible Or mnuContacts.Visible) Then
        ShowMenu
    End If
    Exit Sub
ProcError:
    RaiseError "CIX.CI.btnOptions_Click"
    Exit Sub
End Sub

Private Sub ShowMenu()
    Dim sMnuTop As Single
    Dim sMnuLeft As Single
    On Error GoTo ProcError
    With UserControl
        sMnuTop = .btnOptions.Top - 100 + .btnOptions.Height
        sMnuLeft = .btnOptions.Left + .btnOptions.Width
        
        If UserControl.fraFolders.Visible Then
            If Not (.tvFolders.SelectedItem Is Nothing) Then
                .PopupMenu mnuFolders, vbPopupMenuRightAlign, sMnuLeft, sMnuTop
            End If
        Else
            .PopupMenu mnuContacts, vbPopupMenuRightAlign, sMnuLeft, sMnuTop
        End If
    End With
    Exit Sub
ProcError:
    RaiseError "CIX.CI.ShowMenu"
    Exit Sub
End Sub

Private Sub ShowHelpMenu()
    Dim sMnuTop As Single
    Dim sMnuLeft As Single
    On Error GoTo ProcError
    With UserControl
        sMnuTop = .btnHelp.Top - 100 + .btnHelp.Height
        sMnuLeft = .btnHelp.Left + .btnHelp.Width
        
        .PopupMenu mnuHelp, vbPopupMenuRightAlign, sMnuLeft, sMnuTop
    End With
    Exit Sub
ProcError:
    RaiseError "CIX.CI.ShowHelpMenu"
    Exit Sub
End Sub

Private Sub btnQuickSearch_Click()
    On Error GoTo ProcError
    LoadContacts ciSearchType_Quick
    ResetQuickSearch
    Exit Sub
ProcError:
    ShowError
    Exit Sub
End Sub

Private Sub btnRefresh_Click()
    On Error GoTo ProcError
    Refresh
    Exit Sub
ProcError:
    ShowError
    Exit Sub
End Sub

Private Sub Refresh()
'refreshes either the folder tree branch
'or contacts depending on visible panel
    Dim oNode As MSComctlLib.node
    
    On Error GoTo ProcError
    If UserControl.fraFolders.Visible Then
        'refresh the selected folder tree branch
        Set oNode = UserControl.tvFolders.SelectedItem
        
        If Not oNode Is Nothing Then
            RefreshTreeBranch oNode
        End If
    Else
        'refresh contacts
        LoadContacts ciSearchType_Refresh
    End If

    Exit Sub
ProcError:
    RaiseError "CIX.CI.Refresh"
    Exit Sub
End Sub

Private Sub btnSearch_Click()
    On Error GoTo ProcError
    LoadContacts ciSearchType_Dialog
    Exit Sub
ProcError:
    ShowError
End Sub

Private Sub btnSearchNative_Click()
    On Error GoTo ProcError
    If CurrentBackend().SupportsNativeSearch Then
        LoadContacts ciSearchType_Native
        SetupForInputState True
    End If
    Exit Sub
ProcError:
    ShowError
End Sub

Private Sub btnTogglePanels_Click()
    On Error GoTo ProcError
    If UserControl.fraFolders.Visible Then
        LoadContacts
    Else
        ShowPanel ciPanel_folders
    End If
    Exit Sub
ProcError:
    ShowError
End Sub

Private Sub cbxGroups_Click()
    With UserControl
        .btnDeleteGroup.Enabled = .cbxGroups.ListIndex <> -1
        .btnAddMember.Default = True
        LoadGroupMembers
        .cmdOK.Enabled = True
    End With
End Sub

Private Sub LoadGroupMembers()
'loads the group members of the selected
'group into the group members list

    Dim oListings As CIO.CListings
    Dim oFolder As CIO.CFolder
    Dim oUNID As CIO.CUNID
    Dim lID As Long
    Dim oGroups As CIX.CGroups
    Dim l As Long
    
    With UserControl.cbxGroups
        If .ListIndex = -1 Then
            Exit Sub
        End If
        
        'get id of selected group
        lID = .ItemData(.ListIndex)
    End With
    
    UserControl.lstGroupMembers.Clear
    
    Set oUNID = New CIO.CUNID
    Set oGroups = Me.GroupsBackend
    With oGroups
        Set oListings = .GetMembers(lID)
        
        If oListings.Count Then
            For l = 1 To oListings.Count
                With UserControl.lstGroupMembers
                    .AddItem oListings.Item(l).DisplayName
                    .ItemData(.ListCount - 1) = oListings.Item(l).ID
                End With
            Next l
        End If
    End With
    Exit Sub
ProcError:
    RaiseError "CIX.CI.LoadGroupMembers"
End Sub

Private Sub chkGroups_Click()
    On Error GoTo ProcError
    If UserControl.chkGroups.Value = vbChecked Then
        'refresh groups list
        RefreshGroupsList
        UserControl.lstGroupMembers.Clear
        ShowPanel ciPanel_Groups
    Else
        ShowPanel ciPanel_Contacts
    End If
    Exit Sub
ProcError:
    ShowError
End Sub

Private Sub cmbFilterFld1_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch UserControl.cmbFilterFld1, Reposition
    Exit Sub
ProcError:
    ShowError
    Exit Sub
End Sub

Private Sub cmbOp1_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch UserControl.cmbOp1, Reposition
    Exit Sub
ProcError:
    ShowError
    Exit Sub
End Sub

Private Sub cmbOp1_Validate(Cancel As Boolean)
    On Error GoTo ProcError
    Cancel = Not bValidateBoundTDBCombo(UserControl.cmbOp1)
    Exit Sub
ProcError:
    ShowError
    Exit Sub
End Sub

Private Sub cmbFilterFld1_Validate(Cancel As Boolean)
    On Error GoTo ProcError
    Cancel = Not bValidateBoundTDBCombo(UserControl.cmbFilterFld1)
    Exit Sub
ProcError:
    ShowError
    Exit Sub
End Sub

Private Sub cmdCancel_Click()
    On Error Resume Next
    SaveColumns
    tvFolders.SetFocus
    RaiseEvent InsertionCancelled
    On Error GoTo 0
End Sub

Private Sub cmdCancel_KeyPress(KeyAscii As Integer)
    If KeyAscii = 9 Then    'Tab
        If m_bShowedContacts Then
            On Error Resume Next
            tvFolders.SetFocus
            On Error GoTo 0
        End If
    End If
End Sub

Private Sub cmdCancel_LostFocus()
    If Not m_bShowedContacts Then
        On Error Resume Next
        tvFolders.SetFocus
        On Error GoTo 0
    End If
End Sub

Private Sub cmdOK_Click()
    If UserControl.chkGroups.Value = vbUnchecked Then
        'not in groups mode - this button should insert contacts
        SaveColumns
        RaiseEvent InsertionOK(m_oContacts)
    Else
        'in groups mode - this button should load the current group
        LoadGroup
    End If
End Sub

Private Sub LoadGroup()
    Dim xGroup As String
    Dim lNum As Long
    
    On Error GoTo ProcError
    'get current group
    xGroup = UserControl.cbxGroups.Text
    
    If xGroup <> Empty Then
        'load the group by selecting node, then loading contacts
        SelectNode Me.GroupsBackend.Name & "\" & xGroup
        
        'load contacts
        lNum = LoadContacts()
        
        If lNum > 0 Then
            'exit group mode
            UserControl.chkGroups.Value = vbUnchecked
        End If
    Else
        MsgBox "Please select a group to load.", vbExclamation
    End If
    Exit Sub
ProcError:
    RaiseError "CIX.CI.LoadGroup"
    Exit Sub
End Sub

Private Sub lstContacts_Click(Index As Integer)
    With UserControl.lstContacts(Index)
        UserControl.btnDelete(Index).Enabled = .ListCount > 0 And .ListIndex > -1
    End With
End Sub

Private Sub lstContacts_GotFocus(Index As Integer)
    On Error GoTo ProcError
    
    'mark that this contact list is the current list
    m_iCurList = Index
    
    'enable irrelevant menu items
    UserControl.mnuContacts_MoveDown.Enabled = True
    UserControl.mnuContacts_MoveUp.Enabled = True
    
    'set the selection list to correspond to
    'the list box that is being clicked on
    SelectionList = SelListFromControlIndex(Index)

    ClearListingsSelections
    Exit Sub
ProcError:
    ShowError
    Exit Sub
End Sub

Private Sub ClearListingsSelections()
    Dim i As Integer
    
    On Error GoTo ProcError
    With UserControl.lstListings.SelBookmarks
        For i = .Count - 1 To 0 Step -1
            .Remove i
        Next i
    End With
    
    'UserControl.lstListings.SelectedItem = Null
    Exit Sub
ProcError:
    RaiseError "CIX.CI.ClearListingsSelections"
    Exit Sub
End Sub

Private Function SelListFromControlIndex(ByVal iIndex As Integer) As Integer
    'selection list is 2 exponent lstContacts index
    SelListFromControlIndex = 2 ^ iIndex
End Function

Private Sub MoveSelContactDown()
    Dim oLB As VB.ListBox
    Dim iIndex As Integer
    Dim xDisplayText As String
    Dim xItemData As Long
    Dim i As Integer
    
    On Error GoTo ProcError
    
    'get list box
    Set oLB = SelectionListControl()
    
    'exit if the last item in the list is selected
    If oLB.SelCount = 0 Then
        Exit Sub
    ElseIf oLB.Selected(oLB.ListCount - 1) = True Then
        Exit Sub
    End If
    
    With UserControl
        .btnMoveDown.Enabled = False
        .btnMoveUp.Enabled = False
    End With
    
    'for each selected index, move down
    For i = oLB.ListCount - 2 To 0 Step -1
        If oLB.Selected(i) Then
            'get selected index data
            xDisplayText = oLB.List(i)
            xItemData = oLB.ItemData(i)
            
            oLB.Selected(i) = False
            
            'delete selected index
            oLB.RemoveItem i
            
            'add new item at index one below
            oLB.AddItem xDisplayText, i + 1
            oLB.ItemData(i + 1) = xItemData
            DoEvents
            oLB.Selected(i + 1) = True
            
            'get current index of contact in collection
            With UserControl.lstContacts
                Select Case oLB.Index
                    Case 0
                        iIndex = i + 1
                    Case 1
                        iIndex = .Item(0).ListCount + i + 1
                    Case 2
                        iIndex = .Item(0).ListCount + _
                            .Item(1).ListCount + i + 1
                    Case 3
                        iIndex = .Item(0).ListCount + _
                            .Item(1).ListCount + .Item(2).ListCount + i + 1
                End Select
            End With
            
            Dim oContact As CIO.CContact
            
            'move contact one down in collection
            Set oContact = m_oContacts.Item(iIndex)
            
            'delete contact from collection
            m_oContacts.Delete iIndex
            
            'add contact at new position
            If iIndex + 1 > m_oContacts.Count Then
                'contact needs to be added to the end
                m_oContacts.Add oContact
            Else
                m_oContacts.Add oContact, iIndex + 1
            End If
        End If
    Next i
    
    With UserControl
        .btnMoveDown.Enabled = True
        .btnMoveUp.Enabled = True
    End With
    
    'give list focus again
    oLB.SetFocus
    
    Exit Sub
ProcError:
    Dim lErr As Long
    Dim xDesc As String

    lErr = Err.Number
    xDesc = Err.Description
    
    With UserControl
        .btnMoveDown.Enabled = True
        .btnMoveUp.Enabled = True
    End With
    
    Err.Number = lErr
    Err.Description = xDesc

    RaiseError "CIX.CI.MoveSelContactDown"
    Exit Sub
End Sub

Private Sub MoveSelContactUp()
    Dim oLB As VB.ListBox
    Dim iIndex As Integer
    Dim xDisplayText As String
    Dim xItemData As Long
    Dim i As Integer
    
    On Error GoTo ProcError
    'get list box
    Set oLB = SelectionListControl()
    
    'exit if the first item in the list is selected
    If oLB.SelCount = 0 Then
        Exit Sub
    ElseIf oLB.Selected(0) = True Then
        Exit Sub
    End If
    
    With UserControl
        .btnMoveDown.Enabled = False
        .btnMoveUp.Enabled = False
    End With
    
    'for each selected index, move down
    For i = 1 To oLB.ListCount - 1
        If oLB.Selected(i) Then
            'get selected index data
            xDisplayText = oLB.List(i)
            xItemData = oLB.ItemData(i)
            
            oLB.Selected(i) = False
            
            'delete selected index
            oLB.RemoveItem i
            
            'add new item at index one below
            oLB.AddItem xDisplayText, i - 1
            oLB.ItemData(i - 1) = xItemData
            oLB.Selected(i - 1) = True
            
            'get current index of contact in collection
            With UserControl.lstContacts
                Select Case oLB.Index
                    Case 0
                        iIndex = i + 1
                    Case 1
                        iIndex = .Item(0).ListCount + i + 1
                    Case 2
                        iIndex = .Item(0).ListCount + _
                            .Item(1).ListCount + i + 1
                    Case 3
                        iIndex = .Item(0).ListCount + _
                            .Item(1).ListCount + .Item(2).ListCount + i + 1
                End Select
            End With
            
            Dim oContact As CIO.CContact
            
            'move contact one up in collection
            Set oContact = m_oContacts.Item(iIndex)
            m_oContacts.Delete (iIndex)
            m_oContacts.Add oContact, iIndex - 1
        End If
    Next i
    
    With UserControl
        .btnMoveDown.Enabled = True
        .btnMoveUp.Enabled = True
    End With
    
    'give list focus again
    oLB.SetFocus
    
    Exit Sub
ProcError:
    Dim lErr As Long
    Dim xDesc As String

    lErr = Err.Number
    xDesc = Err.Description
    
    With UserControl
        .btnMoveDown.Enabled = True
        .btnMoveUp.Enabled = True
    End With
    
    Err.Number = lErr
    Err.Description = xDesc
    RaiseError "CIX.CI.MoveSelContactUp"
    Exit Sub
End Sub

Private Sub lstContacts_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)
    
    On Error GoTo ProcError
    
    If IsPressed(KEY_CTL) Then
        If KeyCode = 65 Then
            DoEvents
            SelectAllContacts
            KeyCode = 0
        ElseIf KeyCode = vbKeyDown Then
            MoveSelContactDown
            KeyCode = 0
        ElseIf KeyCode = vbKeyUp Then
            MoveSelContactUp
            KeyCode = 0
        End If
    Else
        If KeyCode = vbKeyDelete Then
            DeleteSelContacts
            KeyCode = 0
        ElseIf Not (KeyCode = vbKeyDown Or KeyCode = vbKeyUp) Then
            KeyCode = 0
        End If
    End If
    
    Exit Sub
ProcError:
    ShowError
    Exit Sub
End Sub

Private Sub SelectAllContacts()
    Dim l As Long
    
    On Error GoTo ProcError
    
    'select all
    With UserControl.lstContacts.Item(m_iCurList)
        For l = 0 To .ListCount - 1
            .Selected(l) = True
        Next l
    End With

    Exit Sub
ProcError:
    RaiseError "CIX.CI.SelectAllContacts"
    Exit Sub
End Sub

Private Sub SelectAllGroupMembers()
    Dim l As Long
    
    On Error GoTo ProcError
    
    'select all
    With UserControl.lstGroupMembers
        For l = 0 To .ListCount - 1
            .Selected(l) = True
        Next l
    End With

    Exit Sub
ProcError:
    RaiseError "CIX.CI.SelectAllGroupMembers"
    Exit Sub
End Sub

Private Sub lstContacts_MouseDown(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    Dim sCtlTop As Single
    Dim iNewIndex As Integer
    Dim i As Integer
    
    On Error GoTo ProcError
    m_bMouseDown = True

    With UserControl.lstContacts.Item(Index)
        If .ListCount = 0 Then
            Exit Sub
        Else
            .SetFocus
            If Button = 2 Then
                'delete previous selections - select item - show detail
                sCtlTop = .Top
                iNewIndex = Int(Y / 220) + .TopIndex
                For i = 0 To .ListCount - 1
                    .Selected(i) = False
                Next i
                
                If iNewIndex > .ListCount - 1 Then
                    iNewIndex = .ListCount - 1
                End If
                
                .Selected(iNewIndex) = True
                DoEvents
                
                If .ListIndex > -1 Then
                    ShowContactDetail .ItemData(iNewIndex)
                End If
            End If
        End If
    End With
    Exit Sub
ProcError:
    g_oError.ShowError
    Exit Sub
End Sub

Private Sub lstContacts_MouseUp(Index As Integer, Button As Integer, Shift As Integer, X As Single, Y As Single)
    m_bMouseDown = False
End Sub

Private Sub ShowContactDetail(ByVal iUniqueTagNumber As Integer)
'shows the detail for the contact having the specified tag number-
'number is stored in tag property and is issued when the
'contact is added to right side list
    Dim oForm As frmContactDetail
    Dim oContact As CIO.CContact
    Dim i As Integer
    
    If m_oContacts.Count = 0 Then
        Exit Sub
    End If
    
    Set oForm = New frmContactDetail
    
    
    With oForm
        'assign contacts to form
        Set .Contacts = m_oContacts
        
        On Error Resume Next
        
        If iUniqueTagNumber = 0 Then
            'no contact is selected in the list - select the first one
            'to display in the detail dialog
            'Set .SelectedContact = .Contacts.Item(1)
            .SelectedContactIndex = 1
        Else
            'select the right contact -
            'cycle through all until the one
            'with the specified tag is found
            For i = 1 To .Contacts.Count
                If .Contacts.Item(i).Tag = iUniqueTagNumber Then
                    'this is the one
                    'Set .SelectedContact = .Contacts.Item(i)
                    .SelectedContactIndex = i
                    Exit For
                End If
            Next i
        End If
        
        'show contact detail form
        .Show vbModal, Screen.ActiveForm
        Unload oForm
        Set oForm = Nothing
    End With
End Sub

Private Sub lstGroupMembers_GotFocus()
    On Error GoTo ProcError
    
    m_iCurList = ciListBoxes_GroupMembers
    
    'disable irrelevant menu items
    With UserControl
        .mnuContacts_MoveDown.Enabled = False
        .mnuContacts_MoveUp.Enabled = False
    
        ClearListingsSelections
        .btnDeleteMember.Enabled = (.lstGroupMembers.ListIndex > -1) _
        And (.lstGroupMembers.ListCount > 0)
    End With
    Exit Sub
ProcError:
    ShowError
    Exit Sub
End Sub

Private Sub lstGroupMembers_KeyDown(KeyCode As Integer, Shift As Integer)
    On Error GoTo ProcError
    
    If IsPressed(KEY_CTL) Then
        If KeyCode = 65 Then
            DoEvents
            SelectAllGroupMembers
        End If
    Else
        If KeyCode = vbKeyDelete Then
            DeleteSelContacts
        End If
    End If

    Exit Sub
ProcError:
    ShowError
    Exit Sub
End Sub

Private Sub lstGroupMembers_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Dim sCtlTop As Single
    Dim iNewIndex As Integer
    Dim i As Integer

    On Error GoTo ProcError
    m_bMouseDown = True

    With UserControl.lstGroupMembers
        If .ListCount = 0 Then
            Exit Sub
        Else
            .SetFocus
            If Button = 2 Then
                'delete previous selections - select item - show detail
                sCtlTop = .Top
                iNewIndex = Int(Y / 220) + .TopIndex
                For i = 0 To .ListCount - 1
                    .Selected(i) = False
                Next i

                If iNewIndex > .ListCount - 1 Then
                    iNewIndex = .ListCount - 1
                End If

                .Selected(iNewIndex) = True
                DoEvents

                If .ListIndex > -1 Then
                    ShowMemberDetail .ItemData(.ListIndex)
                End If
            End If
        End If
    End With
    Exit Sub
ProcError:
    g_oError.ShowError
    Exit Sub
End Sub

Private Sub ShowMemberDetail(ByVal lID As Long)
'shows the detail form for the first selected listing
    Dim oForm As frmListingDetail
    Dim lRow As Long
    Dim oUNID As CUNID
    Dim xUNID As String
    
    On Error GoTo ProcError
    
    UserControl.MousePointer = vbHourglass
    
    'get member UNID
    Set oUNID = New CUNID
    xUNID = Backends.GroupsBackend.InternalID & UNIDSep & UNIDSep & UNIDSep & lID
    
    'show detail form
    Set oForm = New frmListingDetail
    With oForm
        Set .Listing = oUNID.GetListing(xUNID)
        Set .Addresses = Backends.GroupsBackend.GetAddresses(.Listing)
        
        If .Addresses.Count > 0 Then
'            .AddressIndex = 0
        End If
        .Show vbModal, Screen.ActiveForm
        UserControl.MousePointer = vbDefault
    End With
        
    Unload oForm
    UserControl.lstGroupMembers.SetFocus
    Set oForm = Nothing
    Exit Sub
ProcError:
    RaiseError "CIX.CI.ShowListingDetail"
    Exit Sub
End Sub

Private Sub lstListings_ColMove(ByVal Position As Integer, Cancel As Integer)
    On Error GoTo ProcError
    UserControl.lstListings.ClearSelCols
    Exit Sub
ProcError:
    ShowError
End Sub

Private Sub lstListings_DblClick()
    On Error GoTo ProcError
    If lstListings.Row > -1 Then
        OnListingsDblClick
    End If
    Exit Sub
ProcError:
    ShowError
End Sub

Private Sub OnListingsDblClick()
    On Error GoTo ProcError
    If UserControl.chkGroups.Value = 0 Then
        AddSelContacts
    Else
        AddMembers
    End If
    Exit Sub
ProcError:
    RaiseError "CIX.CI.OnListingsDblClick"
    Exit Sub
End Sub

Private Sub lstListings_GotFocus()
    Dim i As Integer
    Dim j As Integer
    
    On Error GoTo ProcError
    
    'mark that this is the current list
    m_iCurList = ciListBoxes_Listings
    
    'disable irrelevant menu items
    UserControl.mnuContacts_MoveDown.Enabled = False
    UserControl.mnuContacts_MoveUp.Enabled = False
    
    'get the contact list that is active
    With UserControl
        For i = 0 To 3
            If .lblSelContacts(i).ForeColor = vbBlue Then
                'this is the active list - remove
                'selection, disable delete button
                With .lstContacts(i)
                    For j = 0 To .ListCount - 1
                        .Selected(j) = False
                    Next
                End With
                .btnDelete(i).Enabled = False
                Exit For
            End If
        Next i
        
        'remove group members selections, disable delete button
        With .lstGroupMembers
            For j = 0 To .ListCount - 1
                .Selected(j) = False
            Next
        End With
        .btnDeleteMember.Enabled = False
    End With
    Exit Sub
ProcError:
    ShowError
    Exit Sub
End Sub

Private Sub lstListings_HeadClick(ByVal ColIndex As Integer)
    On Error GoTo ProcError
    'reserve CTL+HeadClick for column selection
    If Not IsPressed(KEY_CTL) Then
        Sort ColIndex
    End If
    Exit Sub
ProcError:
    ShowError
    Exit Sub
End Sub

Private Sub Sort(ByVal ColIndex As Integer)
    On Error GoTo ProcError
    With UserControl.lstListings
        'exit if there aren't any listings
        If .Array.Count(1) = 0 Then
            Exit Sub
        End If
        
        'get first bookmark of selection
        Dim xBoundText As String
        xBoundText = .BoundText
        
        .ClearSelCols
        DoEvents
        
        'do sort
        m_oListings.Sort ColIndex
        
        'refresh list
        .Refresh
        
        'set this column as the default sort column for this backend
        CurrentBackend.DefaultSortColumn = ColIndex
        
        .ListField = .Columns(ColIndex).DataField
        
        'reselect previous - select
        'first row if no previous selection
        If xBoundText = Empty Then
            .SelBookmarks.Add 0
        Else
            .BoundText = xBoundText
        End If
    End With
    Exit Sub
ProcError:
    RaiseError "CIX.CI.Sort"
    Exit Sub
End Sub

Private Sub lstListings_KeyDown(KeyCode As Integer, Shift As Integer)
    Dim l As Long
    If IsPressed(KEY_CTL) Then
        If KeyCode = 65 Then
            SelectAllListings
        End If
    Else
        Select Case KeyCode
            Case vbKeyHome
                KeyCode = 0
                ClearListingSelections
                With UserControl.lstListings
                    .FirstRow = 1
                    .Row = 0
                    .SelBookmarks.Add 0
                End With
            Case vbKeyEnd
                KeyCode = 0
                ClearListingSelections
                With UserControl.lstListings
                    .FirstRow = m_oListings.Count - 1
                    .Row = .VisibleRows - 1
                    .SelBookmarks.Add .RowBookmark(.Row)
                End With
            Case vbKeyLeft, vbKeyRight
                KeyCode = 0
        End Select
    End If
End Sub

Private Sub SelectAllListings()
    Dim l As Long
    
    On Error GoTo ProcError
    DoEvents
    
    UserControl.MousePointer = vbHourglass
    
    With UserControl.lstListings
        For l = 1 To .Array.Count(1)
            .SelBookmarks.Add l
        Next l
        If .Row = -1 Then
            .Row = 0
        End If
    End With
    
    OnListingsRowChange
    UserControl.MousePointer = vbDefault
    Exit Sub
ProcError:
    Dim lErr As Long
    Dim xDesc As String

    lErr = Err.Number
    xDesc = Err.Description
    
    UserControl.MousePointer = vbDefault
    
    Err.Number = lErr
    Err.Description = xDesc
    RaiseError "CIX.CI.SelectAllListings"
    Exit Sub
End Sub

Private Sub ClearListingSelections()
    Dim i As Integer
    
'   remove listing selections
    With UserControl.lstListings.SelBookmarks
        For i = .Count - 1 To 0 Step -1
            .Remove i
        Next
    End With
    'UserControl.lstListings.Row = Null
End Sub

Private Sub ClearListingSelectionsButLast()
    Dim i As Integer
    
'   remove listing selections
    With UserControl.lstListings.SelBookmarks
        If .Count > 1 Then
            .Add .Item(.Count - 1)
            For i = .Count - 2 To 1 Step -1
                .Remove i
            Next
        End If
    End With
End Sub

Private Sub ShowListingDetail()
'shows the detail form for the first selected listing
    Dim oForm As frmListingDetail
    Dim lRow As Long
    
    On Error GoTo ProcError
    
    If m_oListings.Count = 0 Then
        Exit Sub
    ElseIf UserControl.lstListings.Bookmark = 0 Then
        Exit Sub
    End If
    
    UserControl.MousePointer = vbHourglass
    
    'show detail form
    Set oForm = New frmListingDetail
    With oForm
        Set .Listing = m_oListings.Item( _
            UserControl.lstListings.Bookmark)
            
        If UserControl.lstListings.SelBookmarks.Count = 1 Then
            'only one contact is selected - we can use the
            'addresses in the address list - if there are multiple
            'selections, we can't use these - the form will get
            'the addresses from the supplied listing
            Set .Addresses = m_oAddresses
        End If
        
        If UserControl.cbxAddressType.ListCount > 0 Then
            '.AddressIndex = UserControl.cbxAddressType.ListIndex
            .AddressName = UserControl.cbxAddressType.Text
        End If
        .Show vbModal, Screen.ActiveForm
        UserControl.MousePointer = vbDefault
    
        'select the address that was selected
        'in the listing detail form
        Dim iIndex As Integer
    End With
        
    If Not oForm.Canceled Then
        On Error Resume Next
        'iIndex = oForm.AddressIndex
        
        On Error Resume Next
        UserControl.cbxAddressType.Text = Mid$(oForm.AddressName, 2)
        On Error GoTo ProcError
        Unload oForm
    End If
    
    UserControl.lstListings.SetFocus
    Set oForm = Nothing
    Exit Sub
ProcError:
    RaiseError "CIX.CI.ShowListingDetail"
    Exit Sub
End Sub

Private Sub lstListings_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Dim oForm As frmListingDetail
    Dim lRow As Long
    Dim lNumSel As Long
    
    On Error GoTo ProcError
    
    If Button = 2 Then
        'user right-clicked on listing -
        
        'get original number of selections -
        'we'll need this to force a row change-
        'this will give us the correct addresses
        'for the selected listing
        lNumSel = UserControl.lstListings.SelBookmarks.Count
        
        With UserControl.lstListings
            'select listing
            lRow = .RowContaining(Y) + .FirstRow
            If lRow > 0 Then
                'clear existing listings
                ClearSelectedListings
                .SelBookmarks.Add lRow
                .SelectedItem = lRow
                If lNumSel > 1 Then
                    OnListingsRowChange
                End If
        
                DoEvents
                ShowListingDetail
            End If
        End With
    Else
        lRow = UserControl.lstListings.RowContaining(Y) + UserControl.lstListings.FirstRow
        If lRow = 0 Then
            'UserControl.lstListings.
        End If
    End If
    Exit Sub
ProcError:
    Dim lErr As Long
    Dim xDesc As String
    Dim xSource As String
    
    lErr = Err.Number
    xDesc = Err.Description
    xSource = Err.Source
    
    UserControl.MousePointer = vbDefault
    
    
    Err.Number = lErr
    Err.Description = xDesc
    Err.Source = xSource
    ShowError
    Exit Sub
End Sub

Private Sub ClearSelectedListings()
'clears the selected listings
    Dim i As Integer
    On Error GoTo ProcError
    With UserControl.lstListings.SelBookmarks
        For i = .Count - 1 To 0 Step -1
            .Remove i
        Next i
    End With
    Exit Sub
ProcError:
    RaiseError "CIX.CI.ClearSelectedListings"
    Exit Sub
End Sub

Private Sub OnListingsRowChange()
'executes all necessary functionality
'when a listing is selected
    Dim xFolderPath As String
    
    On Error GoTo ProcError
    With UserControl
        If .lstListings.Row = -1 Then
            Exit Sub
        End If
    
        If .lstListings.BoundText <> Empty Then

            'get folder path of selected listing
            If .lstListings.SelBookmarks.Count > 0 Then
                Dim lBmk As Long
                lBmk = .lstListings.SelBookmarks(0)
                If lBmk > 0 Then
                    xFolderPath = m_oListings.Item( _
                        .lstListings.SelBookmarks.Item(0)).FolderPath
                Else
                    xFolderPath = m_oListings.Item(1).FolderPath
                End If
            Else
                xFolderPath = m_oListings.Item(1).FolderPath
            End If
            
            If xFolderPath = Empty Then
                'no folder path stored - show selected folder path
                .lblStatus.Caption = .tvFolders.SelectedItem.FullPath
            Else
                'display folder path
                .lblStatus.Caption = xFolderPath
            End If
            
            LoadAddresses
            
            EnableAddButtons True
            '.btnEdit.Enabled = True
            'enable/disable edit button per backend support
            .btnEdit.Enabled = CurrentBackend.SupportsContactEdit
        Else
            EnableAddButtons False
            UserControl.btnEdit.Enabled = False
        End If
    End With
    UserControl.lstListings.SetFocus
    Exit Sub
ProcError:
    RaiseError "CIX.CI.OnListingsRowChange"
    Exit Sub
End Sub
Private Sub EnableAddButtons(ByVal bEnable As Boolean)
'enables/disables all add buttons
    Dim i As Integer
    
    On Error GoTo ProcError
    
    With UserControl.btnAdd
        For i = .LBound To .UBound
            .Item(i).Enabled = bEnable
        Next i
    End With
    UserControl.btnAddMember.Enabled = bEnable
    Exit Sub
ProcError:
    RaiseError "CIX.CI.EnableButtons"
End Sub

Private Function ShowTreeTop() As Long
'creates first level nodes on tree
    Dim oBackend As CIO.ICIBackend
    Dim oStores As CIO.CStores
    Dim oStore As CIO.CStore
    
    On Error GoTo ProcError
    
    If Me.ShowBackendNodes Or (mdlMain.Backends.Count > 1) Then
        'show backend nodes
        For Each oBackend In mdlMain.Backends
            If oBackend.SupportsFolders Or oBackend.SupportsMultipleStores Then
                'create backend node-key can not convert to an integer
                UserControl.tvFolders.Nodes.Add _
                    Text:=oBackend.DisplayName, Key:="b" & oBackend.InternalID, _
                    Image:="Backend", SelectedImage:="Backend"
            Else
                'backend has only one store - show the store as a backend node
                Set oStores = oBackend.GetStores
                UserControl.tvFolders.Nodes.Add _
                    Text:=oBackend.DisplayName, Key:=oStores.Item(1).UNID, _
                    Image:="Backend", SelectedImage:="Backend"
            End If
        Next oBackend
    Else
        'show store nodes of backend 1 - get stores
        Set oBackend = GetBackendFromIndex(1)
        Set oStores = oBackend.GetStores
        
        'populate branch
        For Each oStore In oStores
            UserControl.tvFolders.Nodes.Add _
                Key:=oStore.UNID, Text:=oStore.Name, _
                Image:="ClosedStore", SelectedImage:="OpenStore"
        Next oStore
    End If
    Exit Function
ProcError:
    g_oError.RaiseError "CIX.CI.ShowTreeTop"
End Function

Private Function ShowTreeBranch(oNode As MSComctlLib.node) As Long
'creates next level nodes on tree
    Dim i As Integer
    Dim iNumNodes As Integer
    Dim oBackend As CIO.ICIBackend
    Dim oStore As CIO.CStore
    Dim oFolder As CIO.CFolder
    Dim oStores As CIO.CStores
    Dim oFolders As CIO.CFolders
    Dim vStoreID As Variant
    Dim vFolderID As Variant
    Dim xUNID As String
    
    On Error GoTo ProcError
'   get child node based on type of node
    xUNID = oNode.Key
    
    Select Case m_oUNID.GetUNIDType(xUNID)
        Case ciUNIDType_Folder
            'is folder - get subfolders
            Set oBackend = mdlMain.GetBackendFromUNID(xUNID)
            
            If oBackend.SupportsNestedFolders Then
                'create folder from UNID
                Set oFolder = m_oUNID.GetFolder(xUNID)
                Set oFolders = oBackend.GetSubFolders(oFolder)
                
                If Not oFolders Is Nothing Then
                    'populate branch
                    If Not oFolders Is Nothing Then
                        For Each oFolder In oFolders
                            UserControl.tvFolders.Nodes.Add _
                            oNode, tvwChild, oFolder.UNID, oFolder.Name, _
                            "ClosedFolder", "OpenFolder"
                        Next oFolder
                    End If
                End If
            End If
        Case ciUNIDType_Store
            'is store - get folders
            Set oBackend = mdlMain.GetBackendFromUNID(xUNID)
            
            If oBackend.SupportsFolders Then
                'create store from UNID
                Set oStore = m_oUNID.GetStore(xUNID)
                Set oFolders = oBackend.GetFolders(oStore)
            
                If Not oFolders Is Nothing Then
                    'populate branch
                    For Each oFolder In oFolders
                        UserControl.tvFolders.Nodes.Add _
                            oNode, tvwChild, oFolder.UNID, oFolder.Name, _
                            "ClosedFolder", "OpenFolder"
                    Next oFolder
                End If
            End If
        Case ciUNIDType_Backend
            'is backend - get stores
            Set oBackend = mdlMain.GetBackendFromUNID(xUNID)
                
            If oBackend.SupportsMultipleStores Then
                Set oStores = oBackend.GetStores
                
                If Not oStores Is Nothing Then
                    'populate branch
                    For Each oStore In oStores
                        UserControl.tvFolders.Nodes.Add _
                            oNode, tvwChild, oStore.UNID, oStore.Name, _
                                "ClosedStore", "OpenStore"
                    Next oStore
                End If
            Else
                Set oFolders = oBackend.GetFolders(Nothing)
            
                If Not oFolders Is Nothing Then
                    'populate branch
                    For Each oFolder In oFolders
                        UserControl.tvFolders.Nodes.Add _
                            oNode, tvwChild, oFolder.UNID, oFolder.Name, _
                            "ClosedFolder", "OpenFolder"
                    Next oFolder
                End If
           End If
    End Select
    Exit Function
ProcError:
    g_oError.RaiseError "CIX.CI.ShowTreeBranch"
End Function

Private Sub DeleteBranch(ByVal node As MSComctlLib.node)
'deletes an entire branch of the tree and all the associated
'NodeDetail objects - does so by recursive iterations
    
    On Error GoTo ProcError
    If node.Children Then
        While node.Children
            DeleteSubBranch node.Child
        Wend
    End If
    Exit Sub
ProcError:
    g_oError.RaiseError "CIX.CI.DeleteBranch"
End Sub

Private Sub DeleteSubBranch(ByVal node As MSComctlLib.node)
'deletes an entire branch of the tree and all the associated
'NodeDetail objects - does so by recursive iterations
    
    On Error GoTo ProcError
    If node.Children Then
        While node.Children
            DeleteSubBranch node.Child
        Wend
    Else
        Dim lIndex As Long
        lIndex = node.Index
        UserControl.tvFolders.Nodes.Remove lIndex
    End If
    Exit Sub
ProcError:
    g_oError.RaiseError "CIX.CI.DeleteBranch"
End Sub

Private Sub RefreshTreeBranch(ByVal node As MSComctlLib.node)
    On Error GoTo ProcError
    
'   delete existing branch
    DeleteBranch node
        
'   requery for updated tree branch
    ShowTreeBranch node
    
'   refresh toolbar if the refreshed node is the currently selected node
    If node Is UserControl.tvFolders.SelectedItem Then
        SetupDialogForNode node
    End If
    
    Exit Sub
ProcError:
    g_oError.RaiseError "CIX.CI.RefreshTreeBranch"
End Sub

Private Sub lstListings_RowChange()
    On Error GoTo ProcError
    With UserControl.lstListings
        If (Not IsPressed(KEY_CTL)) And _
            (Not IsPressed(KEY_SHIFT)) Then
            ClearListingSelections
            DoEvents
        End If
        
        .SelBookmarks.Add .Row + .FirstRow
        DoEvents
        
        OnListingsRowChange
    End With
    Exit Sub
ProcError:
    ShowError
    Exit Sub
End Sub

Private Sub m_oEvents_AfterExecutingListingsQuery()
    On Error GoTo ProcError
    With UserControl.lblStatusMsg
        .Visible = False
        .Caption = Empty
    End With
    Exit Sub
ProcError:
    ShowError
    Exit Sub
End Sub

Private Sub m_oEvents_AfterListingAdded(ByVal lIndexAdded As Long, ByVal lTotal As Long, Cancel As Boolean)
'update only every time the progress
'bar would add another square.
    Dim xMsg As String
    Dim sPercentComplete As Single
    Static dEstimatedTime As Double
        
    Static iContactsPerBarSquare As Integer
    Static t0 As Long
    Static t1 As Long
    
    On Error GoTo ProcError
    
    DoEvents
    
    If lTotal = 0 Then
        Exit Sub
    End If
    
    'get the estimated time to load 5% of contacts
    If lIndexAdded = 1 Then
        UserControl.MousePointer = vbDefault
        
        'get the time when #1 is added
        t0 = GetTickCount()
        iContactsPerBarSquare = CInt(lTotal / 26)
        dEstimatedTime = 0
    End If
    
    With UserControl.pbxLoad
        If (lIndexAdded / lTotal) >= 0.05 And dEstimatedTime = 0 Then
            '1% have been added - get time
            t1 = GetTickCount()
            
            'get the estimated time for all
            dEstimatedTime = CDbl((t1 - t0) / 0.05)
            If dEstimatedTime > 1000 Then
                'estimated time is more than a
                'second, show progress bar
                If Not .Visible Then
                    'show progress bar dialog
                    .Title = "Loading Contacts"
                    .ZOrder 0
                    .ShowCancelled = True
                    .Message = "Loading contacts.  (0%) complete."
                    .Value = 0
                    .Visible = True
                    DoEvents
                End If
            End If
        End If
    
        On Error Resume Next
        
        If .Visible Then
            'update the progress bar
            If lIndexAdded = lTotal Then
                'last contact has been added - close up
                .Message = "Loading contacts.  (100%) complete."
                .Value = 1
                DoEvents
                .Visible = False
                .Value = 0
            ElseIf (lIndexAdded Mod iContactsPerBarSquare) = 0 Then
                'enough contacts have been added to force
                'the display of another progress bar square
                sPercentComplete = (lIndexAdded / lTotal)
                .Message = "Loading contacts.  (" & _
                    Format(sPercentComplete, "#0%") & ") complete."
                .Value = (sPercentComplete)
            End If
            
            DoEvents
            
            If m_bLoadCancelled Then
    '           user cancelled the process -
                m_bLoadCancelled = False
                Cancel = True
                .Visible = False
                .Value = 0
                xMsg = "You have cancelled this operation. " & lIndexAdded & _
                    " of " & lTotal & " contacts have been loaded."
                MsgBox xMsg, vbInformation, App.Title
            End If
        End If
    End With
    Exit Sub
ProcError:
    ShowError
    Exit Sub
End Sub

Private Sub m_oEvents_AfterListingsRetrieved(ByVal lTotal As Long)
    On Error GoTo ProcError
    With UserControl
        .lblEntries.Caption = "&Available Contacts: (" & lTotal & ")"
        .pbxLoad.Visible = False
'        With UserControl.sbxSearch
'            .Visible = False
'            .Message = Empty
'        End With
        UserControl.MousePointer = vbDefault
    End With
    Exit Sub
ProcError:
    ShowError
    Exit Sub
End Sub

'Private Sub m_oEvents_AfterStoreSearched()
'    On Error GoTo ProcError
'    With UserControl.lblStatusMsg
'        .Visible = False
'        .Caption = Empty
'    End With
'    Exit Sub
'ProcError:
'    ShowError
'    Exit Sub
'End Sub

Private Sub m_oEvents_AfterStoreSearched()
    On Error GoTo ProcError
'    With UserControl.sbxSearch
'        .Visible = False
'        .Message = Empty
'    End With
    Exit Sub
ProcError:
    ShowError
    Exit Sub
End Sub

Private Sub m_oEvents_BeforeExecutingListingsQuery()
    With UserControl.lblStatusMsg
        UserControl.MousePointer = vbHourglass
        .Caption = "Preparing to retrieve contacts.  Please wait..."
        .Visible = True
        DoEvents
    End With
End Sub

Private Sub m_oEvents_BeforeStoreSearchFolderSearch(ByVal xFolderName As String, ByRef Cancel As Boolean)
    Dim xMsg As String

    On Error GoTo ProcError

    If UserControl.pbxLoad.Visible Then
        Exit Sub
    End If

    xMsg = "Searching folder '" & xFolderName & "'."
    
    With UserControl.lblStatusMsg
        .Caption = xMsg
        .Visible = True
    End With
    
    DoEvents
    
'    With UserControl.sbxSearch
'        If Not .Visible Then
'            'reset status dialog canceled
'            'value for next search
'            .Canceled = False
'
'            'show status dialog
'            .Message = xMsg
'            .ZOrder 0
'            .Visible = True
'        Else
'            .Message = xMsg
'        End If
'
'        Cancel = .Canceled
'
'        DoEvents
'    End With
    Exit Sub
ProcError:
    ShowError
    Exit Sub
End Sub

Private Sub mnuHelp_About_Click()
    On Error GoTo ProcError
    ShowAbout
    Exit Sub
ProcError:
    ShowError
    Exit Sub
End Sub

Private Sub mnuContacts_AddGroup_Click()
    On Error GoTo ProcError
    AddGroup
    Exit Sub
ProcError:
    ShowError
    Exit Sub
End Sub

Private Sub mnuContacts_DeleteGroup_Click()
    On Error GoTo ProcError
    DeleteGroup
    Exit Sub
ProcError:
    ShowError
    Exit Sub
End Sub

Private Sub mnuCIHelp_Click()
    On Error GoTo ProcError
    Shell "winhlp32.exe " & App.Path & "\CI2X.hlp"
    Exit Sub
ProcError:
    ShowError
    Exit Sub
End Sub

Private Sub mnuContacts_MoveDown_Click()
    MoveSelContactDown
End Sub

Private Sub mnuContacts_MoveUp_Click()
    MoveSelContactUp
End Sub

Private Sub mnuContacts_SelectAll_Click()
    On Error GoTo ProcError
    Select Case m_iCurList
        Case ciListBoxes_Listings
            SelectAllListings
        Case ciListBoxes_GroupMembers
            SelectAllGroupMembers
        Case Else
            SelectAllContacts
    End Select
    Exit Sub
ProcError:
    ShowError
    Exit Sub
End Sub

Private Sub mnuContacts_ShowDetail_Click()
    If m_iCurList = ciListBoxes_Listings Then
        ShowListingDetail
    Else
        ShowContactDetail 0
    End If
End Sub

Private Sub mnuContacts_ToggleContactList_Click()
    On Error GoTo ProcError
    ToggleListingsWidth
    Exit Sub
ProcError:
    ShowError
    Exit Sub
End Sub
Private Sub mnuFolders_Custom1_Click()
'runs the first custom menu item
    On Error GoTo ProcError
    
    CurrentBackend.CustomProcedure1
    
    'refresh tree
    RefreshBackendBranch
    
    Exit Sub
ProcError:
    g_oError.ShowError
    Exit Sub
End Sub

Private Sub mnuFolders_Custom2_Click()
'runs the second custom menu item
    Dim vFunction As Variant
    
    On Error GoTo ProcError
    CurrentBackend.CustomProcedure2
    
    'refresh tree
    RefreshBackendBranch
    
    Exit Sub
ProcError:
    g_oError.ShowError
    Exit Sub
End Sub

Private Sub mnuFolders_Custom3_Click()
'runs the first custom menu item
    On Error GoTo ProcError
    
    CurrentBackend.CustomProcedure3
    
    'refresh tree
    RefreshBackendBranch
    
    Exit Sub
ProcError:
    g_oError.ShowError
    Exit Sub
End Sub

Private Sub mnuFolders_Custom4_Click()
'runs the second custom menu item
    Dim vFunction As Variant
    
    On Error GoTo ProcError
    CurrentBackend.CustomProcedure4
    
    'refresh tree
    RefreshBackendBranch
    
    Exit Sub
ProcError:
    g_oError.ShowError
    Exit Sub
End Sub

Private Sub mnuFolders_Custom5_Click()
'runs the second custom menu item
    Dim vFunction As Variant
    
    On Error GoTo ProcError
    CurrentBackend.CustomProcedure5
    
    'refresh tree
    RefreshBackendBranch
    
    Exit Sub
ProcError:
    g_oError.ShowError
    Exit Sub
End Sub

Private Sub RefreshBackendBranch()
'refreshes the selected backend branch of the tree
    Dim oNode As Object
    
    On Error GoTo ProcError
    
    UserControl.Parent.MousePointer = vbHourglass
    UserControl.Refresh
    
    With UserControl.tvFolders
        If .SelectedItem.Parent Is Nothing Then
            Set oNode = .SelectedItem
        Else
            Set oNode = .SelectedItem.Parent
        End If
    End With
    
    RefreshTreeBranch oNode
    UserControl.Parent.MousePointer = vbDefault
    Exit Sub
ProcError:
    RaiseError "CIX.CI.RefreshBackendBranch"
    Exit Sub
End Sub

Private Sub mnuHelp_Contents_Click()
    On Error GoTo ProcError
    Shell "winhlp32.exe " & App.Path & "\CI2x.hlp", vbNormalFocus
    Exit Sub
ProcError:
    ShowError
    Exit Sub
End Sub

Private Sub mnuFolders_LoadOnStartup_Click()
    On Error GoTo ProcError
    SetDefaultFolder
    UserControl.mnuFolders_LoadOnStartup.Checked = True
    SetUserIni "CIApplication", ciUserIniKey_LoadDefaultOnStartup, True
    Exit Sub
ProcError:
    ShowError
    Exit Sub
End Sub

Private Sub mnuFolders_RemoveDefault_Click()
    On Error GoTo ProcError
    SetUserIni "CIApplication", ciUserIniKey_DefaultNode, Empty
    UserControl.mnuFolders_LoadOnStartup.Checked = False
    SetUserIni "CIApplication", ciUserIniKey_LoadDefaultOnStartup, False
    UserControl.btnOptions.ToolTipText = "Default Folder: None specified"
    Exit Sub
ProcError:
    ShowError
    Exit Sub
End Sub

Private Sub mnuFolders_SetAsDefault_Click()
    On Error GoTo ProcError
    SetDefaultFolder
    UserControl.mnuFolders_LoadOnStartup.Checked = False
    SetUserIni "CIApplication", ciUserIniKey_LoadDefaultOnStartup, False
    Exit Sub
ProcError:
    ShowError
    Exit Sub
End Sub

Private Sub SetDefaultFolder()
    Dim xFolder As String
    
    On Error GoTo ProcError
    xFolder = UserControl.tvFolders.SelectedItem.FullPath
    
    SetUserIni "CIApplication", ciUserIniKey_DefaultNode, _
        xFolder
        
    UserControl.btnOptions.ToolTipText = "Default Folder: " & xFolder
    Exit Sub
ProcError:
    RaiseError "CIX.CI.SetDefaultFolder"
    Exit Sub
End Sub

Private Sub pbxLoad_Cancelled()
    m_bLoadCancelled = True
    DoEvents
End Sub

Private Sub pbxLoad_OnHide()
    With UserControl
        .fraFolders.Enabled = True
        .fraToolbar.Enabled = True
    End With
End Sub

Private Sub pbxLoad_OnShow()
    With UserControl
        .fraFolders.Enabled = False
        .fraToolbar.Enabled = False
    End With
End Sub

Private Sub sbxSearch_OnHide()
    With UserControl
        .fraFolders.Enabled = True
        .fraToolbar.Enabled = True
    End With
End Sub

Private Sub sbxSearch_OnShow()
    With UserControl
        .fraFolders.Enabled = False
        .fraToolbar.Enabled = False
    End With
End Sub

Private Sub tvFolders_DblClick()
    On Error GoTo ProcError
    If CurrentBackend.IsLoadableEntity(UserControl.tvFolders.SelectedItem.Key) Then
        LoadContacts
    End If
    Exit Sub
ProcError:
    ShowError
End Sub

Private Sub tvFolders_MouseDown(Button As Integer, Shift As Integer, X As Single, Y As Single)
    Dim oNode As MSComctlLib.node

    On Error GoTo ProcError
    If Button = 2 Then
        'right click occurred - select node, then show menu
        Set oNode = UserControl.tvFolders.HitTest(X, Y)
        If Not (oNode Is Nothing) Then
            oNode.Selected = True
            tvFolders_NodeClick oNode
            UserControl.PopupMenu mnuFolders
        End If
    End If
    Exit Sub
ProcError:
    ShowError
    Exit Sub
End Sub

Private Sub tvFolders_NodeClick(ByVal node As MSComctlLib.node)
    On Error GoTo ProcError
    SetupDialogForNode node
    Exit Sub
ProcError:
    g_oError.ShowError
    Exit Sub
End Sub

Private Sub SetupDialogForNode(ByVal node As MSComctlLib.node)
    Dim iUNIDType As ciUNIDTypes
    
    On Error GoTo ProcError
    
    With UserControl
        'enable native search if current backend supports it
        .btnSearchNative.Enabled = CurrentBackend.SupportsNativeSearch
        .btnAddContactInNative.Enabled = CurrentBackend.SupportsContactAdd
        
        'get type of node
        iUNIDType = m_oUNID.GetUNIDType(node.Key)
        
        Dim bIsLoadable As Boolean
        Dim bIsSearchable As Boolean
        
        bIsLoadable = CurrentBackend.IsLoadableEntity(node.Key)
        bIsSearchable = CurrentBackend.IsSearchableEntity(node.Key)
        
        'enable 'find' button for searchable nodes
        .btnSearch.Enabled = bIsSearchable
        .btnQuickSearch.Enabled = bIsSearchable
        .txtFilterValue1.Enabled = bIsSearchable
        .cmbFilterFld1.Enabled = bIsSearchable
        .cmbOp1.Enabled = bIsSearchable
        .lblQuickSearch.Enabled = bIsSearchable
        
        'enable 'show contacts' button for loadable nodes
        .btnTogglePanels.Enabled = bIsLoadable
        
        .mnuFolders_LoadOnStartup.Enabled = bIsLoadable
    End With
    
    If Not node.Expanded Then
        UserControl.Parent.MousePointer = vbHourglass
        ExpandNode node
    End If
    
    'refresh quick search controls based
    'on backend of selected node
    RefreshQuickSearchControls
    
    'setup custom menu items
    SetupCustomMenuItems
    
    UserControl.Parent.MousePointer = vbDefault
    Exit Sub
ProcError:
    Dim lErr As Long
    Dim xDesc As String
    
    lErr = Err.Number
    xDesc = Err.Description
    
    UserControl.Parent.MousePointer = vbDefault
    
    Err.Number = lErr
    Err.Description = xDesc
    RaiseError "CIX.CI.SetupDialogForNode"
End Sub

Private Sub SetupCustomMenuItems()
'adds/removes menu items from popup menu based on backend-
'configuration string is of the form 'MenuCaption,FunctionName,NodeType
    Dim xMenuCaption As String
    Dim i As Integer
    Dim xConfigArray() As String
    
    On Error GoTo ProcError
    i = 1
    
    'remove any previous menus
    With UserControl
        .mnuFolders_Sep1.Visible = False
        .mnuFolders_Custom1.Visible = False
        .mnuFolders_Custom2.Visible = False
        .mnuFolders_Custom3.Visible = False
        .mnuFolders_Custom4.Visible = False
        .mnuFolders_Custom5.Visible = False
    End With
    
    'get first custom menu item caption
    xMenuCaption = CurrentBackend.CustomMenuItem1
    
    If xMenuCaption <> Empty Then
        With UserControl.mnuFolders_Custom1
            .Visible = True
            .Caption = xMenuCaption
        End With
    End If
    
    'get second custom menu item caption
    xMenuCaption = CurrentBackend.CustomMenuItem2
    
    If xMenuCaption <> Empty Then
        With UserControl.mnuFolders_Custom2
            .Visible = True
            .Caption = xMenuCaption
        End With
    End If
    
    'get third custom menu item caption
    xMenuCaption = CurrentBackend.CustomMenuItem3
    
    If xMenuCaption <> Empty Then
        With UserControl.mnuFolders_Custom3
            .Visible = True
            .Caption = xMenuCaption
        End With
    End If
    
    'get fourth custom menu item caption
    xMenuCaption = CurrentBackend.CustomMenuItem4
    
    If xMenuCaption <> Empty Then
        With UserControl.mnuFolders_Custom4
            .Visible = True
            .Caption = xMenuCaption
        End With
    End If
    
    'get fifth custom menu item caption
    xMenuCaption = CurrentBackend.CustomMenuItem5
    
    If xMenuCaption <> Empty Then
        With UserControl.mnuFolders_Custom5
            .Visible = True
            .Caption = xMenuCaption
        End With
    End If
    
    With UserControl
        .mnuFolders_Sep1.Visible = (.mnuFolders_Custom1.Visible Or _
            .mnuFolders_Custom2.Visible Or .mnuFolders_Custom3.Visible Or _
            .mnuFolders_Custom4.Visible Or .mnuFolders_Custom5.Visible)
    End With
    
    Exit Sub
ProcError:
    RaiseError "CIX.CI.SetupCustomMenuItems"
    Exit Sub
End Sub

Private Sub ExpandNode(ByVal node As MSComctlLib.node)
'expands the specified tree node
    On Error GoTo ProcError
    If node.Children = 0 Then
        ShowTreeBranch node
    End If
    node.Expanded = True
    Exit Sub
ProcError:
    Err.Raise Err.Number, "CIX.CI.ExpandNode", Err.Description
End Sub

Public Sub RefreshQuickSearchControls()
'refreshes the quick search fields and operators
'based on the backend of selected node
    Dim oBackend As CIO.ICIBackend
    Dim xUNID As String
    Dim xSelFilter1 As String
    Dim xSelOp1 As String
    Dim xSelFilter2 As String
    Dim xSelOp2 As String
    Dim oFilter As CIO.CFilter
    Dim iOps As ciSearchOperators
    Dim i As Integer
    Dim oFilterFlds As XArrayDB
    Dim oFilterOps As XArrayDB
    Dim oFld As CIO.CFilterField
    
    Static iPrevBackendID As Integer
    
'   get backend
    Set oBackend = CurrentBackend()

    'exit if backend didn't change
    If iPrevBackendID = oBackend.ID Then
        Exit Sub
    Else
        'set static var for next iteration
        iPrevBackendID = oBackend.ID
    End If
    
'   get currently selected filter -
'   used below to reselect in new lists
    On Error GoTo ProcError
    With UserControl
        xSelFilter1 = .cmbFilterFld1.Text
        xSelOp1 = .cmbOp1.Text
        'xSelFilter2 = .cmbFilterFld2.Text
        'xSelOp2 = .cmbOp2.Text
    End With
    
'   get filter for backend
    Set oFilter = oBackend.Filter

'   add each filter field from filter to xarray
    Set oFilterFlds = New XArrayDB
    With oFilterFlds
        .ReDim 0, -1, 0, 1
        For i = 1 To oFilter.CountFields
            .Insert 1, i - 1
            Set oFld = oFilter.FilterFields(i)
            .Value(i - 1, 0) = oFld.Name
            .Value(i - 1, 1) = oFld.ID
        Next i
    End With
    
    'bind array to both filter fields lists
    With UserControl
        .cmbFilterFld1.Array = oFilterFlds
        '.cmbFilterFld2.Array = oFilterFlds
        .cmbFilterFld1.ReBind
        
        'resize dropdowns
        ResizeTDBCombo .cmbFilterFld1, 4
       'ResizeTDBCombo .cmbFilterFld2, 4
    End With
    
'   add each filter operator for this backend to xarray
    Set oFilterOps = New XArrayDB
    iOps = oBackend.SearchOperators
    With oFilterOps
        .ReDim 0, -1, 0, 1
        If iOps And ciSearchOperator_BeginsWith Then
            i = .UpperBound(1) + 1
            .Insert 1, i
            .Value(i, 0) = "Begins With"
            .Value(i, 1) = ciSearchOperator_BeginsWith
        End If
        If iOps And ciSearchOperator_Contains Then
            i = .UpperBound(1) + 1
            .Insert 1, i
            .Value(i, 0) = "Contains"
            .Value(i, 1) = ciSearchOperator_Contains
        End If
        If iOps And ciSearchOperator_Equals Then
            i = .UpperBound(1) + 1
            .Insert 1, i
            .Value(i, 0) = "Is"
            .Value(i, 1) = ciSearchOperator_Equals
        End If
    End With
    
    'bind array to both filter fields lists
    With UserControl
        .cmbOp1.Array = oFilterOps
        .cmbOp1.ReBind
        '.cmbOp2.Array = oFilterOps
    
        'resize dropdowns
        ResizeTDBCombo .cmbOp1, 4
        'ResizeTDBCombo .cmbOp2, 4
    End With
    
'   return to previous selections if possible
    With UserControl.cmbFilterFld1
        If xSelFilter1 <> Empty Then
            .BoundText = xSelFilter1
        End If
        If IsNull(.SelectedItem) Then
            'no selection exists - use default
            .ReBind
'FIX: get default from user.ini
            If Not .Visible Then
                FixTDBCombo UserControl.cmbFilterFld1
            End If
            
            .SelectedItem = 0
        End If
    End With
    
'    With UserControl.cmbFilterFld2
'        If xSelFilter2 <> Empty Then
'            .BoundText = xSelFilter2
'        End If
'        If IsNull(.SelectedItem) Then
'            .ReBind
'            'no selection exists - use default
''FIX: get default from user.ini
'            If Not .Visible Then
'                FixTDBCombo UserControl.cmbFilterFld2
'            End If
'            .SelectedItem = 1
'        End If
'    End With
    
    With UserControl.cmbOp1
        If xSelOp1 <> Empty Then
            .BoundText = xSelOp1
        End If
        If IsNull(.SelectedItem) Then
            'no selection exists - use default
            .ReBind
'FIX: get default from user.ini
            If Not .Visible Then
                FixTDBCombo UserControl.cmbOp1
            End If
            .SelectedItem = 0
        End If
    End With
    Exit Sub
ProcError:
    RaiseError "CIX.CI.RefreshQuickSearchControls"
    Exit Sub
End Sub

Private Function CurrentBackend() As CIO.ICIBackend
'returns the backend of the selected node
    Dim oNode As MSComctlLib.node
    Dim xUNID As String
    
    Set oNode = UserControl.tvFolders.SelectedItem
    
    If Not oNode Is Nothing Then
'       get UNID
        xUNID = GetUNIDNoPrefix(oNode.Key)

'       get backend from UNID
        Set CurrentBackend = mdlMain.GetBackendFromUNID(xUNID)
    End If
End Function

Public Function GetUNIDNoPrefix(ByVal xUNID As String) As String
'returns the supplied UNID without
'the 'b' prefix if it exists
    On Error GoTo ProcError
    If Left(xUNID, 1) = "b" Then
        GetUNIDNoPrefix = Mid(xUNID, 2)
    Else
        GetUNIDNoPrefix = xUNID
    End If
    Exit Function
ProcError:
    RaiseError "CIX.CI.GetUNIDNoPrefix"
    Exit Function
End Function

Private Sub txtFilterValue1_GotFocus()
    UserControl.btnQuickSearch.Default = True
End Sub

Private Sub txtFilterValue1_LostFocus()
    UserControl.btnQuickSearch.Default = False
End Sub

Private Sub txtFilterValue2_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        LoadContacts ciSearchType_Quick
    End If
End Sub

Public Sub Initialize()
    Dim bLoadOnStartup As Boolean
    Dim oNode As MSComctlLib.node
    Dim i As Integer
    
    On Error GoTo ProcError
    
    Set g_oError = New CIO.CError
    
'   set the event generator to the event generator
'   of the backend of this store - we want to handle
'   events raised by that backend
    Set m_oEvents = CIO.CIEvents
            
    Set m_oUNID = New CIO.CUNID
    Set m_oContacts = New CIO.CContacts
    
    SetUpColumns
    
    'setting the mask at run time ensures that button graphics
    'have transparency - this is a problem with switching
    'between Windows XP and Windows Classic themes
    For i = 0 To 3
        UserControl.btnAdd(i).MaskColor = &HC0C0C0
    Next
    
    UserControl.btnHelp.MaskColor = &H8000000F
    UserControl.btnQuickSearch.MaskColor = &HC0C0C0
    UserControl.btnAddMember.MaskColor = &HC0C0C0
    UserControl.btnMoveDown.MaskColor = &HC0C0C0
    UserControl.btnMoveUp.MaskColor = &HC0C0C0
    
    'set Options button tooltip to display current default folder
    Dim xDefFolder As String
    xDefFolder = GetIni("CIApplication", "DefaultNode", CIO.CIUserIni())
    
    If xDefFolder <> Empty Then
        UserControl.btnOptions.ToolTipText = "Default Folder: " & xDefFolder
    Else
        UserControl.btnOptions.ToolTipText = "Default Folder: None specified"
    End If
    
    'show first level of tree
    ShowTreeTop
    
    'select a default node if specified
    Set oNode = SelectNode()
    
    If Not (oNode Is Nothing) Then
        'a default node has been selected
        On Error Resume Next
        bLoadOnStartup = CBool(GetUserIni("CIApplication", _
            ciUserIniKey_LoadDefaultOnStartup))
        On Error GoTo ProcError
        
        If bLoadOnStartup Then
            'user has specified to load contacts in default folder
            UserControl.mnuFolders_LoadOnStartup.Checked = True
            LoadContacts ciSearchType_None
        End If
        
        If Not (oNode Is Nothing) Then
            'set the proper quick search list items for the
            'specified node - these are backend specific
            RefreshQuickSearchControls
        End If
    End If
    
    m_bInitialized = True
    Exit Sub
ProcError:
    ShowError
End Sub

Private Sub SetUpToolbar()
    Dim oB As ICIBackend
    Dim oLastVisibleBtn As VB.CommandButton
    
    If Not Ambient.UserMode Then
        Exit Sub
    End If
    
    On Error GoTo ProcError
    
    With UserControl
        .mnuContacts_SelectAll.Caption = "Select &All" ' & vbTab & "(Ctrl + A)"
        .mnuContacts_ToggleContactList.Caption = "Expand Available Contacts &List" ' & vbTab & "(Ctrl + W)"
        .mnuContacts_ShowDetail.Caption = "Show &Detail" & vbTab & "Right Click on Contact"
        .mnuContacts_MoveUp.Caption = "Move Selected Contacts &Up" & vbTab & "Ctrl + Up Arrow"
        .mnuContacts_MoveDown.Caption = "Move Selected Contacts Do&wn" & vbTab & "Ctrl + Down Arrow"
    End With
    
    'hide 'Native Find' button if no backend supports this functionality
    With UserControl.btnSearchNative
        Dim bShowNativeSearch As Boolean
        
        'cycle through backend, finding one
        'backend that supports NativeSearch
        For Each oB In Backends()
            If oB.SupportsNativeSearch Then
                bShowNativeSearch = True
                Exit For
            End If
        Next oB
        
        If bShowNativeSearch Then
            .Visible = True
        Else
            .Visible = False
            'move refresh button
            UserControl.btnRefresh.Left = .Left
        End If
    End With
    
    Set oLastVisibleBtn = UserControl.btnRefresh
    
    'hide 'New' button if no backend supports this functionality
    With UserControl.btnAddContactInNative
        For Each oB In Backends()
            If oB.SupportsContactAdd Then
                .Visible = True
                .Left = oLastVisibleBtn.Left + oLastVisibleBtn.Width - 15
                Set oLastVisibleBtn = UserControl.btnAddContactInNative
                Exit For
            Else
                .Visible = False
            End If
        Next oB
    End With
    
    'hide 'Edit' button if no backend supports this functionality
    With UserControl.btnEdit
        For Each oB In Backends()
            If oB.SupportsContactEdit Then
                .Visible = True
                .Left = oLastVisibleBtn.Left + oLastVisibleBtn.Width - 15
                Set oLastVisibleBtn = UserControl.btnEdit
                Exit For
            Else
                .Visible = False
            End If
        Next oB
    End With
    
    UserControl.btnOptions.Left = oLastVisibleBtn.Left + oLastVisibleBtn.Width - 15
    Set oLastVisibleBtn = UserControl.btnOptions
    
    'hide 'Groups' button if appropriate
    With UserControl.chkGroups
        If Not (Backends().GroupsBackend Is Nothing) Then
            .Visible = True
            .Left = oLastVisibleBtn.Left + oLastVisibleBtn.Width - 15
        Else
            .Visible = False
        End If
    End With
        
    Exit Sub
ProcError:
    RaiseError "CIX.CI.SetUpToolbar"
    Exit Sub
End Sub

Private Sub UserControl_InitProperties()
    On Error GoTo ProcError
    InitProperties
    Exit Sub
ProcError:
    ShowError
    Exit Sub
End Sub

Private Sub UserControl_ReadProperties(PropBag As PropertyBag)
    On Error GoTo ProcError
    ReadProperties PropBag
    Exit Sub
ProcError:
    ShowError
    Exit Sub
End Sub

Private Sub UserControl_Resize()
    On Error GoTo ProcError
    ResizeControls
    Exit Sub
ProcError:
    ShowError
    Exit Sub
End Sub

Private Sub ResizeControls()
    On Error GoTo ProcError
    With UserControl
        .fraFolders.Width = .Width
        .fraListings.Width = .Width
        With .Line1
            .X1 = 0
            .X2 = UserControl.Width
        End With
        With .Line2
            .X1 = 0
            .X2 = UserControl.Width
        End With
    End With
    Exit Sub
ProcError:
    RaiseError "CIX.CI.ResizeControls"
    Exit Sub
End Sub

Private Sub UserControl_Show()
    Dim i As Integer
    
    If Ambient.UserMode = True Then
        'this is required to fix some funky behavior -
        'if missing, any default node other than IA will
        'cause the folders frame to disable!!!!
        With UserControl
            .fraFolders.Enabled = True
            .fraToolbar.Enabled = True
            .fraListings.Enabled = True
        End With

        SetUpToolbar
    End If
    
    On Error Resume Next
    UserControl.tvFolders.SetFocus
End Sub

Private Sub UserControl_WriteProperties(PropBag As PropertyBag)
    On Error GoTo ProcError
    WriteProperties PropBag
    Exit Sub
ProcError:
    RaiseError "CIX.CI.UserControl_WriteProperties"
    Exit Sub
End Sub

Private Sub InitProperties()
    On Error GoTo ProcError
    Me.OKButtonCaption = "OK"
    Me.ShowBackendNodes = True
    Me.DataRetrievedForTo = ciRetrieveData_All
    Me.DataRetrievedForFrom = ciRetrieveData_All
    Me.DataRetrievedForCC = ciRetrieveData_All
    Me.DataRetrievedForBCC = ciRetrieveData_All
    Me.SelectionList = CIX.ciSelectionList_To
    Exit Sub
ProcError:
    RaiseError "CIX.CI.ReadProperties"
    Exit Sub
End Sub

Private Sub WriteProperties(PropBag As PropertyBag)
    On Error GoTo ProcError
    With PropBag
        PropBag.WriteProperty "OKButtonCaption", Me.OKButtonCaption, "OK"
        PropBag.WriteProperty "DataRetrievedForTo", Me.DataRetrievedForTo, ciRetrieveData_All
        PropBag.WriteProperty "DataRetrievedForFrom", Me.DataRetrievedForFrom, ciRetrieveData_All
        PropBag.WriteProperty "DataRetrievedForCC", Me.DataRetrievedForCC, ciRetrieveData_All
        PropBag.WriteProperty "DataRetrievedForBCC", Me.DataRetrievedForBCC, ciRetrieveData_All
        PropBag.WriteProperty "SelectionList", Me.SelectionList, CIX.ciSelectionList_To
        PropBag.WriteProperty "MaxContacts", m_iMaxContacts, 0
        PropBag.WriteProperty "SelectionLists", Me.SelectionList, CIX.ciSelectionList_To
        PropBag.WriteProperty "ShowBackendNodes", Me.ShowBackendNodes, True
    End With
    Exit Sub
ProcError:
    RaiseError "CIX.CI.ReadProperties"
    Exit Sub
End Sub

Private Sub ReadProperties(PropBag As PropertyBag)
    On Error GoTo ProcError
    With PropBag
        Me.OKButtonCaption = PropBag.ReadProperty("OKButtonCaption", "OK")
        Me.DataRetrievedForTo = PropBag.ReadProperty("DataRetrievedForTo", ciRetrieveData_All)
        Me.DataRetrievedForFrom = PropBag.ReadProperty("DataRetrievedForFrom", ciRetrieveData_All)
        Me.DataRetrievedForCC = PropBag.ReadProperty("DataRetrievedForCC", ciRetrieveData_All)
        Me.DataRetrievedForBCC = PropBag.ReadProperty("DataRetrievedForBCC", ciRetrieveData_All)
        Me.SelectionList = PropBag.ReadProperty("SelectionList", CIX.ciSelectionList_To)
        Me.MaxContacts = PropBag.ReadProperty("MaxContacts", 0)
        Me.SelectionLists = PropBag.ReadProperty("SelectionLists", CIX.ciSelectionList_To)
        Me.ShowBackendNodes = PropBag.ReadProperty("ShowBackendNodes", True)
    End With
    Exit Sub
ProcError:
    RaiseError "CIX.CI.ReadProperties"
    Exit Sub
End Sub

Friend Function SetSelectionLists(ByVal iSelectionLists As ciSelectionLists)
'sets right-side selection lists visible/invisible
'based on iSelectionLists
    Const OneListPos1 As Integer = 870
    Const TwoListsPos1 As Integer = 870
    Const TwoListsPos2 As Integer = 3375
    Const ThreeListsPos1 As Integer = 870
    Const ThreeListsPos2 As Integer = 2437
    Const ThreeListsPos3 As Integer = 4005
    Const FourListsPos1 As Integer = 870
    Const FourListsPos2 As Integer = 2040
    Const FourListsPos3 As Integer = 3225
    Const FourListsPos4 As Integer = 4425

    Const OneListHeight As Integer = 4470
    Const TwoListsHeight As Integer = 1950
    Const ThreeListsHeight As Integer = 1320
    Const FourListsHeight As Integer = 900

    Const ButtonSeparation As Integer = 330
    Const LabelSeparation As Integer = 210
    
    Dim Lists() As Integer
    Dim iNumLists As Integer
    Dim i As Integer
    Dim ctlP As VB.Control
    
    ReDim Lists(0)
    
'   reset controls - make invisible and clean out tags
    For i = 0 To 3
        With UserControl
            .lblSelContacts(i).Visible = False
            .btnAdd(i).Visible = False
            .btnDelete(i).Visible = False
            .lstContacts(i).Visible = False
        End With
    Next i
    
    'get specified lists - if list is to be shown,
    'add list to array of lists
    If iSelectionLists >= ciSelectionList_BCC Then
        Lists(0) = 3
        iSelectionLists = iSelectionLists - ciSelectionList_BCC
    End If
    
    If iSelectionLists >= ciSelectionList_CC Then
        If Lists(0) <> Empty Then
            ReDim Preserve Lists(UBound(Lists) + 1)
        End If
        Lists(UBound(Lists)) = 2
        iSelectionLists = iSelectionLists - ciSelectionList_CC
    End If
    
    If iSelectionLists >= ciSelectionList_From Then
        If Lists(0) <> Empty Then
            ReDim Preserve Lists(UBound(Lists) + 1)
        End If
        Lists(UBound(Lists)) = 1
        iSelectionLists = iSelectionLists - ciSelectionList_From
    End If
    
    If iSelectionLists >= ciSelectionList_To Then
        If Lists(0) <> Empty Then
            ReDim Preserve Lists(UBound(Lists) + 1)
        End If
        Lists(UBound(Lists)) = 0
    End If
    
'   count number of lists
    iNumLists = UBound(Lists) + 1
    
    With UserControl
        Select Case iNumLists
            Case 1
                i = Lists(0)
                .lblSelContacts(i).Top = OneListPos1 - LabelSeparation
                .btnAdd(i).Top = OneListPos1 + 15
                .btnDelete(i).Top = .btnAdd(i).Top + ButtonSeparation
                
                .lblSelContacts(i).Visible = True
                .btnAdd(i).Visible = True
                .btnDelete(i).Visible = True
                
                With .lstContacts(i)
                    .Top = OneListPos1
                    .Height = OneListHeight
                    .Visible = True
                    .Visible = True
                End With
            Case 2
                i = Lists(0)
                .lblSelContacts(i).Top = TwoListsPos2 - LabelSeparation
                .lstContacts(i).Top = TwoListsPos2
                .btnAdd(i).Top = TwoListsPos2 + 15
                .btnDelete(i).Top = .btnAdd(i).Top + ButtonSeparation
            
                .lblSelContacts(i).Visible = True
                .btnAdd(i).Visible = True
                .btnDelete(i).Visible = True
                
                With .lstContacts(i)
                    .Top = TwoListsPos2
                    .Height = TwoListsHeight
                    .Visible = True
                End With
            
                i = Lists(1)
                .lblSelContacts(i).Top = TwoListsPos1 - LabelSeparation
                .lstContacts(i).Top = TwoListsPos1
                .btnAdd(i).Top = TwoListsPos1 + 15
                .btnDelete(i).Top = .btnAdd(i).Top + ButtonSeparation
                
                .lblSelContacts(i).Visible = True
                .btnAdd(i).Visible = True
                .btnDelete(i).Visible = True
                
                With .lstContacts(i)
                    .Top = TwoListsPos1
                    .Height = TwoListsHeight
                    .Visible = True
                End With
            
            Case 3
                i = Lists(0)
                'position controls for 3rd list
                .lblSelContacts(i).Top = ThreeListsPos3 - LabelSeparation
                .lstContacts(i).Top = ThreeListsPos3
                .btnAdd(i).Top = ThreeListsPos3 + 15
                .btnDelete(i).Top = .btnAdd(i).Top + ButtonSeparation
            
                .lblSelContacts(i).Visible = True
                .btnAdd(i).Visible = True
                .btnDelete(i).Visible = True
                
                With .lstContacts(i)
                    .Top = ThreeListsPos3
                    .Height = ThreeListsHeight
                    .Visible = True
                    .Visible = True
                End With
            
                i = Lists(1)
                .lblSelContacts(i).Top = ThreeListsPos2 - LabelSeparation
                .lstContacts(i).Top = ThreeListsPos2
                .btnAdd(i).Top = ThreeListsPos2 + 15
                .btnDelete(i).Top = .btnAdd(i).Top + ButtonSeparation
            
                .lblSelContacts(i).Visible = True
                .btnAdd(i).Visible = True
                .btnDelete(i).Visible = True
                
                With .lstContacts(i)
                    .Top = ThreeListsPos2
                    .Height = ThreeListsHeight
                    .Visible = True
                    .Visible = True
                End With
            
                i = Lists(2)
                .lblSelContacts(i).Top = ThreeListsPos1 - LabelSeparation
                .lstContacts(i).Top = ThreeListsPos1
                .btnAdd(i).Top = ThreeListsPos1 + 15
                .btnDelete(i).Top = .btnAdd(i).Top + ButtonSeparation
            
                .lblSelContacts(i).Visible = True
                .btnAdd(i).Visible = True
                .btnDelete(i).Visible = True
                
                With .lstContacts(i)
                    .Top = ThreeListsPos1
                    .Height = ThreeListsHeight
                    .Visible = True
                    .Visible = True
                End With
            
            Case 4
                i = Lists(0)
                .lblSelContacts(i).Top = FourListsPos4 - LabelSeparation
                .lstContacts(i).Top = FourListsPos4
                .btnAdd(i).Top = FourListsPos4 + 15
                .btnDelete(i).Top = .btnAdd(i).Top + ButtonSeparation
        
                .lblSelContacts(i).Visible = True
                .btnAdd(i).Visible = True
                .btnDelete(i).Visible = True
                
                With .lstContacts(i)
                    .Top = FourListsPos4
                    .Height = FourListsHeight
                    .Visible = True
                End With
            
                i = Lists(1)
                .lblSelContacts(i).Top = FourListsPos3 - LabelSeparation
                .lstContacts(i).Top = FourListsPos3
                .btnAdd(i).Top = FourListsPos3 + 15
                .btnDelete(i).Top = .btnAdd(i).Top + ButtonSeparation
        
                .lblSelContacts(i).Visible = True
                .btnAdd(i).Visible = True
                .btnDelete(i).Visible = True
                
                With .lstContacts(i)
                    .Top = FourListsPos3
                    .Height = FourListsHeight
                    .Visible = True
                End With
            
                i = Lists(2)
                .lblSelContacts(i).Top = FourListsPos2 - LabelSeparation
                .lstContacts(i).Top = FourListsPos2
                .btnAdd(i).Top = FourListsPos2 + 15
                .btnDelete(i).Top = .btnAdd(i).Top + ButtonSeparation
        
                .lblSelContacts(i).Visible = True
                .btnAdd(i).Visible = True
                .btnDelete(i).Visible = True
                
                With .lstContacts(i)
                    .Top = FourListsPos2
                    .Height = FourListsHeight
                    .Visible = True
                End With
            
                i = Lists(3)
                .lblSelContacts(i).Top = FourListsPos1 - LabelSeparation
                .lstContacts(i).Top = FourListsPos1
                .btnAdd(i).Top = FourListsPos1 + 15
                .btnDelete(i).Top = .btnAdd(i).Top + ButtonSeparation
                
                .lblSelContacts(i).Visible = True
                .btnAdd(i).Visible = True
                .btnDelete(i).Visible = True
                
                With .lstContacts(i)
                    .Top = FourListsPos1
                    .Height = FourListsHeight
                    .Visible = True
                End With
        End Select
    End With
End Function

Public Sub SetUpColumns()
'sets column width and position based on ini settings
    Dim xValue As String
    Dim i As Integer
    Dim sWidth As Single
    Dim iPos As Integer
    Dim iIndex As Integer
    Dim xWidth As String
    
    On Error GoTo ProcError
    For i = 0 To 3
        'get ini value
        xValue = GetUserIni("CIColumns", i)
        If xValue <> Empty Then
            
            'parse into width and position
            iPos = InStr(xValue, ",")
            If iPos > 0 Then
                iIndex = Left(xValue, iPos - 1)
                xWidth = Mid$(xValue, iPos + 1)
                
                'localize to regional setting
                sWidth = xLocalizeNumericString(xWidth)
            End If
            
            With UserControl.lstListings.Columns(i)
                .Order = iIndex
                .Width = sWidth
            End With
        End If
    Next i
    Exit Sub
ProcError:
    RaiseError "CIX.CI.SetUpColumns"
    Exit Sub
End Sub

Public Sub SaveColumns()
'save column width and position to ini
    Dim i As Integer
    
    On Error GoTo ProcError
    'cycle through columns
    For i = 0 To 3
        With UserControl.lstListings.Columns(i)
            'write settings in form 'Order,width'
            SetUserIni "CIColumns", i, .Order & "," & .Width
        End With
    Next i
    Exit Sub
ProcError:
    RaiseError "CIX.CI.SaveColumns"
    Exit Sub
End Sub

Public Function SelectNode(Optional ByVal xPath As String) As MSComctlLib.node
'selects folder specified in xPath
'if not supplied, ini default is selected-
'if no ini default specified, first node is selected-
'expands tree as necessary
    Dim iSepPos As Integer
    Dim i As Integer
    Dim xCurLevel As String
    Dim xRemainingLevels As String
    Dim oCurNode As MSComctlLib.node
    Dim oChildNode As MSComctlLib.node
    
    On Error GoTo ProcError
    
    If xPath = Empty Then
'       not supplied - get from ini
        xPath = GetUserIni("CIApplication", ciUserIniKey_DefaultNode)
        If xPath = Empty Then
            'no default path - select first node
            With UserControl.tvFolders
                If .Nodes.Count > 0 Then
                    Set .SelectedItem = .Nodes.Item(1)
                    tvFolders_NodeClick .SelectedItem
                End If
            End With
            Exit Function
        End If
    End If
    
    With UserControl.tvFolders
'       select level while sep exists-
'       expand tree as necessary
        iSepPos = InStr(xPath, "\")
        If iSepPos Then
'           parse xPath
            xCurLevel = Left(xPath, iSepPos - 1)
            xRemainingLevels = Mid(xPath, iSepPos + 1)
        
            For i = 1 To .Nodes.Count
                If .Nodes(i) = xCurLevel Then
                    Set oCurNode = .Nodes(i)
                    oCurNode.Selected = True
                    Exit For
                End If
            Next i
            
            If oCurNode Is Nothing Then
                If UserControl.tvFolders.Nodes.Count > 0 Then
                    UserControl.tvFolders.Nodes.Item(1).Selected = True
                    Set SelectNode = UserControl.tvFolders.SelectedItem
                End If
                
                Err.Raise ciErr_DefaultFolderDoesNotExist
            End If
            
'           expand tree if necessary
            If Not (oCurNode.Children > 0) Then
                ShowTreeBranch oCurNode
            End If
            
            oCurNode.Expanded = True
            
'           get next separator
            iSepPos = InStr(xRemainingLevels, "\")
        
            While iSepPos
'               index first child
                Set oChildNode = oCurNode.Child
            
                If oChildNode Is Nothing Then
                    Err.Raise ciErr_DefaultFolderDoesNotExist
                End If
    
'               parse xPath
                xCurLevel = Left(xRemainingLevels, iSepPos - 1)
                xRemainingLevels = Mid(xRemainingLevels, iSepPos + 1)
            
'               loop through child folders until xCurLevel is found
                While oChildNode <> xCurLevel
                    Set oChildNode = oChildNode.Next
                    If oChildNode Is Nothing Then
                        Err.Raise ciErr_DefaultFolderDoesNotExist
                    End If
                Wend
                
                Set oCurNode = oChildNode
                oCurNode.Selected = True
                
'               expand tree
                ShowTreeBranch oCurNode
            
'               get next separator
                iSepPos = InStr(xRemainingLevels, "\")
            Wend
    
'           select final level folder node-
'           index first child
            Set oChildNode = oCurNode.Child
            oCurNode.Selected = True
            
            If oChildNode Is Nothing Then _
                Err.Raise ciErr_DefaultFolderDoesNotExist
    
'           parse xPath- search for trailing NULL chr
            iSepPos = InStr(xRemainingLevels, Chr(0))
            If iSepPos Then
                xCurLevel = Left(xRemainingLevels, iSepPos - 1)
            Else
                xCurLevel = xRemainingLevels
            End If
        
'           loop through child folders until xCurLevel is found
            While oChildNode <> xCurLevel
                Set oChildNode = oChildNode.Next
                If oChildNode Is Nothing Then
                    Err.Raise ciErr_DefaultFolderDoesNotExist
                End If
            Wend
            
            Set oCurNode = oChildNode
        Else
'           select first level node
            For i = 1 To .Nodes.Count
                If UCase(.Nodes(i)) = UCase(xPath) Then
                    Set oCurNode = .Nodes(i)
                    Exit For
                End If
            Next i
            If oCurNode Is Nothing Then
                Err.Raise ciErr_DefaultFolderDoesNotExist
            End If
        End If
            
'       select node
        If Not (oCurNode Is Nothing) Then
            With oCurNode
                .Selected = True
                tvFolders_NodeClick oCurNode
            End With
        End If
        
        'return selected node
        Set SelectNode = oCurNode
    End With
    Exit Function
    
ProcError:
    Dim lErr As Long
    Dim xDesc As String
    
    lErr = Err.Number
    xDesc = Err.Description
    
    If Err.Number = ciErrs.ciErr_DefaultFolderDoesNotExist Then
        With UserControl.tvFolders
            If Not .SelectedItem Is Nothing Then
                RefreshQuickSearchControls
            End If
        End With
        
        MsgBox "The default folder '" & xPath & _
            "' does not exist.  Please reset your default folder.", _
            vbExclamation, App.Title
        Exit Function
    End If
    
    Err.Number = lErr
    Err.Description = xDesc
    RaiseError "CIX.CI.SelectNode"
    Exit Function
End Function

Private Sub ShowPanel(iPanel As CIPanels)
'displays the specified panel
    Dim iUNIDType As CIO.ciUNIDTypes
    
    On Error GoTo ProcError
    With UserControl
        If iPanel = ciPanel_folders Then
            .fraFolders.Visible = True
            .fraContacts.Visible = False
            .fraGroups.Visible = False
            .fraListings.Visible = False
            .btnTogglePanels.Caption = "&Show Contacts"
            
            'get type of node
            iUNIDType = m_oUNID.GetUNIDType(.tvFolders.SelectedItem.Key)
            
            .btnTogglePanels.Enabled = (iUNIDType = ciUNIDType_Folder)
            
            'enable 'find' button for 1)folders and
            '2)stores that support store searching
            .btnSearch.Enabled = CurrentBackend.IsSearchableEntity( _
                .tvFolders.SelectedItem.Key)
                
            'enable 'find' button for 1)folders and
            '2)stores that support store loading
            .btnTogglePanels.Enabled = CurrentBackend.IsLoadableEntity( _
                .tvFolders.SelectedItem.Key)
                
            .btnEdit.Enabled = False
        ElseIf iPanel = ciPanel_Contacts Then
            m_bShowedContacts = True
            
            'set up for contacts
            .fraFolders.Visible = False
            .fraListings.Visible = True
            .fraContacts.Visible = True
            .fraGroups.Visible = False
            .btnTogglePanels.Caption = "&Show Folders"
            .btnTogglePanels.Enabled = True
            .btnSearch.Enabled = True
            .cmdOK.Caption = "&Insert"
            .mnuContacts_AddGroup.Enabled = False
            .mnuContacts_DeleteGroup.Enabled = False
            
            'show contacts frame if listings box is not Wide
            .fraContacts.Visible = (.lstListings.Width = ciListingsNarrow)
        Else
            'set up for groups
            .fraFolders.Visible = False
            .fraListings.Visible = True
            .fraContacts.Visible = False
            .fraGroups.Visible = True
            .btnTogglePanels.Caption = "&Show Folders"
            .btnTogglePanels.Enabled = True
            .btnSearch.Enabled = True
            .cmdOK.Caption = "Load Gro&up"
            .btnAddMember.Default = True
            .btnDeleteMember.Enabled = False
            .mnuContacts_AddGroup.Enabled = True
            .mnuContacts_DeleteGroup.Enabled = True
            
            'show groups frame if listings box is not Wide
            .fraGroups.Visible = (.lstListings.Width = ciListingsNarrow)
        End If
        .chkGroups.Enabled = iPanel <> ciPanel_folders
    End With
    UserControl.Refresh
    Exit Sub
ProcError:
    Err.Raise Err.Number, "CIX.CI.ShowPanel", Err.Description
End Sub

Private Sub UserControl_KeyDown(KeyCode As Integer, Shift As Integer)
    Dim l As Long

    If Ambient.UserMode = True Then
        On Error GoTo ProcError
        If IsPressed(KEY_CTL) Then
            If KeyCode = vbKeyF6 Then 'F6
                ShowAbout
            ElseIf KeyCode = vbKeyW And UserControl.lstListings.Visible Then 'w
                ToggleListingsWidth
            ElseIf KeyCode = vbKeyG Then
                If UserControl.mnuContacts_AddGroup.Enabled Then
                    AddGroup
                End If
            ElseIf KeyCode = vbKeyU Then
                If UserControl.mnuContacts_DeleteGroup.Enabled Then
                    DeleteGroup
                End If
            End If
        Else
            If KeyCode = vbKeyF5 Then
                LoadContacts ciSearchType_Refresh
            ElseIf KeyCode = vbKeyF1 Then
                mnuHelp_Contents_Click
            End If
        End If
    End If
    Exit Sub
ProcError:
    ShowError
End Sub

Private Sub ShowAbout()
    Dim oForm As frmAbout
    On Error GoTo ProcError
    Set oForm = New frmAbout
    oForm.Show vbModal
    Set oForm = Nothing
    Exit Sub
ProcError:
    RaiseError "CIX.CI.ShowAbout"
    Exit Sub
End Sub

Private Sub UserControl_Terminate()
    On Error GoTo ProcError
    
    Set m_oUNID = Nothing
    Set m_oContacts = Nothing
    Exit Sub
ProcError:
    ShowError
    Exit Sub
End Sub

Sub ToggleListingsWidth()
'toggles the width of the left-side list box
    On Error GoTo ProcError
    Dim bEnable As Boolean
    Dim i As Integer
    Dim sRightBorder As Single
    Dim sWide As Single

    With UserControl
        sRightBorder = .fraContacts.Left + .lstContacts(0).Left + .lstContacts(0).Width
        sWide = sRightBorder - .lstListings.Left
            
        If .lstListings.Width = sWide Then
            .lstListings.Width = ciListingsNarrow
            .mnuContacts_ToggleContactList.Caption = "Expand Available Contact &List" '     (Ctrl+w)"
        Else
            .lstListings.Width = sWide
            .mnuContacts_ToggleContactList.Caption = "Collapse Available Contact &List" '     (Ctrl+w)"
        End If
        
        bEnable = (.lstListings.Width = ciListingsNarrow)
        
        If UserControl.chkGroups.Value = vbUnchecked Then
            'show/hide contacts frame
            .fraContacts.Visible = bEnable
            .fraGroups.Visible = False
        Else
            'show/hide groups frame
            .fraGroups.Visible = bEnable
            .fraContacts.Visible = False
        End If
        
        UserControl.chkGroups.Enabled = bEnable
    End With
    DoEvents
    Exit Sub
ProcError:
    RaiseError "CIX.CI.ToggleListingsWidth"
    Exit Sub
End Sub

Sub ClearContacts()
'clears the contacts chosen - clears lists
    On Error GoTo ProcError
    
    If Not m_oContacts Is Nothing Then
        m_oContacts.DeleteAll
    End If
    
    With UserControl.lstContacts
        .Item(0).Clear
        .Item(1).Clear
        .Item(2).Clear
        .Item(3).Clear
    End With
    
    With UserControl.lstListings
        On Error Resume Next
        .ClearSelCols
        .Row = 0
        On Error GoTo ProcError
    End With
    Exit Sub
ProcError:
    RaiseError "CIX.CI.ClearContacts"
    Exit Sub
End Sub

Public Function GroupsBackend() As ICIBackend
'returns the groups backend
    On Error GoTo ProcError
    Set GroupsBackend = mdlMain.Backends.GroupsBackend
    Exit Function
ProcError:
    RaiseError "CIX.CI.Groups"
    Exit Function
End Function

Public Sub OnSelListChange(iListIndex As Integer)
    Dim i As Integer
    Dim j As Integer
    
    On Error GoTo ProcError
    With UserControl.lstContacts
'       deselect all entries, disable
'       delete buttons, and format controls
'       in inactive lists
        For i = .LBound To .UBound
            If i <> iListIndex Then
                For j = 0 To .Item(i).ListCount - 1
                    .Item(i).Selected(j) = False
                Next j
                UserControl.lblSelContacts.Item(i).ForeColor = vbBlack
                UserControl.btnDelete(i).Enabled = False
            End If
        Next i
    End With
    
'   set properties of active list controls
    UserControl.btnAdd(iListIndex).Default = True
    
'   if multiple lists are visible, make active one blue
    Select Case Me.SelectionLists
        Case ciSelectionList_To, _
             ciSelectionList_From, _
             ciSelectionList_CC, _
             ciSelectionList_BCC
            UserControl.lblSelContacts.Item(iListIndex).ForeColor = vbBlack
        Case Else
            UserControl.lblSelContacts.Item(iListIndex).ForeColor = vbBlue
    End Select
    
'   select first item in list if no selections exist
    With UserControl.lstContacts(iListIndex)
        If .SelCount = 0 And .ListCount Then
            .Selected(0) = True
        End If
    End With
    
'   enable delete button if an item is selected
    UserControl.btnDelete(iListIndex).Enabled = _
            UserControl.lstContacts(iListIndex).ListCount
    Exit Sub
ProcError:
    RaiseError "CIX.CI.OnSelListChange"
    Exit Sub
End Sub

Private Sub SetupForInputState(ByVal bIsReadyForInput As Boolean)
    On Error GoTo ProcError
    With UserControl
'        .Enabled = bIsReadyForInput
        .fraContacts.Enabled = bIsReadyForInput
        .fraFolders.Enabled = bIsReadyForInput
        .fraGroups.Enabled = bIsReadyForInput
        .fraListings.Enabled = bIsReadyForInput
        .fraToolbar.Enabled = bIsReadyForInput
        .cmdCancel.Cancel = bIsReadyForInput
        .cmdCancel.Enabled = bIsReadyForInput
    End With
    Exit Sub
ProcError:
    RaiseError "CIX.CI.SetupForInputState"
    Exit Sub
End Sub
