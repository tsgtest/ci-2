Attribute VB_Name = "mdlMain"
Option Explicit
Private Declare Function GetKeyState Lib "user32" ( _
    ByVal nVirtKey As Long) As Integer
    
Declare Function GetPrivateProfileString Lib "kernel32" Alias _
    "GetPrivateProfileStringA" (ByVal lpApplicationName As String, _
    ByVal lpKeyName As Any, ByVal lpDefault As String, ByVal _
    lpReturnedString As String, ByVal nSize As Long, ByVal lpFileName _
    As String) As Long
    
Declare Function WritePrivateProfileString Lib "kernel32" Alias _
    "WritePrivateProfileStringA" (ByVal lpApplicationName As String, _
    ByVal lpKeyName As Any, ByVal lpString As Any, ByVal lpFileName As _
    String) As Long

Declare Function GetUserName Lib "advapi32.dll" Alias "GetUserNameA" ( _
    ByVal lpBuffer As String, nSize As Long) As Long

Private Declare Sub OutputDebugString Lib "kernel32" _
  Alias "OutputDebugStringA" _
  (ByVal lpOutputString As String)
  
Public Declare Function ShellExecute Lib "shell32.dll" _
  Alias "ShellExecuteA" (ByVal hWnd As Long, _
  ByVal lpOperation As String, ByVal lpFile As String, _
  ByVal lpParameters As String, ByVal lpDirectory As String, _
  ByVal nShowCmd As Long) As Long

Private Declare Function GetVersionEx Lib "kernel32" Alias "GetVersionExA" _
    (lpVersionInformation As OSVERSIONINFO) As Long
    
Private Type OSVERSIONINFO
  OSVSize         As Long
  dwVerMajor      As Long
  dwVerMinor      As Long
  dwBuildNumber   As Long
  PlatformID      As Long
  szCSDVersion    As String * 128
End Type


Private Const SW_SHOWNORMAL = 1

Public Const KEY_SHIFT = &H10
Public Const KEY_CTL = &H11
Public Const KEY_MENU = &H12
Public Const KEY_F1 = &H70
Public Const KEY_F5 = &H74
Public Const KEY_a = 65
Public Const KEY_DEL = 46
Public Const KEY_ENTER = 13

'HTML help
Public Declare Function HtmlHelp Lib "hhctrl.ocx" Alias "HtmlHelpA" _
                        (ByVal hwndCaller As Long, ByVal pszFile As String, _
                        ByVal uCommand As Long, ByVal dwData As Long) As Long
                        
Public Const HH_DISPLAY_TOPIC = &H0         ' select last opened tab,[display a specified topic]
Public Const HH_DISPLAY_TOC = &H1           ' select contents tab, [display a specified topic]
Public Const HH_DISPLAY_INDEX = &H2         ' select index tab and searches for a keyword
Public Const HH_DISPLAY_SEARCH = &H3        ' select search tab and perform a search

Public g_oError As CIO.CError
Public g_oSessionType As CIO.CSessionType
Public g_oIni As CIO.CIni
Public g_vCols(3, 1) As Variant
Private m_oBackends As CIX.CBackends
Public g_oGlobals As CIO.CGlobals
Public g_oConstants As CIO.CConstants
Public g_oBackend As Object
Public g_iDefaultMaxContacts As Integer
Public g_xHelpPath As String
Public g_xHelpFiles() As String
Public oFSO As FileSystemObject
Public g_bIsWindows8 As Boolean
Public g_bIsWord15 As Boolean
Public g_iDefaultFilter As Integer

Public Const g_xUnvalidAddress As String = "A valid Address type does not exist.  Please contact your administrator."

Public Sub Main()
    On Error GoTo ProcError
    
    'initialize a CIO session
    Set g_oError = New CIO.CError
    Set g_oIni = New CIO.CIni
    Set g_oGlobals = New CIO.CGlobals
    Set g_oConstants = New CIO.CConstants
    Set g_oSessionType = New CIO.CSessionType
    
    'remove old debug
    On Error Resume Next
    Kill g_oIni.ApplicationDirectory & "\ciDebug.log"
    On Error GoTo ProcError

    Exit Sub
    
ProcError:
    If g_oError Is Nothing Then
        MsgBox "Could not initialize error object in CIX.ocx.", _
            vbCritical, g_oSessionType.AppTitle
    Else
        g_oError.ShowError
    End If
End Sub

Public Function GetSummary(oContact As CIO.CContact, Optional ByVal xFormatTokenPhrase As String) As String
'gets a summary string for the specified contact
    Dim xToken As String
    
    On Error GoTo ProcError
    
    If xFormatTokenPhrase <> Empty Then
        GetSummary = oContact.GetDetail(xFormatTokenPhrase)
    Else
        xToken = "{<PREFIX> }{<FIRSTNAME> }{<MIDDLENAME> }{<LASTNAME> }{<SUFFIX>}" & vbCrLf
        
        Dim xType As String
        xType = UCase$(oContact.AddressTypeName)
        If InStr(xType, "BUSINESS") Or InStr(xType, "OFFICE") Or InStr(xType, "MAIN") Then
            xToken = xToken & "{<TITLE>}" & vbCrLf & "{<COMPANY>}" & vbCrLf
        End If
            
        xToken = xToken & "{<STREET1>}" & vbCrLf & _
                "{<STREET2>}" & vbCrLf & _
                "{<STREET3>}" & vbCrLf & _
                "{<ADDITIONALINFORMATION>}" & vbCrLf & _
                "{<CITY>{, }<STATE>}{  <ZIPCODE>}"
        GetSummary = oContact.GetDetail(xToken) & _
                    vbCrLf & vbCrLf & vbCrLf
                    
        xToken = "Phone:  {<PHONE>}{  x<PHONEEXTENSION>}" & vbCrLf & _
                 "Fax:  {<FAX>}" & vbCrLf & _
                 "EMail:  {<EMAILADDRESS>}"

        GetSummary = GetSummary & oContact.GetDetail(xToken)

    End If
    Exit Function
ProcError:
    g_oError.RaiseError "CIX.mdlMain.GetSummary"
    Exit Function
End Function

Public Function GetFilter(ByVal iBackendID As Integer) As CIO.CFilter
'returns the collection of filter fields for specified backend
    On Error GoTo ProcError
    Set GetFilter = GetBackendFromID(iBackendID).Filter()
    Exit Function
ProcError:
    g_oError.RaiseError "mdlMain.GetFilter"
End Function

Public Function Backends() As CIX.CBackends
    On Error GoTo ProcError
    If m_oBackends Is Nothing Then
        GetBackends
    End If
    Set Backends = m_oBackends
    Exit Function
ProcError:
    g_oError.RaiseError "mdlMain.Backends"
End Function

Public Sub GetBackends()
    Dim i As Integer
    Dim xProgID As String
    Dim xBackendSec As String
    Dim bExists As Boolean
    
    On Error GoTo ProcError
    
    Set m_oBackends = New CIX.CBackends

    'cycle through backends in ini,
    'creating a backend for each one
    Do
        i = i + 1

'       check to see if there's an ini
'       section for a backend with this index
        xBackendSec = g_oIni.GetIniSection( _
            "Backend" & i, g_oIni.CIIni)

        On Error Resume Next
        bExists = (xBackendSec <> Chr(0)) And (xBackendSec <> Empty)
        On Error GoTo ProcError
        
        If bExists Then
'           backend exists - get id of backend
            Dim iID As Integer
            m_oBackends.Add (i)
        End If
    Loop While bExists
    If m_oBackends.Count = 0 Then
        Err.Raise ciErrs.ciErr_NoConnectedBackends, , _
            "Could not connect to any contact sources."
    End If
    
    Exit Sub
ProcError:
    g_oError.RaiseError "CIX.mdlMain.GetBackends"
End Sub

Public Function GetBackendFromUNID(ByVal xUNID As String) As CIO.ICIBackend
    Dim oUNID As CUNID
    Dim iID As Integer
    
    On Error GoTo ProcError
    
    'trim preceding 'b' - this is done for the backend tree node keys
    If Left$(xUNID, 1) = "b" Then
        xUNID = Mid$(xUNID, 2)
    End If
    
    Set oUNID = New CUNID
    iID = oUNID.GetUNIDField(xUNID, ciUNIDFields_Backend)
    Set GetBackendFromUNID = Backends.ItemFromID(iID)
    Exit Function
ProcError:
    g_oError.RaiseError "CIX.mdlMain.GetBackendFromUNID"
    Exit Function
End Function

Public Function GetBackendFromID(ByVal iID As Integer) As CIO.ICIBackend
    On Error GoTo ProcError
    Set GetBackendFromID = Backends.ItemFromID(iID)
    Exit Function
ProcError:
    g_oError.RaiseError "CIX.mdlMain.GetBackendFromID"
    Exit Function
End Function

Public Function GetBackendFromIndex(ByVal iIndex As Integer) As CIO.ICIBackend
    On Error GoTo ProcError
    Set GetBackendFromIndex = Backends.ItemFromIndex(iIndex)
    Exit Function
ProcError:
    g_oError.RaiseError "CIX.mdlMain.GetBackendFromIndex"
    Exit Function
End Function

Public Function IsPressed(Key As Variant) As Boolean
    IsPressed = (GetKeyState(Key) < 0)
End Function

Public Sub ResizeTDBCombo(tdbCBX As TrueDBList60.TDBCombo, Optional iMaxRows As Integer)
'resizes the dropdown of the true db combo
'to display the specified number of rows.
    Dim oArr As XArrayDB
    Dim iRows As Integer
    
    On Error GoTo ProcError
    With tdbCBX
        Set oArr = .Array
        If oArr Is Nothing Then
            Err.Raise cixErr_ObjectExpected, , _
                "No XArray has been assigned to " & tdbCBX.Name & "."
        End If
        
        If oArr.Count(1) < iMaxRows Then
            iRows = oArr.Count(1)
        Else
            iRows = iMaxRows
        End If
            
        On Error Resume Next
        .DropdownHeight = ((iRows) * .RowHeight + 8)
        
    End With
    Exit Sub
ProcError:
    Err.Raise Err.Number, "CIX.mdlMain.ResizeTDBCombo", Err.Description
    Exit Sub
End Sub

Public Function lCountChrs(xSource As String, xSearch As String) As Long
'returns the number of instances
'of xSearch in xSource

    Dim iPos As Integer
    Dim l As Long
    
    iPos = InStr(xSource, xSearch)
    While iPos
        l = l + 1
        iPos = InStr(iPos + 1, xSource, xSearch)
    Wend
    lCountChrs = l
End Function

Public Function ciMin(i As Double, j As Double) As Double
    If i > j Then
        ciMin = j
    Else
        ciMin = i
    End If
End Function
Public Function ciMax(i As Double, j As Double) As Double
    If i > j Then
        ciMax = i
    Else
        ciMax = j
    End If
End Function

Public Function GetUserIni(ByVal xSection As String, ByVal xKey As String)
'returns the value for the specified key in the User.ini
    Static xUserIni As String
    Dim bUserIniExists As Boolean
    
    On Error Resume Next
    
    If xUserIni = Empty Then
        xUserIni = g_oIni.CIUserIni
    End If
    GetUserIni = g_oIni.GetIni(xSection, xKey, xUserIni)
    Exit Function
ProcError:
    g_oError.RaiseError "CIX.mdlMain.GetUserIni"
End Function

Public Sub SetUserIni(ByVal xSection As String, _
                       ByVal xKey As String, _
                       ByVal xValue As String)
'sets the value of the specified key in User.ini
    Static xUserIni As String
    
    On Error GoTo ProcError
    If xUserIni = Empty Then
        xUserIni = g_oIni.CIUserIni
    End If
    
    g_oIni.SetIni xSection, xKey, xValue, xUserIni
    Exit Sub
ProcError:
    g_oError.RaiseError "CIX.mdlMain.SetUserIni"
End Sub

Public Sub Pause(lSecs As Long)
'pauses the code for specified duration
    Dim datStart As Date
    Dim datEnd As Date
    
    datStart = Now
    datEnd = datStart + (lSecs / 86200)
    
    While Now() <= datEnd
        DoEvents
    Wend
End Sub

Public Sub CorrectTDBComboMismatch(oTDBCombo As TDBCombo, iReposition As Integer)
'resets the tdb combo value to the previous match -
'this procedure should be called in the
'TDBCombo Mismatch even procedure only
    Dim bytStart As Byte
    
    iReposition = False
    
    With oTDBCombo
'       get current selection start
        bytStart = .SelStart
        
'       reset the text to the current list text
        If .ListField = Empty Then
            .Text = .Columns(0)
        Else
            .Text = .Columns(.ListField)
        End If
        
'       return selection to original selection
        .SelStart = Max(CDbl(bytStart - 1), 0)
        .SelLength = Len(.Text)
    End With
End Sub

Function Max(i As Double, j As Double) As Double
    If i > j Then
        Max = i
    Else
        Max = j
    End If
End Function

Function bHasHotKey(xString As String) As Boolean
    If xString <> "" Then
        bHasHotKey = CBool(InStr(xString, "&"))
    End If
End Function

Function Min(i As Double, j As Double) As Double
    If i > j Then
        Min = j
    Else
        Min = i
    End If
End Function

Public Function bValidateBoundTDBCombo(oTDBCombo As TDBCombo) As Boolean
'returns FALSE if invalid author has been specified, else TRUE
    With oTDBCombo
        If .Text = "" Then
            MsgBox "Invalid entry.  Please select an item " & _
                   "from the list.", vbExclamation, g_oSessionType.AppTitle
        Else
'           set text of control equal to the
'           display text of the selected row
            .Text = .Columns(.ListField)
            bValidateBoundTDBCombo = True
        End If
    End With
End Function

Public Function ListingBackend(oListing As CIO.CListing) As CIO.ICIBackend
'returns the backend of the listing
    On Error GoTo ProcError
    Set ListingBackend = Backends.ItemFromID(oListing.BackendID)
    Exit Function
ProcError:
    g_oError.RaiseError "CIX.CGroups.ListingBackend"
    Exit Function
End Function

Public Sub QuitSession()
    Set g_oError = Nothing
    Set g_oIni = Nothing
    Set m_oBackends = Nothing
    Set g_oConstants = Nothing
    Set g_oSessionType = Nothing
    Set g_oBackend = Nothing
'    Set g_oGlobals = Nothing
End Sub

Public Function xLocalizeNumericString(ByVal xString As String) As String
'replaces decimal separator with the current one set for the OS
    Const mpThisFunction As String = "CIX.mdlMain.xLocalizeNumericString"
    Dim xTemp As String
    Dim xDecimal As String
    
    On Error GoTo ProcError
    
    xTemp = xString
    
    If xTemp = Empty Then
        Exit Function
    End If
    
    'get whether decimal sep is a comma or period
    xDecimal = Mid(Format(0, "#,##0.00"), 2, 1)
    
    If xDecimal = "." Then
        'localize to period
        xLocalizeNumericString = Replace(xTemp, ",", ".")
    Else
        'localize to comma
        xLocalizeNumericString = Replace(xTemp, ".", ",")
    End If
    Exit Function
ProcError:
    g_oError.RaiseError mpThisFunction
    Exit Function
End Function


Public Function xarStringToxArrayDB(ByRef xString() As Variant) As XArrayDB

    Dim xARP As XArrayDB
    
    Set xARP = New XArrayDB
    
    xARP.LoadRows xString(), False
    
    Set xarStringToxArrayDB = xARP
End Function

Sub MoveToLastPosition(oForm As Object, _
                       sT As Single, _
                       sL As Single)
'positions dialog to last set position-
'ensures dlg is on the screen
    Dim lXPix As Long
    Dim lYPix As Long
    Dim sW As Single
    Dim sH As Single
    
    
'   move dialog to last position
    On Error Resume Next
    
    With oForm
'       get screen size in pixels
        lYPix = Screen.Height
        lXPix = Screen.Width
    
'       get form size
        sW = .Width
        sH = .Height
        
        If sT + sL <> 0 Then
'           place form - ensure that entire form is on screen
            .Left = Min(CDbl(sL), lXPix - sW)
            .Top = Min(CDbl(sT), lYPix - sH)
        Else
'           place form - ensure that entire form is on screen
            .Left = (lXPix / 2) - (sW / 2)
            .Top = (lYPix / 2) - (sH / 2)
        End If
        
    End With
    
End Sub
Public Sub DebugPrint(xOutput As String)
    Debug.Print xOutput
    OutputDebugString xOutput
End Sub

Public Sub LaunchDocumentByExtension(xFileName As String)
    Dim iRet As Integer
    
    On Error GoTo ProcError
    
    iRet = ShellExecute(0, "open", xFileName, "", "", SW_SHOWNORMAL)
    If iRet <= 32 Then
        MsgBox "The document could not be opened.  " & _
            "There may not be an installed application associated with this extension.", vbExclamation
    End If
    Exit Sub
ProcError:
    g_oError.RaiseError "CIX.mdlMain.LaunchDocumentByExtension"
End Sub

'temporary
Public Function bIsQuickFilterBackend(iInternalID As Integer) As Boolean
    If g_iDefaultFilter = 1 Then    'force text filtering
        bIsQuickFilterBackend = False
    Else
        Select Case iInternalID
            Case 100, 104, 111      'Outlook OM, MP9, MP10
                bIsQuickFilterBackend = True
            Case Else
                bIsQuickFilterBackend = False
        End Select
    End If
End Function

'Returns the version of Windows that the user is running
Public Function GetWindowsVersion() As String
    Dim osv As OSVERSIONINFO
    
    osv.OSVSize = Len(osv)
    
    GetVersionEx osv
    
    GetWindowsVersion = osv.dwVerMajor & "." & osv.dwVerMinor
    
End Function
