VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CBackends"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'**********************************************************
'   ICIBackends CollectionClass
'   created 12/19/00 by Daniel Fisherman
'   Contains properties and methods
'   that manage a collection of ci backends
'**********************************************************
Option Explicit

Private m_oCol As Collection
Private m_oGroups As CIO.ICIBackend

'**********************************************************
'initializer/terminator
'**********************************************************
Dim xObjectID As String
Private Sub Class_Initialize()
    Math.Randomize
    xObjectID = "CBackends" & Format(Now, "hhmmss") & Math.Rnd()
    g_oGlobals.Objects.Add xObjectID, xObjectID
    DebugPrint "Added - " & xObjectID & ": " & g_oGlobals.Objects.Count
    Set m_oCol = New Collection
End Sub

Private Sub Class_Terminate()
    g_oGlobals.Objects.Remove xObjectID
    DebugPrint "Removed - " & xObjectID & ": " & g_oGlobals.Objects.Count
    Set m_oCol = Nothing
    Set m_oGroups = Nothing
End Sub
'**********************************************************

'******************************************************************
'Methods
'******************************************************************
Public Function ProgID(ByVal iBackendID As Integer) As String
    Dim xProgID As String
    Dim xDesc As String
    
    On Error GoTo ProcError
    
    'get from ini
    xProgID = g_oIni.GetIni( _
        "Backend" & iBackendID, "ProgID", g_oIni.CIIni)
    
    'raise error if ini value is missing
    If Len(xProgID) = 0 Then
        xDesc = "Invalid Backend" & iBackendID & "\ProgID key in ci.ini."
        Err.Raise ciErr_MissingOrInvalidINIKey, , xDesc
        Exit Function
    End If
    
    ProgID = xProgID
    Exit Function
ProcError:
    g_oError.RaiseError "CIX.CBackends.ProgID"
End Function

Friend Function Add(ByVal iID As Integer) As CIO.ICIBackend
'creates a new backend with id = iID, then
'adds it to the collection.
'returns the new backend
    Dim xProgID As String
    Dim xDesc As String
    Dim oB As CIO.ICIBackend
    Dim xPrompt As String
    
    On Error GoTo ProcError
    
    'get ini required ini values
    xProgID = Me.ProgID(iID)
    
    'create backend object
    On Error Resume Next
    Set oB = CreateObject(xProgID)
    On Error GoTo ProcError
    
    xPrompt = g_oIni.GetIni( _
                    "CIApplication", "PromptForNonExistingBackend", g_oIni.CIIni)

    If oB Is Nothing Then
    	'GLOG : 5502 : ceh
        If UCase(xPrompt) = "TRUE" Then
            Err.Raise ciErrs.ciErr_CouldNotAddToCollection, , _
                "Could not add backend " & iID & ".  " & _
                "Could not create object with ProgID '" & xProgID & "'."
        End If
        Exit Function
    End If
    
    If xProgID = "CIX.CGroups" Then
        'tag this as the "Groups" backend
        Set m_oGroups = oB
    End If
    
    'initialize
    oB.Initialize iID
    
    'add to collection
    On Error Resume Next
    Err.Clear
    
    'GLOG 2 : 5699 : CEH
    If oB.Exists Then
        m_oCol.Add oB, CStr(oB.InternalID)
    Else
        If UCase(xPrompt) = "TRUE" Then
            MsgBox "You have not been given access to '" & oB.Name & "' for MacPac Contact Integration." & _
                    "  Please contact your MacPac administrator if you have any questions.", _
                    vbInformation, g_oSessionType.AppTitle
        End If
        Set oB = Nothing
        Exit Function
    End If
    
    If Err Then
        'could not add to collection - alert and exit
        On Error GoTo ProcError
        xDesc = "Could not add backend " & iID & "to collection of backends. " & _
            Err.Description
        Err.Raise Err.Number, , xDesc
        Exit Function
    Else
        On Error GoTo ProcError
        Set Add = oB
    End If
    
    Exit Function
ProcError:
    g_oError.RaiseError "CIO.ICIBackends.Add"
End Function

Public Function ItemFromID(ByVal iID As Integer) As CIO.ICIBackend
'will return Nothing if an error is generated
    On Error Resume Next
    Set ItemFromID = m_oCol.Item(CStr(iID))
End Function

Public Function ItemFromIndex(ByVal iIndex As Integer) As CIO.ICIBackend
'will return Nothing if an error is generated
    On Error Resume Next
    Set ItemFromIndex = m_oCol.Item(iIndex)
End Function

Public Function GroupsBackend() As CIO.ICIBackend
'returns the "Groups" backend
    Set GroupsBackend = m_oGroups
End Function

Public Function Count() As Integer
    Count = m_oCol.Count
End Function

Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
Attribute NewEnum.VB_MemberFlags = "40"
    Set NewEnum = m_oCol.[_NewEnum]
End Property
