VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CSession"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
'**********************************************************
'   CSession Class
'   created 01/06/01 by Daniel Fisherman
'   Contains properties and methods defining a
'   CI session
'**********************************************************
Option Explicit

Private m_oForm As CIX.frmCI
Private m_bInitialized As Boolean
Private m_sFormLeft As Single
Private m_sFormTop As Single
Private m_xLabelTo As String
Private m_xLabelFrom As String
Private m_xLabelCC As String
Private m_xLabelBCC As String
Private m_xFormCaption As String

'**********************************************************
'initializer/terminator
'**********************************************************
Dim xObjectID As String
Private Sub Class_Initialize()
    Dim oApp As Object
    
    Math.Randomize
    xObjectID = "CSession" & Format(Now, "hhmmss") & Math.Rnd()
    g_oGlobals.Objects.Add xObjectID, xObjectID
    DebugPrint "Added - " & xObjectID & ": " & g_oGlobals.Objects.Count
    
    On Error GoTo ProcError
    GetBackends
    
    'set Windows version
    g_bIsWindows8 = (Val(GetWindowsVersion()) >= 6.2)
    
    'set Word version if 2013 or newer
    On Error Resume Next
    Set oApp = GetObject(, "Word.Application")
    On Error GoTo ProcError
    If Not (oApp Is Nothing) Then
        g_bIsWord15 = (Val(oApp.Version) >= 15)
    End If
    
    m_bInitialized = True
    
    Exit Sub
ProcError:
    g_oError.RaiseError "CIX.CSession.Class_Initialize"
    Exit Sub
End Sub

Private Sub Class_Terminate()
    If Not m_oForm Is Nothing Then
        Unload m_oForm
        Set m_oForm = Nothing
    End If
    mdlMain.QuitSession
    g_oGlobals.Objects.Remove xObjectID
    DebugPrint "Removed - " & xObjectID & ": " & g_oGlobals.Objects.Count
    Dim i As Integer
    For i = 1 To g_oGlobals.Objects.Count
        If i = 1 Then
            DebugPrint "Objects remaining:"
        End If
        DebugPrint g_oGlobals.Objects.Item(i)
    Next i
    Set g_oGlobals = Nothing
End Sub
'**********************************************************

Public Property Get Initialized() As Boolean
    Initialized = m_bInitialized
End Property

Public Property Get LabelTo() As String
    LabelTo = m_xLabelTo
End Property

Public Property Let LabelTo(xNew As String)
    m_xLabelTo = xNew
End Property

Public Property Get LabelFrom() As String
    LabelFrom = m_xLabelFrom
End Property

Public Property Let LabelFrom(xNew As String)
    m_xLabelFrom = xNew
End Property

Public Property Get LabelCC() As String
    LabelCC = m_xLabelCC
End Property

Public Property Let LabelCC(xNew As String)
    m_xLabelCC = xNew
End Property

Public Property Get LabelBCC() As String
    LabelBCC = m_xLabelBCC
End Property

Public Property Let LabelBCC(xNew As String)
    m_xLabelBCC = xNew
End Property

Public Property Get FormLeft() As Single
    FormLeft = m_sFormLeft
End Property

Public Property Let FormLeft(sNew As Single)
    m_sFormLeft = sNew
End Property

Public Property Get FormTop() As Single
    FormTop = m_sFormTop
End Property

Public Property Let FormTop(sNew As Single)
    m_sFormTop = sNew
End Property

Public Property Get FormCaption() As String
    FormCaption = m_xFormCaption
End Property

Public Property Let FormCaption(xNew As String)
    m_xFormCaption = xNew
End Property

Public Property Get SessionType() As ciSessionType
    SessionType = g_oSessionType.SessionType
End Property

Public Property Let SessionType(iNew As ciSessionType)
    g_oSessionType.SessionType = iNew
End Property

Public Function GetSummary(oContact As CIO.CContact, Optional ByVal xFormatTokenPhrase As String) As String
'returns a default summary of the supplied contact
    On Error GoTo ProcError
    GetSummary = mdlMain.GetSummary(oContact, xFormatTokenPhrase)
    Exit Function
ProcError:
    g_oError.RaiseError "CIX.CSession.GetSummary"
    Exit Function
End Function

Public Sub ShowContactDetail(ByVal xContactUNID As String)
'displays the detail of the contact with the specified UNID
    Dim oUNID As CIO.CUNID
    Dim oForm As frmListingDetail
    Dim oContact As CIO.CContact
    Dim xName As String
    
    On Error GoTo ProcError
    Set oUNID = New CIO.CUNID
    Set oForm = New frmListingDetail
    
    Set oContact = Me.GetContactFromUNID(xContactUNID, ciRetrieveData_Names, ciAlert_None)
    If Not oContact Is Nothing Then
        xName = oContact.DisplayName
    End If
    
    On Error Resume Next
    oForm.Listing = oUNID.GetListing(xContactUNID, xName)
    oForm.AddressIndex = 0
    oForm.Show vbModal
    
    Set oForm = Nothing
    Exit Sub
ProcError:
    g_oError.ShowError
    Exit Sub
End Sub

Public Function Backends() As CIX.CBackends
    On Error GoTo ProcError
    Set Backends = mdlMain.Backends
    Exit Function
ProcError:
    g_oError.RaiseError "CSession.Backends"
End Function

Public Function GetContactFromUNID(ByVal xUNID As String, _
    ByVal iIncludeData As ciRetrieveData, ByVal iAlerts As ciAlerts) As CIO.CContact
'returns the contact having the specified UNID
    Dim oB As CIO.ICIBackend
    Dim oUNID As CUNID
    
    On Error GoTo ProcError
    Set oUNID = New CUNID
    Set oB = mdlMain.GetBackendFromUNID(xUNID)
    
    If Not oB Is Nothing Then
        Dim oCs As CIO.CContacts
        
        Set oCs = oB.GetContacts(oUNID.GetListing(xUNID), _
            oUNID.GetAddress(xUNID), , iIncludeData, iAlerts)
            
        If Not (oCs Is Nothing) Then
            If oCs.Count > 0 Then
                Set GetContactFromUNID = oCs.Item(1)
            Else
                Err.Raise ciErrs.ciErr_InvalidUNID, , _
                    "No contact with UNID '" & xUNID & "' was found."
            End If
        ElseIf iAlerts = ciAlert_None Then
            Err.Raise ciErrs.ciErr_InvalidUNID, , _
                "No contact with UNID '" & xUNID & "' was found."
        End If
    Else
        'no backend with specified ID exists - alert
        Dim xID As String
        xID = oUNID.GetUNIDField(xUNID, ciUNIDFields_Backend)
        Err.Raise ciErrs.ciErr_InvalidUNID, , _
            "No Backend found with ID '" & xID & "'"
    End If
    Exit Function
ProcError:
    g_oError.RaiseError "CIX.CSession.GetContact"
    Exit Function
End Function

Public Function GetFilter(ByVal iBackendID As Integer) As CIO.CFilter
'returns the collection of filter fields for specified backend
    On Error GoTo ProcError
    Set GetFilter = mdlMain.GetFilter(iBackendID)
    Exit Function
ProcError:
    g_oError.RaiseError "CSession.GetFilter"
End Function

Public Sub CreateDataFile(ByVal xFile As String)
'creates a merge data file from selected contacts
    Const ciDataFileHeader As String = "AddressType|ContactType|DisplayName|Prefix|FirstName|MiddleName|LastName|Suffix|Initials|Street1|Street2|Street3|City|State|Zip|Country|Company|Department|Title|EMail|Phone|PhoneExt|Fax"
    Dim oContacts As CIO.CContacts
    
    On Error GoTo ProcError
    
    Set oContacts = GetContacts(ciRetrieveData_All, ciRetrieveData_None, _
        ciRetrieveData_None, ciRetrieveData_None, , , ciAlert_All)
        
    oContacts.CreateDataFile xFile
    Exit Sub
ProcError:
    g_oError.RaiseError "CIX.CSession.CreateDataFile"
    Exit Sub
End Sub

Public Sub CreateDataFileOLD(ByVal xFile As String)
'creates a merge data file from selected contacts
    Const ciDataFileHeader As String = "AddressType|ContactType|DisplayName|Prefix|FirstName|MiddleName|LastName|Suffix|Initials|Street1|Street2|Street3|City|State|Zip|Country|Company|Department|Title|EMail|Phone|PhoneExt|Fax"
    Dim iFile As Integer
    Dim xHeader As String
    Dim oCustFlds As CCustomFields
    Dim i As Integer
    Dim j As Integer
    Dim xRecord As String
    Dim oContacts As CIO.CContacts
    
    On Error GoTo ProcError
    
    Set oContacts = GetContacts(ciRetrieveData_All, ciRetrieveData_None, _
        ciRetrieveData_None, ciRetrieveData_None, , , ciAlert_All)
        
    If oContacts Is Nothing Then
        Exit Sub
    End If
    
    'get file channel
    iFile = FreeFile()
    
    'create file
    Open xFile For Output As #iFile
    
    'create header
    xHeader = ciDataFileHeader
    
    'get custom fields - we can use the first backend since each backend has the
    'same custom fields (although some fields may not be linked to a data field)
    Set oCustFlds = oContacts.Item(1).CustomFields
    For j = 1 To oCustFlds.Count
        xHeader = xHeader & "|" & oCustFlds(j).Name
    Next j
        
    Print #iFile, xHeader
    
    'cycle through contacts, writing to file
    For i = 1 To oContacts.Count
        With oContacts.Item(i)
            xRecord = """" & .AddressTypeName & """" & _
                        "|" & .ContactType & _
                        "|""" & .DisplayName & """" & _
                        "|""" & .Prefix & """" & _
                        "|""" & .FirstName & """" & _
                        "|""" & .MiddleName & """" & _
                        "|""" & .LastName & """" & _
                        "|""" & .Suffix & """" & _
                        "|""" & .Initials & """" & _
                        "|""" & .Street1 & """" & _
                        "|""" & .Street2 & """" & _
                        "|""" & .Street3 & """" & _
                        "|""" & .City & """" & _
                        "|""" & .State & """" & _
                        "|""" & .ZipCode & """" & _
                        "|""" & .Country & """" & _
                        "|""" & .Company & """" & _
                        "|""" & .Department & """" & _
                        "|""" & .Title & """" & _
                        "|""" & .EMailAddress & """" & _
                        "|""" & .Phone & """" & _
                        "|""" & .PhoneExtension & """" & _
                        "|""" & .Fax & """"
            
            For j = 1 To .CustomFields.Count
                xRecord = xRecord & "|""" & .CustomFields(j).Value & """"
            Next j
            
            xRecord = Replace(xRecord, vbCrLf, "<1310>")
            xRecord = Replace(xRecord, vbCr, "<13>")
            xRecord = Replace(xRecord, Chr$(11), "<11>")
            
            Print #iFile, xRecord
        End With
    Next i
    
    'close file
    Close #iFile
    Exit Sub
ProcError:
    g_oError.RaiseError "CIX.CSession.CreateDataFile"
    Exit Sub
End Sub


'v2.4.4007
Public Function GetContactsEx(Optional ByVal RetrieveDataForTo As ciRetrieveData = ciRetrieveData_All, _
    Optional ByVal RetrieveDataForFrom As ciRetrieveData = ciRetrieveData_All, _
    Optional ByVal RetrieveDataForCC As ciRetrieveData = ciRetrieveData_All, _
    Optional ByVal RetrieveDataForBCC As ciRetrieveData = ciRetrieveData_All, _
    Optional ByVal DefaultSelectionList As ciSelectionLists = CIX.ciSelectionList_To, _
    Optional ByVal MaxContacts As Integer = 0, _
    Optional ByVal Alerts As ciAlerts = ciAlert_All, _
    Optional ByVal xLabelTo As String, _
    Optional ByVal xLabelFrom As String, _
    Optional ByVal xLabelCC As String, _
    Optional ByVal xLabelBCC As String, _
    Optional ByVal lMaxTo As Long, _
    Optional ByVal lMaxFrom As Long, _
    Optional ByVal lMaxCC As Long, _
    Optional ByVal lMaxBCC As Long) As CIO.CContacts

    On Error GoTo ProcError
    
    g_oSessionType.SessionType = ciSession_CI
    
    If m_oForm Is Nothing Then
        Set m_oForm = New frmCI
    End If
    
    Dim oContacts As CIO.CContacts
    With m_oForm
        If g_bIsWord15 Then
            .Width = 8800
        Else
            .Width = 8700
        End If
'        MoveToLastPosition m_oForm, Me.FormTop, Me.FormLeft
        With .CI1
            .Width = 8625
            .ClearContacts
            .MaxContacts = MaxContacts
'v2.4.4001
'            .LabelTo = IIf(Me.LabelTo <> "", Me.LabelTo, .LabelTo)
'            .LabelFrom = IIf(Me.LabelFrom <> "", Me.LabelFrom, .LabelFrom)
'            .LabelCC = IIf(Me.LabelCC <> "", Me.LabelCC, .LabelCC)
'            .LabelBCC = IIf(Me.LabelBCC <> "", Me.LabelBCC, .LabelBCC)
            .LabelTo = IIf(xLabelTo <> "", IIf(bHasHotKey(xLabelTo), xLabelTo, "&" & xLabelTo), .LabelTo)
            .LabelFrom = IIf(xLabelFrom <> "", IIf(bHasHotKey(xLabelFrom), xLabelFrom, "&" & xLabelFrom), .LabelFrom)
            .LabelCC = IIf(xLabelCC <> "", IIf(bHasHotKey(xLabelCC), xLabelCC, "&" & xLabelCC), .LabelCC)
            .LabelBCC = IIf(xLabelBCC <> "", IIf(bHasHotKey(xLabelBCC), xLabelBCC, "&" & xLabelBCC), .LabelBCC)
            
'v2.4.007
            .MaxTo = lMaxTo
            .MaxFrom = lMaxFrom
            .MaxCC = lMaxCC
            .MaxBCC = lMaxBCC
            g_iDefaultMaxContacts = MaxContacts
            
            Dim iSelLists As CIX.ciSelectionLists
            
            If RetrieveDataForTo <> ciRetrieveData_None Then
                iSelLists = iSelLists + ciSelectionList_To
            End If
            
            If RetrieveDataForFrom <> ciRetrieveData_None Then
                iSelLists = iSelLists + ciSelectionList_From
            End If
            
            If RetrieveDataForCC <> ciRetrieveData_None Then
                iSelLists = iSelLists + ciSelectionList_CC
            End If
            
            If RetrieveDataForBCC <> ciRetrieveData_None Then
                iSelLists = iSelLists + ciSelectionList_BCC
            End If
            
            .SelectionLists = iSelLists
            .SelectionList = DefaultSelectionList
            .DataRetrievedForTo = RetrieveDataForTo
            .DataRetrievedForFrom = RetrieveDataForFrom
            .DataRetrievedForCC = RetrieveDataForCC
            .DataRetrievedForBCC = RetrieveDataForBCC
            .Alerts = Alerts
            
        End With
        .Enabled = True
'        .Caption = " " & g_oSessionType.AppTitle
        .Show vbModal
        If Not .Cancelled Then
            Set oContacts = .CI1.SelectedContacts
            Me.FormLeft = .Left
            Me.FormTop = .Top
            Set GetContactsEx = oContacts
            Set oContacts = Nothing
        Else
            Set oContacts = New CIO.CContacts
            Set GetContactsEx = oContacts
            Set oContacts = Nothing
        End If
    End With
    Exit Function
ProcError:
    g_oError.RaiseError "CIX.CSession.GetContactsEx"
    Exit Function
End Function


Public Function GetContacts(Optional ByVal RetrieveDataForTo As ciRetrieveData = ciRetrieveData_All, _
    Optional ByVal RetrieveDataForFrom As ciRetrieveData = ciRetrieveData_All, _
    Optional ByVal RetrieveDataForCC As ciRetrieveData = ciRetrieveData_All, _
    Optional ByVal RetrieveDataForBCC As ciRetrieveData = ciRetrieveData_All, _
    Optional ByVal DefaultSelectionList As ciSelectionLists = CIX.ciSelectionList_To, _
    Optional ByVal MaxContacts As Integer = 0, _
    Optional ByVal Alerts As ciAlerts = ciAlert_All) As CIO.CContacts

    On Error GoTo ProcError
    
    g_oSessionType.SessionType = ciSession_CI
    
    If m_oForm Is Nothing Then
        Set m_oForm = New frmCI
    End If
    
    Dim oContacts As CIO.CContacts
    With m_oForm
        If g_bIsWord15 Then
            .Width = 8775
        Else
            .Width = 8675
        End If
'        MoveToLastPosition m_oForm, Me.FormTop, Me.FormLeft
        With .CI1
            .Width = 8625
            
            .ClearContacts
            .MaxContacts = MaxContacts
            
            g_iDefaultMaxContacts = MaxContacts

            .LabelTo = IIf(Me.LabelTo <> "", Me.LabelTo, .LabelTo)
            .LabelFrom = IIf(Me.LabelFrom <> "", Me.LabelFrom, .LabelFrom)
            .LabelCC = IIf(Me.LabelCC <> "", Me.LabelCC, .LabelCC)
            .LabelBCC = IIf(Me.LabelBCC <> "", Me.LabelBCC, .LabelBCC)
            
            Dim iSelLists As CIX.ciSelectionLists
            
            If RetrieveDataForTo <> ciRetrieveData_None Then
                iSelLists = iSelLists + ciSelectionList_To
            End If
            
            If RetrieveDataForFrom <> ciRetrieveData_None Then
                iSelLists = iSelLists + ciSelectionList_From
            End If
            
            If RetrieveDataForCC <> ciRetrieveData_None Then
                iSelLists = iSelLists + ciSelectionList_CC
            End If
            
            If RetrieveDataForBCC <> ciRetrieveData_None Then
                iSelLists = iSelLists + ciSelectionList_BCC
            End If
            
            .SelectionLists = iSelLists
            .SelectionList = DefaultSelectionList
            .DataRetrievedForTo = RetrieveDataForTo
            .DataRetrievedForFrom = RetrieveDataForFrom
            .DataRetrievedForCC = RetrieveDataForCC
            .DataRetrievedForBCC = RetrieveDataForBCC
            .Alerts = Alerts
            
        End With
        .Enabled = True
'        .Caption = " " & g_oSessionType.AppTitle
        .Show vbModal
        If Not .Cancelled Then
            Set oContacts = .CI1.SelectedContacts
            Me.FormLeft = .Left
            Me.FormTop = .Top
            Set GetContacts = oContacts
            Set oContacts = Nothing
        Else
            Set oContacts = New CIO.CContacts
            Set GetContacts = oContacts
            Set oContacts = Nothing
        End If
    End With
    Exit Function
ProcError:
    g_oError.RaiseError "CIX.CSession.GetContacts"
    Exit Function
End Function


Public Function GetContacts_Custom(Optional ByVal RetrieveDataForTo As ciRetrieveData = ciRetrieveData_All, _
    Optional ByVal RetrieveDataForFrom As ciRetrieveData = ciRetrieveData_All, _
    Optional ByVal RetrieveDataForCC As ciRetrieveData = ciRetrieveData_All, _
    Optional ByVal RetrieveDataForBCC As ciRetrieveData = ciRetrieveData_All, _
    Optional ByVal DefaultSelectionList As ciSelectionLists = CIX.ciSelectionList_To, _
    Optional ByVal MaxContacts As Integer = 0, _
    Optional ByVal Alerts As ciAlerts = ciAlert_All, _
    Optional ByVal oArray As XArrayDB) As CIO.CContacts

    Dim iSelLists As CIX.ciSelectionLists
    Dim i As Integer
    Dim j As Integer
    Dim oC As CIO.CContact
    
    On Error GoTo ProcError
    
    If m_oForm Is Nothing Then
        Set m_oForm = New frmCI
    End If
    
    With m_oForm
        If g_bIsWord15 Then
            .Width = 9615
        Else
            .Width = 9510
        End If
'        MoveToLastPosition m_oForm, Me.FormTop, Me.FormLeft
        With .CI1
            .Width = 9480
            .ClearContacts
            .MaxContacts = MaxContacts
            
            g_iDefaultMaxContacts = MaxContacts

            
            If Not (oArray Is Nothing) Then
                
                For i = 0 To oArray.UpperBound(1)
'                   check for UNID value
                    If oArray.Value(i, 4) <> Empty Then
'                       add as reuse contact

                        On Error Resume Next
                        .SelectedContacts.Add GetContactFromUNID(oArray.Value(i, 4), oArray.Value(i, 1), ciAlert_None)
                        If Err.Number Then
                            Err.Clear
                            GoTo skip
                        End If
                        
                        On Error GoTo ProcError
                        Set oC = .SelectedContacts(.SelectedContacts.Count)

                        With oC
                            .Tag = i + 1
                            .CustomTypeName = oArray.Value(i, 0)
                        End With
                    End If
skip:
                Next i
                                
                .EntityList = oArray
                .SelectionLists = ciSelectionList_Custom
                .Alerts = Alerts
            Else
                
                If RetrieveDataForTo <> ciRetrieveData_None Then
                    iSelLists = iSelLists + ciSelectionList_To
                End If
                
                If RetrieveDataForFrom <> ciRetrieveData_None Then
                    iSelLists = iSelLists + ciSelectionList_From
                End If
                
                If RetrieveDataForCC <> ciRetrieveData_None Then
                    iSelLists = iSelLists + ciSelectionList_CC
                End If
                
                If RetrieveDataForBCC <> ciRetrieveData_None Then
                    iSelLists = iSelLists + ciSelectionList_BCC
                End If
                
                .SelectionLists = iSelLists
                .SelectionList = DefaultSelectionList
                .DataRetrievedForTo = RetrieveDataForTo
                .DataRetrievedForFrom = RetrieveDataForFrom
                .DataRetrievedForCC = RetrieveDataForCC
                .DataRetrievedForBCC = RetrieveDataForBCC
                .Alerts = Alerts
                
            End If
        End With
        .Caption = " " & g_oSessionType.AppTitle
'        '#2859 v2.4.3003
'        If UCase(g_oIni.GetIni("CIApplication", "UseCIConnectDialogTitle")) = "FALSE" Then
'            .Caption = " "
'        Else
'            .Caption = " Connect Forms"
'        End If
        .Show vbModal
        If Not .Cancelled Then
            Set GetContacts_Custom = .CI1.SelectedContacts
            Me.FormTop = .Top
            Me.FormLeft = .Left
        End If
    End With
    Exit Function
ProcError:
    g_oError.RaiseError "CIX.CSession.GetContacts_Custom"
    Exit Function
End Function


Public Function GetContactsFromArray(ByRef oArray() As Variant, _
                                     Optional xSep As String = "|") As CIO.CContacts
    Dim oxArrayDB As XArrayDB
    
        
'   turn oarray into xArrayDB
    Set oxArrayDB = xarStringToxArrayDB(oArray())
    
'   call custom function
    Set GetContactsFromArray = GetContacts_Custom(oArray:=oxArrayDB)
    
    Exit Function
ProcError:
    g_oError.RaiseError "CIX.CSession.GetContactsFromArray"
    Exit Function
End Function
