VERSION 5.00
Object = "{F54E2CE0-97F9-11D6-8145-00A0CC58115F}#1.0#0"; "CIX.ocx"
Begin VB.Form frmCI 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   " Contact Integration"
   ClientHeight    =   6600
   ClientLeft      =   660
   ClientTop       =   735
   ClientWidth     =   8205
   BeginProperty Font 
      Name            =   "MS Sans Serif"
      Size            =   8.25
      Charset         =   0
      Weight          =   400
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "frmCI.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6600
   ScaleWidth      =   8205
   StartUpPosition =   1  'CenterOwner
   Begin CIX.CI CI1 
      Height          =   6480
      Left            =   -15
      TabIndex        =   0
      Top             =   -45
      Width           =   8265
      _ExtentX        =   14579
      _ExtentY        =   11430
   End
End
Attribute VB_Name = "frmCI"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub CI1_InsertionCancelled()
    Unload Me
End Sub

Private Sub CI1_InsertionOK(oContacts As CIO.CContacts)
    Dim oContact As CIO.CContact
    Dim xFormat As String
    
    xFormat = "IF(<FirstName> = empty, It's empty, Not Empty)"
    For Each oContact In oContacts
        Debug.Print oContact.GetDetail(xFormat)
    Next oContact
    Unload Me
End Sub

Private Sub Form_Load()
    Me.CI1.SelectionList = ciSelectionList_CC
End Sub
