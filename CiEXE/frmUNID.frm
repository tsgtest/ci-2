VERSION 5.00
Begin VB.Form frmUNID 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Get Contact From UNID"
   ClientHeight    =   1425
   ClientLeft      =   2760
   ClientTop       =   3750
   ClientWidth     =   6030
   Icon            =   "frmUNID.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1425
   ScaleWidth      =   6030
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.TextBox Text1 
      BorderStyle     =   0  'None
      Height          =   300
      Left            =   135
      TabIndex        =   1
      Top             =   375
      Width           =   5730
   End
   Begin VB.CommandButton CancelButton 
      Caption         =   "Cancel"
      Height          =   375
      Left            =   4650
      TabIndex        =   3
      Top             =   900
      Width           =   1215
   End
   Begin VB.CommandButton OKButton 
      Caption         =   "OK"
      Height          =   375
      Left            =   3330
      TabIndex        =   2
      Top             =   900
      Width           =   1215
   End
   Begin VB.Label Label1 
      Caption         =   "&Enter UNID:"
      Height          =   270
      Left            =   165
      TabIndex        =   0
      Top             =   135
      Width           =   2325
   End
End
Attribute VB_Name = "frmUNID"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Option Explicit

Private Sub CancelButton_Click()
    Me.Hide
End Sub

Private Sub OKButton_Click()
    Me.Hide
End Sub
