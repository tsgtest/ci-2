// Mapi.cpp : implementation file
// 
// Written by Christopher W. Backen - VIZ Group, 1998
// 
// Simple MAPI Class to help someone Add/Delete messaging profiles,
// configure messaging services, add/delete messaging services,
// configure messaging services, and set default messaging stores.
//
// For Service Names, please consult the MAPISVC.INF file located
// in \winnt\system32
// 
// Tested on Windows NT 4.0 and Visual C++ 6.0

#include "stdafx.h"
#include "mapidefs.h"
#include "edkmdb.h"
#include "Mapi.h"
#include <stdio.h>
#include <mapix.h>
#include <MAPITAGS.H>
#include <cstring>
#include <string>
#include <iostream>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMapi

CMapi::CMapi()
{
}

CMapi::~CMapi()
{
}


BEGIN_MESSAGE_MAP(CMapi, CWnd)
	//{{AFX_MSG_MAP(CMapi)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CMapi message handlers

HRESULT CMapi::Initialize()
{
	try
	{
		hr = MAPIInitialize(NULL);
		
	}
	catch (...)
	{
		return hr;
	}

	return hr;
}

HRESULT CMapi::AdminProfiles()
{
	try
	{
		hr = MAPIAdminProfiles(0, &pProfAdmin);
		
	}
	catch (...)
	{
		return hr;
	}

	return hr;
}

HRESULT CMapi::CreateNewProfile(LPSTR szProfile)
{
	try
	{
		hr = pProfAdmin->CreateProfile(LPWSTR(szProfile), NULL, 0, 0);
		
	}
	catch (...)
	{
		return hr;
	}

	return hr;
}

HRESULT CMapi::LogonDefaultEx()
{
	try
	{
		hr = MAPILogonEx(0, NULL, NULL, MAPI_NO_MAIL | MAPI_USE_DEFAULT | MAPI_LOGON_UI,
			&pSession);
		
	}
	catch (...)
	{
		return hr;
	}

	return hr;
}

HRESULT CMapi::LogonNewEx()
{
	try
	{
		hr = MAPILogonEx(0, NULL, NULL, MAPI_NO_MAIL | MAPI_NEW_SESSION,
                           &pSession);
		
	}
	catch (...)
	{
		return hr;
	}

	return hr;
}

HRESULT CMapi::OpenAdminServices()
{
	try
	{
		hr = pSession->AdminServices(0, &pSvcAdmin);
		
	}
	catch (...)
	{
		return hr;
	}

	return hr;
}

HRESULT CMapi::CreateMessagingService(LPTSTR lpszService, LPTSTR lpszDisplayName)
{
	try
	{
		hr = pSvcAdmin->CreateMsgService(lpszService, lpszDisplayName, 0, 0);
		
	}
	catch (...)
	{
		return hr;
	}

	return hr;
}

HRESULT CMapi::ConfigureMessagingService(LPTSTR lpszService, LPTSTR lpszExchangeNameFL, LPTSTR lpszServer)
{
	enum {iSvcName, iSvcUID, cptaSvc};
	SPropValue		rgval[2];

    SizedSPropTagArray (cptaSvc, ptaSvc) = { cptaSvc, 
                                            {   PR_SERVICE_NAME,
                                                PR_SERVICE_UID } };

	try
	{
		hr = pSvcAdmin->GetMsgServiceTable(0, &pMsgSvcTable);
		if (FAILED(hr))
		{
			return hr;
		}

		sres.rt = RES_CONTENT;
		sres.res.resContent.ulFuzzyLevel = FL_FULLSTRING;
		sres.res.resContent.ulPropTag = PR_SERVICE_NAME;
		sres.res.resContent.lpProp = &pSvcProps;
	
		pSvcProps.ulPropTag = PR_SERVICE_NAME;
		pSvcProps.Value.lpszA = reinterpret_cast<char*>(lpszService); //"MSEMS"

		hr = HrQueryAllRows(pMsgSvcTable, (LPSPropTagArray)&ptaSvc, &sres, NULL, 0, &lpRows);
		if (FAILED(hr))
		{
			return hr;
		}
	
		// Configure Microsoft Exchange Service Here
		rgval[0].ulPropTag		= PR_PROFILE_UNRESOLVED_NAME;
		rgval[0].Value.lpszA	= reinterpret_cast<char*>(lpszExchangeNameFL);	// Now we use the real name

		rgval[1].ulPropTag		= PR_PROFILE_UNRESOLVED_SERVER;
		rgval[1].Value.lpszA	= reinterpret_cast<char*>(lpszServer);

		hr = pSvcAdmin->ConfigureMsgService((LPMAPIUID) lpRows->aRow->lpProps[iSvcUID].Value.bin.lpb,
			                            0, 0, 2, rgval);
	}
	catch (...)
	{
		return hr;
	}

	return hr;
}

HRESULT CMapi::AttachMailbox(LPSTR lpszProfile, LPSTR lpszMailboxDisplay, LPSTR lpszMailboxDN, LPSTR lpszServer, LPSTR lpszServerDN)
{
    HRESULT             hRes = S_OK;            // Result code returned from MAPI calls.
    LPPROFADMIN         lpProfAdmin = NULL;     // Profile Admin pointer.
    LPSERVICEADMIN      lpSvcAdmin = NULL;      // Message Service Admin pointer.
    LPPROVIDERADMIN     lpProvAdmin = NULL;     // Provider Admin pointer.
    LPMAPITABLE         lpMsgSvcTable = NULL;   // MAPI table pointer.
    LPPROFSECT          lpProfileSection = NULL;// Profile Section Pointer.
    LPSRowSet           lpSvcRows = NULL;       // Row set pointer.
    SPropValue          rgval[4];               // Property value structure to hold configuration info.
    SPropValue          NewVals;                // Property value structure to hold global profile info.
    SRestriction        sres;                   // Restriction structure (used in HrQueryAllRows).
    SPropValue          SvcProps;               // Property value structure used in restriction.
    LPSPropValue        lpGlobalVals = NULL;    // Property value struct pointer for global profile section.
    ULONG               ulProps = 0;            // Count of props.
    ULONG               cbNewBuffer = 0;        // Count of bytes for new buffer.

    // Enumeration for convenience.
    enum {iDispName, iSvcName, iSvcUID, cptaSvc};

     int a = 0;
     BSTR unicodestr = SysAllocStringLen(NULL, a);

    // This structure tells HrQueryAllRows what columns we want returned.
    SizedSPropTagArray(cptaSvc,sptCols) = { cptaSvc, 
                                            PR_DISPLAY_NAME, 
                                            PR_SERVICE_NAME, 
                                            PR_SERVICE_UID };

    // This structure tells our GetProps call what properties to get from the global profile section.
    SizedSPropTagArray(1, sptGlobal) = { 1, PR_STORE_PROVIDERS };

    // Get an IProfAdmin interface.

    hRes = MAPIAdminProfiles(0,             // Flags
                             &lpProfAdmin); // Pointer to new IProfAdmin
    if (FAILED(hRes)) goto error_handler;
    printf("Retrieved IProfAdmin interface.\n");

    // Get an IMsgServiceAdmin interface off of the IProfAdmin interface.

    hRes = lpProfAdmin->AdminServices(LPWSTR(lpszProfile),  // Profile that we want to modify.
                                      LPWSTR(""),           // Password for that profile.
                                      NULL,         // Handle to parent window.
                                      0,            // Flags.
                                      &lpSvcAdmin); // Pointer to new IMsgServiceAdmin.
    if (FAILED(hRes)) goto error_handler;
    printf("Retrieved IMsgServiceAdmin interface.\n");
        
    // We now need to get the entry id for the Exchange service.
    // First, we get the Message service table.

    hRes = lpSvcAdmin->GetMsgServiceTable(0,                // Flags
                                          &lpMsgSvcTable);  // Pointer to table
    if (FAILED(hRes)) goto error_handler;
    printf("Retrieved message service table from profile.\n");

    // Set up restriction to query table.

     sres.rt = RES_CONTENT;
     sres.res.resContent.ulFuzzyLevel = FL_FULLSTRING;
     sres.res.resContent.ulPropTag = PR_SERVICE_NAME_A;
     sres.res.resContent.lpProp = &SvcProps;

     SvcProps.ulPropTag = PR_SERVICE_NAME_A;
    SvcProps.Value.lpszA = "MSEMS";

    // Query the table to get the entry for the Exchange message service.

    hRes = HrQueryAllRows(lpMsgSvcTable,
                          (LPSPropTagArray)&sptCols,
                          &sres,
                          NULL,
                          0,
                          &lpSvcRows);
    if (FAILED(hRes)) goto error_handler;
    printf("Queried table for Exchange message service.\n");


	 if (lpSvcRows->cRows>0)
	 {
		 // Get a provider admin pointer.
		 hRes = lpSvcAdmin->AdminProviders((LPMAPIUID)lpSvcRows->aRow->lpProps[iSvcUID].Value.bin.lpb,
											0,
											&lpProvAdmin);
		 if (FAILED(hRes)) goto error_handler;
		 printf("Retrieved IProviderAdmin interface\n");
	 }

     // Set up a SPropValue array for the properties you need to configure.
     a = lstrlenA(lpszMailboxDisplay);
     unicodestr = SysAllocStringLen(NULL, a);
     MultiByteToWideChar(CP_ACP, 0, lpszMailboxDisplay, a, unicodestr, a);

    // First, display name.
    ZeroMemory(&rgval[0], sizeof(SPropValue) );
     rgval[0].ulPropTag = PR_DISPLAY_NAME_W;
     rgval[0].Value.lpszW = unicodestr;

    // Next, the DN of the mailbox.
    ZeroMemory(&rgval[1], sizeof(SPropValue) );
    rgval[1].ulPropTag = PR_PROFILE_MAILBOX; 
    rgval[1].Value.lpszA = lpszMailboxDN;

    // Next the name of the server the mailbox is on.
    ZeroMemory(&rgval[2], sizeof(SPropValue) );
    rgval[2].ulPropTag = PR_PROFILE_SERVER;
    rgval[2].Value.lpszA = lpszServer;

    // Finally, the DN of the server the mailbox is on.
    ZeroMemory(&rgval[3], sizeof(SPropValue) );
    rgval[3].ulPropTag = PR_PROFILE_SERVER_DN;
    rgval[3].Value.lpszA = lpszServerDN;

    // Create the message service with the above properties.
    hRes = lpProvAdmin->CreateProvider(LPWSTR("EMSDelegate"),
                                       4,
                                       rgval,
                                       0,
                                       0,
                                       (LPMAPIUID)lpSvcRows->aRow->lpProps[iSvcUID].Value.bin.lpb);
    if (FAILED(hRes)) goto error_handler;
    printf("The new mailbox is added.\n");

    // Now let's set the props we need so that the additional mailbox 
    // will display in the UI.

    // Open the global profile section.
    hRes = lpProvAdmin->OpenProfileSection((LPMAPIUID)pbGlobalProfileSectionGuid,
                                           NULL,
                                           MAPI_MODIFY,
                                           &lpProfileSection);
    if (FAILED(hRes)) goto error_handler;
    printf("Opened global profile section.\n");

    // Get the list of store providers in PR_STORE_PROVIDERS.
    hRes = lpProfileSection->GetProps((LPSPropTagArray)&sptGlobal,
                                      0,
                                      &ulProps,
                                      &lpGlobalVals);
    if (FAILED(hRes)) goto error_handler;
    printf("Got the list of mailboxes being opened.\n");

    // Now we set up an SPropValue structure with the original
    // list + the UID of the new service.

    // Compute the new byte count
    cbNewBuffer = lpSvcRows->aRow->lpProps[iSvcUID].Value.bin.cb + lpGlobalVals->Value.bin.cb;

    // Allocate space for the new list of UIDs.
    hRes = MAPIAllocateBuffer( cbNewBuffer,
                              (LPVOID *)&NewVals.Value.bin.lpb);
    if (FAILED(hRes)) goto error_handler;
    printf("Allocated buffer to hold new list of mailboxes to be opened.\n");

    // Copy the bits into the list.
    // First, copy the existing list.
    memcpy(NewVals.Value.bin.lpb,
           lpGlobalVals->Value.bin.lpb,
           lpGlobalVals->Value.bin.cb);

    // Next, copy the new UID onto the end of the list.
    memcpy(NewVals.Value.bin.lpb + lpGlobalVals->Value.bin.cb,
           lpSvcRows->aRow->lpProps[iSvcUID].Value.bin.lpb,
           lpSvcRows->aRow->lpProps[iSvcUID].Value.bin.cb);
    printf("Concatenated list of mailboxes and new mailbox.\n");

    // Set the count of bytes on the SPropValue variable.
    NewVals.Value.bin.cb = cbNewBuffer;
    // Initialize dwAlignPad.
    NewVals.dwAlignPad = 0;
    // Set the prop tag.
    NewVals.ulPropTag = PR_STORE_PROVIDERS;

    // Set the property on the global profile section.
    hRes = lpProfileSection->SetProps(ulProps,
                                      &NewVals,
                                      NULL);
    if (FAILED(hRes)) goto error_handler;
    printf("Set the new list on the global profile section.\n");

    goto cleanup;
    
error_handler:
    printf("ERROR: hRes = %0x\n", hRes);

cleanup:
    // Clean up.
    if (NewVals.Value.bin.lpb) MAPIFreeBuffer(NewVals.Value.bin.lpb);
    if (lpGlobalVals) MAPIFreeBuffer(lpGlobalVals);
    if (lpSvcRows) FreeProws(lpSvcRows);
    if (lpMsgSvcTable) lpMsgSvcTable->Release();
    if (lpSvcAdmin) lpSvcAdmin->Release();
    if (lpProfAdmin) lpProfAdmin->Release();
    if (lpProvAdmin) lpProvAdmin->Release();
    if (lpProfileSection) lpProfileSection->Release();

    printf("Done cleaning up.\n");

    return hRes;
}
	
HRESULT CMapi::RemoveMailbox(LPSTR lpszProfile, LPSTR lpszMailbox)
{
	HRESULT hRes = S_OK; // Result code returned from MAPI calls.
	LPPROFADMIN lpProfAdmin = NULL; // Profile Admin pointer.
	LPSERVICEADMIN lpSvcAdmin = NULL; // Message Service Admin pointer.
	LPPROVIDERADMIN lpProvAdmin = NULL; // Provider Admin pointer.
	LPMAPITABLE lpMsgSvcTable = NULL; // MAPI table pointer.
	LPPROFSECT lpProfileSection = NULL;// Profile Section Pointer.
	LPSRowSet lpSvcRows = NULL; // Row set pointer.
	LPSPropValue lpProp  =   NULL;       
	SRestriction sres; 
	SPropValue SvcProps;
	LPMAPIUID lpProviderUID = {0};

	enum {iDispName, iSvcName, iSvcUID, iProvUID, cptaSvc};

	//TCHAR msg[1024]; 
//	_stprintf(msg, _T("RemoveMailbox - Start"), hRes); 
//	MessageBox (msg);

     // This structure tells HrQueryAllRows what columns we want returned.
    SizedSPropTagArray(cptaSvc,sptCols) = {	cptaSvc,
											PR_DISPLAY_NAME,
											PR_SERVICE_NAME,
											PR_SERVICE_UID,
											PR_PROVIDER_UID};

     // This structure tells our GetProps call what properties to get from the global profile section.
    SizedSPropTagArray(1, sptGlobal) = { 1, PR_STORE_PROVIDERS };

	// Get an IProfAdmin interface.
	hRes = MAPIAdminProfiles(	0, // Flags
								&lpProfAdmin); // Pointer to new IProfAdmin
	if (FAILED(hRes)) goto error_handler;	
	printf("Retrieved IProfAdmin interface.\n"); 

	// Get an IMsgServiceAdmin interface off of the IProfAdmin interface. 
	hRes = lpProfAdmin->AdminServices(	LPWSTR(lpszProfile), // Profile that we want to modify.
										LPWSTR(""), // Password for that profile.
										NULL, // Handle to parent window.
										0, // Flags.
										&lpSvcAdmin); // Pointer to new IMsgServiceAdmin.
	if (FAILED(hRes)) goto error_handler; 
	printf("Retrieved IMsgServiceAdmin interface.\n"); 

	// We now need to get the entry id for the Exchange service.
	// First, we get the Message service table.
	hRes = lpSvcAdmin->GetMsgServiceTable(	0, // Flags 
											&lpMsgSvcTable); // Pointer to table
	if (FAILED(hRes)) goto error_handler; 
	printf("Retrieved message service table from profile.\n"); 

	// Set up restriction to query table.
	sres.rt = RES_CONTENT; 
	sres.res.resContent.ulFuzzyLevel = FL_FULLSTRING; 
	sres.res.resContent.ulPropTag = PR_SERVICE_NAME_A; 
	sres.res.resContent.lpProp = &SvcProps; 

	SvcProps.ulPropTag = PR_SERVICE_NAME_A; 
	SvcProps.Value.lpszA = "MSEMS";  

	// Query the table to get the entry for the Exchange message service.
	hRes = HrQueryAllRows(	lpMsgSvcTable, 
							(LPSPropTagArray)&sptCols, 
							&sres, 
							NULL, 
							0, 
							&lpSvcRows); 

	if (FAILED(hRes)) goto error_handler; 
	printf("Queried table for Exchange message service.\n"); 

	if (lpSvcRows->cRows>0) 
	{ 
		// Get a provider admin pointer.
		hRes = lpSvcAdmin->AdminProviders(	(LPMAPIUID)lpSvcRows->aRow->lpProps[iSvcUID].Value.bin.lpb,
											0, 
											&lpProvAdmin); 
		if (FAILED(hRes)) goto error_handler; 
		printf("Retrieved IProviderAdmin interface\n"); 
	} 

	// Get a provider table
	hRes = lpProvAdmin->GetProviderTable(0, &lpMsgSvcTable);
	if (FAILED(hRes)) goto error_handler; 
	printf("Retrieved IMAPITable pointer.\n"); 
 
	hRes = lpMsgSvcTable->SetColumns((LPSPropTagArray)&sptCols, 0);
	if (FAILED(hRes)) goto error_handler; 
	printf("Set IMAPITable columns.\n"); 

	sres.rt = RES_CONTENT;
    sres.res.resContent.ulFuzzyLevel = FL_FULLSTRING;
    sres.res.resContent.ulPropTag = PR_DISPLAY_NAME_A;
	sres.res.resProperty.lpProp = &SvcProps;  

	SvcProps.ulPropTag = PR_DISPLAY_NAME_A;
	SvcProps.Value.lpszA = lpszMailbox; //Name to find.      
	 
	// Apply the above restriction to just Providers with our display name.
	hRes = lpMsgSvcTable->Restrict(&sres, TBL_ASYNC);
	if (FAILED(hRes)) goto error_handler; 
	printf("Set IMAPITable restriction.\n"); 

	// Set to beginning of table
	hRes = lpMsgSvcTable->SeekRow(BOOKMARK_BEGINNING,0,NULL);      
	hRes = lpMsgSvcTable->QueryRows(4000, 0, &lpSvcRows); // Max 4000 rows.

	// There should be at least one row
	if(lpSvcRows->cRows == 0) goto error_handler; 
	printf("Provider found.\n"); 

	// We've found the Service. Go get it.
	lpProp = &lpSvcRows->aRow[0].lpProps[3];       

	// Allocate space for Service UID and return it.
	hRes = MAPIAllocateBuffer(	lpProp->Value.bin.cb,
								(LPVOID*)&lpProviderUID);       


	// copy the UID into our local.
	memcpy(&lpProviderUID->ab, lpProp->Value.bin.lpb,
	lpProp->Value.bin.cb);


	// Delete the provider.
	hRes = lpProvAdmin->DeleteProvider(lpProviderUID);
 	if (FAILED(hRes)) goto error_handler; 
	printf("Mailbox deleted.\n"); 
	//_stprintf(msg, _T("Mailbox deleted"), hRes); 
	//MessageBox (msg);

	goto cleanup;

error_handler:
     printf("ERROR: hRes = %0x\n", hRes);
	 


cleanup:
     // Clean up.
     /*if (lpSvcRows) FreeProws(lpSvcRows);
     if (lpMsgSvcTable) lpMsgSvcTable->Release();
     if (lpSvcAdmin) lpSvcAdmin->Release();
     if (lpProfAdmin) lpProfAdmin->Release();
     if (lpProvAdmin) lpProvAdmin->Release();
     if (lpProfileSection) lpProfileSection->Release();*/

	 if (lpSvcRows) FreeProws(lpSvcRows);              
                lpSvcRows = NULL;
     if (lpMsgSvcTable) lpMsgSvcTable->Release();
lpMsgSvcTable = NULL;
     if (lpSvcAdmin) lpSvcAdmin->Release();
lpSvcAdmin = NULL;
     if (lpProfAdmin) lpProfAdmin->Release();
lpProfAdmin = NULL;
     if (lpProvAdmin) lpProvAdmin->Release();
lpProvAdmin = NULL;
     if (lpProfileSection) lpProfileSection->Release();
lpProfileSection = NULL;

     printf("Done cleaning up.\n");
//_stprintf(msg, _T("RemoveMailbox - End "), hRes); 
//	MessageBox (msg);

return hRes;
 
}

HRESULT CMapi::DeleteMessagingService(LPTSTR lpszService)
{
	enum {iSvcName, iSvcUID, cptaSvc};
    SizedSPropTagArray (cptaSvc, ptaSvc) = { cptaSvc, 
                                            {   PR_SERVICE_NAME,
                                                PR_SERVICE_UID } };
	pSvcProps.ulPropTag = PR_SERVICE_NAME;
	pSvcProps.Value.lpszA = reinterpret_cast<char*>(lpszService);
	
	try
	{
		hr = HrQueryAllRows(pMsgSvcTable, (LPSPropTagArray)&ptaSvc, &sres, NULL, 0, &lpRows);
    	hr = pSvcAdmin->DeleteMsgService((LPMAPIUID) lpRows->aRow->lpProps[iSvcUID].Value.bin.lpb);
	}
	catch (...) 
	{
		return hr;
	}

	return hr;
}

HRESULT CMapi::GetMessageStoresTable()
{
	try 
	{
		hr = pSession->GetMsgStoresTable(0, &pMsgSvcTable);
		
	}
	catch (...)
	{
		return hr;
	}

	return hr;
}

HRESULT CMapi::SetDefaultMessagingStore(LPTSTR sMailbox)
{
	enum {iSvcName, iSvcUID, cptaSvc};
    SizedSPropTagArray (cptaSvc, ptaSvc) = { cptaSvc, 
                                            {   PR_DISPLAY_NAME,
                                                PR_SERVICE_UID } };

	sres.res.resProperty.ulPropTag = PR_DISPLAY_NAME;

	
	
	pSvcProps.ulPropTag = PR_DISPLAY_NAME;
	pSvcProps.Value.lpszA =  reinterpret_cast<char*>(sMailbox);
	
	hr = pMsgSvcTable->FindRow(&sres, BOOKMARK_BEGINNING, 0);
	if (SUCCEEDED(hr))
	{
		LPSPropValue	lpProp = NULL;
		
		hr = pMsgSvcTable->QueryRows(1, 0, &lpRows);
		if(FAILED(hr))
		{
			return hr;
		}

		if (lpRows->cRows != 1)
		{
			return hr;
		}

		lpProp = &lpRows->aRow[0].lpProps[0];

		if(lpProp->ulPropTag != PR_ENTRYID)
		{
			return hr;
		}

		try 
		{
			hr = pSession->SetDefaultStore(MAPI_DEFAULT_STORE,//Default
				lpProp->Value.bin.cb, (LPENTRYID) lpProp->Value.bin.lpb);
		}
		catch (...)
		{
			return hr;
		}
	}
	
	return hr;
}

HRESULT CMapi::GetEMSServiceUID()
{
	SPropValue		spv;
	LPSPropValue   lpProp  =   NULL;          

	// Restrict the columns to just PR_DISPLAY_NAME & PR_ENTRYID
	static SizedSPropTagArray(2, Columns) =   {2, {PR_DISPLAY_NAME,
												 PR_SERVICE_UID}};       

	hr = pSvcAdmin->GetMsgServiceTable(0, &pMsgSvcTable);
	if (FAILED(hr))
	{
		goto cleanup;
	}

	hr = pMsgSvcTable->SetColumns((LPSPropTagArray)&Columns, 0);       
	if (FAILED(hr))
	{
	   goto cleanup;
	}       

	// Set up a restriction for the Exchange Server Service.
	sres.rt = RES_PROPERTY;
	sres.res.resProperty.relop = RELOP_EQ;
	sres.res.resProperty.ulPropTag = PR_SERVICE_NAME;
	sres.res.resProperty.lpProp = &spv;       
	spv.ulPropTag = PR_SERVICE_NAME;
	spv.Value.lpszA = "MSEMS";       // Call FindRow with that restriction
	hr = pMsgSvcTable->FindRow(&sres, BOOKMARK_BEGINNING, 0);       

	if (SUCCEEDED(hr))
	{
		 // We have found the Service.  Go get it.
		 hr = pMsgSvcTable->QueryRows(1, 0, &lpRows);          
		 if (FAILED(hr))
		 {
			goto cleanup;
		 }          
		 // It would not make sense to have more than one row returned in
		 // this case.
		 if (lpRows->cRows != 1)
		 {
			hr = (E_FAIL);
			goto cleanup;
		 }          
		 // We know that the 2nd row has the Service UID.
		 // See SetColumns() (above).
		 lpProp = &lpRows->aRow[0].lpProps[1];          

		 if (lpProp->ulPropTag != PR_SERVICE_UID)
		 {
			hr = (E_FAIL);
			goto cleanup;
		 }          

		 // Copy the UID into our member.
		 memcpy(&pServiceUID.ab, lpProp->Value.bin.lpb,
				lpProp->Value.bin.cb);       
	}

cleanup:
	 return hr;
}

void CMapi::CloseMapi()
{
	//if (lpRows)
	//{
	//	FreeProws(lpRows);
	//}
	//if (pMsgSvcTable)
	//{
	//	pMsgSvcTable->Release();
	//}
	//if (pProviderAdmin)
	//{
	//	pProviderAdmin->Release();
	//}
	//if (pProfAdmin)
	//{
	//	pProfAdmin->Release();
	//}
	//if (pSvcAdmin)
	//{
	//	pSvcAdmin->Release();
	//}
    MAPIUninitialize();
	return;
}
