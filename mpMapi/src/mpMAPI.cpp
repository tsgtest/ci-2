// mpMAPI.cpp : Defines the entry point for the DLL application.
//

#include "stdafx.h"
#include "Mapi.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

LONG APIENTRY AddMailBox(LPSTR lpszProfile, LPSTR lpszMailboxDisplay, LPSTR lpszMailboxDN, LPSTR lpszServer, LPSTR lpszServerDN)
{
	HRESULT		hr;
	CMapi		Mapi;
	hr = Mapi.Initialize();
	if (FAILED(hr)){ goto cleanup;}

	hr = Mapi.AttachMailbox(lpszProfile, lpszMailboxDisplay, lpszMailboxDN, lpszServer, lpszServerDN);
cleanup:
	MAPIUninitialize();
	return hr;
}

LONG APIENTRY RemoveMailBox(LPSTR szProfile, LPSTR szMailbox)
{
	HRESULT		hr;
	CMapi		Mapi;

	hr = Mapi.Initialize();
	if (FAILED(hr)){ return hr;}
	
	hr = Mapi.AdminProfiles();
	if (FAILED(hr)){ goto cleanup;}

	hr = Mapi.LogonDefaultEx();
	if (FAILED(hr)){ goto cleanup;}

	hr = Mapi.OpenAdminServices();
	if (FAILED(hr)){ goto cleanup;}

	hr = Mapi.RemoveMailbox(szProfile, szMailbox);
	Mapi.CloseMapi();

cleanup:
	return hr;
}
