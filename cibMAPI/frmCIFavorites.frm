VERSION 5.00
Object = "{00028CDA-0000-0000-0000-000000000046}#6.0#0"; "tdbg6.ocx"
Begin VB.Form frmCIFavorites 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Select Favorite Contacts Folders"
   ClientHeight    =   4980
   ClientLeft      =   4152
   ClientTop       =   2808
   ClientWidth     =   6876
   Icon            =   "frmCIFavorites.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4980
   ScaleWidth      =   6876
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton btnDelete 
      Height          =   315
      Left            =   3225
      Picture         =   "frmCIFavorites.frx":000C
      Style           =   1  'Graphical
      TabIndex        =   12
      Top             =   1050
      Width           =   525
   End
   Begin VB.CommandButton btnAdd 
      Height          =   315
      Left            =   3225
      Picture         =   "frmCIFavorites.frx":0182
      Style           =   1  'Graphical
      TabIndex        =   11
      Top             =   630
      UseMaskColor    =   -1  'True
      Width           =   525
   End
   Begin VB.ListBox lstFavorites 
      Appearance      =   0  'Flat
      Height          =   2328
      Left            =   3930
      Sorted          =   -1  'True
      TabIndex        =   3
      Top             =   375
      Width           =   2835
   End
   Begin VB.CommandButton cmdRestore 
      Caption         =   "&Reload Saved Defaults"
      Height          =   375
      Left            =   3930
      TabIndex        =   6
      Top             =   3930
      Width           =   2880
   End
   Begin TrueDBGrid60.TDBGrid lstPeople 
      Height          =   3975
      Left            =   105
      OleObjectBlob   =   "frmCIFavorites.frx":02F8
      TabIndex        =   1
      Top             =   375
      Width           =   2925
   End
   Begin VB.ComboBox Combo1 
      Height          =   330
      Left            =   720
      Style           =   2  'Dropdown List
      TabIndex        =   9
      Top             =   4020
      Visible         =   0   'False
      Width           =   1410
   End
   Begin VB.CommandButton cmdClear 
      Caption         =   "&Clear List"
      Height          =   375
      Left            =   3930
      TabIndex        =   4
      Top             =   3090
      Width           =   2865
   End
   Begin VB.CommandButton btnSave 
      Caption         =   "&Save Current List as Default"
      Height          =   375
      Left            =   3930
      TabIndex        =   5
      Top             =   3510
      Width           =   2880
   End
   Begin VB.CommandButton btnCancel 
      Cancel          =   -1  'True
      Caption         =   "&Done"
      Height          =   400
      Left            =   5880
      TabIndex        =   7
      Top             =   4530
      Width           =   900
   End
   Begin VB.Label lblMessage 
      Caption         =   "No Favorites currently selected."
      Height          =   315
      Left            =   165
      TabIndex        =   10
      Top             =   4560
      Width           =   5565
   End
   Begin VB.Label lblFavorites 
      Caption         =   "Selected &Favorites:"
      Height          =   210
      Left            =   3960
      TabIndex        =   2
      Top             =   165
      Width           =   2175
   End
   Begin VB.Label lblOffices 
      Caption         =   "&Office:"
      Height          =   255
      Left            =   120
      TabIndex        =   8
      Top             =   4050
      Visible         =   0   'False
      Width           =   495
   End
   Begin VB.Label lblPeople 
      Caption         =   "Available &Mailboxes:"
      Height          =   210
      Left            =   135
      TabIndex        =   0
      Top             =   165
      Width           =   2835
   End
   Begin VB.Menu mnuMain 
      Caption         =   "&Main"
      Visible         =   0   'False
   End
End
Attribute VB_Name = "frmCIFavorites"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Const xPermanentMessage = "Current list will be used as the default for future Word sessions."
Const xSessionMessage = "Current list will be used for duration of this Word session only."
Const xNoneMessage = "No Favorites currently selected."

Private m_bCancelled As Boolean
Private m_oCIArray As XArrayDB
Private m_iMaxFavorites As Integer

Public Property Get Cancelled() As Boolean
    Cancelled = m_bCancelled
End Property

Private Sub btnAdd_Click()
    Dim i As Integer
    Dim xName As String
    Dim xNewName As String
    With lstPeople
        For i = 1 To .SelBookmarks.Count
            xName = .Array.Value(.SelBookmarks(i - 1), 0)
            If Not g_oCIFav.AddItem(xName) Then
                MsgBox xName & " is already in list.", vbInformation, g_oSessionType.AppTitle
            Else
                xNewName = xName
            End If
        Next i
        If xNewName <> "" Then
            PopulateFavorites
            lstFavorites.Text = xNewName
        End If
            
        While .SelBookmarks.Count > 0
            .SelBookmarks.Remove 0
        Wend
    End With
    lblMessage = xSessionMessage
End Sub

Private Sub btnCancel_Click()
    Me.Hide
End Sub

Private Sub btnDelete_Click()
    Dim i As Integer
    With lstFavorites
        i = .ListIndex
        If i > -1 Then
            g_oCIFav.RemoveItem .Text
            .RemoveItem i
            If i < .ListCount - 1 Then
                .ListIndex = i
            Else
                .ListIndex = .ListCount - 1
            End If
            
        End If
        If .ListCount > 0 Then
            lblMessage = xSessionMessage
        Else
            g_oCIFav.Clear
            lblMessage = xNoneMessage
        End If
    End With
End Sub

Private Sub btnSave_Click()
    Dim Ret As Integer
    If lstFavorites.ListCount = 0 Then
        Ret = MsgBox("No favorites are selected.  Do you want to delete your saved defaults?", vbYesNo + vbQuestion, g_oSessionType.AppTitle)
        If Ret = vbNo Then Exit Sub
    End If
    g_oCIFav.UpdateTable
    If lstFavorites.ListCount > 0 Then
        lblMessage = xPermanentMessage
    Else
        lblMessage = xNoneMessage
    End If
End Sub
Private Sub cmdClear_Click()
    lstFavorites.Clear
    g_oCIFav.Clear
    lblMessage = xNoneMessage
End Sub
Private Sub cmdRestore_Click()
    PopulateFavorites True
End Sub

Private Sub Form_Activate()
    If Me.lstPeople.Enabled Then
        Me.lstPeople.SetFocus
    Else
        Me.lstFavorites.SetFocus
    End If
End Sub

Private Sub Form_Load()
    Dim oAEs As MAPI.AddressEntries
    Dim xAddList As String
    On Error GoTo ProcError
    Dim oIni As New CIO.CIni
    m_iMaxFavorites = Val(oIni.GetIni("Backend" & g_iID, "MaxFavorites"))
    If m_iMaxFavorites = 0 Then _
        m_iMaxFavorites = 10
    xAddList = oIni.GetIni("Backend" & g_iID, "AddressList")
    On Error Resume Next
    Set oAEs = g_oMAPISession.AddressLists(xAddList).AddressEntries
    On Error GoTo ProcError
    
    If oAEs Is Nothing Then
        Err.Raise ciErr_MissingOrInvalidINIKey, "CIMAPI.frmCIFavorites.Form_Load", _
            "Global Address List not found.  It is possible that you are not currently " & _
            "connected to your Exchange Server.", vbExclamation
        Me.lblPeople.Enabled = False
        Me.lstPeople.Enabled = False
    Else
        PopulateNames oAEs
    End If
    
    PopulateFavorites
    
    Set oAEs = Nothing
    
    Exit Sub
ProcError:
    If Err.Number = MAPI_E_USER_CANCEL Then
        Unload Me
    Else
        MsgBox "The following unexpected error occurred:" & vbCr & vbCr & _
            "Number:  " & Err.Number & vbCr & _
            "Description: " & Err.Description & vbCr & _
            "Source: mpCI.frmCIFavorites.Form_Load", vbExclamation
        'Unload Me
    End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set m_oCIArray = Nothing
    Set frmCIFavorites = Nothing
End Sub

Private Sub lstPeople_DblClick()
    btnAdd_Click
End Sub
Public Sub MatchInXArrayList(KeyCode As Integer, ctlP As Control)
'selects the first/next entry in an xarray-based list control whose
'first character is KeyCode
    Dim lRows As Long
    Dim lCurRow As Long
    Dim xarP As XArrayDB
    Dim lStartRow As Long
    Static xChr As String
    Static lLastTick As Long
    Dim oNum As Object
    
    Set oNum = CreateObject("mpBase2.CNumbers")
    If lLastTick = 0 Then
        lLastTick = oNum.CurrentTick
'   get starting row
        lStartRow = ctlP.Bookmark
'   start with next row
        lCurRow = lStartRow + 1
    Else
'   get starting row
        lStartRow = ctlP.Bookmark
        If oNum.ElapsedTime(lLastTick) > 0.4 Then
            xChr = ""
'   start with next row
            lCurRow = lStartRow + 1
        Else
            lCurRow = lStartRow
        End If
        lLastTick = oNum.CurrentTick
    End If
    
    Set xarP = ctlP.Array
    
'   count rows in xarray
    lRows = xarP.Count(1) - 1
    
'   get the char that we're searching for
    xChr = xChr & UCase(Chr(KeyCode))

    If lCurRow > lRows Then Exit Sub
    
'   loop through xarray until match with first letter is found
    While (xChr <> UCase(Left(xarP.Value(lCurRow, 0), Len(xChr))))
'       if at end of xarray, start ffdsarom beginning,
'       exit if we've looped through all rows in xarray
        If lCurRow = lStartRow - 1 Then
            Exit Sub
        End If
        
'       else move to next row
        If lCurRow < (lRows - 1) Then
            lCurRow = lCurRow + 1
        ElseIf Len(xChr) = 1 And lStartRow <> 0 Then
            lCurRow = 0
        Else
            lCurRow = lCurRow + 1
        End If
        If lCurRow > lRows Then
            Exit Sub
        End If
    Wend
    
    DoEvents
    
'   select the matched item
    With ctlP
        .Bookmark = lCurRow
        While .SelBookmarks.Count > 0
            .SelBookmarks.Remove 0
        Wend
        .SelBookmarks.Add .Bookmark
        .SetFocus
    End With
End Sub
Private Sub PopulateFavorites(Optional bRefresh As Boolean = False)
    Dim i As Integer
'MsgBox "g_oCIFav is nothing: " & g_oCIFav Is Nothing
    If bRefresh Then
        g_oCIFav.Refresh
    End If
    lstFavorites.Clear
    
'    If g_oCIFav Is Nothing Then
'        Set g_oCIFav = New CCIFavorites
'    End If
'
'    If g_oCIFav.ListArray(i, 0) <> Empty Then
        For i = 0 To g_oCIFav.ListArray.UpperBound(1)
            lstFavorites.AddItem g_oCIFav.ListArray(i, 0)
            lstFavorites.ItemData(lstFavorites.NewIndex) = i
        Next i
'    End If
    
    If lstFavorites.ListCount > 0 Then
        lstFavorites.ListIndex = 0
        If g_oCIFav.DefaultState = True Then
            lblMessage = xPermanentMessage
        Else
            lblMessage = xSessionMessage
        End If
    Else
        lstFavorites.ListIndex = -1
        lblMessage = xNoneMessage
    End If
End Sub
Private Sub PopulateNames(oAEs As MAPI.AddressEntries)
    Dim oae As MAPI.AddressEntry
    Dim xCurrentUser As String
    
    If m_oCIArray Is Nothing Then _
        Set m_oCIArray = New XArrayDB
    m_oCIArray.ReDim 0, -1, 0, 1
    
    xCurrentUser = oAEs.Session.CurrentUser.Name
    For Each oae In oAEs
        If oae.DisplayType = CdoUser And oae.Name <> xCurrentUser Then
            m_oCIArray.Insert 1, m_oCIArray.Count(1)
            m_oCIArray.Value(m_oCIArray.UpperBound(1), 0) = oae.Name
            m_oCIArray.Value(m_oCIArray.UpperBound(1), 1) = oae.ID
        End If
    Next oae
    Set lstPeople.Array = m_oCIArray
    lstPeople.ReBind
    lstPeople.Bookmark = 0
    lstPeople.SelBookmarks.Add 0
End Sub

Private Sub lstPeople_KeyDown(KeyCode As Integer, Shift As Integer)
        
    If KeyCode = vbKeyHome Then
        While lstPeople.SelBookmarks.Count > 0
            lstPeople.SelBookmarks.Remove 0
        Wend
        lstPeople.Bookmark = 0
        lstPeople.SelBookmarks.Add 0
        KeyCode = 0
    ElseIf KeyCode = vbKeyEnd Then
        While lstPeople.SelBookmarks.Count > 0
            lstPeople.SelBookmarks.Remove 0
        Wend
        lstPeople.Bookmark = lstPeople.Array.UpperBound(1)
        lstPeople.SelBookmarks.Add lstPeople.Bookmark
        KeyCode = 0
    Else
        MatchInXArrayList KeyCode, Me.lstPeople
    End If
End Sub

