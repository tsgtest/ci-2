Attribute VB_Name = "mdlGlobal"
Option Explicit

Private Declare Sub OutputDebugString Lib "kernel32" _
  Alias "OutputDebugStringA" _
  (ByVal lpOutputString As String)
  
  Declare Function GetUserName Lib "advapi32.dll" Alias "GetUserNameA" ( _
    ByVal lpBuffer As String, nSize As Long) As Long

  
Public Const MAPI_E_USER_CANCEL = &H80040113
Public Const MAPI_E_NOT_FOUND = &H8004010F

Public g_oCIFav As CIMAPI.CCIFavorites
Public g_oMAPISession As MAPI.Session
Public g_iID As Integer

Public g_oError As CIO.CError
Public g_oGlobals As CIO.CGlobals
Public g_oIni As CIO.CIni
Public g_oSessionType As CIO.CSessionType
Public g_oReg As CIO.CRegistry
Public g_oConstants As CIO.CConstants

Public g_xMAPIProfile As String
Public Sub DebugPrint(xOutput As String)
    Debug.Print xOutput
    OutputDebugString xOutput
End Sub

Public Function GetMaxResults() As Long
'returns the maximum number of contacts that can be retrieved
    Dim xMaxresults As String
    Dim lMaxResults As Long
    
    If g_oIni Is Nothing Then
        Set g_oIni = New CIO.CIni
    End If
    
    On Error Resume Next
    xMaxresults = g_oIni.GetIni("Backend" & g_iID, "MaxResults", g_oIni.CIIni)
    On Error GoTo ProcError
    If xMaxresults = Empty Or Not IsNumeric(xMaxresults) Then
        lMaxResults = 0
    Else
        lMaxResults = CLng(xMaxresults)
    End If
    GetMaxResults = lMaxResults
    Exit Function
ProcError:
    g_oError.RaiseError "CIMAPI.mdlGlobal.GetMaxResults"
    Exit Function
End Function
