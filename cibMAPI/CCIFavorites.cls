VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CCIFavorites"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

Private Declare Function AddMailBox Lib "mpMAPI.dll" _
    (ByVal szProfile As String, _
    ByVal szMailbox As String, _
    ByVal szMailBoxEN As String, _
    ByVal szServer As String, _
    ByVal szServerDN As String) As Long
    
Private Declare Function RemoveMailBox Lib "mpMAPI.dll" _
    (ByVal szProfile As String, _
    ByVal szMailbox As String) As Long

Const m_xTableName As String = "tblFavContacts"
Private m_oCustomFields As CIO.CCustomFields
Private m_xDBName As String
Private m_xArray As XArrayDB
Private m_xAddList As String
Private xAdded() As String
Private m_bDefault As Boolean
Private m_xProfile As String

'**********************************************************
'initializer/terminator
'**********************************************************
Dim xObjectID As String
Private Sub Class_Initialize()
    Set m_xArray = New XArrayDB
    m_xArray.ReDim 0, -1, 0, 1
    m_xDBName = MacPacUserFilesDir & "mpPrivate.mdb"
    On Error GoTo 0 '
    If (g_oIni Is Nothing) Then
        Set g_oIni = New CIO.CIni
    End If
    m_xAddList = g_oIni.GetIni("Backend" & g_iID, "AddressList")   ' "Global Address List"
    CreateTableIfNecessary
    Refresh
    
    Math.Randomize
    xObjectID = "CIMAPI.CCIFavorites" & Format(Now, "hhmmss") & Math.Rnd()
    g_oGlobals.Objects.Add xObjectID, xObjectID
    DebugPrint "Added - " & xObjectID & ": " & g_oGlobals.Objects.Count
    Set m_oCustomFields = New CIO.CCustomFields
End Sub

Private Sub Class_Terminate()
    Set m_xArray = Nothing
    g_oGlobals.Objects.Remove xObjectID
    DebugPrint "Removed - " & xObjectID & ": " & g_oGlobals.Objects.Count
    Set m_oCustomFields = Nothing
End Sub
'**********************************************************

Public Sub ManageFavorites()
    Dim oForm As frmCIFavorites
    Set oForm = New frmCIFavorites
    oForm.Show vbModal
    Unload oForm
End Sub

Public Sub Refresh()
    Dim oDB As DAO.Database
    Dim oRS As DAO.Recordset
    
    On Error GoTo ExitProc
    If m_xArray Is Nothing Then _
        Set m_xArray = New XArrayDB
    
    m_xArray.ReDim 0, -1, 0, 1
    Set oDB = DAO.OpenDatabase(m_xDBName, , True)
    Set oRS = oDB.OpenRecordset(m_xTableName, dbOpenForwardOnly, dbReadOnly)
    With oRS
        While Not .EOF
            m_xArray.Insert 1, m_xArray.UpperBound(1) + 1
            m_xArray.Value(m_xArray.UpperBound(1), 0) = .Fields("fldName").Value
            m_xArray.Value(m_xArray.UpperBound(1), 1) = m_xArray.UpperBound(1) + 1 ' .Fields("fldID")
            .MoveNext
        Wend
    End With
    oRS.Close
    m_bDefault = True
    Exit Sub
ExitProc:
    Set oRS = Nothing
End Sub
Public Property Get ListArray() As XArrayDB
    Set ListArray = m_xArray
End Property
Public Property Get DefaultState() As Boolean
    DefaultState = m_bDefault
End Property

Public Function MacPacUserIni() As String
    Dim xPersonalFiles As String

    xPersonalFiles = MacPacUserFilesDir()
    
    If xPersonalFiles <> Empty Then
        On Error Resume Next
        MacPacUserIni = xPersonalFiles & "user.ini"
        On Error GoTo ProcError
    End If
    Exit Function
ProcError:
    Err.Raise Err.Number, "mpCI.CCIFavorites.MacPacUserIni", Err.Description
    Exit Function
End Function

Public Function MacPacUserFilesDir() As String
    Dim xPath As String
    
    On Error GoTo ProcError
    
    If (g_oIni Is Nothing) Then
        Set g_oIni = New CIO.CIni
    End If

    xPath = g_oIni.MacPacUserFilesDir
    
    xPath = GetEnvironVarPath(xPath)
    
    If Right$(xPath, 1) <> "\" Then
        xPath = xPath & "\"
    End If
    
    MacPacUserFilesDir = xPath
    
    Exit Function
ProcError:
    MsgBox "MacPacUserFilesDir Error - " & Err.Description
    Err.Raise Err.Number, "mpCI.CCIFavorites.MacPacUserFilesDir", Err.Description
    Exit Function
End Function
Public Sub CreateTableIfNecessary()
    Dim oDB As DAO.Database
    Dim tbl As DAO.TableDef
    Dim rs As DAO.Recordset
    Dim ix As DAO.Index
    Dim i As Integer
    
    On Error GoTo ProcError
        
    Set oDB = DAO.OpenDatabase(m_xDBName)
    
    On Error Resume Next
    Set tbl = oDB.TableDefs("tblFavContacts")
    If Not (tbl Is Nothing) Then
        Exit Sub
    End If
    
    If tbl Is Nothing Then
        Set tbl = oDB.CreateTableDef("tblFavContacts")
        With tbl
            .Fields.Append .CreateField("fldID", dbInteger)
            .Fields.Append .CreateField("fldName", dbText, 255)
    '        .Fields.Append .CreateField("fldServer", dbText, 255)
    '        .Fields.Append .CreateField("fldContactID", dbText, 255)
        End With
        oDB.TableDefs.Append tbl
        
        Set ix = tbl.CreateIndex("PrimaryKey")
        With ix
            .Fields.Append .CreateField("fldID", dbInteger)
            .Primary = True
            tbl.Indexes.Append ix
        End With
    End If
    
    Set rs = oDB.OpenRecordset("tblFavContacts", dbOpenTable, dbAppendOnly)
    For i = 0 To m_xArray.UpperBound(1)
        With rs
            .AddNew
            .Fields("fldID") = i + 1
            .Fields("fldName") = m_xArray.Value(i, 0)
'                .Fields("fldContactID") = lstPeople.Array.Value(i, 2)
'                .Fields("fldServer") = lstPeople.Array.Value(i, 3)
            .Update
        End With
    Next i
    rs.Close
    Set rs = Nothing
    tbl.Indexes.Refresh
    Set tbl = Nothing
    m_bDefault = True
    Exit Sub
ProcError:
     MsgBox "CreateTableIfNecessary error: " & Err.Description
     Err.Raise Err.Number, "cCIFavorites.CreateTableIfNecessary", Err.Description
End Sub
Public Sub UpdateTable()
    Dim oDB As DAO.Database
    Dim tbl As DAO.TableDef
    Dim rs As DAO.Recordset
    Dim ix As DAO.Index
    Dim i As Integer
    
    On Error GoTo ProcError
        
    Set oDB = DAO.OpenDatabase(m_xDBName)
    
    On Error Resume Next
    Set tbl = oDB.TableDefs(m_xTableName)
    
    If Not tbl Is Nothing Then
        oDB.TableDefs.Delete m_xTableName
    End If
    
    Set tbl = oDB.CreateTableDef(m_xTableName)
    
    With tbl
        .Fields.Append .CreateField("fldID", dbInteger)
        .Fields.Append .CreateField("fldName", dbText, 255)
'        .Fields.Append .CreateField("fldServer", dbText, 255)
'        .Fields.Append .CreateField("fldContactID", dbText, 255)
    End With
    oDB.TableDefs.Append tbl
    
    Set ix = tbl.CreateIndex("PrimaryKey")
    With ix
        .Fields.Append .CreateField("fldID", dbInteger)
        .Primary = True
        tbl.Indexes.Append ix
    End With
    
    Set rs = oDB.OpenRecordset(m_xTableName, dbOpenTable, dbAppendOnly)
    For i = 0 To m_xArray.UpperBound(1)
        With rs
            .AddNew
            .Fields("fldID") = i + 1
            .Fields("fldName") = m_xArray.Value(i, 0)
'                .Fields("fldContactID") = lstPeople.Array.Value(i, 2)
'                .Fields("fldServer") = lstPeople.Array.Value(i, 3)
            .Update
        End With
    Next i
    rs.Close
    Set rs = Nothing
    tbl.Indexes.Refresh
    Set tbl = Nothing
    m_bDefault = True
    Exit Sub
ProcError:
     Err.Raise Err.Number, "cCIFavorites.Update", Err.Description
End Sub
Public Sub RemoveItem(xItem As String)
    Dim i As Integer
    With m_xArray
        For i = 0 To .UpperBound(1)
            If .Value(i, 0) = xItem Then
                m_xArray.Delete 1, i
                m_bDefault = False
                Exit For
            End If
        Next i
    End With
End Sub
Public Function AddItem(xName As String) As Boolean
    Dim i As Integer
    With m_xArray
        For i = 0 To .UpperBound(1)
            If UCase(xName) = UCase(.Value(i, 0)) Then
                AddItem = False
                Exit Function
            End If
        Next i
        .Insert 1, .UpperBound(1) + 1
        .Value(.UpperBound(1), 0) = xName
        .Value(.UpperBound(1), 1) = .UpperBound(1) + 1
        AddItem = True
    End With
    m_bDefault = False
End Function
Public Sub Clear()
    m_xArray.ReDim 0, -1, 0, 1
    m_bDefault = False
End Sub
Public Function AttachFavorites() As Boolean
    Const PR_EMS_AB_HOME_MTA As Long = &H8007001E

    Dim i As Integer
    Dim Ret As Long
    Dim oEntry As MAPI.AddressEntry
    Dim iFavorites As Integer
    Dim xBadNames As String
    Dim xServer As String
    Dim xOrg As String
    Dim xSite As String
    Dim xDN As String
    Dim oStore As MAPI.InfoStore
    Dim xName As String
    Dim xAddress As String
'    Dim xProfile As String
    Dim oMS As MAPI.Session
    
    LogProgress "Start of AttachFavorites", True
    If m_xArray.Count(1) = 0 Then
        Exit Function
    End If
    
    LogProgress "Logon to MAPI"
    On Error GoTo ProcError
    Set oMS = New MAPI.Session
    
    'get existing MAPI user
    If Not (g_oMAPISession Is Nothing) Then
        oMS.Logon g_oMAPISession.Name, , False, False
    Else
        oMS.Logon , , True, False
    End If
    
    LogProgress "Logon Successful"
    m_xProfile = oMS.Name
    LogProgress "Profile Name = " & m_xProfile
    On Error Resume Next
    ReDim xAdded(0)
    LogProgress m_xArray.UpperBound(1) + 1 & " favorites to load"
    For i = 0 To m_xArray.UpperBound(1)
        On Error Resume Next
        Set oEntry = Nothing
        LogProgress "Setting AddressEntry to " & m_xArray.Value(i, 0)
        Set oEntry = oMS.AddressLists(m_xAddList).AddressEntries(m_xArray.Value(i, 0))
        If Not oEntry Is Nothing Then
            xName = oEntry.Name
            xAddress = Trim(oEntry.Address)
            LogProgress "AddressEntry name is " & xName
            Set oStore = Nothing
            Set oStore = oMS.InfoStores("Mailbox - " & xName)
            If oStore Is Nothing Then
                With oEntry
                    ParseInfo .Fields(PR_EMS_AB_HOME_MTA), xOrg, xSite, xServer
                    LogProgress "Address = " & xAddress
                    LogProgress "Organization = " & xOrg
                    LogProgress "Site = " & xSite
                    LogProgress "Server = " & xServer
                    If xServer <> "" Then
                        xDN = "/o=" & xOrg & "/ou=" & xSite & "/cn=Configuration/cn=Servers/cn=" & xServer
                        LogProgress "DN = " & xDN
                        Ret = AddMailBox(m_xProfile, "Mailbox - " & xName, xAddress, xServer, xDN)
                        DoEvents
                        If Ret < 0 Then
                            xBadNames = xBadNames & xName & vbCr
                        Else
                            ReDim Preserve xAdded(UBound(xAdded()) + 1)
                            xAdded(UBound(xAdded())) = xName
                        End If
                    End If
                End With
            End If
        End If
    Next i
    'return TRUE - favorites were attached
    AttachFavorites = True
    LogProgress "Logoff MAPI"
    oMS.Logoff
    If xBadNames <> "" Then
        MsgBox "No public folder found for:" & vbCr & vbCr & xBadNames, vbInformation
    End If
    LogProgress "Exit Sub"

    Exit Function
ProcError:
    Err.Raise Err.Number, "Favorite mailboxes could not be loaded."
    LogProgress "Error handler called"
End Function
Sub RemoveFavorites()
    Dim i As Integer
    Dim Ret As Long
    On Error GoTo ExitProc
    LogProgress "RemoveFavorites - UBound(xAdded()) = " & UBound(xAdded())
    For i = 1 To UBound(xAdded())
        LogProgress "RemoveFavorites - Removing entry " & xAdded(i)
        Ret = RemoveMailBox(m_xProfile, "Mailbox - " & xAdded(i))
        LogProgress "RemoveFavorites - After Removing entry " & xAdded(i)
    Next i
    LogProgress "RemoveFavorites - End"
ExitProc:
    LogProgress "RemoveFavorites - Err.Number " & Err.Number & " : " & Err.Description
    ReDim xAdded(0)
End Sub
Public Sub ParseInfo(ByVal xDN As String, ByRef xOrg As String, _
    ByRef xSite As String, ByRef xServer As String)
    
    ' Parse individual fields from full Entry Address
    Dim i As Integer
    xOrg = ""
    xSite = ""
    xServer = ""
    If xDN <> "" Then
        xDN = Mid(xDN, InStr(UCase(xDN), "/O=") + 3)
        xOrg = Left(xDN, InStr(xDN, "/") - 1)
        xDN = Mid(xDN, InStr(UCase(xDN), "/OU=") + 4)
        xSite = Left(xDN, InStr(xDN, "/") - 1)
        xServer = Mid(xDN, InStr(1, xDN, _
            "/cn=Configuration/cn=Servers/cn=") + _
            Len("/cn=Configuration/cn=Servers/cn="), 255)
        xServer = Left(xServer, InStr(1, xServer, "/") - 1)
    End If
End Sub
Private Sub LogProgress(xOutput As String, Optional bStartNew As Boolean = False)
    Static i As Integer
    If bStartNew Then i = 0
    If i = 0 Then
        Open g_oIni.ApplicationDirectory & "\mpfav.log" For Output As #1
        i = 1
    Else
        i = i + 1
        Open g_oIni.ApplicationDirectory & "\mpfav.log" For Append As #1
    End If
    Print #1, i & ": " & xOutput
    Close #1
End Sub

Private Function GetEnvironVarPath(ByVal xPath As String) As String
'substitutes environment variable for <xToken>;
'if variable doesn't exist, returns path unchanged
    Dim xToken As String
    Dim iPosStart As Integer
    Dim iPosEnd As Integer
    Dim xValue As String
    
    On Error GoTo ProcError
    iPosStart = InStr(xPath, "<")
    iPosEnd = InStr(xPath, ">")
    
    If (iPosStart > 0) And (iPosEnd > 0) Then
        xToken = Mid$(xPath, iPosStart + 1, iPosEnd - iPosStart - 1)
        
        If UCase$(xToken) = "USERNAME" Then
            'we don't use environ for username
            'because it's not an environment variable
            'in Windows 98
            xValue = GetLogonName()
        Else
            xValue = Environ(xToken)
        End If
    End If
    
    If xValue <> "" Then
        GetEnvironVarPath = Replace(xPath, "<" & xToken & ">", xValue)
    Else
        GetEnvironVarPath = xPath
    End If
    
    Exit Function
ProcError:
    Err.Raise Err.Number, "CIMAPI.CCIFavorites.GetEnvironVarPath"
    Exit Function
End Function


Public Function GetLogonName() As String
'returns the current system user name
    Dim xBuf As String
    
    On Error GoTo ProcError
    
'   get logon name
    xBuf = String(255, " ")
    GetUserName xBuf, 255
    
    If Len(xBuf) = 0 Then
'       alert to no logon name
        Err.Raise 5, , _
            "UserName is empty.  You might not be logged on to the system. " & _
                "Please log off, and log on again."
    End If
    
'   trim extraneous buffer chars
    Trim (xBuf)
    xBuf = RTrim$(xBuf)
    xBuf = Left$(xBuf, Len(xBuf) - 1)
    
    GetLogonName = xBuf
    Exit Function
ProcError:
    Err.Raise Err.Number, "CIMAPI.CCIFavorites.GetLogonName"
End Function


