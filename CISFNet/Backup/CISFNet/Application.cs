﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Collections;
using System.Net;
using System.Web;
using System.Web.Services.Protocols;
using CISFNet.Salesforce;

namespace CISFNet
{

    //[ComVisible(true)]
    //[Guid("{274A2A63-084E-4d65-AE8F-5C15D2101E6E}")]
    public interface ICISF
    {
        bool Connect(ref string xSecurityToken);

        //contact interface methods
        string[,] GetListings(string attribute1, string value1,
            string attribute2, string value2,
            string attribute3, string value3,
            string attribute4, string value4,
            string sortField);
        string[,] GetAddresses(string entityID);
        string[,] GetDetails(string contactID, string commaDelimitedFieldNames);
        //string[,] GetNameDetails(string xContactID);
        //string[,] GetPhoneNumbers(string xContactID, string xAddressID);
        //string[,] GetFaxNumbers(string xContactID, string xAddressID);
        //string[,] GetEmailAddresses(string xContactID);
        //string[,] GetAddressDetails(string xEntityID, string xAddressID);

        ////Company/Account interface methods
        //string[,] GetCompanyListings(string xAttribute1, string xValue1,
        //               string xAttribute2, string xValue2,
        //               string xAttribute3, string xValue3,
        //               string xAttribute4, string xValue4,
        //               string xSortField);
        //string[,] GetCompanyDetails(string xCompanyID);
        //string[,] GetCompanyPhoneNumbers(string xContactID, string xAddressID);
        //string[,] GetCompanyFaxNumbers(string xContactID, string xAddressID);
        //string[,] GetCompanyEmailAddresses(string xContactID);
    }

    [ComVisible(true)]
    [Guid("4D9EB454-D5B3-45ac-AF8E-41348DEC27B1"),
    ClassInterface(ClassInterfaceType.None)]
    public class Application: ICISF
    {
        static Salesforce.SforceService service;
        string currentContactDetails;
        string currentContactID;

        #region *********************constructors*********************
        static Application()
        {
            service = new CISFNet.Salesforce.SforceService();

            //timeout after 20 seconds 
            service.Timeout = 20000;

        }
        public Application() { }
        #endregion
        #region *********************enumerations*********************
        public enum StoreType
        {
            ContactEntity = 1,
            CompanyEntity = 2
        }
        #endregion
        #region *********************methods*********************
        /// <summary>
        /// connects to the Salesforce web service-
        /// returns true if connected, false otherwise
        /// </summary>
        /// <returns></returns>
        public bool Connect(ref string xSecurityToken)
        {
            Salesforce.LoginResult lr = null;
            
            try
            {
                // Try logging in   
                try
                {
                    using (AuthenticationForm oForm = new AuthenticationForm(xSecurityToken))
                    {
                        while (lr == null)
                        {
                            if (oForm.ShowDialog() == DialogResult.OK)
                            {
                                try
                                {

                                    lr = service.login(oForm.UserName, oForm.Password + oForm.SecurityToken);  //for mp acct - "VKBEZJY6GX5DbkoZk1RD9LdP2"

                                    //return any change to security token-
                                    //CISF (com) will store the token in the user ini
                                    xSecurityToken = oForm.SecurityToken;
                                }
                                catch (SoapException e)
                                {
                                    if (e.Message.StartsWith("INVALID_LOGIN"))
                                    {
                                        MessageBox.Show("Could not login to Salesforce. Username, password, or security token invalid.",
                                            "Salesforce", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                                    }
                                    else
                                    {
                                        LMP.Error.Show(e);
                                    }
                                }
                            }
                            else
                            {
                                return false;
                            }
                        }
                    }
                }

                // ApiFault is a proxy stub generated from the WSDL contract when     
                // the web service was imported 
                catch (SoapException e)
                {
                    LMP.Error.Show(e);

                    //return False to indicate that the login was not successful 
                    return false;
                }

                // Check if the password has expired 
                if (lr.passwordExpired)
                {
                    Console.WriteLine("An error has occurred. Your password has expired.");
                    return false;
                }

                /** Once the client application has logged in successfully, it will use
                 * the results of the login call to reset the endpoint of the service
                 * to the virtual server instance that is servicing your organization
                 */
                // Save old authentication end point URL
                String authEndPoint = service.Url;
                // Set returned service endpoint URL
                service.Url = lr.serverUrl;

                /** The sample client application now has an instance of the SforceService
                 * that is pointing to the correct endpoint. Next, the sample client
                 * application sets a persistent SOAP header (to be included on all
                 * subsequent calls that are made with SforceService) that contains the
                 * valid sessionId for our login credentials. To do this, the sample
                 * client application creates a new SessionHeader object and persist it to
                 * the SforceService. Add the session ID returned from the login to the
                 * session header
                 */
                service.SessionHeaderValue = new Salesforce.SessionHeader();
                service.SessionHeaderValue.sessionId = lr.sessionId;

                // Return true to indicate that we are logged in, pointed  
                // at the right URL and have our security token in place.     
                return true;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
                return false;
            }
        }

        public string[,] GetListings(string xAttribute1, string xValue1, string xAttribute2, string xValue2, string xAttribute3, string xValue3, string xAttribute4, string xValue4, string xSortField)
        {
            string[,] listings = null;
            string sql = "SELECT LastName, FirstName, Contact.Account.Name, ID FROM Contact";

            //substitute actual field name when neeeded - Company is CI-specific,
            //AccountName is not fully qualified
            if (xAttribute1 == "Company" || xAttribute1 == "AccountName")
            {
                xAttribute1 = "Contact.Account.Name";
            }

            if (xAttribute2 == "Company" || xAttribute2 == "AccountName")
            {
                xAttribute2 = "Contact.Account.Name";
            }

            if (xAttribute3 == "Company" || xAttribute3 == "AccountName")
            {
                xAttribute3 = "Contact.Account.Name";
            }

            if (xAttribute4 == "Company" || xAttribute4 == "AccountName")
            {
                xAttribute4 = "Contact.Account.Name";
            }

            //DisplayName is CI-specific
            if (xSortField == "DisplayName")
            {
                xSortField = "LastName, FirstName";
            }

            //create WHERE clause
            StringBuilder sb = new StringBuilder();

            if (!string.IsNullOrEmpty(xAttribute1))
            {
                sb.AppendFormat("{0} LIKE '{1}'", xAttribute1, xValue1);
            }

            if (!string.IsNullOrEmpty(xAttribute2))
            {
                if (sb.Length > 0)
                    sb.AppendFormat("{0}", " AND ");

                sb.AppendFormat("{0} LIKE '{1}'", xAttribute2, xValue2);
            }

            if (!string.IsNullOrEmpty(xAttribute3))
            {
                if (sb.Length > 0)
                    sb.AppendFormat("{0}", " AND ");

                sb.AppendFormat("{0} LIKE '{1}'", xAttribute3, xValue3);
            }

            if (!string.IsNullOrEmpty(xAttribute4))
            {
                if (sb.Length > 0)
                    sb.AppendFormat("{0}", " AND ");

                sb.AppendFormat("{0} LIKE '{1}'", xAttribute4, xValue4);
            }

            string where = "";

            if (!string.IsNullOrEmpty(sb.ToString()))
            {
                where = " WHERE " + sb.ToString();
            }

            string orderBy = " ORDER BY " + xSortField;
            sql += where + orderBy;

            try
            {
                //query salesforce
                Salesforce.QueryResult qr = service.query(sql);
                bool done = false;

                if (qr.size > 0)
                {
                    listings = new string[qr.size, 5];

                    //cycle through returned set
                    while (!done)
                    {
                        Salesforce.sObject[] records = qr.records;
                        for (int i = 0; i < records.Length; i++)
                        {
                            Salesforce.sObject con = records[i];

                            listings[i, 0] = con.Any[3].InnerText;
                            listings[i, 1] = con.Any[0].InnerText + ", " + con.Any[1].InnerText;
                            listings[i, 2] = con.Any[2].InnerText.StartsWith("Account") ? con.Any[2].InnerText.Substring(7) : con.Any[2].InnerText;
                            listings[i, 3] = con.Any[0].InnerText;
                            listings[i, 4] = con.Any[1].InnerText;
                        }

                        if (qr.done)
                        {
                            done = true;
                        }
                        else
                        {
                            qr = service.queryMore(qr.queryLocator);
                        }
                    }
                }

                return listings;
            }
            catch (Exception e)
            {
                LMP.Error.Show(e);
                return null;
            }
        }
        public string[,] GetAddresses(string xEntityID)
        {
            return new string[2, 2] { { "MailingAddress", "Mailing" }, { "OtherAddress", "Other" } };
        }

        public void DescribeGlobalSample()
        {
            try
            {
                // Make the describeGlobal() call 
                DescribeGlobalResult dgr = service.describeGlobal();

                // Get the sObjects from the describe global result
                DescribeGlobalSObjectResult[] sObjResults = dgr.sobjects;

                // Write the name of each sObject to the console
                for (int i = 0; i < sObjResults.Length; i++)
                {
                    Console.WriteLine(sObjResults[i].name);
                }
            }
            catch (SoapException e)
            {
                Console.WriteLine("An unexpected error has occurred: " +
                    e.Message + "\n" + e.StackTrace);
            }
        }

        public void DescribeOjectType(string objType)
        {
            DescribeSObjectResult res = service.describeSObject(objType);

            for (int i = 0; i < res.fields.Count(); i++)
            {
                Console.WriteLine(res.fields[i].name);
            }
        }

        //public string[,] GetNameDetails(string xContactID)
        //{
        //    return GetDetails("LastName", "FirstName", "Salutation", "Company", "Title", "Department");
        //}

        public string[,] GetDetails(string contactID, string commaDelimitedFieldNames)
        {
            RetreiveDetailsIfNecessary(contactID);

            string[] fields = commaDelimitedFieldNames.Split(',');
            string[,] details = new string[fields.Length, 2];

            for (int i = 0; i < fields.Length; i++)
                details[i, 0] = fields[i];

            int upperBound = details.GetUpperBound(0);

            for (int i = 0; i <= upperBound; i++)
            {
                details[i, 1] = GetDetailValue(details[i, 0]);
            }

            return details;
        }

        //public string[,] GetNameDetailsOLD(string xContactID)
        //{
        //    string[,] entities = null;
        //    string sql = "SELECT LastName, FirstName, Salutation, Contact.Account.Name, Title, Department FROM Contact WHERE ID='" + xContactID + "'";

        //    try
        //    {
        //        //query salesforce
        //        Salesforce.QueryResult qr = service.query(sql);
        //        bool done = false;

        //        if (qr.size > 0)
        //        {
        //            entities = new string[qr.size, 10];

        //            //cycle through returned set
        //            while (!done)
        //            {
        //                Salesforce.sObject[] records = qr.records;
        //                for (int i = 0; i < records.Length; i++)
        //                {
        //                    Salesforce.sObject con = records[i];

        //                    for (int j = 0; j < con.Any.Count(); j++)
        //                    {
        //                        entities[i, j] = con.Any[j].InnerText;
        //                    }
        //                }

        //                if (qr.done)
        //                {
        //                    done = true;
        //                }
        //                else
        //                {
        //                    qr = service.queryMore(qr.queryLocator);
        //                }
        //            }
        //        }
        //    }
        //    catch (System.Exception e)
        //    {
        //        LMP.Error.Show(e);
        //    }

        //    return entities;
        //}

        //public string[,] GetPhoneNumbers(string xContactID, string xAddressID)
        //{
        //    throw new NotImplementedException();
        //}

        //public string[,] GetFaxNumbers(string xContactID, string xAddressID)
        //{
        //    throw new NotImplementedException();
        //}

        //public string[,] GetEmailAddresses(string xContactID)
        //{
        //    throw new NotImplementedException();
        //}
        //public string[,] GetAddressDetails(string xContactID, string xAddressID)
        //{
        //    string[,] entities = null;
        //    string sql = null;

        //    if (xAddressID == "Mailing")
        //    {
        //        sql = "SELECT MailingStreet, MailingCity, MailingState, MailingPostalCode, MailingCountry FROM Contact WHERE ID='" + xContactID + "'";
        //    }
        //    else
        //    {
        //        sql = "SELECT OtherStreet, OtherCity, OtherState, OtherPostalCode, OtherCountry FROM Contact WHERE ID='" + xContactID + "'";
        //    }

        //    try
        //    {
        //        //query salesforce
        //        Salesforce.QueryResult qr = service.query(sql);
        //        bool done = false;

        //        if (qr.size > 0)
        //        {
        //            entities = new string[qr.size, 5];

        //            //cycle through returned set
        //            while (!done)
        //            {
        //                Salesforce.sObject[] records = qr.records;
        //                for (int i = 0; i < records.Length; i++)
        //                {
        //                    Salesforce.sObject con = records[i];

        //                    for (int j = 0; j < con.Any.Count(); j++)
        //                    {
        //                        entities[i, j] = con.Any[j].InnerText;
        //                    }
        //                }

        //                if (qr.done)
        //                {
        //                    done = true;
        //                }
        //                else
        //                {
        //                    qr = service.queryMore(qr.queryLocator);
        //                }
        //            }
        //        }
        //    }
        //    catch (System.Exception e)
        //    {
        //        LMP.Error.Show(e);
        //    }

        //    return entities;
        //}

        private void RetreiveDetailsIfNecessary(string contactID)
        {
            if (currentContactID != contactID)
            {
                RetrieveDetails(contactID);
            }
        }
        /// <summary>
        /// returns the value of the specified field for
        /// the current contact detail
        /// </summary>
        /// <param name="field"></param>
        /// <returns></returns>
        private string GetDetailValue(string field)
        {
            int pos = currentContactDetails.IndexOf("|" + field + "=");

            if (pos > -1)
            {
                pos = pos + field.Length + 2;

                int pos2 = currentContactDetails.IndexOf("|", pos);

                return currentContactDetails.Substring(pos, pos2 - pos);
            }
            else
            {
                return null;
            }
        }

        private void RetrieveDetails(string xContactID)
        {
            string sql = "SELECT Contact.Account.Name";
            StringBuilder sb = new StringBuilder();

            //get all field names
            DescribeSObjectResult res = service.describeSObject("Contact");

            for (int i = 0; i < res.fields.Count(); i++)
            {
                sb.AppendFormat(",{0}", res.fields[i].name);
            }

            sql += sb.ToString();
            sql += " FROM Contact WHERE ID = '" + xContactID + "'";

            try
            {
                //query salesforce
                Salesforce.QueryResult qr = service.query(sql);

                if (qr.size > 0)
                {
                    sb = new StringBuilder();

                    //cycle through returned set
                    if(qr.records.Count() > 0)
                    {
                        Salesforce.sObject con = qr.records[0];

                        //trim first 7 characters, as all companies seem
                        //to begin with the word "Account" - bug?
                        sb.Append("|Company=" + con.Any[0].InnerText.Substring(7));

                        for (int j = 0; j < res.fields.Count(); j++)
                        {
                            sb.AppendFormat("|{0}={1}", res.fields[j].name, con.Any[j + 1].InnerText);
                        }
                    }
                }
            }
            catch (System.Exception e)
            {
                LMP.Error.Show(e);
            }

            currentContactID = xContactID;
            currentContactDetails = sb.ToString();
        }
        #endregion
    }

}
