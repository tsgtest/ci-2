using System;
using System.Globalization;
using System.Threading;
using System.Reflection;
using System.Diagnostics;
using System.Resources;
using System.Text;
using System.Windows.Forms;
using System.IO;
using Interop = System.Runtime.InteropServices;

namespace LMP
{
	/// <summary>
	/// defines an LMP exception - derived from System.Exception
	/// </summary>
	namespace Exceptions
	{
		public class LMPException : Exception
		{
			/// <summary>
			/// constructor for the LMPException class -
			/// derived from System.Exception
			/// </summary>
			/// <param name="xMsg"></param>
			/// <param name="oInnerException"></param>
			public LMPException(
				string xMsg, Exception oInnerException):base(xMsg, oInnerException){}

			/// <summary>
			/// constructor for the LMPException class -
			/// derived from System.Exception
			/// </summary>
			/// <param name="xMsg"></param>
			public LMPException(string xMsg):base(xMsg, null){}

			/// <summary>
			/// shows this LMPException, with the specified into message
			/// </summary>
			/// <param name="xIntroMsg"></param>
			public void Show(string xIntroMsg)
			{
				LMP.Error.Show(this,xIntroMsg);
			}

			/// <summary>
			/// shows this LMPException
			/// </summary>
			public void Show()
			{
				LMP.Error.Show(this);
			}
		}
	}

	/// <summary>
	/// manages exceptions and debugging
	/// </summary>
	public static class Error
	{
        public static string CurrentUser;

		/// <summary>
		/// shows the specified exception - substitutes the
		/// specified intro message for the default message
		/// </summary>
		/// <param name="e"></param>
		/// <param name="xIntroMsg"></param>
		public static void Show(Exception e, string xIntroMsg)
		{		
			string xErrNameLbl=null;
			string xErrDescLbl=null;
			string xErrSourceLbl=null;
			string xErrInternalDescLbl=null;

			//get the message - we do this so that we can modify
			//the message while keeping the rest of the exception's
			//data intact - see the modifications below
			string xMsg = e.Message;

			try
			{
				//get localized strings
				xErrNameLbl="Name";
				xErrDescLbl="Description";
				xErrSourceLbl="Source";
				xErrInternalDescLbl="Internal Description";
			}
			catch
			{
				//could not get localized strings - show error in English only dialog
				ShowInEnglish(e,xMsg);
			}

			try
			{
                string xPromptForFurtherAction = null;

                xPromptForFurtherAction = "Would you like to view more detailed information about this error?";

                //use default intro message if none provided
				if(xIntroMsg==null)
					xIntroMsg = "The following unexpected error occurred: ";
				
				//build error message
				StringBuilder oSB = new StringBuilder();
				oSB.AppendFormat("{0}\n\n{1}  {2}\n{3}  {4}\n\n{5}",
					xIntroMsg,xErrNameLbl,e.GetType().Name,
					xErrDescLbl,xMsg,xPromptForFurtherAction);

				//display error detail to user - prompt to copy to clipboard
                //string xProductName = LMP.ComponentProperties.ProductName;
                string xProductName = "MacPac";
                string xErrorDetail = GetErrorDetail(e);

				DialogResult oChoice = MessageBox.Show(oSB.ToString(),xProductName, 
					MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

				if (oChoice == DialogResult.Yes)
				{
					//copy detail to clipboard
					Clipboard.SetDataObject(xErrorDetail,true);

                    //GLOG : 2762 : CEH 
                    //GLOG : 3106 : CEH 
                    //launch program associated with .txt files and paste error automatically

                    //Get the temp directory.
                    string xFile = System.IO.Path.GetTempPath() + "\\MacPac Error.txt";

                    //create new text file in user documents location
                    StreamWriter oSW = new StreamWriter(xFile);
                    oSW.Write(xErrorDetail);
                    oSW.Close();

                    //launch new text file
                    Process oProcess = new Process();
                    oProcess.StartInfo.FileName = xFile;
                    oProcess.Start();

                    oProcess.WaitForInputIdle();
				}
			}
			catch (System.Exception e2)
			{
				//get new intro message
				xIntroMsg = "An error occurred while displaying an error: ";

				//get error detail in string
				StringBuilder oSB = new StringBuilder();
				oSB.AppendFormat("{0}\n\n{1}:  {2}\n{3}:  {4}",
					xIntroMsg,xErrNameLbl,e2.GetType().ToString(),xErrDescLbl,e2.Message);

				MessageBox.Show(oSB.ToString(),null,MessageBoxButtons.OK,MessageBoxIcon.Error);
			}
		}


		/// <summary>
		/// build error detail- include detail from every inner exception
		/// </summary>
		/// <param name="e"></param>
		/// <returns></returns>
		public static string GetErrorDetail(System.Exception e)
		{
			StringBuilder oSB = new StringBuilder();
			System.Exception oCurExc = e;
					
			//loop through nested inner exceptions
			while (oCurExc != null)
			{
				if (oSB.Length > 0)
					//there's already detail - add blank line to separate
					oSB.AppendFormat("\r\n\r\n{0}\r\n", new string('_',100));

				//add detail for current exception
				oSB.AppendFormat("{0}  {1}\r\n{2}  {3}\r\n{4}{5}",
					"Name:",oCurExc.GetType().Name,"Description:",
					oCurExc.Message, "Stack:",oCurExc.StackTrace);

				//get nested exception
				oCurExc = oCurExc.InnerException;
			}

			return oSB.ToString();
		}


        ///// <summary>
        ///// returns the resource tag in the specified message
        ///// </summary>
        ///// <param name="xMsg"></param>
        ///// <returns></returns>
        //private static string EvaluateMessageTag(string xMsg)
        //{
        //    string xTag = xMsg;

        //    //parse out tag that refers to a particular resource
        //    int iPos1 = xTag.IndexOf("<");
        //    int iPos2 = xTag.IndexOf(">");

        //    if (iPos1 > -1 && iPos2 > -1 && iPos2 > iPos1)
        //    {
        //        //a resource tag exists - get it
        //        xTag = xTag.Substring(iPos1 + 1, iPos2 - iPos1 -1);

        //        if (xTag !="")
        //        {
        //            //get resource from tag
        //            string xRes = LMP.Resources.GetLangString(xTag);
						
        //            if(xRes==null)
        //            {
        //                //no resource was found for tag -
        //                //get message to display below
        //                xMsg= "A resource with the following name could not be found: " + xTag;
        //            }
        //            else
        //            {
        //                //replace tag with resource - 
        //                //we'll display message below
        //                string xTagPortion = xMsg.Substring(iPos1, iPos2-iPos1+1);
        //                xMsg = xMsg.Replace(xTagPortion,xRes);
        //            }
        //        }
        //    }

        //    return xMsg;
        //}


		/// <summary>
		/// shows the specified exception
		/// </summary>
		/// <param name="e"></param>
		public static void Show(Exception e)
		{			
			Show(e,null);
		}

		
		/// <summary>
		/// shows the specified exception in English- substitutes the
		///specified intro message for the default message - displays
		///the specified message instead of the actual error message -
		///we need this method for errors that occur in the LMP.Resource class,
		///when the resources can't be localized
		/// </summary>
		/// <param name="e"></param>
		/// <param name="xNewErrorMsg"></param>
		/// <param name="xIntroMsg"></param>
		public static void ShowInEnglish(Exception e, string xNewErrorMsg, string xIntroMsg)
		{
			StringBuilder oSB = new StringBuilder();

			if (xIntroMsg==null)
				xIntroMsg="The following unexpected error occurred:";

			if (xNewErrorMsg==null)
				xNewErrorMsg=e.Message;

			oSB.AppendFormat("{0}\n\n{1}  {2}\n{3}  {4}\n\n{5}",
				xIntroMsg,"Name:",e.GetType().Name ,
				"Description:",xNewErrorMsg,
				"Would you like to copy this information to the clipboard?");

			//display error detail to user - prompt to copy to clipboard
			string xProductName = Application.ProductName;

            //prompt
			DialogResult oChoice = MessageBox.Show(oSB.ToString(),xProductName, 
				MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

			if (oChoice == DialogResult.Yes)
			{
				//build error detail with stack trace
				oSB.Remove(0,oSB.Length);

				if(e.InnerException == null)
					oSB.AppendFormat("{0}\r\n{1}  {2}\r\n{3}  {4}\r\n{5}{6}\r\n{7}{8}",
						xIntroMsg,"Name:",e.GetType().Name,
						"Description:",xNewErrorMsg, 
						"Internal Description:", "NONE",
						"Source",e.StackTrace);
				else
					oSB.AppendFormat("{0}\r\n{1}  {2}\r\n{3}  {4}\r\n{5}{6}\r\n{7}{8}",
						xIntroMsg,"Name:",e.GetType().Name,
						"Description:",xNewErrorMsg, 
						"Internal Description:", e.InnerException.Message,
						"Source",e.StackTrace);

				//copy to clipboard
				Clipboard.SetDataObject(oSB.ToString(),true);
			}
		}
		

		/// <summary>
		/// shows the specified exception in English- displays
		/// the specified message instead of the actual error message -
		/// we need this method for errors that occur in the LMP.Resource class,
		/// when the resources can't be localized
		/// </summary>
		/// <param name="e"></param>
		/// <param name="xNewErrorMsg"></param>
		public static void ShowInEnglish(Exception e, string xNewErrorMsg)
		{
			ShowInEnglish(e,xNewErrorMsg,null);
		}
		

		/// <summary>
		/// shows the specified exception in English-
		/// we need this method for errors that occur in the LMP.Resource class,
		/// when the resources can't be localized
		/// </summary>
		/// <param name="e"></param>
		public static void ShowInEnglish(Exception e)
		{
			ShowInEnglish(e,null,null);
		}
	}
}
