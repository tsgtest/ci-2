﻿using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("CISFNet")]
[assembly: AssemblyDescription("CISFNet")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("The Sackett Group, Inc.")]
[assembly: AssemblyProduct("Contact Integration")]
[assembly: AssemblyCopyright("© 1990-2013 The Sackett Group, Inc.")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(true)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("fba149bd-7f65-43da-a2ab-9e63b180292c")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("2.6.0.3018")]
[assembly: AssemblyFileVersion("2.6.0.3018")]
