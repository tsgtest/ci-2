using System;
using System.Reflection;
using System.Resources;
using System.Threading;
using System.Globalization;
using System.Text;
using System.Windows.Forms;

namespace LMP
{
	/// <summary>
	/// Summary description for Resources.
	/// </summary>

	///<summary>
	///manages resource retrieval of LMP resources
	///</summary>
	public static class Resources
	{
		private const string mpResourceAssemblyName = "mpLMPResources";
		private const string mpBaseName = "LMP.LMPResources";

		private static Assembly m_oResFileAssembly;
		private static ResourceManager m_oResManager;
		//private static string m_xCulture = "en-US";
		private static int m_iCulture = 1033;
		
		/// <summary>
		/// private constructor - prevent instance creation
		/// </summary>
		static Resources()
		{
			try
			{
				//load resource assembly
				m_oResFileAssembly = Assembly.Load(mpResourceAssemblyName);

				//create resource manager
				m_oResManager = new ResourceManager(mpBaseName,m_oResFileAssembly);
			}
			catch(Exception e)
			{
				Error.ShowInEnglish(e);
			}
		}

		///<summary>
		///gets/sets the LMP culture
		///</summary>
		public static int CultureID
		{
			get
			{
				if(m_iCulture == 0)
					return 1033;
				else
					return m_iCulture;
			}
		}

		/// <summary>
		/// returns the name of the current culture
		/// </summary>
		public static string CultureName
		{
			get
			{
				//get culture info from specified culture id
				CultureInfo oCultureInfo= new CultureInfo(m_iCulture);
				return oCultureInfo.Name;
			}
		}

		/// <summary>
		/// returns the full name of the current culture
		/// </summary>
		public static string CultureEnglishName
		{
			get
			{
				//get culture info from specified culture id
				CultureInfo oCultureInfo= new CultureInfo(m_iCulture);
				return oCultureInfo.EnglishName;
			}
		}

		/// <summary>
		/// sets the culture from the specified culture ID
		/// </summary>
		/// <param name="iCultureID"></param>
		public static void SetCulture(int iCultureID)
		{
			try
			{
				//set culture only if different 
				//than current culture
				if(m_iCulture != iCultureID)
				{
					//set culture for current thread
					Thread.CurrentThread.CurrentUICulture = 
						new System.Globalization.CultureInfo(iCultureID);
			
					m_iCulture = iCultureID;
				}
			}
			catch(System.Exception e)
			{
				Error.ShowInEnglish(e);
			}
		}

		/// <summary>
		/// sets the culture from the specified culture string
		/// </summary>
		/// <param name="xCulture"></param>
		public static void SetCulture(string xCulture)
		{
			try
			{
				//get culture info from specified culture string
				CultureInfo oCultureInfo= new CultureInfo(xCulture);

				//set culture only if different 
				//than current culture
				if(m_iCulture != oCultureInfo.LCID)
				{
					//set culture for current thread
					Thread.CurrentThread.CurrentUICulture = oCultureInfo;
			
					m_iCulture = oCultureInfo.LCID;
				}
			}
			catch(System.Exception e)
			{
				Error.ShowInEnglish(e);
			}
		}


		//		public static string Culture
//		{
//			get
//			{
//				return m_xCulture;
//			}
//		
//			set
//			{
//				try
//				{
//					//set culture only if different 
//					//than current culture
//					if(Culture == value)
//						return;
//
//					LMP.Trace.WriteInfo("Culture=" + value);
//
//					//set culture for current thread
//					Thread.CurrentThread.CurrentUICulture = 
//						new System.Globalization.CultureInfo(value);
//			
//					m_xCulture = value;
//				}
//				catch(System.Exception e)
//				{
//					Error.ShowInEnglish(e);
//				}
//			}
//		}


		/// <summary>
		/// returns the localized string with the specified name in the specified culture
		/// </summary>
		/// <param name="xName">name of the localized string to return</param>
		/// <param name="culture">windows culture id string, e.g. en-US for US English</param>
		/// <returns></returns>
		public static string GetLangString(string xName,string culture)
		{
			CultureInfo oCultureInfo  = new CultureInfo(culture);
			if (m_oResManager != null)
			{
				try
				{
					return m_oResManager.GetString(xName,oCultureInfo);
				}
				catch(System.Exception e)
				{
					Error.ShowInEnglish(e);
					return null;
				}
			}
			else
				return null;
}

		/// <summary>
		/// returns the localized string with the specified name in the current culture
		/// </summary>
		/// <param name="xName"></param>
		/// <returns></returns>
		public static string GetLangString(string xName)
		{
			if (m_oResManager != null)
			{
				try
				{
					return m_oResManager.GetString(xName);
				}
				catch(System.Exception e)
				{
					Error.ShowInEnglish(e);
					return null;
				}
			}
			else
				return null;
		}

        /// <summary>
        /// returns the text content of a file resource as a string.
        /// </summary>
        /// <param name="xName"></param>
        /// <returns></returns>
        public static string GetResourceFileContent(string xName)
        {
			if (m_oResManager != null)
			{
				try
				{
                    return (string)m_oResManager.GetObject(xName);
                }
                catch (System.Exception e)
                {
                    Error.ShowInEnglish(e);
                    return null;
                }
            }
            else
                return null;
        }
	}
}
