﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CISFNet
{
    public partial class AuthenticationForm : Form
    {
        public AuthenticationForm(string xSecurityToken)
        {
            InitializeComponent();

            this.txtSecurityToken.Text = xSecurityToken;
        }

        public string UserName
        {
            get { return this.txtUserName.Text; }
        }

        public string Password
        {
            get { return this.txtPwd.Text; }
        }

        public string SecurityToken
        {
            set { this.txtSecurityToken.Text = value; }
            get { return this.txtSecurityToken.Text; }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.Save();
        }
    }
}
