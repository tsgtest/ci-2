﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.Collections;
using System.Net;
using System.Web;
using System.Web.Services.Protocols;
using CISFNet.Salesforce;

namespace CISFNet
{

    //[ComVisible(true)]
    //[Guid("{274A2A63-084E-4d65-AE8F-5C15D2101E6E}")]
    public interface ICISF
    {
        bool Connect(string xSecurityToken, string xUserName, string xPassword);

        //contact interface methods
        string[,] GetListings(string attribute1, string value1,
            string attribute2, string value2,
            string attribute3, string value3,
            string attribute4, string value4,
            string sortField);
        string[,] GetAddresses(string entityID);
        string[,] GetDetails(string contactID, string commaDelimitedFieldNames);
        string[,] GetAlternateAddressDetails(string AddressID);
        string[,] GetAlternateNumbers(string contactID, string AddressID, string xNumberType);
        //string[,] GetNameDetails(string xContactID);
        //string[,] GetPhoneNumbers(string xContactID, string xAddressID);
        //string[,] GetFaxNumbers(string xContactID, string xAddressID);
        //string[,] GetEmailAddresses(string xContactID);
        //string[,] GetAddressDetails(string xEntityID, string xAddressID);

        ////Company/Account interface methods
        //string[,] GetCompanyListings(string xAttribute1, string xValue1,
        //               string xAttribute2, string xValue2,
        //               string xAttribute3, string xValue3,
        //               string xAttribute4, string xValue4,
        //               string xSortField);
        //string[,] GetCompanyDetails(string xCompanyID);
        //string[,] GetCompanyPhoneNumbers(string xContactID, string xAddressID);
        //string[,] GetCompanyFaxNumbers(string xContactID, string xAddressID);
        //string[,] GetCompanyEmailAddresses(string xContactID);
    }

    [ComVisible(true)]
    [Guid("4D9EB454-D5B3-45ac-AF8E-41348DEC27B1"),
    ClassInterface(ClassInterfaceType.None)]
    public class Application: ICISF
    {
        static Salesforce.SforceService service;
        string currentContactDetails;
        string currentContactID;

        #region *********************constructors*********************
        static Application()
        {
            service = new CISFNet.Salesforce.SforceService();

            //timeout after 20 seconds 
            service.Timeout = 20000;

        }
        public Application() { }
        #endregion
        #region *********************enumerations*********************
        public enum StoreType
        {
            ContactEntity = 1,
            CompanyEntity = 2
        }
        #endregion
        #region *********************methods*********************
        /// <summary>
        /// connects to the Salesforce web service-
        /// returns true if connected, false otherwise
        /// </summary>
        /// <returns></returns>
        public bool Connect(string xSecurityToken, string xUserName, string xPassword)
        {
            Salesforce.LoginResult lr = null;

            //Try logging in   
            try
            {
                //GLOG : 5712 : ceh
                System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
                lr = service.login(xUserName, xPassword + xSecurityToken);
            }
            catch (SoapException e)
            {
                if (e.Message.StartsWith("INVALID_LOGIN"))
                {
                    MessageBox.Show("Could not login to Salesforce. Username, password, or security token invalid.",
                        "Salesforce", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                                        //return False to indicate that the login was not successful 
                    return false;
                }
                else
                {
                    LMP.Error.Show(e);
                }
            }

            try 
	        {	        
                // Check if the password has expired 
                if (lr.passwordExpired)
                {
                    Console.WriteLine("An error has occurred. Your password has expired.");
                    return false;
                }

                /** Once the client application has logged in successfully, it will use
                    * the results of the login call to reset the endpoint of the service
                    * to the virtual server instance that is servicing your organization
                    */
                // Save old authentication end point URL
                String authEndPoint = service.Url;
                // Set returned service endpoint URL
                service.Url = lr.serverUrl;

                /** The sample client application now has an instance of the SforceService
                    * that is pointing to the correct endpoint. Next, the sample client
                    * application sets a persistent SOAP header (to be included on all
                    * subsequent calls that are made with SforceService) that contains the
                    * valid sessionId for our login credentials. To do this, the sample
                    * client application creates a new SessionHeader object and persist it to
                    * the SforceService. Add the session ID returned from the login to the
                    * session header
                    */
                service.SessionHeaderValue = new Salesforce.SessionHeader();
                service.SessionHeaderValue.sessionId = lr.sessionId;

                // Return true to indicate that we are logged in, pointed  
                // at the right URL and have our security token in place.     
                return true;
            }
            catch (System.Exception oE)
            {
                LMP.Error.Show(oE);
                return false;
            }
        }

        public string[,] GetListings(string xAttribute1, string xValue1, string xAttribute2, string xValue2, string xAttribute3, string xValue3, string xAttribute4, string xValue4, string xSortField)
        {
            string[,] listings = null;
            string sql = "SELECT LastName, FirstName, Contact.Account.Name, ID FROM Contact";

            //substitute actual field name when neeeded - Company is CI-specific,
            //AccountName is not fully qualified
            if (xAttribute1 == "Company" || xAttribute1 == "AccountName")
            {
                xAttribute1 = "Contact.Account.Name";
            }

            if (xAttribute2 == "Company" || xAttribute2 == "AccountName")
            {
                xAttribute2 = "Contact.Account.Name";
            }

            if (xAttribute3 == "Company" || xAttribute3 == "AccountName")
            {
                xAttribute3 = "Contact.Account.Name";
            }

            if (xAttribute4 == "Company" || xAttribute4 == "AccountName")
            {
                xAttribute4 = "Contact.Account.Name";
            }

            //DisplayName is CI-specific
            if (xSortField == "DisplayName")
            {
                xSortField = "LastName, FirstName";
            }

            //create WHERE clause
            StringBuilder sb = new StringBuilder();

            if (!string.IsNullOrEmpty(xAttribute1))
            {
                sb.AppendFormat("{0} LIKE '{1}'", xAttribute1, xValue1);
            }

            if (!string.IsNullOrEmpty(xAttribute2))
            {
                if (sb.Length > 0)
                    sb.AppendFormat("{0}", " AND ");

                sb.AppendFormat("{0} LIKE '{1}'", xAttribute2, xValue2);
            }

            if (!string.IsNullOrEmpty(xAttribute3))
            {
                if (sb.Length > 0)
                    sb.AppendFormat("{0}", " AND ");

                sb.AppendFormat("{0} LIKE '{1}'", xAttribute3, xValue3);
            }

            if (!string.IsNullOrEmpty(xAttribute4))
            {
                if (sb.Length > 0)
                    sb.AppendFormat("{0}", " AND ");

                sb.AppendFormat("{0} LIKE '{1}'", xAttribute4, xValue4);
            }

            string where = "";

            if (!string.IsNullOrEmpty(sb.ToString()))
            {
                where = " WHERE " + sb.ToString();
            }

            string orderBy = " ORDER BY " + xSortField;
            sql += where + orderBy;

            try
            {
                //query salesforce
                Salesforce.QueryResult qr = service.query(sql);
                bool done = false;

                if (qr.size > 0)
                {
                    listings = new string[qr.size, 5];

                    //cycle through returned set
                    while (!done)
                    {
                        Salesforce.sObject[] records = qr.records;
                        for (int i = 0; i < records.Length; i++)
                        {
                            Salesforce.sObject con = records[i];

                            listings[i, 0] = con.Any[3].InnerText;
                            listings[i, 1] = con.Any[0].InnerText + ", " + con.Any[1].InnerText;
                            listings[i, 2] = con.Any[2].InnerText.StartsWith("Account") ? con.Any[2].InnerText.Substring(7) : con.Any[2].InnerText;
                            listings[i, 3] = con.Any[0].InnerText;
                            listings[i, 4] = con.Any[1].InnerText;
                        }

                        if (qr.done)
                        {
                            done = true;
                        }
                        else
                        {
                            qr = service.queryMore(qr.queryLocator);
                        }
                    }
                }

                return listings;
            }
            catch (Exception e)
            {
                LMP.Error.Show(e);
                return null;
            }
        }
        public string[,] GetAddresses(string xEntityID)
        {
            //GLOG : 15886 : ceh
            string sql = "SELECT ID, Name FROM OnePlace__C_Address__c WHERE OnePlace__Contact__c = '" + xEntityID + "' ORDER BY Name";

            string[,] Addresses = null;

            try
            {
                //query salesforce
                Salesforce.QueryResult qr = service.query(sql);

                if (qr.size > 0)
                {
                    //cycle through returned set
                    if (qr.records.Count() > 0)
                    {
                        Salesforce.sObject con = null;
                        Addresses = new string[qr.records.Count() + 2, 2];
                        Addresses[0, 0] = "MailingAddress";
                        Addresses[0, 1] = "Mailing";
                        Addresses[1, 0] = "OtherAddress";
                        Addresses[1, 1] = "Other";

                        for (int j = 2; j < qr.size + 2; j++)
                        {
                            con = qr.records[j - 2];
                            Addresses[j, 0] = con.Any[0].InnerText;
                            Addresses[j, 1] = con.Any[1].InnerText;
                        }
                    }
                }
                else
                {
                    Addresses = new string[2, 2] { { "MailingAddress", "Mailing" }, { "OtherAddress", "Other" } };
                }
            }
            catch (System.Exception e)
            {
                LMP.Error.Show(e);
            }

            return Addresses;

        }

        public void DescribeGlobalSample()
        {
            try
            {
                // Make the describeGlobal() call 
                DescribeGlobalResult dgr = service.describeGlobal();

                // Get the sObjects from the describe global result
                DescribeGlobalSObjectResult[] sObjResults = dgr.sobjects;

                // Write the name of each sObject to the console
                for (int i = 0; i < sObjResults.Length; i++)
                {
                    Console.WriteLine(sObjResults[i].name);
                }
            }
            catch (SoapException e)
            {
                Console.WriteLine("An unexpected error has occurred: " +
                    e.Message + "\n" + e.StackTrace);
            }
        }

        public void DescribeOjectType(string objType)
        {
            DescribeSObjectResult res = service.describeSObject(objType);

            for (int i = 0; i < res.fields.Count(); i++)
            {
                Console.WriteLine(res.fields[i].name);
            }
        }

        //public string[,] GetNameDetails(string xContactID)
        //{
        //    return GetDetails("LastName", "FirstName", "Salutation", "Company", "Title", "Department");
        //}

        public string[,] GetDetails(string contactID, string commaDelimitedFieldNames)
        {
            RetrieveDetailsIfNecessary(contactID);

            string[] fields = commaDelimitedFieldNames.Split(',');
            string[,] details = new string[fields.Length, 2];

            for (int i = 0; i < fields.Length; i++)
                details[i, 0] = fields[i];

            int upperBound = details.GetUpperBound(0);

            for (int i = 0; i <= upperBound; i++)
            {
                details[i, 1] = GetDetailValue(details[i, 0]);
            }
            return details;
        }

        //GLOG : 15999 : ceh
        public string[,] GetAlternateNumbers(string contactID, string AddressID, string xNumberType)
        {
            string sql = "";

            switch (xNumberType.ToUpper())
            {
                case "PHONE":
                    sql = "SELECT Phone__c, " +
                                 "Mobile__c " +
                                 "FROM OnePlace__C_Address__c WHERE ID = '" + AddressID + "'";
                    break;
                case "FAX":
                    sql = "SELECT Fax__c " +
                                 "FROM OnePlace__C_Address__c WHERE ID = '" + AddressID + "'";
                    break;
                case "EMAIL":
                    sql = "SELECT OnePlace__Email__c " +
                                 "FROM OnePlace__C_Address__c WHERE ID = '" + AddressID + "'";
                    break;
            }

            string[,] details = new string[2, 2];

            try
            {
                //query salesforce
                Salesforce.QueryResult qr = service.query(sql);

                if (qr.size > 0)
                {
                    //should only be one record per address ID supplied
                    if (qr.records.Count() > 0)
                    {
                        Salesforce.sObject con = qr.records[0];
                        if (xNumberType.ToUpper() == "PHONE")
                        {
                                details[0, 0] = "Phone";
                                details[0, 1] = con.Any[0].InnerText;
                                details[1, 0] = "Mobile";
                                details[1, 1] = con.Any[1].InnerText;
                        }
                        else
                        {
                            details[0, 0] = xNumberType;
                            details[0, 1] = con.Any[0].InnerText;
                            details[1, 0] = "";
                            details[1, 1] = "";
                        }
                    }
                }
            }
            catch (System.Exception e)
            {
                LMP.Error.Show(e);
            }

            return details;
        }

        //GLOG : 15886 : ceh
        public string[,] GetAlternateAddressDetails(string xAddressID)
        {
            string sql = "SELECT OnePlace__Street1__c, " +
                         "OnePlace__Street2__c, " +
                         "Street_3__c, " +
                         "OnePlace__City__c, " +
                         "OnePlace__State__c, " +
                         "OnePlace__Postal_Code__c, " +
                         "OnePlace__Country__c " +
                         "FROM OnePlace__C_Address__c WHERE ID = '" + xAddressID + "'";

            string[,] AddressDetails = new string[5,2];

            try
            {
                //query salesforce
                Salesforce.QueryResult qr = service.query(sql);

                if (qr.size > 0)
                {
                    //should only be one record per address ID supplied
                    if (qr.records.Count() > 0)
                    {
                        Salesforce.sObject con = qr.records[0];

                        string xFullAddress = con.Any[0].InnerText +
                                              (con.Any[1].InnerText != "" ? "\r\n" + con.Any[1].InnerText : "") +
                                              (con.Any[2].InnerText != "" ? "\r\n" + con.Any[2].InnerText : "");

                        AddressDetails[0, 0] = "";
                        AddressDetails[0, 1] = xFullAddress;
                        AddressDetails[1, 0] = "";
                        AddressDetails[1, 1] = con.Any[3].InnerText;
                        AddressDetails[2, 0] = "";
                        AddressDetails[2, 1] = con.Any[4].InnerText;
                        AddressDetails[3, 0] = "";
                        AddressDetails[3, 1] = con.Any[5].InnerText;
                        AddressDetails[4, 0] = "";
                        AddressDetails[4, 1] = con.Any[6].InnerText;
                    }
                }
            }
            catch (System.Exception e)
            {
                LMP.Error.Show(e);
            }

            return AddressDetails;
        }

        //public string[,] GetNameDetailsOLD(string xContactID)
        //{
        //    string[,] entities = null;
        //    string sql = "SELECT LastName, FirstName, Salutation, Contact.Account.Name, Title, Department FROM Contact WHERE ID='" + xContactID + "'";

        //    try
        //    {
        //        //query salesforce
        //        Salesforce.QueryResult qr = service.query(sql);
        //        bool done = false;

        //        if (qr.size > 0)
        //        {
        //            entities = new string[qr.size, 10];

        //            //cycle through returned set
        //            while (!done)
        //            {
        //                Salesforce.sObject[] records = qr.records;
        //                for (int i = 0; i < records.Length; i++)
        //                {
        //                    Salesforce.sObject con = records[i];

        //                    for (int j = 0; j < con.Any.Count(); j++)
        //                    {
        //                        entities[i, j] = con.Any[j].InnerText;
        //                    }
        //                }

        //                if (qr.done)
        //                {
        //                    done = true;
        //                }
        //                else
        //                {
        //                    qr = service.queryMore(qr.queryLocator);
        //                }
        //            }
        //        }
        //    }
        //    catch (System.Exception e)
        //    {
        //        LMP.Error.Show(e);
        //    }

        //    return entities;
        //}

        //public string[,] GetPhoneNumbers(string xContactID, string xAddressID)
        //{
        //    throw new NotImplementedException();
        //}

        //public string[,] GetFaxNumbers(string xContactID, string xAddressID)
        //{
        //    throw new NotImplementedException();
        //}

        //public string[,] GetEmailAddresses(string xContactID)
        //{
        //    throw new NotImplementedException();
        //}
        //public string[,] GetAddressDetails(string xContactID, string xAddressID)
        //{
        //    string[,] entities = null;
        //    string sql = null;

        //    if (xAddressID == "Mailing")
        //    {
        //        sql = "SELECT MailingStreet, MailingCity, MailingState, MailingPostalCode, MailingCountry FROM Contact WHERE ID='" + xContactID + "'";
        //    }
        //    else
        //    {
        //        sql = "SELECT OtherStreet, OtherCity, OtherState, OtherPostalCode, OtherCountry FROM Contact WHERE ID='" + xContactID + "'";
        //    }

        //    try
        //    {
        //        //query salesforce
        //        Salesforce.QueryResult qr = service.query(sql);
        //        bool done = false;

        //        if (qr.size > 0)
        //        {
        //            entities = new string[qr.size, 5];

        //            //cycle through returned set
        //            while (!done)
        //            {
        //                Salesforce.sObject[] records = qr.records;
        //                for (int i = 0; i < records.Length; i++)
        //                {
        //                    Salesforce.sObject con = records[i];

        //                    for (int j = 0; j < con.Any.Count(); j++)
        //                    {
        //                        entities[i, j] = con.Any[j].InnerText;
        //                    }
        //                }

        //                if (qr.done)
        //                {
        //                    done = true;
        //                }
        //                else
        //                {
        //                    qr = service.queryMore(qr.queryLocator);
        //                }
        //            }
        //        }
        //    }
        //    catch (System.Exception e)
        //    {
        //        LMP.Error.Show(e);
        //    }

        //    return entities;
        //}

        private void RetrieveDetailsIfNecessary(string contactID)
        {
            if (currentContactID != contactID)
            {
                RetrieveDetails(contactID);
            }
        }
        /// <summary>
        /// returns the value of the specified field for
        /// the current contact detail
        /// </summary>
        /// <param name="field"></param>
        /// <returns></returns>
        private string GetDetailValue(string field)
        {
            int pos = currentContactDetails.IndexOf("|" + field + "=");

            if (pos > -1)
            {
                pos = pos + field.Length + 2;

                int pos2 = currentContactDetails.IndexOf("|", pos);

                return currentContactDetails.Substring(pos, pos2 - pos);
            }
            else
            {
                return null;
            }
        }

        private void RetrieveDetails(string xContactID)
        {
            string sql = "SELECT Contact.Account.Name";
            StringBuilder sb = new StringBuilder();

            //get all field names
            DescribeSObjectResult res = service.describeSObject("Contact");

            for (int i = 0; i < res.fields.Count(); i++)
            {
                sb.AppendFormat(",{0}", res.fields[i].name);
            }

            sql += sb.ToString();
            sql += " FROM Contact WHERE ID = '" + xContactID + "'";

            try
            {
                //query salesforce
                Salesforce.QueryResult qr = service.query(sql);

                if (qr.size > 0)
                {
                    sb = new StringBuilder();

                    //cycle through returned set
                    if(qr.records.Count() > 0)
                    {
                        Salesforce.sObject con = qr.records[0];

                        //trim first 7 characters, as all companies seem
                        //to begin with the word "Account" - bug?
                        sb.Append("|Company=" + con.Any[0].InnerText.Substring(7));

                        for (int j = 0; j < res.fields.Count(); j++)
                        {
                            sb.AppendFormat("|{0}={1}", res.fields[j].name, con.Any[j + 1].InnerText);
                        }
                    }
                }
            }
            catch (System.Exception e)
            {
                LMP.Error.Show(e);
            }

            currentContactID = xContactID;
            currentContactDetails = sb.ToString();
        }
        #endregion
    }

}
