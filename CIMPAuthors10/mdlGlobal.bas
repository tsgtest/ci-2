Attribute VB_Name = "mdlGlobal"
Option Explicit

Private Declare Function GetTickCount Lib "kernel32" () As Long
Private Declare Function WritePrivateProfileSection Lib "kernel32" Alias _
    "WritePrivateProfileSectionA" (ByVal lpAppName As String, ByVal _
    lpString As String, ByVal lpFileName As String) As Long
Public Declare Function GetUserName Lib "advapi32.dll" Alias "GetUserNameA" ( _
    ByVal lpBuffer As String, nSize As Long) As Long
Private Declare Sub OutputDebugString Lib "kernel32" _
  Alias "OutputDebugStringA" _
  (ByVal lpOutputString As String)
 
Public Const ciMP10InternalID As Integer = 111

Public g_vCols(3, 1) As Variant
Public g_iSortCol As ciListingCols
Public g_oSession As CIO.CSessionType
Public g_oError As CIO.CError
Public g_oIni As CIO.CIni
Public g_oGlobals As CIO.CGlobals
Public g_oConstants As CIO.CConstants

Public g_xBackend As String

Public Sub CorrectTDBComboMismatch(oTDBCombo As TDBCombo, iReposition As Integer)
'resets the tdb combo value to the previous match -
'this procedure should be called in the
'TDBCombo Mismatch even procedure only
    Dim bytStart As Byte
    
    iReposition = False
    
    With oTDBCombo
'       get current selection start
        bytStart = .SelStart
        
'       reset the text to the current list text
        If .ListField = Empty Then
            .Text = .Columns(0)
        Else
            .Text = .Columns(.ListField)
        End If
        
'       return selection to original selection
        .SelStart = Max(CDbl(bytStart - 1), 0)
        .SelLength = Len(.Text)
    End With
End Sub

Function Max(i As Double, j As Double) As Double
    If i > j Then
        Max = i
    Else
        Max = j
    End If
End Function

Function Min(i As Double, j As Double) As Double
    If i > j Then
        Min = j
    Else
        Min = i
    End If
End Function

Public Function bValidateBoundTDBCombo(oTDBCombo As TDBCombo) As Boolean
'returns FALSE if invalid author has been specified, else TRUE
    With oTDBCombo
        If .BoundText = "" Then
            MsgBox "Invalid entry.  Please select an item " & _
                   "from the list.", vbExclamation, g_oSession.AppTitle
        Else
'           set text of control equal to the
'           display text of the selected row
            .Text = .Columns(.ListField)
            bValidateBoundTDBCombo = True
        End If
    End With
End Function

'Function CurrentTick() As Long
'    CurrentTick = GetTickCount()
'End Function
'
Function IsEven(dNum As Double) As Boolean
    If dNum / 2 = CLng(dNum / 2) Then
        IsEven = True
    End If
End Function

'Function ElapsedTime(lStartTick As Long) As Single
''returns the time elapsed from lStartTick-
''precision in milliseconds
'    ElapsedTime = Format((CurrentTick() - lStartTick) / 1000, "#,##0.0000")
'End Function

Public Function xSubstitute(ByVal xString As String, _
                     ByVal xSearch As String, _
                     ByVal xReplace As String) As String
'replaces xSearch in
'xString with xReplace -
'returns modified xString -
'NOTE: SEARCH IS NOT CASE SENSITIVE

    Dim iSeachPos As Integer
    Dim xNewString As String
    
'   get first char pos
    iSeachPos = InStr(UCase(xString), _
                      UCase(xSearch))
    
'   remove switch all chars
    While iSeachPos
        xNewString = xNewString & _
            Left(xString, iSeachPos - 1) & _
            xReplace
        xString = Mid(xString, iSeachPos + Len(xSearch))
        iSeachPos = InStr(UCase(xString), _
                          UCase(xSearch))
    
    Wend
    
    xNewString = xNewString & xString
    xSubstitute = xNewString
End Function

Function CurrentTick() As Long
    CurrentTick = GetTickCount()
End Function

Function ElapsedTime(lStartTick As Long) As Single
'returns the time elapsed from lStartTick-
'precision in milliseconds
    ElapsedTime = Format((CurrentTick() - lStartTick) / 1000, "#,##0.0000")
End Function

Public Sub DebugPrint(xOutput As String)
    Debug.Print xOutput
    OutputDebugString xOutput
End Sub
