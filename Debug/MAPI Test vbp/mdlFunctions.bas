Attribute VB_Name = "mdlFunctions"
Public g_oMAPISession As MAPI.Session

Public Function Connect() As Long
'connects to a MAPI session
    Dim xDesc As String
    Dim lErr As Long
    Dim xProfile As String
    
    
'   log on if not previously logged on
    On Error GoTo ProcError
    If g_oMAPISession Is Nothing Then
        Set g_oMAPISession = New MAPI.Session
        
        On Error Resume Next
        g_oMAPISession.Logon xProfile, , True, False, , True
        
        If Err.Number <> 0 Then
            lErr = Err.Number
            xDesc = Err.Description
            
            'set connected property to FALSE
            m_bIsConnected = False
            If lErr = CdoE_USER_CANCEL Then
                'don't alert - user knows he/she cancelled
                On Error GoTo ProcError
            End If
            
        Else
'            'set connected property to TRUE
'           listStores
        End If
    End If
    DoEvents

    Exit Function
ProcError:
    MsgBox "[Connect]" & vbTab & vbTab & Err.Number & ": " & Err.Description
    Exit Function
End Function


Function xStringToArray(ByVal xString As String, _
                        arrP() As String, _
                        Optional iNumCols As Integer = 1, _
                        Optional xSep As String = ",") As String

    Dim iNumSeps As Long
    Dim iNumEntries As Long
    Dim iSepPos As Long
    Dim xEntry As String
    Dim i As Long
    Dim j As Long
    
'   get ubound of arrP - count delimiter
'   then divide by iNumCols
    iNumSeps = lCountChrs(xString, xSep)
    iNumEntries = (iNumSeps + 1) / iNumCols
    
    ReDim arrP(iNumEntries - 1, iNumCols - 1)
    
    For i = 0 To iNumEntries - 1
        For j = 0 To iNumCols - 1
'           get next entry & store
            iSepPos = InStr(xString, xSep)
            If iSepPos Then
                xEntry = Left(xString, iSepPos - 1)
            Else
                xEntry = xString
            End If
            arrP(i, j) = xEntry
            
'           remove entry from xstring
            If iSepPos Then _
                xString = Mid(xString, iSepPos + 1)
        Next j
    Next i
End Function

Function lCountChrs(xSource As String, xSearch As String) As Long
'returns the number of instances
'of xSearch in xSource

    Dim iPos As Long
    Dim l As Long
    
    iPos = InStr(xSource, xSearch)
    While iPos
        l = l + 1
        iPos = InStr(iPos + 1, xSource, xSearch)
    Wend
    lCountChrs = l
End Function


Public Sub SetFilterSort(oMsgs As MAPI.Messages, ByVal bExcludePrivate As Boolean)
'sets filter for supplied messages collection
    Const ciMAPISensitivityFieldID As Long = &H360003
    Dim i As Integer
    Dim oMAPIFilter As MAPI.MessageFilter
    Dim xLimitToIPMContacts As String
    
    If oMsgs.Count = 0 Then
        Exit Sub
    End If
    
    Set oMAPIFilter = oMsgs.Filter
        
    oMsgs.Filter.Type = "IPM.Contact"
        
    DoEvents
    
    If bExcludePrivate Then
        'include only those contacts not marked as private
        oMAPIFilter.Fields(ciMAPISensitivityFieldID) = 0
    End If
    
    On Error Resume Next
    oMsgs.Sort CdoAscending, CLng("3604510")
    DoEvents
    
    
    Exit Sub
ProcError:
    MsgBox "mdlFunctions.SetFilterSort"
End Sub

