VERSION 5.00
Begin VB.Form frmMAPI 
   Caption         =   "MAPI Testing"
   ClientHeight    =   3795
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   9435
   LinkTopic       =   "Form1"
   ScaleHeight     =   3795
   ScaleWidth      =   9435
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton cmdClose 
      Caption         =   "Close"
      Height          =   465
      Left            =   7845
      TabIndex        =   4
      Top             =   3210
      Width           =   1380
   End
   Begin VB.ListBox lstContacts 
      Height          =   2400
      Left            =   5490
      TabIndex        =   3
      Top             =   420
      Width           =   3750
   End
   Begin VB.CommandButton cmdLoadContacts 
      Caption         =   "Get Contacts"
      Height          =   555
      Left            =   4245
      TabIndex        =   2
      Top             =   1410
      Width           =   1125
   End
   Begin VB.ListBox lstContactFolders 
      Height          =   2400
      Left            =   240
      TabIndex        =   1
      Top             =   420
      Width           =   3840
   End
   Begin VB.CommandButton cmdLoadFolders 
      Caption         =   "Load Folders"
      Height          =   510
      Left            =   255
      TabIndex        =   0
      Top             =   3180
      Width           =   1395
   End
   Begin VB.Label Label3 
      Caption         =   "Contacts:"
      Height          =   240
      Left            =   5490
      TabIndex        =   6
      Top             =   150
      Width           =   2925
   End
   Begin VB.Label Label2 
      Caption         =   "Available Contact Folders:"
      Height          =   255
      Left            =   270
      TabIndex        =   5
      Top             =   150
      Width           =   4365
   End
End
Attribute VB_Name = "frmMAPI"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public g_xFolders As String

Private Sub cmdClose_Click()
    Unload Me
End Sub

Private Sub cmdLoadContacts_Click()
    GetFolderListings
End Sub

Private Sub cmdLoadFolders_Click()
    Set g_oMAPISession = Nothing
    Me.lstContactFolders.Clear
    Me.lstContacts.Clear
    Connect
    Me.Label2.Caption = "Loading available Contact folders..."
    Me.Label3.Caption = "Contacts:"
    ListContactFolders
    Me.Label2.Caption = "Available Contact Folders for " & g_oMAPISession.CurrentUser & ":"
End Sub

Sub ListContactFolders()
    Dim oMAPIFolder As MAPI.Folder
    Dim x As String
    Dim oIS As MAPI.InfoStore
    Dim xClass As String
    Dim i As Integer
    Dim xFolder() As Variant
    
    For Each oIS In g_oMAPISession.InfoStores
        For Each oMAPIFolder In oIS.RootFolder.Folders
            On Error Resume Next
            xClass = UCase$(oMAPIFolder.Fields(CdoPR_CONTAINER_CLASS))

            If InStr(xClass, "CONTACT") > 0 Then
                Me.lstContactFolders.AddItem oIS & ": " & oMAPIFolder.Name
                g_xFolders = g_xFolders & oMAPIFolder.FolderID & "|" & oMAPIFolder.StoreID & "|"
                
                i = i + 1
            End If
        Next oMAPIFolder
    Next oIS

End Sub

Sub GetFolderListings()
    Dim oMsgs As MAPI.Messages
    Dim oMsg As MAPI.Message
    Dim oMAPIFolder As MAPI.Folder
    Dim oFolder As MAPI.Folder
    Dim i As Long
    Dim lNumMsgs As Long
    Dim Cancel As Boolean
    Dim bFolderACL As Variant
    Dim bIsOwner As Boolean
    Dim iSensitivity As Integer
    Dim xClass As String
    Dim xFolders() As String
    
    On Error GoTo ProcError

    'build array
    xStringToArray g_xFolders, xFolders(), 2, "|"

    On Error Resume Next
    'get folder
    Set oMAPIFolder = g_oMAPISession.GetFolder(xFolders(Me.lstContactFolders.ListIndex, 0), _
                                               xFolders(Me.lstContactFolders.ListIndex, 1))

    If oMAPIFolder Is Nothing Then
        MsgBox "Could not get Contacts", vbExclamation
        Exit Sub
    Else
        Set oFolder = oMAPIFolder.Folders("Contacts")
    End If
    
'    'get type of messages contained in folder
'    On Error Resume Next
'    xClass = UCase$(oMAPIFolder.Fields(CdoPR_CONTAINER_CLASS))
'    On Error GoTo ProcError
'
'    If InStr(xClass, "CONTACT") = 0 Then
'        'there are no contacts in this folder - exit
'        MsgBox "There are no contacts in this folder", vbInformation
'        Exit Sub
'    End If
    
    'get folder messages
    On Error Resume Next
    Set oMsgs = oFolder.Messages

    If oMsgs.Count = 0 Then
        MsgBox "No contacts found", vbExclamation
        Exit Sub
    End If
        
'    'Get folder ACL
'    bFolderACL = oFolder.Fields(CdoPR_ACCESS)
    
    'Check if the modify flag is set, because
    'only a owner can change the folder properties
'    bIsOwner = bFolderACL And MAPI_ACCESS_MODIFY
    
'   set filter - exclude private contacts
'   if user is not the owner of the folder
    SetFilterSort oMsgs, False
    
    lNumMsgs = oMsgs.Count
    
    
    If lNumMsgs Then
         Me.Label3.Caption = "Contacts (" & lNumMsgs & "):"
'       create a new listing for each record returned
        If Not Cancel Then
            Dim l As Long
            For Each oMsg In oMsgs
                If UCase$(oMsg.Type) <> "IPM.DISTLIST" Then
                    'AddListing oMsg, oListings, oFolder.UNID, xFolderPath
                    Me.lstContacts.AddItem oMsg.Fields(CLng("3604510"))
                    i = i + 1
                    If Cancel Then
                        Exit Sub
                    End If
                End If
            Next oMsg
        End If
    End If

Exit Sub
ProcError:
    MsgBox "[GetFolderListings]" & vbTab & vbTab & Err.Number & ": " & Err.Description
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Set g_oMAPISession = Nothing
End Sub
