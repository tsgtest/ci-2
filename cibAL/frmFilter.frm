VERSION 5.00
Begin VB.Form frmFilter 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   " Find"
   ClientHeight    =   4560
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6765
   Icon            =   "frmFilter.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4560
   ScaleWidth      =   6765
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.CommandButton btnSearch 
      Caption         =   "O&K"
      Default         =   -1  'True
      Height          =   400
      Left            =   5355
      TabIndex        =   11
      ToolTipText     =   "Execute search"
      Top             =   3480
      Width           =   1200
   End
   Begin VB.ComboBox cbxSortBy 
      Appearance      =   0  'Flat
      Height          =   315
      ItemData        =   "frmFilter.frx":000C
      Left            =   480
      List            =   "frmFilter.frx":000E
      Style           =   2  'Dropdown List
      TabIndex        =   10
      Top             =   3735
      Width           =   4000
   End
   Begin VB.CommandButton btnRemove 
      Caption         =   "Cl&ear Search"
      Height          =   400
      Left            =   5385
      TabIndex        =   12
      ToolTipText     =   "Clear search criteria"
      Top             =   1440
      Visible         =   0   'False
      Width           =   1200
   End
   Begin VB.Frame fraFilter 
      Caption         =   "Display Contacts Where:"
      Height          =   3135
      Left            =   270
      TabIndex        =   0
      Top             =   210
      Width           =   4485
      Begin VB.TextBox txtFilterField 
         Height          =   325
         Index           =   3
         Left            =   225
         TabIndex        =   8
         Top             =   2535
         Width           =   4000
      End
      Begin VB.TextBox txtFilterField 
         Height          =   325
         Index           =   2
         Left            =   225
         TabIndex        =   6
         Top             =   1875
         Width           =   4000
      End
      Begin VB.TextBox txtFilterField 
         Height          =   325
         Index           =   1
         Left            =   225
         TabIndex        =   4
         Top             =   1230
         Width           =   4000
      End
      Begin VB.TextBox txtFilterField 
         Height          =   325
         Index           =   0
         Left            =   225
         TabIndex        =   2
         Top             =   570
         Width           =   4000
      End
      Begin VB.Label lblFilterField 
         Caption         =   "##"
         Height          =   330
         Index           =   3
         Left            =   255
         TabIndex        =   7
         Top             =   2325
         Width           =   1650
      End
      Begin VB.Label lblFilterField 
         Caption         =   "##"
         Height          =   330
         Index           =   2
         Left            =   270
         TabIndex        =   5
         Top             =   1665
         Width           =   1650
      End
      Begin VB.Label lblFilterField 
         Caption         =   "##"
         Height          =   330
         Index           =   1
         Left            =   270
         TabIndex        =   3
         Top             =   1020
         Width           =   1650
      End
      Begin VB.Label lblFilterField 
         Caption         =   "##"
         Height          =   330
         Index           =   0
         Left            =   270
         TabIndex        =   1
         Top             =   360
         Width           =   1650
      End
   End
   Begin VB.CommandButton btnCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   400
      Left            =   5355
      TabIndex        =   13
      Top             =   3960
      Width           =   1200
   End
   Begin VB.Label Label2 
      Caption         =   "&Sort By:"
      Height          =   300
      Left            =   510
      TabIndex        =   9
      Top             =   3510
      Width           =   585
   End
End
Attribute VB_Name = "frmFilter"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public Enum ciMAPIFilterActions
    ciMAPIFilterAction_Cancel = 0
    ciMAPIFilterAction_Search = 1
    ciMAPIFilterAction_OutlookSearch = 2
    ciMAPIFilterAction_ClearSearch = 3
End Enum

Private m_bCancelled As Boolean
Private m_bSearchNow As Boolean
Private xMsg As String
Private m_oBackend As CIO.ICIBackend
Private m_iFilterAction As ciMAPIFilterActions
Private m_iSortCol As ciListingCols

Public Property Set Backend(oNew As CIO.ICIBackend)
    Set m_oBackend = oNew
End Property

Private Property Let Action(iNew As ciMAPIFilterActions)
    m_iFilterAction = iNew
End Property

Public Property Get Action() As ciMAPIFilterActions
    Action = m_iFilterAction
End Property

Public Property Let SortColumn(iNew As ciListingCols)
    m_iSortCol = iNew
End Property

Public Property Get SortColumn() As ciListingCols
    SortColumn = m_iSortCol
End Property

Private Sub btnCancel_Click()
    Me.Hide
    Action = ciMAPIFilterAction_Cancel
End Sub

Private Sub btnRemove_Click()
    Me.ResetFilter
    Action = ciMAPIFilterAction_Search
End Sub

Private Sub btnSearch_Click()
    Action = ciMAPIFilterAction_Search
    On Error Resume Next
    Me.Hide
    DoEvents
End Sub

Private Sub Form_Activate()
    Dim i As Integer
    
'   select contents of first criterion
    With Me.txtFilterField(0)
        .SetFocus
        .SelStart = 0
        .SelLength = Len(.Text)
    End With
    
'   set sort field
    With Me.cbxSortBy
'        For i = 0 To .ListCount - 1
'            If .ItemData(i) = m_oBackend.Filter.SortColumn Then
'                .ListIndex = i
'                Exit For
'            End If
'        Next i
        .ListIndex = m_iSortCol
    End With
    
'TODO:
'   ensure correct sort field - can
'   be changed elsewhere in interface
End Sub

Private Sub Form_Load()
    Dim i As Integer
    Dim iDisplay As Integer
    Dim iNumFields As Integer
    Dim oFilter As CIO.CFilter
    
    Action = ciMAPIFilterAction_Cancel
    
    With m_oBackend.Filter
        iNumFields = .CountFields
        
        For i = 1 To iNumFields
            With .FilterFields(i)
'               set caption for each filter field control
                Me.lblFilterField(i - 1).Caption = "&" & .Name & ":"
                'Me.txtFilterField(i - 1).Text = .Value
            End With
            
        Next i
        
'       show only those controls corresponding
'       to existing filter fields
        For i = 1 To 4
            Me.lblFilterField(i - 1).Visible = i <= iNumFields
            Me.txtFilterField(i - 1).Visible = i <= iNumFields
        Next i
        
        With Me.fraFilter
'           adjust frame for number of filter field controls
            If iNumFields < 4 Then
                .Height = .Height - ((4 - iNumFields) * (.Height / 4)) + _
                          (4 - iNumFields) * 100
            End If
        End With
    End With


End Sub

Public Sub ResetFilter()
    Dim i As Integer
    On Error Resume Next
    With Me.txtFilterField
        For i = .LBound To .UBound
            .Item(i).Text = ""
        Next i
    End With
End Sub

Private Sub txtFilterField_GotFocus(Index As Integer)
    With Me.txtFilterField(Index)
        .SelStart = 0
        .SelLength = Len(.Text)
    End With
End Sub
