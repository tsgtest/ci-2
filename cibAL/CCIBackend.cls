VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "CCIBackend"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Option Explicit

Enum ciMAPI_IS_Addresses
    ciMAPI_IS_Address_Default = 1
    ciMAPI_IS_Address_Business = 2
    ciMAPI_IS_Address_Home = 3
    ciMAPI_IS_Address_Other = 4
End Enum

Implements CIO.ICIBackend

Private Declare Function GetForegroundWindow Lib "user32" () As Long

Private Const ciOutlookContactPropSet As String = "{0420060000000000C000000000000046}"
Private Const ciOutlookBusStreetID As String = "0x8045"
Private Const ciOutlookBusCityID As String = "0x8046"
Private Const ciOutlookBusStateID As String = "0x8047"
Private Const ciOutlookBusZipID As String = "0x8048"
Private Const ciOutlookBusCountryID As String = "0x8049"
    
Private Const ciOutlookEMail1Address As String = "0x8083"
Private Const ciOutlookEMail2Address As String = "0x8093"
Private Const ciOutlookEMail3Address As String = "0x80A3"
    
Private m_iID As Integer
Private WithEvents m_oEvents As CIO.CEventGenerator
Attribute m_oEvents.VB_VarHelpID = -1
Private m_oSession As outlook.NameSpace
Private m_oMAPISession As MAPI.Session
Private m_oOutApp As outlook.Application

Private Sub Class_Initialize()
    Set m_oEvents = New CIO.CEventGenerator
End Sub

Private Sub Class_Terminate()
    Set m_oEvents = Nothing
End Sub

Private Sub ICIBackend_EditContact(oListing As CIO.CListing)

End Sub

Private Function ICIBackend_Events() As CIO.CEventGenerator
    Set ICIBackend_Events = m_oEvents
End Function

Private Function ICIBackend_GetFilterFields() As CIO.CFilterFields
    Dim oSess As CIO.CSession
    Set oSess = New CIO.CSession
    Set ICIBackend_GetFilterFields = oSess.GetFilterFields(m_iID)
End Function

Private Function ICIBackend_GetFolderListings(oFolder As CIO.CFolder, Optional oFilter As CIO.CFilter) As CIO.CListings
    Dim oMFolder As outlook.MAPIFolder
    Dim oItem As Object
    Dim oContactItem As outlook.ContactItem
    Dim oDistItem As outlook.DistListItem
    Dim i As Long
    Dim lFieldID1 As Long
    Dim lFieldID2 As Long
    Dim iFormat As Integer
    Dim lNumItems As Long
    Dim xID As String
    Dim xDisplay As String
    Dim xCompany As String
    Dim xFName As String
    Dim xLName As String
    Dim iListType As ciListingType
    Dim Cancel As Boolean
    Dim oIni As CIni
    Dim xDesc As String
    Dim oSession As CIO.CSession
    Dim oItems As outlook.Items
    Dim oListings As CIO.CListings
    Dim oConst As CConstants

'        For Each oItem In oMFolder.Items
''            Debug.Print oItem.Subject
'        Next
'        Exit Function
'    Dim xFName As String

    On Error GoTo ProcError

    Set oIni = New CIni
    Set oSession = New CIO.CSession
    Set oMFolder = m_oSession.GetFolderFromID(oFolder.ID)
    Set oItems = oMFolder.Items

'   specify filter if supplied
'    If Not oFilter Is Nothing Then
'        Dim oFF As CIO.CFilterField

        'cycle through each filter field in the filter
'        For Each oFF In oFilter.FilterFields
'            If oFF.Value <> Empty Then
'                On Error Resume Next
'                oItems.Restrict .Fields(CLng(oFF.ID)) = oFF.Value
'                If Err Then
'                    'raise error
'                    On Error GoTo ProcError
'                    Err.Raise ciErr_CouldNotSetFilterField, , _
'                        "Could not add field with ID = '" & oFF.ID & _
'                        "' and value = '" & oFF.Value & "' to filter."
'                End If
'                On Error GoTo ProcError
'            End If
'        Next

'   clear out listings
    Set oListings = New CIO.CListings

    lNumItems = oItems.Count

    If lNumItems Then
'       test if the first contact is of type "Contact" or
'       "Distribution List" - the assumption here is that all
'       messages in a folder are of the same type - if it's a
'       contact, then we're in a contact folder.  we're not using
'       .filter.type = "IPM.CONTACT" because it doesn't allow us to
'       retrieve contacts of type eg "IPM.CONTACT.LITTLER EMPLOYEES"

        If oMFolder.DefaultItemType <> olContactItem And _
            oMFolder.DefaultItemType <> olDistributionListItem Then
            Exit Function
        End If

        m_oEvents.RaiseBeforeListingsRetrieved lNumItems, Cancel

'       create a new listing for each record returned
        If Not Cancel Then
            Set oConst = New CConstants
Dim l As Long
l = CurrentTick()
Dim hWnd As Long
hWnd = GetForegroundWindow()

outlook.ActiveExplorer.Display
While GetForegroundWindow() <> hWnd
    DoEvents
Wend

'           cycle through items
'            For Each oItem In oMFolder.Items
            For Each oItem In outlook.ActiveExplorer.Selection
'               clear out vars
                xID = Empty
                xCompany = Empty
                xDisplay = Empty
                xLName = Empty
                xFName = Empty

'               get either contact item or distribution list item
                On Error Resume Next
                If TypeOf oItem Is outlook.ContactItem Then
                    Set oContactItem = oItem
                    Set oDistItem = Nothing
                Else
                    Set oContactItem = Nothing
                    Set oDistItem = oItem
                End If

                If Not oContactItem Is Nothing Then
                    With oContactItem
                        xCompany = .CompanyName
                        xDisplay = .FileAs
                        xLName = .LastName
                        xFName = .FirstName

'                       get UNID
                        xID = oFolder.UNID & oConst.UNIDSep & .Fields(.EntryID)

'                       get listing type
                        iListType = ciListingType_Person

'                       add listing
                        oListings.Add xID, xDisplay, xCompany, iListType
                    End With
                End If
                i = i + 1
                m_oEvents.RaiseAfterListingAdded i + 1, lNumItems, Cancel
'                With oItems(i + 1)
'                    On Error Resume Next
'
'
'                    If oContactItem Is Nothing Then
'                   add entry to list
'                    xLName = UCase(.Type)
'                    xCompany = .Fields()

'                    xDisplay = .Fields(mapi.CdoPR_SURNAME)
'                    If Len(xDisplay) Then
'                        xFName = .Fields(mapi.CdoPR_GIVEN_NAME)
'                        If Len(xFName) Then
'                            xDisplay = xDisplay & ", " & xFName
'                        End If
'                    Else
'                        xDisplay = xFName
'                    End If
'
'                    If Len(xDisplay) = 0 Then
'                        xDisplay = xCompany
'                        If Len(xDisplay) = 0 Then
'                            xDisplay = .Fields(Outlook.CdoPR_SUBJECT)
'                        End If
'                    End If
'
'                    Dim oConst As CConstants
'                    Set oConst = New CConstants
'                    xID = oFolder.UNID & oConst.UNIDSep & .Fields(mapi.CdoPR_ENTRYID)
'                    On Error GoTo 0
'
''                   get listing type based on message type
'                    If InStr(xLName, "IPM.CONTACT") = 0 Then
'                        iListType = ciListingType_Person
'                    ElseIf InStr(xLName, "IPM.DISTLIST") = 0 Then
'                        iListType = ciListingType_Group
'                    End If
'
'                    oListings.Add xID, xDisplay, xCompany, iListType
'                End With


                If Cancel Then
                    Exit Function
                End If
            Next oItem
            m_oEvents.RaiseAfterListingsRetrieved i
        End If
    Else
'       return empty listings collection
        Set oListings = New CIO.CListings
    End If
MsgBox ElapsedTime(l)
    Set ICIBackend_GetFolderListings = oListings
    Exit Function
ProcError:
    Err.Raise Err.Number, "CCIBackend.GetFolderListings", Err.Description
    Exit Function
End Function

Private Function ICIBackend_GetStoreListings(oStore As CIO.CStore, Optional oFilter As CIO.CFilter) As CIO.CListings
    Err.Raise ciErr_NotImplemented, _
              Err.Source & ":CCIBackend.GetStoreListings", _
              "ICIBackend_GetStoreListings is not implemented for CIMAPI.CCIBackend."
End Function

Private Function ICIBackend_GetSubFolders_OUTLOOK(oFolder As CIO.CFolder) As CIO.CFolders
''retrieves root folders in store oStore -
''returns them as a CFolders collection
    Dim oMAPIFolder As outlook.MAPIFolder
    Dim oRootFolder As outlook.MAPIFolder
    Dim oCIFolder As CIO.CFolder
    Dim oCIFolders As CIO.CFolders
    Dim oSession As CIO.CSession

    On Error GoTo ProcError
    Set oSession = New CIO.CSession
    Set oRootFolder = m_oSession.GetFolderFromID(oFolder.ID)
    Set oCIFolders = New CIO.CFolders

    For Each oMAPIFolder In oRootFolder.Folders
'TODO: Allow customization to see all folders or just contact folders
        If IsContactFolder(oMAPIFolder) Then
            Set oCIFolder = oSession.GetFolder(m_iID & UNIDSep & _
                oFolder.StoreID & UNIDSep & oMAPIFolder.EntryID, oMAPIFolder.Name)
            oCIFolders.Add oCIFolder
        End If
    Next oMAPIFolder
    Set ICIBackend_GetSubFolders_OUTLOOK = oCIFolders
    Exit Function
    
ProcError:
    Err.Raise Err.Number, _
              Err.Source & ":CCIBackend.GetSubFolders", _
              Err.Description
End Function

Private Function ICIBackend_GetSubFolders(oFolder As CIO.CFolder) As CIO.CFolders
'retrieves root folders in store oStore -
'returns them as a CFolders collection
    Dim oMAPIFolder As outlook.MAPIFolder
    Dim oRootFolder As outlook.MAPIFolder
    Dim oCIFolder As CIO.CFolder
    Dim oCIFolders As CIO.CFolders
    Dim oSession As CIO.CSession

    On Error GoTo ProcError
    
    Set oSession = New CIO.CSession
    
    'get the corresponding mapi folder
    Set oRootFolder = m_oSession.GetFolderFromID(oFolder.ID)
    
    Set oCIFolders = New CIO.CFolders

    For Each oMAPIFolder In oRootFolder.Folders
        Set oCIFolder = oSession.GetFolder(m_iID & UNIDSep & oFolder.StoreID & _
                        UNIDSep & oMAPIFolder.EntryID, oMAPIFolder.Name)
        oCIFolders.Add oCIFolder
    Next oMAPIFolder
    Set ICIBackend_GetSubFolders = oCIFolders
    Exit Function
    
ProcError:
    Err.Raise Err.Number, _
              Err.Source & ":CCIBackend.GetSubFolders", _
              Err.Description
End Function

Private Sub ICIBackend_Initialize(iID As Integer)
    m_iID = iID
End Sub

Private Property Get ICIBackend_Name() As String
    Dim oSess As CIO.CSession
    Set oSess = New CIO.CSession
    ICIBackend_Name = oSess.GetBackendName(m_iID)
End Property

Private Property Get ICIBackend_SupportsFolders() As Boolean
    ICIBackend_SupportsFolders = True
End Property

Private Property Get ICIBackend_SupportsMultipleStores() As Boolean
    ICIBackend_SupportsMultipleStores = True
End Property

'used to refer back to the ci.ini in the functions of this class
Private Property Let ICIBackend_ID(iID As Integer)
    m_iID = iID
End Property

Private Property Get ICIBackend_ID() As Integer
    ICIBackend_ID = m_iID
End Property

Private Function ICIBackend_GetAddresses(oListing As CIO.CListing) As CIO.CAddresses
'returns a CAddresses collection that represents
'the available addresses for the supplied listing
    Dim oAddress As CIO.CAddress
    Dim oAddresses As CIO.CAddresses
    
    Set oAddresses = New CIO.CAddresses
    
'   4 types of addresses - Default, Business, Home, Other
    Set oAddress = New CAddress
    With oAddress
        .Name = "Mailing"
        .UNID = oListing.UNID & UNIDSep & ciMAPI_IS_Address_Default
    End With
    
    oAddresses.Add oAddress
    
    Set oAddress = New CAddress
    With oAddress
        .Name = "Business"
        .UNID = oListing.UNID & UNIDSep & ciMAPI_IS_Address_Business
    End With
    
    oAddresses.Add oAddress
    
    Set oAddress = New CAddress
    With oAddress
        .Name = "Home"
        .UNID = oListing.UNID & UNIDSep & ciMAPI_IS_Address_Home
    End With
    
    oAddresses.Add oAddress
    
    Set oAddress = New CAddress
    With oAddress
        .Name = "Other"
        .UNID = oListing.UNID & UNIDSep & ciMAPI_IS_Address_Other
    End With
    
    oAddresses.Add oAddress
    Set ICIBackend_GetAddresses = oAddresses
    Exit Function
ProcError:
    Err.Raise Err.Number, "CCIBackend.GetAddresses", Err.Description
End Function

Private Function ICIBackend_GetContact(oListing As CIO.CListing, oAddress As CIO.CAddress, iIncludeData As ciContactData) As CIO.CContact
'   returns a contact from a MAPI infostore-
''   returns contact w/o address if
''   AllowEmptyAddress = TRUE and no address
'
'    Dim Filter As mapi.MessageFilter
'    Dim oMsgs As mapi.Messages
'    Dim oMsg As mapi.Message
'    Dim oContact As CIO.CContact
'    Dim xAddress As String
'    Dim bHasMultPhones As Boolean
'    Dim oConst As CIO.CConstants
'    Dim i As Integer
'    Dim xTempPhone As String
'    Dim xRetPhone As String
'    Dim xRetExt As String
'    Dim xRetFax As String
'    Dim xRetFaxExt As String
'    Dim xRetEAddress As String
'    Dim xDefDisplayName As String
'    Dim oAddr As CIO.CAddress
'
'    Set oConst = New CIO.CConstants
'
'    On Error Resume Next
'    Set oMsg = m_oSession.GetMessage(oListing.ID, oListing.StoreID)
'    On Error GoTo 0
'
'    If oMsg Is Nothing Then
'        Exit Function
'    Else
'        Set oContact = New CIO.CContact
'    End If
'
''   fill Contact fields with DB field values
'    With oMsg.Fields
'        On Error Resume Next
'        oContact.AddressType = oAddress.ID
'        oContact.MiddleName = .Item(mapi.CdoPR_MIDDLE_NAME)
'        oContact.Prefix = .Item(mapi.CdoPR_DISPLAY_NAME_PREFIX)
'        oContact.Suffix = .Item(973406238)  'id is known - find constant represntation
'        oContact.LastName = .Item(mapi.CdoPR_SURNAME)
'        oContact.FirstName = .Item(mapi.CdoPR_GIVEN_NAME)
'        oContact.Company = .Item(mapi.CdoPR_COMPANY_NAME)
'        oContact.Department = .Item(mapi.CdoPR_DEPARTMENT_NAME)
'        oContact.Title = .Item(mapi.CdoPR_TITLE)
'        oContact.Initials = .Item(mapi.CdoPR_INITIALS)
'        xDefDisplayName = .Item(mapi.CdoPR_DISPLAY_NAME)
'        oContact.DisplayName = oListing.BuildDisplayName(oContact, xDefDisplayName)
'
'        Select Case oAddress.ID
'            Case ciMAPI_IS_Address_Default
'                If iIncludeData And ciContactData_Address Then
'                    oContact.AddressType = "Mailing"
'                    oContact.Street1 = .Item(mapi.CdoPR_STREET_ADDRESS)
'                    oContact.City = .Item(mapi.CdoPR_LOCALITY)
'                    oContact.State = .Item(mapi.CdoPR_STATE_OR_PROVINCE)
'                    oContact.ZipCode = .Item(mapi.CdoPR_POSTAL_CODE)
'                    oContact.Country = .Item(mapi.CdoPR_COUNTRY)
'                End If
'
''               get phone/fax only if specified, and ini
''               designates which phone/fax to get -
''               CI is offering firms the ability to associate
''               a phone/fax number with the mailing address
'                Dim xMailAddrPhone As String
'                Dim xMailAddrFax As String
'                Dim oIni As CIO.CIni
'
'                Set oIni = New CIO.CIni
'                xMailAddrPhone = oIni.GetIni("Backend" & oListing.BackendID, "MailingAddressPhone")
'                xMailAddrFax = oIni.GetIni("Backend" & oListing.BackendID, "MailingAddressFax")
'
'                If (iIncludeData And ciContactData_Phone) And (xMailAddrPhone <> Empty) Then _
'                    oContact.Phone = .Item(xMailAddrPhone)
'                If (iIncludeData And ciContactData_Fax) And (xMailAddrFax <> Empty) Then _
'                    oContact.Fax = .Item(xMailAddrFax)
'            Case ciMAPI_IS_Address_Business
'                If iIncludeData And ciContactData_Address Then
'                    oContact.AddressType = "Business"
'                    oContact.Street1 = .Item(ciOutlookContactPropSet & _
'                                             ciOutlookBusStreetID)
'                    oContact.City = .Item(ciOutlookContactPropSet & _
'                                             ciOutlookBusCityID)
'                    oContact.State = .Item(ciOutlookContactPropSet & _
'                                             ciOutlookBusStateID)
'                    oContact.ZipCode = .Item(ciOutlookContactPropSet & _
'                                             ciOutlookBusZipID)
'                    oContact.Country = .Item(ciOutlookContactPropSet & _
'                                             ciOutlookBusCountryID)
'                End If
'
'                If iIncludeData And ciContactData_Phone Then
''                   get phones
'                    oContact.Phone = .Item(mapi.CdoPR_BUSINESS_TELEPHONE_NUMBER)
'
'                    i = 0
''                   test for multiple business-related phones -
''                   the method below looks unnecessary, but it is-
''                   exchange will only create the field if there is
''                   a value for it.  so, we can't simply test the
''                   value of field.  rather, since exchange will raise
''                   an error in such a case, suppress error checking
''                   and see whether xTempPhone has anything in it.
'                    If oContact.Phone <> "" Then _
'                        i = i + 1
'
'                    On Error Resume Next
'                    xTempPhone = Empty
'                    xTempPhone = .Item(mapi.CdoPR_BUSINESS2_TELEPHONE_NUMBER)
'                    If xTempPhone <> Empty Then _
'                        i = i + 1
'
'                    xTempPhone = Empty
'                    xTempPhone = .Item(mapi.CdoPR_COMPANY_MAIN_PHONE_NUMBER)
'                    If xTempPhone <> Empty Then _
'                        i = i + 1
'
'                    xTempPhone = Empty
'                    xTempPhone = .Item(mapi.CdoPR_PRIMARY_TELEPHONE_NUMBER)
'                    If xTempPhone <> Empty Then _
'                        i = i + 1
'
''                   there are multiple phones if i > 1-
''                   set .phone = "" to force a prompt for phones below
'                    If i > 1 Then
'                        Set oConst = New CIO.CConstants
'                        oContact.Phone = oConst.MultiplePhones
'                    End If
'                End If
'
'                If iIncludeData And ciContactData_Fax Then _
'                    oContact.Fax = .Fields(mapi.CdoPR_BUSINESS_FAX_NUMBER)
'
'            Case ciMAPI_IS_Address_Home
'                If iIncludeData And ciContactData_Address Then
'                    oContact.AddressType = "Home"
'                    oContact.Street1 = .Item(mapi.CdoPR_HOME_ADDRESS_STREET)
'                    oContact.City = .Item(mapi.CdoPR_HOME_ADDRESS_CITY)
'                    oContact.State = .Item(mapi.CdoPR_HOME_ADDRESS_STATE_OR_PROVINCE)
'                    oContact.Country = .Item(mapi.CdoPR_HOME_ADDRESS_COUNTRY)
'                    oContact.ZipCode = .Item(mapi.CdoPR_HOME_ADDRESS_POSTAL_CODE)
'                End If
'
'                If iIncludeData And ciContactData_Phone Then
'                    oContact.Phone = .Item(mapi.CdoPR_HOME_TELEPHONE_NUMBER)
'
'                    i = 0
''                   test for multiple home-related phones
'                    If oContact.Phone <> "" Then _
'                        i = i + 1
'
'                    On Error Resume Next
'                    xTempPhone = Empty
'                    xTempPhone = .Item(mapi.CdoPR_HOME2_TELEPHONE_NUMBER)
'                    If xTempPhone <> Empty Then _
'                        i = i + 1
'
''                   there are multiple phones if i > 1-
''                   set .phone = "" to force a prompt for phones below
'                    If i > 1 Then
'                        oContact.Phone = oConst.MultiplePhones
'                    End If
'                End If
'
'                If iIncludeData And ciContactData_Fax Then _
'                    oContact.Fax = .Item(mapi.CdoPR_HOME_FAX_NUMBER)
'
'            Case ciMAPI_IS_Address_Other
'                If iIncludeData And ciContactData_Address Then
'                    oContact.AddressType = "Other"
'                    oContact.Street1 = .Item(mapi.CdoPR_OTHER_ADDRESS_STREET)
'                    oContact.City = .Item(mapi.CdoPR_OTHER_ADDRESS_CITY)
'                    oContact.State = .Item(mapi.CdoPR_OTHER_ADDRESS_STATE_OR_PROVINCE)
'                    oContact.Country = .Item(mapi.CdoPR_OTHER_ADDRESS_COUNTRY)
'                    oContact.ZipCode = .Item(mapi.CdoPR_OTHER_ADDRESS_POSTAL_CODE)
'                End If
'
'                If iIncludeData And ciContactData_Phone Then _
'                    oContact.Phone = .Item(mapi.CdoPR_OTHER_TELEPHONE_NUMBER)
'
''               outlook uses primary fax field as backend for "other fax"
'                If iIncludeData And ciContactData_Fax Then _
'                    oContact.Fax = .Item(mapi.CdoPR_PRIMARY_FAX_NUMBER)
'        End Select
'
''       get email address
'        If iIncludeData And ciContactData_EAddress Then
'            On Error Resume Next
'            oContact.EMailAddress = .Item(ciOutlookContactPropSet & _
'                                          ciOutlookEMail1Address)
'            On Error GoTo 0
'
'            i = 0
''           test for multiple e addresses
'            If oContact.EMailAddress <> "" Then _
'                i = i + 1
'
'            On Error Resume Next
'            xTempPhone = Empty
'            xTempPhone = .Item(ciOutlookContactPropSet & _
'                                 ciOutlookEMail2Address)
'            If xTempPhone <> Empty Then
'                i = i + 1
'                If i = 1 Then
'                    oContact.EMailAddress = _
'                        .Item(ciOutlookContactPropSet & _
'                                ciOutlookEMail2Address)
'                End If
'            End If
'
'            On Error Resume Next
'            xTempPhone = Empty
'            xTempPhone = .Item(ciOutlookContactPropSet & _
'                                 ciOutlookEMail3Address)
'            If xTempPhone <> Empty Then
'                i = i + 1
'                If i = 1 Then
'                    oContact.EMailAddress = _
'                        .Item(ciOutlookContactPropSet & _
'                                ciOutlookEMail3Address)
'                End If
'            End If
'
'
''           there are multiple e addresses if i > 1-
''           set to 'Multiple Phone' to force prompt below
'            If i > 1 Then _
'                oContact.EMailAddress = oConst.MultiplePhones
'        End If
'    End With
'
'    If iIncludeData And ciContactData_Address Then
''       return contact if address is not empty
''       or if OnEmptyAddress = ciEmptyAddress_ReturnEmpty
'        On Error GoTo 0
'        With oContact
''           test for existence of address-related info
'            xAddress = .Street1 & .City & _
'                .State & .Country & .ZipCode
'        End With
'
'''       if there is no address related info,
'''       act according to options
''        If xAddress = "" Then
''            Select Case Options.OnEmptyAddresses
''                Case ciEmptyAddress_ReturnEmpty
''                    Set CreateContact_MAPI_IS = oContact
''                Case ciEmptyAddress_Warn
''                    Set CreateContact_MAPI_IS = Nothing
''                Case ciEmptyAddress_PromptForExisting
'''                   test to see if there are any addresses
'''                   if so, prompt for existing
''                    If HasAddresses_MAPI_IS(oMsg) Then
''                        Set CreateContact_MAPI_IS = oContact
'''                       prompt for addresses
''                        Set oAddr = GetAddress()
''                        Cancel = (oAddr Is Nothing)
''
'''                       vAddressType = ciNoAddressType if user
'''                       cancelled out of address browse dialog
''                        If Not Cancel Then
''                            vAddressID = oAddr.ID
''                            vAddressType = oAddr.TypeID
''                            Set oContact = CreateContact_MAPI_IS(vID, _
''                                                            vStoreID, _
''                                                            vAddressType, _
''                                                            vAddressID, _
''                                                            Cancel)
''
''                            Set CreateContact_MAPI_IS = oContact
''                            Exit Function
''                        Else
''                            Cancel = False
''                        End If
''                    Else
''                        Alert_NoAddresses Cancel
''
''                        If Cancel Then
''                            Set CreateContact_MAPI_IS = Nothing
''                            Exit Function
''                        End If
''                    End If
''            End Select
''        End If
'    End If
'
'''   prompt for phone numbers if necessary and specified-
'''   oContact will be empty if there are no phones, or if there
'''   are multiple phones - code above sets to
'''   ciMultPhones if there are multiple
'''   users will ALWAYS get prompted if multiple phones exist
''    With oContact
''        If iIncludeData And ciContactData_Phone And _
''                Options.PromptForMissingPhones And .Phone = "" Then
''            lRet = GetPhones_MAPI_IS(oMsg, _
''                                    oContact, _
''                                    ciPhoneType_Voice, _
''                                    False, _
''                                    "", _
''                                    Cancel, _
''                                    xRetPhone, _
''                                    xRetExt)
''
''            If lRet Then
''                Err.Raise lRet
''            End If
''
''            If Cancel Then
''                Set oContact = Nothing
''                Exit Function
''            End If
''
''            .Phone = xRetPhone
''            .PhoneExtension = xRetExt
''        ElseIf .Phone = ciMultPhones And _
''                Options.PromptForMultiplePhones Then
''            .Phone = GetPhones_MAPI_IS(oMsg, _
''                                    oContact, _
''                                    ciPhoneType_Voice, _
''                                    True, _
''                                    vAddressType, _
''                                    Cancel, _
''                                    xRetPhone, _
''                                    xRetExt)
''
''            If Cancel Then
''                Set oContact = Nothing
''                Exit Function
''            End If
''
''            .Phone = xRetPhone
''            .PhoneExtension = xRetExt
''        End If
''
''        If iIncludeData And ciContactData_Fax And _
''            Options.PromptForMissingPhones And .Fax = "" Then
''            lRet = GetPhones_MAPI_IS(oMsg, _
''                                    oContact, _
''                                    ciPhoneType_Fax, _
''                                    False, _
''                                    "", _
''                                    Cancel, _
''                                    xRetFax, _
''                                    xRetFaxExt)
''
''            If lRet Then
''                Err.Raise lRet
''            End If
''
''            If Cancel Then
''                Set oContact = Nothing
''                Exit Function
''            End If
''
''            .Fax = xRetFax
''        End If
''
''        If Options.IncludeEAddress And _
''                Options.PromptForMissingPhones And .EMailAddress = "" Then
''            lRet = GetEAddresses_MAPI_IS(oMsg, _
''                                         oContact, _
''                                         ciPhoneType_Voice, _
''                                         False, _
''                                         "", _
''                                         Cancel, _
''                                         xRetEAddress)
''
''           If lRet Then
''                Err.Raise lRet
''            End If
''
''            If Cancel Then
''                Set oContact = Nothing
''                Exit Function
''            End If
''
''            .EMailAddress = xRetEAddress
''        ElseIf .EMailAddress = ciMultPhones And _
''                Options.PromptForMultiplePhones Then
''            lRet = GetEAddresses_MAPI_IS(oMsg, _
''                                           oContact, _
''                                           ciPhoneType_Voice, _
''                                           True, _
''                                           vAddressType, _
''                                           Cancel, _
''                                           xRetEAddress)
''
''            If Cancel Then
''                Set oContact = Nothing
''                Exit Function
''            End If
''
''            .EMailAddress = xRetEAddress
''        End If
''
'''       get custom fields for contact if specified
''        If Options.IncludeCustomFields Then
''            GetCustFields_MAPI_IS Me.Backend, oMsg, .CustomFields
''        End If
''    End With
'
'    Set ICIBackend_GetContact = oContact
End Function

Private Function IsContactFolder(oFolder As outlook.MAPIFolder) As Boolean
    IsContactFolder = InStr(UCase(oFolder.DefaultMessageClass), "IPM.CONTACT") > 0
End Function

Private Function ICIBackend_GetFolders(oStore As CIO.CStore) As CIO.CFolders
''retrieves root folders in store oStore -
''returns them as a CFolders collection
    Dim oMAPIFolder As outlook.MAPIFolder
    Dim oRootFolder As outlook.MAPIFolder
    Dim oCIFolder As CIO.CFolder
    Dim oCIFolders As CIO.CFolders
    Dim oSession As CIO.CSession

    On Error GoTo ProcError
    Set oSession = New CIO.CSession
    
    Set oCIFolders = New CIO.CFolders
    Set oRootFolder = m_oSession.GetFolderFromID(oStore.ID)
    For Each oMAPIFolder In oRootFolder.Folders
'TODO: Allow customization to see all folders or just contact folders
        If IsContactFolder(oMAPIFolder) Then
            Set oCIFolder = oSession.GetFolder(oStore.UNID & UNIDSep & oMAPIFolder.EntryID, oMAPIFolder.Name)
            oCIFolders.Add oCIFolder
        End If
    Next oMAPIFolder
    Set ICIBackend_GetFolders = oCIFolders
    Exit Function
    
ProcError:
    Err.Raise Err.Number, _
              Err.Source & ":CCIBackend.GetFolders", _
              Err.Description
End Function

Private Function ICIBackend_GetStores() As CIO.CStores
''retrieves infostores in current session -
''returns them as a CStores collection
'    Dim oIS As mapi.InfoStore
    Dim oStore As CIO.CStore
    Dim oStores As CIO.CStores
    Dim oCISession As CIO.CSession
    Dim oFolder As outlook.MAPIFolder
    
    'start mapi session if necessary
    If m_oSession Is Nothing Then
        Connect
    End If
    
    Set oCISession = New CIO.CSession
    
    Set oStores = New CIO.CStores

    For Each oFolder In m_oSession.Folders
        Set oStore = oCISession.GetStore(m_iID & UNIDSep & oFolder.EntryID, oFolder.Name)
        oStores.Add oStore
    Next oFolder
    Set ICIBackend_GetStores = oStores
    Exit Function
    
ProcError:
    Err.Raise Err.Number, _
              Err.Source & ":CCIBackend.GetStores", _
              Err.Description
End Function

Private Function Connect() As Long
    Dim lRet As Long
    Dim User As String
    Dim xDesc As String
    
    On Error GoTo ProcError
    
'   log on if not previously logged on
    If m_oSession Is Nothing Then
'       get a MapiSession object
        Set m_oOutApp = GetObject("", "Outlook.Application")
        Set m_oSession = m_oOutApp.Session
            
'       log on if there isn't a valid
'       session somewhere on the system
        On Error Resume Next
        m_oSession.Logon , , , True
        If Err.Number Then
'           other kind of error - raise
            Dim lErr As Long
            lErr = Err.Number
            On Error GoTo ProcError
            xDesc = "Could not start a Outlook Session.  " & Err.Description
            Err.Raise lErr, , xDesc
        ElseIf (m_oSession Is Nothing) Then
    '       raise error if no session
            On Error GoTo ProcError
            xDesc = "Could not start a Outlook Session"
            Err.Raise cimapiErr_NoMAPISession, , xDesc
        End If
        Set m_oMAPISession = New MAPI.Session
        m_oMAPISession.Logon , , False, False
        DoEvents
    End If
    Exit Function
ProcError:
    Err.Raise Err.Number, _
              Err.Source & ":CCIBackend.Connect", _
              Err.Description
End Function

Private Property Get ICIBackend_SupportsNestedFolders() As Boolean
    ICIBackend_SupportsNestedFolders = True
End Property
