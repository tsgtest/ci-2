VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "Application"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = True
Dim m_oContacts As CIO.CContacts
Dim m_oArray As XArrayDB
Dim m_oDOM As DOMDocument

Public Sub GetEntityInformation(Optional mpCI20Session As CIX.CSession)
    'get session if passed by MacPac
    If Not (mpCI20Session Is Nothing) Then
        Set g_oCI20Session = mpCI20Session
    End If
    
    RetrieveContacts
End Sub

Private Sub RetrieveContacts()
    
    Dim iSelList As CIX.ciSelectionLists
    Dim i As Integer
    Dim oStatus As Object
    Dim iData As CIO.ciRetrieveData
    
    On Error GoTo ProcError
    
    'exit if no documents open
    If Word.Documents.Count = 0 Then
        Exit Sub
    End If
    
    If g_oCI20Session Is Nothing Then
        Set g_oCI20Session = New CIX.CSession
    End If
    
    If Not g_oCI20Session.Initialized Then
        Set g_oCI20Session = Nothing
        Exit Sub
    End If
    
    g_oCI20Session.SessionType = ciSession_Connect
    
    'load entities
    LoadEntityArray
    
    If m_oArray.UpperBound(1) > -1 Then
        'get contacts
        Set m_oContacts = g_oCI20Session.GetContacts_Custom(oArray:=m_oArray)
    Else
        MsgBox "This document contains no updatable bookmarks.", _
               vbInformation, App.Title
    End If

    If Not m_oContacts Is Nothing Then
        FillOutForm
    End If
    
    DoEvents
    
    Set m_oContacts = Nothing
    Set m_oArray = Nothing
    Set m_oDOM = Nothing
    
    Exit Sub
ProcError:
    g_oError.RaiseError "ConnectForms.ApplicationRetrieveContacts"
    Exit Sub
End Sub

Private Function LoadEntityArray()
        Dim oBook As Word.Bookmark
        Dim i As Integer
        Dim iData As Integer
        Dim oElem As IXMLDOMElement
        Dim child As IXMLDOMElement
        Dim objNameNode As IXMLDOMNode
        Dim oNodeList As IXMLDOMNodeList
        Dim xEntity As String
        Dim oArrayTemp As XArrayDB
        Dim X As String
        Dim l As Long
        Dim xXMLPath As String
        
        
        Set m_oDOM = New DOMDocument
        m_oDOM.async = False
        'v. 2.4.4006 - xml should always be in App.Path
        xXMLPath = App.Path & "\ConnectForms.xml"
        m_oDOM.Load xXMLPath
        
        'check that the load of the XML document was successful
        If m_oDOM.parseError.reason <> "" Then
          ' there has been an error with the loaded XML - show the reason
          MsgBox "Could not load " & xXMLPath & "." & vbCr & vbCr & _
                 "Description:  " & m_oDOM.parseError.reason
          Exit Function
        End If
        
        'Fill Array of entities
        Set m_oArray = New XArrayDB

        With m_oArray
            .ReDim 0, -1, 0, 4

            For Each oBook In ActiveDocument.Bookmarks
                Set oElem = m_oDOM.documentElement.selectSingleNode _
                                ("Entity/detail/target[.='" & UCase(oBook.Name) & "']")
                If Not (oElem Is Nothing) Then
                    xEntity = GetEntity(oElem)
                    If xEntity <> Empty Then
'                        If BookmarkIsValid(oBook, xEntity) Then
                            On Error Resume Next
                            l = .Find(0, 0, xEntity, , XCOMP_EQ, XTYPE_STRING)
                            On Error GoTo ProcError
                            If l > -1 And .UpperBound(1) <> -1 Then
                                'update existing entity's data if necessary
                                iData = GetData(oElem)
                                If (.Value(l, 1) And iData) <> iData Then
                                    .Value(l, 1) = .Value(l, 1) + iData
                                End If
                                'update display if necessary
                                If .Value(l, 2) = "" Then
                                    .Value(l, 2) = GetDisplay(xEntity, oBook)
                                End If
                            Else
                                .Insert 1, i
                                .Value(i, 0) = xEntity
                                .Value(i, 1) = GetData(oElem)
                                .Value(i, 2) = GetDisplay(xEntity, oBook)
                                .Value(i, 3) = i + 1
                                .Value(i, 4) = GetUNID(xEntity)
                                i = i + 1
                            End If
'                        End If
                    End If
                End If
            Next oBook
        End With
    Exit Function
ProcError:
    g_oError.RaiseError "ConnectForms.Application.LoadEntityArray"
    Exit Function

End Function

Private Function GetEntity(oElem As IXMLDOMElement) As String
    GetEntity = oElem.parentNode.parentNode.Attributes(0).nodeValue
End Function

Private Function GetData(oElem As IXMLDOMElement) As Integer
    GetData = CInt(oElem.parentNode.childNodes(2).Text)
End Function

Private Function GetSource(oElem As IXMLDOMElement) As String
    GetSource = oElem.parentNode.childNodes(0).Text
End Function

Private Sub FillOutForm()
    Dim xSource As String
    Dim xValue As String
    Dim i As Integer
    Dim j As Integer
    Dim oNodeList As IXMLDOMNodeList
    Dim oElem As IXMLDOMElement
    Dim bIsOldPass As Boolean
    Dim bIsNewPass As Boolean
    Dim rBookmark As Word.Range
    
    On Error GoTo ProcError
    
    ClearVariables
    
    If m_oContacts.Count = 0 Then Exit Sub
    
    If ActiveDocument.ProtectionType <> wdNoProtection Then
        On Error Resume Next
        ActiveDocument.Unprotect "aln777x"
        If ActiveDocument.ProtectionType <> wdNoProtection Then
            ActiveDocument.Unprotect "aln888x"
            If ActiveDocument.ProtectionType <> wdNoProtection Then
                MsgBox "Could not unprotect form, please try unprotecting the document first.", vbExclamation, App.Title
                Exit Sub
            Else
                bIsNewPass = True
            End If
        Else
            bIsOldPass = True
        End If
        On Error GoTo ProcError
    End If
    
    For i = 1 To m_oContacts.Count
    
        With m_oContacts(i)
            'get element for contact type
            Set oElem = m_oDOM.documentElement.selectSingleNode("Entity[@name='" & .CustomTypeName & "']")
            'get bookmark list
            Set oNodeList = oElem.getElementsByTagName("target")
            For j = 0 To oNodeList.Length - 1
                If ActiveDocument.Bookmarks.Exists(oNodeList.Item(j).Text) Then
                    Set rBookmark = ActiveDocument.Bookmarks(oNodeList.Item(j).Text).Range
                    On Error Resume Next
                    If rBookmark.Fields.Count Then
                        rBookmark.Fields(1).Result.Text = _
                                GetContactField(m_oContacts(i), oElem.getElementsByTagName("source").Item(j).Text)
                    Else
                        rBookmark.Text = _
                                GetContactField(m_oContacts(i), oElem.getElementsByTagName("source").Item(j).Text)
                    End If
                End If
            Next j
        End With
        SaveVariableForReuse m_oContacts(i)
    Next i
    
    'reprotect document
    If bIsNewPass Then
        ActiveDocument.Protect wdAllowOnlyFormFields, True, "aln888x"
    ElseIf bIsOldPass Then
        ActiveDocument.Protect wdAllowOnlyFormFields, True, "aln777x"
    End If
    
    'make sure Word is activated
    Application.Activate
    
    Exit Sub
ProcError:
    g_oError.RaiseError "ConnectForms.FillOutForm"
    Exit Sub
End Sub

Private Function GetContactField(oContact As CIO.CContact, xSource As String) As String
    Dim xFieldValue As String
    Dim xCustomValue As String
    Dim oCustField As CCustomField
    
    With oContact
        Select Case xSource
            Case "Name"
                xFieldValue = .FullName
            Case "NameWithPrefix"
                xFieldValue = IIf(.prefix <> "", .prefix & " ", "") & .FullName
            Case "DisplayName"
                xFieldValue = .DisplayName
            Case "GoesBy"
                xFieldValue = .GoesBy
            Case "Salutation"
                xFieldValue = .Salutation
            Case "Title"
                xFieldValue = .Title
            Case "Prefix"
                xFieldValue = .prefix
            Case "FirstName"
                xFieldValue = .FirstName
            Case "MiddleName"
                xFieldValue = .MiddleName
            Case "LastName"
                xFieldValue = .LastName
            Case "Suffix"
                xFieldValue = .Suffix
            Case "FirmName"
                xFieldValue = .Company
            Case "Address"
                xFieldValue = .CoreAddress
            Case "FullAddress"
                xFieldValue = GetFullAddress(oContact)
            Case "StreetAddress"
                xFieldValue = .Street1 & _
                              IIf(.Street2 <> "", ", " & .Street2, "") & _
                              IIf(.Street3 <> "", ", " & .Street3, "")
            Case "Street1"
                xFieldValue = .Street1
            Case "Street2"
                xFieldValue = .Street2
            Case "Street3"
                xFieldValue = .Street3
            Case "City"
                xFieldValue = .City
            Case "State"
                xFieldValue = .State
            Case "Zip"
                xFieldValue = .ZipCode
            Case "CityStateZip"
                xFieldValue = .City & _
                              IIf(.State <> "", " " & .State, "") & _
                              IIf(.ZipCode <> "", " " & .ZipCode, "")
            Case "StateZip"
                xFieldValue = .State & _
                              IIf(.ZipCode <> "", " " & .ZipCode, "")
            Case "CityState"
                xFieldValue = .City & _
                              IIf(.State <> "", " " & .State, "")
            Case "Country"
                xFieldValue = .Country
            Case "Phone"
                xFieldValue = .Phone
            Case "PhoneExtension"
                xFieldValue = .PhoneExtension
            Case "Fax"
                xFieldValue = .Fax
            Case "Email"
                xFieldValue = .EMailAddress
        End Select
        'deal with custom properties
        If xFieldValue = "" And _
        UCase(xSource) Like "CUSTOM*" Then
            On Error Resume Next
            xFieldValue = .CustomFields(CInt(Mid(xSource, 7, Len(xSource) - 6))).Value
        End If
    
    End With
  
    GetContactField = xFieldValue
    
End Function

Public Function Initialized() As Boolean
    g_bIsWord12x = Int(Left(Word.Application.Version, InStr(Word.Application.Version, ".") - 1)) >= 12
    GetAppPaths
    If Not MacPacIsLegal Then
        xMsg = "Your evaluation copy of Connect Forms has expired." & _
               "  Please contact your system administrator."
        MsgBox xMsg, vbExclamation, "The Legal MacPac"
        On Error Resume Next
        MarkStartupSaved
        
        If g_bIsWord12x Then
            Word.AddIns("Connect Forms.dotm").Installed = False
        Else
            Word.AddIns("Connect Forms.dot").Installed = False
        End If
        
        Exit Function
    End If
      
    Initialized = True
End Function

Private Sub Class_Initialize()
    'initialize errors and events
    Set g_oGlobals = New CIO.CGlobals
    Set g_oIni = New CIO.CIni
    Set g_oError = New CIO.CError
    Set g_oReg = New CIO.CRegistry
    Set m_oUNID = New CIO.CUNID
End Sub

Private Sub Class_Terminate()
    Set m_oEvents = Nothing
    Set g_oIni = Nothing
    Set g_oError = Nothing
    Set g_oReg = Nothing
    Set m_oUNID = Nothing
    Set g_oGlobals = Nothing
End Sub
