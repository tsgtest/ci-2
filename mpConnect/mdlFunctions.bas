Attribute VB_Name = "mdlFunctions"
Private Const CSIDL_LOCAL_APPDATA = &H1C&
Private Const CSIDL_FLAG_CREATE = &H8000&
Private Const CSIDL_COMMON_DOCUMENTS = &H2E

Private Const SHGFP_TYPE_CURRENT = 0
Private Const SHGFP_TYPE_DEFAULT = 1
Private Const MAX_PATH = 260

Private Declare Function SHGetFolderPath Lib "shfolder" _
    Alias "SHGetFolderPathA" _
    (ByVal hwndOwner As Long, ByVal nFolder As Long, _
    ByVal hToken As Long, ByVal dwFlags As Long, _
    ByVal pszPath As String) As Long
    
Private Declare Function GetUserName Lib "advapi32.dll" Alias "GetUserNameA" ( _
    ByVal lpBuffer As String, nSize As Long) As Long

    
Public Function MacPacIsLegal() As Boolean
'Connect Forms doesn't use MacPac or Numbering signature key

    Dim d As Date
    Dim xCode As String
    Dim bIsValid As Boolean
    Dim oReg As CRegistry
    Dim oForm As frmLicenseNum
    Dim iPos As Integer
    Dim xMPPath As String
    
    On Error GoTo ProcError

    

'   first look for license in CI.ini
    xCode = xGetAppIni("CIApplication", "ConnectSignature")
    
    If xCode = "" Then
        bIsValid = False
    Else
'       unencrypt date
        d = UnEncryptDate(xCode)
        bIsValid = (Now < d)
    End If
    
'   load license key prompt
    If Not bIsValid Then
           Set oForm = New frmLicenseNum
    End If

    While Not bIsValid
        With oForm
'           show prompt
            .Show vbModal

            If .Cancelled Then
                Exit Function
            Else
'               unencrypt date
                d = UnEncryptDate(.Key)

'               test for validity
                bIsValid = (Now < UnEncryptDate(.Key))

'               write to ini if valid
                If bIsValid Then
                    bSetAppIni "CIApplication", "ConnectSignature", .Key
                Else
'                   set message for next time around
                    .Message = "Invalid entry.  Please enter a valid license number:"
                End If
            End If
        End With
    Wend

'   unload form if necessary
    If Not oForm Is Nothing Then
        Unload oForm
    End If
    
    MacPacIsLegal = bIsValid
    Exit Function
ProcError:
    MacPacIsLegal = False
    g_oError.RaiseError "ConnectForms.mdlFunctions.MacPacIsLegal"
End Function

Function xGetAppIni(xSection As String, xKey As String) As String
    xGetAppIni = _
        System.PrivateProfileString(g_xFirmIni, xSection, xKey)
End Function

Public Function UnEncryptDate(ByVal xCode As String) As Date
'returns the date encrypted in xcode
    Dim bRnd As Byte
    Dim xCodeMod As String
    Dim i As Integer
    Dim xDate As String
    Dim X As String
    
    On Error GoTo ProcError
    
'   remove garbage 1st digit
    xCode = Mid(xCode, 2)

'   store and remove random factor
    bRnd = Left(xCode, 1)
    xCode = Mid(xCode, 2)
    
'   collect chars at even indexes
    For i = 2 To Len(xCode) Step 2
        xCodeMod = xCodeMod & Mid(xCode, i, 1)
    Next
    
    While Len(xCodeMod)
        X = Left(xCodeMod, 1)
        If Not IsNumeric(X) Then
            X = Asc(X) - 100
        End If
        
        xDate = xDate & X
        
'       trim left char
        xCodeMod = Mid(xCodeMod, 2)
    Wend
    
'DanCore 9.2.0 - UnEncryptDateFix - remember to add fix to PostInstall utility
'   convert to date
    UnEncryptDate = DateSerial(Mid(xDate, 3, 4), Mid(xDate, 7, 2), Mid(xDate, 1, 2))
    Exit Function
    
ProcError:
    Exit Function
End Function

Function MarkStartupSaved()
'force startup templates to be 'saved'
    On Error Resume Next
    With Word.Application
        If g_bIsWord12x Then
            .Templates(g_xStartPath & "\Connect Forms.dotm").Saved = True
    '       error trap isn't enough for Word >=10;
    '       if files aren't there, trying to set .Saved will
    '       disable mouse pointer for rest of session
            If Dir(g_xStartPath & "\Connect Forms.dotm") <> "" Then _
                .Templates(g_xStartPath & "\Connect Forms.dotm").Saved = True
        Else
            .Templates(g_xStartPath & "\Connect Forms.dot").Saved = True
    '       error trap isn't enough for Word 10;
    '       if files aren't there, trying to set .Saved will
    '       disable mouse pointer for rest of session
            If Dir(g_xStartPath & "\Connect Forms.dot") <> "" Then _
                .Templates(g_xStartPath & "\Connect Forms.dot").Saved = True
        End If
    End With

    Err.Clear
End Function

Function GetAppPaths()

    With Application.Options
'       global in mpVariables
        g_xStartPath = .DefaultFilePath(wdStartupPath)
        
        If g_bIsWord12x Then
            If Dir(g_xStartPath & "\Connect Forms.dotm") = "" Then
    '           startup templates may be in Word's default startup directory
                g_xStartPath = .Application.AddIns("Connect Forms.dotm").Path
            End If
        Else
            If Dir(g_xStartPath & "\Connect Forms.dot") = "" Then
    '           startup templates may be in Word's default startup directory
                g_xStartPath = .Application.AddIns("Connect Forms.dot").Path
            End If
        End If
    End With
    
    g_xFirmIni = GetAppPath() & "\CI.ini"
End Function

Function bSetAppIni(xSection As String, _
                             xKey As String, _
                             xValue As String) As Boolean
    System.PrivateProfileString(g_xFirmIni, xSection, xKey) = xValue
End Function

Function SaveVariableForReuse(oC As CIO.CContact)

    On Error Resume Next
    
    With oC
        ActiveDocument.Variables("zzmpconnect_" & .CustomTypeName).Value = oC.DisplayName & Chr(134) & oC.UNID
    End With
    
End Function

Public Function GetDisplay(ByVal xName As String, oBook As Word.Bookmark) As String
    Dim xTemp As String
'   gets the value of variable xName
    On Error Resume Next
    xTemp = ActiveDocument.Variables("zzmpconnect_" & xName)
    If xTemp <> Empty Then
        xTemp = Left(xTemp, InStr(xTemp, Chr(134)) - 1)
    ElseIf oBook.Range.Text <> "" Then
        If Asc(oBook.Range.Text) <> 32 Then
            xTemp = "(edited manually)"
        End If
    End If
    GetDisplay = xTemp
End Function

Public Function GetUNID(ByVal xName As String) As String
    Dim xTemp As String
'   gets the value of variable xName
    On Error Resume Next
    xTemp = ActiveDocument.Variables("zzmpconnect_" & xName)
    If xTemp <> Empty Then
        xTemp = Mid(xTemp, InStr(xTemp, Chr(134)) + 1, Len(xTemp))
    End If
    GetUNID = xTemp
End Function

Public Function ClearVariables(Optional bMacPacOnly As Boolean = True)
    Dim oVar As Word.Variable
    
    For Each oVar In ActiveDocument.Variables
        If bMacPacOnly Then
            If UCase(oVar.Name) Like "ZZMPCONNECT_*" Then
                oVar.Delete
            End If
        Else
            oVar.Delete
        End If
    Next oVar


End Function

Public Function GetAppPath() As String
'Subsitute for App.Path call: first looks in the registry for Public documents location
'If not found, then uses regular App.Path command

    Dim oReg As CRegistry
    Dim vPath As Variant
    Dim lRet As Long
    Dim xTemp As String
    Static xDir As String
    
    On Error GoTo ProcError
    Set oReg = New CRegistry
    'get registry key value
    vPath = String(255, Chr(32))
    lRet = oReg.GetValue(HKEY_LOCAL_MACHINE, "Software\The Sackett Group\CI", "DataDirectory", vPath)
    vPath = RTrim(vPath)
    
    If Len(vPath) <= 0 Then
    'try old registry key value
        vPath = String(255, Chr(32))
        lRet = oReg.GetValue(HKEY_LOCAL_MACHINE, "Software\Legal MacPac\9.x", "ContactIntegrationData", vPath)
        vPath = RTrim(vPath)
    End If
    
    'exists, so set path to value
    If Len(vPath) > 0 Then
'       substitute real path for user var if it exists in string
        If (InStr(UCase(vPath), "<USERNAME>") > 0) Then
    '       use API to get Windows user name
            xTemp = GetUserVarPath(CStr(vPath))
        ElseIf (InStr(UCase(vPath), "<COMMON_DOCUMENTS>") > 0) Then
            xTemp = GetCommonDocumentsPath(CStr(vPath))
        Else
    '       use environmental variable
            xTemp = GetEnvironVarPath(CStr(vPath))
        End If
    Else
        'registry value empty, so set path to App.Path
        xTemp = App.Path
    End If
    
'   raise error if path is invalid
    If Not DirExists(xTemp) Then
        MsgBox xTemp & "\CI.ini could not be found.  Please contact your administrator.", _
               vbExclamation, App.Title
        Exit Function
    End If
    
'   store for future retrieval
    xDir = xTemp
    GetAppPath = xTemp
    Exit Function
ProcError:
    g_oError.RaiseError "ConnectForms.mdlFunctions.GetAppPath"
    Exit Function
End Function

Public Function GetCommonDocumentsPath(ByVal xPath As String) As String
'substitutes user path for <COMMON_DOCUMENTS>
    Dim xBuf As String
    
    On Error GoTo ProcError
    
'   get common documents folder
    xBuf = String(255, " ")
    
    SHGetFolderPath 0, CSIDL_COMMON_DOCUMENTS Or CSIDL_FLAG_CREATE, 0, SHGFP_TYPE_CURRENT, xBuf

    If Len(xBuf) = 0 Then
'       alert to no logon name
        Err.Raise mpError.mpError_NullValueNotAllowed, , _
            "Common_Documents is empty.  You might not be logged on to the system. " & _
                "Please log off, and log on again."
    End If
    
'   trim extraneous buffer chars
    xBuf = RTrim(xBuf)
    xBuf = Left(xBuf, Len(xBuf) - 1)
    
'   substitute common documents at specified point in path
    GetCommonDocumentsPath = Replace(xPath, "<COMMON_DOCUMENTS>", xBuf)
        
    Exit Function
ProcError:
    g_oError.RaiseError "ConnectForms.mdlFunctions.GetCommonDocumentsPath"
    Exit Function
End Function

Public Function GetUserVarPath(ByVal xPath As String) As String
'substitutes user path for <UserName>
    Dim xBuf As String
    
    On Error GoTo ProcError
    
'   get logon name
    xBuf = String(255, " ")
    GetUserName xBuf, 255
    
    If Len(xBuf) = 0 Then
'       alert to no logon name
        Err.Raise mpError.mpError_NullValueNotAllowed, , _
            "UserName is empty.  You might not be logged on to the system. " & _
                "Please log off, and log on again."
    End If
    
'   trim extraneous buffer chars
    xBuf = RTrim(xBuf)
    xBuf = Left(xBuf, Len(xBuf) - 1)
    
'   substitute user name at specified point in path
    GetUserVarPath = Replace(xPath, "<UserName>", xBuf)
        
    Exit Function
ProcError:
    g_oError.RaiseError "ConnectForms.mdlFunctions.GetUserVarPath"
    Exit Function
End Function

Public Function GetEnvironVarPath(ByVal xPath As String) As String
'substitutes environment variable for <xToken>;
'if variable doesn't exist, returns path unchanged
    Dim xToken As String
    Dim iPosStart As Integer
    Dim iPosEnd As Integer
    Dim xValue As String
    
    On Error GoTo ProcError
    iPosStart = InStr(xPath, "<")
    iPosEnd = InStr(xPath, ">")
    
    If (iPosStart > 0) And (iPosEnd > 0) Then
        xToken = Mid(xPath, iPosStart + 1, iPosEnd - iPosStart - 1)
        xValue = Environ(xToken)
    End If
    
    If xValue <> "" Then
        GetEnvironVarPath = Replace(xPath, "<" & xToken & ">", xValue)
    Else
        GetEnvironVarPath = xPath
    End If
    
    Exit Function
ProcError:
    g_oError.RaiseError "ConnectForms.mdlFunctions.GetEnvironVarPath"
    Exit Function
End Function

Public Function DirExists(ByVal xDirName As String) As Boolean
    On Error Resume Next
    DirExists = (GetAttr(xDirName) And vbDirectory) = vbDirectory
    On Error GoTo 0
End Function
Public Function GetFullAddress(oContact As CContact) As String
'this function will return full address including title & company
'except for name.  If .FullName = .Company, then the company
'will not be included

    Dim xTemp As String
    
    With oContact
        'get title
        xTemp = .Title
        'get company
        xTemp = IIf(xTemp <> "", xTemp & vbCrLf, "") & IIf(.FullName <> .Company, .Company, "")
        'get address
        xTemp = IIf(xTemp <> "", xTemp & vbCrLf, "") & .CoreAddress
    End With

    GetFullAddress = xTemp

End Function
