Attribute VB_Name = "mdlGlobal"
Option Explicit

Private Declare Function WritePrivateProfileSection Lib "kernel32" Alias _
    "WritePrivateProfileSectionA" (ByVal lpAppName As String, ByVal _
    lpString As String, ByVal lpFileName As String) As Long

Public Const ciIA5InternalID As Integer = 101

Public g_vCols(3, 1) As Variant
Public g_iSortCol As ciListingCols
Public g_bDisplayByContactType As Boolean
Public g_oCnn As Interaction.Connection
Public g_oProxiedUser As Interaction.IAUser

Public g_oError As CIO.CError
Public g_oGlobals As CIO.CGlobals
Public g_oIni As CIO.CIni
Public g_oReg As CIO.CRegistry
Public g_oSession As CIO.CSessionType
Public g_oConstants As CIO.CConstants
Public g_iID As Integer

Public Sub CorrectTDBComboMismatch(oTDBCombo As TDBCombo, iReposition As Integer)
'resets the tdb combo value to the previous match -
'this procedure should be called in the
'TDBCombo Mismatch even procedure only
    Dim bytStart As Byte
    
    iReposition = False
    
    With oTDBCombo
'       get current selection start
        bytStart = .SelStart
        
'       reset the text to the current list text
        If .ListField = Empty Then
            .Text = .Columns(0)
        Else
            .Text = .Columns(.ListField)
        End If
        
'       return selection to original selection
        .SelStart = Max(CDbl(bytStart - 1), 0)
        .SelLength = Len(.Text)
    End With
End Sub

Function Max(i As Double, j As Double) As Double
    If i > j Then
        Max = i
    Else
        Max = j
    End If
End Function

Function Min(i As Double, j As Double) As Double
    If i > j Then
        Min = j
    Else
        Min = i
    End If
End Function

Public Function bValidateBoundTDBCombo(oTDBCombo As TDBCombo) As Boolean
'returns FALSE if invalid author has been specified, else TRUE
    With oTDBCombo
        If .BoundText = "" Then
            MsgBox "Invalid entry.  Please select an item " & _
                   "from the list.", vbExclamation, App.Title
        Else
'           set text of control equal to the
'           display text of the selected row
            .Text = .Columns(.ListField)
            bValidateBoundTDBCombo = True
        End If
    End With
End Function

Public Function GetMaxResults() As Long
'returns the maximum number of contacts that can be retrieved
    Dim xMaxresults As String
    Dim lMaxResults As Long
    
    On Error GoTo ProcError
    xMaxresults = g_oIni.GetUserIni("CIIA5", "MaxResults")
    If xMaxresults = Empty Or Not IsNumeric(xMaxresults) Then
        lMaxResults = 500
    Else
        lMaxResults = CLng(xMaxresults)
    End If
    GetMaxResults = lMaxResults
    Exit Function
ProcError:
    g_oError.RaiseError "CIIAWeb.CCIBackend.GetMaxResults"
    Exit Function
End Function

