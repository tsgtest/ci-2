VERSION 5.00
Object = "{0D62353D-DBA2-11D1-B5DF-0060976089D0}#6.0#0"; "TDBL6.OCX"
Begin VB.Form frmProjectFilter 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   " Find Contacts In Project"
   ClientHeight    =   1980
   ClientLeft      =   48
   ClientTop       =   336
   ClientWidth     =   6540
   Icon            =   "frmProjectFilter.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1980
   ScaleWidth      =   6540
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin TrueDBList60.TDBCombo cmbProjects 
      Height          =   330
      Left            =   225
      OleObjectBlob   =   "frmProjectFilter.frx":000C
      TabIndex        =   1
      Top             =   390
      Width           =   4710
   End
   Begin VB.CommandButton btnCancel 
      Caption         =   "Cancel"
      Height          =   400
      Left            =   5115
      TabIndex        =   6
      Top             =   1440
      Width           =   1200
   End
   Begin VB.CommandButton btnSearch 
      Caption         =   "O&K"
      Height          =   400
      Left            =   3855
      TabIndex        =   5
      ToolTipText     =   "Execute search"
      Top             =   1440
      Width           =   1200
   End
   Begin VB.CommandButton btnFind 
      Caption         =   "&Find Project..."
      Height          =   390
      Left            =   5025
      TabIndex        =   2
      Top             =   375
      Width           =   1305
   End
   Begin VB.CheckBox chkPersonContacts 
      Caption         =   "&Only Person Contacts"
      Height          =   270
      Left            =   3345
      TabIndex        =   4
      Top             =   900
      Width           =   1875
   End
   Begin VB.CheckBox chkDistributionList 
      Caption         =   "Only Contacts On &Distribution List"
      Height          =   270
      Left            =   270
      TabIndex        =   3
      Top             =   900
      Width           =   2775
   End
   Begin VB.Label lblProject 
      Caption         =   "&Project:"
      Height          =   300
      Left            =   285
      TabIndex        =   0
      Top             =   180
      Width           =   2130
   End
End
Attribute VB_Name = "frmProjectFilter"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private g_oCnn As Interaction.Connection
Private m_oArr As XArrayDB
Private m_bCanceled As Boolean

Public Property Get Canceled() As Boolean
    Canceled = m_bCanceled
End Property

Public Property Set IAConnection(oCnn As Interaction.Connection)
    Set g_oCnn = oCnn
End Property

Private Sub btnCancel_Click()
    Me.Hide
    m_bCanceled = True
End Sub

Private Sub btnFind_Click()
    Dim oIAProj As IAProject
    On Error GoTo ProcError
    Me.MousePointer = vbHourglass
    Set oIAProj = g_oCnn.FindProjectDlg("Find Project")
    If Not oIAProj Is Nothing Then
        'check if project is in list
        With Me.cmbProjects
            .BoundText = oIAProj.ProjectId
            If .BoundText = Empty Then
                'add project to array
                With .Array
                    .Insert 1, .UpperBound(1) + 1
                    .Value(.UpperBound(1), 0) = oIAProj.DisplayName
                    .Value(.UpperBound(1), 1) = oIAProj.ProjectId
                End With
                .ReBind
                ResizeTDBCombo Me.cmbProjects, 8
                
                'select the item
                Me.cmbProjects.BoundText = oIAProj.ProjectId
            End If
        End With
    End If
    Me.MousePointer = vbDefault
    Exit Sub
ProcError:
    Dim lErr As Long
    Dim xDesc As String

    lErr = Err.Number
    xDesc = Err.Description
    
    Me.MousePointer = vbDefault

    Err.Number = lErr
    Err.Description = xDesc
    g_oError.RaiseError "CIIAWeb.frmProjectFilter.btnFind_Click"
    Exit Sub
End Sub

Private Sub btnSearch_Click()
    If IsNull(Me.cmbProjects.SelectedItem) Then
        MsgBox "Please select a project.", vbExclamation
    Else
        Me.Hide
        m_bCanceled = False
    End If
End Sub

Private Sub cmbProjects_Mismatch(NewEntry As String, Reposition As Integer)
    On Error GoTo ProcError
    CorrectTDBComboMismatch Me.cmbProjects, Reposition
    Exit Sub
ProcError:
    g_oError.ShowError
    Exit Sub
End Sub

Private Sub cmbProjects_Validate(Cancel As Boolean)
    On Error GoTo ProcError
    If Me.cmbProjects.Text <> Empty Then
        bValidateBoundTDBCombo Me.cmbProjects
    End If
    Exit Sub
ProcError:
    g_oError.ShowError
    Exit Sub
End Sub

Private Sub Form_Activate()
    On Error GoTo ProcError
    With Me.cmbProjects
        If .Array.Count(1) > 0 Then
            .SelectedItem = 0
        End If
    End With
    Exit Sub
ProcError:
    g_oError.ShowError
    Exit Sub
End Sub

Private Sub Form_Load()
    Dim xSec As String
    
    GetMyProjects
    
    m_bCanceled = True
End Sub

Private Sub GetMyProjects()
    Dim oProjSearch As IAProjectSearch
    
    On Error GoTo ProcError
    
    Set oProjSearch = g_oCnn.NewProjectSearch
    With oProjSearch
        Dim oIAModule As IAProjectModule
        Set oIAModule = g_oCnn.ProjectModules.FindById("200")
        Set .Module = oIAModule
        .InMyProjects = True
        .Execute
        Dim oProjs As IAProjects
        Dim oProj As IAProject
        Set oProjs = .Results
    End With
        
    Dim oArray As XArrayDB
    Set oArray = New XArrayDB
    oArray.ReDim 0, -1, 0, 1

    For Each oProj In oProjs
        oArray.Insert 1, oArray.UpperBound(1) + 1
        oArray.Value(oArray.UpperBound(1), 0) = oProj.DisplayName
        oArray.Value(oArray.UpperBound(1), 1) = oProj.ProjectId
    Next oProj
    Me.cmbProjects.Array = oArray
    ResizeTDBCombo Me.cmbProjects, 8
    Exit Sub
ProcError:
    g_oError.RaiseError "CIIAWeb.frmProjectFilter.GetMyProjects"
    Exit Sub
End Sub

Private Sub ResizeTDBCombo(tdbCBX As TrueDBList60.TDBCombo, Optional iMaxRows As Integer)
'resizes the dropdown of the true db combo
'to display the specified number of rows.
    Dim oArr As XArrayDB
    Dim iRows As Integer
    
    On Error GoTo ProcError
    With tdbCBX
        Set oArr = .Array
        If oArr Is Nothing Then
            iRows = 0
        End If
        
        If oArr.Count(1) < iMaxRows Then
            iRows = oArr.Count(1)
        Else
            iRows = iMaxRows
        End If
            
        On Error Resume Next
        .DropdownHeight = ((iRows) * .RowHeight + 8)
        
    End With
    Exit Sub
ProcError:
    Err.Raise Err.Number, "CII5.frmProjectFilter.ResizeTDBCombo", Err.Description
    Exit Sub
End Sub


