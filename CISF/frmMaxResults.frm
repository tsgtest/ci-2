VERSION 5.00
Object = "{0D62353D-DBA2-11D1-B5DF-0060976089D0}#6.0#0"; "TDBL6.OCX"
Begin VB.Form frmMaxResults 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Set Maximum Number"
   ClientHeight    =   2220
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   4425
   Icon            =   "frmMaxResults.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2220
   ScaleWidth      =   4425
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.ListBox lstSetMax 
      Height          =   1620
      ItemData        =   "frmMaxResults.frx":000C
      Left            =   210
      List            =   "frmMaxResults.frx":000E
      TabIndex        =   3
      Top             =   255
      Width           =   1305
   End
   Begin VB.CommandButton btnCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   400
      Left            =   3015
      TabIndex        =   2
      Top             =   1650
      Width           =   1200
   End
   Begin VB.CommandButton btnOK 
      Caption         =   "O&K"
      Default         =   -1  'True
      Height          =   400
      Left            =   1725
      TabIndex        =   1
      Top             =   1650
      Width           =   1200
   End
   Begin VB.Label lblSetMax 
      BackStyle       =   0  'Transparent
      Caption         =   "Specify the maximum number of contacts to retrieve from Saleforce."
      Height          =   720
      Left            =   1695
      TabIndex        =   0
      Top             =   210
      Width           =   2565
   End
End
Attribute VB_Name = "frmMaxResults"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Option Explicit

Private m_bCancelled As Boolean

Public Property Get Cancelled()
    Cancelled = m_bCancelled
End Property

Private Sub btnCancel_Click()
    DoEvents
    Me.Hide
    m_bCancelled = True
End Sub

Private Sub btnOK_Click()
    DoEvents
    Me.Hide
    m_bCancelled = False
End Sub

Private Sub Form_Load()
    Dim xMax As String
    Dim i As Integer
    With Me.lstSetMax
        .AddItem "50"
        .AddItem "100"
        .AddItem "500"
        .AddItem "1,000"
        .AddItem "2,000"
        .AddItem "3,000"
        .AddItem "5,000"
        .AddItem "10,000"
    
        .ListIndex = 1
    
        xMax = g_oIni.GetUserIni("CISF", "MaxResults")
    
        If xMax <> Empty Then
            
            For i = 1 To Me.lstSetMax.ListCount
                If .List(i) = xMax Then
                    .ListIndex = i
                End If
            Next i
        End If
    End With
End Sub

Private Sub lstSetMax_DblClick()
    btnOK_Click
End Sub
