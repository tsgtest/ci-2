VERSION 5.00
Begin VB.Form frmFilter 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   " Find"
   ClientHeight    =   5025
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6765
   Icon            =   "frmFilter.frx":0000
   LinkTopic       =   "Form1"
   LockControls    =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5025
   ScaleWidth      =   6765
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.Frame fraFilter 
      Caption         =   "Display Contacts Where:"
      Height          =   3135
      Left            =   270
      TabIndex        =   0
      Top             =   210
      Width           =   4485
      Begin VB.TextBox txtFilterField 
         Height          =   325
         Index           =   0
         Left            =   225
         TabIndex        =   2
         Top             =   570
         Width           =   4000
      End
      Begin VB.TextBox txtFilterField 
         Height          =   325
         Index           =   1
         Left            =   225
         TabIndex        =   4
         Top             =   1230
         Width           =   4000
      End
      Begin VB.TextBox txtFilterField 
         Height          =   325
         Index           =   2
         Left            =   225
         TabIndex        =   6
         Top             =   1875
         Width           =   4000
      End
      Begin VB.TextBox txtFilterField 
         Height          =   325
         Index           =   3
         Left            =   225
         TabIndex        =   8
         Top             =   2535
         Width           =   4000
      End
      Begin VB.Label lblFilterField 
         Caption         =   "##"
         Height          =   225
         Index           =   0
         Left            =   270
         TabIndex        =   1
         Top             =   360
         Width           =   3930
      End
      Begin VB.Label lblFilterField 
         Caption         =   "##"
         Height          =   225
         Index           =   1
         Left            =   270
         TabIndex        =   3
         Top             =   1020
         Width           =   3930
      End
      Begin VB.Label lblFilterField 
         Caption         =   "##"
         Height          =   225
         Index           =   2
         Left            =   270
         TabIndex        =   5
         Top             =   1665
         Width           =   3930
      End
      Begin VB.Label lblFilterField 
         Caption         =   "##"
         Height          =   225
         Index           =   3
         Left            =   255
         TabIndex        =   7
         Top             =   2325
         Width           =   3930
      End
   End
   Begin VB.CommandButton btnSearch 
      Caption         =   "O&K"
      Default         =   -1  'True
      Height          =   400
      Left            =   5355
      TabIndex        =   13
      ToolTipText     =   "Execute search"
      Top             =   3810
      Width           =   1200
   End
   Begin VB.ComboBox cmbSearchType 
      Appearance      =   0  'Flat
      Height          =   315
      ItemData        =   "frmFilter.frx":000C
      Left            =   480
      List            =   "frmFilter.frx":0016
      Style           =   2  'Dropdown List
      TabIndex        =   12
      Top             =   4395
      Width           =   4020
   End
   Begin VB.CommandButton btnCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   400
      Left            =   5355
      TabIndex        =   14
      Top             =   4290
      Width           =   1200
   End
   Begin VB.ComboBox cmbSortBy 
      Appearance      =   0  'Flat
      Height          =   315
      ItemData        =   "frmFilter.frx":0028
      Left            =   480
      List            =   "frmFilter.frx":002A
      Style           =   2  'Dropdown List
      TabIndex        =   10
      Top             =   3720
      Width           =   4020
   End
   Begin VB.Label lblSortBy 
      Caption         =   "Sort &By:"
      Height          =   225
      Left            =   525
      TabIndex        =   9
      Top             =   3510
      Width           =   1230
   End
   Begin VB.Label lblSearchType 
      Caption         =   "Search T&ype:"
      Height          =   225
      Left            =   510
      TabIndex        =   11
      Top             =   4185
      Width           =   1230
   End
End
Attribute VB_Name = "frmFilter"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private m_bCancelled As Boolean
Private m_bSearchNow As Boolean
Private xMsg As String
Private m_oBackend As CIO.ICIBackend

Public Property Set Backend(oNew As CIO.ICIBackend)
    Set m_oBackend = oNew
End Property

Public Property Get Canceled() As Boolean
    Canceled = m_bCancelled
End Property

Private Sub btnCancel_Click()
    Me.Hide
    m_bCancelled = True
End Sub

Private Sub btnSearch_Click()
    m_bCancelled = False
    On Error Resume Next
    Me.Hide
    DoEvents
End Sub

Private Sub Form_Activate()
    Dim i As Integer
    
'   select contents of first criterion
    On Error GoTo ProcError
    With Me.txtFilterField(0)
        .SetFocus
        .SelStart = 0
        .SelLength = Len(.Text)
    End With
    Exit Sub
ProcError:
    g_oError.ShowError
    Exit Sub
End Sub

Private Sub Form_Load()
    Dim i As Integer
    Dim iDisplay As Integer
    Dim iNumFields As Integer
    Dim oFilter As CIO.CFilter
    
    On Error GoTo ProcError
    m_bCancelled = True
    With m_oBackend.Filter
        iNumFields = .CountFields
        
        For i = 1 To iNumFields
            With .FilterFields(i)
'               set caption for each filter field control
                Me.lblFilterField(i - 1).Caption = "&" & .Name & ":"
                'Me.txtFilterField(i - 1).Text = .Value
            End With
            
        Next i
        
'       show only those controls corresponding
'       to existing filter fields
        For i = 1 To 4
            Me.lblFilterField(i - 1).Visible = i <= iNumFields
            Me.txtFilterField(i - 1).Visible = i <= iNumFields
        Next i
        
        With Me.fraFilter
            Dim dAdjustment As Double
            
            'get amount to move up controls - we'll have to move up controls
            'if not all 4 of the filter fields are being used
            dAdjustment = (((4 - iNumFields) * (.Height / 4)) + _
                      Min((4 - iNumFields), 1) * 100) - 300
                      
            .Height = .Height - dAdjustment
        End With
        
        'adjust vertical position of all controls
        Me.lblSortBy.Top = Me.lblSortBy.Top - dAdjustment
        Me.lblSearchType.Top = Me.lblSearchType.Top - dAdjustment
        Me.cmbSortBy.Top = Me.cmbSortBy.Top - dAdjustment
        Me.cmbSearchType.Top = Me.cmbSearchType.Top - dAdjustment
        Me.btnCancel.Top = Me.btnCancel.Top - dAdjustment
        Me.btnSearch.Top = Me.btnSearch.Top - dAdjustment
        Me.Height = Me.Height - dAdjustment
        
'       load sort columns
        With Me.cmbSortBy
            .AddItem "Last Name"
            .ItemData(.ListCount - 1) = 2
            .AddItem "First Name"
            .ItemData(.ListCount - 1) = 3
            .AddItem "Company"
            .ItemData(.ListCount - 1) = 1
'
'            For i = 0 To 3
'                If g_vCols(i, 0) <> Empty Then
'                    .AddItem g_vCols(i, 0)
'                    .ItemData(.ListCount - 1) = i
'                End If
'            Next i
        End With
        
        'select previous sort column
        Me.cmbSearchType.ListIndex = 0
        If g_iSortCol = Empty Then
            g_iSortCol = ciListingCols_DisplayName
        End If
        
        Me.cmbSortBy.ListIndex = g_iSortCol
    End With
    
    Exit Sub
ProcError:
    g_oError.ShowError
    Exit Sub
End Sub

Public Sub ResetFilter()
    Dim i As Integer
    On Error Resume Next
    With Me.txtFilterField
        For i = .LBound To .UBound
            .Item(i).Text = ""
        Next i
    End With
End Sub

Private Sub txtFilterField_GotFocus(Index As Integer)
    With Me.txtFilterField(Index)
        .SelStart = 0
        .SelLength = Len(.Text)
    End With
End Sub
