VERSION 5.00
Begin VB.Form frmAddresses 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   " Select An Address"
   ClientHeight    =   3210
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6435
   FillColor       =   &H00C0FFFF&
   FillStyle       =   0  'Solid
   Icon            =   "frmAddresses.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3210
   ScaleWidth      =   6435
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.ListBox lstAddressTypes 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      Height          =   1590
      Left            =   135
      TabIndex        =   1
      Top             =   780
      Width           =   2040
   End
   Begin VB.CommandButton btnOK 
      Caption         =   "O&K"
      Default         =   -1  'True
      Height          =   400
      Left            =   4050
      TabIndex        =   3
      Top             =   2730
      Width           =   1100
   End
   Begin VB.CommandButton btnCancel 
      Cancel          =   -1  'True
      Caption         =   "Cancel"
      Height          =   400
      Left            =   5220
      TabIndex        =   4
      Top             =   2730
      Width           =   1100
   End
   Begin VB.CheckBox chkPromptForMissingAddresses 
      Caption         =   "&Continue to prompt for addresses when necessary"
      Height          =   390
      Left            =   165
      TabIndex        =   2
      Top             =   2730
      Value           =   1  'Checked
      Visible         =   0   'False
      Width           =   6015
   End
   Begin VB.Label lblDetailCaptions 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      ForeColor       =   &H80000008&
      Height          =   1500
      Left            =   2355
      TabIndex        =   7
      Top             =   975
      Width           =   870
   End
   Begin VB.Label lblAddresses 
      Caption         =   "The requested address for this contact does not exist.  Please select from one of the available addresses listed below."
      Height          =   405
      Left            =   135
      TabIndex        =   0
      Top             =   150
      Width           =   6150
   End
   Begin VB.Label lblAddressType 
      BackStyle       =   0  'Transparent
      Caption         =   "Address T&ype:"
      Height          =   285
      Left            =   1275
      TabIndex        =   6
      Top             =   3330
      Visible         =   0   'False
      Width           =   1125
   End
   Begin VB.Label lblDetail 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BackStyle       =   0  'Transparent
      ForeColor       =   &H80000008&
      Height          =   1500
      Left            =   3225
      TabIndex        =   5
      Top             =   975
      Width           =   2820
   End
   Begin VB.Shape Shape1 
      BorderStyle     =   0  'Transparent
      Height          =   1785
      Left            =   2160
      Top             =   780
      Width           =   4140
   End
End
Attribute VB_Name = "frmAddresses"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private m_oAddresses As CIO.CAddresses
Private m_xContactName As String
Private m_bCancelled As Boolean
Private m_bPromptForMissingPhones As Boolean
Private m_bPromptForMultiplePhones As Boolean
Private m_bPromptForMultiple As Boolean
Private m_oB As CIO.ICIBackend

'**********************************************************
'   Properties
'**********************************************************
Public Property Let ContactName(ByVal xNew As String)
    m_xContactName = xNew
End Property

Public Property Get ContactName() As String
    ContactName = m_xContactName
End Property

Public Property Let Cancelled(bNew As Boolean)
    m_bCancelled = bNew
End Property

Public Property Get Cancelled() As Boolean
    Cancelled = m_bCancelled
End Property

Public Property Set Backend(oNew As ICIBackend)
    Set m_oB = oNew
End Property

Public Property Set Addresses(oNew As CIO.CAddresses)
    Set m_oAddresses = oNew
End Property

Public Property Get Addresses() As CIO.CAddresses
    Set Addresses = m_oAddresses
End Property

'**********************************************************
'   Methods
'**********************************************************
'**********************************************************
'   Event Procedures
'**********************************************************
Private Sub btnCancel_Click()
    Me.Cancelled = True
    Me.Hide
    DoEvents
End Sub

Private Sub btnOK_Click()
    Me.Hide
    DoEvents
End Sub

Private Sub Form_Activate()
    If Me.lstAddressTypes.ListCount > 1 Then
        Me.lstAddressTypes.SetFocus
    End If
End Sub

Private Sub lstAddressTypes_Click()
    ShowListingDetail
End Sub

Private Sub Form_Load()
'   turn off prompt for existing addresses and phones-
'   will get reset in btnOK_Click, btnCancel_Click
    On Error GoTo ProcError
    Me.Cancelled = False
    
    Dim oAddress As CIO.CAddress
    
    With Me.lstAddressTypes
        'load address types into list
        For Each oAddress In m_oAddresses
            .AddItem oAddress.Name
        Next oAddress
        
        On Error Resume Next
        .ListIndex = 0
    
        'load message caption
        Me.lblAddresses.Caption = "The requested address for '" & _
            Me.ContactName & "' does not exist.  Please select " & _
            "from one of the available addresses listed below."
            
        'load label caption
        Me.lblDetailCaptions.Caption = "Street 1:" & vbCr & _
                                        "Street 2:" & vbCr & _
                                        "Street 3:" & vbCr & _
                                        "City:" & vbCr & _
                                        "State:" & vbCr & _
                                        "Zip:" & vbCr & _
                                        "Country:" & vbCr
    End With
    
    Exit Sub
ProcError:
    g_oError.ShowError
    Exit Sub
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
    Me.Hide
End Sub

Private Sub lstAddressTypes_DblClick()
'do what happens when OK is clicked
    btnOK_Click
End Sub

'**********************************************************
'   Internal Functions
'**********************************************************
Private Sub ShowListingDetail()
'displays detail form with detail for
'specified listing with specified address

    Dim oAddress As CIO.CAddress
    Dim oUNID As CIO.CUNID
    Dim oContact As CIO.CContact
    Dim xToken As String
    
'   get address type from dlg
    Set oUNID = New CIO.CUNID
    
    On Error GoTo ProcError
    With Me.lstAddressTypes
        If .ListIndex > -1 Then
            Set oAddress = m_oAddresses.Item(.ListIndex + 1)
            
            Set oContact = m_oB.GetContacts(oUNID.GetListing(oAddress.UNID), _
                oAddress, , ciRetrieveData_Addresses, ciAlert_None).Item(1)
                
            xToken = "<STREET1>" & vbCr & _
                    "<STREET2>" & vbCr & _
                    "<STREET3>" & vbCr & _
                    "<CITY>" & vbCr & _
                    "<STATE>" & vbCr & _
                    "<ZIPCODE>" & vbCr & _
                    "<COUNTRY>" & vbCr
                    
            Me.lblDetail.Caption = oContact.GetDetail(xToken, False)
        End If
    End With
    DoEvents

'   enable OK if there is detail
    Me.btnOK.Enabled = (Me.lblDetail.Caption <> "")
    Exit Sub
ProcError:
   g_oError.RaiseError "CIO.frmAddresses.ShowListingDetail"
    Exit Sub
End Sub

